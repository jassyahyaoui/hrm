<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel {

    public function registerBundles() {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),
            new FOS\UserBundle\FOSUserBundle(),
            new UserBundle\UserBundle(),
            new FOS\RestBundle\FOSRestBundle(),
            new AdminBundle\AdminBundle(),
            new FOS\JsRoutingBundle\FOSJsRoutingBundle(),
            new OAuth2WpBundle\OAuth2WpBundle(),
            new ExpenseBundle\ExpenseBundle(),
            new requestAbsenceBundle\requestAbsenceBundle(),
            new JMS\SerializerBundle\JMSSerializerBundle(),
            new PaydayVariationsBundle\PaydayVariationsBundle(),
            new DashboardBundle\DashboardBundle(),
            new FileBundle\FileBundle(),
            new ExportBundle\ExportBundle(),
            new Accord\MandrillSwiftMailerBundle\AccordMandrillSwiftMailerBundle(),
            new EE\DataExporterBundle\EEDataExporterBundle(),
            new MailingBundle\MailingBundle(),
            new PayPeriodBundle\PayPeriodBundle(),
            new ExtraHoursBundle\ExtraHoursBundle(),
            new SettingsBundle\SettingsBundle(),
            new BaseBundle\BaseBundle(),
            new BonusBundle\BonusBundle(),
            new TrainingBundle\TrainingBundle(),
            new TransportBundle\TransportBundle(),
            new BenefitBundle\BenefitBundle(),
            new RemunerationBundle\RemunerationBundle(),
            new HiringBundle\HiringBundle(),
            new Ivory\CKEditorBundle\IvoryCKEditorBundle(),
            new FOS\OAuthServerBundle\FOSOAuthServerBundle(),
            new FOSOAuth2ServerBundle\FOSOAuth2ServerBundle(),
            new RemunerationPropBundle\RemunerationPropBundle(),
        );

        if (in_array($this->getEnvironment(), array('dev', 'test'), true)) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
            $bundles[] = new Hautelook\AliceBundle\HautelookAliceBundle();
        }

        return $bundles;
    }

    public function registerContainerConfiguration(LoaderInterface $loader) {
        $loader->load($this->getRootDir() . '/config/config_' . $this->getEnvironment() . '.yml');
    }

}
