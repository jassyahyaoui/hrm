#features/expense/other/create_validate.feature
Feature: Create and validate an expense 
  Create and validate an expense
  @javascript
  Scenario: creation/validation d'un objet par un collaborateur valide par un responsable 
    #0 - Se connecter en mode Collaborateur
    Given I am logged using "collaborateur" and "collaborateur"
    And I wait for ajax
    #And I save a screenshot to "shot.png"
    And I select the company of "collaborateur"
    When I delete all "expenses"
    When I maximaze window   
    Then I should be on "/dashboard/"
    And I wait for ajax
    And I wait for ajax

    #1 - Creation d'un objet
    When I click on the link "TabExpense"
    And I wait for ajax
    And I press "btn_new_expense"
    And I wait for the form to appear
 
    And I select "Autres" from "expensebundle_ik_type"
    And I select "Collaborateur Collaborateur" from "expensebundle_hrm_expenses_other_contributor"
    And I fill random value in field "expensebundle_hrm_expenses_other_displayName" 
    And I select "Responsable Responsable" from "expensebundle_hrm_expenses_other_validator"
    And I select "Amende" from "expensebundle_hrm_expenses_other_SettingsExpenses"  
    And I wait for ajax
    #And I select random from "#expensebundle_hrm_expenses_other_SettingsExpenses"
    #And I fill the form "#form_expenses_other_new" with random values
    And I wait for ajax  
    And I fill random value in field "expensebundle_hrm_expenses_other_amountOth"
    #And I fill random value in field "expensebundle_Autres_tva_28_5" 
    #And I fill random value in field "expensebundle_Autres_tva_28_20" 
    And I fill in the following "20" in field "expensebundle_Autres_tva_28_5"
    And I fill in the following "40" in field "expensebundle_Autres_tva_28_20"
    #And I fill random value in field "expensebundle_hrm_expenses_other_comment"   
    And I fill in the following "11/2017" in field "expensebundle_hrm_expenses_other_settings_pay_period"
    And I should see amount ht "#expensebundle_hrm_expenses_other_amountOth" "#expensebundle_Autres_tva_28_5" "#expensebundle_Autres_tva_28_20" "#expensebundle_hrm_expenses_other_untaxedAmountOth"
    #And I fill in the current open payperiod in field "expensebundle_hrm_expenses_other_settings_pay_period"

    #2 - Controler les champs
    #valeur par défaut
    And I should see default value of field validator "#expensebundle_ik_validator"
    And I should see default value current date of field "#expensebundle_ik_date"

    #format
    And I should see format date "d/m/Y" on field "#expensebundle_ik_date"
    And I should see format date "m/Y" on field "#expensebundle_hrm_expenses_other_settings_pay_period"

    #obligatoire
    #Then I should see all required fields of form "expensebundle_hrm_expenses_other_SettingsExpenses"

    #3 - Renseigner l'exhaustivité des champs dans le formulaire
    Then I should see all fields of form "#form_expenses_other_new" are filled

    When I scroll "SauvegarderOTHER" into view
    And I press "SauvegarderOTHER" 
    And I wait for the form to close
    And I wait for ajax

    #4 - Sauvegarder en mode Brouillon l'objet
    #Then the expense should be "Brouillon"
    #Then I should found the new expense

    #5 - Passer l'objet du statut 
    When I click to "edit" a "expense"
    And I wait for 2 seconds
    And I wait for the form to appear
    When I scroll "EditValiderOTHER" into view
    And I press "EditValiderOTHER"
    And I wait for ajax
    And I wait for ajax
    When I click to "show" a "expense"
    Then I wait for 2 seconds
    Then I should see "En attente de validation"
    And I press "btn_return_expense"
    And I wait for ajax

    #6 - Controler en mode lecture que tous les champs sont renseignes
    When I click to "show" a "expense"
    And I wait for 2 seconds
    Then I should see "Collaborateur Collaborateur"
    And I should see "En attente de validation"
    And I should see "Collaborateur Collaborateur"
    And I should see "Responsable Responsable"  
    #And I should see "49,00"
    #And I should see "2 roues 5 CV_Ma"
    And I should see "En attente"
    And I wait for 2 seconds

    #7 - Controler en mode liste la presence de l'objet dans la grille de donnees
    And I press "btn_return_expense"
    And I wait for ajax
    #And I wait for the table "expenses" to appear

    #8 - Controler les valeurs de l'objet dans la grille de donnees
    Then I should see "Collaborateur Collaborateur"
    And I should see "nom"
    And I should see "Autres"
    #And I should see "11/2017"
    #And I should see "49,00"  
    And I should see "En attente"

    #9 - Se connecter avec le responsable du collaborateur 
    When I am logged using "responsable" and "responsable"
    When I click on the link "TabExpense"
    And I wait for ajax
    And I wait for 5 seconds

    #10 - Dans le mode Liste selectionner le filtre par statut 
    #1And I press "btn_filter_statut" 
    #1And I wait for 20 seconds
    #1#1And I click on the link "btn_filter_status_waiting"
    #And I wait for 2 seconds

    And I press "btn_filter_statut" 
    And I wait for 2 seconds
    #When I click on the text "valid"
    And I click on the link "btn_filter_status_waiting" 
    And I wait for 2 seconds

    #11 - Valider la demande du collaborateur
    #confirm a expense absence
    When I click to "confirm" a "expense"
    And I wait for ajax
    And I wait for 5 seconds

    #12 - Controler que l'objet est au statut "Valide"
    When I scroll "btn_filter_statut" into view
    And I press "btn_filter_statut" 
    And I wait for 2 seconds
    And I click on the link "btn_filter_status_all" 
    And I wait for 2 seconds
    When I click to "show" a "expense"
    Then I should see "Valide"
    And I press "btn_return_expense"
    And I wait for 2 seconds