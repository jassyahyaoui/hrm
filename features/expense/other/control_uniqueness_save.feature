#features/expense/other/control_uniqueness_save.feature
Feature: CONTROLE UNICITE SAUVEGARDE
  CONTROLE UNICITE SAUVEGARDE
  @javascript
  Scenario: CONTROLE UNICITE SAUVEGARDE
    #0 - Se connecter en mode Collaborateur
    Given I am logged using "collaborateur" and "collaborateur"
    And I wait for ajax
    And I select the company of "collaborateur"

    When I delete all "expenses"
    When I maximaze window   
    Then I should be on "/dashboard/"
    And I wait for ajax

    #1 - Creation d'un objet
    When I click on the link "TabExpense"
    And I wait for ajax
    And I scroll "btn_new_expense" into view
    And I press "btn_new_expense"
    And I wait for the form to appear
    And I select "Autres" from "expensebundle_ik_type"
    And I select "Collaborateur Collaborateur" from "expensebundle_hrm_expenses_other_contributor"
    And I fill random value in field "expensebundle_hrm_expenses_other_displayName" 
    And I select "Responsable Responsable" from "expensebundle_hrm_expenses_other_validator"
    And I select "Amende" from "expensebundle_hrm_expenses_other_SettingsExpenses"  
    And I wait for ajax
    And I wait for ajax  
    And I fill random value in field "expensebundle_hrm_expenses_other_amountOth"
    And I fill in the following "20" in field "expensebundle_Autres_tva_28_5"
    And I fill in the following "40" in field "expensebundle_Autres_tva_28_20"
    And I fill random value in field "expensebundle_hrm_expenses_other_comment"   
    And I fill in the following "11/2017" in field "expensebundle_hrm_expenses_other_settings_pay_period"


    #2 - Renseigner les champs obligatoires
    Then I should see all required fields of form "expenseOther"

    #3 - Cliquer deux fois sur le bouton "Sauvegarder"
    When I scroll "SauvegarderOTHER" into view
    And I press "SauvegarderOTHER" 
    And I press "SauvegarderOTHER" 
    And I wait for the form to close
    And I wait for ajax

    #4 - Contrôle de la présence d'un seul objet
    Then I should found 1 expense created

    #5 - En mode Edition cliquer deux fois sur le bouton "Valider" 
    When I click to "edit" a "expense"
    And I wait for 2 seconds
    And I wait for the form to appear
    When I scroll "EditValiderOTHER" into view
    And I press "EditValiderOTHER"
    And I press "EditValiderOTHER"
    And I wait for the form to close
    And I wait for ajax

    #6 - Contrôle de la présence d'un seul objet
    Then I should found 1 expense created