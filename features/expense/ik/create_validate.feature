#features/expense/create_validate.feature
Feature: D.1 - FRAIS - IK 
  @javascript
  Scenario: D.1 CREATION 
    #0 - Se connecter en mode Collaborateur
    Given I am logged using "collaborateur" and "collaborateur"
    And I wait for ajax
    And I select the company of "collaborateur"
    When I delete all "expenses"
    When I maximaze window   
    Then I should be on "/dashboard/"

    #1 - Creation d'un objet
    When I click on the link "TabExpense"
    And I wait for ajax
    And I press "btn_new_expense"
    And I wait for the form to appear
    And I select "Ik" from "expensebundle_ik_type"
    And I fill random value in field "expensebundle_ik_displayName" 
    #And I fill in the following "15/10/2017" in field "expensebundle_ik_date"
    Then I fill current date in field "expensebundle_ik_date"
    #And I fill in the following "TEST EXPENSE" in field "expensebundle_ik_displayName"  
    #And I select "2 roues 5 CV_Ma" from "expensebundle_ik_settingsExpIk"
    And I select random from "#expensebundle_ik_settingsExpIk"
    And I wait for ajax
    And I fill in the following "100" in field "expensebundle_ik_kilometer"
    And I fill in the following "TEST EXPENSE" in field "expensebundle_ik_comment"
    Then I fill current month in field "expensebundle_ik_settings_pay_period"
    #And I fill in the following "10/2017" in field "expensebundle_ik_settings_pay_period"
    Then I fill current date in field "expensebundle_ik_date"

    #2 - Controler les champs
    #valeur par défaut
    And I should see default value of field validator "#expensebundle_ik_validator"

    #format
    And I should see format date "d/m/Y" on field "#expensebundle_ik_date"

    #obligatoire
    Then I should see all required fields of form "expenseIk"

    And I select "Collaborateur Collaborateur" from "expensebundle_ik[contributor]"
    And I select "Responsable Responsable" from "expensebundle_ik[validator]"

    #4 - Sauvegarder en mode Brouillon l'objet
    When I scroll "SauvegarderIK" into view
    And I press "SauvegarderIK" 
    And I wait for the form to close
    And I wait for ajax

    #5 - Passer l'objet du statut 
    When I click to "edit" expense
    And I wait for 2 seconds
    And I wait for the form to appear
    When I scroll "EditValiderIK" into view
    And I press "EditValiderIK" 
    And I wait for ajax
    When I click to "show" a "expense"
    Then I wait for 2 seconds
    Then I should see "En attente de validation"
    And I press "btn_return_expense"
    And I wait for ajax

    #6 - Controler en mode lecture que tous les champs sont renseignes
    When I click to "show" a "expense"
    And I wait for 2 seconds
    Then I should see "Collaborateur Collaborateur"
    And I should see "En attente de validation"
    And I should see "Collaborateur Collaborateur"
    And I should see "Responsable Responsable"  
    #And I should see "49,00"
    #And I should see "2 roues 5 CV_Ma"
    And I should see "En attente"
    And I wait for 2 seconds

    #7 - Controler en mode liste la presence de l'objet dans la grille de donnees
    And I press "btn_return_expense"
    And I wait for ajax
    #And I wait for the table to appear

    #8 - Controler les valeurs de l'objet dans la grille de donnees
    Then I should see "Collaborateur Collaborateur"
    And I should see "nom"
    And I should see "Ik"
    #And I should see "10/2017"
    And I should see current month
    #And I should see "49,00"  
    And I should see "En attente"

    #9 - Se connecter avec le responsable du collaborateur 
    When I am logged using "responsable" and "responsable"
    When I click on the link "TabExpense"
    And I wait for ajax
    And I wait for 5 seconds


    #10 - Dans le mode Liste selectionner le filtre par statut 
    #1And I press "btn_filter_statut" 
    #1And I wait for 20 seconds
    #1#1And I click on the link "btn_filter_status_waiting"
    #And I wait for 2 seconds

    And I press "btn_filter_statut" 
    And I wait for 2 seconds
    #When I click on the text "valid"
    And I click on the link "btn_filter_status_waiting" 
    And I wait for 2 seconds

    #11 - Valider la demande du collaborateur
    #confirm a expense absence
    When I click to "confirm" a "expense"
    And I wait for ajax
    And I wait for 5 seconds

    #12 - Controler que l'objet est au statut "Valide"
    And I press "btn_filter_statut" 
    And I wait for 2 seconds
    And I click on the link "btn_filter_status_all" 
    And I wait for 2 seconds
    When I click to "show" a expense"
    Then I should see "Valide"
    And I press "btn_return_expense"
    And I wait for 2 seconds