#features/expense/control_uniqueness_save.feature
Feature: Controle unicite sauvegarde
  Controle unicite sauvegarde
  @javascript
 Scenario: Controle unicite sauvegarde 
    #0 - Se connecter en mode Collaborateur
    Given I am logged using "collaborateur" and "collaborateur"
    When I delete all "expenses"
    And I maximaze window   
    And I wait for ajax
    And I select the company of "collaborateur"
    Then I should be on "/dashboard/"

    #1 - Création d'un objet
    When I click on the link "TabExpense"
    And I wait for ajax

    And I press "btn_new_expense"
    Then I wait for the form to appear

    #2 - Contrôler les champs
    Then the "Collaborateur Collaborateur" option from "expensebundle_ik[contributor]" should be selected
    And I should see default value of field validator "#expensebundle_ik_validator"

    And I fill random value in field "expensebundle_ik_displayName" 
    And I select random from "#expensebundle_ik_settingsExpIk"
    And I select random from "#expensebundle_ik_settingsExpIk"
    And I fill in the following 100" in field "expensebundle_ik_kilometer"
    #And I select "Temps de repos" from "expensebundle_ik_settingsExpIk"
    #And I fill in the following "10/2017" in field "expensebundle_ik_settings_pay_period"

    Then I fill current month in field "expensebundle_ik_settings_pay_period"

    And I fill in the following "18/10/2017" in field "expensebundle_ik_date"
    And I fill in the following "TEST EXPENSE" in field "expensebundle_ik_comment"
    
    #3 - Cliquer deux fois sur le bouton "Sauvegarder" 
    When I scroll "SauvegarderIK" into view
    And I press "SauvegarderIK" 
    And I press "SauvegarderIK" 

    #4 - Contrôle de la présence d'un seul objet
    When I wait for ajax
    And I wait for 2 seconds
    Then I should found 1 expense created

    #5 - En mode Edition cliquer deux fois sur le bouton "Valider"
    When I click to "edit" a "expense"
    And I wait for 2 seconds
    And I wait for the form to appear
    When I scroll "EditValiderIK" into view
    And I press "EditValiderIK"
    And I press "EditValiderIK" 
    And I wait for ajax
    Then I should found 1 expense created