<?php

use Behat\MinkExtension\Context\MinkContext;

class ExpenseContext extends MinkContext
{

    /**
     * @When I wait for :arg1 s
     */
    public function iWaitForS($arg1)
    {
        $this->getSession()->wait($arg1 * 3000);
    }

    /**
     * @Then a popup should appear
     */
    public function aPopupShouldAppear()
    {
        $this->getSession()->wait(2000, "$('.modal').hasClass('in')");
    }

    /**
     * @Then /^(?:|I )click (?:on |)(?:|the )"([^"]*)"(?:|.*)$/
     */
    public
    function iClickOn($arg1)
    {
        $findName = $this->getSession()->getPage()->find("css", $arg1);
        if (!$findName) {
            throw new Exception($arg1 . " could not be found");
        } else {
            $findName->click();
        }
    }

    /**
     * Click some text
     *
     * @When /^I click on the text "([^"]*)"$/
     */
    public function iClickOnTheText($text)
    {
        $session = $this->getSession();
        $element = $session->getPage()->find(
            'xpath',
            $session->getSelectorsHandler()->selectorToXpath('xpath', '*//*[text()="' . $text . '"]')
        );
        if (null === $element) {
            throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $text));
        }

        $element->click();

    }

    /**
     * @When /^I click li option "([^"]*)"$/
     *
     * @param $text
     * @throws \InvalidArgumentException
     */
    public function iClickLiOption($text)
    {
        $session = $this->getSession();
        $element = $session->getPage()->find(
            'xpath',
            $session->getSelectorsHandler()->selectorToXpath('xpath', '*//*[text()="' . $text . '"]')
        );

        if (null === $element) {
            throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $text));
        }

        $element->click();
    }
    /**
     * @When /^I click an element with ID "([^"]*)"$/
     *
     * @param $id
     * @throws \Exception
     */
    public function iClickAnElementWithId($id)
    {
        $element = $this->getSession()->getPage()->findById($id);
        if (null === $element) {
            throw new \Exception(sprintf('Could not evaluate element with ID: "%s"', $id));
        }
        $element->click();
    }
}