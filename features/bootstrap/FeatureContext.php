<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Behat\Context\TranslatableContext;
use Behat\Gherkin\Node\TableNode;
use Behat\Behat\Hook\Scope\BeforeScenarioScope;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\SecurityContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use BonusBundle\Entity\hrm_bonus;
use Behat\Mink\Driver\GoutteDriver;
use Behat\Mink\Driver\BrowserKitDriver;
use Symfony\Component\BrowserKit\Client;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends DefaultContext implements Context, SnippetAcceptingContext, KernelAwareContext {

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    private $randomValue;
    private $currentUser;
    private $session;
    private $tokenStorage;

    public function __construct($session, TokenStorage $tokenStorage, $mySymfonyParam) {
        $this->randomValue = strval(rand());
        $this->session = $session;
        $this->tokenStorage = $tokenStorage;
    }

    public function getRand() {
        return $this->randomValue;
    }

    public function getRandEmail() {
        return $this->randomValue . "@fabereo.com";
    }

    /**
     * @When I set current user
     */
    public function setcurrentUser($user) {
        $this->currentUser = $user;
    }

    /**
     * @When I set random amount
     */
    public function setRand() {
        $this->randomValue = strval(rand());
        echo "random value: " . $this->randomValue;
    }

    /** @var \Behat\MinkExtension\Context\MinkContext */
    private $minkContext;

    /** @BeforeScenario */
    public function gatherContexts(BeforeScenarioScope $scope) {
        $environment = $scope->getEnvironment();
        $this->minkContext = $environment->getContext('Behat\MinkExtension\Context\MinkContext');
    }

    /**
     * @When I click on the link :arg1
     */
    public function iClickOnTheLink($link) {
        $this->clickLink($link);
    }

    /**
     * @When I fill in the following :arg1 in field :arg2
     */
    public function iFillInTheFollowing($value, $key) {
        $this->fillField($key, $value);
    }

    /** @BeforeStep @javascript */
    public function beforeStep($event) {
        $waitTime = 10000;
        $jqDefined = "return (typeof jQuery != 'undefined')";
        $active = "(0 === jQuery.active && 0 === jQuery('animated').length)";
        if ($this->getSession()->evaluateScript($jqDefined)) {
            $this->getSession()->wait($waitTime, $active);
        }
    }

    /**
     * @When I wait for ajax
     */
    public function iWaitForAjax() {
        $waitTime = 10000;
        try {
            //Wait for Angular

            $angularIsNotUndefined = $this->getSession()->evaluateScript("return (typeof angular != 'undefined')");
            if ($angularIsNotUndefined) {
                //If you run the below code on a page ending in #, the page reloads.
                if (substr($this->getSession()->getCurrentUrl(), -1) !== '#') {
                    $angular = 'angular.getTestability(document.body).whenStable(function() {
                window.__testable = true;
            })';
                    $this->getSession()->evaluateScript($angular);
                    $this->getSession()->wait($waitTime, 'window.__testable == true');
                }

                /*
                 * Angular JS AJAX can't be detected overall like in jQuery,
                 * but we can check if any of the html elements are marked as showing up when ajax is running,
                 * then wait for them to disappear.
                 */
                $ajaxRunningXPath = "//*[@ng-if='ajax_running']";
                //$this->waitForElementToDisappear($ajaxRunningXPath, $waitTime);
            }

            //Wait for jQuery
            if ($this->getSession()->evaluateScript("return (typeof jQuery != 'undefined')")) {
                $this->getSession()->wait($waitTime, '(0 === jQuery.active && 0 === jQuery(\':animated\').length)');
            }
        } catch (Exception $e) {
            //var_dump($e->getMessage()); //Debug here.
        }
        sleep(10);
    }

    /**
     * @Then /^I wait for the form to appear$/
     */
    public function iWaitForTheFormToAppear() {
        $this->getSession()->wait(5000, "$('.modal').hasClass('in')");
    }

    /**
     * @Then /^I wait for the form to close$/
     */
    public function iWaitForTheFormToClose() {
        $this->getSession()->wait(5000, "!$('.modal').hasClass('in')");
    }

    /**
     * @Then I check if logout
     */
    public function iCheckIfLogout() {
        $row = $this->getSession()->wait(0, "$('.home.page').hasClass('page-template-default')");
        if (!$row)
            throw new Exception('Cannot Logout!');
        else
            echo 'Logout!';
    }

    /**
     * @Then /^(?:|I )should see a? :arg element$/
     */
    public function iShouldSee($element) {
        $this->assertSession()->elementExists('css', $element);
    }

    /**
     * @Then I am loged
     */
    public function iAmLoged() {
        $user = $this->getContainer()->get('user')->getCurrentUser();
        if ($user)
            echo "You are logged as " . $user->getUsername();
        else
            echo "Error ! please log in";
    }

    /**
     * @Then /^I check if the form appear$/
     */
    public function iCheckIfTheFormAppear() {
        #$this->getSession()->wait(5000, "$('.modal').hasClass('in')");
        #$rowText = "10/08/2017";
        $row = $this->getSession()->wait(1000, "$('.modal').hasClass('in')");
        if (!$row)
            throw new Exception('Cannot find open modal!');
        else
            echo 'I can see the modal open!';
    }

    /**
     * @Then I should see the request on table :value
     */
    public function iShouldSeeTheRequestOnTable($rowText) {
        #$rowText = "10/08/2017";
        $row = $this->getSession()->getPage()->find('css', sprintf('table tr:contains("%s")', $rowText));
        if (!$row)
            throw new Exception('Cannot find a table row with this text!');
        else
            echo 'I can see the request on table!';
    }

    /**
     * @Then I wait
     */
    public function iWait() {
        sleep(10);
    }

    /**
     * @Then I wait for $seconds seconds
     */
    public function iWaitFor($seconds) {
        sleep($seconds);
    }

    /**
     *  @When I fill random value in field :arg1
     */
    public function iFillRandomValueInField($arg1) {
        $random = $this->getRand();
        if ($random) {
            $this->getSession()->getPage()->fillField($this->fixStepArgument($arg1), $this->fixStepArgument($random));
            echo "The value of . $arg1 . is : " . $random . "\xA";
        } else
            echo "No random value " . "\xA";
    }

    /**
     *  @When I fill random email in field :arg1
     */
    public function iFillRandomEmailInField($arg1) {
        $random = $this->getRand() . "@fabereo.com";
        if ($random) {
            $this->getSession()->getPage()->fillField($this->fixStepArgument($arg1), $this->fixStepArgument($random));
            echo "The value of . $arg1 . is : " . $random . "\xA";
        } else
            echo "No random value" . "\xA";
    }

    /**
     * @Transform /^(\d+)$/
     */
    public function castStringToNumber($string) {
        return intval($string);
    }

    /**
     * @When I should found :number of :object
     */
    public function iShouldFoundOf($object, $number) {
        echo "Contrôle de la présence de " . $number . " " . $object . "\xA";
        switch ($object) {
            case "absences":
                $founds = Count($this->getContainer()->get('requestAbsenceBundle.service')->findRequestAbsence());
                break;
            case "expenses":
                $founds = Count($this->getContainer()->get('ExpenseBundle.service')->findListExpensesWithName());
                break;
            case "bonus":
                $founds = Count($this->getContainer()->get('BonusBundle.service')->findListBonusWithAmount());
                break;
            case "extrahours":
                $founds = Count($this->getContainer()->get('ExtraHoursBundle.service')->findListExtraHours());
                break;
            case "transport":
                $founds = Count($this->getContainer()->get('TransportBundle.service')->findListTransport($this->getRand()));
                break;
            case "benefit":
                $founds = Count($this->getContainer()->get('BenefitBundle.service')->findListBenefit($this->getRand()));
                break;
        }
        if ($number == $founds)
            echo "Check for the presence of a single object OK " . $number . " " . $object . "\xA";
        else
            throw new Exception("Check for the presence of a single object FAIL ! expect " . $number . " but " . $founds . " created" . "\xA");
    }

    /**
     * @Then I should found :arg1 expense created
     */
    public function iShouldFoundExpenseCreated($arg1) {
        $name = $this->getRand();
        $expenses = $this->getContainer()->get('ExpenseBundle.service')->findListExpensesWithName($name);
        if ($expenses) {
            foreach ($expenses as $expense) {
                echo "Expene id : " . $expense->getUid() . "\xA";
            }
            echo "Found " . count($expenses) . " expense in database" . "\xA";
            if (count($expenses) > $arg1)
                throw new Exception("Check for the presence of a single object Error." . count($expenses) . " Found !");
        } else
            echo "No expense found in the database" . "\xA";
    }

    /**
     * @Then I should found bonus with amount and bonus type :arg1
     */
    public function iShouldFoundBonusWithAmountAndBonusType($arg1) {
        $user = $this->getContainer()->get('user.service')->findCurrentEmployee("collaborateur");
        $bonus = $this->getContainer()
                ->get('bonus.service')
                ->findBonus($user->getId(), $arg1);
        if ($bonus)
            echo "The new bonus " . $bonus->getAmount() . "  has been found in database";
        else
            echo "The new bonus does not exist in database";
    }

    /**
     * @When I change :email with :newemail
     */
    public function iChangeWith($email, $newemail) {
        $user = $this->getContainer()
                ->get('user')
                ->findEmployee($email);
        if ($user) {
            $this->getPage()->fillField('edit_employee_email', $newemail);
            $this->getPage()->pressButton('save_collab_up');
            $upadteduser = $this->getContainer()
                    ->get('user')
                    ->findEmployee($newemail);
            if ($upadteduser)
                if ($user->getId() == $upadteduser->getId())
                    echo 'user updated';
                else
                    throw new Exception("User not updated");
        }
    }

    /**
     * @Then I delete collaborater :arg
     */
    public function iDeleteCollaborater($email) {
        $user = $this->getContainer()->get('user')->findEmployee($email);
        $this->getContainer()->get('user')->deleteEmployee($email);
    }

    /**
     * @Then I should see request absence is waiting :request
     */
    public function iShouldSeeRequestAbsenceIsWaiting($request) {
        echo "Current URL: " . $this->getSession()->getCurrentUrl() . "\n";
        $nbrequestabsence = $this->getContainer()
                ->get('requestAbsenceBundle.service')
                ->findRequestAbsence("TEST_REQUEST_ABSENCE");
        echo "The Request Absence is " . $nbrequestabsence->getStatus->getDisplayName();
    }

    /**
     * @When I scroll :elementId into view
     */
    public function scrollIntoView($elementId) {
        $function = <<<JS
(function(){
  var elem = document.getElementById("$elementId");
  elem.scrollIntoView(false);
})()
JS;
        try {
            $this->getSession()->executeScript($function);
        } catch (Exception $e) {
            throw new \Exception("ScrollIntoView failed");
        }
    }

    /**
     * @return \Behat\Mink\Element\DocumentElement
     */
    private function getPage() {
        return $this->getSession()->getPage();
    }

    /**
     * @Given I am logged as responsable
     * */
    public function iAmLoggedResponable() {

        $this->visitPath('/login');
        $this->getPage()->fillField('_username', 'responsable');
        $this->getPage()->fillField('_password', 'responsable');
        $this->getPage()->pressButton('_submit');



        die();
    }

    /**
     * @Given I am logged as collaborater
     * */
    public function iAmLoggedCollaborater() {

        $this->visitPath('/login');
        $this->getPage()->fillField('_username', 'collaborateur');
        $this->getPage()->fillField('_password', 'collaborateur');
        $this->getPage()->pressButton('_submit');
    }

    /**
     * @When I maximaze window
     * */
    public function iMaximazeWindow() {
        $this->getSession()->getDriver()->maximizeWindow();
    }

    /**
     * @When I should see default value of collaborator in field :arg1
     */
    public function iShouldSeeDefaultValueOfCollaboratorInField($arg1) {
        $defaultValue = $this->currentUser;
        echo "defaultValue : " . $defaultValue . "\xA";
        $selectField = $this->getSession()->getPage()->find("css", $arg1);
        if ($selectField) {
            $options = $selectField->findAll('css', 'option');
            foreach ($options as $value) {
                if ($value->isSelected()) {
                    if (strcasecmp($value->getText(), $defaultValue) == 0)
                        echo 'Default value of field collaborator is ' . $defaultValue . '\n';
                    else
                        throw new Exception('Default id of collaborator shoudl be "' . $defaultValue . '" but "' . $value->getText() . '" found \n');
                }
            }
        } else {
            echo "pas de field trouvé !" . "\xA";
        }
    }

    /**
     * @Then I should see default value :value of field :arg2
     */
    public function iShouldSeeDefaultValueOfField($defaultValue, $idField) {
        $field = $this->getSession()->getPage()->find("css", $idField);
        if ($field) {
            if (strcasecmp($field->getText(), $defaultValue) == 0)
                echo 'Default value of field "' . $idField . '" is ' . $defaultValue . '\n';
            else
                throw new Exception('Default id of "' . $idField . '" shoudl be "' . $defaultValue . '" but "' . $field->getText() . '" found \n');
        } else {
            throw new Exception('Cannot find a field with this id! ' . $idField);
        }
    }

    /**
     * @Then I should see default value of field requester :arg2
     */
    public function iShouldSeeDefaultValueOfFieldRequester($idField) {
        echo "- Fill in the completeness of fields in the form \n";
        $exhaustiviteChamps = true;
        $user = $this->getContainer()->get('user.service')->findCurrentEmployee("collaborateur");
        $defaultValue = $user->getUsername();
        $field = $this->getSession()->getPage()->find("css", $idField);
        if ($field) {
            if (strcasecmp($field->getValue(), $defaultValue) == 0)
                echo 'Default value of field "' . $idField . '" is ' . $defaultValue . '\n';
            else
                throw new Exception('Default value "' . $idField . '" shoudl be "' . $defaultValue . '" but "' . $field->getValue() . '" found \n');
            echo 'I can see default value of field "' . $idField . '" -> ' . $field->getValue() . "\n";
        } else {
            $exhaustiviteChamps = true;
            throw new Exception('Cannot find a field with this text! ' . $idField);
        }
    }

    /**
     * 
     * @Then I should see default value of field pay period :arg2
     */
    public function iShouldSeeDefaultValueOfFieldPayperiod($idField) {
        echo "- Fill in the completeness of fields in the form \n";
        $exhaustiviteChamps = true;
        $user = $this->getContainer()->get('user.service')->findCurrentEmployee("collaborateur");
        $payperiodlist = $this->getContainer()->get('payperiod.service')->findPayPeriod($user);
        $field = $this->getSession()->getPage()->find("css", $idField);
        if (($field) && ($payperiodlist)) {
            if (count($payperiodlist) == 2) {
                echo $payperiodlist[1]->getStartDate()->format('m/Y');
                if ($payperiodlist[1]->getStartDate()->format('m/Y') == $field->getValue()) {
                    echo 'Default value of field "' . $idField . '" is ' . $payperiodlist[1]->getStartDate()->format('m/Y') . '\n';
                } else if ($payperiodlist[0]->getStartDate()->format('m/Y') == $field->getValue()) {
                    echo 'Default value of field "' . $idField . '" is ' . $payperiodlist[0]->getStartDate()->format('m/Y') . '\n';
                } else {
                    throw new Exception('Default value "' . $idField . '" shoudl be "null" but "' . $field->getValue() . '" found \n');
                    echo 'I can see default value of field "' . $idField . '" -> ' . $field->getValue() . "\n";
                }
            } else if (count($payperiodlist) == 1 && ($payperiodlist[0]->getStartDate()->format('m/Y') == $field->getValue())) {
                echo 'Default value of field "' . $idField . '" is ' . $payperiodlist[0]->getStartDate()->format('m/Y') . '\n';
            } else {
                throw new Exception('Default value "' . $idField . '" shoudl be "null" but "' . $field->getValue() . '" found \n');
            }
            echo 'I can see default value of field "' . $idField . '" -> ' . $field->getValue() . "\n";
        } else {
            $exhaustiviteChamps = true;
            throw new Exception('Cannot find a field with this text! ' . $idField);
        }
    }

    /**
     * @Then I should see default value of field date :arg1
     */
    public function iShouldSeeDefaultValueOfFieldDate($idField) {
        echo "- Fill in the completeness of fields in the form \n";
        $exhaustiviteChamps = true;
        $defaultValue = date("d/m/Y");
        $field = $this->getSession()->getPage()->find("css", $idField);
        if ($field) {
            if (strcasecmp(trim($field->getValue()), $defaultValue) == 0)
                echo 'Default value of field "' . $idField . '" is ' . $defaultValue . '\n';
            else
                throw new Exception('Default value "' . $idField . '" shoudl be "' . $defaultValue . '" but "' . $field->getValue() . '" found \n');
            echo 'I can see default value of field "' . $idField . '" -> ' . $field->getValue() . "\n";
        } else {
            $exhaustiviteChamps = true;
            throw new Exception('Cannot find a field with this text! ' . $idField);
        }
    }

    /**
     * @When I press option list for bonus added :arg1
     */
    public function iPressOptionListForBonusAdded($arg1) {
        $amount = $this->getContainer()->get('bonus.service')->getRandomAmount();
        $user = $this->getContainer()->get('user.service')->findCurrentEmployee("collaborateur");
        $bonus = $this->getContainer()
                ->get('bonus.service')
                ->findBonus($user->getId(), $arg1);
        $id_bonus = $bonus->getId();
        $this->getPage()->pressButton('btn_option_bonus_' . $id_bonus);
    }

    /**
     * @When I fill in date with :date
     */
    public function iFillInDateWith($date) {
        $this->getSession()->getPage()->find('named', array('id_or_name', 'requestabsencebundle_requestabsence_startdate'))->setValue($date);
    }

    /**
     * @When I click to :action last request absence
     */
    public function iClickToLastRequestAbsence($action) {
        $requestabsence = $this->getContainer()->get('requestAbsenceBundle.service')->getLastRequestAbsence();
        if ($requestabsence)
            $id = (string) $requestabsence->getId();
        else
            echo "Can't found requestabsence" . "\xA";
        $button_name = "#BUTTON_REQUEST_ABSENCE" . $id;
        $button = $this->getSession()->getPage()->find("css", $button_name);
        if (null === $button) {
            throw new \Exception('The button is not found ' . $button_name);
        }
        $button->click();
        sleep(3);
        $button_action_name = "";
        switch ($action) {
            case "show":
                $button_action_name = "#show_request_absence_" . $id;
                break;
            case "edit":
                $button_action_name = "#edit_request_absence_" . $id;
                break;
            case "cancel":
                $button_action_name = "#cancel_request_absence_" . $id;
                break;
            case "validate":
                $button_action_name = "#validate_request_absence_" . $id;
                break;
            case "reject":
                $button_action_name = "#reject_request_absence_" . $id;
                break;
            case "remove":
                $button_action_name = "#remove_request_absence_" . $id;
                break;
        }
        $button_action = $this->getSession()->getPage()->find("css", $button_action_name);
        if ($button_action)
            $button_action->click();
        else
            throw new Exception('the button "' . $button_action_name . '" does not exist in the page');
        sleep(3);
    }

    /**
     * @When I click to :action a :object
     */
    public function iClickToA($action, $object) {
        $random = $this->getRand();
        sleep(3);
        $button_action_name = "";
        $button_name = "";
        echo $action . " " . $object . "\xA";
        switch ($object) {
            case "contract":
                $entity = $this->getContainer()->get('user.service')->findEmployeeByUsername($random);
                $id = $entity->getId();
                $button_name = "#btn_collaborator_" . $id;
                break;
            case "employe":
                $entity = $this->getContainer()->get('user.service')->findEmployeeByUsername($random);
                $id = $entity->getId();
                $button_name = "#btn_collaborator_" . $id;
                break;
            case "expense":
                $entity = $this->getContainer()->get('ExpenseBundle.service')->findExpense($random);
                $id = $entity->getUid();
                $button_name = "#btn_expense_" . $id;
                break;
            case "extrahour":
                $entity = $this->getContainer()->get('ExtraHoursBundle.service')->findExtraHours($random);
                $id = $entity->getId();
                $button_name = "#btn_extrahour_" . $id;
                break;
            case "bonus":
                $entity = $this->getContainer()->get('BonusBundle.service')->findBonus($random);
                $id = $entity->getId();
                $button_name = "#btn_bonus_" . $id;
                break;
            case "transport":
                $entity = $this->getContainer()->get('TransportBundle.service')->findTransport($random);
                $id = $entity->getId();
                $button_name = "#btn_transport_" . $id;
                break;
            case "absence":
                $entity = $this->getContainer()->get('requestAbsenceBundle.service')->findRequestAbsence($random);
                $id = $entity->getId();
                $button_name = "#btn_request_absence_" . $id;
                break;
            case "paydayvariations":
                $entity = $this->getContainer()->get('PaydayVariationsBundle.service')->findPaydayVariations($random);
                if ($entity) {
                    $id = $entity->getId();
                    $button_name = "#btn_paydayvariations_" . $id;
                }
                break;
            case "benefit":
                $entity = $this->getContainer()->get('BenefitBundle.service')->findBenefit($random);
                if ($entity)
                    $id = $entity->getId();
                $button_name = "#btn_benefit_" . $id;
                break;
        }
        $button = $this->getSession()->getPage()->find("css", $button_name);
        echo $button_name . "\xA";
        if (null === $button) {
            throw new \Exception('The button is not found ' . $button_name . "\xA");
        }
        $button->click();
        switch ($action) {
            case "show":
                $button_action_name = "#show_" . $object . "_" . $id;
                break;
            case "edit":
                $button_action_name = "#edit_" . $object . "_" . $id;
                break;
            case "cancel":
                $button_action_name = "#cancel_" . $object . "_" . $id;
                break;
            case "confirm":
                $button_action_name = "#confirm_" . $object . "_" . $id;
                break;
            case "reject":
                $button_action_name = "#reject_" . $object . "_" . $id;
                break;
            case "remove":
                $button_action_name = "#remove_" . $object . "_" . $id;
                break;
        }
        $button_action = $this->getSession()->getPage()->find("css", $button_action_name);
        if ($button_action)
            $button_action->click();
        else
            throw new Exception('the button "' . $button_action_name . '" does not exist in the page');
        sleep(3);
    }

    /**
     * @When I click to :action expense
     */
    public function iClickToExpense($action) {
        $random = $this->getRand();
        $expense = $this->getContainer()->get('ExpenseBundle.service')->findExpense($random);
        if ($expense)
            echo "The new expense " . $expense->getAmountIk() . "  has been found in database";
        else
            throw new \Exception("The button " . $action . " expense was not found ");
        $id = $expense->getUid();
        $button_name = "#btn_expense_" . $id;
        $button = $this->getSession()->getPage()->find("css", $button_name);
        if (null === $button) {
            throw new \Exception('The button is not found ' . $button_name);
        }
        $button->click();
        sleep(3);
        $button_action_name = "";
        switch ($action) {
            case "show":
                $button_action_name = "#show_expense_" . $id;
                break;
            case "edit":
                $button_action_name = "#edit_expense_" . $id;
                break;
            case "cancel":
                $button_action_name = "#cancel_expense_" . $id;
                break;
            case "validate":
                $button_action_name = "#validate_expense_" . $id;
                break;
            case "reject":
                $button_action_name = "#reject_expense_" . $id;
                break;
            case "remove":
                $button_action_name = "#remove_expense_" . $id;
                break;
        }
        $button_action = $this->getSession()->getPage()->find("css", $button_action_name);
        if ($button_action)
            $button_action->click();
        else
            throw new Exception('the button "' . $button_action_name . '" does not exist in the page');
        sleep(3);
    }

    /**
     * @Given I am logged using :username and :password
     * */
    public function iAmLoggedUsing($username, $password) {
        $this->visitPath('/login');
        $this->getSession()->getPage()->fillField('_username', $username);
        $this->getSession()->getPage()->fillField('_password', $password);
        $this->getSession()->getPage()->pressButton('_submit');
    }

    /**
     * @When I logout
     * */
    public function iLogOut() {
//        $menu = $this->getSession()->getPage()->findLink("menu_actions");
//        $menu->click();
//        sleep(3);
//        $logout = $this->getSession()->getPage()->findLink("menu_logout");
//        $logout->click();

        $menu = $this->getSession()->visit('login');
    }

    /**
     * @Then I wait for the table :arg1 to appear
     */
    public function iWaitForTheTableToAppear2($arg1) {
        switch ($arg1) {
            case "absences":
                $this->getSession()->wait(5000, '(0 === $("#table-absence").length)');
                $this->getSession()->executeScript("angular.element($('#absenceCtrl')).scope().datatable();");
                break;
            case "expenses":
                $this->getSession()->wait(5000, '(0 === $("#table-expenses").length)');
                $this->getSession()->executeScript("angular.element($('#expenseCtrl')).scope().datatable();");
                break;
            case "bonus":
                $this->getSession()->wait(5000, '(0 === $("#table-bonus").length)');
                $this->getSession()->executeScript("angular.element($('#bonusCtrl')).scope().datatable();");
                break;
            case "transport":
                $this->getSession()->wait(5000, '(0 === $("#table-transport").length)');
                $this->getSession()->executeScript("angular.element($('#transportCtrl')).scope().datatable();");
                break;
            case "paydayvariations":
                $this->getSession()->wait(5000, '(0 === $("#table-paydayvariations").length)');
                $this->getSession()->executeScript("angular.element($('#paydayvariationsCtrl')).scope().datatable();");
                break;
            case "benefit":
                $this->getSession()->wait(5000, '(0 === $("#table-benefit").length)');
                $this->getSession()->executeScript("angular.element($('#benefitCtrl')).scope().datatable();");
                break;
        }
    }

    /**
     * @Then I wait for :seconds seconds
     */
    public function iWaitForSeconds($seconds) {
        sleep($seconds);
    }

    /**
     * @When I delete all :object
     */
    public function iDeleteAll($object) {
        echo "Lorsque je supprime toutes les " . $object . "\xA";
        switch ($object) {
            case "absences":
                $this->getContainer()->get('requestAbsenceBundle.service')->deleteAllRequestAbsence();
                break;
            case "expenses":
                $this->getContainer()->get('ExpenseBundle.service')->deleteAllExpenses();
                break;
            case "bonus":
                $this->getContainer()->get('BonusBundle.service')->deleteAllBonus();
                break;
            case "extrahours":
                $this->getContainer()->get('ExtraHoursBundle.service')->deleteAllextraHours();
                break;
            case "transport":
                $this->getContainer()->get('TransportBundle.service')->deleteAllTransport();
                break;
            case "paydayvariations":
                $this->getContainer()->get('PaydayVariationsBundle.service')->deleteAllPaydayVariations();
                break;
            case "benefit":
                $this->getContainer()->get('BenefitBundle.service')->deleteAllBenefit();
                break;
        }
    }

    /**
     * @When last request absence
     */
    public function lastRequestsAbsence() {
        $employeeId = 59;
        $companyId = 39;
        $requestabsence = $this->getContainer()
                ->get('requestAbsenceBundle.service')
                ->getLastRequestAbsence($employeeId, $companyId);
        if ($requestabsence)
            echo "Last Request Absence Id is " . $requestabsence->getId();
        else
            echo "Last request absence does not exist ! The database is empty.";
    }

    /**
     * @Then the request absence should be :status
     */
    public function TheRequestsAbsenceShouldBe($status) {
        $employeeId = 59;
        $companyId = 39;
        $requestabsence = $this->getContainer()->get('requestAbsenceBundle.service')->getLastRequestAbsence($employeeId, $companyId);
        $list_requestsabsence = $this->getContainer()->get('requestAbsenceBundle.service')->getRequestAbsence();
        foreach ($list_requestsabsence as $value) {
            $value->getStatus()->getId();
        }
//       $requestabsence = $list_requestsabsence
        if (!$requestabsence) {
//            throw new Exception('Request does not exist');
            echo "can't find any requestabsence";
        } else {
            $request_status = $requestabsence->getStatus()->getDisplayName();
            if (strcasecmp($status, $request_status) != 0) {
                throw new Exception('Request shoudl be "' . $status . '" but status "' . $request_status . '" found');
            }
            echo "The Request Absence is " . $status;
        }
    }

    /**
     * @Then I should have :nbRequests requests absence
     */
    public function iShouldHaveRequestsAbsence($nbRequests) {
        $nbrequestabsence = $this->getContainer()
                ->get('requestAbsenceBundle.service')
                ->getNbrRequestAbsence();
        echo "Found " . $nbrequestabsence . " in the database";
    }

    /**
     * @Then /^(?:|I )click (?:on |)(?:|the )"([^"]*)"(?:|.*)$/
     */
    public function iClickOn($arg1) {
        $findName = $this->getSession()->getPage()->find("css", $arg1);
        if (!$findName) {
            throw new Exception($arg1 . " could not be found");
        } else {
            $findName->click();
        }
    }

    /**
     * @Then I click on  :arg1
     */
    public function iClickOn2($arg1) {
        $findName = $this->getSession()->getPage()->findLink($arg1);
        if (!$findName) {
            throw new Exception($arg1 . " could not be found");
        } else {
            $findName->click();
        }
    }

    /**
     * @Then I should see the request absence in table
     */
    public function iShouldSeeTheRequestAbsenceInTable() {
        $rowText = "29/09/2017";
        $row = $this->getSession()->getPage()->find('css', sprintf('table tr:contains("%s")', $rowText));
//        $this->assert(!$row, sprintf('Cannot find a table row with this text!'));
        if (!$row)
            throw new Exception('Cannot find a table row with this text!');
        else
            echo 'I can see the request in table!';
    }

    /**
     * @Then I should see default values
     */
    public function iShouldSeeDefaultValues() {
        $exhaustiviteChamps = true;
        $defaultValue = date('d/m/Y');
        $idField = "#requestabsencebundle_requestabsence_requestdate";
        $field = $this->getSession()->getPage()->find("css", $idField);
        if ($field) {
            if (strcasecmp($field->getValue(), $defaultValue) == 0)
                echo 'Default value "Request date" is ' . $defaultValue . "\n";
            else
                throw new Exception('Default value "Request date" shoudl be "' . $defaultValue . '" but "' . $field->getValue() . '" found');
        } else {
            $exhaustiviteChamps = false;
            throw new Exception('Cannot find a field with this text ! ' . $idField . "\n");
        }
    }

    /**
     * @Then I save as draft
     */
    public function iSaveAsDraft() {
        echo "  4 - Sauvegarder en mode Brouillon l'objet";
    }

    /**
     * Checks, that option from select with specified id|name|label|value is selected.
     *
     * @Then /^the "(?P<option>(?:[^"]|\\")*)" option from "(?P<select>(?:[^"]|\\")*)" (?:is|should be) selected/
     * @Then /^the option "(?P<option>(?:[^"]|\\")*)" from "(?P<select>(?:[^"]|\\")*)" (?:is|should be) selected$/
     * @Then /^"(?P<option>(?:[^"]|\\")*)" from "(?P<select>(?:[^"]|\\")*)" (?:is|should be) selected$/
     */
    public function theOptionFromShouldBeSelected($option, $select) {
        $selectField = $this->getSession()->getPage()->findField($select);
        if (null === $selectField) {
//            throw new ElementNotFoundException($this->getSession(), 'select field', 'id|name|label|value', $select);
            throw new Exception('select field ! ' . $option . ' not found');
        }

        $optionField = $selectField->find('named', array('option', $option,));

        if (null === $optionField) {
//            throw new ElementNotFoundException($this->getSession(), 'select option field', 'id|name|label|value', $option);
            throw new Exception('select option field' . $option);
        }

        if (!$optionField->isSelected()) {
//            throw new ExpectationException('Select option field with value|text "' . $option . '" is not selected in the select "' . $select . '"', $this->getSession());
            throw new Exception('Select option field with value ' . $option . '" is not selected in the select "' . $select);
        }
    }

    function validateDate($date, $format = 'd/m/Y h:i A') {
        $date = DateTime::createFromFormat($format, $date);
        if ($date)
            return true;
        else
            return false;
    }

    /**
     * @Then I should see format date
     */
    public function iShouldSeeFormatDate() {
        $idField = "new_startdate";
        $field = $this->getSession()->getPage()->find('named', array('id_or_name', $idField));
        if ($field)
            $validate_date = $this->validateDate($field->getValue(), "d/m/Y h:i");
        if (!$validate_date) {
            throw new Exception('Format date not valid ! ' . $field->getValue() . "\n");
        }

        $idField = "requestabsencebundle_requestabsence_enddate";
        $field = $this->getSession()->getPage()->find('named', array('id_or_name', $idField));
        if ($field)
            $validate_date = $this->validateDate($field->getValue(), "d/m/Y h:i");
        if (!$validate_date) {
            throw new Exception('Format date not valid ! ' . $field->getValue() . "\n");
        }

        $idField = "new_requestdate";
        $field = $this->getSession()->getPage()->find('named', array('id_or_name', $idField));
        if ($field)
            $validate_date = $this->validateDate($field->getValue(), "d/m/Y");
        if (!$validate_date) {
            throw new Exception('Format date not valid ! ' . $field->getValue() . "\n");
        }
    }

    /**
     * @Then I should see all required fields of form :form
     */
    public function iShouldSeeAllRequiredFielsOfForm($form) {
        $absenceFields = [
            "requestabsencebundle_requestabsence_employee", "requestabsencebundle_requestabsence_requestdate", "requestabsencebundle_requestabsence_manager"
            , "requestabsencebundle_requestabsence_requesters", "requestabsencebundle_requestabsence_absencetype", "new_startdate"
            , "new_enddate", "requestabsencebundle_requestabsence_numberdays"];

        $expenseTransportFields = [
            "expensebundle_ik_contributor", "expensebundle_ik_displayName", "expensebundle_transport_date"
            , "expensebundle_transport_validator", "expensebundle_transport_requester", "expensebundle_transport_type"
            , "expensebundle_transport_SettingsExpTransport", "expensebundle_transport_amountTran", "expensebundle_transport_untaxedAmountTrans",
            "expensebundle_transport_comment"];

        $expenseIkFields = [
            "expensebundle_ik_contributor", "expensebundle_ik_displayName", "expensebundle_transport_date", "expensebundle_transport_validator",
            "expensebundle_transport_requester", "expensebundle_transport_type"
            , "expensebundle_ik_kilometer", "expensebundle_ik_settingsExpIk", "expensebundle_ik_settingsExpIk", "expensebundle_ik_amountIk", "expensebundle_ik_comment"
        ];

        $expenseAccomodationFields = [
            "expensebundle_ik_contributor", "expensebundle_ik_displayName", "expensebundle_transport_date", "expensebundle_transport_validator",
            "expensebundle_transport_requester", "expensebundle_hrm_expenses_accomodation_type", "expensebundle_hrm_expenses_accomodation_settingsExpAccomodation",
            "expensebundle_hrm_expenses_accomodation_amountAcc", "expensebundle_hrm_expenses_accomodation_untaxedAmount", "expensebundle_hrm_expenses_accomodation_comment",
            "expensebundle_hrm_expenses_accomodation_settings_pay_period"];

        $employeeFields = ["collaborator_new_first_name", "collaborator_new_last_name", "collaborator_new_email"];

        $contractFields = ["userbundle_employeecontract_contract_number"
            , "userbundle_employeecontract_empStatus", "userbundle_employeecontract_contractType"
            , "userbundle_employeecontract_contractCustomCondition", "userbundle_employeecontract_convention"
            , "userbundle_employeecontract_salGrade", "userbundle_employeecontract_salClassification"
            , "userbundle_employeecontract_salEchelon", "userbundle_employeecontract_salJobTitle"
            , "userbundle_employeecontract_salJoinedDate", "userbundle_employeecontract_salSeniorityDate"
            , "userbundle_employeecontract_salLeftDate", "userbundle_employeecontract_work_timetable"
            , "userbundle_employeecontract_working_time", "userbundle_employeecontract_gross_annual_base_salary"
            , "userbundle_employeecontract_nb_of_installments", "userbundle_employeecontract_fixed_rest_days_agreed"
            , "userbundle_employeecontract_insuranceContractRef1", "userbundle_employeecontract_insuranceContractRef2", "userbundle_employeecontract_notes"];

        $expenseOtherFields = ["expensebundle_hrm_expenses_other_contributor", "expensebundle_hrm_expenses_other_displayName",
            "expensebundle_hrm_expenses_other_date", "expensebundle_hrm_expenses_other_validator",
            "expensebundle_hrm_expenses_other_requester", "expensebundle_hrm_expenses_other_type",
            "expensebundle_hrm_expenses_other_SettingsExpenses", "expensebundle_hrm_expenses_other_amountOth",
            "expensebundle_hrm_expenses_other_settings_pay_period"];

        $extrahourFields = [
            "extrahoursbundle_hrm_additional_hours_contributor", "extrahoursbundle_hrm_additional_hours_date",
            "extrahoursbundle_hrm_additional_hours_requester", "extrahoursbundle_hrm_additional_hours_validator",
            "extrahoursbundle_hrm_additional_hours_settings_additional_hours_type",
            "extrahoursbundle_hrm_additional_hours_hour25", "extrahoursbundle_hrm_additional_hours_hour50",
            "extrahoursbundle_hrm_additional_hours_settings_pay_period"];

        $transportFields = [
            "transportbundle_hrm_transport_contributor", "transportbundle_hrm_transport_date",
            "transportbundle_hrm_transport_validator", "transportbundle_hrm_transport_requester",
            "transportbundle_hrm_transport_type", "transportbundle_hrm_transport_amount"];

        $paydayFields = [
            "paydayvariationsbundle_paydayvariations_contributor", "paydayvariationsbundle_paydayvariations_date",
            "paydayvariationsbundle_paydayvariations_requester", "paydayvariationsbundle_paydayvariations_type",
            "paydayvariationsbundle_paydayvariations_validator", "paydayvariationsbundle_paydayvariations_amount",
            "paydayvariationsbundle_paydayvariations_nbre_months"];

        $benefitFields = [
            "benefitbundle_hrm_benefit_contributor", "benefitbundle_hrm_benefit_type",
            "benefitbundle_hrm_benefit_date", "benefitbundle_hrm_benefit_validator",
            "benefitbundle_hrm_benefit_realAmount", "benefitbundle_hrm_benefit_hrm_settings_benefit_periodicity",
            "benefitbundle_hrm_benefit_comment"];

        $employeFields = ["collaborator_new_first_name", "collaborator_new_last_name", "collaborator_new_email"];

        $companyFields = ["company_name", "company_industrySector"];

         
        switch ($form) {
            case "absence":
                echo $form;
                $fields = $absenceFields;
                break;
            case "expenseIk":
                echo $form;
                $fields = $expenseIkFields;
                break;
            case "expenseOther":
                echo $form;
                $fields = $expenseOtherFields;
                break;
            case "expenseTransportIk":
                $fields = $expenseFields;
                break;
            case "employee":
                $fields = $employeeFields;
                break;
            case "contract":
                $fields = $contractFields;
                break;
            case "extrahour":
                $fields = $extrahourFields;
                break;
            case "transport":
                $fields = $transportFields;
                break;
            case "paydayvariations":
                $fields = $paydayFields;
                break;
            case "benefit":
                $fields = $benefitFields;
                break;
            case "employe":
                $fields = $employeFields;
                break;
            case "company":
                $fields = $companyFields;
                break;
        }
        foreach ($fields as $field_id) {
            $field = $this->getSession()->getPage()->find('named', array('id_or_name', $field_id));
            if ($field)
                echo "Field : " . $field_id . " " . $field->getValue() . " Ok\n";
            else
                throw new Exception('The field cannot be empty ' . $field->getValue() . "\n");
        }
    }

    /**
     * @Then I should see default value of field validator :arg2
     */
    public function iShouldSeeDefaultValueOfFieldValidator($idField) {
        $companyId = $this->getContainer()->get('settingsbundle.preference.service')->getCompanyId();
        if (!$companyId)
            echo "No company found" . "\xA";
        else
            echo "Company found " . $companyId . "\xA";
        $firstValidator = $this->getContainer()->get('user.service')->findValidators($companyId);
        if (!$firstValidator)
            throw new Exception('No validator found! ');
        $defaultValue = $firstValidator->getId();
        $field = $this->getSession()->getPage()->find("css", $idField);
        if ($field) {
            if (strcasecmp($field->getValue(), $defaultValue) == 0)
                echo 'Default value of field "' . $idField . '" is ' . $defaultValue . "\xA";
            else
                throw new Exception('Default id of "' . $idField . '" shoudl be "' . $defaultValue . '" but "' . $field->getValue() . '" found \xA');
            echo 'I can see default id of field "' . $idField . '" -> ' . $field->getValue() . "\xA";
        } else {
            throw new Exception('Cannot find a field with this id! ' . $idField);
        }
    }

    /**
     * Fills in specified field with date
     * Example: When I fill in "field_ID" with date "now"
     * Example: When I fill in "field_ID" with date "-7 days"
     * Example: When I fill in "field_ID" with date "+7 days"
     * Example: When I fill in "field_ID" with date "-/+0 weeks"
     * Example: When I fill in "field_ID" with date "-/+0 years"
     *
     * @When /^(?:|I )fill in "(?P<field>(?:[^"]|\\")*)" with date "(?P<value>(?:[^"]|\\")*)"$/
     */
    public function fillDateField($field, $value) {
        $newDate = strtotime("$value");

        $dateToSet = date("d/m/Y", $newDate);
        $this->getSession()->getPage()->fillField($field, $dateToSet);
    }

    /**
     * @Then I should see format date :format on field :field
     */
    public function iShouldSeeFormatDateOnField($format, $field) {
        $field = $this->getSession()->getPage()->find("css", $field);
        if ($field) {
            if (($this->validateDate($field->getValue(), $format)) && ($field->getValue() != '01/01/1970'))
                echo "Format date is valid " . $format . " " . $field->getValue() . "\xA";
            else {
                echo 'Format date not valid !' . $format . " " . $field->getValue() . "\xA";
            }
        } else
            throw new Exception('Field not found !' . $field->getValue() . "\xA");
    }

    /**
     * @Then I should see format email on field :fieldId
     */
    public function iShouldSeeFormatEmailOnField($fieldId) {
        $field = $this->getSession()->getPage()->find("css", $fieldId);
        if ($field) {
            $value = $field->getValue();
            if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
                echo "Format email is valid  " . $value . "\xA";
            } else {
                throw new Exception("Format email on field " . $fieldId . " is not valid ! " . $value . "\xA");
            }
        } else
            throw new Exception('Field not found ! ' . $fieldId . "\xA");
    }

    /**
     * Selects current node specified option if it's a select field.
     *
     * @param   string  $option
     */
    public function selectOption($option) {
        $this->getSession()->getDriver()->selectOption($this->getXpath(), $option);
    }

    //empStatus

    /**
     * @When I select random from :name
     */
    public function selectState($name) {
        $select = $this->getSession()->getPage()->find('css', $name);
        $options = $select->findAll('css', 'option');
        echo Count($options) . " options found " . "\xA";
        $randomChoice = function($array) {
            return array_slice($array, 1)[array_rand(array_slice($array, 1))];
        };
        echo "I select random choice : " . $randomChoice($options)->getValue() . "\xA";
        $select->selectOption($randomChoice($options)->getValue());
    }

    /**
     * @When I select my name from :name
     */
    public function selectMyName($name) {
        $select = $this->getSession()->getPage()->find('css', $name);
        $options = $select->findAll('css', 'option');

        $current_user_FName = $this->getContainer()->get('security.context')->getToken()->getUser()->getFirstName();
        $current_user_LName = $this->getContainer()->get('security.context')->getToken()->getUser()->getLastName();
        echo "Name : " . $current_user_FName . " Lastname : " . $current_user_LName . "\xA";
        $current_user = $current_user_FName . " " . $current_user_LName;
        $select->selectOption($current_user);
    }

    public function getSymfonyProfile() {
        $driver = $this->getSession()->getDriver();
        if (!$driver instanceof KernelDriver) {
            throw new UnsupportedDriverActionException(
            'You need to tag the scenario with ' .
            '"@mink:symfony2". Using the profiler is not ' .
            'supported by %s', $driver
            );
        }

        $profile = $driver->getClient()->getProfile();
        if (false === $profile) {
            throw new \RuntimeException(
            'The profiler is disabled. Activate it by setting ' .
            'framework.profiler.only_exceptions to false in ' .
            'your config'
            );
        }

        return $profile;
    }

    /**
     * @Given I select the company of :arg1     
     */
    public function iSelectTheCompanyOf($username) {
        $select = $this->getSession()->getPage()->find('css', "company"); // if radio button exist 
        if ($select) {
            echo "I have many companies" . "\xA";
            if ($company)
                $select->selectOption($company);
            else
                $this->selectState("#" . $value->getAttribute("id")); //select random comapany
        }
        else {
            echo "I have one company, i select the first comapny" . "\xA";
            $user = $this->getContainer()->get('user.service')->findCurrentEmployee($username);
            if ($user) {
//                echo "I am " . $user->getFirstName() . " " . $user->getFirstName() . "\xA";
//                echo "My provider " . $user->getEmployee()->first()->getProvider()->getId() . "\xA";
                $this->getContainer()->get('session')->set('company', $user->getEmployee()->first()->getCompany()->getId());  // 30 ou 2 Définir l'id de company
            } else
                echo "No user found" . "\xA";
        }
        $company = $this->getContainer()->get('session')->get('company');
        $user = $this->getContainer()->get('user.service')->findUserByUsernameAndCompany($username, $company);
        if ($user)
            $this->currentUser = $user;
        else
            echo "No user found" . "\xA";
    }

    private static $container;

    /**
     * @BeforeSuite
     */
    public static function bootstrapSymfony() {
        require_once __DIR__ . '/../../app/autoload.php';
        require_once __DIR__ . '/../../app/AppKernel.php';
//        require_once __DIR__.'/../../vendor/phpunit/phpunit/src/Framework/Assert/Functions.php';
        $kernel = new AppKernel('test', true);
        $kernel->boot();
        self::$container = $kernel->getContainer();
    }

    /**
     * @param \Symfony\Component\HttpKernel\KernelInterface $kernel
     */
    public function setKernel(\Symfony\Component\HttpKernel\KernelInterface $kernel) {
        $this->kernel = $kernel;
    }

    /**
     * @Given there is an admin user :username with password :password
     */
    public function thereIsAnAdminUserWithPassword($username, $password) {
        $user = new \UserBundle\Entity\hrm_user();
        $user->setUsername($username);
        $user->setPlainPassword($password);
        $user->setRoles(array('ROLE_ADMIN'));
        $em = self::$container->get('doctrine')->getManager();
        $em->persist($user);
        $em->flush();
    }

    /**
     * Saving a screenshot
     *
     * @When I save a screenshot to :filename
     */
    public function iSaveAScreenshotIn($filename) {
        sleep(1);
        $this->saveScreenshot($filename, __DIR__ . '/../..');
    }

    /**
     * Lorsque je remplis un formulaire 
     * 
     * @When I fill the form :form with random values
     */
    public function iFillTheFormWithRandomValues($form) {
        $form = $this->getSession()->getPage()->find('css', $form);
        if ($form) {
            $options = $form->findAll('css', 'input');        // recuperer la liste des champs de formualire
            foreach ($options as $value) {                    // recuperer les noms des champs
                if ($value->isVisible()) {                    //if field is visible 
                    if ($value->getAttribute("type") == "number") {
//                        echo "Field " . $value->getAttribute("id") . "\xA";
                        $this->iFillRandomValueInField($value->getAttribute("id"));  //randomly fill values from 
                    } else if ($value->getAttribute("type") == "text") {
                        if ($value->hasAttribute("data-date-format")) //  if (strpos($value->getAttribute("css"), 'datepicker ') !== false)        
                            $this->iFillCurrentDateInField($value->getAttribute("id"));
                        else {                                                        //  check if not date field
//                            echo "Field " . $value->getAttribute("id") . "\xA";
                            $this->iFillRandomValueInField($value->getAttribute("id"));  //randomly fill values from 
                        }
                    }
                    if ($value->getAttribute("type") == "email") {
//                        echo "Field " . $value->getAttribute("id") . "\xA";
                        $this->iFillRandomEmailInField($value->getAttribute("id"));
                    }
                }
            }
            $options = $form->findAll('css', 'select');        // recuperer la liste des champs de formualire
            foreach ($options as $value) {                    // recuperer les noms des champs
                if ($value->isVisible()) {                    //if field is visible 
                    echo "Field select " . $value->getAttribute("id") . "\xA";
                    $this->selectState("#" . $value->getAttribute("id"));
                }
            }
        }
        echo "Form does not exist";
    }

    /**
     * Lorsque je remplis un formulaire 
     * 
     * @When I fill the form :form with random values and wait
     */
    public function iFillTheFormWithRandomValuesAndWait($form) {
        $form = $this->getSession()->getPage()->find('css', $form);
        if ($form) {
            $options = $form->findAll('css', 'input');        // recuperer la liste des champs de formualire
            foreach ($options as $value) {                    // recuperer les noms des champs
                if ($value->isVisible()) {                    //if field is visible 
                    if (($value->getAttribute("type") == "number") || ($value->getAttribute("type") == "text")) {
                        echo "Field " . $value->getAttribute("id") . "\xA";
                        $this->iFillRandomValueInField($value->getAttribute("id"));  //randomly fill values from 
                    } else if ($value->getAttribute("type") == "email")
                        $this->iFillRandomEmailInField($value->getAttribute("id"));
                }
            }
            $options = $form->findAll('css', 'select');        // recuperer la liste des champs de formualire
            foreach ($options as $value) {                    // recuperer les noms des champs
                if ($value->isVisible()) {                    //if field is visible 
                    echo "Field select " . $value->getAttribute("id") . "\xA";
                    $this->selectState("#" . $value->getAttribute("id"));
                }
            }
        }
        echo "Form does not exist";
    }

    /**
     * Lorsque je remplis un formulaire 
     * 
     * @When I shoud see required fields of :form
     */
    public function iSeeRequiredFiledsInForm($form) {

        $form = $this->getSession()->getPage()->find('css', $form);
        if ($form) {
            $options = $form->findAll('css');                 // recuperer la liste des champs de formualire
            foreach ($options as $value) {                    // recuperer les noms des champs
                if ($value->isVisible()) {                    // if field is visible 
                    if (($value->getAttribute("re") == "number") || ($value->getAttribute("type") == "text")) {
                        echo "Field " . $value->getAttribute("id") . "\xA";
                        $this->iFillRandomValueInField($value->getAttribute("id"));  //randomly fill values from 
                    } else if ($value->getAttribute("type") == "email")
                        $this->iFillRandomEmailInField($value->getAttribute("id"));
                }
            }
            $options = $form->findAll('css', 'select');        // recuperer la liste des champs de formualire
            foreach ($options as $value) {                    // recuperer les noms des champs
                if ($value->isVisible()) {                    //if field is visible 
                    echo "Field select " . $value->getAttribute("id") . "\xA";
                    $this->selectState("#" . $value->getAttribute("id"));
                }
            }
        }
        echo "Form does not exist";
    }

    /**
     * @Then I should see format :format of field :arg2
     */
    public function iShouldSeeFormatOfFieldCollab($idField) {
        $user = $this->getContainer()->get('user.service')->findCurrentEmployee("collaborateur");
        $defaultValue = $user->getId();
        $field = $this->getSession()->getPage()->find("css", $idField);


        if ($field) {
            if (strcasecmp($field->getValue(), $defaultValue) == 0)
                echo 'Default value of field "' . $idField . '" is ' . $defaultValue . '\n';
            else
                throw new Exception('Default id of "' . $idField . '" shoudl be "' . $defaultValue . '" but "' . $field->getValue() . '" found \n');
            echo 'I can see default id of field "' . $idField . '" -> ' . $field->getValue() . "\n";
        } else {
            throw new Exception('Cannot find a field with this id! ' . $idField);
        }
    }

    /**
     * @Then I should see dafault value :defaultValue in field :arg2
     */
    public function iShouldSeeDefaultValueInField($defaultValue, $idField) {
        $field = $this->getSession()->getPage()->find("css", $idField);
        if ($field) {
            if (strcasecmp($field->getValue(), $defaultValue) == 0)
                echo 'Default value of field "' . $idField . '" is ' . $field->getValue() . '\n';
            else
                throw new Exception('Default value of field "' . $idField . '" shoudl be "' . $defaultValue . '" but "' . $field->getValue() . '" found \n');
        } else {
            throw new Exception('Cannot find a field with this id! ' . $idField);
        }
    }

    /**
     * @Then I should see amount ht :idFieldAmount :idFieldTVA5 :idFieldTVA20 :amount
     */
    public function iShouldSeeValueOfAmountHT($idFieldAmount, $idFieldTVA5, $idFieldTVA20, $idFieldUntaxedAmount) {
        $fieldAmount = $this->getSession()->getPage()->find("css", $idFieldAmount);
        if (!$fieldAmount)
            throw new Exception('Cannot find a field with this id! ' . $idFieldAmount);

        $fieldTVA5 = $this->getSession()->getPage()->find("css", $idFieldTVA5);
        if (!$fieldTVA5)
            throw new Exception('Cannot find a field with this id! ' . $idFieldTVA5);

        $fieldTVA20 = $this->getSession()->getPage()->find("css", $idFieldTVA20);
        if (!$fieldTVA20)
            throw new Exception('Cannot find a field with this id! ' . $idFieldTVA20);

        $fieldUntaxedAmount = $this->getSession()->getPage()->find("css", $idFieldUntaxedAmount);
        if (!$fieldUntaxedAmount)
            throw new Exception('Cannot find a field with this id! ' . $idFieldUntaxedAmount);

        $expectedAmountHt = $fieldAmount->getValue() - $fieldTVA5->getValue() - $fieldTVA20->getValue();
        $AmountHt = str_replace(" ", "", $fieldUntaxedAmount->getValue());
        $AmountHtv = substr($AmountHt, 0, strpos($AmountHt, ","));
        $AmountHtv = preg_replace('/\D/', '', $AmountHtv);

        echo "expectedAmountHt : " . $expectedAmountHt . "\xA";
        echo "AmountHt : " . $AmountHtv . "\xA";

        if ($expectedAmountHt != $AmountHtv)
            throw new Exception('Montant HT shoudl be "' . $expectedAmountHt . '" but "' . $AmountHtv . '" found \n' . "\xA");
        else
            echo "Test Calcul TVA est correct " . "\xA";
    }

    /**
     * @When I press the button :button :number times
     */
    public function iPressTheButtonTimes($number, $button) {
        echo "When I press the button " . $number . " times" . "\xA";
        $this->getPage()->pressButton('btn_option_bonus_' . $button);
    }

    /**
     *
     * @Then /^I should see current month$/
     */
    public function iShouldSeeCurrentMonth() {
        $currentMonth = date('m/Y');
        $element = $this->getSession()->getPage()->find(
                'xpath', $this->getSession()->getSelectorsHandler()->selectorToXpath('xpath', '*//*[text()="' . $currentMonth . '"]')
        );
        if (!$element) {
            echo "I can see default current month date " . $currentMonth . "\xA";
        } else
            throw new \Exception("Can't find current month " . $currentMonth . "\xA");
    }

    /**
     * @Then I should see the current date
     */
    public function iShouldSeeTheCurrentDate() {
        $currentDate = date('d/m/Y');
        $element = $this->getSession()->getPage()->find(
                'xpath', $this->getSession()->getSelectorsHandler()->selectorToXpath('xpath', '*//*[text()="' . $currentDate . '"]')
        );
        if (!$element) {
            echo $currentDate . "I can see default Current date " . $currentDate . "\xA";
        } else
            throw new \Exception("Can't find current date " . $currentDate . "\xA");
    }

    /**
     * @Then I should see default value current date of field :arg2 
     */
    public function iShouldSeeDefaultValueCurrentDateOfField($idField) {
        $defaultValue = date('d/m/Y');
        $field = $this->getSession()->getPage()->find("css", $idField);
        if ($field) {
            echo "field " . $field->getValue() . "\xA";
            if (strcasecmp($field->getValue(), $defaultValue) == 0)
                echo 'Default value of field "' . $idField . '" is ' . $defaultValue . "\xA";
            else
                throw new Exception('Default id of "' . $idField . '" shoudl be "' . $defaultValue . '" but "' . $field->getValue() . '" found \xA');
            echo 'I can see default id of field "' . $idField . '" -> ' . $field->getValue() . "\xA";
        } else {
            throw new Exception('Cannot find a field with this id! ' . $idField);
        }
    }

    /**
     * @Then I fill current date in field :arg2 
     */
    public function iFillCurrentDateInField($idField) {
        $currentDate = date('d/m/Y');
        $field = $this->getSession()->getPage()->find("css", $idField);

        echo "Current date " . $currentDate . "\xA";
        $this->getSession()->getPage()->fillField($idField, $currentDate);

//             if ($field) {
//            
//    } else {
//            throw new Exception('Cannot find a field with this  ! ' . $idField);
//        }
    }

    /**
     * @Then I fill current month in field :arg2 
     */
    public function iFillCurrenMonthInField($idField) {
        $currentMonth = date('m/Y');
        echo "Current month " . $currentMonth . "\xA";
        $field = $this->getSession()->getPage()->find("css", $idField);
//        if ($field)
        $this->getSession()->getPage()->fillField($idField, $currentMonth);
//        throw new Exception('Cannot find a field with this  ! ' . $idField);
    }

    /**
     * @Then I should see all fields of form :formName are filled
     */
    public function IShouldSeeAllFieldsOfFormAreFilled($formName) {
        $allfield = true;
        $form = $this->getSession()->getPage()->find('css', $formName);
        if ($form) {
            $options = $form->findAll('css', 'input');        // recuperer la liste des champs de formualire
            foreach ($options as $field) {                    // recuperer les noms des champs
                if ($field->isVisible()) {                    //if field is visible 
                    echo "Field : " . $field->getAttribute("id") . " Value : " . $field->getValue() . "\xA";
                    if (strlen($field->getValue()) < 1)
                        $allfield = false;
                }
            }
            $options = $form->findAll('css', 'select');        // recuperer la liste des champs de formualire
            foreach ($options as $field) {                    // recuperer les noms des champs
                if ($field->isVisible()) {                    //if field is visible 
                    echo "Field : " . $field->getAttribute("id") . " Value : " . $field->getValue() . "\xA";
                    if (strlen($field->getValue()) < 1)
                        $allfield = false;
                }
            }
            $options = $form->findAll('css', 'textarea');        // recuperer la liste des champs de formualire
            foreach ($options as $field) {                    // recuperer les noms des champs
                if ($field->isVisible()) {                    //if field is visible 
                    echo "Field : " . $field->getAttribute("id") . " Value :" . $field->getValue() . "\xA";
                    if (strlen($field->getValue()) < 1)
                        $allfield = false;
                }
            }
        } else
            echo "Form " . $formName . " does not exist \xA";
        if ($allfield == false)
            echo "All fields of form are NOT completed" . "\xA";
        else if ($allfield == true)
            echo "All fields of form are completed" . "\xA";
    }

    /**
     * @When I close symfony toolbar
     */
    public function iCloseSymfonyToolBar() {
        $script = "$('[id^=sfToolbarMainContent] > a').click();";
        $this->getSession()->executeScript("$('[id^=sfToolbarMainContent] > a').click();");
        $this->getSession()->executeScript('$("[id^=sfToolbarMainContent] > a").click();');
        echo "close toolbaar ";
        $function = <<<JS
(function(){
  var elem = $('[id^=sfToolbarMainContent] > a').click();
})()
JS;
        try {
            $this->getSession()->executeScript($function);
        } catch (Exception $e) {
            throw new \Exception("ScrollIntoView failed");
        }
        $isactive = "$('[id^=sfToolbarMainContent]').css('display');";
        $toolbarstatut = $this->getSession()->evaluateScript($isactive);
        if ($toolbarstatut = "block")
            echo "toolbar is active \xA";
        else if ($toolbarstatut = "none")
            echo "toolbar n'est pas active \xA";
    }

    /**
     * @When I scroll to menu :elementId
     */
    public function scrollToMenu($elementId) {
        $function = <<<JS
        (function(){
                 document.getElementById("TabExport").scrollIntoView(false);
//                $("#id-sidebar-1").scrollTop($("#'.$elementId.'").scrollTop());
        })()
JS;
        try {
            $this->getSession()->executeScript($function);
        } catch (Exception $e) {
            //   throw new \Exception("ScrollIntoView failed");
        }

        $this->getSession()->executeScript(' document.getElementById("TabExport").scrollIntoView(false);');

        $script = '$("#id-sidebar-1").scrollTop($("#' . $elementId . '").scrollTop())';
        $this->getSession()->executeScript($script);
    }

    /**
     * @Then  I should download a :fileType file after clicking on :link
     */
    public function iShouldSeeInTheHeader($linkName, $fileType) {
        if ($fileType == "zip")
            $contentType = "application/zip";
        else if ($fileType == "csv")
            $contentType = "text/csv; charset=utf-8";

        //SeleniumDriver is not able to check response header so i will use GoutteDriver
        $seleniumCookies = $this->getSession()->getDriver()->getCookie("PHPSESSID");
        $this->iWaitForAjax();
        $client = new \Goutte\Client();
        $driver = new \Behat\Mink\Driver\GoutteDriver($client);
        $goutteSession = new \Behat\Mink\Session($driver);
        $goutteSession->setCookie("PHPSESSID", $seleniumCookies);
        $this->iWaitForAjax();
        // Test if the link contain a CSV file
        $link = $this->getSession()->getPage()->find("css", $linkName);
        if (null === $link) {
//            throw new \Exception('The button was not found ' . $link);
            echo ('The button was not found ' . $link);
        }
        $url = $link->getAttribute("href");
        echo "url" . $url . "\xA";
        $goutteSession->visit($url);

        if ($goutteSession->getResponseHeader("Content-Type") != $contentType) {
//            throw new \Exception(sprintf("Unable to download the CSV file ,Response is !".$goutteSession->getResponseHeader("Content-Type")."\xA"));
            echo ("Unable to download the CSV file ,Response is !" . $goutteSession->getResponseHeader("Content-Type") . "\xA");
        } else {
            echo "The CSV file has been found on the remote server \xA";
            if ($fileType == "CSV") {
                $content = $goutteSession->getPage()->getContent();
                $lines = explode(PHP_EOL, $content);
                $this->csvRows = [];
                foreach ($lines as $line) {
                    if (strlen(trim($line))) {
                        $this->csvRows[] = str_getcsv($line);
                    }
                }
            }
//            print_r($this->csvRows);
        }
        // Download the csv file
        $link->click();
        echo "Downloading CSV file...\xA";



//        
//        echo "Url ".$button->getAttribute("href");
//        $goutteSession->visit('http://localhost/dev/hrm/web/app_dev.php/export/exportemployees');
//        $this->iWaitForAjax();
//        $this->iSaveAScreenshotIn("two.png");
//        echo "getStatusCode " . $goutteSession->getStatusCode() . "\xA";
//        echo "getCurrentUrl " . $goutteSession->getCurrentUrl() . "\xA";
//        echo "getHeaders " . $goutteSession->getResponseHeader("Content-Type") . "\xA";
    }

    /**
     * @When  I open payperiod
     */
    public function iOpenPayperiod() {
        $companyId = $this->getContainer()->get('settingsbundle.preference.service')->getCompanyId();
        echo "companyId " . $companyId . "\xA";

        $d = new DateTime();
        $startDate = $d->modify('first day of next month');
        $endDate = $d->modify('last day of next month');

        $delete = $this->getContainer()->get('PayPeriodBundle.service')->deletePayPeriodForcompany($companyId, $startDate, $endDate);
        if ($delete)
            echo "Payperiod deleted" . "\xA";
        else
            echo "Errror ! Payperiod Not deleted" . "\xA";

//        $getOpenPayPeriod = $this->getContainer()->get('PayPeriodBundle.service')->getOpenPayPeriodForcompany($companyId);
//      
//        if ($getOpenPayPeriod) {
////         print_r($getOpenPayPeriod);
//            echo " pay period found" . "\xA";
////          var_dump($getOpenPayPeriod);
//        } 
//        else
//        {
//            echo "no pay period found" . "\xA";
//        }

        $d = new DateTime();
        $d->modify('next month');
        $nextMonth = $d->format('m/Y');

        $linkName = "#btn_export_open_payperiod";
        $link = $this->getSession()->getPage()->find("css", $linkName);
        if (null === $link) {
            throw new \Exception('The button was not found ' . $link);
        }
        else
            echo "btn_export_open_payperiod not found !" . "\xA";
        $link->click();

        
        
        
        
          $open_payperiod_confirm = $this->getSession()->getPage()->find("css", "#open_payperiod_confirm");
          if ($open_payperiod_confirm)
//        $btn_payperiod_close_unable = $this->getSession()->getPage()->find("css", "#btn_payperiod_close_unable");
//        if ($btn_payperiod_close_unable)
         {
            echo "Found open_payperiod_confirm !" . "\xA";
            //$buttonNameOpenConfirm = "#add_payperiod_confirm";
            $buttonNameOpenConfirm = "#open_payperiod_confirm";
            $buttonOpenConfirm = $this->getSession()->getPage()->find("css", $buttonNameOpenConfirm);
            if ($buttonOpenConfirm) {
                echo "Button found !I can add a new payperiod !" . "\xA";

                echo "next month " . $nextMonth . "\xA";
//          $idInput = "#pay_period";
                $idInput = "#enddate";
                echo "1 " . "\xA";
                $input = $this->getSession()->getPage()->find("css", $idInput);
                if ($input) {
                    echo "input found ! " . $idInput . "\xA";
                    $input->SetValue($nextMonth);
                } else
                    echo "input not found !" . "\xA";
                echo "2 " . "\xA";
                $buttonNameOpenConfirm = "#open_payperiod_confirm";

                echo "i click on button" . $buttonNameOpenConfirm . "\xA";
                $buttonOpenConfirm->click();
            }
            else {
                echo "Error ! Button open_payperiod_confirm is not visible !" . "\xA";
                echo "Button open payperiod confirm not found i will close" . "\xA";
                sleep(2);
                $buttonNameClose = "#btn_payperiod_close_unable";
                $buttonClose = $this->getSession()->getPage()->find("css", $buttonNameClose);
                if ($buttonClose) {
                    echo "button close found ! \xA";
                    $buttonClose->click();
                } else
                    echo('The button ' . $buttonNameClose . " was not found " . "\xA");
            }
        }
    }

    
      /**
     * @When I close payperiod
     */
    public function iClosePayperiod() {
        $d = new DateTime();
        $d->modify('next month');
        $nextMonth = $d->format('m/Y');
        
        $linkName = "#btn_export";
        $link = $this->getSession()->getPage()->find("css", $linkName);
        if ($link) {
            echo "button btn_export found  !" . "\xA";
            $link->click();
        }
        else
             echo "error button btn_export not found  !" . "\xA";
         sleep(2);
        
        $linkName = "#btn_export_close_payperiod";
        $link = $this->getSession()->getPage()->find("css", $linkName);
        if ($link) {
            echo "button btn_export_close_payperiod found !" . "\xA";
            $link->click();
        }
        else
             echo "error btn_export_close_payperiod not found  !" . "\xA";
        sleep(2);
        
            $select = $this->getSession()->getPage()->find('css', "#select_pay_to_close"); //
            if ($select) {
                echo "select_pay_to_close found  !" . "\xA";
                
                  $options = $select->findAll('css', 'option');
                  echo Count($options) . " options found " . "\xA";
                  foreach ($options as $option) {
                        echo "option  ".$option->getValue() ." Text  ".$option->getText(). "\xA";
                  }
                $select->selectOption($nextMonth);
                echo "i select current month " . $nextMonth . "\xA";
            }
            else
            echo "no select found select_pay_to_close " . "\xA";
                $input = $this->getSession()->getPage()->find("css", "#input_pay_to_close");
                if ($input) {
                    echo "input found ! I will write CLOTURE". "\xA";
                    $input->SetValue("CLOTURE");
                }
                else
                    echo "input not found !" . "\xA";
         sleep(2);               
        $confirm_payperiod_close = $this->getSession()->getPage()->find("css", "#confirm_payperiod_close");
        if ($confirm_payperiod_close) 
            echo "Found confirm_payperiod_close   !" . "\xA";
        else
            echo "error confirm_payperiod_close not found  !" . "\xA";
        $confirm_payperiod_close->click();
     
    }
   
    
        /**
     * @When I should see format in number field :fieldId
     */
    public function iShouldSeeFormatInNumberField($fieldId)
    {
       $field = $this->getSession()->getPage()->find("css", $fieldId);
        if ($field) {
            $value = $field->getValue();
            if (is_numeric($value)) {
                echo "Format number is valid  " . $value . "\xA";
            } else {
                throw new Exception("Format number in field " . $fieldId . " is not valid ! " . $value . "\xA");
            }
        } else
            throw new Exception('Field not found ! ' . $fieldId . "\xA");
    }
       

     /**
     * @When I should see all fields on form :formName are filled out
     */
    public function iShouldSeeAllFieldsOnFormAreFilledOut($formName) {
        $allfield = true;
        $form = $this->getSession()->getPage()->find('css', $formName);
        if ($form) {
            $options = $form->findAll('css', 'input');        // recuperer la liste des champs de formualire
            foreach ($options as $field) {                    // recuperer les noms des champs
                if ($field->isVisible()) {                    //if field is visible 
                    echo "Field : " . $field->getAttribute("id") . " Value : " . $field->getValue() . "\xA";
                    if (strlen($field->getValue()) < 1) {
                        $allfield = false;
                        break;
                    }
                }
            }
            $options = $form->findAll('css', 'select');        // recuperer la liste des champs de formualire
            foreach ($options as $field) {                    // recuperer les noms des champs
                if ($field->isVisible()) {                    //if field is visible 
                    echo "Field : " . $field->getAttribute("id") . " Value : " . $field->getValue() . "\xA";
                    if (strlen($field->getValue()) < 1) {
                        $allfield = false;
                        break;
                    }
                }
            }
            $options = $form->findAll('css', 'textarea');        // recuperer la liste des champs de formualire
            foreach ($options as $field) {                    // recuperer les noms des champs
                if ($field->isVisible()) {                    //if field is visible 
                    echo "Field : " . $field->getAttribute("id") . " Value :" . $field->getValue() . "\xA";
                    if (strlen($field->getValue()) < 1) {
                        $allfield = false;
                        break;
                    }
                }
            }
        } else
            echo "Form " . $formName . " does not exist \xA";
        if ($allfield == false)
            throw new \Exception("All fields of form are NOT completed" . "\xA");
        else if ($allfield == true)
            echo "All fields of form are completed" . "\xA";
    }

}