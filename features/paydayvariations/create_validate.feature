#features/paydayvariations/create_validate.feature
Feature: Create and validate an payday variations 
  Create and validate a payday variations
  @javascript
  Scenario: creation/validation d'un objet par un collaborateur valide par un responsable 
    #0 - Se connecter en mode Collaborateur
    Given I am logged using "collaborateur" and "collaborateur"
    When I delete all "paydayvariations"
    And I select the company of "collaborateur"
    When I maximaze window   
    Then I should be on "/dashboard/"

    #1 - Creation d'un objet
    When I delete all "paydayvariations"
    And I click on the link "TabPaydayVariations"
    And I wait for ajax
    And I press "btn_new_paydayvariations"
    And I wait for the form to appear
    And I select "Collaborateur Collaborateur" from "paydayvariationsbundle_paydayvariations_contributor"
    And I fill current date in field "paydayvariationsbundle_paydayvariations_date"
    And I select "Avance" from "paydayvariationsbundle_paydayvariations_type"
    And I select "Responsable Responsable" from "paydayvariationsbundle_paydayvariations_validator"
    #And I select "Saisie sur salaire" from "paydayvariationsbundle_paydayvariations_nature"
    And I fill random value in field "paydayvariationsbundle_paydayvariations_amount" 
    And I fill random value in field "paydayvariationsbundle_paydayvariations_nbre_months"
    And I fill random value in field "paydayvariationsbundle_paydayvariations_comment" 

    #2 - Controler les champs
    #valeur par défaut
    And I should see default value of collaborator in field "#paydayvariationsbundle_paydayvariations_contributor"
    And I should see default value of field validator "#paydayvariationsbundle_paydayvariations_validator"
    And I should see default value current date of field "#paydayvariationsbundle_paydayvariations_date"

    #format
    And I should see format date "d/m/Y" on field "#paydayvariationsbundle_paydayvariations_date"
  
    #obligatoire
    Then I should see all required fields of form "paydayvariations"
    
    #3 - Renseigner l'exhaustivité des champs dans le formulaire
    Then I should see all fields of form "#form_paydayvariations_new" are filled

    #4 - Sauvegarder en mode Brouillon l'objet
    When I scroll "SauvegarderPayDay" into view
    And I press "SauvegarderPayDay" 
    And I wait for the form to close
    And I wait for ajax

    #5 - Passer l'objet du statut 
    When I click to "edit" a "paydayvariations"
    And I wait for 2 seconds
    And I wait for the form to appear
    When I scroll "EditValiderPayDay" into view
    And I press "EditValiderPayDay" 
    And I wait for ajax
    When I click to "show" a "paydayvariations"
    Then I wait for 2 seconds
    Then I should see "En attente de validation"
    And I press "btn_return_paydayvariations"
    And I wait for ajax

    #6 - Controler en mode lecture que tous les champs sont renseignes
    When I click to "show" a "paydayvariations"
    And I wait for 2 seconds
    Then I should see "Collaborateur Collaborateur"
    And I should see "Avance"  
    And I should see the current date
    And I should see "Responsable Responsable"  
    And I should see "Collaborateur Collaborateur"
    And I should see "En attente de validation"
    And I should see "Commentaires"
    And I should see "Montant"
    And I should see "Nombre de mensualités"
    And I wait for 2 seconds

    #7 - Controler en mode liste la presence de l'objet dans la grille de donnees
    And I press "btn_return_paydayvariations"
    And I wait for ajax
    And I wait for the table "paydayvariations" to appear

    #8 - Controler les valeurs de l'objet dans la grille de donnees
    And I wait for ajax
    Then I should see "Collaborateur Collaborateur"
    #And I should see "10/2017"
    And I should see "En attente"

    #9 - Se connecter avec le responsable du collaborateur 
    When I am logged using "responsable" and "responsable"
    When I click on the link "TabPaydayVariations"
    And I wait for ajax
    And I wait for 5 seconds

    #10 - Dans le mode Liste selectionner le filtre par statut 
    And I press "btn_filter_statut" 
    And I wait for 2 seconds
    And I click on the link "btn_filter_status_waiting" 
    Then I wait for 2 seconds

    #11 - Valider la demande du collaborateur
    When I click to "confirm" a "paydayvariations"
    And I wait for ajax
    And I wait for 5 seconds

    #12 - Controler que l'objet est au statut "Valide"
    When I press "btn_filter_statut" 
    And I wait for 2 seconds
    And I click on the link "btn_filter_status_all" 
    And I wait for 2 seconds
    And I click to "show" a "paydayvariations"
    Then I should see "Valide"