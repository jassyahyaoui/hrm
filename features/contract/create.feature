#features/contract/create.feature
Feature: Create a contract 
  Create a contract
  @javascript
  Scenario: creation de contract pour collaborateur
    #0 - Se connecter en mode Responsable
    Given I am logged using "responsable" and "responsable"
   
    #And I select "36" from "company"
    #And I press "_submit"
    When I maximaze window   
    Then I should be on "/dashboard/"
    And I wait for ajax

    #1 - Creation d'un objet

    When I click on the link "TabCollaborator"
    And I wait for ajax
    And I press "btn_new_collaborator"
    And I wait for the form to appear

    #2 - Controler les champs
    And I wait for ajax
    And I fill random value in field "collaborator_new_first_name" 
    And I fill random value in field "collaborator_new_last_name" 
    And I fill random email in field "collaborator_new_email" 
    And I press "btn_new_validate_collaborator_contract"
    And I wait for ajax
    And I wait for ajax
    And I wait for the form to appear
    And I fill random value in field "userbundle_employeecontract_contract_number" 
    And I select random from "#userbundle_employeecontract_empStatus"
    And I select random from "#userbundle_employeecontract_contractType"
    #And I select "stagiaire" from "userbundle_employeecontract_empStatus"
    #And I select "Stage" from "userbundle_employeecontract_contractType"

    And I fill random value in field "userbundle_employeecontract_contractCustomCondition" 
    And I fill random value in field "userbundle_employeecontract_convention"
    And I fill random value in field "userbundle_employeecontract_salGrade"
    And I fill random value in field "userbundle_employeecontract_salClassification"
    And I fill random value in field "userbundle_employeecontract_salEchelon"
    And I fill random value in field "userbundle_employeecontract_salJobTitle"
    And I fill in the following "01/10/2017" in field "userbundle_employeecontract_salJoinedDate"
    And I fill in the following "15/10/2017" in field "userbundle_employeecontract_salSeniorityDate"
    And I fill in the following "30/10/2017" in field "userbundle_employeecontract_salLeftDate"
    And I fill random value in field "userbundle_employeecontract_work_timetable"
    And I fill random value in field "userbundle_employeecontract_working_time"
    And I fill random value in field "userbundle_employeecontract_gross_annual_base_salary"
    And I fill random value in field "userbundle_employeecontract_nb_of_installments"
    And I fill random value in field "userbundle_employeecontract_fixed_rest_days_agreed"
    And I fill random value in field "userbundle_employeecontract_insuranceContractRef1"
    And I fill random value in field "userbundle_employeecontract_insuranceContractRef2"
    And I fill random value in field "userbundle_employeecontract_notes"

    And I wait for ajax
    And I should see format date "d/m/Y" on field "#userbundle_employeecontract_salJoinedDate"
    And I should see format date "d/m/Y" on field "#userbundle_employeecontract_salSeniorityDate"
    And I should see format date "d/m/Y" on field "#userbundle_employeecontract_salLeftDate"
    Then I should see all required fields of form "contract"
    And I wait for 5 seconds

    When I scroll "btn_edit_validate_contract" into view
    And I press "btn_edit_validate_contract" 
    And I wait for the form to close
    And I wait for ajax
    And I wait for 2 seconds
    And I wait for ajax
    And I wait for ajax

    #4 - Sauvegarder en mode Brouillon l'objet

    #5 - Passer l'objet du statut 
    When I click to "show" a "contract"
    Then I wait for 2 seconds

    And I press "btn_return_contract"
    And I wait for ajax

    #6 - Controler en mode lecture que tous les champs sont renseignes
    When I click to "show" a "contract"
    And I wait for 2 seconds
    Then I should see "Numéro du contrat"
    And I wait for 2 seconds

    #7 - Controler en mode liste la presence de l'objet dans la grille de donnees
    And I press "btn_return_contract"
    And I wait for ajax

    #8 - Controler les valeurs de l'objet dans la grille de donnees
 