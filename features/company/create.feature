#features/comany/create.feature
Feature: A.1 Company

@javascript
Scenario: B.1 CREATION
    #0 - Se connecter en mode responsable
    Given I am logged using "responsable" and "responsable"
    #When I maximaze window  
    When I click on the link "TabCompany"
    And I wait for ajax

    #1 - Création d'un objet
    And I fill the form "#form_edit_company" with random values
    And I fill random email in field "company_email" 

     #2 - Controler les champs
    #format
    And I should see format email on field "#company_email"
    And I should see format in number field "#company_employeeNumber"

    #1 - modification d'un objet

    #obligatoire
    Then I should see all required fields of form "company"

    #3 - Renseigner l'exhaustivité des champs dans le formulaire
    And I should see all fields on form "#form_edit_company" are filled out

    #4 - Sauvegarder l'objet
    When I scroll "EditSave" into view
    And I press "EditSave"

