#features/transport/create_validate.feature
Feature: Create and validate an transport 
  Create and validate an transport
  @javascript
  Scenario: creation/validation d'un objet par un collaborateur valide par un responsable 
    #0 - Se connecter en mode Collaborateur
    Given I am logged using "collaborateur" and "collaborateur"
    When I delete all "transport"
    And I select the company of "collaborateur"
    When I maximaze window   
    Then I should be on "/dashboard/"

    #1 - Creation d'un objet
    When I delete all "transport"
    When I click on the link "TabTransport"
    And I wait for ajax
    And I press "btn_new_transport"
    And I wait for the form to appear
    #And I fill in the following "31/10/2017" in field "transportbundle_hrm_transport_date"
    And I fill current date in field "transportbundle_hrm_transport_date"
    And I select "Collaborateur Collaborateur" from "transportbundle_hrm_transport_contributor"
    And I select "Responsable Responsable" from "transportbundle_hrm_transport_validator"
    And I select "Transport public" from "transportbundle_hrm_transport_type"
    And I fill random value in field "transportbundle_hrm_transport_amount" 
    And I fill in the following "30/10/2017 08:00 AM" in field "transportbundle_hrm_transport_startDate"
    And I fill in the following "31/10/2017 05:00 PM" in field "transportbundle_hrm_transport_endDate"
    And I fill random value in field "transportbundle_hrm_transport_comment" 

    #2 - Controler les champs
    #valeur par défaut
    #And I should see default value of field validator "#transportbundle_hrm_transport_validator"

    #format
    And I should see format date "d/m/Y" on field "#transportbundle_hrm_transport_startDate"
    And I should see format date "d/m/Y" on field "#transportbundle_hrm_transport_endDate"
  
    #obligatoire
    Then I should see all required fields of form "transport"

    When I scroll "SauvegarderTransport" into view
    And I press "SauvegarderTransport" 
    And I wait for the form to close
    And I wait for ajax

    #4 - Sauvegarder en mode Brouillon l'objet
    #Then the transport should be "Brouillon"
    #Then I should found the new "transport"

    #5 - Passer l'objet du statut 
    When I click to "edit" a "transport"
    And I wait for 2 seconds
    And I wait for the form to appear
    When I scroll "EditValiderTransport" into view
    And I press "EditValiderTransport" 
    And I wait for ajax
    When I click to "show" a "transport"
    Then I wait for 2 seconds
    Then I should see "En attente de validation"
    And I press "btn_return_transport"
    And I wait for ajax

    #6 - Controler en mode lecture que tous les champs sont renseignes
    When I click to "show" a "transport"
    And I wait for 2 seconds
    Then I should see "Collaborateur Collaborateur"
    And I should see the current date
    And I should see "Responsable Responsable"  
    And I should see "Collaborateur Collaborateur"
    And I should see "Transport public"
    And I should see "30/10/2017"
    And I should see "31/10/2017"
    And I should see "En attente de validation"
    And I wait for 2 seconds

    #7 - Controler en mode liste la presence de l'objet dans la grille de donnees
    And I press "btn_return_transport"
    And I wait for ajax
    And I wait for the table "transport" to appear

    #8 - Controler les valeurs de l'objet dans la grille de donnees
    And I wait for ajax
    Then I should see "Collaborateur Collaborateur"
    And I should see the current date
    And I should see "30/10/2017"
    And I should see "31/10/2017"
    And I should see "En attente"

    #9 - Se connecter avec le responsable du collaborateur 
    When I am logged using "responsable" and "responsable"
    When I click on the link "TabTransport"
    And I wait for ajax
    And I wait for 5 seconds

    #10 - Dans le mode Liste selectionner le filtre par statut 
    #1And I press "btn_filter_statut" 
    #1And I wait for 20 seconds
        #1#1And I click on the link "btn_filter_status_waiting"
    #And I wait for 2 seconds

    And I press "btn_filter_statut" 
    And I wait for 2 seconds
    #When I click on the text "valid"
    And I click on the link "btn_filter_status_waiting" 
    And I wait for 2 seconds

    #11 - Valider la demande du collaborateur
    When I click to "confirm" a "transport"
    And I wait for ajax
    And I wait for 5 seconds

    #12 - Controler que l'objet est au statut "Valide"
    When I press "btn_filter_statut" 
    And I wait for 2 seconds
    And I click on the link "btn_filter_status_all" 
    And I wait for 2 seconds
    And I click to "show" a "transport"
    Then I should see "Valide"