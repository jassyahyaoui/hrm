#features/transport/control_uniqueness_save.feature
Feature: CONTROLE UNICITE SAUVEGARDE
  @javascript
  Scenario: CONTROLE UNICITE SAUVEGARDE
    #0 - Se connecter en mode Collaborateur
    Given I am logged using "collaborateur" and "collaborateur"
    And I wait for ajax
    And I select the company of "collaborateur"

    When I delete all "expenses"
    When I maximaze window   
    Then I should be on "/dashboard/"
    And I wait for ajax

    #1 - Creation d'un objet
    When I delete all "transport"
    When I click on the link "TabTransport"
    And I wait for ajax
    And I press "btn_new_transport"
    And I wait for the form to appear
      And I fill current date in field "transportbundle_hrm_transport_date"
    And I select "Collaborateur Collaborateur" from "transportbundle_hrm_transport_contributor"
    And I select "Responsable Responsable" from "transportbundle_hrm_transport_validator"
    And I select "Transport public" from "transportbundle_hrm_transport_type"
    And I fill random value in field "transportbundle_hrm_transport_amount" 
    And I fill in the following "30/10/2017 08:00 AM" in field "transportbundle_hrm_transport_startDate"
    And I fill in the following "31/10/2017 05:00 PM" in field "transportbundle_hrm_transport_endDate"
    And I fill random value in field "transportbundle_hrm_transport_comment" 

    #2 - Renseigner les champs obligatoires
    Then I should see all required fields of form "transport"

    #3 - Cliquer deux fois sur le bouton "Sauvegarder"
    When I scroll "SauvegarderTransport" into view
    And I press "SauvegarderTransport" 
    And I press "SauvegarderTransport" 
    And I wait for the form to close
    And I wait for ajax

    #4 - Contrôle de la présence d'un seul objet
    #Then I should found 1 expense created
    Then I should found "1" of "transport"

    #5 - En mode Edition cliquer deux fois sur le bouton "Valider" 
    When I click to "edit" a "transport"
    And I wait for 2 seconds
    And I wait for the form to appear
    When I scroll "EditValiderTransport" into view
    And I press "EditValiderTransport"
    And I press "EditValiderTransport"
    And I wait for the form to close
    And I wait for ajax

    #6 - Contrôle de la présence d'un seul objet
    Then I should found "1" of "transport"