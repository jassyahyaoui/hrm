#features/expense/create_validate.feature
Feature: Create and validate an expense 
  Create and validate an expense
  @javascript
  Scenario: creation/validation d'un objet par un collaborateur valide par un responsable 
    #0 - Se connecter en mode Collaborateur
    Given I am logged using "responsable" and "responsable"
    And I wait for ajax
    #And I select "36" from "company"
    And I select the company of "collaborateur"
    #And I press "_submit"

    When I delete all "bonus"
    When I maximaze window   
    Then I should be on "/dashboard/"

    #1 - Creation d'un objet
    When I click on the link "TabBonus"
    And I wait for ajax
    And I press "btn_new_bonus"
    And I wait for the form to appear
    And I select "Collaborateur Collaborateur" from "bonusbundle_hrm_bonus_contributor"
    And I select "Responsable Responsable" from "bonusbundle_hrm_bonus_validator"
    And I select random from "#bonusbundle_hrm_bonus_settings_bonus_type"
    And I select random from "#bonusbundle_hrm_bonus_settings_bonus_name"
    And I fill random value in field "bonusbundle_hrm_bonus_amount"
    And I fill random value in field "bonusbundle_hrm_bonus_comment"
    #And I fill in the following "10/2017" in field "bonusbundle_hrm_bonus_settings_pay_period"
    Then I fill current month in field "bonusbundle_hrm_bonus_settings_pay_period"

    #2 - Controler les champs
    #valeur par défaut
    And I should see default value current date of field "#bonusbundle_hrm_bonus_date"
    And I should see default value of field validator "#bonusbundle_hrm_bonus_validator"

    #format
    #And I should see format date "m/Y" on field "#bonusbundle_hrm_bonus_settings_pay_period"

    #obligatoire
    #Then I should see all required fields of form "bonus"

    #4 - Sauvegarder en mode Brouillon l'objet
    And I scroll "SauvegarderBonus" into view
    Then I press "SauvegarderBonus"

    #5 - Passer l'objet du statut 
    And I wait for the form to close
    And I wait for the table "bonus" to appear
    And I wait for ajax
    And I wait for ajax
    When I click to "edit" a "bonus"
    And I wait for 2 seconds
    And I wait for the form to appear
    When I scroll "EditValiderBonus" into view
    And I press "EditValiderBonus" 
    And I wait for the form to close
    And I wait for ajax
    And I wait for ajax
    When I click to "show" a "bonus"
    Then I should see "En attente de validation"
    And I press "btn_return_bonus"
    And I wait for ajax

    #6 - Controler en mode lecture que tous les champs sont renseignes
    When I click to "show" a "bonus"
    And I wait for 2 seconds
    Then I should see "Collaborateur Collaborateur"
    And I should see "En attente de validation"
    And I should see current month
    And I wait for 2 seconds

    #7 - Controler en mode liste la presence de l'objet dans la grille de donnees
    And I press "btn_return_bonus"
    And I wait for ajax
    #And I wait for the table to appear

    #8 - Controler les valeurs de l'objet dans la grille de donnees
    Then I should see "Collaborateur Collaborateur"
    And I should see "En attente"

    #9 - Se connecter avec le responsable du collaborateur 
    When I am logged using "responsable" and "responsable"
    When I click on the link "TabBonus"
    And I wait for ajax
    And I wait for 5 seconds

    #10 - Dans le mode Liste selectionner le filtre par statut 
    #1And I press "btn_filter_statut" 
    #And I wait for 20 seconds
    #And I click on the link "btn_filter_status_waiting"
    #And I wait for 2 seconds
    And I press "btn_filter_statut" 
    And I wait for 2 seconds
    #When I click on the text "valid"
    And I click on the link "btn_filter_status_waiting" 
    And I wait for 2 seconds

    #11 - Valider la demande du collaborateur
    #confirm a expense absence
    When I click to "confirm" a "bonus"
    And I wait for ajax
    And I wait for 5 seconds

    #12 - Controler que l'objet est au statut "Valide"
    And I press "btn_filter_statut" 
    And I wait for 2 seconds
    And I click on the link "btn_filter_status_all" 
    And I wait for 2 seconds
    When I click to "show" a "bonus"
    Then I should see "Validé"
    And I press "btn_return_bonus"
    And I wait for 2 seconds
