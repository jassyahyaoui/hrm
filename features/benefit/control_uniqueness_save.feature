#features/benefit/control_uniqueness_save.feature
Feature: CONTROLE UNICITE SAUVEGARDE
  @javascript
  Scenario: CONTROLE UNICITE SAUVEGARDE
    #0 - Se connecter en mode Collaborateur
    Given I am logged using "responsable" and "responsable"
    And I wait for ajax
    And I select the company of "collaborateur"

    When I delete all "benefit"
    When I maximaze window   
    Then I should be on "/dashboard/"
    And I wait for ajax

    #1 - Creation d'un objet
    When I close symfony toolbar
    And I click on the link "TabBenefit"
    And I wait for ajax
    And I press "btn_new_benefit"
    And I wait for the form to appear
    And I select "Collaborateur Collaborateur" from "benefitbundle_hrm_benefit_contributor"
    And I select "Nourriture" from "benefitbundle_hrm_benefit_type"
    And I fill current date in field "benefitbundle_hrm_benefit_date"
    And I select "Responsable Responsable" from "benefitbundle_hrm_benefit_validator"
    And I fill random value in field "benefitbundle_hrm_benefit_realAmount" 
    And I select random from "#benefitbundle_hrm_benefit_hrm_settings_benefit_periodicity"  
    And I fill random value in field "benefitbundle_hrm_benefit_comment" 
    And I fill current date in field "benefitbundle_hrm_benefit_date"

    #2 - Renseigner les champs obligatoires
    Then I should see all required fields of form "benefit"

    #3 - Cliquer deux fois sur le bouton "Sauvegarder"
    When I scroll "SauvegarderBenefit" into view
    And I press "SauvegarderBenefit" 
    And I press "SauvegarderBenefit" 
    And I wait for the form to close
    And I wait for ajax

    #4 - Contrôle de la présence d'un seul objet
    #Then I should found 1 expense created
    Then I should found "1" of "benefit"

    #5 - En mode Edition cliquer deux fois sur le bouton "Valider" 
    When I click to "edit" a "benefit"
    And I wait for 2 seconds
    And I wait for the form to appear
    When I scroll "EditValiderBenefit" into view
    And I press "EditValiderBenefit"
    And I press "EditValiderBenefit"
    And I wait for the form to close
    And I wait for ajax

    #6 - Contrôle de la présence d'un seul objet
    Then I should found "1" of "benefit"