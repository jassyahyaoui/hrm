#features/benefit/create_validate.feature
Feature: Create and validate an benefit
  Create and validate a benefit
  @javascript
  Scenario: creation/validation d'un objet par un collaborateur valide par un responsable 
    #0 - Se connecter en mode Collaborateur
    Given I am logged using "responsable" and "responsable"
    When I delete all "benefit"
    And I select the company of "collaborateur"
    When I maximaze window   
    Then I should be on "/dashboard/"

    #1 - Creation d'un objet
    When I close symfony toolbar
    And I click on the link "TabBenefit"
    And I wait for ajax
    And I press "btn_new_benefit"
    And I wait for the form to appear
    And I select "Collaborateur Collaborateur" from "benefitbundle_hrm_benefit_contributor"
    And I select "Nourriture" from "benefitbundle_hrm_benefit_type"
    And I fill current date in field "benefitbundle_hrm_benefit_date"
    And I select "Responsable Responsable" from "benefitbundle_hrm_benefit_validator"
    And I fill random value in field "benefitbundle_hrm_benefit_realAmount" 
    And I select random from "#benefitbundle_hrm_benefit_hrm_settings_benefit_periodicity"  
    And I fill random value in field "benefitbundle_hrm_benefit_comment" 
    And I fill current date in field "benefitbundle_hrm_benefit_date"

    #2 - Controler les champs
    #valeur par défaut
    And I should see default value of collaborator in field "#benefitbundle_hrm_benefit_contributor"
    #And I should see default value of field validator "#benefitbundle_hrm_benefit_validator"
    And I should see default value current date of field "#benefitbundle_hrm_benefit_date"

    #format
    And I should see format date "d/m/Y" on field "#benefitbundle_hrm_benefit_date"
  
    #obligatoire
    Then I should see all required fields of form "benefit"
    
    #3 - Renseigner l'exhaustivité des champs dans le formulaire
    Then I should see all fields of form "#form_benefit_new" are filled

    #4 - Sauvegarder en mode Brouillon l'objet
    When I scroll "SauvegarderBenefit" into view
    And I press "SauvegarderBenefit" 
    And I wait for the form to close
    And I wait for ajax

    #5 - Passer l'objet du statut 
    When I click to "edit" a "benefit"
    And I wait for 2 seconds
    And I wait for the form to appear
    When I scroll "EditValiderBenefit" into view
    And I press "EditValiderBenefit" 
    And I wait for ajax
    When I click to "show" a "benefit"
    Then I wait for 2 seconds
    Then I should see "En attente de validation"
    And I press "btn_return_benefit"
    And I wait for ajax

    #6 - Controler en mode lecture que tous les champs sont renseignes
    When I click to "show" a "benefit"
    And I wait for 2 seconds
    Then I should see "Collaborateur Collaborateur"
    And I should see "Nourriture"  
    And I should see the current date
    And I should see "Responsable Responsable"  
    And I should see "Responsable Responsable"
    #And I should see "Montant valeur réelle" 
    #And I should see "Périodicité de versement"
    And I should see "En attente de validation"
    #And I should see "Commentaires"
    And I wait for 2 seconds

    #7 - Controler en mode liste la presence de l'objet dans la grille de donnees
    And I press "btn_return_benefit"
    And I wait for ajax
    And I wait for the table "benefit" to appear

    #8 - Controler les valeurs de l'objet dans la grille de donnees
    And I wait for ajax
    Then I should see "Collaborateur Collaborateur"  
    And I should see the current date
    And I should see "Nourriture" 
    And I should see "En attente"

    #9 - Se connecter avec le responsable du collaborateur 

    #10 - Dans le mode Liste selectionner le filtre par statut 
    And I press "btn_filter_statut" 
    And I wait for 2 seconds
    And I click on the link "btn_filter_status_waiting" 
    Then I wait for 2 seconds

    #11 - Valider la demande du collaborateur
    When I click to "confirm" a "benefit"
    And I wait for ajax
    And I wait for 5 seconds

    #12 - Controler que l'objet est au statut "Valide"
    When I press "btn_filter_statut" 
    And I wait for 2 seconds
    And I click on the link "btn_filter_status_all" 
    And I wait for 2 seconds
    And I click to "show" a "benefit"
    Then I should see "Valide"