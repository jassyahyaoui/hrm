#features/RequestAbsence/create_validate.feature
Feature: Create and validate a request absence
  Create a new requet absence 
  @javascript
  Scenario: creation/validation d'un objet par un collaborateur valide par un responsable 
    #0 - Se connecter en mode Collaborateur
    Given I am logged using "collaborateur" and "collaborateur"
    When I delete all "absences"
    And I maximaze window   
    And I wait for ajax
    Then I should be on "/dashboard/"
    And I select the company of "collaborateur"

    #1 - Création d'un objet
    When I click on the link "TabHolidays"
    And I wait for ajax
    And I press "NewAbsenceRequestBtn"
    And I wait for the form to appear
    And I select "Collaborateur Collaborateur" from "requestabsencebundle_requestabsence_employee"
    And I fill current date in field "requestabsencebundle_requestabsence_requestdate"
    And I select "Responsable Responsable" from "requestabsencebundle_requestabsence_manager"
    #And I select "Temps de repos" from "requestabsencebundle_requestabsence[absencetype]"
    And I select random from "#requestabsencebundle_requestabsence_absencetype"  
    And I fill in the following "20/10/2017 08:00 AM" in field "new_startdate"
    And I fill in the following "25/10/2017 05:00 PM" in field "new_enddate"
    And I fill in the following "20/10/2017 08:00 AM" in field "new_startdate"  
    And I fill in the following "TEST_REQUEST_ABSENCE" in field "requestabsencebundle_requestabsence[notes]"
    Then I should see all required fields of form absence
    And I wait for 2 seconds
    
    #2 - Controler les champs
    #valeur par défaut
    Then the "Collaborateur Collaborateur" option from "requestabsencebundle_requestabsence[employee]" should be selected
    And I should see default value of field validator "#requestabsencebundle_requestabsence_manager"

    #2- format
    Then I should see format date
    #Then I should see format date "d/m/Y" on field "new_startdate"
    #Then I should see format date "d/m/Y" on field "new_enddate"

    #2 - obligatoire
    #Then I should see all required fields of form "expensebundle_hrm_expenses_other_SettingsExpenses"

    Then I save as draft
    When I scroll "SauvegarderAbsence" into view
    And I press "SauvegarderAbsence" 
    And I wait for ajax
    And I wait for 2 seconds

    #4 - Sauvegarder en mode Brouillon l'objet
    Then the request absence should be "Brouillon"

    #5 - Passer l'objet du statut 
    When I click to "edit" last request absence
    And I wait for 2 seconds
    And I wait for the form to appear
    When I scroll "EditValiderAbsence" into view
    And I press "EditValiderAbsence" 
    And I wait for ajax
    When I click to "show" last request absence
    Then I wait for 2 seconds
    Then I should see "En attente de validation"
    And I press "btn_return_requestabsence"
    And I wait for ajax

    #6 - Contrôler en mode lecture que tous les champs sont renseignés
    When I click to "show" last request absence
    And I wait for 2 seconds
    Then I should see "collaborateur"
    And I should see the current date
    And I should see "Responsable Responsable"
    And I should see "Collaborateur Collaborateur"
    #And I should see "Temps de repos"
    #And I should see "20/10/2017"
    #And I should see "25/10/2017"
    And I should see "5,50"
    And I should see "En attente de validation"
    And I should see "TEST_REQUEST_ABSENCE"
    And I wait for 2 seconds

    #7 - Contrôler en mode liste la présence de l'objet dans la grille de données
    And I press "btn_return_requestabsence"
    And I wait for ajax
    #And I wait for the table to appear

    #8 - Contrôler les valeurs de l'objet dans la grille de données
    Then I should see "Collaborateur"
    And I should see the current date
    #And I should see "Temps de repos"
    #And I should see "20/10/2017"   
    #And I should see "25/10/2017"
    And I should see "5,50"
    And I should see "En attente"

    #9 - Se connecter avec le responsable du collaborateur 
    When I am logged using "responsable" and "responsable"
    When I click on the link "TabHolidays"
    And I wait for ajax
    And I wait for 5 seconds

    #10 - Dans le mode Liste sélectionner le filtre par statut 
    And I press "btn_filter_statut" 
    And I wait for 2 seconds
    And I click on the link "btn_filter_status_waiting" 
    And I wait for 2 seconds

    #11 - Valider la demande du collaborateur
    When I click to "validate" last request absence
    And I wait for ajax
    And I wait for 5 seconds

    #12 - Contrôler que l'objet est au statut "Valide"
    And I press "btn_filter_statut" 
    And I wait for 2 seconds
    And I click on the link "btn_filter_status_all" 
    And I wait for 2 seconds
    When I click to "show" last request absence
    Then I should see "Valide"
    And I press "btn_return_requestabsence"
    And I wait for 2 seconds