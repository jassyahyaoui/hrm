#features/export/export.feature
Feature: Export
    Export
@javascript
Scenario: Export

    #0 - Se connecter en mode responsable
    Given I am logged using "responsable" and "responsable"
    When I maximaze window  
    And I select the company of "responsable"
    When I close symfony toolbar
    When I scroll to menu "TabExport"
    When I click on the link "TabExport"
    And I wait for ajax

    #1 - Contrôler que l'export des collaborateurs renvoie un fichier valide
    And I press "btn_export"
    And I wait for ajax
    Then  I should download a "csv" file after clicking on "#btn_export_collaborators"
    And I wait for ajax

    #2- Contrôler que l'export des Absences renvoie un fichier valide
    And I press "btn_export"
    And I wait for ajax
    Then  I should download a "zip" file after clicking on "#btn_export_absences"
    And I wait for ajax

    #3 - Contrôler que l'export des EVP renvoie un fichier valide
    And I press "btn_export"
    And I wait for ajax
    Then  I should download a "csv" file after clicking on "#btn_export_expenses"
    And I wait for ajax

    #4 - Contrôler qu'il est possible d'ouvrir une nouvelle période de paie
    When I open payperiod
    And I wait for ajax

    #5 - Contrôler qu'il est possible de fermer la premiere periode de paie
    When I close payperiod
    And I wait for ajax