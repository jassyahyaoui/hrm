#features/hours/create_validate.feature
Feature: Create and validate an hours 
  Create and validate an hours
  @javascript
  Scenario: creation/validation d'un objet par un collaborateur valide par un responsable 
    #0 - Se connecter en mode Collaborateur
    Given I am logged using "responsable" and "responsable"
    And I wait for ajax
    #And I save a screenshot to "shot.png"
    #And I select "36" from "company"
    And I select the company of "collaborateur"
    #And I press "_submit"

    When I delete all "extrahours"
    When I maximaze window   
    Then I should be on "/dashboard/"

    #1 - Creation d'un objet
    When I click on the link "TabHours"
    And I wait for ajax
    And I press "btn_new_extrahour"
    And I wait for the form to appear
    And I select "Collaborateur Collaborateur" from "extrahoursbundle_hrm_additional_hours_contributor"
    And I select "Responsable Responsable" from "extrahoursbundle_hrm_additional_hours_validator"
    And I fill in the following "05/11/2017" in field "extrahoursbundle_hrm_additional_hours_date"
    And I fill in the following "02/11/2017" in field "extrahoursbundle_hrm_additional_hours_date"
    #And I fill current date in field "#extrahoursbundle_hrm_additional_hours_date"

    And I select "Responsable Responsable" from "extrahoursbundle_hrm_additional_hours_validator"
    And I select random from "#extrahoursbundle_hrm_additional_hours_settings_additional_hours_type"
    #And I select "Heures Fabereo" from "extrahoursbundle_hrm_additional_hours_settings_additional_hours_type"
    And I fill random value in field "extrahoursbundle_hrm_additional_hours_hour25" 
    And I fill random value in field "extrahoursbundle_hrm_additional_hours_hour50" 
    And I fill random value in field "extrahoursbundle_hrm_additional_hours_comment"
    And I fill in the following "10/2017" in field "extrahoursbundle_hrm_additional_hours_settings_pay_period"

    #2 - Controler les champs
    #valeur par défaut
    And I should see default value of field validator "#extrahoursbundle_hrm_additional_hours_validator"
    #And I should see default value of field validator "#extrahoursbundle_hrm_additional_hours_requester"
    #And I should see default value current date of field "#extrahoursbundle_hrm_additional_hours_date"

    #format
    And I should see format date "d/m/Y" on field "#extrahoursbundle_hrm_additional_hours_date"
    And I should see format date "m/Y" on field "#extrahoursbundle_hrm_additional_hours_settings_pay_period"

    #2 - Controler les champs
    #Then I should see default values

    #obligatoire
    Then I should see all required fields of form "extrahour"
    When I scroll "SauvegarderExtraHours" into view
    And I press "SauvegarderExtraHours" 
    And I wait for the form to close
    And I wait for ajax

    #4 - Sauvegarder en mode Brouillon l'objet
    #Then I should found the new "extrahour"

    #5 - Passer l'objet du statut 
    When I click to "edit" a "extrahour"
    And I wait for 2 seconds
    And I wait for the form to appear
    #And I fill in the following "10/2017" in field "extrahoursbundle_hrm_additional_hours_settings_pay_period"
    When I scroll "EditValiderExtraHours" into view
    And I press "EditValiderExtraHours" 
    And I wait for ajax
    When I click to "show" a "extrahour"
    Then I wait for 2 seconds
    Then I should see "En attente de validation"
    And I press "btn_return_extrahour"
    And I wait for ajax

    #6 - Controler en mode lecture que tous les champs sont renseignes
    When I click to "show" a "extrahour"
    And I wait for 2 seconds
    Then I should see "Collaborateur Collaborateur"
    And I should see "Responsable Responsable"  
    And I should see "Responsable Responsable"  
    And I should see "En attente de validation"
    #And I should see current month
    And I should see "Collaborateur Collaborateur"
    And I should see "En attente"
    And I wait for 2 seconds

    #7 - Controler en mode liste la presence de l'objet dans la grille de donnees
    And I press "btn_return_extrahour"
    And I wait for ajax
    #And I wait for the table to appear

    #8 - Controler les valeurs de l'objet dans la grille de donnees
    Then I should see "Collaborateur Collaborateur"
    #And I should see "10/2017"
    And I should see "En attente"

    #9 - Se connecter avec le responsable du collaborateur 

    #10 - Dans le mode Liste selectionner le filtre par statut 

    #11 - Valider la demande du collaborateur

    #12 - Controler que l'objet est au statut "Valide"
