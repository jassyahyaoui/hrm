#features/User/create.feature
Feature: B.1 EMPLOYE : FICHE IDENTIFICATION

@javascript
Scenario: B.1 CREATION
    #0 - Se connecter en mode responsable
    Given I am logged using "responsable" and "responsable"
    When I maximaze window  
    And I wait for ajax
    When I click on the link "TabCollaborator"
    And I wait for ajax

    #1 - Création d'un objet
    And I press "btn_new_collaborator"
    And I wait for the form to appear
    And I wait for ajax

    #And I scroll "userbundle_profils_data_employee_birth_date" into view
    #Then I fill current date in field "userbundle_profils_data_employee_birth_date"

    
    

    #And I wait for ajax
    #And I wait for 2 seconds
    #And I fill random value in field "collaborator_new_first_name" 
    #And I fill random value in field "collaborator_new_last_name" 
    #And I fill random email in field "collaborator_new_email" 
    #And I press "btn_new_validate_collaborator"
    #And I wait for ajax
    #When I press "btn_new_collaborator"
    #And I check if the form appear
    #And I wait for the form to appear
    #And I wait for ajax
    #And I wait for 2 seconds
    #And I fill random value in field "collaborator_new_first_name" 
    #And I fill random value in field "collaborator_new_last_name" 
    #And I fill random email in field "collaborator_new_email" 
    #And I press "btn_new_validate_collaborator_contract"
    #And I wait for ajax
    #And I check if the form appear
    #And I fill random value in field "userbundle_employeecontract_contract_number" 
    #And I select random from "#userbundle_employeecontract_empStatus"
    #And I select random from "#userbundle_employeecontract_contractType"
    #When I scroll "btn_edit_validate_contract" into view
    #And I press "btn_edit_validate_contract" 
    #And I wait for the form to close
    #And I wait for ajax


    #2 - Controler les champs
    #valeur par défaut
    Then I should see dafault value "FR" in field "#collaborator_new_country_code"

    And I fill the form "#collaborator_new_form" with random values

    #format
    And I should see format date "d/m/Y" on field "#collaborator_new_birth_date"
    And I should see format email on field "#collaborator_new_email"


    #obligatoire
    Then I should see all required fields of form "employe"

    #3 - Renseigner l'exhaustivité des champs dans le formulaire

    #4 - Sauvegarder en mode Brouillon l'objet
    And I press "btn_new_validate_collaborator_contract"

    #6 - Contrôler en mode lecture que tous les champs sont renseignés

    #7 - Contrôler en mode liste la présence de l'objet dans la grille de données

    #8 - Contrôler les valeurs de l'objet dans la grille de données