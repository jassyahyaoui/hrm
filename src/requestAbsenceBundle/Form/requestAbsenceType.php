<?php

namespace requestAbsenceBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\SecurityContext;
use SettingsBundle\DependencyInjection\SettingsPreferences;
use Doctrine\ORM\EntityRepository;

class requestAbsenceType extends AbstractType {

    private $securityContext;
    private $userService;
    private $companyId;

    public function __construct(SecurityContext $securityContext, SettingsPreferences $userService) {
        $this->securityContext = $securityContext;
        $this->userService = $userService;
        $this->companyId = $this->userService->getEmpData()->getCompany()->getId();
        $this->provider = $this->userService->getEmpData()->getCompany()->getProvider()->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {

        $builder
                ->add('startdate', 'datetime', array(
                    'label' => 'Date de dÃ©but',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'attr' => array(
                        'class' => 'form-control  date-timepicker1',
                        'data-date-format' => 'DD/MM/YYYY HH:mm',
                        'autocomplete' => 'false',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
            )))
                ->add('enddate', 'datetime', array(
                    'label' => 'Date de fin',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy HH:mm',
                    'attr' => array(
                        'class' => 'form-control  date-timepicker1',
                        'data-date-format' => 'DD/MM/YYYY HH:mm',
                        'autocomplete' => 'false',                        
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
            )))
                ->add('requestdate', 'date', array(
                    'label' => 'Date de la demande',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array(
                        'id' => 'datepicker_requestdate',
                        'date-directive' => ''
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
//                ->add('manager', 'entity', array(
//                    'label' => 'Valideur de la demande',
//                    'required' => true,
//                    'attr' => array(
//                        'class' => 'form-control',
//                        'id' => 'id',
//                        'required' => true,
//                    ),
//                    'class' => 'UserBundle\Entity\HrmEmployee',
//                    'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
//                        return $er->createQueryBuilder("u")
//                                ->leftJoin('u.company', 'c')
//                                ->leftJoin('c.provider', 'p')
//                                ->leftJoin('u.user', 'ue')
//                                ->where("ue.roles LIKE '%ROLE_RESPONSABLE%'")
//                                ->AndWhere('c.id = :companyId')
//                                ->setParameter('companyId', $this->companyId);
//                        ;
//                    },
//                    'label_attr' => array(
//                        'class' => 'col-sm-3 control-label no-padding-right asterix')))
            ->add('manager', 'entity', array(
                'label' => 'type',
                'required' => false,
                'class' => 'UserBundle\Entity\HrmEmployee',
//                    'property' => 'display_name',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->where("e.roles LIKE '%RESPONSABLE%'")
                        ->andwhere('u.company = :value')
                        ->setParameter('value', $this->companyId);
                },
            ))
                            
                ->add('requesters', 'text', array(
                    "mapped" => false, 'disabled' => true, 'read_only' => true,
                ))
                ->add('requester', 'entity', array(
                    'empty_value' => 'Sélectionner un demandeur',
                    'label' => 'Demandeur',
                    'disabled' => true,
                    'required' => true,
                    'read_only' => true,
                    'attr' => array(
                        'required' => true,
                        'class' => 'form-control',
                    ),
                    'class' => 'UserBundle\Entity\HrmEmployee',
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
                        return $er->createQueryBuilder("u")
                                ->leftJoin('u.company', 'c')
                                ->leftJoin('c.provider', 'p')
                                ->leftJoin('u.user', 'ue')
                                ->where("ue.roles LIKE '%ROLE_COLLABORATEUR%' OR ue.roles LIKE '%ROLE_RESPONSABLE%'")
                                ->AndWhere('p.id = :companyId')
                                ->setParameter('companyId', $this->companyId);
                    },
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix', 'disabled' => 'true'
            )))
                ->add('employee', 'entity', array(
                    'label' => 'Collaborateur',
                    'required' => true,
                    'class' => 'UserBundle\Entity\HrmEmployee',
                    'attr' => array(
                        'required' => true,
                        'class' => 'form-control',
                    ),
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
                        return $er->createQueryBuilder('u')
                                ->leftJoin('u.company', 'c')
                                ->leftJoin('c.provider', 'p')
                                ->leftJoin('u.user', 'ue')
                                ->where("ue.roles LIKE '%ROLE_COLLABORATEUR%' OR ue.roles LIKE '%ROLE_RESPONSABLE%'")
                                ->AndWhere('c.id = :companyId')
                                ->setParameter('companyId', $this->companyId)
                        ;
                    },
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
            )))
                ->add('absencetype', null, array('label' => 'Type de la demande',
                    'required' => true,
                    'attr' => array('ng-model' => 'absencetype'),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                    ,
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', $this->provider)
                                ->orderBy('t.seqno', 'ASC');
                    },
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
            )))
                ->add('numberdays', 'text', array('required' => true,
                    'label' => 'Nombre de jours',
                    'read_only' => true,
                    'attr' => array(
                        'ng-model' => 'requestabsence.numberdays', 'ng-bind' => 'daydiff()',
                        'id' => 'name',
                        'class' => 'form-control'
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('notes', 'textarea', array('required' => false, 'attr' => array('ng-model' => 'notes'),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right'
            )))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'requestAbsenceBundle\Entity\requestAbsence',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'requestabsencebundle_requestabsence';
    }

}
