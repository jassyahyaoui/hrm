<?php

namespace requestAbsenceBundle\Service;

use requestAbsenceBundle\Entity\requestAbsence;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

class requestAbsenceService {

    protected $em;
    private $container;

    public function __construct(EntityManager $entityManager, Container $container) {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function getRequestAbsence() {
        $companyId = 1; // $this->container->get('security.context')->getToken()->getUser()->getCompany()->getId();
        $ManagerId = 1; // $this->container->get('security.context')->getToken()->getUser()->getCompany()->getProvider()->getId();
        $qb = $this->em->createQueryBuilder();
        $qb->select('a,at,s,e,m,r,att,pp,cu')
                ->from('requestAbsenceBundle:requestAbsence', 'a')
                ->leftJoin('a.absencetype', 'at')
                ->leftJoin('a.createuid', 'cu')
                ->leftJoin('a.status', 's')
                ->leftJoin('a.employee', 'e')
                ->leftJoin('e.company', 'c')
                ->leftJoin('a.manager', 'm')
                ->leftJoin('a.requester', 'r')
                ->leftJoin('a.attachments', 'att')
                ->leftJoin('a.settings_pay_period', 'pp')
                ->andWhere('s.display_name != :value OR m.id = :Manager')
                ->setParameter('value', 'Brouillon')
                ->AndWhere('c.id = :company')
                ->setParameter('company', $companyId)
                ->setParameter('Manager', $ManagerId)
                ->orderBy('a.id', 'ASC')
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
        return $qb->getQuery()->getResult();
    }

    public function getNbrRequestAbsence() {
        $requestabsences = $this->getRequestAbsence();
        return count($requestabsences);
    }
    
     public function getLastRequestAbsence() {
        $qb = $this->em->createQueryBuilder();
        $qb->select('a,at,s,e,m,r,att,pp,cu')
                ->from('requestAbsenceBundle:requestAbsence', 'a')
                ->leftJoin('a.absencetype', 'at')
                ->leftJoin('a.createuid', 'cu')
                ->leftJoin('a.status', 's')
                ->leftJoin('a.employee', 'e')
                ->leftJoin('e.company', 'c')
                ->leftJoin('a.manager', 'm')
                ->leftJoin('a.requester', 'r')
                ->leftJoin('a.attachments', 'att')
                ->leftJoin('a.settings_pay_period', 'pp')
                ->orderBy('a.id', 'DESC')
                ->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult();
        
    }
    
      public function deleteAllRequestAbsence() {
               $qb = $this->em->createQuery('DELETE requestAbsenceBundle:requestAbsence p')->execute();
    }
    
       public function findRequestAbsence($notes) {
        $qb = $this->em->createQueryBuilder();
        $qb->select('a,at,s,e,m,r,att,pp,cu')
                ->from('requestAbsenceBundle:requestAbsence', 'a')
                ->leftJoin('a.absencetype', 'at')
                ->leftJoin('a.createuid', 'cu')
                ->leftJoin('a.status', 's')
                ->leftJoin('a.employee', 'e')
                ->leftJoin('e.company', 'c')
                ->leftJoin('a.manager', 'm')
                ->leftJoin('a.requester', 'r')
                ->leftJoin('a.attachments', 'att')
                ->leftJoin('a.settings_pay_period', 'pp')
                ->andWhere('a.notes = :notes')
                ->setParameter('notes', $notes)
                ->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
        return $qb->getQuery()->getSingleResult();
    }

}
