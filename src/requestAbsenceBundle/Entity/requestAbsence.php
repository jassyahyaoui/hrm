<?php

namespace requestAbsenceBundle\Entity;
use Doctrine\ORM\Mapping as ORM;

/**
 * requestAbsence
 */
class requestAbsence {

    /**
     * @var int
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $requestdate;
    
     /**
     * @var string
     */
    private $absencetype;
    
    /**
     * @var \DateTime
     */
    private $startdate;

    /**
     * @var \DateTime
     */
    private $enddate;

    /**
     * @var string
     */
    private $notes;
    
    /**
     * @var float
     */
    private $numberdays;

    /**
     * @var \AdminBundle\Entity\Status
     */
    private $status;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $employee;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $manager;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $requester;

 
    /**
     * @var \FileBundle\Entity\attachments
     */
    private $attachments;
     
    /**
      * @var \UserBundle\Entity\HrmEmployee
     */
    private $createuid;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $lastupdateuid;
     
    /**
     * @var \DateTime
     */
    private $lastupdate;

    /**
     * @var \DateTime
     */
    private $createdate;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }
    
    /**
     * Set requestdate
     *
     * @param \DateTime $requestdate
     *
     * @return requestAbsence
     */
    public function setRequestdate($requestdate)
    {
        $this->requestdate = $requestdate;

        return $this;
    }

    /**
     * Get requestdate
     *
     * @return \DateTime
     */
    public function getRequestdate()
    {
        return $this->requestdate;
    }

    /**
     * Set startdate
     *
     * @param \DateTime $startdate
     *
     * @return requestAbsence
     */
    public function setStartdate($startdate)
    {
        $this->startdate = $startdate;

        return $this;
    }

    /**
     * Get startdate
     *
     * @return \DateTime
     */
    public function getStartdate()
    {
        return $this->startdate;
    }

    /**
     * Set enddate
     *
     * @param \DateTime $enddate
     *
     * @return requestAbsence
     */
    public function setEnddate($enddate)
    {
        $this->enddate = $enddate;

        return $this;
    }

    /**
     * Get enddate
     *
     * @return \DateTime
     */
    public function getEnddate()
    {
        return $this->enddate;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return requestAbsence
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

   
    /**
     * Set lastupdate
     *
     * @param \DateTime $lastupdate
     *
     * @return requestAbsence
     */
    public function setLastupdate($lastupdate)
    {
        $this->lastupdate = $lastupdate;

        return $this;
    }

    /**
     * Get lastupdate
     *
     * @return \DateTime
     */
    public function getLastupdate()
    {
        return $this->lastupdate;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     *
     * @return requestAbsence
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }




    /**
     * Set status
     *
     * @param \AdminBundle\Entity\SettingsStatus  $status
     *
     * @return requestAbsence
     */
    public function setStatus(\AdminBundle\Entity\SettingsStatus  $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AdminBundle\Entity\SettingsStatus 
     */
    public function getStatus()
    {
        return $this->status;
    }

    public function __construct()
{
    $this->createdate = new \DateTime();
    $this->requestdate = new \DateTime();
}

     /**
     * Set employee
     *
     * @param \UserBundle\Entity\HrmEmployee $employee
     *
     * @return requestAbsence
     */
    public function setEmployee(\UserBundle\Entity\HrmEmployee $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set manager
     *
     * @param \UserBundle\Entity\HrmEmployee $manager
     *
     * @return requestAbsence
     */
    public function setManager(\UserBundle\Entity\HrmEmployee $manager = null)
    {
        $this->manager = $manager;

        return $this;
    }

    /**
     * Get manager
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * Set requester
     *
     * @param \UserBundle\Entity\HrmEmployee $requester
     *
     * @return requestAbsence
     */
    public function setRequester(\UserBundle\Entity\HrmEmployee $requester = null)
    {
        $this->requester = $requester;

        return $this;
    }

    /**
     * Get requester
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getRequester()
    {
        return $this->requester;
    }

    /**
     * Set absencetype
     *
     * @param \AdminBundle\Entity\SettingsAbsences $absencetype
     *
     * @return requestAbsence
     */
    public function setAbsencetype(\AdminBundle\Entity\SettingsAbsences $absencetype = null)
    {
        $this->absencetype = $absencetype;

        return $this;
    }

    /**
     * Get absencetype
     *
     * @return \AdminBundle\Entity\SettingsAbsences
     */
    public function getAbsencetype()
    {
        return $this->absencetype;
    }

    /**
     * Set attachments
     *
     * @param \FileBundle\Entity\attachments $attachments
     *
     * @return requestAbsence
     */
    public function setAttachments(\FileBundle\Entity\attachments $attachments = null)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Get attachments
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

 

    /**
     * Set lastupdateuid
     *
     * @param \UserBundle\Entity\HrmEmployee $lastupdateuid
     *
     * @return requestAbsence
     */
    public function setLastupdateuid(\UserBundle\Entity\HrmEmployee $lastupdateuid = null)
    {
        $this->lastupdateuid = $lastupdateuid;

        return $this;
    }

    /**
     * Get lastupdateuid
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getLastupdateuid()
    {
        return $this->lastupdateuid;
    }
    /**
     * @var \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    private $settings_pay_period;


    /**
     * Set settingsPayPeriod
     *
     * @param \PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod
     *
     * @return requestAbsence
     */
    public function setSettingsPayPeriod(\PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod = null)
    {
        $this->settings_pay_period = $settingsPayPeriod;

        return $this;
    }

    /**
     * Get settingsPayPeriod
     *
     * @return \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    public function getSettingsPayPeriod()
    {
        return $this->settings_pay_period;
    }

    /**
     * Set numberdays
     *
     * @param float $numberdays
     *
     * @return requestAbsence
     */
    public function setNumberdays($numberdays)
    {
        $this->numberdays = $numberdays;

        return $this;
    }

    /**
     * Get numberdays
     *
     * @return float
     */
    public function getNumberdays()
    {
        return $this->numberdays;
    }

    /**
     * Set createuid
     *
     * @param \UserBundle\Entity\HrmEmployee $createuid
     *
     * @return requestAbsence
     */
    public function setCreateuid(\UserBundle\Entity\HrmEmployee $createuid = null)
    {
        $this->createuid = $createuid;

        return $this;
    }

    /**
     * Get createuid
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getCreateuid()
    {
        return $this->createuid;
    }
}
