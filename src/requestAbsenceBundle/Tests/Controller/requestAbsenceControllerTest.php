<?php

namespace requestAbsenceBundle\Tests\Controller;

use Goutte\Client;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use requestAbsenceBundle\Controller\requestAbsenceController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\BrowserKit\Cookie;

class requestAbsenceControllerTest extends WebTestCase {

    private $client = null;

    public function setUp() {
        $this->client = static::createClient();
    }

    public function doLogin($username, $password) {
        $crawler = $this->client->request('GET', '/login');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());

        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => $username,
            '_password' => $password,
        ));
        $this->client->submit($form);
        $this->assertTrue($this->client->getResponse()->isRedirect());
        $crawler = $this->client->followRedirect();
    }

    public function testRequestAbsence() {
        $this->doLogin("responsable", "responsable");
        $crawler = $this->client->request('GET', '/dashboard/');
//        $this->assertTrue($crawler->filter("html:contains('New')")->count() > 0);
//        $this->assertTrue($crawler->filter("html:contains('AjouterAbsence')")->count() > 0);
        $link_save = $crawler->selectLink('REQUEST_ABSENCE_BUTTON_VALIDATE')->link();
        $form = $crawler->selectButton('submit')->form();    // var_dump($form);
        $this->assertNotEmpty($form);                        // tester n'est pas vide
        //$key = substr(hash('sha512',rand()),0,12);           // Generer un code rand
        $form->setValues(array(
            'requestabsencebundle_requestabsence[startdate]' => "15/08/2017 12:00 AM",
            'requestabsencebundle_requestabsence[enddate]' => "26/08/2017 12:00 AM",
            'requestabsencebundle_requestabsence[employee]' => '1',
            'requestabsencebundle_requestabsence[requestdate]' => "15/08/2017",
            'requestabsencebundle_requestabsence[manager]' => '1',
            'requestabsencebundle_requestabsence[absencetype]' => '20',
            'requestabsencebundle_requestabsence[numberdays]' => '11',
            'requestabsencebundle_requestabsence[notes]' => 'TESTABSENCE'
        ));
        $values = $form->getValues();                      // recuperer les valuers de formulaire
        $this->client->click($link_save);

        $crawler = $this->client->request('POST', '/request/api/requestabsence', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'CONTENT_TYPE' => 'application/json',
                ), '{
	"absencetype": "20",
	"action": "valider",
	"employee": "2",
	"enddate": "26/08/2017 12:00 AM",
	"manager": "1",
	"notes": "TESTABSENCE",
	"numberdays": 11,
	"requestdate": "15/08/2017",
	"requester": "collaborateur",
	"startdate": "15/08/2017 12:00 AM"}');

        $crawler = $this->client->request('GET', '/api/requestabsence');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode(),"Unable to get list of absence request");
        $data = json_decode($this->client->getResponse()->getContent(), true);
        $valid = false;
        $id_request = null;  echo $id_request;
        foreach ($data["requestabsences"] as $item) { 
            if (strcmp($item["notes"], "TESTABSENCE") == 0) {
                $valid = true;
                $id_request = $item["id"];
                break;
            }
        }
        $this->assertTrue($valid, "Error ! The request absence has not been saved.");
        $crawler = $this->client->request( 'DELETE','/api/requestabsence/'.$id_request, array(),  array(),    array());
        
//        var_dump($crawler);
        $this->assertSame(200, $this->client->getResponse()->getStatusCode(),"Unable to delete a absence request");
        
    }
}
