<?php

namespace requestAbsenceBundle\Controller;

use requestAbsenceBundle\Entity\requestAbsence;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use FOS\RestBundle\View\View;
use FOS\RestBundle\View\ViewHandler;
use FileBundle\Entity\attachments;
use ExpenseBundle\Repository\hrm_attachmentsRepository;
use requestAbsenceBundle\Form\requestAbsenceType;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\BrowserKit\Response as rep;
use Symfony\Component\Validator\Constraints\DateTime;
use PayPeriodBundle\model\PayPeriod;

/**
 * Requestabsence controller.
 *
 */
class requestAbsenceController extends Controller {

    public function statusInsertion($status = null, $entity) {
        $date = null;
        if ($status == "2") {
            $LastUpdateDate = new \DateTime("now");
            $date = $LastUpdateDate->getTimestamp();
        } else {
            $entity->setSettingsPayPeriod(null);
        }
        return $date;
    }

    /*  public function openORcloseAction($status = null, $entity, $entity2, $em) {
      $date = $this->statusInsertion($status, $entity);
      if (!empty($entity2)) {
      foreach ($entity2 as $key => $value) {
      $IdPayPeriod[$key] = $value->getId();
      $start[$key] = $value->getStartDate();
      $end[$key] = $value->getEndDate();
      $dateStart = $start[$key]->getTimestamp();
      $dateEnd = $end[$key]->getTimestamp();
      if ($dateStart <= $date && $dateEnd >= $date) {
      $entity->setSettingsPayPeriod($em->getRepository('PayPeriodBundle:SettingsPayPeriod')->find($IdPayPeriod[$key]));
      }
      }
      }
      } */

    /**
     *
     * @param type $em
     * @param type $entity
     * @param type $payPeriodDate celui envoyÃ© depuis from
     * @return boolean/object
     */
    private function setOpenedSettingsPayPeriod($em, $entity, $payPeriodDate = null) {
        $ret = '';
        if ($payPeriodDate == null) {
            $searchBy = array("companyId" => $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId(),
                "status" => "open");
        } else {
            $searchBy = array("companyId" => $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId());
        }
        $payperiodlist = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')
                ->findBy($searchBy, array('startDate' => 'ASC'));
        if ($payperiodlist) {

            if ($payPeriodDate == null) {
                $entity->setSettingsPayPeriod($payperiodlist[0]);
                $ret = true;
            } else {
                foreach ($payperiodlist as $key => $value) {
                    if ($value->getStartDate()->format('m/Y') == $payPeriodDate && ($value->getStatus() === 'open')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = true;
                        break;
                    }
                    if (($value->getStartDate()->format('m/Y') === $payPeriodDate ) && ($value->getStatus() === 'close')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = false;
                        break;
                    }
                }
            }
            if ($ret === true)
                return true;
            if ($ret === false)
                return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_CLOSED_PAY_PERIOD"));
            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_NOT_PAY_PERIOD"));
        }
        else {
            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_EMPTY_PAY_PERIOD"));
        }
    }

    public function layoutAction() {
        return $this->render('requestabsence/requestabsence_layout.html.twig');
    }

    /**
     * Lists all requestAbsence entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $requestAbsences = $em->getRepository('requestAbsenceBundle:requestAbsence')->findAll();
        return $this->render('requestabsence/index.html.twig', array(
                    'requestAbsences' => $requestAbsences,
        ));
    }

    public function getrequestabsenceAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $provider_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $employee_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getId();
        if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
            $requestabsences = $em->getRepository('requestAbsenceBundle:requestAbsence')->FindByCreator($employee_id);
        } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
            $requestabsences = $em->getRepository('requestAbsenceBundle:requestAbsence')->FindByMangerNCompanyId($company_id, $employee_id);
        } else {
            $requestabsences = null;
        }
        $absences_type = $em->getRepository('AdminBundle\Entity\SettingsAbsences')->SettingsAbsencesByProvider($provider_id);
        $status = $this->getDoctrine()->getManager()->getRepository('AdminBundle\Entity\SettingsStatus')->SettingsStatusByProvider($provider_id);
        $users = $em->getRepository('UserBundle:HrmEmployee')->FindCollaborateurByCompanyId($company_id);
        return array(
            'requestabsences' => $requestabsences,
            'users' => $users,
            'absences_type' => $absences_type,
            'status' => $status,
        );
    }

    public function calendarAction() {
        return $this->render('requestabsence/calendrier.html.twig');
    }

    public function getcalendarAction(Request $request) {
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
            $user = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $absences_request = $this->get('doctrine.orm.entity_manager')
                    ->getRepository('requestAbsenceBundle:requestAbsence')
                    ->findAll();
        } else {
            $user = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $absences_request = $this->get('doctrine.orm.entity_manager')
                    ->getRepository('requestAbsenceBundle:requestAbsence')
                    ->findByRequester($user);
        }
        $absence_request_array = [];
        foreach ($absences_request as $absence_request) {
            $absence_request_array[] = [
                'title' => $absence_request->getAbsencetype()->getType(),
                'start' => date_format($absence_request->getStartDate(), 'm/d/Y h:i'),
                'end' => date_format($absence_request->getEndDate(), 'm/d/Y h:i'),
            ];
        }
        return $absence_request_array;
    }

    /**
     * @Rest\View(statusCode=Response::HTTP_CREATED)
     * @Rest\Post("/api/requestabsence")
     */
    public function postrequestabsenceAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, true);
        $securityContext = $this->container->get('security.context');
        $userService = $this->container->get('settingsbundle.preference.service');
        $currentProfile =  $userService->getEmpData();
        $requestabsence = new Requestabsence();
//        $local = $userService->getEmpData()->getPreference()->getLocal();
        $form = $this->createForm(new requestAbsenceType($securityContext, $this->container->get('settingsbundle.preference.service')), $requestabsence);
        if ($request->isMethod('POST')) {
            $form->submit($data);
            $requestabsence->setCreateuid($currentProfile);
            $securityContext = $this->container->get('security.authorization_checker');
            if (isset($data['employee']))
            $requestabsence->setEmployee($em->getRepository('UserBundle:HrmEmployee')->find($data['employee']));
            if (isset($data['manager']))
            $requestabsence->setManager($em->getRepository('UserBundle:HrmEmployee')->find($data['manager']));
            $requestabsence->setRequester($currentProfile);
            if ($request->request->has('attachments')) {
                $attachment = new attachments();
                if (isset($data['attachments']['path']))
                    $attachment->setPath($data['attachments']['path']);
                if (isset($data['attachments']['mimetype']))
                    $attachment->setMimetype($data['attachments']['mimetype']);
                $attachment->preUpload();
                $attachment->upload();
                $em->persist($attachment);
                $em->flush($attachment);
                $requestabsence->setAttachments($attachment);
            }
            $action = $data['action'];
            if ($action == "valider") {
                $requestabsence->setStatus($em->getRepository('AdminBundle:SettingsStatus')->find(1));
                $TranslateMail = $this->get('mailingbundle.email.send.service');
                $this->get('mailingbundle.email.send.service')->Notify2($requestabsence, 'COLLABORATEUR', $TranslateMail->TagsTranslateRequestAbsence('titleNew'), $TranslateMail->TagsTranslateRequestAbsence('etatNew'), $TranslateMail->TagsTranslateRequestAbsence('typeRequest'), $TranslateMail->TagsTranslateRequestAbsence('folder'), $TranslateMail->TagsTranslateRequestAbsence('viewName'));
            } else if ($action == "sauvegarder") {
                $requestabsence->setStatus($em->getRepository('AdminBundle:SettingsStatus')->find(4));
            }
            // try to add first open one of pay period
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $requestabsence);
            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }
            $em->persist($requestabsence);
            $em->flush();
            return new Response("200");
        }
        return new Response("200");
    }

    function createFromFormat($format, $time) {
        $is_pm = (stripos($time, 'PM') !== false);
        $time = str_replace(array('AM', 'PM'), '', $time);
        $format = str_replace('A', '', $format);
        $date = DateTime::createFromFormat(trim($format), trim($time));
        if ($is_pm) {
            $date->modify('+12 hours');
        }
        return $date;
    }

    public function deleterequestabsenceAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $demandeconge = $em->getRepository('requestAbsenceBundle:requestAbsence')
                ->find($request->get('id'));
        if ($demandeconge) {
            if (($demandeconge->getStatus()->getId() != "2") || ($demandeconge->getStatus()->getId() != "3")) {
                $em->remove($demandeconge);
                $em->flush();
            }
            return new Response(200);
        }
        return new Response(400);
    }

    public function putrequestabsenceAction(Request $request, requestAbsence $requestAbsence) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, true);
        $entity = $em->getRepository('requestAbsenceBundle:requestAbsence')->find($data['id']);
        $securityContext = $this->container->get('security.context');
        $userService = $this->container->get('settingsbundle.preference.service');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $update_form = $this->createForm(new requestAbsenceType($securityContext, $userService), $entity);
        if ($request->isMethod('PUT')) {
            $update_form->submit($data);
            $currentProfile = $em->getRepository('UserBundle:HrmEmployee')->find($userService->getEmpData()->getId());
            $entity->setRequester($currentProfile);
            if ($request->request->has('attachments')) {
                if ($data['attachments'] != null) {
                    if ($request->request->has('attachments')) {
                        if ($entity->getAttachments() != null) {
                            $attachment = $entity->getAttachments();
                        } else
                            $attachment = new attachments();
                        $attachment->setPath($data['attachments']['path']);
                        if ((isset($data['attachments']['mimetype'])) && ($data['attachments']['mimetype'] != null))
                            $attachment->setMimetype($data['attachments']['mimetype']);
                        $em->persist($attachment);
                        $em->flush($attachment);
                        $entity->setAttachments($attachment);
                    }
                }
                else {
                    $entity->setAttachments(null);
                }
            } else if ((!is_string($request->request->get('attachments'))) || (is_null($request->request->get('attachments'))))
                $entity->setAttachments(null);
            if ($request->request->get('action') == "valider") {
                if ($entity->getStatus()->getId() == 4)
                    $entity->setStatus($em->getRepository('AdminBundle:SettingsStatus')->find(1));
                
                  if ($entity->getStatus()->getId() == 2)
                    $entity->setStatus($em->getRepository('AdminBundle:SettingsStatus')->find(1));
                
            } else if ($request->request->get('action') == "sauvegarder") {
                if ($entity->getStatus()->getId() == 1)
                    $entity->setStatus($em->getRepository('AdminBundle:SettingsStatus')->find(4));
            }
            // try to add first open one of pay period
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $entity);
            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }
            $em->persist($entity);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('Les donnÃ©es sont enregistrÃ©s', 201);
            return $response;
        }
        $response = new \Symfony\Component\BrowserKit\Response('Un problÃ¨me inattendu est survenu', 409);
    }

    public function confirmabsencerequestAction(Request $request) {
        $em = $this->get('doctrine.orm.entity_manager');
        $requestabsence = $em->getRepository('requestAbsenceBundle:requestAbsence')
                ->find($request->get('id'));
        if ($requestabsence) {
            // try to add first open one of pay period
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $requestabsence);
            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }
            if ($request->get('action') == "annuler") {
                $requestabsence->setStatus($em->getRepository('AdminBundle:SettingsStatus')->find(4));
                $em->persist($requestabsence);
                $em->flush();
                return new Response("200");
            }
            if ($request->get('action') == "valider") {
                $requestabsence->setStatus($em->getRepository('AdminBundle:SettingsStatus')->find(2));
            }
            if ($request->get('action') == "refuser") {
                $requestabsence->setStatus($em->getRepository('AdminBundle:SettingsStatus')->find(3));
            }
            $em->persist($requestabsence);
            $em->flush();
            $TranslateMail = $this->get('mailingbundle.email.send.service');
            $this->get('mailingbundle.email.send.service')->Notify2($requestabsence, 'RESPONSABLE', $TranslateMail->TagsTranslateRequestAbsence('confirmMsg'), $TranslateMail->TagsTranslateRequestAbsence('confirmEtat'), $TranslateMail->TagsTranslateRequestAbsence('typeRequest'), $TranslateMail->TagsTranslateRequestAbsence('folder'), $TranslateMail->TagsTranslateRequestAbsence('viewName'));
            return new Response("200");
        }
        return new Response("400");
    }

    public function getrequestabsencAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $absence_request = $em->getRepository('requestAbsenceBundle:requestAbsence')->find($request->get('id'));
        return $absence_request;
    }

    /**
     * Creates a new requestAbsence entity.
     *
     */
    public function newmodalAction(Request $request) {
        $securityContext = $this->container->get('security.context');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $requestAbsence = new Requestabsence();
        $securityContext = $this->container->get('security.context');
        $form = $this->createForm(new requestAbsenceType($securityContext), $requestAbsence);
        $form->handleRequest($request);
        return $this->render('requestabsence/modal_new.html.twig', array(
                    'requestAbsence' => $requestAbsence,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new requestAbsence entity.
     *
     */
    public function showmodalAction(requestAbsence $requestAbsence) {
        $deleteForm = $this->createDeleteForm($requestAbsence);
        return $this->render('requestabsence/modal_show.html.twig', array(
                    'requestAbsence' => $requestAbsence,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Creates a new requestAbsence entity.
     *
     */
    public function newAbsenceAction(Request $request) {
        $securityContext = $this->container->get('security.context');
        $userService = $this->container->get('settingsbundle.preference.service');
        $requestAbsence = new Requestabsence();
        $form = $this->createForm(new requestAbsenceType($securityContext, $userService), $requestAbsence);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($requestAbsence);
            $em->flush($requestAbsence);
            return $this->redirectToRoute('requestabsence_show', array('id' => $requestAbsence->getId()));
        }
        return $this->render('requestabsence/new.html.twig', array(
                    'requestAbsence' => $requestAbsence,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new requestAbsence entity.
     *
     */
    public function newAction(Request $request) {
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $user = $this->get('security.context')->getToken()->getUser();
            return new Response($this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany());
            $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        } else
            $company_id = 0;
        $requestAbsence = new Requestabsence();
        $form = $this->createForm(new requestAbsenceType($company_id), $requestAbsence);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($requestAbsence);
            $em->flush($requestAbsence);

            return $this->redirectToRoute('requestabsence_show', array('id' => $requestAbsence->getId()));
        }
        return $this->render('requestabsence/new.html.twig', array(
                    'requestAbsence' => $requestAbsence,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a requestAbsence entity.
     *
     */
    public function showAction(requestAbsence $requestAbsence) {
        $deleteForm = $this->createDeleteForm($requestAbsence);
        return $this->render('requestabsence/show.html.twig', array(
                    'requestAbsence' => $requestAbsence,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing requestAbsence entity.
     *
     */
    public function editmodalAction(Request $request) {
        $requestAbsence = new Requestabsence();
        $securityContext = $securityContext = $this->container->get('security.context');
        $user_service = $this->container->get('settingsbundle.preference.service');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED'))
            $editForm = $this->createForm(new requestAbsenceType($securityContext, $user_service), $requestAbsence);
        $editForm->handleRequest($request);
        return $this->render('requestabsence/edit.html.twig', array(
                    'requestAbsence' => $requestAbsence,
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing requestAbsence entity.
     *
     */
    public function editAction(Request $request, requestAbsence $requestAbsence) {
        $deleteForm = $this->createDeleteForm($requestAbsence);
        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        }
        $editForm = $this->createForm(new requestAbsenceType($company_id), $requestAbsence);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('requestabsence_edit', array('id' => $requestAbsence->getId()));
        }
        return $this->render('requestabsence/edit.html.twig', array(
                    'requestAbsence' => $requestAbsence,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a requestAbsence entity.
     *
     */
    public function deleteAction(Request $request, requestAbsence $requestAbsence) {
        $form = $this->createDeleteForm($requestAbsence);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($requestAbsence);
            $em->flush();
        }
        return $this->redirectToRoute('requestabsence_index');
    }

    /**
     * Creates a form to delete a requestAbsence entity.
     *
     * @param requestAbsence $requestAbsence The requestAbsence entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(requestAbsence $requestAbsence) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('requestabsence_delete', array('id' => $requestAbsence->getId())))
                        ->setMethod('DELETE')
                        ->getForm();
    }

    public function getabsencetypesAction(Request $request) {
        $absencetypes = $this->getDoctrine()->getManager()->getRepository('AdminBundle:SettingsAbsences')->findAll();
        foreach ($bonustypes as $bonustype) {
            $type = $bonustype->getId();
            $em = $this->getDoctrine()->getManager();
            $qb = $em->createQueryBuilder();
            $nbjoursprisregime = $qb->select('b.display_name AS name,  b.id')
                    ->from('AdminBundle:SettingsBonus', 'b')
                    ->from('AdminBundle:SettingsBonusType', 't')
                    ->innerJoin('b.bonus_type', 'pt')
                    ->where("pt.id = :type")
                    ->setParameter('type', $type)
                    ->groupBy('b.display_name, b.id')
                    ->getQuery()
                    ->getArrayResult();
            $absencetypes_array[$bonustype->getId()] = array_values($nbjoursprisregime);
        }
        return $absencetypes_array;
        return $absencetypes;
    }

    public function DashboardCongeAction() {
        $em = $this->getDoctrine()->getManager();
        $requestAbsence = new Requestabsence();
        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getId();

        if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
            $conge = $em->getRepository('requestAbsenceBundle:requestAbsence')->FindByCreator($current_user_id);
        } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
            $company_id = $em->getRepository('UserBundle:HrmEmployee')->find($current_user_id)->getCompany()->getCompanyId();
            $conge = $em->getRepository('requestAbsenceBundle:requestAbsence')->FindByMangerNCompanyId($company_id, $current_user_id);
        } else {
            $conge = null;
        }
        return array('conge' => $conge);
    }

    public function CancelAction(Request $request, Requestabsence $requestAbsence) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, true);
        $requestAbsence->setStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['cancel']));
        $entity = $em->getRepository('requestAbsenceBundle:requestAbsence')->find($requestAbsence);
        $this->statusInsertion($data['cancel'], $requestAbsence);
        /* $SettingsPayPeriod = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')->findAll();
          $this->openORcloseAction($data['cancel'], $requestAbsence, $SettingsPayPeriod, $em); */
        //Mail notifier to Collaborateur
        $requestabsence = $entity;
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify2($entity, 'RESPONSABLE', $TranslateMail->TagsTranslateRequestAbsence('cancelMsg'), $TranslateMail->TagsTranslateRequestAbsence('cancelEtat'), $TranslateMail->TagsTranslateRequestAbsence('typeRequest'), $TranslateMail->TagsTranslateRequestAbsence('folder'), $TranslateMail->TagsTranslateRequestAbsence('viewName'));
        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $requestabsence);
        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }
        $em->persist($requestAbsence);
        $em->flush();
        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }
    
     public function ConfirmAction(Request $request, Requestabsence $requestAbsence) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, true);
        $requestAbsence->setStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['confirm']));
        $entity = $em->getRepository('requestAbsenceBundle:requestAbsence')->find($requestAbsence);
        $requestabsence = $entity;
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify2($entity, 'RESPONSABLE', $TranslateMail->TagsTranslateRequestAbsence('cancelMsg'), $TranslateMail->TagsTranslateRequestAbsence('cancelEtat'), $TranslateMail->TagsTranslateRequestAbsence('typeRequest'), $TranslateMail->TagsTranslateRequestAbsence('folder'), $TranslateMail->TagsTranslateRequestAbsence('viewName'));
        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $requestabsence);
        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }
        $em->persist($requestAbsence);
        $em->flush();
        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function removeAction(Request $request, Requestabsence $requestAbsence) {
        $form = $this->createDeleteForm($requestAbsence);
        $form->handleRequest($request);
        if ($request->isMethod('DELETE')) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($requestAbsence);
            $em->flush($requestAbsence);
            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }

    public function allStaticDataOfAbsencesDonutChartAction($_period = null) {
        $em = $this->getDoctrine()->getManager();
        $current_user_id = $this->get('security.context')->getToken()->getUser();
        $securityContext = $this->container->get('security.authorization_checker');
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $result = array("valid" => NULL, "waiting" => NULL, "draft" => NULL);
        $date_period = new \DateTime($_period);
        $date_period = $date_period->modify('first day of this month');
        $e_payPeriod = new PayPeriod();
        $obj_Payperiod = $e_payPeriod->getObjectPayPeriod($date_period, $this->container, $em, $current_user_id);
        if (is_object($obj_Payperiod)) {
            if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $result = $em->getRepository('requestAbsenceBundle:requestAbsence')->AbsencesStaticDataFindStaticDByUserCompanyId($company_id, $obj_Payperiod->getId());
                $final = array();
                array_walk_recursive($result, function($item, $key) use (&$final) {
                    $final[$key] = isset($final[$key]) ? $item + $final[$key] : $item;
                });
                $result = $final;
            }
        }
        return array('result' => $result /* , 'users' => $users */);
    }

    public function allStaticDataOfAbsencesBarChartAction($_year = null) {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $preference = ((is_null($this->container->get('settingsbundle.preference.service')->getEmpData()->getPreference()))? 'fr': $this->container->get('settingsbundle.preference.service')->getEmpData()->getPreference()->getFormat());
        if (( $_year == null ) || ( strtotime($_year) === false )) {
            $_year = new \DateTime();
            $_year = $_year->format('Y');
        }
        $all_months_year = array();
        $result = array();
        for ($iM = 1; $iM <= 12; $iM++) {
            if ($iM < 10)
                $iM = '0' . $iM;
            $all_months_year[] = array('month' => $_year . '-' . $iM, "valid" => 0, "waiting" => 0);
        }
        if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
            $result = $em->getRepository('requestAbsenceBundle:requestAbsence')->AbsencesStaticDataOfBarChartsFindByUserCompanyId($company_id, $_year);
        }else if($securityContext->isGranted('ROLE_COLLABORATEUR')){
            $result = $em->getRepository('requestAbsenceBundle:requestAbsence')->AbsencesStaticDataOfBarChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $_year, FALSE);
        }
        
        foreach ($all_months_year as $key => $val) {
            $_key = array_search($val['month'], array_column($result, 'month'));
            if ($_key !== false) {
                $all_months_year[$key]['valid'] = (float) $result[$_key]['valid'];
                $all_months_year[$key]['waiting'] = (float) $result[$_key]['waiting'];
            }
        }
        
        return array('result' => $all_months_year, 'format' => $preference);
    }

    public function allStaticDataOfAbsencesGridChartAction($_period = null) {
        $em = $this->getDoctrine()->getManager();
        $current_user_id = $this->get('security.context')->getToken()->getUser();
        $securityContext = $this->container->get('security.authorization_checker');
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $provider_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $date_period = new \DateTime($_period);
        $date_period = $date_period->modify('first day of this month');
        $e_payPeriod = new PayPeriod();
        $obj_Payperiod = $e_payPeriod->getObjectPayPeriod($date_period, $this->container, $em, $current_user_id);
        if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
            if (is_object($obj_Payperiod))
                $requestabsences = $em->getRepository('requestAbsenceBundle:requestAbsence')->AbsencesStaticDataOfGridFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
            else
                $requestabsences = array();
        } else if($securityContext->isGranted('ROLE_COLLABORATEUR')){
            if (is_object($obj_Payperiod))
                $requestabsences = $em->getRepository('requestAbsenceBundle:requestAbsence')->AbsencesStaticDataOfGridFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);
            else
                $requestabsences = array();            
        }
        return array('result' => $requestabsences);
    }

    public function singlePageLayoutAction() {
        return $this->render('requestabsence/single_page_layout.html.twig');
    }

}