<?php

namespace ExportBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class ExportType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('salClassification', CheckboxType::class, array(
                    'label' => 'Matricule',
                    'required' => false,
                ))
                ->add('first_name', CheckboxType::class, array(
                    'label' => 'Nom',
                    'required' => false,
                ))
                ->add('last_name', CheckboxType::class, array(
                    'label' => 'Prenom',
                    'required' => false,
                ))
                ->add('statut', CheckboxType::class, array(
                    'label' => 'Statut',
                    'required' => false,
                ))

//,
//                    'label_attr' => array(
//                        'class' => 'col-sm-3 control-label no-padding-right asterix'
//            )
                
                
//                ->add('typetransport', 'choice', array(
//                     'label' => 'Type de transport ',              
//                    'choices' => array(
//                        'Transport personnel' => 'Transport personnel',
//                        'Transport public' => 'Transport public',
//                    ),
//                    'attr' => array('ng-model' => 'type',
//                        'class' => 'form-control',
//                        'ng-model' => 'type',
//                    ),
//                    'label_attr' => array(
//                        'class' => 'col-sm-3 control-label no-padding-right asterix'
//            )))
//                
                ->add('dateAn', CheckboxType::class, array(
                    'label' => 'date ancienneté',
                    'required' => false,
                ))
                ->add('dateTr', CheckboxType::class, array(
                    'label' => 'durée de travail',
                    'required' => false,
                ))
                ->add('bonus', 'entity', array(
                    'label' => 'Primes',
                    // used to render a select box, check boxes or radios
                    'multiple' => true,
                    'expanded' => true,
                    'empty_value' => 'Sélectionner un collaborateur',
                    'class' => 'AdminBundle\Entity\SettingsBonusType',
                    //'property' => 'displayName',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
            )))
//                
//                ->add('primeEx', CheckboxType::class, array(
//                    'label' => 'Prime Exceptionnelle',
//                    'required' => false,
//                ))
//                ->add('primeOb', CheckboxType::class, array(
//                    'label' => 'prime sur objectif',
//                    'required' => false,
//                ))
//                ->add('prime', CheckboxType::class, array(
//                    'label' => 'Autres primes',
//                    'required' => false,
//                ))
                ->add('hours', CheckboxType::class, array(
                    'label' => "Nombre d'heures",
                    'required' => false,
                ))
                ->add('absence', 'entity', array(
                    'label' => 'Congés et absences',
                    'multiple' => true,
                    'expanded' => true,
                    'class' => 'AdminBundle\Entity\SettingsAbsences',
                    //    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
            )))

//                ->add('congePaye', CheckboxType::class, array(
//                    'label' => 'congé Payé',
//                    'required' => false,
//                ))
//                ->add('rtt', CheckboxType::class, array(
//                    'label' => 'RTT',
//                    'required' => false,
//                ))
//                ->add('autreAb', CheckboxType::class, array(
//                    'label' => 'Autres absences',
//                    'required' => false,
//                ))
                ->add('notrFraisRemb', CheckboxType::class, array(
                    'label' => 'note de Frais à Rembourser',
                    'required' => false,
                ))
                ->add('heuressupp', CheckboxType::class, array(
                    'label' => 'Heures supplémentaires',
                    'required' => false,
                ))
                ->add('acompte', CheckboxType::class, array(
                    'label' => 'Acompte à déduire',
                    'required' => false,
                ))
                ->add('avance', CheckboxType::class, array(
                    'label' => 'Avance à déduire',
                    'required' => false,
                ))
                ->add('exporter', SubmitType::class, array(
                    'attr' => array('class' => 'exporter'),
        ));


        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'hrmexportbundle_export';
    }

}
