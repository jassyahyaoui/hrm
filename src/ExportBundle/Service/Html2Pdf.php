<?php

namespace ExportBundle\Service;

class Html2Pdf {

    private $pdf;

    public function create($orientation = null, $format = null, $lang = null, $unicode = null, $encoding = null, $margin = null) {
        $this->pdf = new \HTML2PDF(
                $orientation ? $orientation : $this->orientation, $format ? $format : $this->format, $lang ? $lang : $this->lang, $unicode ? $unicode : $this->unicode, $encoding ? $encoding : $this->encoding, $margin ? $margin : $this->margin
        );
    }

    public function generatePdf($template, $name, $return = false, $type = 'D') {
        $this->pdf->writeHtml($template);
        if($return)
            return $this->pdf->Output($name.'.pdf', $type);
        
        $this->pdf->Output($name.'.pdf', $type);
    }

}
