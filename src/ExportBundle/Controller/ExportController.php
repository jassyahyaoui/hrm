<?php

namespace ExportBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\Query;

class ExportController extends Controller {

    public function layoutAction() {
        return $this->render('export/export_layout.html.twig');
    }
    
    public function indexAction()
    {
        return $this->render('hrm_exports/index.html.twig');
    }

    public function exportemployeesAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $exporter = $this->get('ee.dataexporter');
        $securityContext = $this->container->get('security.authorization_checker');
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $company_name = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany();
        if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
            $users = $em->getRepository('UserBundle:HrmEmployee')->ExportEmployees($company_id);
        }
        
        $response = new Response();
        $handle = fopen('php://output', 'w+');
        fprintf($handle, chr(0xEF) . chr(0xBB) . chr(0xBF));
        fputcsv($handle, ['Entreprise', 'PrÃ©nom', 'Nom', 'Numero de matricule', 'Sexe', 'Nom usage', 'Date de naissance', 'DÃ©partement de naissance', 'Lieu de naissance'
            , 'Pays de naissance', 'Pays de nationalitÃ©', 'Adresse domicile', 'Commune de domicile'
            , 'Code Postal de domicile', 'Pays de domicile', 'NumÃ©ro de sÃ©curitÃ© sociale', 'Personne handicapÃ©', 'Situation de famille'
            , 'Enfants Ã  charge', 'Mode de rÃ¨glement', 'Type Ã©tude', 'Niveau Ã©tude', 'Commentaires fiche identification', 'Statut'
            , 'Nature du contrat', 'Dispositif particulier', 'Convention', 'Position', 'Classification', 'Coefficient', 'IntitulÃ© du poste'
            , 'DÃ©but de contrat', 'Fin de contrat', 'Date d anciennetÃ©', 'Horaire hebdomadaire ou mensuel - si temps partiel', 'DurÃ©e du travail du salariÃ©'
            , 'Nb de jours de RTT/repos fixe', 'Salaire de base annuel brut', 'Nb de mensualitÃ©s', 'PrÃ©voyance-RÃ©fÃ©rence du contrat', 'Mutuelle-RÃ©fÃ©rence du contrat'
            , 'Commentaires contrat', 'Justificatif RIB URL', 'Justificatif Contrat URL', 'Justificatif Identite URL', 'Justificatif Travail URL', 'Justificatif Secu URL'
            , 'Justificatif DUE URL', 'JustificatifÂ Prevoyance URL', 'JustificatifÂ Sante URL', 'JustificatifÂ Retraite URL', 'JustificatifÂ Autre URL'], ';');
    
        foreach ($users as $utilisateur) {
            fputcsv(
                $handle,
                [ $utilisateur->getCompany()
                , (empty($utilisateur->getUser()->getFirstName())? $utilisateur->getUser()->getUserName(): $utilisateur->getUser()->getFirstName())
                , (empty($utilisateur->getUser()->getLastName())? $utilisateur->getUser()->getUserName(): $utilisateur->getUser()->getLastName())                    
                , $utilisateur->getEmployeeNumber()
                , $utilisateur->getGender()
                , $utilisateur->getUser()->getNameUsage()
                , (!$utilisateur->getBirthDate()) ? '' : $utilisateur->getBirthDate()->format('d/m/Y')      
                , $utilisateur->getDepartmentBirth()
                , $utilisateur->getPlaceBirth()
                , $utilisateur->getNationCode()
                , $utilisateur->getCountryNationality()
                , $utilisateur->getAddress()
                , $utilisateur->getCommune()
                , $utilisateur->getZipCode()
                , $utilisateur->getCountryCode()
                , $utilisateur->getSsnNum()
                , (($utilisateur->getIsHandicape())? 'Oui' : 'Non')
                , $utilisateur->getFamilyStatus()
                , $utilisateur->getNumChildren()
                , $utilisateur->getPaymentMethod()
                , $utilisateur->getStudyType()
                , $utilisateur->getStudyLevel()
                , $utilisateur->getComments()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getEmpStatus()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getContractType()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getContractCustomCondition()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getConvention()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getSalGrade()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getSalClassification()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getSalEchelon()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getSalJobTitle()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getSalJoinedDateS()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getSalLeftDateS()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getSalSeniorityDateS()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getWorkTimetable()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getWorkingTime()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getFixedRestDaysAgreed()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getGrossAnnualBaseSalary()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getNbOfInstallments()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getInsuranceContractRef1()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getInsuranceContractRef2()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getNotes()
                , $utilisateur->getRibAttachments()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getContractAttachment()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getIdentificationAttachment()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getAutorsationAttachment()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getVitalCardAttachment()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getSingleDeclarationAttachment()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getAffiliationPensionAttachment()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getAffiliationHealthAttachment()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getAffiliationComplementaryPensionAttachment()
                , (!$utilisateur->getEmployeeContract()) ? '' : $utilisateur->getEmployeeContract()->getOtherAttachment()
                    
                ],
                ';'
             );
        }
    fclose($handle);
    $response->setStatusCode(200);
    $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
    $response->headers->set('Content-Disposition','attachment; filename="FABEREO_' . $company_name . '_' . date('dmYHis') . '_Employees.csv"');
    return $response;
    }
    
    
    /**
     * Flux :  Export des Elements Variables de Paie
     */    
    public function variablePayItemsAction($_period = null) {
//$this->exportinColumnAction($_period);
            $user = $this->container->get('settingsbundle.preference.service')->getEmpData();       
            $em = $this->get('doctrine.orm.entity_manager');
            $allData = array();
         
            // retourne list exportÃ© pour un mois sÃ©lectionnÃ© sur calendriÃ©
            try
            {
               $date_period = new \DateTime($_period);
               $date_period = $date_period->modify( 'first day of this month' );
            } catch (\Exception $ex) {
                 return  new Response('ERR_FORMAT_PERIOD');;
            }

            // retourne list exportÃ© pour un mois sÃ©lectionnÃ© sur calendriÃ©
           $obj_payperiod = $this->getObjectPayPeriod($em, $date_period); 

           if(!is_object($obj_payperiod))
           {
               return  new Response('ERR_DATA_NOT_FOUND');
           }
           
           // retourne list txa by provider
           $obj_tax_provider = $this->getObjectTaxBelongProvider($em); 
          
            $response = new Response();
            $handle = fopen('php://output', 'w+');
            fprintf($handle, chr(0xEF) . chr(0xBB) . chr(0xBF));
            
            fputcsv($handle, array_merge(
                                    array('Matricule','Nom','PrÃ©nom',
                                          'Type', 'Nature','Codification' ,
                                          'Fiscal recup coeff', 'Date',
                                          'Nom', 'Montant', 'UnitÃ© du Montant', 'Commentaires','Justificatif Url') ,
                                    array_values($obj_tax_provider)),
                                    ';');
            
            // get all expenses data (ik, transport, acco, dining, other)
            $list_expense_taxes_data = $this->getAllExpenseTaxesData($em, $obj_payperiod, $user);
            $list_expense_id_have_taxes = $this->getAllExpenseIdHaveTaxes($list_expense_taxes_data);
            
            $list_expense_without_taxes_data = $this->getAllExpenseWithoutTaxesData($em, $obj_payperiod, $user, $list_expense_id_have_taxes);
            
            $this->getRowsBonus($handle, $list_expense_taxes_data['bonus']);
            $this->getRowsPaydayVariations($handle, $list_expense_taxes_data['payday']);
            $this->getRowsExtraHours($handle, $list_expense_taxes_data['extra_hours']);
            $this->getRowsIK($handle, $list_expense_taxes_data['ik']);
            $this->getRowsTransport($handle, $list_expense_taxes_data['public_transport']);
            /** FOR EXPENSE HAVE TAXES **/
            $this->getRowsOfAllExpenseTaxesBelongType($handle, $em, $obj_tax_provider, $list_expense_taxes_data['transport'], 't');
            $this->getRowsOfAllExpenseTaxesBelongType($handle, $em, $obj_tax_provider, $list_expense_taxes_data['accomodation'], 'a');
            $this->getRowsOfAllExpenseTaxesBelongType($handle, $em, $obj_tax_provider, $list_expense_taxes_data['dining'], 'd');
            $this->getRowsOfAllExpenseTaxesBelongType($handle, $em, $obj_tax_provider, $list_expense_taxes_data['other'], 'o');
            /** FOR EXPENSE HAVEN'T TAXES **/   
            $this->getRowsOfAllExpenseWithoutBelongType($handle, $em, $list_expense_without_taxes_data['transport'], 't');
            $this->getRowsOfAllExpenseWithoutBelongType($handle, $em, $list_expense_without_taxes_data['accomodation'], 'a');
            $this->getRowsOfAllExpenseWithoutBelongType($handle, $em, $list_expense_without_taxes_data['dining'], 'd');
            $this->getRowsOfAllExpenseWithoutBelongType($handle, $em, $list_expense_without_taxes_data['other'], 'o');
            
        fclose($handle);
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'text/csv; charset=utf-8'); 
        $response->headers->set('Content-Disposition','attachment; filename="FABEREO_'. $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getName() .'_'. $date_period->format('mY'). '_' . date('dmYHis') . '_Export_EVP.csv"');
        return $response;  
    }

    
    private function getObjectPayPeriod($em, $pay_period)
    {
        $obj_PayPeriod = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')    
                          ->findOneBy(array('startDate'=>$pay_period, 'companyId'=>$this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId()));
        
        return $obj_PayPeriod;
    }   
    
    private function getObjectTaxBelongProvider($em)
    {
        $obj_tax = $em->getRepository('AdminBundle:hrm_settings_tax')    
                          ->findBy(array('provider'=>$this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId()), array('rate' => 'ASC'));
        
        $_arr = array();
        if(!empty($obj_tax))
        {
            foreach ($obj_tax as $key => $value) {
                $_arr[$value->getId()] = '%'.$value->getRate().'%';
            }
        }
        return $_arr;
    } 

    private function getAllExpenseTaxesData($em, $obj_payperiod, $user)
    {
        
        return array_merge(array('public_transport'=>$em->getRepository('TransportBundle:hrm_transport')->findByPayPeriodBelongPublicTransport($obj_payperiod, $user)),
                           array('extra_hours'=>$em->getRepository('ExtraHoursBundle:hrm_additional_hours')->findByPayPeriodBelongExtraHours($obj_payperiod, $user)),
                           array('payday'=>$em->getRepository('PaydayVariationsBundle:PaydayVariations')->findByPayPeriodBelongPaydayVariations($obj_payperiod, $user)),
                           array('bonus'=>$em->getRepository('BonusBundle:hrm_bonus')->findByPayPeriodBelongBonus($obj_payperiod, $user)),
                           array('ik'=>$em->getRepository('ExpenseBundle:hrm_expense_ik')->findByPayPeriodBelongExpense($obj_payperiod, $user)),
                           array('transport'=>$em->getRepository('ExpenseBundle:hrm_expenses_transport_taxes')->findByPayPeriodBelongExpense($obj_payperiod, $user)),
                           array('accomodation'=>$em->getRepository('ExpenseBundle:hrm_expenses_accomodation_taxes')->findByPayPeriodBelongExpense($obj_payperiod, $user)),
                           array('dining'=>$em->getRepository('ExpenseBundle:hrm_expenses_dining_taxes')->findByPayPeriodBelongExpense($obj_payperiod, $user)),
                           array('other'=>$em->getRepository('ExpenseBundle:hrm_expenses_other_taxes')->findByPayPeriodBelongExpense($obj_payperiod, $user))
                                );
    }
    
    private function getAllExpenseWithoutTaxesData($em, $obj_payperiod, $user, $list_expense_Id_without_taxes)
    {
        return array_merge(array('transport'=>$em->getRepository('ExpenseBundle:hrm_expense_transport')->findByPayPeriodBelongExpenseHaventTaxes($obj_payperiod, $user, $list_expense_Id_without_taxes['transport'])),
                           array('accomodation'=>$em->getRepository('ExpenseBundle:hrm_expenses_accomodation')->findByPayPeriodBelongExpenseHaventTaxes($obj_payperiod, $user, $list_expense_Id_without_taxes['accomodation'])),
                           array('dining'=>$em->getRepository('ExpenseBundle:hrm_expenses_dining')->findByPayPeriodBelongExpenseHaventTaxes($obj_payperiod, $user, $list_expense_Id_without_taxes['dining'])),
                           array('other'=>$em->getRepository('ExpenseBundle:hrm_expenses_other')->findByPayPeriodBelongExpenseHaventTaxes($obj_payperiod, $user, $list_expense_Id_without_taxes['other']))
                                );
    }
    
    private function getRowsOfAllExpenseTaxesBelongType($handle, $em, $_array_keys_tax, $_data, $expense_type)
    {  
        if(!empty($_data))
        {
            $c_id = $_data[0][1];
            $_arrExp = array();           
            $rows_tax = array();
            $type = $this->getExpenseType($expense_type);
            
            foreach ($_data as $key => $value) {                
                if($c_id != $value)
                {
                    $c_id = $value[1];
                    
                }
                $_arrExp[$c_id][] = $value[0];                    
            }
                foreach ($_arrExp as $key => $value) {
                    $_arr_tax = array();
                    foreach ($value as $k => $v) {
                             $_arr_tax[$v->getTax()->getId()] =  $v->getAmount();
                    }
                    
                  $rows_tax =  $this->getTriTaxHeader($_arr_tax, $_array_keys_tax);
                  $_setting_expense = $this->getSettingsExpense($value[0], $em, $expense_type);
                 
                    fputcsv(
                            $handle, 
                            array_merge(array($value[0]->getExpense()->getContributor()->getEmployeeNumber(),
                                  $value[0]->getExpense()->getContributor()->getUser()->getFirstName(),
                                  $value[0]->getExpense()->getContributor()->getUser()->getLastName(),
                                  $type, 
                                  $_setting_expense->getDisplayName() , 
                                  $_setting_expense->getDescription(), 
                                  $_setting_expense->getRecoveryRate(),
                                  $value[0]->getExpense()->getDate()->format('d/m/Y'),
                                  $value[0]->getExpense()->getDisplayName(),
                                  $this->getAmountExpenseByType($value[0], $expense_type),
                                  'euros',
                                  $value[0]->getExpense()->getComment(),
                                  ((!is_null($value[0]->getExpense()->getAttachments()))? $value[0]->getExpense()->getAttachments()->getPath():""),
                                  
                                ), $rows_tax), 
                            ';' 
                        );            
            }
        }
        return NULL;
    }
    
    private function getRowsOfAllExpenseWithoutBelongType($handle, $em, $_data, $expense_type)
    {  
        if(!empty($_data))
        {
            $type = $this->getExpenseType($expense_type);
            
                foreach ($_data as $key => $value) {
                 
                 $_setting_expense = $this->getSettingsExpenseWithoutTaxes($value, $em, $expense_type);
                    fputcsv(
                            $handle, 
                            array($value->getContributor()->getEmployeeNumber(),
                                  $value->getContributor()->getUser()->getFirstName(),
                                  $value->getContributor()->getUser()->getLastName(),
                                  $type, 
                                  $_setting_expense->getDisplayName() , 
                                  $_setting_expense->getDescription(), 
                                  $_setting_expense->getRecoveryRate(),
                                  $value->getDate()->format('d/m/Y'),
                                  $value->getDisplayName(),
                                  $this->getAmountExpenseByType($value, $expense_type, true),
                                  'euros',
                                  $value->getComment(),
                                  ((!is_null($value->getAttachments()))? $value->getAttachments()->getPath():""),
                                  
                                ), 
                            ';' 
                        );            
            }
        }
        return NULL;
    }
    
    private function getRowsIK($handle, $_data)
    {
        if(!empty($_data))
        {
           
                foreach ($_data as $key => $value) {
                  
                    fputcsv(
                            $handle, 
                            array($value->getContributor()->getEmployeeNumber(),
                                  $value->getContributor()->getUser()->getFirstName(),
                                  $value->getContributor()->getUser()->getLastName(),
                                  'FraisIK', 
                                  $value->getSettingsExpIk()->getDisplayName(), 
                                  $value->getSettingsExpIk()->getDescription(), 
                                  '',
                                  $value->getDate()->format('d/m/Y'),
                                  $value->getDisplayName(),
                                  $value->getKilometer(),
                                  'km',
                                  "Puissance fiscale :".$value->getSettingsExpIk()->getCoeffValue()
                                  ."-".
                                    $value->getComment(),
                                  ((!is_null($value->getAttachments()))? $value->getAttachments()->getPath():""),
                                  
                                ), 
                            ';' 
                        );             
            }
        }
        return NULL;
    }
    
    private function getRowsBonus($handle, $_data)
    {
        if(!empty($_data))
        {
           
                foreach ($_data as $key => $value) {
                  
                    fputcsv(
                            $handle, 
                            array($value->getContributor()->getEmployeeNumber(),
                                  $value->getContributor()->getUser()->getFirstName(),
                                  $value->getContributor()->getUser()->getLastName(),
                                  'Prime de rendement', 
                                  $value->getSettingsBonusName()->getDisplayName(), 
                                  $value->getSettingsBonusName()->getDescription(), 
                                  '',
                                  $value->getDate()->format('d/m/Y'),
                                  '',
                                  $value->getAmount(),
                                  'euros',
                                  $value->getComment(),
                                  ((!is_null($value->getAttachments()))? $value->getAttachments()->getPath():""),
                                  
                                ), 
                            ';' 
                        );             
            }
        }
        return NULL;
    }
    
    private function getRowsPaydayVariations($handle, $_data)
    {
        if(!empty($_data))
        {
           
                foreach ($_data as $key => $value) {
                  
                    fputcsv(
                            $handle, 
                            array($value->getContributor()->getEmployeeNumber(),
                                  $value->getContributor()->getUser()->getFirstName(),
                                  $value->getContributor()->getUser()->getLastName(),
                                  (empty($value->getNature())? strtoupper($value->getType()) : strtoupper('Saisie') ), 
                                  (empty($value->getNature())? strtoupper($value->getType()) : strtoupper($value->getNature()->getDisplayName() )), 
                                  (empty($value->getNature())? $value->getComment() : $value->getNature()->getDescription() ), 
                                  '',
                                  $value->getDate()->format('d/m/Y'),
                                  '',
                                  $value->getAmount(),
                                  'euros',
                                  $value->getComment(),
                                  ((!is_null($value->getAttachments()))? $value->getAttachments()->getPath():""),
                                  
                                ), 
                            ';' 
                        );             
            }
        }
        return NULL;
    }
    
    private function getRowsExtraHours($handle, $_data)
    {
        if(!empty($_data))
        {
           
                foreach ($_data as $key => $value) {
                  
                    fputcsv(
                            $handle, 
                            array($value->getContributor()->getEmployeeNumber(),
                                  $value->getContributor()->getUser()->getFirstName(),
                                  $value->getContributor()->getUser()->getLastName(),
                                  (empty($value->getSettingsAdditionalHoursType())? '' : mb_strtoupper($value->getSettingsAdditionalHoursType()->getDisplayName()) ), 
                                  (empty($value->getSettingsAdditionalHoursType())? '' : mb_strtoupper($value->getSettingsAdditionalHoursType()->getDisplayName()) ), 
                                  '', 
                                  '',
                                  $value->getDate()->format('d/m/Y'),
                                  '',
                                  ($value->getHour25() + $value->getHour50()),
                                  'H',
                                  'Niveau 1: '. $value->getHour25() . ', Niveau 2: '.$value->getHour50() . ', ' . $value->getComment(),
                                  ((!is_null($value->getAttachments()))? $value->getAttachments()->getPath():""),
                                  
                                ), 
                            ';' 
                        );             
            }
        }
        return NULL;
    }    
    
    
    private function getRowsTransport($handle, $_data)
    {
        if(!empty($_data))
        {
           
                foreach ($_data as $key => $value) {
                  if( strcmp(mb_strtoupper($value->getType()), mb_strtoupper('Transport public')) !== 0)
                  {
                      $_transpTye = ', Transport Public Obligatoire : ';
                  }
                  else if(strcmp(mb_strtoupper($value->getType()), mb_strtoupper('Transport personnel')) !== 0)
                  {
                       $_transpTye = ', Transport Personnel Obligatoire : ';
                  }
                  else{ $_transpTye = ', Transport Obligatoire : '; }
                  
                    fputcsv(
                            $handle, 
                            array($value->getContributor()->getEmployeeNumber(),
                                  $value->getContributor()->getUser()->getFirstName(),
                                  $value->getContributor()->getUser()->getLastName(),
                                  $value->getType(), 
                                  '', 
                                  '', 
                                  '',
                                  $value->getDate()->format('d/m/Y'),
                                  '',
                                  $value->getAmount(),
                                  'euros',
                                  $value->getComment().$_transpTye.$value->getPublicMandatory().", Date de DÃ©but de Prise en charge "
                                  .$value->getStartDate()->format('d/m/Y').", Date de Fin de Prise en charge ".$value->getEndDate()->format('d/m/Y'),
                                  ((!is_null($value->getAttachments()))? $value->getAttachments()->getPath():""),
                                  
                                ), 
                            ';' 
                        );             
            }
        }
        return NULL;
    }
    
    private function getTriTaxHeader($_tax_, $_arr_keys_tax)
    {
        $_arr = array();
        foreach ($_arr_keys_tax as $key => $value) {
            if(isset($_tax_[$key]))
            {
                $_arr[] = $_tax_[$key];   
            }                
            else {
                $_arr[] = "";
            }
        }
         return $_arr;
     }
    
    private function getExpenseType($expense_type)
    {
        if($expense_type == 't')
            return 'FraisTRP';
        if($expense_type == 'a')
            return 'FraisHEB';
        if($expense_type == 'd')
            return 'FraisRES';
        if($expense_type == 'o')
            return 'FraisAUT';
        return NULL;
    }
    
    private function getSettingsExpense($row, $em, $expense_type)
    {
        if($expense_type == 't')
            return $em->getRepository('ExpenseBundle:hrm_expense_transport')->findSettingsExpTransport($row->getExpense()->getUid())[0]->getSettingsExpTransport();
        if($expense_type == 'a')
                return $em->getRepository('ExpenseBundle:hrm_expenses_accomodation')->findSettingsExpAccomodation($row->getExpense()->getUid())[0]->getSettingsExpAccomodation();
            if($expense_type == 'd')
            return $em->getRepository('ExpenseBundle:hrm_expenses_dining')->findSettingsExpDining($row->getExpense()->getUid())[0]->getSettingsExpDining();
        if($expense_type == 'o')
            return $em->getRepository('ExpenseBundle:hrm_expenses_other')->findSettingsExpOther($row->getExpense()->getUid())[0]->getSettingsExpenses();
        return NULL;
    } 
    
    private function getSettingsExpenseWithoutTaxes($row, $em, $expense_type)
    {
        if($expense_type == 't')
            return $row->getSettingsExpTransport();
        if($expense_type == 'a')
            return $row->getSettingsExpAccomodation();
        if($expense_type == 'd')
            return $row->getSettingsExpDining();
        if($expense_type == 'o')
            return $row->getSettingsExpenses();
        return NULL;
    }     
    
    private function getAllExpenseIdHaveTaxes($list_expense_data) {
        $_arr = array('transport'=>array(), 'accomodation'=>array(), 'dining'=>array(), 'other'=>array());
        if(!empty($list_expense_data['transport']))
        {
            $id_transp = $list_expense_data['transport'][0][1];
            $_arr['transport'][] = $id_transp;
            foreach ($list_expense_data['transport'] as $key => $value) {
                if($id_transp != $value[1])
                {
                    $_arr['transport'][] = $value[1];
                    $id_transp = $value[1];
                }
            }
        }
        
        if(!empty($list_expense_data['accomodation']))
        {
            $id_accom = $list_expense_data['accomodation'][0][1];
            $_arr['accomodation'][] = $id_accom;
            foreach ($list_expense_data['accomodation'] as $key => $value) {
                if($id_accom != $value[1])
                {
                    $_arr['accomodation'][] = $value[1];
                    $id_accom = $value[1];
                }
            }
        }
        
        if(!empty($list_expense_data['dining']))
        {
            
            $id_dining = $list_expense_data['dining'][0][1];
            $_arr['dining'][] = $id_dining;
            foreach ($list_expense_data['dining'] as $key => $value) {
                if($id_dining != $value[1])
                {
                    $_arr['dining'][] = $value[1];
                    $id_dining = $value[1];
                }
            }
        }
        if(!empty($list_expense_data['other']))
        {
            $id_other = $list_expense_data['other'][0][1];
            $_arr['other'][] = $id_other;
            foreach ($list_expense_data['other'] as $key => $value) {
                if($id_other != $value[1])
                {
                    $_arr['other'][] = $value[1];
                    $id_other = $value[1];
                }
            }
        } 
        
        return $_arr;
    }
    
    private function getAmountExpenseByType($_obj_exp, $expense_type, $score = false) {
        
       
        if($expense_type == 't')
            return (($score)? $_obj_exp->getAmountTran() : $_obj_exp->getExpense()->getAmountTran() );
        if($expense_type == 'a')
            return (($score)? $_obj_exp->getAmountAcc()  : $_obj_exp->getExpense()->getAmountAcc()  );
        if($expense_type == 'd')
            return (($score)? $_obj_exp->getAmountDin()  : $_obj_exp->getExpense()->getAmountDin()  );
        if($expense_type == 'o')
            return (($score)? $_obj_exp->getAmountOth()  : $_obj_exp->getExpense()->getAmountOth()  );
        return NULL;           
        
    }
    
       private function getSettingsExternalCode($em, $settings) {
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $companyData = $empData->getCompany();
        $providerId = $companyData->getProvider()->getId();

        if ($settings == 'ah')
            return $em->getRepository('AdminBundle:hrm_settings_additional_hours_type')->FindTypesByProvider($providerId)[0]["external_code"];
        if ($settings == 'b')
            return $em->getRepository('AdminBundle:SettingsBonus')->SettingsBonusByProvider($providerId)[0]["external_code"];
        if ($settings == 'ea')
            return $em->getRepository('AdminBundle:hrm_settings_exp_accommodation')->hrm_settings_exp_accommodationByProvider($providerId)[0]["external_code"];
        if ($settings == 'eo')
            return $em->getRepository('AdminBundle:hrm_settings_exp_other')->hrm_settings_exp_otherByProvider($providerId)[0]["external_code"];
        if ($settings == 'et')
            return $em->getRepository('AdminBundle:hrm_settings_exp_transport')->hrm_settings_exp_transportByProvider($providerId)[0]["external_code"];
        if ($settings == 't')
            return $em->getRepository('AdminBundle:SettingsTransports')->SettingsTransportsByProvider($providerId)[0]["external_code"];
        if ($settings == 'p')
            return $em->getRepository('AdminBundle:settings_paydayvar_nature')->dayvarNatureByProvider($providerId)[0]["external_code"];
        return "";
    }

    public function exportinColumnAction($_period = null) {
        ini_set("memory_limit","15000M");
        ini_set("max-execution_time", "5000");
            try {
                $date_period = new \DateTime($_period);
                $date_period = $date_period->modify('first day of this month');
            } catch (\Exception $ex) {
                return new Response('ERR_FORMAT_PERIOD');
            }

            $em = $this->getDoctrine()->getManager();
            $obj_payperiod = $this->getObjectPayPeriod($em, $date_period);

            if (!is_object($obj_payperiod)) {
                return new Response('ERR_DATA_NOT_FOUND');
            }

//
        //          $handle = fopen('php://php', 'w+'); // Erreur  ! when exporting a huge data return a not save csv file
      $handle = fopen('php://output', 'w+'); // Erreur  ! when exporting a huge data return a not save csv file
//        $handle = fopen('php://temp/maxmemory:'. (5*1024*1024), 'w+');
//        $handle = fopen('FABEREO_' . $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getName() . '_' . $date_period->format('mY') . '_' . date('dmYHis') . '_Export_EVP.csv', 'w+');
        fprintf($handle, chr(0xEF) . chr(0xBB) . chr(0xBF));

            $curentUser = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $users = $em->getRepository('UserBundle:HrmEmployee')->ExportEmployees($curentUser->getCompany()->getId());
            $providerId = $curentUser->getCompany()->getProvider()->getId();

            $settingsAdditionalHours = $em->getRepository('AdminBundle:hrm_settings_additional_hours_type')->FindTypesByProvider($providerId);
            $settingsBonus = $em->getRepository('AdminBundle:SettingsBonus')->SettingsBonusByProvider($providerId);
            $settingsExpAccommodation = $em->getRepository('AdminBundle:hrm_settings_exp_accommodation')->hrm_settings_exp_accommodationByProvider($providerId);
            $settingsExpDinning = $em->getRepository('AdminBundle:hrm_settings_exp_dinning')->SettingsExpDinningByProvider($providerId);
            $settingsExpIk = $em->getRepository('AdminBundle:hrm_settings_exp_ik')->hrm_settings_exp_ikByProvider($providerId);
            $settingsExpTransport = $em->getRepository('AdminBundle:hrm_settings_exp_transport')->hrm_settings_exp_transportByProvider($providerId);
            $settingsExpOther = $em->getRepository('AdminBundle:hrm_settings_exp_other')->hrm_settings_exp_otherByProvider($providerId);
            $settingsTransport = $em->getRepository('AdminBundle:SettingsTransports')->SettingsTransportsByProvider($providerId);
            $settingsPaydayvar = $em->getRepository('AdminBundle:settings_paydayvar_nature')->dayvarNatureByProvider($providerId);
            $list_expense_taxes_data = $this->getAllExpenseTaxesData($em, $obj_payperiod, $curentUser);

//var_dump($settingsTransport);die();
            $expensesAccomodation = $em->getRepository('ExpenseBundle:hrm_expenses_accomodation')->findByPayPeriodBelongAccomodation($obj_payperiod, $curentUser);
            $expensesDinning = $em->getRepository('ExpenseBundle:hrm_expenses_dining')->findByPayPeriodBelongDinning($obj_payperiod, $curentUser);
            $expensesIk = $em->getRepository('ExpenseBundle:hrm_expense_ik')->findByPayPeriodBelongIk($obj_payperiod, $curentUser);
            $expensesTransports  = $em->getRepository('ExpenseBundle:hrm_expense_transport')->findByPayPeriodBelongTransport($obj_payperiod, $curentUser);
            $expensesOthers = $em->getRepository('ExpenseBundle:hrm_expenses_other')->findByPayPeriodBelongOthers($obj_payperiod, $curentUser);
            $transports = $em->getRepository('TransportBundle:hrm_transport')->findByPayPeriodBelongTransport($obj_payperiod, $curentUser);

            $colums = array("Matricule", "Nom", "PrÃ©nom");
            foreach ($settingsAdditionalHours as $settingAdditionalHours) {
                if ($settingAdditionalHours["external_code"]) {
                    array_push($colums, $settingAdditionalHours["external_code"] . "25");
                    array_push($colums, "COM_" . $settingAdditionalHours["external_code"] . "25");
                }
            }
            foreach ($settingsAdditionalHours as $settingAdditionalHours) {
                if ($settingAdditionalHours["external_code"]) {
                    array_push($colums, $settingAdditionalHours["external_code"] . "50");
                    array_push($colums, "COM_" . $settingAdditionalHours["external_code"] . "50");
                }
            }
            $settingsLabels = array_merge(
                $settingsBonus
                ,$settingsExpAccommodation
                ,$settingsExpDinning
                ,$settingsExpIk
                ,$settingsExpTransport
                ,$settingsExpOther
                ,$settingsTransport
                ,$settingsPaydayvar
            );
            foreach ($settingsLabels as $label) {
                if ($label["external_code"]) {
                    array_push($colums, $label["external_code"]);
                    array_push($colums, "COM_" . $label["external_code"]);
                }
            }
//dump($settingsExpOther);die();

            // Clean the array of string for rending a safe CSV File
//         for($i=0; $i < count($colums);$i++){
//         $colums[$i] =  $this->cleanString($colums[$i]);}
            // END Clean CSV

            fputcsv($handle, $colums,";");
            foreach ($users as $user) {
                $row = [
                    (empty($user->getEmployeeNumber()) ? "" : $user->getEmployeeNumber())
                    , (empty($user->getUser()->getFirstName()) ? $user->getUser()->getUserName() : $user->getUser()->getFirstName())
                    ,(empty($user->getUser()->getLastName()) ? $user->getUser()->getUserName() : $user->getUser()->getLastName())
                ];
                foreach ($settingsAdditionalHours as $settingAdditionalHours) {
                    $data = 0;
                    $com = "";
                    foreach ($list_expense_taxes_data['extra_hours'] as $key => $value) {
                        if ($value->getSettingsAdditionalHoursType()->getDisplayName() == $settingAdditionalHours["display_name"])
                            if ($user->getId() == $value->getContributor()->getId()) {
                                $data = $data + $value->getHour25();
                                if($value->getComment())
                                    $com = $com ." ".   $value->getComment();
                                if($value->getAttachments())
                                    $com =  $com ." ".  $value->getAttachments()->getPath();
                            }
                    }
                    array_push($row, $data);
                    array_push($row, trim($com));
                }
                foreach ($settingsAdditionalHours as $settingAdditionalHours) {
                    $data = 0;
                    $com = "";
                    foreach ($list_expense_taxes_data['extra_hours'] as $key => $value) {
                        if ($value->getSettingsAdditionalHoursType()->getDisplayName() == $settingAdditionalHours["display_name"])
                            if ($user->getId() == $value->getContributor()->getId()) {
                                $data = $data + $value->getHour50();
                                if($value->getComment())
                                    $com = $com ."  ".$value->getComment();
                                if($value->getAttachments())
                                    $com =  $com ." ".  $value->getAttachments()->getPath();
                            }
                    }
                    array_push($row, $data);
                    array_push($row, trim($com));
                }
                foreach ($settingsBonus as $settingBonus) {
                    $data = 0;
                    $com = "";
                    foreach ($list_expense_taxes_data['bonus'] as $key => $value) {
                        if ($value->getSettingsBonusName()) {
                            if ($value->getSettingsBonusName()->getDisplayName() == $settingBonus["display_name"])
                                if ($user->getId() == $value->getContributor()->getId()) {
                                    $data = $data + $value->getAmount();
                                    if($value->getComment())
                                        $com = $com ." ".$value->getComment();
                                    if($value->getAttachments())
                                        $com =  $com ." ".  $value->getAttachments()->getPath();
                                }
                        }
                    }
                    array_push($row, $data);
                    array_push($row, $com);
                }
                foreach ($settingsExpAccommodation as $settingExpAccommodation) {
                    $data = 0;
                    $com = "";
                    foreach ($expensesAccomodation as $key => $value) {
                        if ($value->getSettingsExpAccomodation()->getDisplayName() == $settingExpAccommodation["display_name"])
                            if ($user->getId() == $value->getUser()->getId()) {
                                $data = $data + $value->getAmountAcc();
                                if($value->getComment())
                                    $com = $com ." ".$value->getComment();
                                if($value->getAttachments())
                                    $com =  $com ." ".  $value->getAttachments()->getPath();
                            }
                    }
                    array_push($row, $data);
                    array_push($row, trim($com));
                }
                foreach ($settingsExpDinning as $settingExpDining) {
                    $data = 0;
                    $com = "";
                    foreach ($expensesDinning as $key => $value) {
                        if ($value->getSettingsExpDining()->getDisplayName() == $settingExpDining["display_name"])
                            if ($user->getId() == $value->getUser()->getId()) {
                                $data = $data + $value->getAmountDin();
                                if($value->getComment())
                                    $com = $com ." ".$value->getComment();
                                if($value->getAttachments())
                                    $com =  $com ." ".  $value->getAttachments()->getPath();
                            }
                    }
                    array_push($row, $data);
                    array_push($row, trim($com));
                }
                foreach ($settingsExpIk as $settingExpIk) {
                    $data = 0;
                    $com = "";
                    foreach ($expensesIk as $key => $value) {
                        if ($value->getSettingsExpIk()->getDisplayName() == $settingExpIk["display_name"])
                            if ($user->getId() == $value->getUser()->getId()) {
                                $data = $data + $value->getAmountIk();
                                if($value->getComment())
                                    $com = $com ." ".$value->getComment();
                                if($value->getAttachments())
                                    $com =  $com ." ".  $value->getAttachments()->getPath();
                            }
                    }
                    array_push($row, $data);
                    array_push($row, trim($com));
                }
                foreach ($settingsExpTransport as $settingExpTransport) {
                    $data = 0;
                    $com = "";
                    foreach ($expensesTransports as $key => $value) {
                        if ($value->getSettingsExpTransport()->getDisplayName() == $settingExpTransport["display_name"])
                            if ($user->getId() == $value->getUser()->getId()) {
                                $data = $data + $value->getAmountTran();
                                if($value->getComment())
                                    $com = $com ." ".$value->getComment();
                                if($value->getAttachments())
                                    $com =  $com ." ".  $value->getAttachments()->getPath();
                            }
                    }
                    array_push($row, $data);
                    array_push($row, trim($com));
                }
                foreach ($settingsExpOther as $settingExpOther) {
                    $data = 0;
                    $com = "";
                    foreach ($expensesOthers as $key => $value) {
                        if ($value->getSettingsExpenses()->getDisplayName() == $settingExpOther["display_name"])
                            if ($user->getId() == $value->getUser()->getId())   {
                                $data = $data + $value->getAmountOth();
                                if($value->getComment())
                                    $com = $com ." ".$value->getComment();
                                if($value->getAttachments())
                                    $com =  $com ." ".  $value->getAttachments()->getPath();
                            }
                    }
                    array_push($row, $data);
                    array_push($row, trim($com));
                }
//                dump($transports);die();
                foreach ($settingsTransport as $settingTransport) {
                    $data = 0;
                    $com = "";
                    foreach ($transports as $key => $value) {

                        if($value->getType())
                        if ($value->getType()->getDisplayName() == $settingTransport["display_name"])// Erreur pas de setting
                            if ($user->getId() == $value->getContributor()->getId()) {
                                $data = $data + $value->getAmount();
                                $com = $com . " " . $value->getComment();
                                if($value->getAttachments())
                                    $com =  $com ." ".  $value->getAttachments()->getPath();
                            }


                    }
                    array_push($row, $data);
                    array_push($row, trim($com));
                }

                foreach ($settingsPaydayvar as $settingPaydayvar) {
                    $data = 0;
                    $com = "";
                    foreach ($list_expense_taxes_data['payday'] as $key => $value) {
                        if ($value->getNature())
                            if ($value->getNature()->getDisplayName() == $settingPaydayvar["displayName"])
                                if ($user->getId() == $value->getContributor()->getId()) {
                                    $data = $data + $value->getAmount();
                                    if($value->getComment())
                                        $com = $com ." ".$value->getComment();
                                }
                    }
                    array_push($row, $data);
                    array_push($row, trim($com));
                }
                fputcsv($handle, $row,";",'"');
            }
        //      rewind($buffer);
        //   $csv = fgets($handle);
            fclose($handle);
            $response = new Response();
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'text/csv; charset=utf-8');
            $response->headers->set('Content-Disposition', 'attachment; filename="FABEREO_' . $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getName() . '_' . $date_period->format('mY') . '_' . date('dmYHis') . '_Export_EVP.csv"');

        //      dump($csv);die();
            return $response;
        }

        function cleanString($string) {
            // allow only letters
            $res = preg_replace("/[^a-zA-Z]/", "", $string);
//   trim what's left to 8 chars
            $res = substr($res, 0, 8);
            // make lowercase
            $res = strtolower($res);
            // return
            return $res;
        }

        
    public function singlePageLayoutAction() {
        return $this->render('hrm_exports/single_page_layout.html.twig');
    }

    }
