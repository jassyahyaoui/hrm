<?php

namespace ExportBundle\Tests;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ExportTest extends WebTestCase {

    private $client = null;

    public function setUp() {
     //  echo "\n --------Run Unit test of SettingsPayPeriod-------- \n";
       $this->client = static::createClient();
       
       /*** user login **********/
        $this->client->request('GET', '/logout');
       $crawler = $this->client->request('GET', '/login');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('html:contains("Veuillez")')->count() > 0);

        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => "responsable",
            '_password' => "responsable",
        ));
        $this->client->submit($form);
    }

    public function testExportCollaborators() {

        $this->client->request('GET', '/export/exportemployees');
        $this->assertContains('text/csv', $this->client->getResponse()->headers->get('Content-Type'), 'Error export csv file of collaborators');        
    }
    
    public function testExportVariablePayItems() {

        $this->client->request('GET', '/export/vpi/'.date('Y-m'));
        $this->assertContains('text/csv', $this->client->getResponse()->headers->get('Content-Type'), 'Error export csv file of Variable Pay Items');        
    }
       
}
