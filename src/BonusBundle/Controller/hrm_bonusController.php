<?php

namespace BonusBundle\Controller;

use BonusBundle\Entity\hrm_bonus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use FileBundle\Entity\attachments;



/**
 * Hrm_bonus controller.
 *
 */
class hrm_bonusController extends Controller {

    /**
     * Lists all hrm_bonus entities.
     *
     */
    public function layoutAction() {
        return $this->render('hrm_bonus/hrm_bonus_layout.html.twig');
    }

    /**
     * Lists all hrm_bonus entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $hrm_bonuses = $em->getRepository('BonusBundle:hrm_bonus')->findAll();
        return $this->render('hrm_bonus/index.html.twig', array(
                    'hrm_bonuses' => $hrm_bonuses,
        ));
    }

    /**
     * Creates a new hrm_bonus entity.
     *
     */
    public function newAction(Request $request) {
        $hrm_bonus = new Hrm_bonus();
        $em = $this->getDoctrine()->getManager();
        $providerId = $this->container->get('settingsbundle.preference.service')->getProviderId();
        $companyId = $this->container->get('settingsbundle.preference.service')->getCompanyId();
        $form = $this->createForm('BonusBundle\Form\hrm_bonusType', $hrm_bonus, array(
            'companyId' => $companyId,
            'providerId' => $providerId
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($hrm_bonus);
            $em->flush();
            return $this->redirectToRoute('bonus_show', array('id' => $hrm_bonus->getId()));
        }
        return $this->render('hrm_bonus/new.html.twig', array(
                    'hrm_bonus' => $hrm_bonus,
                    'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request) {
        $hrm_bonus = new Hrm_bonus();
        $em = $this->getDoctrine()->getManager();
        $providerId = $this->container->get('settingsbundle.preference.service')->getProviderId();
        $companyId = $this->container->get('settingsbundle.preference.service')->getCompanyId();
        $form = $this->createForm('BonusBundle\Form\hrm_bonusType', $hrm_bonus, array(
            'companyId' => $companyId,
            'providerId' => $providerId
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($hrm_bonus);
            $em->flush();
            return $this->redirectToRoute('bonus_show', array('id' => $hrm_bonus->getId()));
        }
        return $this->render('hrm_bonus/edit.html.twig', array(
                    'hrm_bonus' => $hrm_bonus,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_bonus entity.
     *
     */
    public function showAction(hrm_bonus $hrm_bonus) {
        $deleteForm = $this->createDeleteForm($hrm_bonus);

        return $this->render('hrm_bonus/show.html.twig', array(
                    'hrm_bonus' => $hrm_bonus,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_bonus entity.
     *
     */
    public function editBaseAction(Request $request, hrm_bonus $hrm_bonus) {
        $deleteForm = $this->createDeleteForm($hrm_bonus);
        $editForm = $this->createForm('BonusBundle\Form\hrm_bonusType', $hrm_bonus);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('bonus_edit', array('id' => $hrm_bonus->getId()));
        }
        return $this->render('hrm_bonus/edit.html.twig', array(
                    'hrm_bonus' => $hrm_bonus,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a hrm_bonus entity.
     *
     */
    public function deleteAction(Request $request, hrm_bonus $hrm_bonus) {
        $form = $this->createDeleteForm($hrm_bonus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_bonus);
            $em->flush();
        }

        return $this->redirectToRoute('bonus_index');
    }

    /**
     * Creates a form to delete a hrm_bonus entity.
     *
     * @param hrm_bonus $hrm_bonus The hrm_bonus entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_bonus $hrm_bonus) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('bonus_delete', array('id' => $hrm_bonus->getId())))
                        ->setMethod('DELETE')
                        ->getForm();
    }

    public function addAction(Request $request) {
        $hrm_bonus = new hrm_bonus();
        $form = $this->createForm('BonusBundle\Form\hrm_bonusType', $hrm_bonus);
        $form->handleRequest($request);
        $body = $request->getContent();
        $data = json_decode($body, true);
        $em = $this->getDoctrine()->getManager();
        //set default status if ROLE_COLLABORATEUR else set with the submited value
        if ($request->isMethod('POST')) {
            // will be get error if not deleted settings_pay_period from FormType before submit
            $form->remove('settings_pay_period');

            $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $current_user_id = $empData->getId();
            $form->submit($data);
            //Add values for all forgein Keys    
            $employee = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $hrm_bonus->setUser($employee);

            $hrm_bonus->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $hrm_bonus->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $hrm_bonus->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));

            $hrm_bonus->setSettingsBonusType($em->getRepository('AdminBundle:SettingsBonusType')->find($data['settings_bonus_type']));
            $hrm_bonus->setSettingsBonusName($em->getRepository('AdminBundle:SettingsBonus')->find($data['settings_bonus_name']));
            $hrm_bonus->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));

            if ($request->request->has('attachments')) {
                if ($data['attachments'] != null) {
//                    if ($request->request->has('attachments')) {
                    if ($hrm_bonus->getAttachments() != null) {
                        $attachment = $hrm_bonus->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if ((isset($data['attachments']['mimetype'])) && ($data['attachments']['mimetype'] != null))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $hrm_bonus->setAttachments($attachment);
//                    }
                }
                else {

                    $hrm_bonus->setAttachments(null);
                }
            }
            //Add default value on create Action
            $hrm_bonus->setCreateDate(new \DateTime('now'));
            $hrm_bonus->setCreateUid($current_user_id);
            $hrm_bonus->setLastUpdateUid($current_user_id);
            $hrm_bonus->setLastUpdate(new \DateTime('now'));
            //send mail to responsable

            $securityContext = $this->container->get('security.authorization_checker');
            $TranslateMail = $this->get('mailingbundle.email.send.service');
            if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $this->get('mailingbundle.email.send.service')->Notify($hrm_bonus, 'RESPONSABLE', $TranslateMail->TagsTranslateBonus('titleNew'), $TranslateMail->TagsTranslateBonus('etatNew'), $TranslateMail->TagsTranslateBonus('typeRequest'), $TranslateMail->TagsTranslateBonus('folder'), $TranslateMail->TagsTranslateBonus('viewName'));
            } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $this->get('mailingbundle.email.send.service')->Notify($hrm_bonus, 'COLLABORATEUR', $TranslateMail->TagsTranslateBonus('titleNew'), $TranslateMail->TagsTranslateBonus('etatNew'), $TranslateMail->TagsTranslateBonus('typeRequest'), $TranslateMail->TagsTranslateBonus('folder'), $TranslateMail->TagsTranslateBonus('viewName'));
            }


            // try to add first open one of pay period
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $hrm_bonus, $data['settings_pay_period']);
            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }
            try {
                $em->persist($hrm_bonus);
                $em->flush();
                $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
                return $response;
            } catch (\Exception $exc) {
                return new \Symfony\Component\BrowserKit\Response('Error to save data', 201);
            }
        }
    }

    public function allAction() {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');
//        $currentEmployeeId = $this->container->get('settingsbundle.preference.service')->getEmployeeId();
//        $companyId = $this->container->get('settingsbundle.preference.service')->getCompanyId();
//        $provider = $this->container->get('settingsbundle.preference.service')->getProviderId();

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();

        $current_user_id = $empData->getId();
        $companyData = $empData->getCompany();
        $company_id = $companyData->getId();
        $providerId = $companyData->getProvider();
        if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
            $result = $em->getRepository('BonusBundle:hrm_bonus')->FindForResp($company_id, $current_user_id);
        } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
            $result = $em->getRepository('BonusBundle:hrm_bonus')->FindForCollab($current_user_id);
        }
        $users = $em->getRepository('UserBundle:HrmEmployee')->FindCollaborateurByCompanyId($company_id);
        $BonusNames = $em->getRepository('BonusBundle:hrm_bonus')->FindBonusNames($providerId);
        $BonusTypes = $em->getRepository('BonusBundle:hrm_bonus')->FindBonusTypes($providerId);
        return array('$companyId' => $company_id, 'result' => $result, 'users' => $users, 'BonusNames' => $BonusNames, 'BonusTypes' => $BonusTypes);
    }

    public function updateAction(Request $request, hrm_bonus $hrm_bonus) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, true);
        $hrm_bonus = $em->getRepository('BonusBundle:hrm_bonus')->find($data['id']);
        $update_form = $this->createForm('BonusBundle\Form\hrm_bonusType', $hrm_bonus);
        if ($request->isMethod('PUT')) {
            $update_form->remove('settings_pay_period');
            $update_form->submit($data);
            $collab_id = $hrm_bonus->getCreateUid();
            $employee = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $hrm_bonus->setUser($em->getRepository('UserBundle:HrmEmployee')->find($collab_id));
            $hrm_bonus->setLastUpdate(new \DateTime('now'));
            $hrm_bonus->setLastUpdateUid($employee);
            if ($data['attachments'] != null) {
                if ($request->request->has('attachments')) {
                    if ($hrm_bonus->getAttachments() != null) {
                        $attachment = $hrm_bonus->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if (isset($data['attachments']['mimetype']))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $hrm_bonus->setAttachments($attachment);
                }
            } else {
                $hrm_bonus->setAttachments(null);
            }
            $hrm_bonus->setUser($em->getRepository('UserBundle:HrmEmployee')->find($employee));

            $hrm_bonus->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $hrm_bonus->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $hrm_bonus->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));

            $hrm_bonus->setSettingsBonusType($em->getRepository('AdminBundle:SettingsBonusType')->find($data['settings_bonus_type']['id']));
            if(is_int($data['settings_bonus_name'])) $settings_bonus_id = $data['settings_bonus_name'];
            else $settings_bonus_id = $data['settings_bonus_name']['id'];
                
            $hrm_bonus->setSettingsBonusName($em->getRepository('AdminBundle:SettingsBonus')->find($settings_bonus_id));
            $hrm_bonus->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));

            $this->statusInsertion($data['status'], $hrm_bonus);
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $hrm_bonus, $data['payPeriod']);
            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }
            $em->persist($hrm_bonus);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function removeAction(Request $request, hrm_bonus $hrm_bonus) {
        $form = $this->createDeleteForm($hrm_bonus);
        $form->handleRequest($request);

        if ($request->isMethod('DELETE')) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_bonus);
            $em->flush($hrm_bonus);
            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }

    public function ConfirmAction(Request $request, hrm_bonus $hrm_bonus) {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $hrm_bonus->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['confirm']));

        $entity = $em->getRepository('BonusBundle:hrm_bonus')->find($hrm_bonus);

        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $entity);
        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }

        //send mail to responsable
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($hrm_bonus, 'RESPONSABLE', $TranslateMail->TagsTranslateBonus('confirmMsg'), $TranslateMail->TagsTranslateBonus('confirmEtat'), $TranslateMail->TagsTranslateBonus('typeRequest'), $TranslateMail->TagsTranslateBonus('folder'), $TranslateMail->TagsTranslateBonus('viewName'));

        //send mail to responsable

        $em->persist($hrm_bonus);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function CancelAction(Request $request, hrm_bonus $hrm_bonus) {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $hrm_bonus->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['cancel']));

        $entity = $em->getRepository('BonusBundle:hrm_bonus')->find($hrm_bonus);

        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $hrm_bonus);
        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }

        //send mail to responsable
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($hrm_bonus, 'RESPONSABLE', $TranslateMail->TagsTranslateBonus('cancelMsg'), $TranslateMail->TagsTranslateBonus('cancelEtat'), $TranslateMail->TagsTranslateBonus('typeRequest'), $TranslateMail->TagsTranslateBonus('folder'), $TranslateMail->TagsTranslateBonus('viewName'));

        $em->persist($hrm_bonus);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    private function setOpenedSettingsPayPeriod($em, $entity, $payPeriodDate = null) {
        $ret = '';
        // affect first pay period

        if ($payPeriodDate == null) {
            $searchBy = array("companyId" => $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getCompany(),
                "status" => "open");
        } else {
            $searchBy = array("companyId" => $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getCompany());
        }

        $payperiodlist = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')
                ->findBy($searchBy, array('startDate' => 'ASC'));
        if ($payperiodlist) {

            if ($payPeriodDate == null) {
                $entity->setSettingsPayPeriod($payperiodlist[0]);
                $ret = true;
            } else {
                foreach ($payperiodlist as $key => $value) {
                    if ($value->getStartDate()->format('m/Y') == $payPeriodDate && ($value->getStatus() === 'open')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = true;
                        break;
                    }
                    if (($value->getStartDate()->format('m/Y') === $payPeriodDate) && ($value->getStatus() === 'close')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = false;
                        break;
                    }
                }
            }

            if ($ret === true)
                return true;
            if ($ret === false)
                return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_CLOSED_PAY_PERIOD"));

            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_NOT_PAY_PERIOD"));
        } else {
            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_EMPTY_PAY_PERIOD"));
        }
    }

    public function statusInsertion($status = null, $entity) {
        $date = null;
        if ($status == "2" || $status == "3") {
            $LastUpdateDate = new \DateTime("now");
            $date = $LastUpdateDate->getTimestamp();
        } else {
            $entity->setSettingsPayPeriod(null);
        }
        return $date;
    }

    public function singlePageLayoutAction() {
        return $this->render('hrm_bonus/single_page_layout.html.twig');
    }

}
