<?php

namespace BonusBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Security\Core\SecurityContext;
use SettingsBundle\DependencyInjection\SettingsPreferences;

class hrm_bonusType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('amount', 'text', array('required' => true,
                    'label' => 'Montant ',
                    'attr' => array(
                        'class' => 'form-control',
                        'id' => 'amount',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('settings_bonus_type', 'entity', array(
                    'label' => 'Collaborateur',
                    'required' => true,
                    'class' => 'AdminBundle\Entity\SettingsBonusType',
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("b")
                                ->leftJoin('b.provider', 'p')
                                ->Where('p.id = :providerId')
                                ->setParameter('providerId', (int) $options['providerId']);
                    },
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('settings_bonus_name', 'entity', array(
                    'label' => 'Collaborateur',
                    'empty_value' => 'Sélectionner un nom de bonus',
                    'required' => true,
                    'class' => 'AdminBundle\Entity\SettingsBonus',
                     'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("b")
                                ->leftJoin('b.provider', 'p')
                                ->Where('p.id = :providerId')
                                ->setParameter('providerId', (int) $options['providerId']);
                    },
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('date', 'date', array(
                    'label' => 'Date du frais',
                    'required' => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'date-directive' => ''
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))

            ->add('contributor', 'entity', array(
                'label' => 'Collaborateur',
                'required' => true,
                'class' => 'UserBundle\Entity\HrmEmployee',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->where("e.roles LIKE '%COLLABORATEUR%' OR e.roles LIKE '%RESPONSABLE%'")
                        ->andwhere('u.company = :value')
                        ->setParameter('value', (int)$options['companyId']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('validator', 'entity', array(
                    'label' => 'Valideur de la demande',
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control',
                        'id' => 'id',
                        'required' => true,
                    ),
                    'class' => 'UserBundle\Entity\HrmEmployee',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('u')
                            ->leftjoin('u.user', 'e')
                            ->where("e.roles LIKE '%RESPONSABLE%'")
                            ->andwhere('u.company = :value')
                            ->setParameter('value', (int)$options['companyId']);
                    },
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'))
            )
            ->add('requester', 'entity', array(
                'label' => 'Collaborateur',
                'empty_value' => 'Sélectionner un requester',
                'required' => true,
                'disabled' => true,
                'read_only' => true,
                'class' => 'UserBundle\Entity\HrmEmployee',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->Where('u.company = :value')
//                        ->andwhere('e.id = :valueUser')
                        ->setParameter('value', (int)$options['companyId']);
//                        ->setParameter('valueUser', (int)$options['currentUser']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
                ->add('settings_pay_period', 'text', array(
                    // 'class' => 'PayPeriodBundle\Entity\SettingsPayPeriod',
                    'label' => 'Remboursement en',
                    'required' => true,
//                    'read_only' => true,
                    //'widget' => 'single_text',
                    // 'format' => 'MM/yyyy',
                    'attr' => array(
                        'class' => 'form-control date-picker-expense',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('comment', 'textarea', array(
                    'label' => 'Commentaire',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right'
                    )
                ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'BonusBundle\Entity\hrm_bonus',
            'providerId' => null,
            'companyId' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'bonusbundle_hrm_bonus';
    }

}
