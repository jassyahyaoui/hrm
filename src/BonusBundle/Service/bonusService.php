<?php

namespace BonusBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class bonusService {

    protected $em;
    private $container;

    public function __construct(EntityManager $entityManager, Container $container) {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function deleteAllBonus() {
        $qb = $this->em->createQuery('DELETE BonusBundle:hrm_bonus b')->execute();
    }

    public function findBonus($amount) {
        $bonus = $this->container->get('doctrine')->getEntityManager()->getRepository('BonusBundle:hrm_bonus')
                ->findOneBy(array('amount' => $amount));
        if ($bonus)
            return $bonus;
        else
            return null;
    }

    public function findListBonusWithAmount($amount) {
        $bonuss = $this->container->get('doctrine')->getEntityManager()->getRepository('BonusBundle:hrm_bonus')
                ->findBy(array('amount' => $amount));
        return $bonuss;
    }

}
