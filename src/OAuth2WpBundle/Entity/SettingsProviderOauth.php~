<?php

namespace OAuth2WpBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SettingsProviderOauth
 *
 * @ORM\Table(name="hrm_settings_provider_oauth")
 * @ORM\Entity(repositoryClass="OAuth2WpBundle\Repository\SettingsProviderOauthRepository")
 */
class SettingsProviderOauth
{
    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
      /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsProvider")
    * @ORM\JoinColumn(name="provider_id", referencedColumnName="uid")
    */
    private $providerId;    

    /**
     * @var string
     *
     * @ORM\Column(name="redirectUri",  type="string", length=255)
     */
    private $redirectUri;
    
    /**
     * @var string
     *
     * @ORM\Column(name="endpointOrigin",  type="string", length=255)
     */
    private $endpointOrigin;

    /**
     * @var string
     *
     * @ORM\Column(name="clientId", type="string", length=255)
     */
    private $clientId;

    /**
     * @var string
     *
     * @ORM\Column(name="clientSecret", type="string", length=255)
     */
    private $clientSecret;

    /**
     * @var string
     *
     * @ORM\Column(name="authorizationEndpoint", type="string", length=255)
     */
    private $authorizationEndpoint;

    /**
     * @var string
     *
     * @ORM\Column(name="tokenEndpoint", type="string", length=255)
     */
    private $tokenEndpoint;

    /**
     * @var Date
     *
     * @ORM\Column(name="create_date", type="date", nullable=true)
     */
    private $create_date;
    
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\hrm_user")
     * @ORM\JoinColumn(name="create_uid", referencedColumnName="uid")
     */    
    private $createUid;
    
    /**
     * @var Date
     *
     * @ORM\Column(name="last_update", type="date", nullable=true)
     */
    private $last_update;
    
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\hrm_user")
     * @ORM\JoinColumn(name="last_update_uid", referencedColumnName="uid")
     */     
    private $last_update_uid;
   

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set providerId
     *
     * @param integer $providerId
     *
     * @return SettingsProviderOauth
     */
    public function setProviderId($providerId)
    {
        $this->providerId = $providerId;

        return $this;
    }

    /**
     * Get providerId
     *
     * @return integer
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * Set endpointOrigin
     *
     * @param string $endpointOrigin
     *
     * @return SettingsProviderOauth
     */
    public function setEndpointOrigin($endpointOrigin)
    {
        $this->endpointOrigin = $endpointOrigin;

        return $this;
    }

    /**
     * Get endpointOrigin
     *
     * @return string
     */
    public function getEndpointOrigin()
    {
        return $this->endpointOrigin;
    }

    /**
     * Set clientId
     *
     * @param string $clientId
     *
     * @return SettingsProviderOauth
     */
    public function setClientId($clientId)
    {
        $this->clientId = $clientId;

        return $this;
    }

    /**
     * Get clientId
     *
     * @return string
     */
    public function getClientId()
    {
        return $this->clientId;
    }

    /**
     * Set clientSecret
     *
     * @param string $clientSecret
     *
     * @return SettingsProviderOauth
     */
    public function setClientSecret($clientSecret)
    {
        $this->clientSecret = $clientSecret;

        return $this;
    }

    /**
     * Get clientSecret
     *
     * @return string
     */
    public function getClientSecret()
    {
        return $this->clientSecret;
    }

    /**
     * Set authorizationEndpoint
     *
     * @param string $authorizationEndpoint
     *
     * @return SettingsProviderOauth
     */
    public function setAuthorizationEndpoint($authorizationEndpoint)
    {
        $this->authorizationEndpoint = $authorizationEndpoint;

        return $this;
    }

    /**
     * Get authorizationEndpoint
     *
     * @return string
     */
    public function getAuthorizationEndpoint()
    {
        return $this->authorizationEndpoint;
    }

    /**
     * Set tokenEndpoint
     *
     * @param string $tokenEndpoint
     *
     * @return SettingsProviderOauth
     */
    public function setTokenEndpoint($tokenEndpoint)
    {
        $this->tokenEndpoint = $tokenEndpoint;

        return $this;
    }

    /**
     * Get tokenEndpoint
     *
     * @return string
     */
    public function getTokenEndpoint()
    {
        return $this->tokenEndpoint;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return SettingsProviderOauth
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return SettingsProviderOauth
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return SettingsProviderOauth
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return SettingsProviderOauth
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->last_update_uid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->last_update_uid;
    }

    /**
     * Set redirectUri
     *
     * @param string $redirectUri
     *
     * @return SettingsProviderOauth
     */
    public function setRedirectUri($redirectUri)
    {
        $this->redirectUri = $redirectUri;

        return $this;
    }

    /**
     * Get redirectUri
     *
     * @return string
     */
    public function getRedirectUri()
    {
        return $this->redirectUri;
    }
}
