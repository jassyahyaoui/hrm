<?php

namespace OAuth2WpBundle\Listener;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use OAuth2WpBundle\OAuth2Wp\OAuth;

class RedirectionListener {

    //put your code here
    private $container;

    public function __construct(ContainerInterface $container) {
        $this->container = $container;
        $this->router = $container->get('router');
    }

    public function onKernelRequest(GetResponseEvent $event) {

        $route = $event->getRequest()->attributes->get('_route');

        $c_uri = $this->container->get('session')->get('uri');
        if (is_null($c_uri)) {
            $uri = $this->container->get('request')->query->get('uri');
        } else {
            $uri = $this->container->get("session")->get('uri');
        }

        // only pass if uri existing in data list of uri
        if (in_array($uri, array('absence', 'collaborator', 'expense', 'dashboard', 'bonus', 'export', 'payVariation', 'additionalHours','transport','remunerations'))) {


            $this->container->get("session")->set("uri", $uri);
            /*   $this->container->get("session")->remove('uri');
              $this->container->get("session")->remove('access_token');die(); */
            $_oauth = new OAuth($this->container);
            if (!isset($_SERVER['HTTP_REFERER'])) {
                die('Error Client ID');
            }

            $OrgPath = $_oauth->getClientUrl($_SERVER['HTTP_REFERER']);
            $o_serverData = $_oauth->getAuthenticationServerFromDB($OrgPath);
            if ($OrgPath == '' || !isset($o_serverData[0])) {
                die('No such data');
            }

            if ($this->container->get("session")->has('access_token') != true) {
                $_oauth->setToken($this->container->get('request'), $o_serverData[0]->getClientId(), $o_serverData[0]->getClientSecret(), $o_serverData[0]->getTokenEndpoint(), $o_serverData[0]->getAuthorizationEndpoint(), $o_serverData[0]->getRedirectUri()
                );
            }

            $url = rtrim($o_serverData[0]->getEndpointOrigin(), "/") . "?oauth=me&access_token=" . $_oauth->getToken($this->container->get('request'));

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            $output = curl_exec($ch);

            // convert response
            $user_data = json_decode($output, true);


            if (isset($user_data['error'])) {
                die($user_data['error_description']);
            }

            // check if json format from sandbox or dev
            if (isset($user_data['user_email'])) {
                $user_data = $this->_rename_arr_key($user_data, array('username' => array('user_login'),
                    'email' => array('user_email'),
                    /*  'first_name' => array('display_name'),
                      'last_name' => array('display_name'), */
                    'roles' => array('user_role', 'ToArray'),
                        // add another field if wanted..
                ));
            }
            $o_provider = $_oauth->getProvider( $o_serverData[0]->getProviderId() );
            
            if(!is_object($o_provider))
            {
                die('Error Client ID');
            }
            
            $user = $_oauth->getUser($user_data['username'], $o_provider); //profile.php??

            if (is_object($user)) {

                // for update
                $_oauth->registerUser($user, $user_data, true, $o_provider);
                $username = $user->getUsername();
            } else {

                // for create
                $ret_regster = $_oauth->registerUser(null, $user_data, false, $o_provider);

                if ($ret_regster == false) {
                    die('error..1   ');
                }
                $username = $user_data['username'];
            }

            // force user logout when connecting from PORTAIL to HRM 
            $ret_login = $_oauth->login($username, $o_serverData[0]);

            if ($ret_login == false) {
                die('error..2');
            }

            $company = $this->container->get("session")->get('employee')->getCompany();
            if($company)
                  $this->container->get("session")->set('company', $company->getId());
         
            $this->container->get("session")->remove("employee");
            $this->container->get("session")->remove('uri');
            $this->container->get("session")->remove('access_token');
            // convert and generate true router
            $c_genRouter = $this->ApplyToChangeUri($uri);
            $event->setResponse(new RedirectResponse(str_replace('%23', '#!', $this->router->generate($c_genRouter))));
        }// end if router
    }

    private function _rename_arr_key($arr, $fromArr) {

        foreach ($fromArr as $key => $value) {
            if (isset($fromArr[$key][1])) {
                if ($fromArr[$key][1] == 'ToArray') {
                    $arr[$key][] = $arr[$fromArr[$key][0]];
                }
            } else {
                $arr[$key] = $arr[$fromArr[$key][0]];
            }
        }
        foreach ($fromArr as $key => $value) {
            unset($arr[$fromArr[$key][0]]);
        }
        return $arr;
    }

    private function ApplyToChangeUri($uri) {
        if ($uri == 'expense') {
            return 'expense_single_page';
        }
        if ($uri == 'absence') {
            return 'absence_single_page';
        }
        if ($uri == 'collaborator') {
            return 'collaborator_single_page';
        }
        if ($uri == 'dashboard') {
            return 'dashboard_single_page';
        }
        if ($uri == 'recruitment') {
            return 'hiring_single_page';
        }
//        if ($uri == 'export') {
//            return 'export_single_page';
//        }  
//        if ($uri == 'payVariation'){
//        return 'paydayvariations_layout';
//        } 
//        if ($uri == 'additionalHours'){
//        return 'hours_layout';
//        }
//         if ($uri == 'transport'){
//        return 'hrm_transport_layout';
//        }    
       if ($uri == 'remunerations'){
           return 'remuneration_single_page';
           }        
    }

    private function logOut($container) {
        $container->get('request')->getSession()->invalidate();
        $container->get('security.token_storage')->setToken(null);
        return true;
    }

}
