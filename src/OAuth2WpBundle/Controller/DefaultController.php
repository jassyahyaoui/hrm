<?php

namespace OAuth2WpBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AcmeBundle\Form\UserType;
use Symfony\Component\HttpFoundation\JsonResponse;
use OAuth2WpBundle\OAuth2Wp\OAuth;

class DefaultController extends Controller
{
   
     public function logoutAction ()
    {
        if(! $this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY') ){
          return new JsonResponse(false);
        }
         
        $req_wp = $this->get('request')->query->get('wp');

        $this->get('security.token_storage')->setToken(null);
        $this->get('request')->getSession()->invalidate();
        $this->get('session')->clear();
        
       // check if request from WordPress
       if($req_wp)
       {
           if($req_wp == 'true')
           {
               return new JsonResponse(true);
           }           
       }
       return $this->redirect( $this->generateUrl('dashboard_index') );
    }
    
    private function _rename_arr_key($arr, $fromArr){
        
        foreach ($fromArr as $key => $value) {
            if(isset($fromArr[$key][1]))
            {
                if($fromArr[$key][1] == 'ToArray'){
                   $arr[$key][] = $arr[$fromArr[$key][0]]; 
                }
            }
            else {
                   $arr[$key] = $arr[$fromArr[$key][0]];
            }
            
                        
        }    
        foreach ($fromArr as $key => $value) {
            unset($arr[$fromArr[$key][0]]);
        }
        return $arr;
    }
    
}