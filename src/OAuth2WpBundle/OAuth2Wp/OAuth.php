<?php

namespace OAuth2WpBundle\OAuth2Wp;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use UserBundle\Entity\IsPublicProfils;
use UserBundle\Entity\HrmCompany;
use UserBundle\Entity\HrmEmployee;
use UserBundle\Entity\EmployeeContract;
use PayPeriodBundle\Entity\SettingsPayPeriod;
use OAuth2WpBundle\Entity\SettingsProviderOauth;
use SettingsBundle\Entity\hrm_employee_settings;

class OAuth {

    private $client;
    private $session;
    private $container;
    private $gtoUri;

    public function __construct($cntr) {
        $this->session = $cntr->get("session");
        $this->container = $cntr;
    }

    public function setToken(Request $request = null, $CLIENT_ID, $CLIENT_SECRET, $TOKEN_ENDPOINT, $AUTHORIZATION_ENDPOINT, $REDIRECT_URI) {
        $this->client = new \OAuth2\Client($CLIENT_ID, $CLIENT_SECRET);

        if (!$request->query->get('code')) {
            $auth_url = $this->client->getAuthenticationUrl($AUTHORIZATION_ENDPOINT, $REDIRECT_URI);

            header('Location: ' . $auth_url);
            die();
        } else {

            $params = array('code' => $request->query->get('code'), 'redirect_uri' => $REDIRECT_URI);
            $response = $this->client->getAccessToken($TOKEN_ENDPOINT, 'authorization_code', $params);

            if (isset($response['result']['error'])) {
                header('Location: ' . $REDIRECT_URI);
                die();
            }

            $access_token = $response['result']['access_token']; //un problem dans l'affichage
            $this->session->set("access_token", $access_token);
        }


        $this->client->setAccessToken($access_token);
    }

    //////get access token

    public function getToken(Request $request = null) {
        $token = $this->session->get('access_token');
        return $token;
    }

    ////fin get access token

    public function getDataRequest($path) {

        $this->client->setAccessToken($this->getToken());
        return $this->client->fetch($path);
    }

    public function getDataUrl($url) {
        return json_decode($url, true);
    }

    public function registerUser($userEntity = null, $user_data, $is_update = false, $oauth_server_provider) {
        $em = $this->container->get('doctrine')->getManager();

        if (!isset($user_data['ID'])) {
            return false;
        }

        $_fos_roles = array();
        foreach ($user_data['roles'] as $role) {

            if ($role == 'admin') {
                $_fos_roles[] = array('ROLE_RESPONSABLE');
            } else {
                $_fos_roles[] = array('ROLE_COLLABORATEUR');
            }
        }
        //$o_provider = $this->container->get('doctrine')->getRepository('AdminBundle:SettingsProvider')->find($oauth_server_provider->getProviderId());
        $userManager = $this->container->get('fos_user.user_manager');
        if ($is_update == true) {
            $user = $userEntity;
            
        } else {
            $user = $userManager->createUser();
        }


        $user->setUsername($user_data['username']);
        $user->setUsernameCanonical($user_data['username']);
        $user->setEmail($user_data['email']);
        $user->setEmailCanonical($user_data['email']);
        $user->setFirstName($user_data['first_name']);
        $user->setLastName($user_data['last_name']);
        $user->setLocked(0); // don't lock the user
        $user->setEnabled(1); // enable the user or enable it later with a confirmation token in the email
        $user->setPlainPassword($user_data['username']);
        $user->setRoles($_fos_roles[0]);
        $user->setCreateDate(new \DateTime('now'));
       // $user->setAvatars('avatar.png');
        /**** find company if exist **/     
        $user->setProvider($oauth_server_provider);

        // create or update company
        $company = $this->saveBelongCompany($em, $user_data, $oauth_server_provider, $is_update, $_fos_roles[0]);
        // $user->setCompany($company);
        $userManager->updateUser($user);
        $use_employee = $this->saveEmployeeBelongUser($em, $user, $oauth_server_provider, $company, $is_update, $_fos_roles[0]);
        
        /*** update createUid and UpdateUid by current user id **/
        $user->setCreateUid($user->getId());
        $user->setLastUpdateUid($user->getId());
        $em->persist($user);
        $em->flush($user);
        /*** END ADD/update createUid and UpdateUid by current user id **/
        
        $this->session->remove("employee"); // delete employe if exist on session
        $this->session->set("employee", $use_employee); // set default company to work with it
        if (!$is_update) {
            if (!empty($user)) {                
                $employeeContract = new EmployeeContract();
                $employeeContract->setEmployee($use_employee);
                $employeeContract->setProvider($oauth_server_provider);
                $em->persist($employeeContract);
                $em->flush($employeeContract);
                
                // add emplyee setting
              $prefrences =  $this->saveSettingsBelongEmployee($em, $user, $use_employee);
                // update employe with contract
                $use_employee->setPreference($prefrences);
                $use_employee->setEmployeeContract($employeeContract);
                $em->persist($use_employee);
                $em->flush($use_employee);
            }

            // Open Pay period
            $this->openPayPeriod($em, $use_employee, $company);
            // create new ligne on IsPublicProfils with all field is false
            //$this->addNewPPublicProfil($user); 
        }

        return true;
    }

    public function login($username, $o_serverData) {
        $em = $this->container->get('doctrine');
        $repo = $em->getRepository("UserBundle:hrm_user"); //Entity Repository
        $user = $repo->findOneBy(array("username" => $username, 'provider'=>$o_serverData->getProviderId()));
        if (!$user) {
            return false;
        } else {
            $token = new UsernamePasswordToken($user, $user->getPassword(), "main", $user->getRoles());
            $this->container->get("security.context")->setToken($token); //now the user is logged in
            //now dispatch the login event
            $request = $this->container->get("request");
            $event = new InteractiveLoginEvent($request, $token);
            $this->container->get("event_dispatcher")->dispatch("security.interactive_login", $event);

            /** Update hrm_settings_provider_oauth fields * */
            $o_serverData->setCreateDate(new \DateTime('now'));
            $o_serverData->setCreateUid($user);
            $o_serverData->setLastUpdate(new \DateTime('now'));
            $o_serverData->setLastUpdateUid($user);
            $em->getManager()->persist($o_serverData);
            $em->getManager()->flush();
            /** END Update hrm_settings_provider_oauth fields * */
            return $user;
        }
    }

    public function getUser($username, $provider) {
        $userManager = $this->container->get('fos_user.user_manager');
        return $userManager->findUserBy(array("username" => $username, 'provider'=>$provider));
    }

    private function addNewPPublicProfil($user) {
        $c_publicProfil = new IsPublicProfils();
        $c_publicProfil->setEmployee($user);
//Add a comment to this line
        $em = $this->container->get('doctrine')->getManager();
        $em->persist($c_publicProfil);
        $em->flush($c_publicProfil);
    }

    private function openPayPeriod($em, $use_employee, $company) {
        $_f_date = new \DateTime('first day of this month');
        $_l_date = new \DateTime('last day of this month');

        $_isExistPayPeriod = $this->_isHaveOpenPayPeriod($em, $_f_date, $use_employee, $company);
        if ($_isExistPayPeriod) {
            return;
        }

        $payperiods = new SettingsPayPeriod();
        $payperiods->setStartDate($_f_date);
        $payperiods->setEndDate($_l_date);
        $payperiods->setStatus('open');
        $payperiods->setCompanyId($company);
        $payperiods->setCreateUid($use_employee);
        $payperiods->setLastUpdateUid($use_employee);

        $payperiods->setCreateDate(new \DateTime('now'));
        $payperiods->setLastUpdate(new \DateTime('now'));
        $em->persist($payperiods);
        $em->flush();
    }

    private function _isHaveOpenPayPeriod($em, $_date, $use_employee, $company) {
        return $em->getRepository('PayPeriodBundle:SettingsPayPeriod')
                        ->isHaveOpenPayPeriodByMonth(array('date' => $_date->format('Y-m-d'),
                            'companyId' => $company));
    }

    public function getAuthenticationServerFromDB($OrgPath) {
        $em = $this->container->get('doctrine')->getManager();

        return $em->getRepository('OAuth2WpBundle:SettingsProviderOauth')
                        ->findHostBy(array('endpointOrigin' => $OrgPath));
    }

    private function isTrueParseUrlSchema($_host) {
        /** check Schema * */
        if (!isset($_host['scheme'])) {
            return false;
        }
        if (!in_array($_host['scheme'], array('http', 'https'))) {
            return false;
        }
        if (!isset($_host['host'])) {
            return false;
        }
        if ($_host['scheme'] === '') {
            return false;
        }
        return true;
    }

    public function getClientUrl($_host) {
        $c_parse_url = parse_url($_host);
        $isOkUrl = $this->isTrueParseUrlSchema($c_parse_url);
        if ($isOkUrl) {
            return $c_parse_url['scheme'] . '://' . $c_parse_url['host'];
        }
        return '';
    }

    private function saveBelongCompany($em, $user_data, $o_provider, $is_update, $_fos_roles) {

        if ($is_update || in_array('ROLE_COLLABORATEUR', $_fos_roles)) {
            $company = $this->container->get('doctrine')->getRepository('UserBundle:HrmCompany')->findOneBy(array('external_ref' => $user_data['company_id']));

            //$company = new HrmCompany();
            if (!is_object($company)) {

                $company = new HrmCompany();
            }
        } else {
            $company = new HrmCompany();
        }

        // if (in_array('ROLE_RESPONSABLE', $_fos_roles[0])) /** create company if user role  ROLE_RESPONSABLE * */ {

        $company->setExternalRef($user_data['company_id']);
        $company->setName($user_data['company_name']);
        $company->setAdress($user_data['address_1']);
        $company->setPostalCode($user_data['postcode']);
        $company->setPhoneNumber($user_data['phone']);
        $company->setFax($user_data['fax']);
        $company->setWebSite($user_data['website']);
        $company->setEmail($user_data['company_email']);
        $company->setSiret($user_data['siret_code']);
        $company->setSiren($user_data['siren_code']);
        $company->setCapitalSocial($user_data['capital']);
        $company->setTva($user_data['tva']);
        $company->setRm($user_data['rm']);
        $company->setRcs($user_data['rcs']);

        $company->setState($user_data['state']);
        //a verifier
        $company->setNaf($user_data['ape']);
        $company->setCountry($user_data['country']);
        $company->setProvider($o_provider);
        $em->persist($company);
        $em->flush();

        return $company;
    }

    private function saveEmployeeBelongUser($em, $user, $o_provider, $company, $is_update, $_fos_roles) {

        if ($is_update || in_array('ROLE_COLLABORATEUR', $_fos_roles)) {
            $employee = $this->container->get('doctrine')->getRepository('UserBundle:HrmEmployee')->findOneBy(array('company' => $company->getId(), 'user' => $user->getId()));
            if (!is_object($employee)) {
                $employee = new HrmEmployee();
            }
        } else {
            $employee = new HrmEmployee();
        }

        $employee->setCompany($company);
        $employee->setUser($user);
        $employee->setProvider($o_provider);
        $em->persist($employee);
        $em->flush();

        return $employee;
    }

    public function getProvider($providerId)
    {
        return $this->container->get('doctrine')->getRepository('AdminBundle:SettingsProvider')->find($providerId);
    }
    
    
    public function saveSettingsBelongEmployee($em, $user, $employee)
    {
        $E_employee_settings = new hrm_employee_settings();
        
        $E_employee_settings->setEmployee($employee);
        $E_employee_settings->setCreateUid($employee->getId());
        $E_employee_settings->setLastUpdateUid($employee->getId());
        $E_employee_settings->setLocal('fr');
        $E_employee_settings->setFormat('fr-fr');
        $E_employee_settings->setCreateDate(new \DateTime('now'));
        $E_employee_settings->setLastUpdate(new \DateTime('now'));
        $em->persist($E_employee_settings);
        $em->flush();
        return $E_employee_settings;
    }
}
