<?php

namespace SettingsBundle\DependencyInjection;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAware;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\EntityManager;

/**
 * @property  token_storage
 */
class SettingsPreferences {

    private $token_storage;
    private $session;
    protected $em;
    private $company;
    private $provider;

    /**
     * @param SecurityContextInterface $security
     */
    public function __construct(TokenStorage $token_storage, Container $container, EntityManager $entityManager) {
        $this->container = $container;
        $this->session = $this->container->get('session');
        $this->token_storage = $token_storage;
        $this->em = $entityManager;
    }

    public function getUserId() {
        $userId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getUser()->getId();
        //$userId = $this->token_storage->getToken()->getUser()->getId();
        return $userId;
    }

    public function getEmployeeId() {
        $userId = $this->token_storage->getToken()->getUser()->getId();
        $companyId = $companyId = $this->container->get('session')->get('company');
        $employe = $this->em->createQueryBuilder()
                ->select('u')
                ->from('UserBundle:HrmEmployee', 'u')
                ->leftJoin('u.user', 'ue')
                ->leftJoin('u.company', 'c')
//                        ->leftJoin('c.provider', 'p')
                ->where('c.id = :companyId')
                ->setParameter('companyId', $companyId)
                ->AndWhere('ue.id = :userId')
                ->setParameter('userId', $userId)
                ->AndWhere("ue.roles LIKE '%ROLE_COLLABORATEUR%' OR ue.roles LIKE '%ROLE_RESPONSABLE%'")
                // ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        if ($employe)
            return $employe->getId();
        return null;
    }

    public function getAvatars() {
        $avatars = $this->getEmpData()->getAvatars();
        return $avatars;
    }

    public function getUsername() {
        $userName = $this->getEmpData()->getUser()->getUsername();
        return $userName;
    }

    public function getLocal() {
        if (empty($this->getEmpData()->getPreference())) {
            $local = 'fr';
        } else {
            $local = $this->getEmpData()->getPreference()->getLocal();
        }
        return $local;
    }

    public function getFormat() {
        if (empty($this->getEmpData()->getPreference())) {
            $format = 'fr-fr';
        } else {
            $format = $this->getEmpData()->getPreference()->getFormat();
        }
        return $format;
    }

    public function getProvider() {
        $companyId = $this->container->get('session')->get('company');
        $provider = $this->em->createQueryBuilder()
                ->select('c,p')
                ->from('UserBundle:HrmCompany', 'c')
                ->leftJoin('c.provider', 'p')
                ->where('c.id = :companyId')
                ->setParameter('companyId', $companyId)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        if ($provider == null)
            return ( $providerId = null);
        return $providerId = $provider->getId();
    }

    public function getProviderId() {

        $company = $this->getCompany();
        //  var_dump($this->container->get('session')->get('company'));  die();

        if ($company->getProvider()) {
            return $this->getCompany()->getProvider()->getId();
        } else {
            return null;
        }
    }

    public function getCompanyId() {
        $verify = $this->container->get('session')->get('company');
        if (empty($verify)) {
            $companyId = null;
        } else {
            $companyId = $this->container->get('session')->get('company');
        }
        return $companyId;
    }

    public function getCompany() {
        $companyId = $this->container->get('session')->get('company');
        $company = $this->em->createQueryBuilder()
                ->select('c')
                ->from('UserBundle:HrmCompany', 'c')
                ->where('c.id = :companyId')
                ->setParameter('companyId', $companyId)
                ->leftJoin('c.provider', 'p')
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        if ($company == null) {
            return 'sorry no company with id : ' . $companyId;
        }
        return $company;
    }

    public function getCompanies() {
        $providerId = $this->container->get('session')->get('provider');
        $companies = $this->em->createQueryBuilder()
                ->select('c')
                ->from('UserBundle:HrmCompany', 'c')
                ->leftJoin('c.provider', 'p')
                ->where('p.id = :providerId')
                ->setParameter('providerId', $providerId)
                ->getQuery()
                ->getOneOrNullResult();
        return $companies;
    }

    public function getPortalreturnurl() {
        if (empty($this->getEmpData()->getCompany()->getProvider())) {
            $portalReturnUrl = "https://www.fabereo.com/";
        } else {
            $portalReturnUrl = $this->getEmpData()->getCompany()->getProvider()->getPortalReturnUrl();
        }
        return $portalReturnUrl;
    }

    public function getHelpUrl() {
        if (empty($this->getEmpData()->getCompany()->getProvider())) {
            $helpUrl = "https://www.fabereo.com/";
        } else {
            $helpUrl = $this->getEmpData()->getCompany()->getProvider()->getHelpUrl();
        }
        return $helpUrl;
    }

    public function getIsCompanyAllowed() {
        if (empty($this->getEmpData()->getCompany()->getProvider())) {
            $allowed = false;
        } else {
            if ($this->getEmpData()->getCompany()->getProvider()->getIsCompanyAllowed() == null) {
                $allowed = false;
            } else {
                $allowed = $this->getEmpData()->getCompany()->getProvider()->getIsCompanyAllowed();
            }
        }
        return $allowed;
    }

    public function getProviders() {
        $verify = $this->getEmpData();
        if (empty($verify)) {
            $providers = null;
        } else {
            $providers = $this->getEmpData();
        }
        return $providers;
    }

    // fonction gÃ©nÃ©rique qui rÃ©cupÃ¨rer Employee via company_id
    public function getEmpData() {
        // fonction qui rÃ©cupÃ¨re l'employee depuis variable company_id passer en params + info user dans getToken()->getUser() (provider, id..etc)
        $_obj_company = $this->container->get('doctrine')->getEntityManager()->getRepository('UserBundle:HrmCompany')->find($this->container->get('session')->get('company'));
        if (!(is_object($_obj_company)))
            return null;
        // get single employee from user_id, company_id, provider_id
        $_emp = $this->container->get('doctrine')->getEntityManager()->getRepository('UserBundle:HrmEmployee')->FindOneBy(array('user' => $this->container->get('security.context')->getToken()->getUser(),
            'company' => $_obj_company));

        if (is_object($_emp))
            return $_emp;

        return null;
    }

    public function companyId() {
        return $this->container->get('session')->get('company');
    }

    public function providersMenuList($first = null) {
        $provider_id = $this->getEmpData()->getCompany()->getProvider()->getId();

        $list_menu = $this->container->get('doctrine')->getEntityManager()->getRepository('AdminBundle:hrm_settings_provider_menu')->findAllMenuShowingByProvidres($provider_id);
        if ($first == 1) {
            return $this->getFistBaseMenus($list_menu);
        } else
            return $list_menu;
    }

    private function getFistBaseMenus($param) {
        $val = "";
        foreach ($param as $value) {
            if ($value['setting_menu']['associatedTo'] == NULL) {
                $val = $value;
                break;
            }
        }
        return $val;
    }

    public function getBaseCurrencyIsoCode() {
        if (empty($this->getEmpData()->getCompany()->getProvider())) {
            $base_currency_iso_code = "EUR - Euro";
        } else {
            $base_currency_iso_code = $this->getEmpData()->getCompany()->getProvider()->getBaseCurrencyIsoCode();
        }
        return $base_currency_iso_code;
    }

    public function getLocaleCode() {
        if (empty($this->getEmpData()->getCompany()->getProvider())) {
            $locale_code = "fr-fr";
        } else {
            $locale_code = $this->getEmpData()->getCompany()->getLocaleCode();
        }
        return $locale_code;
    }

}
