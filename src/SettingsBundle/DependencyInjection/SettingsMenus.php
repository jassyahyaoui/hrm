<?php

namespace SettingsBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;

class SettingsMenus
{

    private $router;
    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;

    public function __construct(Container $container)
    {
        $this->router = $container->get('router');
        $this->container = $container;
    }

    public function onKernelRequest(FilterControllerEvent $event) {
        // TO DO REMOVE THIS LINE
        return;
        try{
            $preference = $this->container->get('settingsbundle.preference.service');
            $request = $event->getRequest();
            // Matched route
            $_route  = $request->attributes->get('_route');
            $provider_id = $preference->getEmpData()->getCompany()->getProvider()->getId();
            $menu_obj = $this->container->get('doctrine')->getEntityManager()
                                 ->getRepository('AdminBundle:hrm_settings_menu')->findBy(array('routeLinks'=>$_route));
            $accessRouter = $this->container->get('doctrine')->getEntityManager()
                                 ->getRepository('AdminBundle:hrm_settings_provider_menu')->findIfDislpayedMenuByRouteLinks($_route, $provider_id);

            if(empty($accessRouter) && !empty($menu_obj))
            {

                 $active_menu_item = $preference->providersMenuList(1);
                 if(empty($active_menu_item)) return;
                 $redirectUrl = $active_menu_item['setting_menu']['routeLinks'];
                // dump($this->router->generate($redirectUrl));die();
                 $response = new RedirectResponse($this->router->generate($redirectUrl));
                 $response->send();
            }
        }catch(\Exception $e)
        {
            return;
        }

        return;
    }
}