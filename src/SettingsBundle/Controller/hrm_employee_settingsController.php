<?php

namespace SettingsBundle\Controller;

use SettingsBundle\Entity\hrm_employee_settings;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Hrm_employee_setting controller.
 *
 */
class hrm_employee_settingsController extends Controller {

    public function settingsContactAction($id) {
        $data = $this->getDoctrine()->getRepository('UserBundle:HrmEmployee')->findContact($id);
        if($data)
        {
            $emp = $data["0"]["0"];
            unset($data["0"]["0"]);
            $data = array_merge($data["0"], $emp);
        }
        return array('applicantContact' => $data);
    }
    
    public function settingsAction() {
      $provider_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();

      $settingsStudyTypes = $this->getDoctrine()->getRepository('AdminBundle:SettingsStudyType')->StudyTypeByProvider($provider_id);
      $settingsStudyLevels = $this->getDoctrine()->getRepository('AdminBundle:SettingsStudyLevel')->StudyLevelByProvider($provider_id);
      $settingsPlannedJourney = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_plannedjourney_type')->PlannedJourneyByProvider($provider_id);
      $settingsPeriodicity = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_periodicity')->PeriodicityByProvider($provider_id);
      $typecontracts = $this->getDoctrine()->getRepository('AdminBundle:TypeContracts')->ListTypeContractsByProvider($provider_id);
      $settingsCustomerType = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_customertype')->CustomertypeByProvider($provider_id);
      $settingsLanguage = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_language')->TechnologyByProvider($provider_id);
      $settingsLanguageLevel = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_language_level')->LanguageLevelByProvider($provider_id);
      $settingsTechnology = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_technology')->TechnologyByProvider($provider_id);
      $settingsTechnologyLevel = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_technology_level')->TechnologyLevelByProvider($provider_id);
      $settingsJob= $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_job')->JobByProvider($provider_id);
      $settingsIndustrySector = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_industrysector')->IndustrySectorByProvider($provider_id);
      
      return array(
          'settingsSettingsStudyTypes' => $settingsStudyTypes,
          'settingsStudyLevels' => $settingsStudyLevels,
          'settingsPlannedJourney' => $settingsPlannedJourney,
          'settingsPeriodicity' => $settingsPeriodicity,
          'settingsTypecontracts' =>$typecontracts,
          'settingsCustomerType' =>$settingsCustomerType,
          'settingsLanguage' =>$settingsLanguage,
          'settingsLanguageLevel' =>$settingsLanguageLevel,
          'settingsTechnology' =>$settingsTechnology,
          'settingsTechnologyLevel' =>$settingsTechnologyLevel,
          'settingsJob' =>$settingsJob,
          'settingsIndustrySector' =>$settingsIndustrySector,

      );
  }
    
    /**
     * Api check action to do in preference Add Or update if has existing values
     */
    public function checkAction($id) {

        $em = $this->getDoctrine()->getManager();
        $infos = $em->getRepository('SettingsBundle:hrm_employee_settings')->getPreferences($id);
        $result = [];

        if (empty($infos)) {
            $data = new \Symfony\Component\BrowserKit\Response($result = ['action' => 'ADD', 'preferenceId' => '', 'local' => '', 'format' => '']);
        } else {
            $data = new \Symfony\Component\BrowserKit\Response($result = ['action' => 'UPDATE', 'preferenceId' => $infos[0]['id'], 'local' => $infos[0]['local'], 'format' => $infos[0]['format']]);
        }

        return $data;
    }

    public function addAction(Request $request) {

        $preferences = new Hrm_employee_settings();

        $form = $this->createForm('SettingsBundle\Form\hrm_employee_settingsType', $preferences);
        $form->handleRequest($request);


        $body = $request->getContent();
        $data = json_decode($body, true);

        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {

            $current_user_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getId();

            $form->submit($data);

            $preferences->setEmployee($em->getRepository('UserBundle:HrmEmployee')->find($current_user_id));
            //Add default value on create Action
            $preferences->setCreateDate(new \DateTime('now'));
            $preferences->setCreateUid($current_user_id);
            $preferences->setLastUpdateUid($current_user_id);
            $preferences->setLastUpdate(new \DateTime('now'));


            $em->persist($preferences);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('API POST Method', 200);
            return $response;
        }
    }



    /**
     * Displays a form to update an existing ik entity.
     *
     */
    public function updateAction(Request $request, Hrm_employee_settings $preference) {
        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);
        $entity = $em->getRepository('SettingsBundle:Hrm_employee_settings')->find($preference);

        $update_form = $this->createForm('SettingsBundle\Form\hrm_employee_settingsType', $entity);

        if ($request->isMethod('PUT')) {

            $securityContext = $this->container->get('security.authorization_checker');
            //get current connected user
            $current_user_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getId();

            $update_form->submit($data);

            $preference->setEmployee($em->getRepository('UserBundle:HrmEmployee')->find($current_user_id));
            $preference->setLastUpdate(new \DateTime('now'));
            $preference->setLastUpdateUid($current_user_id);

            $em->persist($entity);
            $em->flush();

            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }




    /**
     * Lists all hrm_employee_setting entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $hrm_employee_settings = $em->getRepository('SettingsBundle:hrm_employee_settings')->findAll();

        return $this->render('hrm_employee_settings/index.html.twig', array(
            'hrm_employee_settings' => $hrm_employee_settings,
        ));
    }

    /**
     * Creates a new hrm_employee_setting entity.
     *
     */
    public function newAction(Request $request) {
        $hrm_employee_setting = new Hrm_employee_settings();
        $form = $this->createForm('SettingsBundle\Form\hrm_employee_settingsType', $hrm_employee_setting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($hrm_employee_setting);
            $em->flush();

            return $this->redirectToRoute('dashboard_index');
        }

        return $this->render('hrm_employee_settings/preference.html.twig', array(
            'hrm_employee_setting' => $hrm_employee_setting,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_employee_setting entity.
     *
     */
    public function showAction(hrm_employee_settings $hrm_employee_setting) {
        $deleteForm = $this->createDeleteForm($hrm_employee_setting);

        return $this->render('hrm_employee_settings/show.html.twig', array(
            'hrm_employee_setting' => $hrm_employee_setting,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_employee_setting entity.
     *
     */
    public function editAction(Request $request, hrm_employee_settings $hrm_employee_setting) {
        $deleteForm = $this->createDeleteForm($hrm_employee_setting);
        $editForm = $this->createForm('SettingsBundle\Form\hrm_employee_settingsType', $hrm_employee_setting);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('preferences_edit', array('id' => $hrm_employee_setting->getId()));
        }

        return $this->render('hrm_employee_settings/edit.html.twig', array(
            'hrm_employee_setting' => $hrm_employee_setting,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a hrm_employee_setting entity.
     *
     */
    public function deleteAction(Request $request, hrm_employee_settings $hrm_employee_setting) {
        $form = $this->createDeleteForm($hrm_employee_setting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_employee_setting);
            $em->flush();
        }

        return $this->redirectToRoute('preferences_index');
    }

    /**
     * Creates a form to delete a hrm_employee_setting entity.
     *
     * @param hrm_employee_settings $hrm_employee_setting The hrm_employee_setting entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_employee_settings $hrm_employee_setting) {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('preferences_delete', array('id' => $hrm_employee_setting->getId())))
            ->setMethod('DELETE')
            ->getForm()
            ;
    }
    public function singlePageLayoutAction(Request $request) {
        $hrm_employee_setting = new Hrm_employee_settings();
        $form = $this->createForm('SettingsBundle\Form\hrm_employee_settingsType', $hrm_employee_setting);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($hrm_employee_setting);
            $em->flush();

            return $this->redirectToRoute('dashboard_index');
        }
        return $this->render('hrm_employee_settings/single_page_layout.html.twig', array(
            'hrm_employee_setting' => $hrm_employee_setting,
            'form' => $form->createView(),
        ));
    }
}
