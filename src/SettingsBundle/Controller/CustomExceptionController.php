<?php

namespace SettingsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Bundle\TwigBundle\Controller\ExceptionController;


class CustomExceptionController extends ExceptionController
{
    public function showExceptionAction()
    {
        return $this->render('TwigBundle/views/Exception/error.404.html.twig');
    }
    
//     public function showAction()
//    {
//        return $this->render('TwigBundle/views/Exception/error.404.html.twig');
//    }
    
//     public function showException()
//    {
//        return $this->render('TwigBundle/views/Exception/error.404.html.twig');
//    }
}
