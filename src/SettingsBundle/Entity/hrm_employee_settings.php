<?php

namespace SettingsBundle\Entity;

/**
 * hrm_employee_settings
 */
class hrm_employee_settings
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $local;

    /**
     * @var string
     */
    private $date;

    /**
     * @var string
     */
    private $amount;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set local
     *
     * @param string $local
     *
     * @return hrm_employee_settings
     */
    public function setLocal($local)
    {
        $this->local = $local;

        return $this;
    }

    /**
     * Get local
     *
     * @return string
     */
    public function getLocal()
    {
        return $this->local;
    }

    /**
     * Set date
     *
     * @param string $date
     *
     * @return hrm_employee_settings
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set amount
     *
     * @param string $amount
     *
     * @return hrm_employee_settings
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return string
     */
    public function getAmount()
    {
        return $this->amount;
    }
    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * @var integer
     */
    private $createUid;

    /**
     * @var \DateTime
     */
    private $lastUpdate;

    /**
     * @var integer
     */
    private $lastUpdateUid;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $employee;


    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_employee_settings
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_employee_settings
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_employee_settings
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_employee_settings
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->lastUpdateUid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->lastUpdateUid;
    }

    /**
     * Set employee
     *
     * @param \UserBundle\Entity\HrmEmployee $employee
     *
     * @return hrm_employee_settings
     */
    public function setEmployee(\UserBundle\Entity\HrmEmployee $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getEmployee()
    {
        return $this->employee;
    }
    /**
     * @var string
     */
    private $format;


    /**
     * Set format
     *
     * @param string $format
     *
     * @return hrm_employee_settings
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return string
     */
    public function getFormat()
    {
        return $this->format;
    }
}
