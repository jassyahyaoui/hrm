<?php

namespace SettingsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class hrm_employee_settingsType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $langue = array("fr" => "Français", "en" => "English");
        $format = array("fr-fr" => "France", "en-us" => "United States");

        $builder
                ->add('local', 'choice', array(
                    'label' => 'Langue',
                    'choices' => $langue,
                    'empty_value' => 'Sélectionner une langue',
                    'required' => true,
                    'attr' => array('class' => 'form-control',
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label asterix'
                    )
                        )
                )
                ->add('format', 'choice', array(
                    'label' => 'Format des dates et des montants',
                    'choices' => $format,
                    'empty_value' => 'Sélectionner un format',
                    'required' => true,
                    'attr' => array('class' => 'form-control',
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => ' col-sm-3 control-label asterix'
                    )
                        )
                )

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'SettingsBundle\Entity\hrm_employee_settings'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'settingsbundle_hrm_employee_settings';
    }

}
