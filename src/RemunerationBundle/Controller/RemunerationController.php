<?php

namespace RemunerationBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RemunerationController extends Controller
{
    public function layoutAction() {
        return $this->render('remuneration/layout.html.twig');
    }
    public function singlePageLayoutAction() {
        return $this->render('remuneration/single_page_layout.html.twig');
    }
}
