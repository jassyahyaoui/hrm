<?php

namespace HiringBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class hrm_document_jobDescriptionType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name', 'text', array(
                    'required' => false,
                    'label' => 'name',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('departement', 'text', array(
                    'required' => false,
                    'label' => 'departement',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('superior', 'entity', array(//
                    'label' => 'superior',
                    'empty_value' => 'Sélectionner un responsable',
                    'required' => false,
                    'class' => 'UserBundle\Entity\HrmEmployee',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('u')
                                ->leftjoin('u.user', 'e')
                                ->leftjoin('u.company', 'c')
                                ->where("e.roles LIKE '%RESPONSABLE%'")
                                ->AndWhere('c.id = :companyId')
                                ->setParameter('companyId', (int) $options['companyId'])
                        ;
                    },
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('assignement', 'textarea', array('required' => false,
                    'label' => 'assignement',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('plannedJourneyNumber', 'text', array('required' => false,
                    'label' => 'name',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('partTimeDayNumber', 'text', array('required' => false,
                    'label' => 'partTimeDayNumber',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right'
                    )
                ))
                ->add('startDate', 'date', array(
                    'label' => 'De',
                    'required' => false,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array(
                        'id' => 'startdate',
                        'date-directive' => ''
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('endDate', 'date', array(
                    'label' => 'à',
                    'required' => false,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array(
                        'id' => 'startdate',
                        'date-directive' => ''
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('remunerationAmountFrom', 'text', array('required' => false,
                    'label' => 'remunerationAmountFrom',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('remunerationAmountTo', 'text', array('required' => false,
                    'label' => 'remunerationAmountTo',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('remunerationOthers')
                ->add('plannedJourneyType', 'entity', array(
                    'label' => 'Planed Journey Type',
                    'required' => false,
                    'class' => 'AdminBundle\Entity\hrm_settings_plannedjourney_type',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('contractType', 'entity', array(
                    'label' => 'Contract type',
                    'required' => false,
                    'class' => 'AdminBundle\Entity\TypeContracts',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('partTimeDayPeriodicity', 'entity', array(
                    'label' => 'Part time day periodicity',
                    'required' => false,
                    'class' => 'AdminBundle\Entity\hrm_settings_periodicity',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'HiringBundle\Entity\hrm_document_jobDescription',
            'provider' => null,
            'companyId' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'hiringbundle_hrm_document_jobdescription';
    }

}
