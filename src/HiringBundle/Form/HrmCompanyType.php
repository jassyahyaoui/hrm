<?php

namespace HiringBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class HrmCompanyType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('name')
                ->add('industrySector', 'entity', array(
                    'label' => 'Part time day periodicity',
                    'required' => false,
                    'class' => 'AdminBundle\Entity\hrm_settings_industrysector',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right '
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
               ->add('employeeNumber','integer', array(
                    'attr' => array(
                        'min' => '0',
                        'pattern' =>"^[0-9]*",
                         'onkeypress' => "return (event.charCode >= 48 && event.charCode <= 57)",
                    ),
                    'required' => false,
                ))
                ->add('adress')
                ->add('city')
                ->add('state')
                ->add('postalCode')
                ->add('country', 'country', array(
                    'label' => "{[{'COUNTRY' | translate}]}",
                    'empty_value' => "{[{'COUNTRY' | translate}]}",
                    'data' => 'FR',
                    'required' => false,
                ))
                ->add('jobAdress')
                ->add('jobCity')
                ->add('jobPostalCode')
                ->add('jobCountry', 'country', array(
                    'label' => "{[{'COUNTRY' | translate}]}",
                    'empty_value' => "{[{'COUNTRY' | translate}]}",
                    'data' => 'FR',
                    'required' => false,
                ))  
                ->add('description')
                ->add('customerType', 'entity', array(
                    'label' => 'Part time day periodicity',
                    'empty_value' => 'Sélectionner une clientèle',
                    'required' => false,
                    'class' => 'AdminBundle\Entity\hrm_settings_customertype',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right '
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('applicantContact', 'entity', array(
                    'label' => 'Employé',
                    'empty_value' => 'Sélectionner un employé',
                    'required' => false,
                    'class' => 'UserBundle\Entity\HrmEmployee',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('u')
                                ->leftjoin('u.user', 'e')
                                ->leftjoin('u.company', 'c')
                                ->where("e.roles LIKE '%COLLABORATEUR%' OR e.roles LIKE '%RESPONSABLE%'")
                                ->AndWhere('c.id = :companyId')
                                ->setParameter('companyId', (int) $options['companyId'])
                        ;
                    },
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right '
                    )
                ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\HrmCompany',
            'provider' => null,
            'companyId' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'userbundle_hrmcompany';
    }

}
