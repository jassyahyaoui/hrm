<?php

namespace HiringBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class hrm_document_jobApplicantStudyType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('name', null, array('required'=> false))
                ->add('documentJobDescription')
                ->add('studyTypeId', 'entity', array(
                    'label' => 'Sexe',
                    'required' => false,
                    'class' => 'AdminBundle\Entity\SettingsStudyType',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('studyLevelId', 'entity', array(
                    'label' => 'Sexe',
                    'required' => false,
                    'class' => 'AdminBundle\Entity\SettingsStudyLevel',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'HiringBundle\Entity\hrm_document_jobApplicantStudy',
            'provider' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'hiringbundle_hrm_document_jobapplicantstudy';
    }

}
