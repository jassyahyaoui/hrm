<?php

namespace HiringBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;
use HiringBundle\Form\hrm_document_jobApplicantCompanyType;
use HiringBundle\Form\hrm_document_jobApplicantStudyType;

class jobApplicantType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('jobApplicationStudy', new hrm_document_jobApplicantStudyType(), array(
                    'provider' => $options['provider'],
                ))
                ->add('ApplicantStudySup', 'checkbox', array(
                    'required' => false,
                    'label_attr' => array(
                     'class' => 'col-sm-3 control-label no-padding-right'
            )))
                ->add('ApplicantStudyComment', 'textarea', array(
                    'required' => false,
                ))
                ->add('applicantProfessionalExperience', 'integer', array(
                    'attr' => array(
                        'min' => '0',
                        'pattern' => "^[0-9]*",
                        'onkeypress' => "return event.charCode >= 48",
                    ),
                    'required' => false,
                ))
                ->add('jobApplicationCompany', new hrm_document_jobApplicantCompanyType(), array(
                    'provider' => $options['provider'],
                ))
                ->add('language_id', 'entity', array(
                    'label' => 'Langue',
//                    'empty_value' => "{[{'SELECT_LANGUAGE' | translate}]}",
                    'required' => false,
                    'class' => 'AdminBundle\Entity\hrm_settings_language',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('language_level_id', 'entity', array(
                    'label' => 'Niveau',
//                    'empty_value' => "{[{'SELECT_LEVEL' | translate}]}",
                    'required' => false,
                    'class' => 'AdminBundle\Entity\hrm_settings_language_level',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('technologyId', 'entity', array(
                    'label' => 'Niveau',
//                    'empty_value' => "{[{'SELECT_TECHNO' | translate}]}",
                    'required' => false,
                    'class' => 'AdminBundle\Entity\hrm_settings_technology',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('technologyLevelId', 'entity', array(
                    'label' => 'Niveau',
//                    'empty_value' => "{[{'SELECT_LEVEL' | translate}]}",
                    'required' => false,
                    'class' => 'AdminBundle\Entity\hrm_settings_technology_level',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('applicantOthersCompetenciesTags', new hrm_document_jobApplicantTagType($options['provider']), array('mapped' => false))
                 ->add('ApplicantExperienceComment', 'textarea', array(
                    'required' => false,
                ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'HiringBundle\Entity\hrm_document_jobDescription',
            'provider' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'hiringbundle_hrm_document_jobApplicantdescription';
    }

}
