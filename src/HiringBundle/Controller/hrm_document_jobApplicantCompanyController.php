<?php

namespace HiringBundle\Controller;

use HiringBundle\Entity\hrm_document_jobApplicantCompany;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Hrm_document_jobapplicantcompany controller.
 *
 */
class hrm_document_jobApplicantCompanyController extends Controller
{
    /**
     * Lists all hrm_document_jobApplicantCompany entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hrm_document_jobApplicantCompanies = $em->getRepository('HiringBundle:hrm_document_jobApplicantCompany')->findAll();

        return $this->render('hrm_document_jobapplicantcompany/index.html.twig', array(
            'hrm_document_jobApplicantCompanies' => $hrm_document_jobApplicantCompanies,
        ));
    }

    /**
     * Creates a new hrm_document_jobApplicantCompany entity.
     *
     */
    public function newAction(Request $request)
    {
        $hrm_document_jobApplicantCompany = new Hrm_document_jobapplicantcompany();
        $form = $this->createForm('HiringBundle\Form\hrm_document_jobApplicantCompanyType', $hrm_document_jobApplicantCompany);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($hrm_document_jobApplicantCompany);
            $em->flush();

            return $this->redirectToRoute('hrm_document_jobapplicantcompany_show', array('id' => $hrm_document_jobApplicantCompany->getId()));
        }

        return $this->render('hrm_document_jobapplicantcompany/new.html.twig', array(
            'hrm_document_jobApplicantCompany' => $hrm_document_jobApplicantCompany,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_document_jobApplicantCompany entity.
     *
     */
    public function showAction(hrm_document_jobApplicantCompany $hrm_document_jobApplicantCompany)
    {
        $deleteForm = $this->createDeleteForm($hrm_document_jobApplicantCompany);

        return $this->render('hrm_document_jobapplicantcompany/show.html.twig', array(
            'hrm_document_jobApplicantCompany' => $hrm_document_jobApplicantCompany,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_document_jobApplicantCompany entity.
     *
     */
    public function editAction(Request $request, hrm_document_jobApplicantCompany $hrm_document_jobApplicantCompany)
    {
        $deleteForm = $this->createDeleteForm($hrm_document_jobApplicantCompany);
        $editForm = $this->createForm('HiringBundle\Form\hrm_document_jobApplicantCompanyType', $hrm_document_jobApplicantCompany);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('hrm_document_jobapplicantcompany_edit', array('id' => $hrm_document_jobApplicantCompany->getId()));
        }

        return $this->render('hrm_document_jobapplicantcompany/edit.html.twig', array(
            'hrm_document_jobApplicantCompany' => $hrm_document_jobApplicantCompany,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a hrm_document_jobApplicantCompany entity.
     *
     */
    public function deleteAction(Request $request, hrm_document_jobApplicantCompany $hrm_document_jobApplicantCompany)
    {
        $form = $this->createDeleteForm($hrm_document_jobApplicantCompany);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_document_jobApplicantCompany);
            $em->flush();
        }

        return $this->redirectToRoute('hrm_document_jobapplicantcompany_index');
    }

    /**
     * Creates a form to delete a hrm_document_jobApplicantCompany entity.
     *
     * @param hrm_document_jobApplicantCompany $hrm_document_jobApplicantCompany The hrm_document_jobApplicantCompany entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_document_jobApplicantCompany $hrm_document_jobApplicantCompany)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('hrm_document_jobapplicantcompany_delete', array('id' => $hrm_document_jobApplicantCompany->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
