<?php

namespace HiringBundle\Controller;

use HiringBundle\Entity\hrm_document_jobDescription;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use HiringBundle\Form\hrm_document_jobDescriptionType;
use HiringBundle\Form\jobApplicantType;
use HiringBundle\Entity\hrm_document_jobApplicantStudy;
use HiringBundle\Entity\hrm_document_jobApplicantCompany;
use HiringBundle\Entity\hrm_document_jobApplicantLanguage;
use HiringBundle\Entity\hrm_document_jobApplicantTechnology;
use HiringBundle\Entity\hrm_document_jobApplicantTag;
use UserBundle\Entity\HrmCompany;
use UserBundle\Form\HrmCompanyType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\HttpFoundation\Response;

/**
 * Hrm_document_jobdescription controller.
 *
 */
class hrm_document_jobDescriptionController extends Controller {

    public function removeJobDescriptionAction(Request $request, hrm_document_jobDescription $job_description) {
        $id = $job_description->getId();
        if ($request->isMethod('DELETE')) {
            $em = $this->getDoctrine()->getManager();
            $entities = array(
                        0 => "hrm_document_jobApplicantStudy",
                        1 => "hrm_document_jobApplicantCompany",
                        2 => "hrm_document_jobApplicantLanguage",
                        3 => "hrm_document_jobApplicantTechnology",
                        4 => "hrm_document_jobApplicantTag",
            );
            foreach ($entities as $entity) {
                $em->createQuery('DELETE HiringBundle:' . str_replace('"', "", $entity) . ' e WHERE e.documentJobDescription = :id')
                        ->setParameter("id", $id)->execute();
            }
            $em->createQuery('DELETE HiringBundle:hrm_document_jobDescription e WHERE e.id = :id')
                    ->setParameter("id", $id)->execute();
            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }

    public function listAction() {
        $company = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $providerId = $this->container->get('settingsbundle.preference.service')->getProviderId();
        $documentJobDescriptions = $this->getDoctrine()->getRepository('HiringBundle:hrm_document_jobDescription')->jobDescriptionByCompany($company);
        $typecontracts = $this->getDoctrine()->getRepository('AdminBundle:TypeContracts')->TypeContractsByProvider($providerId);
        return new JsonResponse(array(
            'documentJobDescriptions' => $documentJobDescriptions, 'typecontracts' => $typecontracts,
        ));
    }

    /**
     * Lists all hrm_document_jobDescription entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $hrm_document_jobDescriptions = $em->getRepository('HiringBundle:hrm_document_jobDescription')->findAll();
        return $this->render('hrm_hiring/hrm_document_jobdescription/index.html.twig', array(
                    'hrm_document_jobDescriptions' => $hrm_document_jobDescriptions,
        ));
    }

    public function saveCompanyAction(Request $request) {
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $currentUser = $this->get('security.context')->getToken()->getUser();
        $user_provide_id = $this->container->get('settingsbundle.preference.service')->getProviderId();

        if (isset($data['jobId'])) {
            $job = $this->getDoctrine()->getRepository('HiringBundle:hrm_document_jobDescription')->find($data['jobId']);
        } else {
            $job = new hrm_document_jobDescription();
            $job->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
            $job->setCreateDate(new \DateTime('now'));
//            $job->setCreateUid($currentUser);
        }
        $company = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany();
        $form = $this->createForm(new HrmCompanyType(), $company);
        $form->handleRequest($request);
//        $form->submit($data);

        if (isset($data['customerType'])) {
            $customerType = $em->getRepository('AdminBundle:hrm_settings_customertype')->find($data['customerType']);
            if ($customerType)
                $company->setCustomerType($customerType);
        }
        if (isset($data['industrySector'])) {
            $industrySectorret = $em->getRepository('AdminBundle:hrm_settings_industrysector')->find($data['industrySector']);
            if ($industrySectorret)
                $company->setIndustrySector($industrySectorret);
        }

        if ((isset($data['jobAdress']))) {
            $job->setJobAdress($data['jobAdress']);
        }
        if ((isset($data['jobCity']))) {
            $job->setJobCity($data['jobCity']);
        }
        if ((isset($data['jobPostalCode']))) {
            $job->setJobPostalCode($data['jobPostalCode']);
        }
        if ((isset($data['jobCountry']))) {
            $job->setJobCountry($data['jobCountry']);
        }

        if ((isset($data['applicantContact']))) {
            $applicantContact = $em->getRepository('UserBundle:HrmEmployee')->find($data['applicantContact']);
            $job->setApplicantContact($applicantContact);
        }

        $job->setCompany($company);

        $company->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $em->persist($job);
//        $em->persist($company);
        $em->flush();

        if ($job->getCompany()) {
            $companyId = $job->getCompany()->getId();
        } else {
            $companyId = null;
        }
        return new JsonResponse(array('http_code' => 200,
            "message" => array("Élément Ajouté avec succès"),
            "companyId" => $companyId,
            "jobId" => $job->getId()
        ));
    }

    public function saveJobAction(Request $request) {
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $CurrentUser = $this->get('security.context')->getToken()->getUser();
        $user_provide_id = $this->container->get('settingsbundle.preference.service')->getProviderId();
        $company = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany();

        if (isset($json_data['jobId'])) {
            $job = $this->getDoctrine()->getRepository('HiringBundle:hrm_document_jobDescription')->find($json_data['jobId']);
//            $job->setLastUpdateUid($CurrentUser);
            $job->setLastUpdate(new \DateTime('now'));
        } else {
            $job = new hrm_document_jobDescription();
            $job->setCreateDate(new \DateTime('now'));
//            $job->setCreateUid($CurrentUser);
            $job->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        }

        $provider = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $form = $this->createForm('HiringBundle\Form\hrm_document_jobDescriptionType', $job, array(
            'provider' => $provider
        ));
        $form->handleRequest($request);
        $form->submit($json_data);

        if ((isset($json_data['partTimeDayPeriodicity'])) && is_int($json_data['partTimeDayPeriodicity'])) {
            $partTimeDayPeriodicity = $em->getRepository('AdminBundle:hrm_settings_periodicity')->find($json_data['partTimeDayPeriodicity']);
            $job->setPartTimeDayPeriodicity($partTimeDayPeriodicity);
        }
        if ((isset($json_data['contractType'])) && is_int($json_data['contractType'])) {
            $contractType = $em->getRepository('AdminBundle:TypeContracts')->find($json_data['contractType']);
            $job->setContractType($contractType);
        }
        if ((isset($json_data['partTimeDayPeriodicity'])) && is_int($json_data['partTimeDayPeriodicity'])) {
            $partTimeDayPeriodicity = $em->getRepository('AdminBundle:hrm_settings_periodicity')->find($json_data['partTimeDayPeriodicity']);
            $job->setPartTimeDayPeriodicity($partTimeDayPeriodicity);
        }
        if ((isset($json_data['plannedJourneyType'])) && is_int($json_data['plannedJourneyType'])) {
            $plannedJourneyType = $em->getRepository('AdminBundle:hrm_settings_plannedjourney_type')->find($json_data['plannedJourneyType']);
            $job->setPlannedJourneyType($plannedJourneyType);
        }

        if ((isset($json_data['superior']))) {
            $superior = $em->getRepository('UserBundle:HrmEmployee')->find($json_data['superior']);
            $job->setSuperior($superior);
        }

        $em->persist($job);
        $em->flush();

        return new JsonResponse(array('http_code' => 200,
            "message" => array("Élément Ajouté avec succès"),
            "companyId" => $company->getId(),
            "jobId" => $job->getId()
        ));
    }

    public function saveApplicantAction(Request $request) {
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $user_provide_id = $this->container->get('settingsbundle.preference.service')->getProviderId();
        $currentUser = $this->get('security.context')->getToken()->getUser();
        $company = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany();
        if (isset($data['jobId'])) {
            $job = $this->getDoctrine()->getRepository('HiringBundle:hrm_document_jobDescription')->find($data['jobId']);
//            $job->setLastUpdateUid($currentUser);
//            $job->setLastUpdate(new \DateTime('now'));
        } else {
            $job = new hrm_document_jobDescription();
            $job->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
            $job->setCreateDate(new \DateTime('now'));
//            $job->setCreateUid($currentUser);
        }
        
        
         if (isset($data['applicantProfessionalExperience']))
            $job->setApplicantProfessionalExperience($data['applicantProfessionalExperience']);
         else
             $job->setApplicantProfessionalExperience(null);
        
        if (isset($data['ApplicantExperienceComment']))
            $job->SetApplicantExperienceComment($data['ApplicantExperienceComment']);
        if (isset($data['ApplicantStudyComment']))
            $job->SetApplicantStudyComment($data['ApplicantStudyComment']);
        if (isset($data['ApplicantStudySup']))
            $job->SetApplicantStudySup($data['ApplicantStudySup']);
        $em->persist($job);
        $em->flush();

        if (isset($data['jobId']))
            $jobid = $data['jobId'];
        else {
            if ($job)
                $jobid = $job->getId();
        }

        $entities = array(
            0 => "hrm_document_jobApplicantStudy",
            1 => "hrm_document_jobApplicantCompany",
            2 => "hrm_document_jobApplicantLanguage",
            3 => "hrm_document_jobApplicantTechnology",
            4 => "hrm_document_jobApplicantTag",
        );

        foreach ($entities as $entity) {
            $query = $em->createQuery('DELETE HiringBundle:' . str_replace('"', "", $entity) . ' e WHERE e.documentJobDescription = :id')
                    ->setParameter("id", $jobid);
            $query->execute();
        }

        $SkillsID = $job->getId();
        if (isset($data['jobApplicationStudy']))
        $this->SaveStudy($em, $data['jobApplicationStudy'], $SkillsID);
        if (isset($data['jobApplicationCompany']))
        $this->SaveJobs($em, $data['jobApplicationCompany'], $SkillsID);
        if (isset($data['jobApplicationLanguage']))
        $this->SaveLanguages($em, $data['jobApplicationLanguage'], $SkillsID);
        if (isset($data['jobApplicationTechnology']))
        $this->SaveTechnologies($em, $data['jobApplicationTechnology'], $SkillsID);
        if (isset($data['applicantOthersCompetenciesTags']))
        $this->SaveTags($em, $data['applicantOthersCompetenciesTags'], $SkillsID);
        $em->persist($job);
        $em->flush();
        return new JsonResponse(array('http_code' => 200,
            "message" => array("Élément Ajouté avec succès"),
            "companyId" => $company->getId(),
            "jobId" => $job->getId()
        ));
    }

    public function SaveStudy($em, $data, $SkillsID) {
        $StudyID = array();
        for ($i = 0; $i < sizeof($data); $i++) {
            if (isset($data[$i]['name'])) {
                $skills_study = new hrm_document_jobApplicantStudy();
                $skills_study->setName($data[$i]['name']);
                $skills_study->setStudyTypeId($em->getRepository('AdminBundle:SettingsStudyType')->find($data[$i]['studyTypeId']));
                $skills_study->setStudyLevelId($em->getRepository('AdminBundle:SettingsStudyLevel')->find($data[$i]['studyLevelId']));
                $skills_study->setDocumentJobDescription($em->getRepository('HiringBundle:hrm_document_jobDescription')->find($SkillsID));
                $em->persist($skills_study);
                $em->flush();
                array_push($StudyID, $skills_study->getId());
            }
        }
        return $StudyID;
    }

    public function SaveJobs($em, $data, $SkillsID) {
        $CompanyID = array();
        for ($i = 0; $i < sizeof($data); $i++) {
            if (isset($data[$i]['name'])) {
                $skills_company = new hrm_document_jobApplicantCompany();
                $skills_company->setName($data[$i]['name']);
                $skills_company->setJobId($em->getRepository('AdminBundle:hrm_settings_job')->find($data[$i]['jobId']));
                $skills_company->setIndustrysectorId($em->getRepository('AdminBundle:hrm_settings_industrysector')->find($data[$i]['industrysectorId']));
                $skills_company->setDocumentJobDescription($em->getRepository('HiringBundle:hrm_document_jobDescription')->find($SkillsID));
                $em->persist($skills_company);
                $em->flush();
                array_push($CompanyID, $skills_company->getId());
            }
        }
        return $CompanyID;
    }

    public function SaveLanguages($em, $data, $SkillsID) {
        $LanguageID = array();
        for ($i = 0; $i < sizeof($data); $i++) {
            if (isset($data[$i]['languageId'])) {
                $skills_language = new hrm_document_jobApplicantLanguage();
                $skills_language->setLanguageId($em->getRepository('AdminBundle:hrm_settings_language')->find($data[$i]['languageId']));
                $skills_language->setLanguageLevelId($em->getRepository('AdminBundle:hrm_settings_language_level')->find($data[$i]['languageLevelId']));
                $skills_language->setDocumentJobDescription($em->getRepository('HiringBundle:hrm_document_jobDescription')->find($SkillsID));
                $em->persist($skills_language);
                $em->flush();
                array_push($LanguageID, $skills_language->getId());
            }
        }
        return $LanguageID;
    }

    public function SaveTechnologies($em, $data, $SkillsID) {
        $TechnologyID = array();
        for ($i = 0; $i < sizeof($data); $i++) {
            if (isset($data[$i]['technologyId'])) {
                $skills_technology = new hrm_document_jobApplicantTechnology();
                $skills_technology->setTechnologyId($em->getRepository('AdminBundle:hrm_settings_technology')->find($data[$i]['technologyId']));
                $skills_technology->setTechnologyLevelId($em->getRepository('AdminBundle:hrm_settings_technology_level')->find($data[$i]['technologyLevelId']));
                $skills_technology->setDocumentJobDescription($em->getRepository('HiringBundle:hrm_document_jobDescription')->find($SkillsID));
                $em->persist($skills_technology);
                $em->flush();
                array_push($TechnologyID, $skills_technology->getId());
            }
        }
        return $TechnologyID;
    }

    public function SaveTags($em, $data, $SkillsID) {
        $TagsID = array();
        for ($i = 0; $i < sizeof($data); $i++) {
            if (isset($data[$i]['text'])) {
                $skills_tags = new hrm_document_jobApplicantTag();
                $skills_tags->setTag($data[$i]['text']);
                $skills_tags->setDocumentJobDescription($em->getRepository('HiringBundle:hrm_document_jobDescription')->find($SkillsID));
                $em->persist($skills_tags);
                $em->flush();
                array_push($TagsID, $skills_tags->getId());
            }
        }
        return $TagsID;
    }

    public function newJobApplicantAction() {
        $job = new Hrm_document_jobdescription();
        $provider = $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $formjobApplicantType = $this->createForm(new jobApplicantType(), null, array(
            'provider' => $provider
        ));
        return $this->render('hrm_hiring/applicant/new.html.twig', array(
                    'hrm_document_jobDescription' => $job,
                    'formjobApplicant' => $formjobApplicantType->createView(),
        ));
    }

    public function editJobApplicantAction() {
        $job = new Hrm_document_jobdescription();
        $provider = $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $formjobApplicantType = $this->createForm(new jobApplicantType(), null, array(
            'provider' => $provider
        ));
        return $this->render('hrm_hiring/applicant/edit.html.twig', array(
                    'hrm_document_jobDescription' => $job,
                    'formjobApplicant' => $formjobApplicantType->createView(),
        ));
    }

    public function newJobOtherAction() {
        $defaultData = array('message' => 'Type your message here');
        $form = $this->createFormBuilder($defaultData)
                ->add('content', CKEditorType::class)
                ->getForm();
        return $this->render('hrm_hiring/hrm_document_jobdescription/newjobOther.html.twig', array('form' => $form->createView()));
    }

    /**
     * Creates a new hrm_document_jobDescription entity.
     *
     */
    public function newAction() {
        $job = new Hrm_document_jobdescription();
        $provider = $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $form = $this->createForm('HiringBundle\Form\hrm_document_jobDescriptionType', $job, array(
            'provider' => $provider, 'companyId' => $companyId
        ));
        return $this->render('hrm_hiring/hrm_document_jobdescription/new.html.twig', array(
                    'hrm_document_jobDescription' => $job,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new hrm_document_jobDescription entity.
     *
     */
    public function editJobDescriptionAction() {
        $job = new Hrm_document_jobdescription();
        $provider = $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $form = $this->createForm('HiringBundle\Form\hrm_document_jobDescriptionType', $job, array(
            'provider' => $provider, 'companyId' => $companyId
        ));
        return $this->render('hrm_hiring/hrm_document_jobdescription/edit.html.twig', array(
                    'hrm_document_jobDescription' => $job,
                    'form' => $form->createView(),
        ));
    }

    public function newCompanyAction() {
        $provider = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $form = $this->createForm('HiringBundle\Form\HrmCompanyType', null, array(
            'provider' => $provider, 'companyId' => $companyId
        ));
        return $this->render('hrm_hiring/company/new.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function editCompanyAction() {
        $provider = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $edit_form = $this->createForm('HiringBundle\Form\HrmCompanyType', null, array(
            'provider' => $provider, 'companyId' => $companyId
        ));
        return $this->render('hrm_hiring/company/edit.html.twig', array(
                    'edit_form' => $edit_form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_document_jobDescription entity.
     *
     */
    public function showAction(hrm_document_jobDescription $hrm_document_jobDescription) {
        $deleteForm = $this->createDeleteForm($hrm_document_jobDescription);

        return $this->render('hrm_document_jobdescription/show.html.twig', array(
                    'hrm_document_jobDescription' => $hrm_document_jobDescription,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_document_jobDescription entity.
     *
     */
    public function editAction() {
        $provider = $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $editForm = $this->createForm('HiringBundle\Form\HrmCompanyType', null, array(
            'provider' => $provider
        ));
        return $this->render('hrm_document_jobdescription/edit.html.twig', array(
                    'edit_form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a typetransport entity.
     *
     */
    public function deleteAction($id) {
//        if ($this->get('request')->getMethod() != 'DELETE') {
//            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
//        }
        $jobDescription = $this->getDoctrine()->getRepository('HiringBundle:hrm_document_jobDescription')->find($id);
        if ($jobDescription) {
            $em = $this->getDoctrine()->getManager();
            $jobApplicantTechnologyarray = $jobDescription->getJobApplicationCompany();
            return new JsonResponse(array('http_code' => 200, "message" => array(
                    Count($jobDescription->getJobApplicationCompany()),
                    $jobDescription->getId()
            )));

            foreach ($jobApplicantTechnologyarray as $value) {
                $jobApplicantTechnology = $this->getDoctrine()->getRepository('HiringBundle:hrm_document_jobApplicantTechnology')->find($value->getId());
                return new JsonResponse(array('http_code' => 200, "message" => array($value->getId())));

                $jobApplicantTechnology->setDocumentJobDescription(null);
                $em->persist($jobApplicantTechnology);
                $em->flush($jobApplicantTechnology);
                $em->remove($jobApplicantTechnology);
                $em->flush($jobApplicantTechnology);
            }


//        if($jobApplicantCompany){
//        $jobDescription->removeJobApplicationCompany($jobApplicantCompany);
//         $em->remove($jobApplicantCompany);
//         }
//         
//        if($jobApplicantLanguage){
//        $jobDescription->removeJobApplicationLanguage($jobApplicantLanguage);
//           $em->remove($jobApplicantLanguage);
//         }
//        if($jobApplicantStudy){
//        $jobDescription->removeJobApplicationStudy($jobApplicantStudy);
//          $em->remove($jobApplicantStudy);
//         }
//        if($jobApplicantTag){
//        $jobDescription->setApplicantOthersCompetenciesTags(null);
//           $em->remove($jobApplicantTag);
//
//         }
//        if($jobApplicantTechnology){
//      
//      $jobDescription->removeJobApplicationTechnology($jobApplicantTechnology);
//      
//      
//      $jobApplicantTechnology->setDocumentJobDescription(null);
//      $em->persist($jobApplicantTechnology);
//      $em->flush($jobApplicantTechnology);
//      $em->remove($jobApplicantTechnology);
//
//        }







            $em->remove($jobDescription);
            $em->flush();
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }
        return new JsonResponse(null);
    }

    /**
     * Creates a form to delete a hrm_document_jobDescription entity.
     *
     * @param hrm_document_jobDescription $hrm_document_jobDescription The hrm_document_jobDescription entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_document_jobDescription $hrm_document_jobDescription) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('hrm_document_jobdescription_delete', array('id' => $hrm_document_jobDescription->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    public function newJobApplicantOffreAction() {
        $defaultData = array('message' => 'Type your message here');
        $form = $this->createFormBuilder($defaultData)
                ->add('content', CKEditorType::class)
                ->getForm();
        return $this->render('hrm_hiring/hrm_job_offre/new.html.twig', array('form' => $form->createView()));
    }

    /**
     * save document
     */
    public function saveDocumentAction(Request $request) {
        if ($request->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $em = $this->getDoctrine()->getManager();
        $provider = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider();
        $company = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany();
        $empId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getId();
        $data = json_decode($this->get('request')->getContent(), true);

        if (isset($data['jobId'])) {
            $job = $this->getDoctrine()->getRepository('HiringBundle:hrm_document_jobDescription')->find($data['jobId']);
//            $job->setLastUpdateUid($empId);
            $job->setLastUpdate(new \DateTime('now'));
        } else {

            $job = new hrm_document_jobDescription();
            $job->setProvider($provider);
            $job->setCreateDate(new \DateTime('now'));
//            $job->setCreateUid($empId);
        }


        $job->setCompany($company);
        $job->setHtmlTemplate($data['data']);
        $em->persist($job);
        $em->flush();
        return new JsonResponse(array('http_code' => 200,
            "message" => array("Élément Ajouté avec succès"),
            "companyId" => $company->getId(),
            "jobId" => $job->getId()
        ));
    }

    public function getModeleTemplateAction($action) {
        $lReturn = array();
        $defaultData = array('message' => 'Type your message here');
        $form = $this->createFormBuilder($defaultData)
                ->add('content', CKEditorType::class)
                ->getForm();

        if ($action === 'edit')
            $temp = 'hrm_hiring/hrm_job_offre/edit.html.twig';
        else
            $temp = 'hrm_hiring/hrm_job_offre/new.html.twig';
        $lReturn['html'] = $this->renderView($temp, array('form' => $form->createView()));
        return $lReturn;
    }

    public function downloadDocumentAction(Request $request) {
        if ($request->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $html2pdf = $this->get('app.html2pdf');
        $html2pdf->create('P', 'A4', 'fr', true, 'UTF-8', array(10, 15, 10, 15));
        $template = $this->renderView('hrm_hiring/hrm_job_offre/pdf.html.twig', array(
            'html_post' => $request->request->get('content')
        ));

        return new Response($html2pdf->generatePdf($template, 'Offre d\'emploi'));
    }

    public function getJobAction($id) {

        return array('job' => $this->getDoctrine()->getRepository('HiringBundle:hrm_document_jobDescription')->find($id));
    }

    public function EmailModeleAction() {
        $defaultData = array('message' => 'Type your message here');
        $form = $this->createFormBuilder($defaultData)
                ->add('recipient', 'email')
                ->add('copie_carbone', 'email')
                ->add('subject', 'text')
                ->add('message', 'textarea')
                ->getForm();
        return $this->render('hrm_hiring/hrm_job_offre/mail.html.twig', array('mail_form' => $form->createView()));
    }

    public function sendDocumentAction(Request $request) {
        
        if ($request->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $data = json_decode($this->get('request')->getContent(), true);
        $html2pdf = $this->get('app.html2pdf');
        $html2pdf->create('P', 'A4', 'fr', true, 'UTF-8', array(10, 15, 10, 15));
        $template = $this->renderView('hrm_hiring/hrm_job_offre/pdf.html.twig', array(
            'html_post' => $data['attachment_file']
        ));
        $pdf = $html2pdf->generatePdf($template, 'Offre_emploi', true, 'S');

        return $this->get('mailingbundle.email.send.service')->simpleMailSend($data['receipient'], $data['message'], $data['subject'], $data['copie_carbone'], $pdf, 'Offre_emploi.pdf');
    }

    public function getContractAction() {

        $company = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $documentJobDescriptions = $this->getDoctrine()->getRepository('HiringBundle:hrm_document_jobDescription')->jobDescriptionByCompany($company);
        // return new JsonResponse(array(
        //     'collaborateur' => $company,
        //     'documentJobDescriptions' => $documentJobDescriptions,
        // ));
        return array('collaborateur' => $company, 'documentJobDescriptions' => $documentJobDescriptions);
    }

    public function newJobOthersAction() {
        $form = $this->createForm('HiringBundle\Form\JobOthersType', null);
        return $this->render('hrm_hiring/others/new.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    public function editJobOthersAction() {
        $formEdit = $this->createForm('HiringBundle\Form\JobOthersType', null);
        return $this->render('hrm_hiring/others/edit.html.twig', array(
                    'edit_form' => $formEdit->createView(),
        ));
    }

    public function saveOthersAction(Request $request) {
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $CurrentUser = $this->get('security.context')->getToken()->getUser()->getId();
        $user_provide_id = $this->container->get('settingsbundle.preference.service')->getProviderId();
        if (isset($json_data['jobId'])) {
            $job = $this->getDoctrine()->getRepository('HiringBundle:hrm_document_jobDescription')->find($json_data['jobId']);
            $job->setLastUpdate(new \DateTime('now'));
            $job->setCreateUid($CurrentUser);
        } else {
            $job = new hrm_document_jobDescription();
            $job->setCreateDate(new \DateTime('now'));
            $job->setLastUpdateUid($CurrentUser);
            $job->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        }
        if (isset($json_data['otherComment'])) {
         $job->setOtherComment($json_data['otherComment']);   
        }
        $provider = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $form = $this->createForm('HiringBundle\Form\hrm_document_jobDescriptionType', $job, array(
            'provider' => $provider
        ));
        $form->handleRequest($request);
        $em->persist($job);
        $em->flush();
        return new JsonResponse(array('http_code' => 200,
            "message" => array("Élément Ajouté avec succès"),
            "jobId" => $job->getId()
        ));
    }

}
