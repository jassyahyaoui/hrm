<?php

namespace HiringBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HiringController extends Controller
{
    
    public function layoutAction() {
        return $this->render('hrm_hiring/layout.html.twig');
    }
    
    public function indexDocumentJobAction() {
        return $this->render('hrm_hiring/hrm_document_job/index.html.twig');
    }
    public function newDocumentJobAction() {
        return $this->render('hrm_hiring/hrm_document_job/new.html.twig');
    } 
    public function editDocumentJobAction() {
        return $this->render('hrm_hiring/hrm_document_job/edit.html.twig');
    } 
    public function indexCostsAction() {
        return $this->render('hrm_hiring/hrm_costs_help/index.html.twig');
    }

    public function singlePageLayoutAction() {
        return $this->render('hrm_hiring/single_page_layout.html.twig');
    }
}
