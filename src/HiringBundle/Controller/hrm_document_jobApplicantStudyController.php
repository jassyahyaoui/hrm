<?php

namespace HiringBundle\Controller;

use HiringBundle\Entity\hrm_document_jobApplicantStudy;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Hrm_document_jobapplicantstudy controller.
 *
 */
class hrm_document_jobApplicantStudyController extends Controller
{
    /**
     * Lists all hrm_document_jobApplicantStudy entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hrm_document_jobApplicantStudies = $em->getRepository('HiringBundle:hrm_document_jobApplicantStudy')->findAll();

        return $this->render('hrm_document_jobapplicantstudy/index.html.twig', array(
            'hrm_document_jobApplicantStudies' => $hrm_document_jobApplicantStudies,
        ));
    }

    /**
     * Creates a new hrm_document_jobApplicantStudy entity.
     *
     */
    public function newAction(Request $request)
    {
        $hrm_document_jobApplicantStudy = new Hrm_document_jobapplicantstudy();
        $form = $this->createForm('HiringBundle\Form\hrm_document_jobApplicantStudyType', $hrm_document_jobApplicantStudy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($hrm_document_jobApplicantStudy);
            $em->flush();

            return $this->redirectToRoute('hrm_document_jobapplicantstudy_show', array('id' => $hrm_document_jobApplicantStudy->getId()));
        }

        return $this->render('hrm_document_jobapplicantstudy/new.html.twig', array(
            'hrm_document_jobApplicantStudy' => $hrm_document_jobApplicantStudy,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_document_jobApplicantStudy entity.
     *
     */
    public function showAction(hrm_document_jobApplicantStudy $hrm_document_jobApplicantStudy)
    {
        $deleteForm = $this->createDeleteForm($hrm_document_jobApplicantStudy);

        return $this->render('hrm_document_jobapplicantstudy/show.html.twig', array(
            'hrm_document_jobApplicantStudy' => $hrm_document_jobApplicantStudy,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_document_jobApplicantStudy entity.
     *
     */
    public function editAction(Request $request, hrm_document_jobApplicantStudy $hrm_document_jobApplicantStudy)
    {
        $deleteForm = $this->createDeleteForm($hrm_document_jobApplicantStudy);
        $editForm = $this->createForm('HiringBundle\Form\hrm_document_jobApplicantStudyType', $hrm_document_jobApplicantStudy);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('hrm_document_jobapplicantstudy_edit', array('id' => $hrm_document_jobApplicantStudy->getId()));
        }

        return $this->render('hrm_document_jobapplicantstudy/edit.html.twig', array(
            'hrm_document_jobApplicantStudy' => $hrm_document_jobApplicantStudy,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a hrm_document_jobApplicantStudy entity.
     *
     */
    public function deleteAction(Request $request, hrm_document_jobApplicantStudy $hrm_document_jobApplicantStudy)
    {
        $form = $this->createDeleteForm($hrm_document_jobApplicantStudy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_document_jobApplicantStudy);
            $em->flush();
        }

        return $this->redirectToRoute('hrm_document_jobapplicantstudy_index');
    }

    /**
     * Creates a form to delete a hrm_document_jobApplicantStudy entity.
     *
     * @param hrm_document_jobApplicantStudy $hrm_document_jobApplicantStudy The hrm_document_jobApplicantStudy entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_document_jobApplicantStudy $hrm_document_jobApplicantStudy)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('hrm_document_jobapplicantstudy_delete', array('id' => $hrm_document_jobApplicantStudy->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
