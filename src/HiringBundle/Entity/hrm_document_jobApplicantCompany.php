<?php

namespace HiringBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_document_jobApplicantCompany
 *
 * @ORM\Table(name="hrm_document_job_applicant_company")
 * @ORM\Entity(repositoryClass="HiringBundle\Repository\hrm_document_jobApplicantCompanyRepository")
 */
class hrm_document_jobApplicantCompany
{
    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   /**
     * @ORM\ManyToOne(targetEntity="hrm_document_jobDescription", inversedBy="jobApplicationCompany")
     * @ORM\JoinColumn(name="documentJobDescription", referencedColumnName="uid")
     */
    private $documentJobDescription;
    
   /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_job")
    * @ORM\JoinColumn(name="job_id", referencedColumnName="uid")
    */
    private $jobId;

   /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_industrysector")
    * @ORM\JoinColumn(name="industrysector_id", referencedColumnName="uid")
    */
    private $industrysectorId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;
    



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return hrm_document_jobApplicantCompany
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set documentJobDescription
     *
     * @param \HiringBundle\Entity\hrm_document_jobDescription $documentJobDescription
     *
     * @return hrm_document_jobApplicantCompany
     */
    public function setDocumentJobDescription(\HiringBundle\Entity\hrm_document_jobDescription $documentJobDescription = null)
    {
        $this->documentJobDescription = $documentJobDescription;

        return $this;
    }

    /**
     * Get documentJobDescription
     *
     * @return \HiringBundle\Entity\hrm_document_jobDescription
     */
    public function getDocumentJobDescription()
    {
        return $this->documentJobDescription;
    }

    /**
     * Set jobId
     *
     * @param \AdminBundle\Entity\hrm_settings_job $jobId
     *
     * @return hrm_document_jobApplicantCompany
     */
    public function setJobId(\AdminBundle\Entity\hrm_settings_job $jobId = null)
    {
        $this->jobId = $jobId;

        return $this;
    }

    /**
     * Get jobId
     *
     * @return \AdminBundle\Entity\hrm_settings_job
     */
    public function getJobId()
    {
        return $this->jobId;
    }

    /**
     * Set industrysectorId
     *
     * @param \AdminBundle\Entity\hrm_settings_industrysector $industrysectorId
     *
     * @return hrm_document_jobApplicantCompany
     */
    public function setIndustrysectorId(\AdminBundle\Entity\hrm_settings_industrysector $industrysectorId = null)
    {
        $this->industrysectorId = $industrysectorId;

        return $this;
    }

    /**
     * Get industrysectorId
     *
     * @return \AdminBundle\Entity\hrm_settings_industrysector
     */
    public function getIndustrysectorId()
    {
        return $this->industrysectorId;
    }
}
