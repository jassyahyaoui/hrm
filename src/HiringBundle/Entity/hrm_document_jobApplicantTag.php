<?php

namespace HiringBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_document_jobApplicantTag
 *
 * @ORM\Table(name="hrm_document_job_applicant_tag")
 * @ORM\Entity(repositoryClass="HiringBundle\Repository\hrm_document_jobApplicantTagRepository")
 */
class hrm_document_jobApplicantTag {

    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="hrm_document_jobDescription", inversedBy="documentJobApplicantStudy")
     * @ORM\JoinColumn(name="documentJobDescription", referencedColumnName="uid")
     */
    private $documentJobDescription;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=100)
     */
    private $tag;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set documentJobDescription
     *
     * @param \HiringBundle\Entity\hrm_document_jobDescription $documentJobDescription
     *
     * @return hrm_document_jobApplicantTag
     */
    public function setDocumentJobDescription(\HiringBundle\Entity\hrm_document_jobDescription $documentJobDescription = null) {
        $this->documentJobDescription = $documentJobDescription;

        return $this;
    }

    /**
     * Get documentJobDescription
     *
     * @return \HiringBundle\Entity\hrm_document_jobDescription
     */
    public function getDocumentJobDescription() {
        return $this->documentJobDescription;
    }


    /**
     * Set tag
     *
     * @param string $tag
     *
     * @return hrm_document_jobApplicantTag
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }
}
