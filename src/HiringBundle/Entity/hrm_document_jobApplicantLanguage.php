<?php

namespace HiringBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_document_jobApplicantLanguage
 *
 * @ORM\Table(name="hrm_document_job_applicant_language")
 * @ORM\Entity(repositoryClass="HiringBundle\Repository\hrm_document_jobApplicantLanguageRepository")
 */
class hrm_document_jobApplicantLanguage
{
    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   /**
     * @ORM\ManyToOne(targetEntity="hrm_document_jobDescription", inversedBy="jobApplicantLanguage")
     * @ORM\JoinColumn(name="documentJobDescription", referencedColumnName="uid")
     */
    private $documentJobDescription;
    
    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="uid")     
     */
    private $languageId;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_language_level")
     * @ORM\JoinColumn(name="language_level_id", referencedColumnName="uid")     
     */
    private $languageLevelId;



 

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set documentJobDescription
     *
     * @param \HiringBundle\Entity\hrm_document_jobDescription $documentJobDescription
     *
     * @return hrm_document_jobApplicantLanguage
     */
    public function setDocumentJobDescription(\HiringBundle\Entity\hrm_document_jobDescription $documentJobDescription = null)
    {
        $this->documentJobDescription = $documentJobDescription;

        return $this;
    }

    /**
     * Get documentJobDescription
     *
     * @return \HiringBundle\Entity\hrm_document_jobDescription
     */
    public function getDocumentJobDescription()
    {
        return $this->documentJobDescription;
    }

    /**
     * Set languageId
     *
     * @param \AdminBundle\Entity\hrm_settings_language $languageId
     *
     * @return hrm_document_jobApplicantLanguage
     */
    public function setLanguageId(\AdminBundle\Entity\hrm_settings_language $languageId = null)
    {
        $this->languageId = $languageId;

        return $this;
    }

    /**
     * Get languageId
     *
     * @return \AdminBundle\Entity\hrm_settings_language
     */
    public function getLanguageId()
    {
        return $this->languageId;
    }

    /**
     * Set languageLevelId
     *
     * @param \AdminBundle\Entity\hrm_settings_language_level $languageLevelId
     *
     * @return hrm_document_jobApplicantLanguage
     */
    public function setLanguageLevelId(\AdminBundle\Entity\hrm_settings_language_level $languageLevelId = null)
    {
        $this->languageLevelId = $languageLevelId;

        return $this;
    }

    /**
     * Get languageLevelId
     *
     * @return \AdminBundle\Entity\hrm_settings_language_level
     */
    public function getLanguageLevelId()
    {
        return $this->languageLevelId;
    }
}
