<?php

namespace HiringBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="hrm_document")
 * @ORM\Entity(repositoryClass="HiringBundle\Repository\hrm_documentRepository")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"document" = "hrm_document", "document_jobDescription" = "hrm_document_jobDescription"})
 */

class hrm_document
{
    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsProvider")
    * @ORM\JoinColumn(name="provider_id", referencedColumnName="uid")
    */
    private $provider;
    
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\HrmCompany")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")     
     */
    private $company;   
    
    /**
     * @var string
     *
     * @ORM\Column(name="document_name", type="string", length=100, nullable=true)
     */
    private $documentName;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="text", nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="html_template", type="text", nullable=true)
     */
    private $htmlTemplate;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $create_date;
    
    /**
     * @var int
     *
     * @ORM\Column(name="create_uid", type="integer", nullable=true)
     */
    private $createUid;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $last_update;
    

    /**
     * @var int
     *
     * @ORM\Column(name="last_update_uid", type="integer", nullable=true)
     */
    private $last_update_uid;    



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set documentName
     *
     * @param string $documentName
     *
     * @return hrm_document
     */
    public function setDocumentName($documentName)
    {
        $this->documentName = $documentName;

        return $this;
    }

    /**
     * Get documentName
     *
     * @return string
     */
    public function getDocumentName()
    {
        return $this->documentName;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return hrm_document
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return hrm_document
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set htmlTemplate
     *
     * @param string $htmlTemplate
     *
     * @return hrm_document
     */
    public function setHtmlTemplate($htmlTemplate)
    {
        $this->htmlTemplate = $htmlTemplate;

        return $this;
    }

    /**
     * Get htmlTemplate
     *
     * @return string
     */
    public function getHtmlTemplate()
    {
        return $this->htmlTemplate;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_document
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_document
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_document
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_document
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->last_update_uid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->last_update_uid;
    }

    /**
     * Set provider
     *
     * @param \AdminBundle\Entity\SettingsProvider $provider
     *
     * @return hrm_document
     */
    public function setProvider(\AdminBundle\Entity\SettingsProvider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \AdminBundle\Entity\SettingsProvider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set company
     *
     * @param \UserBundle\Entity\HrmCompany $company
     *
     * @return hrm_document
     */
    public function setCompany(\UserBundle\Entity\HrmCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \UserBundle\Entity\HrmCompany
     */
    public function getCompany()
    {
        return $this->company;
    }
}
