<?php

namespace HiringBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_document_jobApplicantTechnology
 *
 * @ORM\Table(name="hrm_document_job_applicant_technology")
 * @ORM\Entity(repositoryClass="HiringBundle\Repository\hrm_document_jobApplicantTechnologyRepository")
 */
class hrm_document_jobApplicantTechnology
{
    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   /**
     * @ORM\ManyToOne(targetEntity="hrm_document_jobDescription", inversedBy="jobApplicantTechnology")
     * @ORM\JoinColumn(name="documentJobDescription", referencedColumnName="uid")
     */
    private $documentJobDescription;
    
    
    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_technology")
     * @ORM\JoinColumn(name="technology_id", referencedColumnName="uid")     
     */
    private $technologyId;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_technology_level")
     * @ORM\JoinColumn(name="technology_level_id", referencedColumnName="uid")     
     */
    private $technologyLevelId;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set documentJobDescription
     *
     * @param \HiringBundle\Entity\hrm_document_jobDescription $documentJobDescription
     *
     * @return hrm_document_jobApplicantTechnology
     */
    public function setDocumentJobDescription(\HiringBundle\Entity\hrm_document_jobDescription $documentJobDescription = null)
    {
        $this->documentJobDescription = $documentJobDescription;

        return $this;
    }

    /**
     * Get documentJobDescription
     *
     * @return \HiringBundle\Entity\hrm_document_jobDescription
     */
    public function getDocumentJobDescription()
    {
        return $this->documentJobDescription;
    }

    /**
     * Set technologyId
     *
     * @param \AdminBundle\Entity\hrm_settings_technology $technologyId
     *
     * @return hrm_document_jobApplicantTechnology
     */
    public function setTechnologyId(\AdminBundle\Entity\hrm_settings_technology $technologyId = null)
    {
        $this->technologyId = $technologyId;

        return $this;
    }

    /**
     * Get technologyId
     *
     * @return \AdminBundle\Entity\hrm_settings_technology
     */
    public function getTechnologyId()
    {
        return $this->technologyId;
    }

    /**
     * Set technologyLevelId
     *
     * @param \AdminBundle\Entity\hrm_settings_technology_level $technologyLevelId
     *
     * @return hrm_document_jobApplicantTechnology
     */
    public function setTechnologyLevelId(\AdminBundle\Entity\hrm_settings_technology_level $technologyLevelId = null)
    {
        $this->technologyLevelId = $technologyLevelId;

        return $this;
    }

    /**
     * Get technologyLevelId
     *
     * @return \AdminBundle\Entity\hrm_settings_technology_level
     */
    public function getTechnologyLevelId()
    {
        return $this->technologyLevelId;
    }
}
