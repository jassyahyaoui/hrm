<?php

namespace HiringBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
/**
 * hrm_document_jobDescription
 *
 * @ORM\Table(name="hrm_document_job_description")
 * @ORM\Entity(repositoryClass="HiringBundle\Repository\hrm_document_jobDescriptionRepository")
 */
class hrm_document_jobDescription extends hrm_document
{
    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="jobAdress", type="string", length=255, nullable=true)
     */
    private $jobAdress;

    /**
     * @var string
     *
     * @ORM\Column(name="jobCity", type="string", length=255, nullable=true)
     */
    private $jobCity;

    /**
     * @var string
     *
     * @ORM\Column(name="jobPostalCode", type="string", length=255, nullable=true)
     */
    private $jobPostalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="jobCountry", type="string", length=100, nullable=true)
     */
    private $jobCountry;

   /**
    * @ORM\ManyToOne(targetEntity="UserBundle\Entity\HrmEmployee")
    * @ORM\JoinColumn(name="applicantContact", referencedColumnName="uid")
    */
    private $applicantContact;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="departement", type="string", length=255, nullable=true)
     */
    private $departement;

   
    /**
    * @ORM\ManyToOne(targetEntity="UserBundle\Entity\HrmEmployee")
    * @ORM\JoinColumn(name="superior", referencedColumnName="uid")
    */
    private $superior;

    /**
     * @var string
     *
     * @ORM\Column(name="assignement", type="text", nullable=true)
     */
    private $assignement;

    /**
     * @var int
     *
     * @ORM\Column(name="plannedJourneyNumber", type="integer", nullable=true)
     */
    private $plannedJourneyNumber;

   /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_plannedjourney_type")
    * @ORM\JoinColumn(name="plannedJourneyType", referencedColumnName="uid")
    */    
    private $plannedJourneyType;

   /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\TypeContracts")
    * @ORM\JoinColumn(name="contractType", referencedColumnName="uid")
    */
    private $contractType;

    /**
     * @var string
     *
     * @ORM\Column(name="partTimeDayNumber", type="string", length=100, nullable=true)
     */
    private $partTimeDayNumber;

   /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_periodicity")
    * @ORM\JoinColumn(name="partTimeDayPeriodicity", referencedColumnName="uid")
    */
    private $partTimeDayPeriodicity;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="startDate", type="datetime", nullable=true)
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="endDate", type="datetime", nullable=true)
     */
    private $endDate;

    /**
     * @var float
     *
     * @ORM\Column(name="remunerationAmountFrom", type="float", nullable=true)
     */
    private $remunerationAmountFrom;

    /**
     * @var float
     *
     * @ORM\Column(name="remunerationAmountTo", type="float", nullable=true)
     */
    private $remunerationAmountTo;

    /**
     * @var string
     *
     * @ORM\Column(name="remunerationOthers", type="text", nullable=true)
     */
    private $remunerationOthers;

    /**
     * @ORM\OneToMany(targetEntity="hrm_document_jobApplicantStudy", mappedBy="documentJobDescription")
     */    
    private $jobApplicationStudy;

    /**
     * @var bool
     *
     * @ORM\Column(name="ApplicantStudySup", type="boolean", nullable=true)
     */
    private $applicantStudySup;

    /**
     * @var string
     *
     * @ORM\Column(name="ApplicantStudyComment", type="text", nullable=true)
     */
    private $applicantStudyComment;

    /**
     * @var int
     *
     * @ORM\Column(name="applicantProfessionalExperience", type="integer", nullable=true)
     */
    private $applicantProfessionalExperience;

    /**
     * @ORM\OneToMany(targetEntity="hrm_document_jobApplicantCompany", mappedBy="documentJobDescription")
     */   
    private $jobApplicationCompany;

    /**
     * @ORM\OneToMany(targetEntity="hrm_document_jobApplicantLanguage", mappedBy="documentJobDescription")
     */ 
    private $jobApplicationLanguage;

    /**
     * @ORM\OneToMany(targetEntity="hrm_document_jobApplicantTechnology", mappedBy="documentJobDescription")
     */
    private $jobApplicationTechnology;

    /**
     * @ORM\OneToMany(targetEntity="HiringBundle\Entity\hrm_document_jobApplicantTag", mappedBy="documentJobDescription")
     */
    private $applicantOthersCompetenciesTags;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ApplicantExperienceComment", type="text", nullable=true)
     */
    private $applicantExperienceComment;
    
    /**
     * @var string
     *
     * @ORM\Column(name="OtherComment", type="text", nullable=true)
     */
    private $otherComment;
    
    public function __construct() {
        $this->jobApplicationStudy = new ArrayCollection(); 
        $this->jobApplicationCompany = new ArrayCollection();
        $this->JobApplicationLanguage = new ArrayCollection();
        $this->JobApplicationTechnology = new ArrayCollection();
    }


    /**
     * Set jobAdress
     *
     * @param string $jobAdress
     *
     * @return hrm_document_jobDescription
     */
    public function setJobAdress($jobAdress)
    {
        $this->jobAdress = $jobAdress;

        return $this;
    }

    /**
     * Get jobAdress
     *
     * @return string
     */
    public function getJobAdress()
    {
        return $this->jobAdress;
    }

    /**
     * Set jobCity
     *
     * @param string $jobCity
     *
     * @return hrm_document_jobDescription
     */
    public function setJobCity($jobCity)
    {
        $this->jobCity = $jobCity;

        return $this;
    }

    /**
     * Get jobCity
     *
     * @return string
     */
    public function getJobCity()
    {
        return $this->jobCity;
    }

    /**
     * Set jobPostalCode
     *
     * @param string $jobPostalCode
     *
     * @return hrm_document_jobDescription
     */
    public function setJobPostalCode($jobPostalCode)
    {
        $this->jobPostalCode = $jobPostalCode;

        return $this;
    }

    /**
     * Get jobPostalCode
     *
     * @return string
     */
    public function getJobPostalCode()
    {
        return $this->jobPostalCode;
    }

    /**
     * Set jobCountry
     *
     * @param string $jobCountry
     *
     * @return hrm_document_jobDescription
     */
    public function setJobCountry($jobCountry)
    {
        $this->jobCountry = $jobCountry;

        return $this;
    }

    /**
     * Get jobCountry
     *
     * @return string
     */
    public function getJobCountry()
    {
        return $this->jobCountry;
    }

    /**
     * Set departement
     *
     * @param string $departement
     *
     * @return hrm_document_jobDescription
     */
    public function setDepartement($departement)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement
     *
     * @return string
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Set assignement
     *
     * @param string $assignement
     *
     * @return hrm_document_jobDescription
     */
    public function setAssignement($assignement)
    {
        $this->assignement = $assignement;

        return $this;
    }

    /**
     * Get assignement
     *
     * @return string
     */
    public function getAssignement()
    {
        return $this->assignement;
    }

    /**
     * Set plannedJourneyNumber
     *
     * @param integer $plannedJourneyNumber
     *
     * @return hrm_document_jobDescription
     */
    public function setPlannedJourneyNumber($plannedJourneyNumber)
    {
        $this->plannedJourneyNumber = $plannedJourneyNumber;

        return $this;
    }

    /**
     * Get plannedJourneyNumber
     *
     * @return integer
     */
    public function getPlannedJourneyNumber()
    {
        return $this->plannedJourneyNumber;
    }

    /**
     * Set partTimeDayNumber
     *
     * @param string $partTimeDayNumber
     *
     * @return hrm_document_jobDescription
     */
    public function setPartTimeDayNumber($partTimeDayNumber)
    {
        $this->partTimeDayNumber = $partTimeDayNumber;

        return $this;
    }

    /**
     * Get partTimeDayNumber
     *
     * @return string
     */
    public function getPartTimeDayNumber()
    {
        return $this->partTimeDayNumber;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return hrm_document_jobDescription
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return hrm_document_jobDescription
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set remunerationAmountFrom
     *
     * @param float $remunerationAmountFrom
     *
     * @return hrm_document_jobDescription
     */
    public function setRemunerationAmountFrom($remunerationAmountFrom)
    {
        $this->remunerationAmountFrom = $remunerationAmountFrom;

        return $this;
    }

    /**
     * Get remunerationAmountFrom
     *
     * @return float
     */
    public function getRemunerationAmountFrom()
    {
        return $this->remunerationAmountFrom;
    }

    /**
     * Set remunerationAmountTo
     *
     * @param float $remunerationAmountTo
     *
     * @return hrm_document_jobDescription
     */
    public function setRemunerationAmountTo($remunerationAmountTo)
    {
        $this->remunerationAmountTo = $remunerationAmountTo;

        return $this;
    }

    /**
     * Get remunerationAmountTo
     *
     * @return float
     */
    public function getRemunerationAmountTo()
    {
        return $this->remunerationAmountTo;
    }

    /**
     * Set remunerationOthers
     *
     * @param string $remunerationOthers
     *
     * @return hrm_document_jobDescription
     */
    public function setRemunerationOthers($remunerationOthers)
    {
        $this->remunerationOthers = $remunerationOthers;

        return $this;
    }

    /**
     * Get remunerationOthers
     *
     * @return string
     */
    public function getRemunerationOthers()
    {
        return $this->remunerationOthers;
    }

    /**
     * Set applicantStudySup
     *
     * @param boolean $applicantStudySup
     *
     * @return hrm_document_jobDescription
     */
    public function setApplicantStudySup($applicantStudySup)
    {
        $this->applicantStudySup = $applicantStudySup;

        return $this;
    }

    /**
     * Get applicantStudySup
     *
     * @return boolean
     */
    public function getApplicantStudySup()
    {
        return $this->applicantStudySup;
    }

    /**
     * Set applicantStudyComment
     *
     * @param string $applicantStudyComment
     *
     * @return hrm_document_jobDescription
     */
    public function setApplicantStudyComment($applicantStudyComment)
    {
        $this->applicantStudyComment = $applicantStudyComment;

        return $this;
    }

    /**
     * Get applicantStudyComment
     *
     * @return string
     */
    public function getApplicantStudyComment()
    {
        return $this->applicantStudyComment;
    }

    /**
     * Set applicantProfessionalExperience
     *
     * @param integer $applicantProfessionalExperience
     *
     * @return hrm_document_jobDescription
     */
    public function setApplicantProfessionalExperience($applicantProfessionalExperience)
    {
        $this->applicantProfessionalExperience = $applicantProfessionalExperience;

        return $this;
    }

    /**
     * Get applicantProfessionalExperience
     *
     * @return integer
     */
    public function getApplicantProfessionalExperience()
    {
        return $this->applicantProfessionalExperience;
    }

    /**
     * Set applicantExperienceComment
     *
     * @param string $applicantExperienceComment
     *
     * @return hrm_document_jobDescription
     */
    public function setApplicantExperienceComment($applicantExperienceComment)
    {
        $this->applicantExperienceComment = $applicantExperienceComment;

        return $this;
    }

    /**
     * Get applicantExperienceComment
     *
     * @return string
     */
    public function getApplicantExperienceComment()
    {
        return $this->applicantExperienceComment;
    }

    /**
     * Set applicantContact
     *
     * @param \UserBundle\Entity\HrmEmployee $applicantContact
     *
     * @return hrm_document_jobDescription
     */
    public function setApplicantContact(\UserBundle\Entity\HrmEmployee $applicantContact = null)
    {
        $this->applicantContact = $applicantContact;

        return $this;
    }

    /**
     * Get applicantContact
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getApplicantContact()
    {
        return $this->applicantContact;
    }

    /**
     * Set superior
     *
     * @param \UserBundle\Entity\HrmEmployee $superior
     *
     * @return hrm_document_jobDescription
     */
    public function setSuperior(\UserBundle\Entity\HrmEmployee $superior = null)
    {
        $this->superior = $superior;

        return $this;
    }

    /**
     * Get superior
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getSuperior()
    {
        return $this->superior;
    }

    /**
     * Set plannedJourneyType
     *
     * @param \AdminBundle\Entity\hrm_settings_plannedjourney_type $plannedJourneyType
     *
     * @return hrm_document_jobDescription
     */
    public function setPlannedJourneyType(\AdminBundle\Entity\hrm_settings_plannedjourney_type $plannedJourneyType = null)
    {
        $this->plannedJourneyType = $plannedJourneyType;

        return $this;
    }

    /**
     * Get plannedJourneyType
     *
     * @return \AdminBundle\Entity\hrm_settings_plannedjourney_type
     */
    public function getPlannedJourneyType()
    {
        return $this->plannedJourneyType;
    }

    /**
     * Set contractType
     *
     * @param \AdminBundle\Entity\TypeContracts $contractType
     *
     * @return hrm_document_jobDescription
     */
    public function setContractType(\AdminBundle\Entity\TypeContracts $contractType = null)
    {
        $this->contractType = $contractType;

        return $this;
    }

    /**
     * Get contractType
     *
     * @return \AdminBundle\Entity\TypeContracts
     */
    public function getContractType()
    {
        return $this->contractType;
    }

    /**
     * Set partTimeDayPeriodicity
     *
     * @param \AdminBundle\Entity\hrm_settings_periodicity $partTimeDayPeriodicity
     *
     * @return hrm_document_jobDescription
     */
    public function setPartTimeDayPeriodicity(\AdminBundle\Entity\hrm_settings_periodicity $partTimeDayPeriodicity = null)
    {
        $this->partTimeDayPeriodicity = $partTimeDayPeriodicity;

        return $this;
    }

    /**
     * Get partTimeDayPeriodicity
     *
     * @return \AdminBundle\Entity\hrm_settings_periodicity
     */
    public function getPartTimeDayPeriodicity()
    {
        return $this->partTimeDayPeriodicity;
    }

    /**
     * Add jobApplicationStudy
     *
     * @param \HiringBundle\Entity\hrm_document_jobApplicantStudy $jobApplicationStudy
     *
     * @return hrm_document_jobDescription
     */
    public function addJobApplicationStudy(\HiringBundle\Entity\hrm_document_jobApplicantStudy $jobApplicationStudy)
    {
        $this->jobApplicationStudy[] = $jobApplicationStudy;

        return $this;
    }

    /**
     * Remove jobApplicationStudy
     *
     * @param \HiringBundle\Entity\hrm_document_jobApplicantStudy $jobApplicationStudy
     */
    public function removeJobApplicationStudy(\HiringBundle\Entity\hrm_document_jobApplicantStudy $jobApplicationStudy)
    {
        $this->jobApplicationStudy->removeElement($jobApplicationStudy);
    }

    /**
     * Get jobApplicationStudy
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobApplicationStudy()
    {
        return $this->jobApplicationStudy;
    }

    /**
     * Add jobApplicationCompany
     *
     * @param \HiringBundle\Entity\hrm_document_jobApplicantCompany $jobApplicationCompany
     *
     * @return hrm_document_jobDescription
     */
    public function addJobApplicationCompany(\HiringBundle\Entity\hrm_document_jobApplicantCompany $jobApplicationCompany)
    {
        $this->jobApplicationCompany[] = $jobApplicationCompany;

        return $this;
    }

    /**
     * Remove jobApplicationCompany
     *
     * @param \HiringBundle\Entity\hrm_document_jobApplicantCompany $jobApplicationCompany
     */
    public function removeJobApplicationCompany(\HiringBundle\Entity\hrm_document_jobApplicantCompany $jobApplicationCompany)
    {
        $this->jobApplicationCompany->removeElement($jobApplicationCompany);
    }

    /**
     * Get jobApplicationCompany
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobApplicationCompany()
    {
        return $this->jobApplicationCompany;
    }

    /**
     * Add jobApplicationLanguage
     *
     * @param \HiringBundle\Entity\hrm_document_jobApplicantLanguage $jobApplicationLanguage
     *
     * @return hrm_document_jobDescription
     */
    public function addJobApplicationLanguage(\HiringBundle\Entity\hrm_document_jobApplicantLanguage $jobApplicationLanguage)
    {
        $this->jobApplicationLanguage[] = $jobApplicationLanguage;

        return $this;
    }

    /**
     * Remove jobApplicationLanguage
     *
     * @param \HiringBundle\Entity\hrm_document_jobApplicantLanguage $jobApplicationLanguage
     */
    public function removeJobApplicationLanguage(\HiringBundle\Entity\hrm_document_jobApplicantLanguage $jobApplicationLanguage)
    {
        $this->jobApplicationLanguage->removeElement($jobApplicationLanguage);
    }

    /**
     * Get jobApplicationLanguage
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobApplicationLanguage()
    {
        return $this->jobApplicationLanguage;
    }

    /**
     * Add jobApplicationTechnology
     *
     * @param \HiringBundle\Entity\hrm_document_jobApplicantTechnology $jobApplicationTechnology
     *
     * @return hrm_document_jobDescription
     */
    public function addJobApplicationTechnology(\HiringBundle\Entity\hrm_document_jobApplicantTechnology $jobApplicationTechnology)
    {
        $this->jobApplicationTechnology[] = $jobApplicationTechnology;

        return $this;
    }

    /**
     * Remove jobApplicationTechnology
     *
     * @param \HiringBundle\Entity\hrm_document_jobApplicantTechnology $jobApplicationTechnology
     */
    public function removeJobApplicationTechnology(\HiringBundle\Entity\hrm_document_jobApplicantTechnology $jobApplicationTechnology)
    {
        $this->jobApplicationTechnology->removeElement($jobApplicationTechnology);
    }

    /**
     * Get jobApplicationTechnology
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getJobApplicationTechnology()
    {
        return $this->jobApplicationTechnology;
    }

    /**
     * Add applicantOthersCompetenciesTag
     *
     * @param \HiringBundle\Entity\hrm_document_jobApplicantTag $applicantOthersCompetenciesTag
     *
     * @return hrm_document_jobDescription
     */
    public function addApplicantOthersCompetenciesTag(\HiringBundle\Entity\hrm_document_jobApplicantTag $applicantOthersCompetenciesTag)
    {
        $this->applicantOthersCompetenciesTags[] = $applicantOthersCompetenciesTag;

        return $this;
    }

    /**
     * Remove applicantOthersCompetenciesTag
     *
     * @param \HiringBundle\Entity\hrm_document_jobApplicantTag $applicantOthersCompetenciesTag
     */
    public function removeApplicantOthersCompetenciesTag(\HiringBundle\Entity\hrm_document_jobApplicantTag $applicantOthersCompetenciesTag)
    {
        $this->applicantOthersCompetenciesTags->removeElement($applicantOthersCompetenciesTag);
    }

    /**
     * Get applicantOthersCompetenciesTags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApplicantOthersCompetenciesTags()
    {
        return $this->applicantOthersCompetenciesTags;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return hrm_document_jobDescription
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    public function __toString() {
        return "";
    }

    /**
     * Set otherComment
     *
     * @param string $otherComment
     *
     * @return hrm_document_jobDescription
     */
    public function setOtherComment($otherComment)
    {
        $this->otherComment = $otherComment;

        return $this;
    }

    /**
     * Get otherComment
     *
     * @return string
     */
    public function getOtherComment()
    {
        return $this->otherComment;
    }
}
