<?php

namespace HiringBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_document_jobApplicantStudy
 *
 * @ORM\Table(name="hrm_document_job_applicant_study")
 * @ORM\Entity(repositoryClass="HiringBundle\Repository\hrm_document_jobApplicantStudyRepository")
 */
class hrm_document_jobApplicantStudy
{
    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   /**
     * @ORM\ManyToOne(targetEntity="hrm_document_jobDescription", inversedBy="documentJobApplicantStudy")
     * @ORM\JoinColumn(name="documentJobDescription", referencedColumnName="uid")
     */
    private $documentJobDescription;
    
   /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsStudyType")
    * @ORM\JoinColumn(name="study_type_id", referencedColumnName="uid")
    */
    private $studyTypeId;

   /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsStudyLevel")
    * @ORM\JoinColumn(name="study_level_id", referencedColumnName="uid")
    */
    private $studyLevelId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;




    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return hrm_document_jobApplicantStudy
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set documentJobDescription
     *
     * @param \HiringBundle\Entity\hrm_document_jobDescription $documentJobDescription
     *
     * @return hrm_document_jobApplicantStudy
     */
    public function setDocumentJobDescription(\HiringBundle\Entity\hrm_document_jobDescription $documentJobDescription = null)
    {
        $this->documentJobDescription = $documentJobDescription;

        return $this;
    }

    /**
     * Get documentJobDescription
     *
     * @return \HiringBundle\Entity\hrm_document_jobDescription
     */
    public function getDocumentJobDescription()
    {
        return $this->documentJobDescription;
    }

    /**
     * Set studyTypeId
     *
     * @param \AdminBundle\Entity\SettingsStudyType $studyTypeId
     *
     * @return hrm_document_jobApplicantStudy
     */
    public function setStudyTypeId(\AdminBundle\Entity\SettingsStudyType $studyTypeId = null)
    {
        $this->studyTypeId = $studyTypeId;

        return $this;
    }

    /**
     * Get studyTypeId
     *
     * @return \AdminBundle\Entity\SettingsStudyType
     */
    public function getStudyTypeId()
    {
        return $this->studyTypeId;
    }

    /**
     * Set studyLevelId
     *
     * @param \AdminBundle\Entity\SettingsStudyLevel $studyLevelId
     *
     * @return hrm_document_jobApplicantStudy
     */
    public function setStudyLevelId(\AdminBundle\Entity\SettingsStudyLevel $studyLevelId = null)
    {
        $this->studyLevelId = $studyLevelId;

        return $this;
    }

    /**
     * Get studyLevelId
     *
     * @return \AdminBundle\Entity\SettingsStudyLevel
     */
    public function getStudyLevelId()
    {
        return $this->studyLevelId;
    }
}
