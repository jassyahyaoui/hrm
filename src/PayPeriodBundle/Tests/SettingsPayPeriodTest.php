<?php

namespace PayPeriodBundle\Tests;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ExpenseTestTest extends WebTestCase {

    private $client = null;

    public function setUp() {
     //  echo "\n --------Run Unit test of SettingsPayPeriod-------- \n";
       $this->client = static::createClient();
       
       /*** user login **********/
        $this->client->request('GET', '/logout');
       $crawler = $this->client->request('GET', '/login');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('html:contains("Veuillez")')->count() > 0);

        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => "responsable",
            '_password' => "responsable",
        ));
        $this->client->submit($form);
    }

    public function testAddPayPeriod() {

        // add payperiod
       $this->client->request('POST', '/api/payperiod/payperiod/add', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'CONTENT_TYPE' => 'application/json',
                ), '{"start_pay_period":"'.date('Y-m').'"}');
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());
        
    }
    
    public function testOpenPayPeriod() {
        // open payperiod
        $this->client->request('PUT', '/api/payperiod/payperiod/open', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'CONTENT_TYPE' => 'application/json',
                ), '{"next_pay_period":"'.date('Y-m-d').'"}');
      //  echo($this->client->getResponse());
        $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());
        
    } 
    
    public function testClosePayPeriod() {
        // close payperiod
       $this->client->request('PUT', '/api/payperiod/payperiod/close', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'CONTENT_TYPE' => 'application/json',
                ), '{"id": 27, "status": "CLOTURE"}');
      // echo($this->client->getResponse()->getContent());
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), 'Error closed pay period');
    } 
       
}
