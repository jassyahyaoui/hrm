<?php

namespace PayPeriodBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SettingsPayPeriod
 *
 * @ORM\Table(name="hrm_settings_payperiod")
 * @ORM\Entity(repositoryClass="PayPeriodBundle\Repository\SettingsPayPeriodRepository")
 */
class SettingsPayPeriod
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=7, nullable=true)
     */
    private $status;

    /**
     * @var \Date
     *
     * @ORM\Column(name="start_date", type="date" , nullable=true)
     */
    private $startDate;

    /**
     * @var \Date
     *
     * @ORM\Column(name="end_date", type="date", nullable=true)
     */
    private $endDate;

    /**
     * @var \Date
     *
     * @ORM\Column(name="close_date", type="date", nullable=true)
     */
    private $closeDate;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\HrmCompany")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $companyId;

 /**
     * @var Date
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=true)
     */
    private $create_date;
    
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\HrmEmployee")
     * @ORM\JoinColumn(name="create_uid", referencedColumnName="uid")
     */    
    private $createUid;
    
    /**
     * @var Date
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $last_update;
    
    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\HrmEmployee")
     * @ORM\JoinColumn(name="last_update_uid", referencedColumnName="uid")
     */     
    private $last_update_uid;
    
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set status
     *
     * @param string $status
     *
     * @return SettingsPayPeriod
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return SettingsPayPeriod
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return SettingsPayPeriod
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set closeDate
     *
     * @param \DateTime $closeDate
     *
     * @return SettingsPayPeriod
     */
    public function setCloseDate($closeDate)
    {
        $this->closeDate = $closeDate;

        return $this;
    }

    /**
     * Get closeDate
     *
     * @return \DateTime
     */
    public function getCloseDate()
    {
        return $this->closeDate;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return SettingsPayPeriod
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return SettingsPayPeriod
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set companyId
     *
     * @param \UserBundle\Entity\HrmCompany $companyId
     *
     * @return SettingsPayPeriod
     */
    public function setCompanyId(\UserBundle\Entity\HrmCompany $companyId = null)
    {
        $this->companyId = $companyId;

        return $this;
    }

    /**
     * Get companyId
     *
     * @return \UserBundle\Entity\HrmCompany
     */
    public function getCompanyId()
    {
        return $this->companyId;
    }

    /**
     * Set createUid
     *
     * @param \UserBundle\Entity\HrmEmployee $createUid
     *
     * @return SettingsPayPeriod
     */
    public function setCreateUid(\UserBundle\Entity\HrmEmployee $createUid = null)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdateUid
     *
     * @param \UserBundle\Entity\HrmEmployee $lastUpdateUid
     *
     * @return SettingsPayPeriod
     */
    public function setLastUpdateUid(\UserBundle\Entity\HrmEmployee $lastUpdateUid = null)
    {
        $this->last_update_uid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getLastUpdateUid()
    {
        return $this->last_update_uid;
    }
}
