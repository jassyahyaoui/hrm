<?php

namespace PayPeriodBundle\Service;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use ExpenseBundle\Model\GenerateTaxModelInputs;

class payperiodService {

    protected $em;
    private $container;

    public function __construct(EntityManager $entityManager, Container $container) {
        $this->em = $entityManager;
        $this->container = $container;
    }

      public function getOpenPayPeriod()
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->get('doctrine.orm.entity_manager');
        $payperiodlist = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')
                            ->findBy(array("companyId"=>$this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany(), "status"=>"open"), array('startDate' => 'ASC'));
        if($payperiodlist)
        {
           return array('payperiodlist' => $payperiodlist);
        }
        return "null";
    }

      public function getOpenPayPeriodForcompany($companyId)
    {
        $payperiodlist = $this->container->get('doctrine')->getEntityManager()->getRepository('PayPeriodBundle:SettingsPayPeriod')
                            ->findBy(array("companyId"=>$companyId, "status"=>"open"), array('startDate' => 'ASC'));
        if($payperiodlist)
        {
           return array('payperiodlist' => $payperiodlist);
        }
        return "null";
    }
    
       public function deletePayPeriodForcompany($companyId,$startDate,$endDate)
    {
           $em = $this->container->get('doctrine')->getEntityManager();
           $payperiod = $this->container->get('doctrine')->getEntityManager()->getRepository('PayPeriodBundle:SettingsPayPeriod')
                        ->findOneBy(array("companyId"=>$companyId, 
                  "status"=>"close","startDate"=>$startDate,"endDate"=>$endDate), array('startDate' => 'ASC'));
           if($payperiod)
        {
            $payperiod->setCompany(Null);  
            $em->remove($payperiod);
            $em->flush();
            return true;
        }
        return false;
    }
    
    public function openPayPeriod($em, $use_employee, $company) {
        $_f_date = new \DateTime('first day of this month');
        $_l_date = new \DateTime('last day of this month');

        $_isExistPayPeriod = $this->_isHaveOpenPayPeriod($em, $_f_date, $use_employee, $company);
        if ($_isExistPayPeriod) {
            return true;
        }

        $payperiods = new \PayPeriodBundle\Entity\SettingsPayPeriod();
        $payperiods->setStartDate($_f_date);
        $payperiods->setEndDate($_l_date);
        $payperiods->setStatus('open');
        $payperiods->setCompanyId($company);
        $payperiods->setCreateUid($use_employee);
        $payperiods->setLastUpdateUid($use_employee);

        $payperiods->setCreateDate(new \DateTime('now'));
        $payperiods->setLastUpdate(new \DateTime('now'));
        $em->persist($payperiods);
        $em->flush();
    } 
    
    private function _isHaveOpenPayPeriod($em, $_date, $use_employee, $company) {
        return $em->getRepository('PayPeriodBundle:SettingsPayPeriod')
                        ->isHaveOpenPayPeriodByMonth(array('date' => $_date->format('Y-m-d'),
                            'companyId' => $company));
    }    
}
