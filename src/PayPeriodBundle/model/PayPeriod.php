<?php

namespace PayPeriodBundle\model;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of PayPeriod
 *
 * @author MAZ_USER2
 */
class PayPeriod {
    
    public function getObjectPayPeriod($pay_period, $container, $em, $user)
    {
        $obj_PayPeriod = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')    
                          ->findOneBy(array('startDate'=>$pay_period, 'companyId'=>$container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId()));
        
        return $obj_PayPeriod;
    } 
}
