<?php

namespace BenefitBundle\Controller;

use BenefitBundle\Entity\hrm_benefit;
use BenefitBundle\Entity\hrm_benefit_car;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FileBundle\Entity\attachments;

/**
 * Hrm_benefit controller.
 *
 */
class hrm_benefitController extends Controller
{

    public function layoutAction()
    {
        return $this->render('benefit/layout.html.twig');
    }

    public function indexAction()
    {
        return $this->render('benefit/index.html.twig');
    }

    public function newAction(Request $request)
    {
        $hrm_benefit_car = new hrm_benefit_car();
        $hrm_benefit = new hrm_benefit();

        $providerId = $this->container->get('settingsbundle.preference.service')->getProviderId();
        $companyId = $this->container->get('settingsbundle.preference.service')->getCompanyId();

        $form_car = $this->createForm('BenefitBundle\Form\hrm_benefit_carType', $hrm_benefit_car, array(
            'companyId' => $companyId,
            'providerId' => $providerId
        ));
        $form_car->handleRequest($request);

        $form = $this->createForm('BenefitBundle\Form\hrm_benefitType', $hrm_benefit, array(
            'companyId' => $companyId,
            'providerId' => $providerId
        ));

        $form->handleRequest($request);
        return $this->render('benefit/new.html.twig', array(
            'form_car' => $form_car->createView(),
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request)
    {
        $hrm_benefit_car = new hrm_benefit_car();
        $hrm_benefit = new hrm_benefit();

        $providerId = $this->container->get('settingsbundle.preference.service')->getProviderId();
        $companyId = $this->container->get('settingsbundle.preference.service')->getCompanyId();

        $form_car = $this->createForm('BenefitBundle\Form\hrm_benefit_carType', $hrm_benefit_car, array(
            'companyId' => $companyId,
            'providerId' => $providerId
        ));
        $form_car->handleRequest($request);

        $form = $this->createForm('BenefitBundle\Form\hrm_benefitType', $hrm_benefit, array(
            'companyId' => $companyId,
            'providerId' => $providerId
        ));

        $form->handleRequest($request);
        return $this->render('benefit/edit.html.twig', array(
            'form_car' => $form_car->createView(),
            'form' => $form->createView(),
        ));
    }

    public function addAction(Request $request)
    {
        $hrm_benefit = new hrm_benefit();
        $form = $this->createForm('BenefitBundle\Form\hrm_benefitType', $hrm_benefit);
        $form->handleRequest($request);
        $body = $request->getContent();
        $data = json_decode($body, true);
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');
        //set default status if ROLE_COLLABORATEUR else set with the submited value
        if ($request->isMethod('POST')) {

            $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $current_user_id = $empData->getId();

            $form->submit($data);

            $hrm_benefit->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $hrm_benefit->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $hrm_benefit->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));

            $hrm_benefit->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            $hrm_benefit->setHrmSettingsBenefitPeriodicity($em->getRepository('AdminBundle:hrm_settings_benefit_periodicity')->find($data['periodicity']['id']));

            //Add default value on create Action
            $hrm_benefit->setCreateDate(new \DateTime('now'));
            $hrm_benefit->setCreateUid($current_user_id);
            $hrm_benefit->setLastUpdateUid($current_user_id);
            $hrm_benefit->setLastUpdate(new \DateTime('now'));

            if ($request->request->has('attachments')) {
                if ($data['attachments'] != null) {
//                    if ($request->request->has('attachments')) {
                    if ($hrm_benefit->getAttachments() != null) {
                        $attachment = $hrm_benefit->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if ((isset($data['attachments']['mimetype'])) && ($data['attachments']['mimetype'] != null))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $hrm_benefit->setAttachments($attachment);
//                    }
                } else {

                    $hrm_benefit->setAttachments(null);
                }
            }
            //send mail to responsable

            $TranslateMail = $this->get('mailingbundle.email.send.service');
            if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $this->get('mailingbundle.email.send.service')->Notify($hrm_benefit, 'RESPONSABLE', $TranslateMail->TagsTranslateBenefit('titleNew'), $TranslateMail->TagsTranslateBenefit('etatNew'), $TranslateMail->TagsTranslateBenefit('typeRequest'), $TranslateMail->TagsTranslateBenefit('folder'), $TranslateMail->TagsTranslateBenefit('viewName'));
            } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $this->get('mailingbundle.email.send.service')->Notify($hrm_benefit, 'COLLABORATEUR', $TranslateMail->TagsTranslateBenefit('titleNew'), $TranslateMail->TagsTranslateBenefit('etatNew'), $TranslateMail->TagsTranslateBenefit('typeRequest'), $TranslateMail->TagsTranslateBenefit('folder'), $TranslateMail->TagsTranslateBenefit('viewName'));
            }

            $em->persist($hrm_benefit);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function addCarAction(Request $request)
    {
        $hrm_benefit_car = new hrm_benefit_car();
        $form = $this->createForm('BenefitBundle\Form\hrm_benefit_carType', $hrm_benefit_car);
        $form->handleRequest($request);
        $body = $request->getContent();
        $data = json_decode($body, true);
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');
        //set default status if ROLE_COLLABORATEUR else set with the submited value
        if ($request->isMethod('POST')) {

            $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $current_user_id = $empData->getId();
            $form->submit($data);

            $hrm_benefit_car->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $hrm_benefit_car->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $hrm_benefit_car->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));

            $hrm_benefit_car->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));

            //Add default value on create Action
            $hrm_benefit_car->setCreateDate(new \DateTime('now'));
            $hrm_benefit_car->setCreateUid($current_user_id);
            $hrm_benefit_car->setLastUpdateUid($current_user_id);
            $hrm_benefit_car->setLastUpdate(new \DateTime('now'));

            if ($request->request->has('attachments')) {
                if ($data['attachments'] != null) {
//                    if ($request->request->has('attachments')) {
                    if ($hrm_benefit_car->getAttachments() != null) {
                        $attachment = $hrm_benefit_car->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if ((isset($data['attachments']['mimetype'])) && ($data['attachments']['mimetype'] != null))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $hrm_benefit_car->setAttachments($attachment);
//                    }
                } else {

                    $hrm_benefit_car->setAttachments(null);
                }
            }
            //send mail to responsable

            $TranslateMail = $this->get('mailingbundle.email.send.service');
            if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $this->get('mailingbundle.email.send.service')->Notify($hrm_benefit_car, 'RESPONSABLE', $TranslateMail->TagsTranslateBenefitCar('titleNew'), $TranslateMail->TagsTranslateBenefitCar('etatNew'), $TranslateMail->TagsTranslateBenefitCar('typeRequest'), $TranslateMail->TagsTranslateBenefitCar('folder'), $TranslateMail->TagsTranslateBenefitCar('viewName'));
            } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $this->get('mailingbundle.email.send.service')->Notify($hrm_benefit_car, 'COLLABORATEUR', $TranslateMail->TagsTranslateBenefitCar('titleNew'), $TranslateMail->TagsTranslateBenefitCar('etatNew'), $TranslateMail->TagsTranslateBenefitCar('typeRequest'), $TranslateMail->TagsTranslateBenefitCar('folder'), $TranslateMail->TagsTranslateBenefitCar('viewName'));
            }


            $em->persist($hrm_benefit_car);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function updateCarAction(Request $request, hrm_benefit_car $hrm_benefit_car)
    {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, true);
        $hrm_benefit_car = $em->getRepository('BenefitBundle:hrm_benefit_car')->find($data['id']);
        $update_form = $this->createForm('BenefitBundle\Form\hrm_benefit_carType', $hrm_benefit_car);
        if ($request->isMethod('PUT')) {
            $update_form->submit($data);

            $collab_id = $hrm_benefit_car->getCreateUid();

            $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $current_user_id = $empData->getId();
            $hrm_benefit_car->setLastUpdate(new \DateTime('now'));
            $hrm_benefit_car->setLastUpdateUid($current_user_id);

            if ($data['fuel'] == false) {
                $hrm_benefit_car->setFuel(0);
            } else {
                $hrm_benefit_car->setFuel(1);
            }

            $hrm_benefit_car->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $hrm_benefit_car->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $hrm_benefit_car->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));
            $hrm_benefit_car->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));

            if ($data['attachments'] != null) {
                if ($request->request->has('attachments')) {
                    if ($hrm_benefit_car->getAttachments() != null) {
                        $attachment = $hrm_benefit_car->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if (isset($data['attachments']['mimetype']))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $hrm_benefit_car->setAttachments($attachment);
                }
            } else {
                $hrm_benefit_car->setAttachments(null);
            }
            $em->persist($hrm_benefit_car);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function updateAction(Request $request, hrm_benefit $hrm_benefit)
    {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, true);
        $hrm_benefit = $em->getRepository('BenefitBundle:hrm_benefit')->find($data['id']);
        $update_form = $this->createForm('BenefitBundle\Form\hrm_benefitType', $hrm_benefit);
        if ($request->isMethod('PUT')) {
            $update_form->submit($data);

            $collab_id = $hrm_benefit->getCreateUid();

            $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $current_user_id = $empData->getId();
            $hrm_benefit->setLastUpdate(new \DateTime('now'));
            $hrm_benefit->setLastUpdateUid($current_user_id);

            $hrm_benefit->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $hrm_benefit->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $hrm_benefit->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));

            $hrm_benefit->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            $hrm_benefit->setHrmSettingsBenefitPeriodicity($em->getRepository('AdminBundle:hrm_settings_benefit_periodicity')->find($data['periodicity']['id']));

            if ($data['attachments'] != null) {
                if ($request->request->has('attachments')) {
                    if ($hrm_benefit->getAttachments() != null) {
                        $attachment = $hrm_benefit->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if (isset($data['attachments']['mimetype']))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $hrm_benefit->setAttachments($attachment);
                }
            } else {
                $hrm_benefit->setAttachments(null);
            }
            $em->persist($hrm_benefit);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function allAction()
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();

        $current_user_id = $empData->getId();
        $companyData = $empData->getCompany();
        $company_id = $companyData->getId();
        $providerId = $companyData->getProvider();

        if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
            $benefit = $em->getRepository('BenefitBundle:hrm_benefit')->FindForResp($company_id, $current_user_id);
            $benefitCar = $em->getRepository('BenefitBundle:hrm_benefit')->CARFindForResp($company_id, $current_user_id);
        } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
            $benefit = $em->getRepository('BenefitBundle:hrm_benefit')->FindForCollab($current_user_id);
            $benefitCar = $em->getRepository('BenefitBundle:hrm_benefit')->CARFindForCollab($current_user_id);
        }
        $result = array_merge($benefit, $benefitCar);

        $users = $em->getRepository('UserBundle:HrmEmployee')->FindCollaborateurByCompanyId($company_id);
        return array('result' => $result, 'users' => $users);
    }

    public function removeCarAction(Request $request, hrm_benefit_car $hrm_benefit_car)
    {
        if ($request->isMethod('DELETE')) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_benefit_car);
            $em->flush($hrm_benefit_car);
            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }

    public function removeAction(Request $request, hrm_benefit $hrm_benefit)
    {
        if ($request->isMethod('DELETE')) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_benefit);
            $em->flush($hrm_benefit);
            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }

    public function ConfirmCarAction(Request $request, hrm_benefit_car $hrm_benefit_car)
    {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $hrm_benefit_car->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['confirm']));

        $entity = $em->getRepository('BenefitBundle:hrm_benefit_car')->find($hrm_benefit_car);

        //send mail to responsable
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($hrm_benefit_car, 'RESPONSABLE', $TranslateMail->TagsTranslateBenefitCar('confirmMsg'), $TranslateMail->TagsTranslateBenefitCar('confirmEtat'), $TranslateMail->TagsTranslateBenefitCar('typeRequest'), $TranslateMail->TagsTranslateBenefitCar('folder'), $TranslateMail->TagsTranslateBenefitCar('viewName'));


        $em->persist($hrm_benefit_car);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function ConfirmAction(Request $request, hrm_benefit $hrm_benefit)
    {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $hrm_benefit->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['confirm']));

        $entity = $em->getRepository('BenefitBundle:hrm_benefit')->find($hrm_benefit);

        //send mail to responsable
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($hrm_benefit, 'RESPONSABLE', $TranslateMail->TagsTranslateBenefit('confirmMsg'), $TranslateMail->TagsTranslateBenefit('confirmEtat'), $TranslateMail->TagsTranslateBenefit('typeRequest'), $TranslateMail->TagsTranslateBenefit('folder'), $TranslateMail->TagsTranslateBenefit('viewName'));


        $em->persist($hrm_benefit);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function CancelCarAction(Request $request, hrm_benefit_car $hrm_benefit_car)
    {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $hrm_benefit_car->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['cancel']));

        $entity = $em->getRepository('BenefitBundle:hrm_benefit_car')->find($hrm_benefit_car);

        //send mail to responsable
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($hrm_benefit_car, 'RESPONSABLE', $TranslateMail->TagsTranslateBenefitCar('cancelMsg'), $TranslateMail->TagsTranslateBenefitCar('cancelEtat'), $TranslateMail->TagsTranslateBenefitCar('typeRequest'), $TranslateMail->TagsTranslateBenefitCar('folder'), $TranslateMail->TagsTranslateBenefitCar('viewName'));

        $em->persist($hrm_benefit_car);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }
    
    public function CancelAction(Request $request, hrm_benefit $hrm_benefit)
    {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $hrm_benefit->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['cancel']));

        $entity = $em->getRepository('BenefitBundle:hrm_benefit_car')->find($hrm_benefit);

        //send mail to responsable
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($hrm_benefit, 'RESPONSABLE', $TranslateMail->TagsTranslateBenefit('cancelMsg'), $TranslateMail->TagsTranslateBenefit('cancelEtat'), $TranslateMail->TagsTranslateBenefit('typeRequest'), $TranslateMail->TagsTranslateBenefit('folder'), $TranslateMail->TagsTranslateBenefit('viewName'));

        $em->persist($hrm_benefit);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }
}
