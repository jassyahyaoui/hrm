<?php

namespace BenefitBundle\Entity;

/**
 * hrm_benefit_car
 */
class hrm_benefit_car
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $type;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return hrm_benefit_car
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var boolean
     */
    private $calculationMethod;

    /**
     * @var boolean
     */
    private $car;

    /**
     * @var \DateTime
     */
    private $circulationDate;

    /**
     * @var float
     */
    private $buyingPrice;

    /**
     * @var boolean
     */
    private $fuel;

    /**
     * @var float
     */
    private $participation;

    /**
     * @var float
     */
    private $cost;

    /**
     * @var float
     */
    private $totalDistance;

    /**
     * @var float
     */
    private $privateDistance;

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * @var integer
     */
    private $createUid;

    /**
     * @var \DateTime
     */
    private $lastUpdate;

    /**
     * @var integer
     */
    private $lastUpdateUid;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $contributor;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $validator;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $requester;

    /**
     * @var \AdminBundle\Entity\SettingsStatus
     */
    private $settings_status;

    /**
     * @var \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    private $settings_pay_period;
    
    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return hrm_benefit_car
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set calculationMethod
     *
     * @param boolean $calculationMethod
     *
     * @return hrm_benefit_car
     */
    public function setCalculationMethod($calculationMethod)
    {
        $this->calculationMethod = $calculationMethod;

        return $this;
    }

    /**
     * Get calculationMethod
     *
     * @return boolean
     */
    public function getCalculationMethod()
    {
        return $this->calculationMethod;
    }

    /**
     * Set car
     *
     * @param boolean $car
     *
     * @return hrm_benefit_car
     */
    public function setCar($car)
    {
        $this->car = $car;

        return $this;
    }

    /**
     * Get car
     *
     * @return boolean
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * Set circulationDate
     *
     * @param \DateTime $circulationDate
     *
     * @return hrm_benefit_car
     */
    public function setCirculationDate($circulationDate)
    {
        $this->circulationDate = $circulationDate;

        return $this;
    }

    /**
     * Get circulationDate
     *
     * @return \DateTime
     */
    public function getCirculationDate()
    {
        return $this->circulationDate;
    }

    /**
     * Set buyingPrice
     *
     * @param float $buyingPrice
     *
     * @return hrm_benefit_car
     */
    public function setBuyingPrice($buyingPrice)
    {
        $this->buyingPrice = $buyingPrice;

        return $this;
    }

    /**
     * Get buyingPrice
     *
     * @return float
     */
    public function getBuyingPrice()
    {
        return $this->buyingPrice;
    }

    /**
     * Set fuel
     *
     * @param boolean $fuel
     *
     * @return hrm_benefit_car
     */
    public function setFuel($fuel)
    {
        $this->fuel = $fuel;

        return $this;
    }

    /**
     * Get fuel
     *
     * @return boolean
     */
    public function getFuel()
    {
        return $this->fuel;
    }

    /**
     * Set participation
     *
     * @param float $participation
     *
     * @return hrm_benefit_car
     */
    public function setParticipation($participation)
    {
        $this->participation = $participation;

        return $this;
    }

    /**
     * Get participation
     *
     * @return float
     */
    public function getParticipation()
    {
        return $this->participation;
    }

    /**
     * Set cost
     *
     * @param float $cost
     *
     * @return hrm_benefit_car
     */
    public function setCost($cost)
    {
        $this->cost = $cost;

        return $this;
    }

    /**
     * Get cost
     *
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * Set totalDistance
     *
     * @param float $totalDistance
     *
     * @return hrm_benefit_car
     */
    public function setTotalDistance($totalDistance)
    {
        $this->totalDistance = $totalDistance;

        return $this;
    }

    /**
     * Get totalDistance
     *
     * @return float
     */
    public function getTotalDistance()
    {
        return $this->totalDistance;
    }

    /**
     * Set privateDistance
     *
     * @param float $privateDistance
     *
     * @return hrm_benefit_car
     */
    public function setPrivateDistance($privateDistance)
    {
        $this->privateDistance = $privateDistance;

        return $this;
    }

    /**
     * Get privateDistance
     *
     * @return float
     */
    public function getPrivateDistance()
    {
        return $this->privateDistance;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_benefit_car
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_benefit_car
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_benefit_car
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_benefit_car
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->lastUpdateUid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->lastUpdateUid;
    }

    /**
     * Set contributor
     *
     * @param \UserBundle\Entity\HrmEmployee $contributor
     *
     * @return hrm_benefit_car
     */
    public function setContributor(\UserBundle\Entity\HrmEmployee $contributor = null)
    {
        $this->contributor = $contributor;

        return $this;
    }

    /**
     * Get contributor
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getContributor()
    {
        return $this->contributor;
    }

    /**
     * Set validator
     *
     * @param \UserBundle\Entity\HrmEmployee $validator
     *
     * @return hrm_benefit_car
     */
    public function setValidator(\UserBundle\Entity\HrmEmployee $validator = null)
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * Get validator
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * Set requester
     *
     * @param \UserBundle\Entity\HrmEmployee $requester
     *
     * @return hrm_benefit_car
     */
    public function setRequester(\UserBundle\Entity\HrmEmployee $requester = null)
    {
        $this->requester = $requester;

        return $this;
    }

    /**
     * Get requester
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getRequester()
    {
        return $this->requester;
    }

    /**
     * Set settingsStatus
     *
     * @param \AdminBundle\Entity\SettingsStatus $settingsStatus
     *
     * @return hrm_benefit_car
     */
    public function setSettingsStatus(\AdminBundle\Entity\SettingsStatus $settingsStatus = null)
    {
        $this->settings_status = $settingsStatus;

        return $this;
    }

    /**
     * Get settingsStatus
     *
     * @return \AdminBundle\Entity\SettingsStatus
     */
    public function getSettingsStatus()
    {
        return $this->settings_status;
    }
    /**
     * @var \FileBundle\Entity\attachments
     */
    private $attachments;


    /**
     * Set attachments
     *
     * @param \FileBundle\Entity\attachments $attachments
     *
     * @return hrm_benefit_car
     */
    public function setAttachments(\FileBundle\Entity\attachments $attachments = null)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Get attachments
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getAttachments()
    {
        return $this->attachments;
    }
    /**
     * @var string
     */
    private $comment;


    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return hrm_benefit_car
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }
    
    /**
     * Set settingsPayPeriod
     *
     * @param \PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod
     *
     * @return PaydayVariations
     */
    public function setSettingsPayPeriod(\PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod = null)
    {
        $this->settings_pay_period = $settingsPayPeriod;

        return $this;
    }

    /**
     * Get settingsPayPeriod
     *
     * @return \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    public function getSettingsPayPeriod()
    {
        return $this->settings_pay_period;
    }    
}
