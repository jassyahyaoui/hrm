<?php

namespace BenefitBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;


class hrm_benefitType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $type = array(
            "Nourriture" => "Nourriture",
            "Logement" => "Logement",
            "Véhicule" => "Véhicule",
            "NTIC" => "NTIC",
            "Autre" => "Autre",
            "Cadeau" => "Cadeau"
        );
        $builder
            ->add('contributor', 'entity', array(
                'label' => 'Collaborateur',
//                'empty_value' => 'Sélectionner un collaborateur',
                'required' => true,
                'class' => 'UserBundle\Entity\HrmEmployee',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->where("e.roles LIKE '%COLLABORATEUR%' OR e.roles LIKE '%RESPONSABLE%'")
                        ->andwhere('u.company = :value')
                        ->setParameter('value', (int)$options['companyId']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('validator', 'entity', array(
                    'label' => 'Valideur de la demande',
//                    'empty_value' => 'Sélectionner un valideur',
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control',
                        'id' => 'id',
                        'required' => true,
                    ),
                    'class' => 'UserBundle\Entity\HrmEmployee',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('u')
                            ->leftjoin('u.user', 'e')
                            ->where("e.roles LIKE '%RESPONSABLE%'")
                            ->andwhere('u.company = :value')
                            ->setParameter('value', (int)$options['companyId']);
                    },
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix')))
            ->add('requester', 'entity', array(
                'label' => 'Collaborateur',
                'empty_value' => 'Sélectionner un requester',
                'required' => true,
                'disabled' => true,
                'read_only' => true,
                'class' => 'UserBundle\Entity\HrmEmployee',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->Where('u.company = :value')
//                        ->andwhere('e.id = :valueUser')
                        ->setParameter('value', (int)$options['companyId']);
//                        ->setParameter('valueUser', (int)$options['currentUser']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('type', 'choice', array(
                'label' => 'Type d\'avantage en nature',
                'choices' => $type,
                'required' => true,
                'attr' => array('class' => 'form-control',
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('date', 'date', array(
                'label' => 'Date du frais',
                'required' => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'class' => 'form-control datepicker',
                    'date-directive' => ''
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('realAmount', 'text', array('required' => true,
                'label' => 'Montant ',
                'attr' => array(
                    'class' => 'form-control',
                    'id' => 'amount',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('hrm_settings_benefit_periodicity', 'entity', array(
                'label' => 'Periodicity',
                'empty_value' => 'Sélectionner une periodicité',
                'required' => true,
                'class' => 'AdminBundle\Entity\hrm_settings_benefit_periodicity',
                'property'=>'display_name',
                'query_builder' => function (EntityRepository $er) use($options) {
                    return $er->createQueryBuilder("b")
                        ->leftJoin('b.provider', 'p')
                        ->Where('p.id = :providerId')
                        ->setParameter('providerId', (int) $options['providerId']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))

            ->add('comment', 'textarea', array(
                'label' => 'Commentaire',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right'
                )
            ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'BenefitBundle\Entity\hrm_benefit',
            'providerId' => null,
            'companyId' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'benefitbundle_hrm_benefit';
    }


}
