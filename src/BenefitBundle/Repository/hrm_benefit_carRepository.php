<?php

namespace BenefitBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
/**
 * hrm_benefitRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class hrm_benefit_carRepository extends \Doctrine\ORM\EntityRepository
{
    
     /*************** get Grid data BENEFIT CAR by status belong PayPeriod*******************/
    public function BenefitCarStaticDataOfGridChartsFindByUserCompanyId($Id, $CurrentUserId, $obj_Payperiod, $isbelongCompany = TRUE) {
       $ret = $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select("u.id as user_id, Count(i) nbr_request,SUM(i.buyingPrice) as total, u.first_name, u.last_name , 'AVNATAGES_NATURE' as type, '€' as fixer")
                        ->from('BenefitBundle:hrm_benefit_car', 'i')
                        ->leftJoin('i.settings_status', 's')
                        ->leftJoin('i.settings_pay_period', 'pp')
                        ->leftJoin('i.contributor', 'con')
                        ->leftJoin('con.user', 'u');
                    
                        if($isbelongCompany)
                        {    
                           $ret->leftJoin('i.validator', 'v')
                            ->leftJoin('con.company', 'co')
                            ->andwhere('co.id = :value')            
                            ->andWhere('i.createUid = :CurrentUserId OR v.id = :CurrentUserId OR con.id = :CurrentUserId')
                            ->setParameter('value', (int)$Id)
                            ->setParameter('CurrentUserId', $CurrentUserId);
                        }else{
                            $ret->andWhere('con.id = :CurrentUserId')
                               ->setParameter('CurrentUserId', $Id);                 
                        }                 
                        $ret->andWhere('s.id = :s')
                        ->setParameter('s', 2)
                        ->andWhere('pp.id = :pay_period_id')
                        ->setParameter('pay_period_id', $obj_Payperiod->getId())
                        ->groupBy('u');
                    return $ret->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }  
    
    /*************** get total benefit car by status *******************/
     public function BenefitCarStaticDataFindStaticDByUserCompanyId($Id, $pay_period_id, $isbelongCompany = TRUE) {
        $ret  = $this->getEntityManager()
                    ->createQueryBuilder()
                    ->addselect("SUM(CASE WHEN s.id = 2 THEN 1 ELSE 0 END) AS valid")
                        ->addselect("SUM(CASE WHEN s.id = 1 THEN 1 ELSE 0 END) AS waiting")
                        ->addselect("SUM(CASE WHEN s.id = 4 THEN 1 ELSE 0 END) AS draft")
                        ->from('BenefitBundle:hrm_benefit_car', 'i')
                        ->leftJoin('i.settings_status', 's')
                        ->leftJoin('i.settings_pay_period', 'pp')
                        ->leftJoin('i.contributor', 'e');
                        if($isbelongCompany)
                        {
                        $ret->leftJoin('e.company', 'co')
                            ->andWhere('co.id = :value')
                            ->setParameter('value', (int)$Id);
                        }
                        else
                        {
                         $ret->andWhere('e.id = :CurrentUserId')
                             ->setParameter('CurrentUserId', $Id);            
                        }
                        $ret->andWhere('pp.id = :pay_period_id')
                            ->setParameter('pay_period_id', $pay_period_id);
            return $ret->getQuery()
                       ->getResult(Query::HYDRATE_ARRAY);
    }     
}
