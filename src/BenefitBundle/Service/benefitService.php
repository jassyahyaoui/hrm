<?php

namespace BenefitBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class benefitService {

    protected $em;
    private $container;

    public function __construct(EntityManager $entityManager, Container $container) {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function deleteAllBenefit() {
        $qb = $this->em->createQuery('DELETE BenefitBundle:hrm_benefit h')->execute();
    }

    public function findBenefit($value) {
        $benefit = $this->container->get('doctrine')->getEntityManager()->getRepository('BenefitBundle:hrm_benefit')
                ->findOneBy(array('realAmount' => $value));
        return $benefit;
    }

    public function findListBenefit($value) {
        $benefits = $this->container->get('doctrine')->getEntityManager()->getRepository('BenefitBundle:hrm_benefit')
                ->findBy(array('realAmount' => $value));
        return $benefits;
    }

}
