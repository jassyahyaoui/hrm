<?php

namespace ExpenseBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * hrm_expenses_diningRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class hrm_expenses_diningRepository extends \Doctrine\ORM\EntityRepository {



    public function FindForCollab($CurrentUserId) {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('i,pp,s,a,v,vu,vc,vcp,c,cu,cc,ccp,r,ru,rc,rcp,n')
            ->from('ExpenseBundle:hrm_expenses_dining', 'i')

            ->leftJoin('i.settings_status', 's')

            ->leftJoin('i.settings_exp_dining', 'n')
//            ->leftJoin('i.nature', 'n')
            // manager Bloc
            ->leftJoin('i.validator', 'v')
            ->leftJoin('v.user', 'vu')
            ->leftJoin('v.company', 'vc')
            ->leftJoin('vc.provider', 'vcp')
            // employee Bloc
            ->leftJoin('i.contributor', 'c')
            ->leftJoin('c.user', 'cu')
            ->leftJoin('c.company', 'cc')
            ->leftJoin('cc.provider', 'ccp')
            // Requester Bloc
            ->leftJoin('i.requester', 'r')
            ->leftJoin('r.user', 'ru')
            ->leftJoin('r.company', 'rc')
            ->leftJoin('rc.provider', 'rcp')
            // type Bloc
            ->leftJoin('i.settings_pay_period', 'pp')
            // Attachment Bloc
            ->leftJoin('i.attachments', 'a')
            // Condition Bloc
            ->andWhere('s.id != :s OR r.id = :CurrentUserId ')
            ->andWhere('r.id = :CurrentUserId OR c.id = :CurrentUserId')
            ->setParameter('s', 4)
            ->setParameter('CurrentUserId', $CurrentUserId)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }


    public function FindForResp($companyId, $CurrentUserId) {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('i,pp,s,a,v,vu,vc,vcp,c,cu,cc,ccp,r,ru,rc,rcp,n')
            ->from('ExpenseBundle:hrm_expenses_dining', 'i')

            ->leftJoin('i.settings_status', 's')

                        ->leftJoin('i.settings_exp_dining', 'n')
//            ->leftJoin('i.nature', 'n')
            // manager Bloc
            ->leftJoin('i.validator', 'v')
            ->leftJoin('v.user', 'vu')
            ->leftJoin('v.company', 'vc')
            ->leftJoin('vc.provider', 'vcp')
            // employee Bloc
            ->leftJoin('i.contributor', 'c')
            ->leftJoin('c.user', 'cu')
            ->leftJoin('c.company', 'cc')
            ->leftJoin('cc.provider', 'ccp')
            // Requester Bloc
            ->leftJoin('i.requester', 'r')
            ->leftJoin('r.user', 'ru')
            ->leftJoin('r.company', 'rc')
            ->leftJoin('rc.provider', 'rcp')
            // type Bloc
            ->leftJoin('i.settings_pay_period', 'pp')
            // Attachment Bloc
            ->leftJoin('i.attachments', 'a')
            // Condition Bloc
            ->where('vc.id = :value')
            ->andWhere('s.id != :s OR r.id = :CurrentUserId')
            ->andWhere('r.id = :CurrentUserId OR v.id = :CurrentUserId OR c.id = :CurrentUserId')
            ->setParameter('value', (int) $companyId)
            ->setParameter('CurrentUserId', $CurrentUserId)
            ->setParameter('s', 4)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }


//
//    public function DiningfindByUser($CurrentUserId) {
//        return $this->getEntityManager()
//                        ->createQueryBuilder()
//                        ->select('n,d,s,p,u,e,g,a,con,conu,v,vu,pp')
//                        ->from('ExpenseBundle:hrm_expenses_dining', 'd')
//                        ->leftJoin('d.settings_status', 's')
//                        ->leftJoin('d.settings_projects', 'p')
//                        ->leftJoin('d.settings_pay_period', 'pp')
//                        ->leftJoin('d.user', 'e')
//                        ->leftJoin('e.user', 'u')
//                        ->leftJoin('d.contributor', 'con')
//                 ->leftJoin('con.user', 'conu')
//                        ->leftJoin('d.validator', 'v')
//                 ->leftJoin('v.user', 'vu')
//                        ->leftJoin('d.attachments', 'a')
//                        ->leftJoin('d.hrm_expenses_guests', 'g')
//                        ->leftJoin('d.settings_exp_dining', 'n')
//                        ->andWhere('s.id != :s OR d.createUid = :CurrentUserId ')
//                        ->andWhere('con.id = :CurrentUserId')
//                        ->setParameter('s', 4)
//                        ->setParameter('CurrentUserId', $CurrentUserId)
//                        ->getQuery()
//                        ->getResult(Query::HYDRATE_ARRAY);
//    }
//
//    public function DiningfindByUserCompanyId($companyId, $CurrentUserId) {
//        return $this->getEntityManager()
//                        ->createQueryBuilder()
//                        ->select('n,d,s,p,u,g,d,a,con,conu,v,vu,pp')
//                        ->from('ExpenseBundle:hrm_expenses_dining', 'd')
//                        ->leftJoin('d.settings_status', 's')
//                        ->leftJoin('d.settings_projects', 'p')
//                        ->leftJoin('d.settings_pay_period', 'pp')
//                        ->leftJoin('d.user', 'e')
//                        ->leftJoin('e.user', 'u')
//                        ->leftJoin('d.contributor', 'con')
//                        ->leftJoin('con.user', 'conu')
//                        ->leftJoin('d.validator', 'v')
//                        ->leftJoin('v.user', 'vu')
//                        ->leftJoin('d.hrm_expenses_guests', 'g')
//                        ->leftJoin('d.attachments', 'a')
//                        ->leftJoin('e.company', 'c')
//                        ->leftJoin('d.settings_exp_dining', 'n')
//                        ->where('c.id = :value')
//                        ->andWhere('s.id != :s OR d.createUid = :CurrentUserId')
//                        ->andWhere('d.createUid = :CurrentUserId OR v.id = :CurrentUserId')
//                        ->setParameter('value', (int) $companyId)
//                        ->setParameter('CurrentUserId', $CurrentUserId)
//                        ->setParameter('s', 4)
//                        ->getQuery()
//                        ->getResult(Query::HYDRATE_ARRAY);
//    }
   public function expenseDiningForPayPeriodList($array_data) {

        return $this->getEntityManager()
                        ->createQueryBuilder()
                       ->from('ExpenseBundle:hrm_expenses_dining', 'ed')
                        ->leftJoin('ed.contributor', 'e')
                        ->leftJoin('ed.settings_status', 's')
                        ->leftJoin('ed.settings_pay_period', 'pp')
                        ->addselect('u.last_name')
                         ->addselect('u.first_name')
                        ->addselect('u.id emp')
                        ->leftJoin('e.user', 'u')
                        //->addselect('ed.payPeriod as period')
                        ->addselect("SUM(ed.amountDin) AS expense_dining_amount")
                        ->andWhere('s.id = :s')
                        ->setParameter('s', 2)
                        ->andWhere('e.company = :company_id')
                        ->setParameter('company_id', $array_data['company_id'])
                         ->andWhere('pp.id = :pay_period_id')
                        ->setParameter('pay_period_id', $array_data['pay_period_id'])
                        ->groupBy('e.id')
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }
    
    public function findSettingsExpDining($expense_id)
    {
        return $this->getEntityManager()
                ->createQueryBuilder()
                ->select('i, sttg_exp')
                ->from('ExpenseBundle:hrm_expenses_dining', 'i')
                ->leftJoin('i.settings_exp_dining', 'sttg_exp')
                ->where('i.uid = :expense_id')
                ->setParameter('expense_id', $expense_id)
                ->getQuery()
                ->getResult();
    }  
    
    public function findByPayPeriodBelongExpenseHaventTaxes($obj_payperiod, $user, $list_expense_Id_without_taxes)
    {
        return $this->getEntityManager()
         ->createQueryBuilder()
         ->select('i, sttg_trans')
         ->from('ExpenseBundle:hrm_expenses_dining', 'i')
         ->leftJoin('i.settings_exp_dining', 'sttg_trans')
         ->leftJoin('i.settings_status', 's')
         ->leftJoin('i.contributor', 'e')
         ->leftJoin('e.company', 'c')
         ->andWhere('i.settings_pay_period = :pay_period_id')
         ->setParameter('pay_period_id', $obj_payperiod->getId())
         ->andWhere('i.uid NOT IN (:expense_id)')
         ->andWhere('s.id = :value')
         ->andwhere('c.id = :company_id')
         ->setParameter('value', 2)       
         ->setParameter('company_id', $user->getCompany()->getId())
         ->setParameter('expense_id', array_values($list_expense_Id_without_taxes), \Doctrine\DBAL\Connection::PARAM_INT_ARRAY)
         ->getQuery()
         ->getResult(); 
    }      

    /*************** get total dining by status *******************/
     public function DiningStaticDataOfDonutChartsFindByUserCompanyId($Id, $pay_period_id, $isbelongCompany = TRUE) {
        $ret  = $this->getEntityManager()
                    ->createQueryBuilder()
                    ->addselect("SUM(CASE WHEN s.id = 2 THEN 1 ELSE 0 END) AS valid")
                        ->addselect("SUM(CASE WHEN s.id = 1 THEN 1 ELSE 0 END) AS waiting")
                        ->addselect("SUM(CASE WHEN s.id = 4 THEN 1 ELSE 0 END) AS draft")
                        ->from('ExpenseBundle:hrm_expenses_dining', 'i')
                        ->leftJoin('i.settings_status', 's')
                        ->leftJoin('i.settings_pay_period', 'pp')
                        ->leftJoin('i.contributor', 'e');
                        if($isbelongCompany)
                        {
                        $ret->leftJoin('e.company', 'co')
                            ->andWhere('co.id = :value')
                            ->setParameter('value', (int)$Id);
                        }
                        else
                        {
                         $ret->andWhere('e.id = :CurrentUserId')
                             ->setParameter('CurrentUserId', $Id);            
                        }
                        $ret->andWhere('pp.id = :pay_period_id')
                            ->setParameter('pay_period_id', $pay_period_id);
            return $ret->getQuery()
                       ->getResult(Query::HYDRATE_ARRAY);
    } 
    
    /*************** get total Dining by status and GROUP by YEARS belong PayPeriod*******************/
     public function DiningStaticDataOfBarChartsFindByUserCompanyId($Id, $pay_period_year, $isbelongCompany = TRUE) {         
        $emConfig = $this->getEntityManager()->getConfiguration();
        $emConfig->addCustomDatetimeFunction('DATE_FORMAT', 'DoctrineExtensions\Query\Mysql\DateFormat');
        $ret = $this->getEntityManager()
                        ->createQueryBuilder()
                        ->addselect("DATE_FORMAT(pp.startDate, '%Y-%m') as month")
                        ->addselect("SUM(CASE WHEN s.id = 2 THEN i.amountDin ELSE 0 END) AS valid")
                        ->addselect("SUM(CASE WHEN s.id = 1 THEN i.amountDin ELSE 0 END) AS waiting")
                        ->from('ExpenseBundle:hrm_expenses_dining', 'i')
                        ->leftJoin('i.settings_status', 's')
                        ->leftJoin('i.settings_pay_period', 'pp')
                        ->leftJoin('i.contributor', 'e');
                    if($isbelongCompany)
                    {    
                      $ret->leftJoin('e.company', 'co')
                        ->andWhere('co.id = :value')
                        ->setParameter('value', (int)$Id);
                    }else{
                    $ret->andWhere('e.id = :CurrentUserId')
                       ->setParameter('CurrentUserId', $Id);                 
                    }   
                    $ret->andWhere("DATE_FORMAT(pp.startDate, '%Y') = :pay_period_year")
                        ->setParameter('pay_period_year', $pay_period_year)                
                        ->groupBy('pp');
                    return $ret->getQuery()
                               ->getResult(Query::HYDRATE_ARRAY);
    }   
    /*************** get Grid data Dining by status belong PayPeriod*******************/
    public function DiningStaticDataOfGridChartsFindByUserCompanyId($Id, $CurrentUserId, $obj_Payperiod, $isbelongCompany = TRUE) {
        $ret = $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select("u.id as user_id, Count(d) nbr_request,SUM(d.amountDin) as total, u.first_name, u.last_name, 'Frais' as type, '€' as fixer")
                        ->from('ExpenseBundle:hrm_expenses_dining', 'd')
                        ->leftJoin('d.settings_status', 's')
                        ->leftJoin('d.settings_projects', 'p')
                        ->leftJoin('d.settings_pay_period', 'pp')
                        ->leftJoin('d.contributor', 'con')
                        ->leftJoin('con.user', 'u');
                    
                if($isbelongCompany)
                {    
                   $ret->leftJoin('d.validator', 'v')
                    ->leftJoin('con.company', 'co')
                    ->andwhere('co.id = :value')            
                    ->andWhere('d.createUid = :CurrentUserId OR v.id = :CurrentUserId OR con.id = :CurrentUserId')
                    ->setParameter('value', (int)$Id)
                    ->setParameter('CurrentUserId', $CurrentUserId);
                }else{
                    $ret->andWhere('con.id = :CurrentUserId')
                       ->setParameter('CurrentUserId', $Id);                 
                }                 
                $ret->andWhere('s.id = :s')
                ->setParameter('s', 2)
                ->andWhere('pp.id = :pay_period_id')
                ->setParameter('pay_period_id', $obj_Payperiod->getId())
                ->groupBy('u');
            return $ret->getQuery()
                ->getResult(Query::HYDRATE_ARRAY);
    }



    public function findByPayPeriodBelongDinning($obj_payperiod, $user)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('i')
            ->from('ExpenseBundle:hrm_expenses_dining', 'i')
            ->leftJoin('i.settings_status', 's')
            ->leftJoin('i.contributor', 'e')
            ->leftJoin('e.company', 'c')
            ->where('i.settings_pay_period = :pay_period_id')
            ->andWhere('s.id = :value')
            ->setParameter('value', 2)
            ->andwhere('c.id = :company_id')
            ->setParameter('pay_period_id', $obj_payperiod->getId())
            ->setParameter('company_id', $user->getCompany()->getId())
            ->getQuery()
            ->getResult();
    }
}
