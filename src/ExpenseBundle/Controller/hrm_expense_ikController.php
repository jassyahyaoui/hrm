<?php

namespace ExpenseBundle\Controller;

use ExpenseBundle\Entity\hrm_expense_ik;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\HttpFoundation\JsonResponse;
use FileBundle\Entity\attachments;
use PayPeriodBundle\Entity\SettingsPayPeriod;
use PayPeriodBundle\model\PayPeriod;

/**
 * Ik controller.
 *
 */
class hrm_expense_ikController extends Controller
{

    public function statusInsertion($status = null, $entity)
    {
        $date = null;
        if ($status == "2" || $status == "3") {
            $LastUpdateDate = new \DateTime("now");
            $date = $LastUpdateDate->getTimestamp();
        } else {
            $entity->setSettingsPayPeriod(null);
        }
        return $date;
    }


    /**
     *
     * @param type $em
     * @param type $entity
     * @param type $payPeriodDate celui envoyé depuis from
     * @return boolean/object
     */
    private function setOpenedSettingsPayPeriod($em, $entity, $payPeriodDate = null)
    {
        $ret = '';
        // affect first pay period

        if ($payPeriodDate == null) {
            $searchBy = array("companyId" => $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId(),
                "status" => "open");
        } else {
            $searchBy = array("companyId" => $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId());
        }

        $payperiodlist = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')
            ->findBy($searchBy, array('startDate' => 'ASC'));
        if ($payperiodlist) {

            if ($payPeriodDate == null) {
                $entity->setSettingsPayPeriod($payperiodlist[0]);
                $ret = true;

            } else {
                foreach ($payperiodlist as $key => $value) {
                    if ($value->getStartDate()->format('m/Y') == $payPeriodDate && ($value->getStatus() === 'open')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = true;
                        break;
                    }
                    if (($value->getStartDate()->format('m/Y') === $payPeriodDate) && ($value->getStatus() === 'close')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = false;
                        break;
                    }
                }
            }

            if ($ret === true)
                return true;
            if ($ret === false)
                return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_CLOSED_PAY_PERIOD"));

            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_NOT_PAY_PERIOD"));

        } else {
            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_EMPTY_PAY_PERIOD"));
        }

    }


    public function ConfirmAction(Request $request, hrm_expense_ik $ik)
    {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);
        $ik->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['confirm']));
        $entity = $em->getRepository('ExpenseBundle:hrm_expense_ik')->find($ik);

        /* $SettingsPayPeriod = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')->findAll();
         $this->openORcloseAction($data['confirm'], $ik, $SettingsPayPeriod, $em);*/

        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $entity);
        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }
        //Mail notifier to Collaborateur
//        if ($ik->getCreateUid() != NULL) {
//            if ($ik->getCreateUid() == $ik->getContributor()->getId()) {
//            //    $this->get('mailingbundle.email.send.service')->SendMail($entity, $TranslateMail->TagsTranslateExpense('confirmMsg'), $TranslateMail->TagsTranslateExpense('confirmEtat'), $entity->getAmountIk());
//            }
//        }
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($entity, 'RESPONSABLE', $TranslateMail->TagsTranslateExpense('confirmMsg'), $TranslateMail->TagsTranslateExpense('confirmEtat'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));

        $em->persist($ik);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function CancelAction(Request $request, hrm_expense_ik $ik)
    {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $ik->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['cancel']));

        $entity = $em->getRepository('ExpenseBundle:hrm_expense_ik')->find($ik);

        $SettingsPayPeriod = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')->findAll();
        //$this->openORcloseAction($data['cancel'], $ik, $SettingsPayPeriod, $em);

        // try to add first open one of pay period
        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $entity);
        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }

        //Mail notifier to Collaborateur
//        $TranslateMail = $this->get('mailingbundle.email.send.service');
        //$this->get('mailingbundle.email.send.service')->SendMail($entity, $TranslateMail->TagsTranslateExpense('cancelMsg'), $TranslateMail->TagsTranslateExpense('cancelEtat'), $entity->getAmountIk());
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($entity, 'RESPONSABLE', $TranslateMail->TagsTranslateExpense('cancelMsg'), $TranslateMail->TagsTranslateExpense('cancelEtat'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));

        $em->persist($ik);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    /**
     * Lists all ik entities.
     *
     */
    public function allAction()
    {
        $em = $this->getDoctrine()->getManager();
        $ik = new hrm_expense_ik();

        $securityContext = $this->container->get('security.authorization_checker');
//        $current_user_id = $this->get('security.context')->getToken()->getUser();
//        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();

        $current_user_id = $empData->getId();
        $companyData = $empData->getCompany();
        $company_id = $companyData->getId();
        $providerId = $companyData->getProvider();
        if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {

            $iks = $em->getRepository('ExpenseBundle:hrm_expense_ik')->FindForCollab($current_user_id);
            $transport = $em->getRepository('ExpenseBundle:hrm_expense_transport')->FindForCollab($current_user_id);
            $accomodation = $em->getRepository('ExpenseBundle:hrm_expenses_accomodation')->FindForCollab($current_user_id);
            $dining = $em->getRepository('ExpenseBundle:hrm_expenses_dining')->FindForCollab($current_user_id);
            $other = $em->getRepository('ExpenseBundle:hrm_expenses_other')->FindForCollab($current_user_id);
        } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
            $iks = $em->getRepository('ExpenseBundle:hrm_expense_ik')->FindForResp($company_id, $current_user_id);
            $transport = $em->getRepository('ExpenseBundle:hrm_expense_transport')->FindForResp($company_id, $current_user_id);
            $accomodation = $em->getRepository('ExpenseBundle:hrm_expenses_accomodation')->FindForResp($company_id, $current_user_id);
            $dining = $em->getRepository('ExpenseBundle:hrm_expenses_dining')->FindForResp($company_id, $current_user_id);
            $other = $em->getRepository('ExpenseBundle:hrm_expenses_other')->FindForResp($company_id, $current_user_id);
        } else {
            $iks = null;
            $transport = null;
            $accomodation = null;
            $dining = null;
            $other = null;
        }

        $users = $em->getRepository('UserBundle:HrmEmployee')->FindCollaborateurByCompanyId($company_id);

        $result = array_merge($iks, $transport, $accomodation, $dining, $other);
//        $result = array_merge($iks,$transport);

        return array('result' => $result, 'users' => $users);
    }

    public function addAction(Request $request)
    {
        $ik = new hrm_expense_ik();

        $form = $this->createForm('ExpenseBundle\Form\hrm_expense_ikType', $ik);
        $form->handleRequest($request);

        $body = $request->getContent();
        $data = json_decode($body, true);

        $em = $this->getDoctrine()->getManager();
        //set default status if ROLE_COLLABORATEUR else set with the submited value 
        $securityContext = $this->container->get('security.authorization_checker');

        if ($request->isMethod('POST')) {
            // will be get error if not deleted settings_pay_period from FormType before submit
            $form->remove('settings_pay_period');

            $current_user_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getId();
            $form->submit($data);

            //set default status if ROLE_COLLABORATEUR else set with the submited value 
            if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                //Send mail to responsable

                $ik->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
                $this->statusInsertion($data['status'], $ik);
            } else if ($securityContext->isGranted('ROLE_ADMIN')) {
                $ik->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
                $this->statusInsertion($data['status'], $ik);
            } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $ik->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
                $this->statusInsertion($data['status'], $ik);
            }
            //Add values for all forgein Keys 

            $ik->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $ik->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']));
            $ik->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));
//            $ik->setSettingsProjects($em->getRepository('AdminBundle:SettingsProjects')->find($data['project']));
            $ik->setSettingsExpIk($em->getRepository('AdminBundle:hrm_settings_exp_ik')->find($data['coefficient']));
            $ik->setUser($em->getRepository('UserBundle:HrmEmployee')->find($current_user_id));

            if ($request->request->has('attachments')) {
                if ($data['attachments'] != null) {
//                    if ($request->request->has('attachments')) {
                    if ($ik->getAttachments() != null) {
                        $attachment = $ik->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if ((isset($data['attachments']['mimetype'])) && ($data['attachments']['mimetype'] != null))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $ik->setAttachments($attachment);
//                    }
                } else {

                    $ik->setAttachments(null);
                }
            }

            //Add default value on create Action
            $ik->setCreateDate(new \DateTime('now'));
            $ik->setCreateUid($current_user_id);
            $ik->setLastUpdateUid($current_user_id);
            $ik->setLastUpdate(new \DateTime('now'));

            //send mail to responsable
//            $TranslateMail = $this->get('mailingbundle.email.send.service');
            //  $this->get('mailingbundle.email.send.service')->SendMailToResponsable($TranslateMail->TagsTranslateExpense('titleNew'), $TranslateMail->TagsTranslateExpense('etatNew'), $ik, $em->getRepository('UserBundle:HrmEmployee')->find($data['validator']), $ik->getAmountIk());

            $TranslateMail = $this->get('mailingbundle.email.send.service');
            if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $this->get('mailingbundle.email.send.service')->Notify($ik, 'RESPONSABLE', $TranslateMail->TagsTranslateExpense('titleNew'), $TranslateMail->TagsTranslateExpense('etatNew'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));
            } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $this->get('mailingbundle.email.send.service')->Notify($ik, 'COLLABORATEUR', $TranslateMail->TagsTranslateExpense('titleNew'), $TranslateMail->TagsTranslateExpense('etatNew'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));
            }
            // try to add first open one of pay period
            $ret_payperiod =  $this->setOpenedSettingsPayPeriod($em, $ik, $data['settings_pay_period']);

            if($ret_payperiod !== true)
            {
                return $ret_payperiod;
            }

            try {
                $em->persist($ik);
                $em->flush();
                $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
                return $response;
            } catch (\Exception $exc) {
                return new \Symfony\Component\BrowserKit\Response('Error to save data', 201);
            }
        }
    }

    /**
     * Displays a form to update an existing ik entity.
     *
     */
    public function updateAction(Request $request, hrm_expense_ik $ik)
    {
        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $entity = $em->getRepository('ExpenseBundle:hrm_expense_ik')->find($data['uid']);
        if (!is_object($entity)) {
            return new \Symfony\Component\BrowserKit\Response('DATA_NOT_FOUND', 404);
        }

        $update_form = $this->createForm('ExpenseBundle\Form\hrm_expense_ikType', $entity);

        if ($request->isMethod('PUT')) {
            // will be get error if not deleted settings_pay_period from FormType before submit
            $update_form->remove('settings_pay_period');

            $securityContext = $this->container->get('security.authorization_checker');
            //get current connected user
            $current_user_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getId();

            $update_form->submit($data);

            //get the Id of user who request ik
            $infos_user = $em->getRepository('UserBundle:hrm_user')->find($current_user_id);
            $collab_id = $entity->getCreateUid();


            $ik->setUser($em->getRepository('UserBundle:HrmEmployee')->find($collab_id));

            $ik->setLastUpdate(new \DateTime('now'));
            $ik->setLastUpdateUid($current_user_id);

            if ($data['attachments'] != null) {
                if ($request->request->has('attachments')) {
                    if ($entity->getAttachments() != null) {
                        $attachment = $entity->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if (isset($data['attachments']['mimetype']))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $ik->setAttachments($attachment);
                }
            } else {

                $ik->setAttachments(null);
            }


            if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $ik->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
                $this->statusInsertion($data['status'], $ik);
                if ($data['status'] == 1) {
//                    $this->SendMailToResponsable('Nouvelle demande');
                }
            } else if ($securityContext->isGranted('ROLE_ADMIN')) {
                $ik->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
                $this->statusInsertion($data['status'], $ik);
            } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $ik->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
                $this->statusInsertion($data['status'], $ik);

                switch ($data['status']) {
                    case 2:
                        $TranslateMail = $this->get('mailingbundle.email.send.service');
                        $this->get('mailingbundle.email.send.service')->SendMail($entity, $TranslateMail->TagsTranslateExpense('confirmMsg'), $TranslateMail->TagsTranslateExpense('confirmEtat'));

                        break;
                    case 3:
                        $TranslateMail = $this->get('mailingbundle.email.send.service');
                        $this->get('mailingbundle.email.send.service')->SendMail($entity, $TranslateMail->TagsTranslateExpense('cancelMsg'), $TranslateMail->TagsTranslateExpense('cancelEtat'));
                        break;
                }
            }

            $ik->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $ik->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $ik->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));
//            $ik->setSettingsProjects($em->getRepository('AdminBundle:SettingsProjects')->find($data['project']));
            $ik->setSettingsExpIk($em->getRepository('AdminBundle:hrm_settings_exp_ik')->find($data['coefficient']));


            // try to add first open one of pay period
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $entity, $data['settings_pay_period']);
            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }

            $em->persist($entity);
            $em->flush();

            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function removeAction(Request $request, hrm_expense_ik $ik)
    {
        $form = $this->createDeleteForm($ik);
        $form->handleRequest($request);

        if ($request->isMethod('DELETE')) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ik);
            $em->flush($ik);
            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $iks = $em->getRepository('ExpenseBundle:hrm_expense_ik')->findAll();

        return $this->render('hrm_expense/hrm_expense_ik/index.html.twig', array(
            'iks' => $iks,
        ));
    }

    /**
     * Creates a new ik entity.
     *
     */
    public function modaleditAction(Request $request)
    {
        $ik = new hrm_expense_ik();
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();

        $infos_user = $em->getRepository('UserBundle:hrm_user')->find($current_user_id);
        $provider = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $form = $this->createForm('ExpenseBundle\Form\hrm_expense_ikType', $ik, array(
            'user_id' => $company_id, 'provider' => $provider
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($ik);
            $em->flush($ik);

            return $this->redirectToRoute('ik_show', array('id' => $ik->getId()));
        }

        return $this->render('hrm_expense/hrm_expense_ik/modal_edit.html.twig', array(
            'ik' => $ik,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new ik entity.
     *
     */
    public function newAction(Request $request)
    {
        $ik = new hrm_expense_ik();
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();

        $infos_user = $em->getRepository('UserBundle:hrm_user')->find($current_user_id);

        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();

        $provider = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $form = $this->createForm('ExpenseBundle\Form\hrm_expense_ikType', $ik, array(
            'user_id' => $company_id, 'provider' => $provider,
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($training);
            $em->flush($training);

            return $this->redirectToRoute('ik_show', array('id' => $ik->getId()));
        }

        return $this->render('hrm_expense/hrm_expense_ik/new.html.twig', array(
            'ik' => $ik,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ik entity.
     *
     */
    public function showAction(hrm_expense_ik $ik)
    {
        $deleteForm = $this->createDeleteForm($ik);

        return $this->render('hrm_expense/hrm_expense_ik/show.html.twig', array(
            'ik' => $ik,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function editAction(Request $request, hrm_expense_ik $ik)
    {
        $deleteForm = $this->createDeleteForm($ik);
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();

        $infos_user = $em->getRepository('UserBundle:HrmEmployee')->find($current_user_id);

        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();

        $editForm = $this->createForm('ExpenseBundle\Form\hrm_expense_ikType', $ik, array(
            'user_id' => $company_id,
        ));

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('ik_edit', array('id' => $ik->getId()));
        }

        return $this->render('hrm_expense/hrm_expense_ik/edit.html.twig', array(
            'ik' => $ik,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a ik entity.
     *
     */
    public function deleteAction(Request $request, hrm_expense_ik $ik)
    {
        $form = $this->createDeleteForm($ik);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($ik);
            $em->flush($ik);
        }

        return $this->redirectToRoute('ik_index');
    }

    /**
     * Creates a form to delete a ik entity.
     *
     * @param ik $ik The ik entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_expense_ik $ik)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('ik_delete', array('id' => $ik->getUid())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function dropzoneAction()
    {

        return $this->render('dropzone/uploader.html.twig');
    }

    /**
     * Lists all data charts of exepnese entities.
     *
     */
    public function allStaticDataOfExepnsesDonutChartAction($_period = null)
    {
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->get('security.context')->getToken()->getUser();
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $result = array("valid" => NULL, "waiting" => NULL, "draft" => NULL);

        $date_period = new \DateTime($_period);
        $date_period = $date_period->modify('first day of this month');

        $e_payPeriod = new PayPeriod();
        $obj_Payperiod = $e_payPeriod->getObjectPayPeriod($date_period, $this->container, $em, $current_user_id);
        if (is_object($obj_Payperiod)) {
             if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {

                $result = array_merge($em->getRepository('ExpenseBundle:hrm_expense_ik')->IkStaticDataOfDonutChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $obj_Payperiod->getId(), FALSE),
                    $em->getRepository('ExpenseBundle:hrm_expense_transport')->TransportStaticDataOfDonutChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $obj_Payperiod->getId(), FALSE),
                    $em->getRepository('ExpenseBundle:hrm_expenses_accomodation')->AccomodationStaticDataOfDonutChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $obj_Payperiod->getId(), FALSE),
                    $em->getRepository('ExpenseBundle:hrm_expenses_dining')->DiningStaticDataOfDonutChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $obj_Payperiod->getId(), FALSE),
                    $em->getRepository('ExpenseBundle:hrm_expenses_other')->OtherStaticDataOfDonutChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $obj_Payperiod->getId(), FALSE),
                    $em->getRepository('requestAbsenceBundle:requestAbsence')->AbsencesStaticDataFindStaticDByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $obj_Payperiod->getId(), FALSE),
                    $em->getRepository('BonusBundle:hrm_bonus')->BonusStaticDataFindStaticDByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $obj_Payperiod->getId(), FALSE),
                    $em->getRepository('PaydayVariationsBundle:PaydayVariations')->PayDayStaticDataFindStaticDByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $obj_Payperiod->getId(), FALSE),
                    $em->getRepository('ExtraHoursBundle:hrm_additional_hours')->ExtraHoursStaticDataFindStaticDByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $obj_Payperiod->getId(), FALSE),
                    $em->getRepository('TrainingBundle:training')->TrainingStaticDataFindStaticDByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $obj_Payperiod->getId(), FALSE),
                    $em->getRepository('TransportBundle:hrm_transport')->PublicTransportStaticDataFindStaticDByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $obj_Payperiod->getId(), FALSE),
                    $em->getRepository('BenefitBundle:hrm_benefit')->BenefitStaticDataFindStaticDByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $obj_Payperiod->getId(), FALSE),
                    $em->getRepository('BenefitBundle:hrm_benefit_car')->BenefitCarStaticDataFindStaticDByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $obj_Payperiod->getId(), FALSE));
            /*   } else if ($securityContext->isGranted('ROLE_ADMIN')) {

                  $iks = $em->getRepository('ExpenseBundle:hrm_expense_ik')->findAll();
                  $transports = $em->getRepository('ExpenseBundle:hrm_expense_transport')->findAll();
                  $accomodation = $em->getRepository('ExpenseBundle:hrm_expenses_accomodation')->findAll();
                  $dining = $em->getRepository('ExpenseBundle:hrm_expenses_dining')->findAll();
                  $other = $em->getRepository('ExpenseBundle:hrm_expenses_other')->findAll();
              } else */
             } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {

                $result = array_merge($em->getRepository('ExpenseBundle:hrm_expense_ik')->IkStaticDataOfDonutChartsFindByUserCompanyId($company_id, $obj_Payperiod->getId()),
                    $em->getRepository('ExpenseBundle:hrm_expense_transport')->TransportStaticDataOfDonutChartsFindByUserCompanyId($company_id, $obj_Payperiod->getId()),
                    $em->getRepository('ExpenseBundle:hrm_expenses_accomodation')->AccomodationStaticDataOfDonutChartsFindByUserCompanyId($company_id, $obj_Payperiod->getId()),
                    $em->getRepository('ExpenseBundle:hrm_expenses_dining')->DiningStaticDataOfDonutChartsFindByUserCompanyId($company_id, $obj_Payperiod->getId()),
                    $em->getRepository('ExpenseBundle:hrm_expenses_other')->OtherStaticDataOfDonutChartsFindByUserCompanyId($company_id, $obj_Payperiod->getId()),
                    $em->getRepository('requestAbsenceBundle:requestAbsence')->AbsencesStaticDataFindStaticDByUserCompanyId($company_id, $obj_Payperiod->getId()),
                    $em->getRepository('BonusBundle:hrm_bonus')->BonusStaticDataFindStaticDByUserCompanyId($company_id, $obj_Payperiod->getId()),
                    $em->getRepository('PaydayVariationsBundle:PaydayVariations')->PayDayStaticDataFindStaticDByUserCompanyId($company_id, $obj_Payperiod->getId()),
                    $em->getRepository('ExtraHoursBundle:hrm_additional_hours')->ExtraHoursStaticDataFindStaticDByUserCompanyId($company_id, $obj_Payperiod->getId()),
                    $em->getRepository('TrainingBundle:training')->TrainingStaticDataFindStaticDByUserCompanyId($company_id, $obj_Payperiod->getId()),
                    $em->getRepository('TransportBundle:hrm_transport')->PublicTransportStaticDataFindStaticDByUserCompanyId($company_id, $obj_Payperiod->getId()),
                    $em->getRepository('BenefitBundle:hrm_benefit')->BenefitStaticDataFindStaticDByUserCompanyId($company_id, $obj_Payperiod->getId()),
                    $em->getRepository('BenefitBundle:hrm_benefit_car')->BenefitCarStaticDataFindStaticDByUserCompanyId($company_id, $obj_Payperiod->getId()));

            }
                $final = array();
                array_walk_recursive($result, function ($item, $key) use (&$final) {
                    $final[$key] = isset($final[$key]) ? $item + $final[$key] : $item;
                });
                $result = $final;            
        }
        return array('result' => $result /*, 'users' => $users*/);
    }

    /**
     * Lists all data charts of exepnese entities.
     * _period : years
     *  return json object
     */
    public function allStaticDataOfExepnsesBarChartAction($_year = null)
    {
        $em = $this->getDoctrine()->getManager();
        $preference = ((is_null($this->container->get('settingsbundle.preference.service')->getEmpData()->getPreference()))? 'fr': $this->container->get('settingsbundle.preference.service')->getEmpData()->getPreference()->getFormat());
        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->get('security.context')->getToken()->getUser();
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $all_months_year = array();
        if ($_year == null) {
            $_year = new \DateTime();
            $_year = $_year->format('Y');
        }
        for ($iM = 1; $iM <= 12; $iM++) {
            if ($iM < 10) $iM = '0' . $iM;
            $all_months_year[] = array('month' => $_year . '-' . $iM, "valid" => 0, "waiting" => 0);
        }
         if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {

            $all_months_year = $this->merger_results($all_months_year, $em->getRepository('ExpenseBundle:hrm_expense_ik')->IkStaticDataOfBarChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $_year, FALSE));
            $all_months_year = $this->merger_results($all_months_year, $em->getRepository('ExpenseBundle:hrm_expense_transport')->TransportStaticDataOfBarChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $_year, FALSE));
            $all_months_year = $this->merger_results($all_months_year, $em->getRepository('ExpenseBundle:hrm_expenses_accomodation')->AccomodationStaticDataOfBarChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $_year, FALSE));
            $all_months_year = $this->merger_results($all_months_year, $em->getRepository('ExpenseBundle:hrm_expenses_dining')->DiningStaticDataOfBarChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $_year, FALSE));
            $all_months_year = $this->merger_results($all_months_year, $em->getRepository('ExpenseBundle:hrm_expenses_other')->OtherStaticDataOfBarChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $_year, FALSE));
        /*   } else if ($securityContext->isGranted('ROLE_ADMIN')) {

              $iks = $em->getRepository('ExpenseBundle:hrm_expense_ik')->findAll();
              $transports = $em->getRepository('ExpenseBundle:hrm_expense_transport')->findAll();
              $accomodation = $em->getRepository('ExpenseBundle:hrm_expenses_accomodation')->findAll();
              $dining = $em->getRepository('ExpenseBundle:hrm_expenses_dining')->findAll();
              $other = $em->getRepository('ExpenseBundle:hrm_expenses_other')->findAll();
          } else */


        }else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {

            $all_months_year = $this->merger_results($all_months_year, $em->getRepository('ExpenseBundle:hrm_expense_ik')->IkStaticDataOfBarChartsFindByUserCompanyId($company_id, $_year));
            $all_months_year = $this->merger_results($all_months_year, $em->getRepository('ExpenseBundle:hrm_expense_transport')->TransportStaticDataOfBarChartsFindByUserCompanyId($company_id, $_year));
            $all_months_year = $this->merger_results($all_months_year, $em->getRepository('ExpenseBundle:hrm_expenses_accomodation')->AccomodationStaticDataOfBarChartsFindByUserCompanyId($company_id, $_year));
            $all_months_year = $this->merger_results($all_months_year, $em->getRepository('ExpenseBundle:hrm_expenses_dining')->DiningStaticDataOfBarChartsFindByUserCompanyId($company_id, $_year));
            $all_months_year = $this->merger_results($all_months_year, $em->getRepository('ExpenseBundle:hrm_expenses_other')->OtherStaticDataOfBarChartsFindByUserCompanyId($company_id, $_year));
        }
        return array('result' => $all_months_year, 'format' => $preference);
    }


    /**** find expense by period for data GRID ***/
    public function allStaticDataOfExepnsesGridChartAction($_period = null)
    {
        $em = $this->getDoctrine()->getManager();
        $ik = new hrm_expense_ik();

        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getId();
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $date_period = new \DateTime($_period);
        $date_period = $date_period->modify('first day of this month');
        $e_payPeriod = new PayPeriod();
        $obj_Payperiod = $e_payPeriod->getObjectPayPeriod($date_period, $this->container, $em, $current_user_id);


        if ($securityContext->isGranted('ROLE_RESPONSABLE')) {

            // $infos_user = $em->getRepository('UserBundle:HrmEmployee')->find($current_user_id);
            // $company_id = $infos_user->getEmployee()->first()->getCompany()->getId();
            if (is_object($obj_Payperiod)) {
                $iks = $em->getRepository('ExpenseBundle:hrm_expense_ik')->IkStaticDataOfGridChartsFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
                $transport = $em->getRepository('ExpenseBundle:hrm_expense_transport')->TransportStaticDataOfGridChartsFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
                $accomodation = $em->getRepository('ExpenseBundle:hrm_expenses_accomodation')->AccomodationStaticDataOfGridChartsFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
                $dining = $em->getRepository('ExpenseBundle:hrm_expenses_dining')->DiningStaticDataOfGridChartsFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
                $other = $em->getRepository('ExpenseBundle:hrm_expenses_other')->OtherStaticDataOfGridChartsFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
                $requestabsences = $em->getRepository('requestAbsenceBundle:requestAbsence')->AbsencesStaticDataOfGridFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
                $bonus = $em->getRepository('BonusBundle:hrm_bonus')->BonusStaticDataOfGridChartsFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
                $payDya = $em->getRepository('PaydayVariationsBundle:PaydayVariations')->PayDayStaticDataOfGridChartsFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
                $extraHours = $em->getRepository('ExtraHoursBundle:hrm_additional_hours')->ExtraHoursStaticDataOfGridChartsFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
                $training = $em->getRepository('TrainingBundle:training')->TrainingStaticDataOfGridChartsFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
                $public_transport = $em->getRepository('TransportBundle:hrm_transport')->PublicTransportStaticDataOfGridChartsFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
                $benefit = $em->getRepository('BenefitBundle:hrm_benefit')->BenefitStaticDataOfGridChartsFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
                $benefit_car = $em->getRepository('BenefitBundle:hrm_benefit_car')->BenefitCarStaticDataOfGridChartsFindByUserCompanyId($company_id, $current_user_id, $obj_Payperiod);
            } else {
                $iks = array();
                $transport = array();
                $accomodation = array();
                $dining = array();
                $other = array();
                $requestabsences =  array();
                $bonus =  array();
                $payDya =  array();
                $extraHours =  array();
                $training =  array();
                $public_transport =  array();     
                $benefit =  array();
                $benefit_car =  array();                
            }
        } else if($securityContext->isGranted('ROLE_COLLABORATEUR')){
            if (is_object($obj_Payperiod)) {
                $iks = $em->getRepository('ExpenseBundle:hrm_expense_ik')->IkStaticDataOfGridChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);
                $transport = $em->getRepository('ExpenseBundle:hrm_expense_transport')->TransportStaticDataOfGridChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);
                $accomodation = $em->getRepository('ExpenseBundle:hrm_expenses_accomodation')->AccomodationStaticDataOfGridChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);
                $dining = $em->getRepository('ExpenseBundle:hrm_expenses_dining')->DiningStaticDataOfGridChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);
                $other = $em->getRepository('ExpenseBundle:hrm_expenses_other')->OtherStaticDataOfGridChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);
                $requestabsences = $em->getRepository('requestAbsenceBundle:requestAbsence')->AbsencesStaticDataOfGridFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);
                $bonus = $em->getRepository('BonusBundle:hrm_bonus')->BonusStaticDataOfGridChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);
                $payDya = $em->getRepository('PaydayVariationsBundle:PaydayVariations')->PayDayStaticDataOfGridChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);
                $extraHours = $em->getRepository('ExtraHoursBundle:hrm_additional_hours')->ExtraHoursStaticDataOfGridChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);
                $training = $em->getRepository('TrainingBundle:training')->TrainingStaticDataOfGridChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);
                $public_transport = $em->getRepository('TransportBundle:hrm_transport')->PublicTransportStaticDataOfGridChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);                
                $benefit = $em->getRepository('BenefitBundle:hrm_benefit')->BenefitStaticDataOfGridChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);
                $benefit_car = $em->getRepository('BenefitBundle:hrm_benefit_car')->BenefitCarStaticDataOfGridChartsFindByUserCompanyId($this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $current_user_id, $obj_Payperiod, FALSE);                
            } else {
                $iks = array();
                $transport = array();
                $accomodation = array();
                $dining = array();
                $other = array();
                $requestabsences =  array();
                $bonus =  array();
                $payDya =  array();
                $extraHours =  array();
                $training =  array();
                $public_transport =  array();
                $benefit =  array();
                $benefit_car =  array();                 
            }
        }

        $result = $this->getExpenseDatalistFormat(array_merge($iks, $transport, $accomodation, $dining, $other));
        return array('result' => array_merge($result, $requestabsences, $bonus, $payDya, $extraHours, $training, $public_transport, $benefit, $benefit_car));
    }

    private function merger_results($all_months_year, $_array_pushed)
    {
        foreach ($all_months_year as $key => $val) {
            $_key = array_search($val['month'], array_column($_array_pushed, 'month'));

            if ($_key !== false) {
                $all_months_year[$key]['valid'] += (float)$_array_pushed[$_key]['valid'];
                $all_months_year[$key]['waiting'] += (float)$_array_pushed[$_key]['waiting'];
            }
        }
        return $all_months_year;
    }

    private function getExpenseDatalistFormat($arr)
    {
        foreach ($arr as $k1 => $v1) {
            foreach ($arr as $k2 => $v2) {
                if ($k1 === $k2) continue;
                if (!isset($arr[$k1]) || !isset($arr[$k2])) continue;
                if ($v1['user_id'] === $v2['user_id']) {
                    $arr[$k1]['nbr_request'] += $v2['nbr_request'];
                    $arr[$k1]['total'] += $v2['total'];
                    unset($arr[$k2]);
                }
            }
        }
        return $arr;
    }

}
