<?php

namespace ExpenseBundle\Controller;

use ExpenseBundle\Entity\hrm_expense_transport;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FileBundle\Entity\attachments;
use ExpenseBundle\Model\GenerateTaxModelInputs;
/**
 * Transport controller.
 *
 */
class hrm_expense_transportController extends Controller {

      public function statusInsertion($status = null, $entity) {
        $date = null;
        if ($status == "2"||$status == "3") {
            $LastUpdateDate = new \DateTime("now");
            $date = $LastUpdateDate->getTimestamp();
        } else {
            $entity->setSettingsPayPeriod(null);
        }
        return $date;
    }

  public function openORcloseAction($status = null, $entity, $entity2, $em) {
        $date = $this->statusInsertion($status, $entity);
        if (!empty($entity2)) {
            foreach ($entity2 as $key => $value) {
                $IdPayPeriod[$key] = $value->getId();
                $start[$key] = $value->getStartDate();
                $end[$key] = $value->getEndDate();
                $dateStart = $start[$key]->getTimestamp();
                $dateEnd = $end[$key]->getTimestamp();
                if ($dateStart <= $date && $dateEnd >= $date) {
                    $entity->setSettingsPayPeriod($em->getRepository('PayPeriodBundle:SettingsPayPeriod')->find($IdPayPeriod[$key]));
                }
            }
        }
    }
    
       /**
     * 
     * @param type $em
     * @param type $entity
     * @param type $payPeriodDate celui envoyé depuis from 
     * @return boolean/object
     */
    private function setOpenedSettingsPayPeriod($em, $entity, $payPeriodDate = null)
    {
        $ret = '';
         // affect first pay period
        
        if($payPeriodDate == null)
        {
            $searchBy = array("companyId"=>$this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId(),
                                       "status"=>"open");
        }
        else {
            $searchBy = array("companyId"=>$this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId());
        }
        
        $payperiodlist = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')
                        ->findBy($searchBy, array('startDate' => 'ASC'));
        if($payperiodlist)
        {   
           
                if($payPeriodDate == null)
                {
                    $entity->setSettingsPayPeriod($payperiodlist[0]);  
                    $ret = true;
                 
                } else {
                     foreach ($payperiodlist as $key => $value) {
                        if($value->getStartDate()->format('m/Y') == $payPeriodDate && ($value->getStatus() === 'open') )
                           {
                               $entity->setSettingsPayPeriod($value);  
                               $ret = true;
                               break;
                           }
                           if(($value->getStartDate()->format('m/Y') === $payPeriodDate ) && ($value->getStatus() === 'close') )
                           {
                               $entity->setSettingsPayPeriod($value); 
                               $ret = false;
                               break;
                           }   
                    } 
            }
            
            if($ret === true)
                 return true;
            if($ret === false)
                 return new \Symfony\Component\BrowserKit\Response('PAY_PERIOD', 'ERR_CLOSED_PAY_PERIOD');
            
            return new \Symfony\Component\BrowserKit\Response('PAY_PERIOD', 'ERR_NOT_PAY_PERIOD');
            
        }
        else
        {
            return new \Symfony\Component\BrowserKit\Response('PAY_PERIOD', 'ERR_EMPTY_PAY_PERIOD');;
        }
        
    }

    
    public function ConfirmAction(Request $request, hrm_expense_transport $transport) {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $transport->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['confirm']));
        
        $entity = $em->getRepository('ExpenseBundle:hrm_expense_transport')->find($transport);
        
        
       /* $SettingsPayPeriod = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')->findAll();
        $this->openORcloseAction($data['confirm'], $transport, $SettingsPayPeriod, $em);*/

//        //Mail notifier to Collaborateur
//        if ($transport->getCreateUid() != NULL) {
//            if ($transport->getCreateUid() == $transport->getContributor()->getUser()->getId()) {
//                $TranslateMail = $this->get('mailingbundle.email.send.service');
//            //   $this->get('mailingbundle.email.send.service')->SendMail($entity, $TranslateMail->TagsTranslateExpense('confirmMsg'), $TranslateMail->TagsTranslateExpense('confirmEtat'), $transport->getAmountTran());
//            }
//        }
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($entity, 'RESPONSABLE', $TranslateMail->TagsTranslateExpense('confirmMsg'), $TranslateMail->TagsTranslateExpense('confirmEtat'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));

        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $entity);
         if($ret_payperiod !== true)
        {
            return $ret_payperiod;
        }
        
        $em->persist($transport);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function CancelAction(Request $request, hrm_expense_transport $transport) {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $transport->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['cancel']));

        $entity = $em->getRepository('ExpenseBundle:hrm_expense_transport')->find($transport);
        
        
        /*$SettingsPayPeriod = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')->findAll();
        $this->openORcloseAction($data['cancel'], $transport, $SettingsPayPeriod, $em);*/
        
        //Mail notifier to Collaborateur
//        $TranslateMail = $this->get('mailingbundle.email.send.service');
        //$this->get('mailingbundle.email.send.service')->SendMail($entity, $TranslateMail->TagsTranslateExpense('cancelMsg'), $TranslateMail->TagsTranslateExpense('cancelEtat'), $transport->getAmountTran());
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($entity, 'RESPONSABLE', $TranslateMail->TagsTranslateExpense('cancelMsg'), $TranslateMail->TagsTranslateExpense('cancelEtat'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));

        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $transport);
         if($ret_payperiod !== true)
        {
            return $ret_payperiod;
        }
        
        $em->persist($transport);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function addAction(Request $request) {
        $transport = new hrm_expense_transport();
        $form = $this->createForm('ExpenseBundle\Form\hrm_expense_transportType', $transport);
        $form->handleRequest($request);

        $body = $request->getContent();
        $data = json_decode($body, true);

        $em = $this->getDoctrine()->getManager();

        if ($request->isMethod('POST')) {
            // will be get error if not deleted settings_pay_period from FormType before submit
            $form->remove('settings_pay_period');
            
            $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
            $form->submit($data);

            //set default status if ROLE_COLLABORATEUR else set with the submited value 
            $securityContext = $this->container->get('security.authorization_checker');

            //Mail notifier to Responsable
            if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                
        
                $transport->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            } else if ($securityContext->isGranted('ROLE_ADMIN')) {
                $transport->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $transport->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            }

            if ($request->request->has('attachments')) {
//                if (is_string($request->request->get('attachments'))) {
//                    $attachment = new attachments();
//                    $attachment->setPath($request->request->get('attachments'));
//                    $em->persist($attachment);
//                    $em->flush($attachment);
//                    $transport->setAttachments($attachment);
//                }
                if ($data['attachments'] != null) {
//                    if ($request->request->has('attachments')) {
                        if ($transport->getAttachments() != null) {
                            $attachment = $transport->getAttachments();
                        } else
                            $attachment = new attachments();
                        $attachment->setPath($data['attachments']['path']);
                               if ((isset($data['attachments']['mimetype'])) && ($data['attachments']['mimetype'] != null))
                    $attachment->setMimetype($data['attachments']['mimetype']);
                        $em->persist($attachment);
                        $em->flush($attachment);
                        $transport->setAttachments($attachment);
//                    }
                }
                else {

                    $transport->setAttachments(null);
                }                
            }


            //Add values for all forgein Keys 

            $transport->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $transport->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']));
            $transport->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));
//            $transport->setSettingsProjects($em->getRepository('AdminBundle:SettingsProjects')->find($data['project']));
            $transport->setSettingsExpTransport($em->getRepository('AdminBundle:hrm_settings_exp_transport')->find($data['nature']));
            $transport->setUser($em->getRepository('UserBundle:HrmEmployee')->find($this->container->get('settingsbundle.preference.service')->getEmpData()->getId()));
            //Add default value on create Action
            $transport->setCreateDate(new \DateTime('now'));
            $transport->setCreateUid($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
            $transport->setLastUpdateUid($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
            $transport->setLastUpdate(new \DateTime('now'));
//            $transport->setPayPeriod(new \DateTime($data['settings_pay_period']));

//            $transport->setCompany(1);
//            $transport->setProjet($em->getRepository('ExpenseBundle:projet')->find($data['projet']));

             //send mail to responsable

            $TranslateMail = $this->get('mailingbundle.email.send.service');
            if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $this->get('mailingbundle.email.send.service')->Notify($transport, 'RESPONSABLE', $TranslateMail->TagsTranslateExpense('titleNew'), $TranslateMail->TagsTranslateExpense('etatNew'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));
            } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $this->get('mailingbundle.email.send.service')->Notify($transport, 'COLLABORATEUR', $TranslateMail->TagsTranslateExpense('titleNew'), $TranslateMail->TagsTranslateExpense('etatNew'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));
            }
            // try to add first open one of pay period
           $ret_payperiod =  $this->setOpenedSettingsPayPeriod($em, $transport, $data['settings_pay_period']);

            if($ret_payperiod !== true)
            {
                return $ret_payperiod;
            }
            
            $em->persist($transport);
            $em->flush();
            
            // save new model tax data of expense transport
            if(isset($data['tax_list']))
            {
                 $_generateTaxModelInputs = new GenerateTaxModelInputs($this->container);
                $_generateTaxModelInputs->saveExpensesTaxes($em, $transport, $data, 'transport', $this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
            }
           

            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 201);
            return $response;
        }
    }

    /**
     * Displays a form to update an existing transport entity.
     *
     */
    public function updateAction(Request $request, hrm_expense_transport $transport) {
        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $entity = $em->getRepository('ExpenseBundle:hrm_expense_transport')->find($data['uid']);
        $update_form = $this->createForm('ExpenseBundle\Form\hrm_expense_transportType', $entity);

        if ($request->isMethod('PUT')) {
            // will be get error if not deleted settings_pay_period from FormType before submit
            $update_form->remove('settings_pay_period');
            
            $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
            $update_form->submit($data);
            $infos_user = $em->getRepository('UserBundle:hrm_user')->find($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
//            $collab_id = $infos_user->getId();
            $collab_id = $entity->getCreateUid();
            $entity->setUser($em->getRepository('UserBundle:HrmEmployee')->find($collab_id));
            $securityContext = $this->container->get('security.authorization_checker');
            if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $entity->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
                if ($data['status'] == 1) {
                    $this->SendMailToResponsable('Nouvelle demande');
                }
            } else if ($securityContext->isGranted('ROLE_ADMIN')) {
                $entity->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $entity->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            }

            if ($data['attachments'] != null) {
                if ($data['attachments']['path']) {
                    if ($entity->getAttachments() != null) {
                        $attachment = $entity->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if (isset($data['attachments']['mimetype']))
                $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $entity->setAttachments($attachment);
                }
            }
            else {

                $entity->setAttachments(null);
            }
            
           
            

            $entity->setLastUpdate(new \DateTime('now'));
            $entity->setLastUpdateUid($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());

            $entity->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $entity->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $entity->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));
//            $entity->setSettingsProjects($em->getRepository('AdminBundle:SettingsProjects')->find($data['project']));
            $entity->setSettingsExpTransport($em->getRepository('AdminBundle:hrm_settings_exp_transport')->find($data['nature']));
            
                        // try to add first open one of pay period
           $ret_payperiod =  $this->setOpenedSettingsPayPeriod($em, $entity, $data['settings_pay_period']);

            if($ret_payperiod !== true)
            {
                return $ret_payperiod;
            }
            
            // update/save  model tax data of expense transport
            if(isset($data['tax_list']))
            {
                $_generateTaxModelInputs = new GenerateTaxModelInputs($this->container);
                $_generateTaxModelInputs->saveExpensesTaxes($em, $entity, $data['tax_list'], 'transport', $this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), 'update');
            }
            
            $em->persist($entity);
            $em->flush();

            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function allAction() {
        $em = $this->getDoctrine()->getManager();
        $transport = new hrm_expense_transport();
        $securityContext = $this->container->get('security.authorization_checker');
        $currentuser = $this->get('security.context')->getToken()->getUser();
        $entity = $em->getRepository('ExpenseBundle:hrm_expense_transport')->findOneByUser($currentuser);

//        if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
//            $transports = $em->getRepository('ExpenseBundle:transport')->findByUser($currentuser);
//        } else if ($securityContext->isGranted('ROLE_ADMIN')) {
//            $transports = $em->getRepository('ExpenseBundle:transport')->findAll();
//        } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
//            $CompanyId = $entity->getCompany();
//            $transports = $em->getRepository('ExpenseBundle:transport')->ResponsableValidationInfos($CompanyId, $currentuser);
//        } else if ($securityContext->isGranted('ROLE_EXPERT_COMPTABLE')) {
//            $transports = $em->getRepository('ExpenseBundle:transport')->findAll();
//        }
        $array = array();
        foreach ($transports as $transport) {
            $array [] = array(
                "id" => $transport->getId(),
                "name" => $transport->getName(),
                "date" => $transport->getDate()->format('d/m/Y'),
                "refund" => $transport->getRefund()->format('d/m/Y'),
                "amount" => $transport->getAmount(),
                "type" => $transport->getType(),
//                "projetId" => $transport->getProjet()->getId(),
//                "projetName" => $transport->getProjet()->getName(),
//                "statusId" => $transport->getStatus()->getId(),
//                "statusName" => $transport->getStatus()->getName()
            );
        }
        return new JsonResponse(array(
            'transports' => $array,
        ));
    }

    public function removeAction(Request $request, hrm_expense_transport $transport) {
        $form = $this->createDeleteForm($transport);
        $form->handleRequest($request);

        if ($request->isMethod('DELETE')) {
            $em = $this->getDoctrine()->getManager();
            
            // update/save  model tax data of expense transport
            $_generateTaxModelInputs = new GenerateTaxModelInputs($this->container);            
            $_generateTaxModelInputs->DeleteAllExpensesTaxes($em, 'transport', $transport);
            
            $em->remove($transport);
            $em->flush($transport);
            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }

    /**
     * Lists all transport entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $transports = $em->getRepository('ExpenseBundle:hrm_expense_transport')->findAll();

        return $this->render('hrm_expense/hrm_expense_transport/index.html.twig', array(
                    'transports' => $transports,
        ));
    }

    /**
     * Creates a new transport entity.
     *
     */
    public function newAction(Request $request) {
        $transport = new hrm_expense_transport();
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();

        $infos_user = $em->getRepository('UserBundle:hrm_user')->find($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());

        $provider = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $form = $this->createForm('ExpenseBundle\Form\hrm_expense_transportType', $transport, array(
            'user_id' => $company_id,'provider'=> $provider,
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($transport);
            $em->flush($transport);

            return $this->redirectToRoute('transport_show', array('id' => $transport->getId()));
        }

        return $this->render('hrm_expense/hrm_expense_transport/new.html.twig', array(
                    'transport' => $transport,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a transport entity.
     *
     */
    public function showAction(hrm_expense_transport $transport) {
        $deleteForm = $this->createDeleteForm($transport);

        return $this->render('hrm_expense/hrm_expense_transport/show.html.twig', array(
                    'transport' => $transport,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    public function modaleditAction(Request $request) {
        $transport = new hrm_expense_transport();
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
        $infos_user = $em->getRepository('UserBundle:hrm_user')->find($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
        $provider = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $form = $this->createForm('ExpenseBundle\Form\hrm_expense_transportType', $transport, array(
            'user_id' => $company_id,'provider'=>$provider
        ));
        $form->handleRequest($request);
        return $this->render('hrm_expense/hrm_expense_transport/modal_edit.html.twig', array(
                    'transport' => $transport,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing transport entity.
     *
     */
    public function editAction(Request $request, hrm_expense_transport $transport) {
        $deleteForm = $this->createDeleteForm($transport);
        $editForm = $this->createForm('ExpenseBundle\Form\hrm_expense_transportType', $transport);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('transport_edit', array('id' => $transport->getId()));
        }

        return $this->render('hrm_expense/hrm_expense_transport/edit.html.twig', array(
                    'transport' => $transport,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a transport entity.
     *
     */
    public function deleteAction(Request $request, hrm_expense_transport $transport) {
        $form = $this->createDeleteForm($transport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($transport);
            $em->flush($transport);
        }

        return $this->redirectToRoute('transport_index');
    }

    /**
     * Creates a form to delete a transport entity.
     *
     * @param transport $transport The transport entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_expense_transport $transport) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('transport_delete', array('id' => $transport->getUid())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    // try to generate input form of tax model
    public function generateModelTaxAction($type, $nature, $expense, $action)
    {
        $em = $this->getDoctrine()->getManager();

         $_generateTaxModelInputs = new GenerateTaxModelInputs($this->container);
         $ret = $_generateTaxModelInputs->createFormInputs($em, $type, $nature, $expense, $action);
         return array("data"=>$ret['data'], "scope_list"=>$ret['scope']);
    }
}
