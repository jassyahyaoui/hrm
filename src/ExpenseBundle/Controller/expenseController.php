<?php

namespace ExpenseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Expense controller.
 *
 */
class expenseController extends Controller {

    public function DefaultValidatorAction($id) {
        $em = $this->getDoctrine()->getManager();
        $infos_user = $em->getRepository('UserBundle:HrmEmployee')->find($id);
        $company_id = $infos_user->getCompany()->getId();
        $validators = $em->getRepository('ExpenseBundle:hrm_expenses')->ValidatorUser($company_id, $id);
        $validator = $validators[0];

        return $validators;
    }

    public function hrm_expense_indexAction() {
        return 'ok';
    }

    public function hrm_expense_layoutAction() {

        return $this->render('hrm_expense/hrm_expense_layout.html.twig');
    }

    public function hrm_new_expenseAction() {
        return $this->render('hrm_expense/hrm_new_expense.html.twig');
    }

    public function singlePageLayoutAction() {
        return $this->render('hrm_expense/single_page_layout.html.twig');
    }
}
