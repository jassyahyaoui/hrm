<?php

namespace ExpenseBundle\Controller;

use ExpenseBundle\Entity\hrm_expenses_dining;
use ExpenseBundle\Entity\hrm_expenses_guests;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\ArrayCollection;
use FileBundle\Entity\attachments;
use ExpenseBundle\Model\GenerateTaxModelInputs;

/**
 * Hrm_expenses_dining controller.
 *
 */
class hrm_expenses_diningController extends Controller {

      public function statusInsertion($status = null, $entity) {
        $date = null;
        if ($status == "2"||$status == "3") {
            $LastUpdateDate = new \DateTime("now");
            $date = $LastUpdateDate->getTimestamp();
        } else {
            $entity->setSettingsPayPeriod(null);
        }
        return $date;
    }

  /*  public function openORcloseAction($status = null, $entity, $entity2, $em) {
        $date = $this->statusInsertion($status, $entity);
        if (!empty($entity2)) {
            foreach ($entity2 as $key => $value) {
                $IdPayPeriod[$key] = $value->getId();
                $start[$key] = $value->getStartDate();
                $end[$key] = $value->getEndDate();
                $dateStart = $start[$key]->getTimestamp();
                $dateEnd = $end[$key]->getTimestamp();
                if ($dateStart <= $date && $dateEnd >= $date) {
                    $entity->setSettingsPayPeriod($em->getRepository('PayPeriodBundle:SettingsPayPeriod')->find($IdPayPeriod[$key]));
                }
            }
        }
    }*/
    
    /**
     * 
     * @param type $em
     * @param type $entity
     * @param type $payPeriodDate celui envoyé depuis from 
     * @return boolean/object
     */
    private function setOpenedSettingsPayPeriod($em, $entity, $payPeriodDate = null)
    {
        $ret = '';
         // affect first pay period
        
        if($payPeriodDate == null)
        {
            $searchBy = array("companyId"=>$this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany(),
                                       "status"=>"open");
        }
        else {
            $searchBy = array("companyId"=>$this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany());
        }
        
        $payperiodlist = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')
                        ->findBy($searchBy, array('startDate' => 'ASC'));
        if($payperiodlist)
        {   
           
                if($payPeriodDate == null)
                {
                    $entity->setSettingsPayPeriod($payperiodlist[0]);  
                    $ret = true;
                 
                } else {
                     foreach ($payperiodlist as $key => $value) {
                        if($value->getStartDate()->format('m/Y') == $payPeriodDate && ($value->getStatus() === 'open') )
                           {
                               $entity->setSettingsPayPeriod($value);  
                               $ret = true;
                               break;
                           }
                           if(($value->getStartDate()->format('m/Y') === $payPeriodDate ) && ($value->getStatus() === 'close') )
                           {
                               $entity->setSettingsPayPeriod($value); 
                               $ret = false;
                               break;
                           }   
                    } 
            }
            
            if($ret === true)
                 return true;
            if($ret === false)
                 return new \Symfony\Component\BrowserKit\Response('PAY_PERIOD', 'ERR_CLOSED_PAY_PERIOD');
            
            return new \Symfony\Component\BrowserKit\Response('PAY_PERIOD', 'ERR_NOT_PAY_PERIOD');
            
        }
        else
        {
            return new \Symfony\Component\BrowserKit\Response('PAY_PERIOD', 'ERR_EMPTY_PAY_PERIOD');;
        }
        
    }    

    
    
    public function ConfirmAction(Request $request, hrm_expenses_dining $dining) {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $dining->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['confirm']));
        
        $entity = $em->getRepository('ExpenseBundle:hrm_expenses_dining')->find($dining);
        
        
        /*$SettingsPayPeriod = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')->findAll();
        $this->openORcloseAction($data['confirm'], $dining, $SettingsPayPeriod, $em);*/
//            if ($dining->getCreateUid() != NULL) {
//            if ($dining->getCreateUid() == $dining->getContributor()->getId()) {
//                $TranslateMail = $this->get('mailingbundle.email.send.service');
//               // $this->get('mailingbundle.email.send.service')->SendMail($entity, $TranslateMail->TagsTranslateExpense('confirmMsg'), $TranslateMail->TagsTranslateExpense('confirmEtat'), $dining->getAmountDin());
//
//            }}

        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($entity, 'RESPONSABLE', $TranslateMail->TagsTranslateExpense('confirmMsg'), $TranslateMail->TagsTranslateExpense('confirmEtat'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));


        // try to add first open one of pay period
       $ret_payperiod =  $this->setOpenedSettingsPayPeriod($em, $dining);

        if($ret_payperiod !== true)
        {
            return $ret_payperiod;
        } 
        
        $em->persist($dining);
        $em->flush();
        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function CancelAction(Request $request, hrm_expenses_dining $dining) {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $dining->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['cancel']));
        
        $entity = $em->getRepository('ExpenseBundle:hrm_expenses_dining')->find($dining);
        
        
        /*$SettingsPayPeriod = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')->findAll();
        $this->openORcloseAction($data['cancel'], $dining, $SettingsPayPeriod, $em);*/
        //Mail notifier to Collaborateur
//        $TranslateMail = $this->get('mailingbundle.email.send.service');
        //$this->get('mailingbundle.email.send.service')->SendMail($entity, $TranslateMail->TagsTranslateExpense('cancelMsg'), $TranslateMail->TagsTranslateExpense('cancelEtat'), $dining->getAmountDin());
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($entity, 'RESPONSABLE', $TranslateMail->TagsTranslateExpense('cancelMsg'), $TranslateMail->TagsTranslateExpense('cancelEtat'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));

        // try to add first open one of pay period
       $ret_payperiod =  $this->setOpenedSettingsPayPeriod($em, $dining);

        if($ret_payperiod !== true)
        {
            return $ret_payperiod;
        }        
        $em->persist($dining);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function addAction(Request $request) {
        $dining = new hrm_expenses_dining();
        $guests = new hrm_expenses_guests();

        $form = $this->createForm('ExpenseBundle\Form\hrm_expenses_diningType', $dining);
        $form->handleRequest($request);

        $body = $request->getContent();
        $data = json_decode($body, true);

        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
            // will be get error if not deleted settings_pay_period from FormType before submit
            $form->remove('settings_pay_period');
            
            $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
            $form->submit($data);
//            $form2->submit($data);
            //set default status if ROLE_COLLABORATEUR else set with the submited value 
            $securityContext = $this->container->get('security.authorization_checker');
            if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $dining->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            } else if ($securityContext->isGranted('ROLE_ADMIN')) {
                $dining->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $dining->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            }

            //Add values for all forgein Keys 
//            $dining->setSettingsProjects($em->getRepository('AdminBundle:SettingsProjects')->find($data['project']));
            $dining->setUser($em->getRepository('UserBundle:HrmEmployee')->find($this->container->get('settingsbundle.preference.service')->getEmpData()->getId()));
            if ($request->request->has('attachments')) {
                if (is_string($request->request->get('attachments'))) {
                    $attachment = new attachments();
                    $attachment->setPath($request->request->get('attachments'));
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $dining->setAttachments($attachment);
                }
            }


            //Add default value on create Action

            $dining->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $dining->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']));
            $dining->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));

            $dining->setSettingsExpDining($em->getRepository('AdminBundle:hrm_settings_exp_dinning')->find($data['settingsExpDining']));
            $dining->setCreateDate(new \DateTime('now'));
            $dining->setCreateUid($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
            $dining->setLastUpdateUid($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
            $dining->setLastUpdate(new \DateTime('now'));

    //send mail to responsable

            $TranslateMail = $this->get('mailingbundle.email.send.service');
            if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $this->get('mailingbundle.email.send.service')->Notify($dining, 'RESPONSABLE', $TranslateMail->TagsTranslateExpense('titleNew'), $TranslateMail->TagsTranslateExpense('etatNew'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));
            } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $this->get('mailingbundle.email.send.service')->Notify($dining, 'COLLABORATEUR', $TranslateMail->TagsTranslateExpense('titleNew'), $TranslateMail->TagsTranslateExpense('etatNew'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));
            }
            // try to add first open one of pay period
           $ret_payperiod =  $this->setOpenedSettingsPayPeriod($em, $dining, $data['settings_pay_period']);

            if($ret_payperiod !== true)
            {
                return $ret_payperiod;
            }
            
            $em->persist($dining);
            $em->flush();

            // save new model tax data of expense transport
            if(isset($data['tax_list']))
            {
                $_generateTaxModelInputs = new GenerateTaxModelInputs($this->container);
                $_generateTaxModelInputs->saveExpensesTaxes($em, $dining, $data, 'dining', $this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
            }
            
            $lastId = $dining->getUid();

            foreach ($data['ArrayfirstName'] as $key => $field) {
                $field2 = $data['ArraylastName'][$key];
                $guests->AddGuest($lastId, $field, $field2, $this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $em);
            }
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    /**
     * Displays a form to update an existing guests entity.
     *
     */
    public function updateAction(Request $request, hrm_expenses_dining $dining) {
        $em = $this->getDoctrine()->getManager();


        $body = $request->getContent();
        $data = json_decode($body, true);
        $entity = $em->getRepository('ExpenseBundle:hrm_expenses_dining')->find($data['uid']);
        $originalGuests = new ArrayCollection();
        $existUid = true;
        $keyUid = null;
        $item = null;
        foreach ($entity->getHrmExpensesGuests()->toArray() as $guest) {
            if (count($data['hrm_expenses_guests']) > 0) {
                foreach ($data['hrm_expenses_guests'] as $key => $val) {
                    if ($guest->getUid() != $val['uid']) {
                        $existUid = false;
                        $keyUid = $key;
                    } else {
                        $existUid = true;
                        $item = $val;
                        break;
                    }
                }
            } else {
                $em->remove($guest);
                $em->flush();
            }

            if ($existUid === false) {
                $em->remove($guest);
                $em->flush();
                $existUid = true;
                $keyUid = null;
            } else {
                $guest->setFirstName($item['firstName']);
                $guest->setLastName($item['lastName']);
                $guest->setLastUpdate(new \DateTime());
                $originalGuests->add($guest);
            }
        }
        $update_form = $this->createForm('ExpenseBundle\Form\hrm_expenses_diningType', $entity);
        $update_form->handleRequest($request);
        if ($request->isMethod('PUT')) {
            // will be get error if not deleted settings_pay_period from FormType before submit
            $update_form->remove('settings_pay_period');
            
            $update_form->remove('hrmExpensesGuests');
            foreach ($originalGuests as $guest) { /** @var $originalDetail Detail */
                if (property_exists($entity, 'getHrmExpensesGuests'))
                    if ($entity->getHrmExpensesGuests()->contains($guest) === false) {
                        $guest->getHrmExpensesGuests->removeElement($entity);
                        $em->persist($guest);
                    }
            }

            $update_form->submit($data);
            $securityContext = $this->container->get('security.authorization_checker');
            //get current connected user
            $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();


            $guests = new hrm_expenses_guests();
            if (!empty($data['ArrayfirstName'])) { // create new guests
                foreach ($data['ArrayfirstName'] as $key => $field) {
                    $field2 = $data['ArraylastName'][$key];
                    $guests->AddGuest($data['uid'], $field, $field2, $this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), $em);
                }
            }


            //get the Id of user who request guests
            $infos_user = $em->getRepository('UserBundle:hrm_user')->find($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
//            $collab_id = $infos_user->getId();
            $collab_id = $entity->getCreateUid();

            $entity->setLastUpdate(new \DateTime('now'));
            $entity->setLastUpdateUid($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());

            if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $entity->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
                if ($data['status'] == 1) {
                   // $this->SendMailToResponsable('Nouvelle demande');
                }
            } else if ($securityContext->isGranted('ROLE_ADMIN')) {
                $entity->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $entity->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            }

  if ($request->request->has('attachments')) {
                
                if ($data['attachments'] != null) {
//                    if ($request->request->has('attachments')) {
                        if ($entity->getAttachments() != null) {
                            $attachment = $entity->getAttachments();
                        } else
                            $attachment = new attachments();
                        $attachment->setPath($data['attachments']['path']);
                               if ((isset($data['attachments']['mimetype'])) && ($data['attachments']['mimetype'] != null))
                    $attachment->setMimetype($data['attachments']['mimetype']);
                        $em->persist($attachment);
                        $em->flush($attachment);
                        $entity->setAttachments($attachment);
//                    }
                }
                else {

                    $entity->setAttachments(null);
                }

  }

            $entity->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $entity->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $entity->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));
//            $entity->setSettingsProjects($em->getRepository('AdminBundle:SettingsProjects')->find($data['project']));
            $entity->setSettingsExpDining($em->getRepository('AdminBundle:hrm_settings_exp_dinning')->find($data['settingsExpDining']));
            $entity->setUser($em->getRepository('UserBundle:HrmEmployee')->find($collab_id));
            
            // try to add first open one of pay period
           $ret_payperiod =  $this->setOpenedSettingsPayPeriod($em, $entity, $data['settings_pay_period']);

            if($ret_payperiod !== true)
            {
                return $ret_payperiod;
            }
            
            $em->persist($entity);
            $em->flush();
            
            // update/save  model tax data of expense transport
            if(isset($data['tax_list']))
            {
                $_generateTaxModelInputs = new GenerateTaxModelInputs($this->container);
                $_generateTaxModelInputs->saveExpensesTaxes($em, $entity, $data['tax_list'], 'dining', $this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), 'update');            
            }
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function removeAction(Request $request, hrm_expenses_dining $dining) {
        $form = $this->createDeleteForm($dining);
        $form->handleRequest($request);

        if ($request->isMethod('DELETE')) {            
            $em = $this->getDoctrine()->getManager();
           // update/save  model tax data of expense transport
            $_generateTaxModelInputs = new GenerateTaxModelInputs($this->container);            
            $_generateTaxModelInputs->DeleteAllExpensesTaxes($em, 'dining', $dining);
            
            $em->remove($dining);
            $em->flush($dining);
            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }

    /**
     * Lists all hrm_expenses_dining entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $hrm_expenses_dinings = $em->getRepository('ExpenseBundle:hrm_expenses_dining')->findAll();

        return $this->render('hrm_expense/hrm_expenses_dining/index.html.twig', array(
                    'dining' => $hrm_expenses_dinings,
        ));
    }

    /**
     * Creates a new hrm_expenses_dining entity.
     *
     */
    public function newAction(Request $request) {
        $dining = new Hrm_expenses_dining();
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();

        $infos_user = $em->getRepository('UserBundle:hrm_user')->find($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
        $provider = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $form = $this->createForm('ExpenseBundle\Form\hrm_expenses_diningType', $dining, array(
            'user_id' => $company_id,'provider'=>$provider
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dining);
            $em->flush($dining);

            return $this->redirectToRoute('dining_show', array('id' => $dining->getId()));
        }

        return $this->render('hrm_expense/hrm_expenses_dining/new.html.twig', array(
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_expenses_dining entity.
     *
     */
    public function showAction(hrm_expenses_dining $hrm_expenses_dining) {
        $deleteForm = $this->createDeleteForm($hrm_expenses_dining);

        return $this->render('hrm_expense/hrm_expenses_dining/show.html.twig', array(
                    'hrm_expenses_dining' => $hrm_expenses_dining,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_expenses_dining entity.
     *
     */
    public function editAction(Request $request, hrm_expenses_dining $hrm_expenses_dining) {
        $deleteForm = $this->createDeleteForm($hrm_expenses_dining);
        $editForm = $this->createForm('ExpenseBundle\Form\hrm_expenses_diningType', $hrm_expenses_dining, true);
        $editForm->handleRequest($request);

        $originalDetails = new ArrayCollection();
        foreach ($hrm_expenses_dining->getHrmExpensesGuests() as $detail) {
            $originalDetails->add($detail);
        }
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            foreach ($originalDetails as $originalDetail) { /** @var $originalDetail Detail */
                if ($hrm_expenses_dining->getHrmExpensesGuests()->contains($originalDetail) === false) {
                    $hrm_expenses_dining->removeHrmExpensesGuest($originalDetail);
//                    $originalDetail->setFrai(null);
                    $em->remove($originalDetail);
//                    $em->persist($originalDetail);
                }
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dining_edit', array('id' => $hrm_expenses_dining->getUid()));
        }

        return $this->render('hrm_expense/hrm_expenses_dining/edit.html.twig', array(
                    'dining' => $hrm_expenses_dining,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    public function modaleditAction(Request $request) {
        $dining = new Hrm_expenses_dining();
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
        $infos_user = $em->getRepository('UserBundle:hrm_user')->find($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
        $provider = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $form = $this->createForm('ExpenseBundle\Form\hrm_expenses_diningType', $dining, array(
            'user_id' => $company_id,'provider'=>$provider
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dining);
            $em->flush($dining);

            return $this->redirectToRoute('dining_show', array('id' => $dining->getId()));
        }
        return $this->render('hrm_expense/hrm_expenses_dining/modal_edit.html.twig', array(
                    'dining' => $dining,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Deletes a hrm_expenses_dining entity.
     *
     */
    public function deleteAction(Request $request, hrm_expenses_dining $hrm_expenses_dining) {
        $form = $this->createDeleteForm($hrm_expenses_dining);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_expenses_dining);
            $em->flush();
        }

        return $this->redirectToRoute('dining_index');
    }

    /**
     * Creates a form to delete a hrm_expenses_dining entity.
     *
     * @param hrm_expenses_dining $hrm_expenses_dining The hrm_expenses_dining entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_expenses_dining $hrm_expenses_dining) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('dining_delete', array('id' => $hrm_expenses_dining->getUid())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }
    
    // try to generate input form of tax model
    public function generateModelTaxAction($type, $nature, $expense, $action)
    {
        $em = $this->getDoctrine()->getManager();

         $_generateTaxModelInputs = new GenerateTaxModelInputs($this->container);
         $ret = $_generateTaxModelInputs->createFormInputs($em, $type, $nature, $expense, $action);
         return array("data"=>$ret['data'], "scope_list"=>$ret['scope']);
    }

}
