<?php

namespace ExpenseBundle\Controller;

use ExpenseBundle\Entity\hrm_expenses_guests;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Hrm_expenses_guest controller.
 *
 */
class hrm_expenses_guestsController extends Controller
{
    /**
     * Lists all hrm_expenses_guest entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hrm_expenses_guests = $em->getRepository('ExpenseBundle:hrm_expenses_guests')->findAll();

        return $this->render('hrm_expenses_guests/index.html.twig', array(
            'hrm_expenses_guests' => $hrm_expenses_guests,
        ));
    }

    /**
     * Creates a new hrm_expenses_guest entity.
     *
     */
    public function newAction(Request $request)
    {
        $hrm_expenses_guest = new Hrm_expenses_guest();
        $form = $this->createForm('ExpenseBundle\Form\hrm_expenses_guestsType', $hrm_expenses_guest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($hrm_expenses_guest);
            $em->flush($hrm_expenses_guest);

            return $this->redirectToRoute('guests_show', array('id' => $hrm_expenses_guest->getId()));
        }

        return $this->render('hrm_expenses_guests/new.html.twig', array(
            'hrm_expenses_guest' => $hrm_expenses_guest,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_expenses_guest entity.
     *
     */
    public function showAction(hrm_expenses_guests $hrm_expenses_guest)
    {
        $deleteForm = $this->createDeleteForm($hrm_expenses_guest);

        return $this->render('hrm_expenses_guests/show.html.twig', array(
            'hrm_expenses_guest' => $hrm_expenses_guest,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_expenses_guest entity.
     *
     */
    public function editAction(Request $request, hrm_expenses_guests $hrm_expenses_guest)
    {
        $deleteForm = $this->createDeleteForm($hrm_expenses_guest);
        $editForm = $this->createForm('ExpenseBundle\Form\hrm_expenses_guestsType', $hrm_expenses_guest);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('guests_edit', array('id' => $hrm_expenses_guest->getId()));
        }

        return $this->render('hrm_expenses_guests/edit.html.twig', array(
            'hrm_expenses_guest' => $hrm_expenses_guest,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a hrm_expenses_guest entity.
     *
     */
    public function deleteAction(Request $request, hrm_expenses_guests $hrm_expenses_guest)
    {
        $form = $this->createDeleteForm($hrm_expenses_guest);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_expenses_guest);
            $em->flush();
        }

        return $this->redirectToRoute('guests_index');
    }

    /**
     * Creates a form to delete a hrm_expenses_guest entity.
     *
     * @param hrm_expenses_guests $hrm_expenses_guest The hrm_expenses_guest entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_expenses_guests $hrm_expenses_guest)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('guests_delete', array('id' => $hrm_expenses_guest->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
