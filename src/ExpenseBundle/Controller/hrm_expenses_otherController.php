<?php

namespace ExpenseBundle\Controller;

use ExpenseBundle\Entity\hrm_expenses_other;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FileBundle\Entity\attachments;
use ExpenseBundle\Model\GenerateTaxModelInputs;

/**
 * Hrm_expenses_other controller.
 *
 */
class hrm_expenses_otherController extends Controller
{


    public function statusInsertion($status = null, $entity)
    {
        $date = null;
        if ($status == "2" || $status == "3") {
            $LastUpdateDate = new \DateTime("now");
            $date = $LastUpdateDate->getTimestamp();
        } else {
            $entity->setSettingsPayPeriod(null);
        }
        return $date;
    }

    public function openORcloseAction($status = null, $entity, $entity2, $em)
    {
        $date = $this->statusInsertion($status, $entity);
        if (!empty($entity2)) {
            foreach ($entity2 as $key => $value) {
                $IdPayPeriod[$key] = $value->getId();
                $start[$key] = $value->getStartDate();
                $end[$key] = $value->getEndDate();
                $dateStart = $start[$key]->getTimestamp();
                $dateEnd = $end[$key]->getTimestamp();
                if ($dateStart <= $date && $dateEnd >= $date) {
                    $entity->setSettingsPayPeriod($em->getRepository('PayPeriodBundle:SettingsPayPeriod')->find($IdPayPeriod[$key]));
                }
            }
        }
    }

    /**
     *
     * @param type $em
     * @param type $entity
     * @param type $payPeriodDate celui envoyé depuis from
     * @return boolean/object
     */
    private function setOpenedSettingsPayPeriod($em, $entity, $payPeriodDate = null)
    {
        $ret = '';
        // affect first pay period

        if ($payPeriodDate == null) {
            $searchBy = array("companyId" => $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany(),
                "status" => "open");
        } else {
            $searchBy = array("companyId" => $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany());
        }

        $payperiodlist = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')
            ->findBy($searchBy, array('startDate' => 'ASC'));
        if ($payperiodlist) {

            if ($payPeriodDate == null) {
                $entity->setSettingsPayPeriod($payperiodlist[0]);
                $ret = true;

            } else {
                foreach ($payperiodlist as $key => $value) {
                    if ($value->getStartDate()->format('m/Y') == $payPeriodDate && ($value->getStatus() === 'open')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = true;
                        break;
                    }
                    if (($value->getStartDate()->format('m/Y') === $payPeriodDate) && ($value->getStatus() === 'close')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = false;
                        break;
                    }
                }
            }

            if ($ret === true)
                return true;
            if ($ret === false)
                return new \Symfony\Component\BrowserKit\Response('PAY_PERIOD', 'ERR_CLOSED_PAY_PERIOD');

            return new \Symfony\Component\BrowserKit\Response('PAY_PERIOD', 'ERR_NOT_PAY_PERIOD');

        } else {
            return new \Symfony\Component\BrowserKit\Response('PAY_PERIOD', 'ERR_EMPTY_PAY_PERIOD');;
        }

    }


    public function ConfirmAction(Request $request, hrm_expenses_other $other)
    {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $other->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['confirm']));

        $entity = $em->getRepository('ExpenseBundle:hrm_expenses_other')->find($other);


        /* $SettingsPayPeriod = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')->findAll();
         $this->openORcloseAction($data['confirm'], $other, $SettingsPayPeriod, $em);*/
        //Mail notifier to Collaborateur
//          if ($other->getCreateUid() != NULL) {
//            if ($other->getCreateUid() == $other->getContributor()->getId()) {
//                $TranslateMail = $this->get('mailingbundle.email.send.service');
//              //  $this->get('mailingbundle.email.send.service')->SendMail($entity, $TranslateMail->TagsTranslateExpense('confirmMsg'), $TranslateMail->TagsTranslateExpense('confirmEtat'), $other->getAmountOth());
//            }}
//
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($entity, 'RESPONSABLE', $TranslateMail->TagsTranslateExpense('confirmMsg'), $TranslateMail->TagsTranslateExpense('confirmEtat'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));

        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $other);

        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }

        $em->persist($other);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function CancelAction(Request $request, hrm_expenses_other $other)
    {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $other->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['cancel']));

        $entity = $em->getRepository('ExpenseBundle:hrm_expenses_other')->find($other);


        /*$SettingsPayPeriod = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')->findAll();
        $this->openORcloseAction($data['cancel'], $other, $SettingsPayPeriod, $em);*
         * 
         */
        //Mail notifier to Collaborateur
//        $TranslateMail = $this->get('mailingbundle.email.send.service');
        //$this->get('mailingbundle.email.send.service')->SendMail($entity, $TranslateMail->TagsTranslateExpense('cancelMsg'), $TranslateMail->TagsTranslateExpense('cancelEtat'), $other->getAmountOth());
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($entity, 'RESPONSABLE', $TranslateMail->TagsTranslateExpense('cancelMsg'), $TranslateMail->TagsTranslateExpense('cancelEtat'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));


        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $other);

        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }

        $em->persist($other);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function addAction(Request $request)
    {
        $other = new hrm_expenses_other();
        $form = $this->createForm('ExpenseBundle\Form\hrm_expenses_otherType', $other);
        $form->handleRequest($request);

        $body = $request->getContent();
        $data = json_decode($body, true);

        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
            // will be get error if not deleted settings_pay_period from FormType before submit
            $form->remove('settings_pay_period');

            $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
            $form->submit($data);

            if (isset($data['currency'])) {
                if (!empty($em->getRepository('UserBundle:hrm_company_currency')->find($data['currency']))) {
                    $other->setCompanyCurrency($em->getRepository('UserBundle:hrm_company_currency')->find($data['currency']));
                }
            }
            if (!isset($data['currencyIsoCode'])) {
                $other->setCurrencyIsoCode("EUR - Euro");
            }
            if (!isset($data['amountOthBase'])) {
                $other->setAmountOthBase($data['amountOth']);
            }

            //set default status if ROLE_COLLABORATEUR else set with the submited value 
            $securityContext = $this->container->get('security.authorization_checker');
            if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $other->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            } else if ($securityContext->isGranted('ROLE_ADMIN')) {
                $other->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $other->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            }


            if ($request->request->has('attachments')) {
//                if (is_string($request->request->get('attachments'))) {
//                    $attachment = new attachments();
//                    $attachment->setPath($request->request->get('attachments'));
//                    $em->persist($attachment);
//                    $em->flush($attachment);
//                    $other->setAttachments($attachment);
//                }
                if ($data['attachments'] != null) {
//                    if ($request->request->has('attachments')) {
                    if ($other->getAttachments() != null) {
                        $attachment = $other->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if ((isset($data['attachments']['mimetype'])) && ($data['attachments']['mimetype'] != null))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $other->setAttachments($attachment);
//                    }
                } else {

                    $other->setAttachments(null);
                }
            }

            //Add values for all forgein Keys 

            $other->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $other->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']));
            $other->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));
//            $other->setSettingsProjects($em->getRepository('AdminBundle:SettingsProjects')->find($data['project']));
            $other->setSettingsExpenses($em->getRepository('AdminBundle:hrm_settings_exp_other')->find($data['natureOth']));
            $other->setUser($em->getRepository('UserBundle:HrmEmployee')->find($this->container->get('settingsbundle.preference.service')->getEmpData()->getId()));

            //Add default value on create Action
            $other->setCreateDate(new \DateTime('now'));
            $other->setCreateUid($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
            $other->setLastUpdateUid($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
            $other->setLastUpdate(new \DateTime('now'));

            //send mail to responsable

            $TranslateMail = $this->get('mailingbundle.email.send.service');
            if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $this->get('mailingbundle.email.send.service')->Notify($other, 'RESPONSABLE', $TranslateMail->TagsTranslateExpense('titleNew'), $TranslateMail->TagsTranslateExpense('etatNew'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));
            } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $this->get('mailingbundle.email.send.service')->Notify($other, 'COLLABORATEUR', $TranslateMail->TagsTranslateExpense('titleNew'), $TranslateMail->TagsTranslateExpense('etatNew'), $TranslateMail->TagsTranslateExpense('typeRequest'), $TranslateMail->TagsTranslateExpense('folder'), $TranslateMail->TagsTranslateExpense('viewName'));
            }

            // try to add first open one of pay period
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $other, $data['settings_pay_period']);

            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }

            $em->persist($other);
            $em->flush();

            // save new model tax data of expense transport
            if (isset($data['tax_list'])) {
                $_generateTaxModelInputs = new GenerateTaxModelInputs($this->container);
                $_generateTaxModelInputs->saveExpensesTaxes($em, $other, $data, 'other', $this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
            }

            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    /**
     * Displays a form to update an existing ik entity.
     *
     */
    public function updateAction(Request $request, hrm_expenses_other $other)
    {
        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $entity = $em->getRepository('ExpenseBundle:hrm_expenses_other')->find($data['uid']);

        $update_form = $this->createForm('ExpenseBundle\Form\hrm_expenses_otherType', $entity);

        if ($request->isMethod('PUT')) {
            // will be get error if not deleted settings_pay_period from FormType before submit
            $update_form->remove('settings_pay_period');

            $securityContext = $this->container->get('security.authorization_checker');
            //get current connected user
            $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();

            $update_form->submit($data);

            if (isset($data['currency'])) {
                if (!empty($em->getRepository('UserBundle:hrm_company_currency')->find($data['currency']))) {
                    $other->setCompanyCurrency($em->getRepository('UserBundle:hrm_company_currency')->find($data['currency']));
                }
            }
        
            if (!isset($data['currencyIsoCode'])) {
                $CurrencyCode = $this->container->get('settingsbundle.preference.service')->getBaseCurrencyIsoCode();
                $other->setCurrencyIsoCode($CurrencyCode);
            }
            if (!isset($data['amountOthBase'])) {
                $other->setAmountOthBase($data['amountOth']);
            }
            //get the Id of user who request ik
            $infos_user = $em->getRepository('UserBundle:hrm_user')->find($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
//            $collab_id = $infos_user->getId();
            $collab_id = $entity->getCreateUid();
            $other->setUser($em->getRepository('UserBundle:HrmEmployee')->find($collab_id));

            $other->setLastUpdate(new \DateTime('now'));
            $other->setLastUpdateUid($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());

            if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $other->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
                if ($data['status'] == 1) {
                    //$this->SendMailToResponsable('Nouvelle demande');
                }
            } else if ($securityContext->isGranted('ROLE_ADMIN')) {
                $other->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $other->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            }


            if ($data['attachments'] != null) {
                if ($data['attachments']['path']) {
                    if ($entity->getAttachments() != null) {
                        $attachment = $entity->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if (isset($data['attachments']['mimetype']))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $other->setAttachments($attachment);
                }
            } else {

                $other->setAttachments(null);
            }

            $other->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $other->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $other->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));
//            $other->setSettingsProjects($em->getRepository('AdminBundle:SettingsProjects')->find($data['project']));
            $other->setSettingsExpenses($em->getRepository('AdminBundle:hrm_settings_exp_other')->find($data['natureOth']));


            // try to add first open one of pay period
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $entity, $data['settings_pay_period']);

            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }

            $em->persist($entity);
            $em->flush();

            // update/save  model tax data of expense transport
            if (isset($data['tax_list'])) {
                $_generateTaxModelInputs = new GenerateTaxModelInputs($this->container);
                $_generateTaxModelInputs->saveExpensesTaxes($em, $entity, $data, 'other', $this->container->get('settingsbundle.preference.service')->getEmpData()->getId(), 'update');
            }
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function removeAction(Request $request, hrm_expenses_other $other)
    {
        $form = $this->createDeleteForm($other);
        $form->handleRequest($request);

        if ($request->isMethod('DELETE')) {

            $em = $this->getDoctrine()->getManager();
            // update/save  model tax data of expense transport
            $_generateTaxModelInputs = new GenerateTaxModelInputs($this->container);
            $_generateTaxModelInputs->DeleteAllExpensesTaxes($em, 'other', $other);

            $em->remove($other);
            $em->flush($other);
            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }

    /**
     * Lists all hrm_expenses_other entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $others = $em->getRepository('ExpenseBundle:hrm_expenses_other')->findAll();

        return $this->render('hrm_expense/hrm_expenses_other/index.html.twig', array(
            'hrm_expenses_others' => $others,
        ));
    }

    /**
     * Creates a new hrm_expenses_other entity.
     *
     */
    public function newAction(Request $request)
    {
        $other = new Hrm_expenses_other();
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
        $infos_user = $em->getRepository('UserBundle:hrm_user')->find($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
        $provider = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $form = $this->createForm('ExpenseBundle\Form\hrm_expenses_otherType', $other, array(
            'user_id' => $company_id, 'provider' => $provider
        ));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($other);
            $em->flush($other);

            return $this->redirectToRoute('other_show', array('id' => $other->getId()));
        }
        return $this->render('hrm_expense/hrm_expenses_other/new.html.twig', array(
            'hrm_expenses_other' => $other,
            'form' => $form->createView(),
        ));
    }

    public function modaleditAction(Request $request)
    {
        $other = new Hrm_expenses_other();
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();

        $infos_user = $em->getRepository('UserBundle:hrm_user')->find($this->container->get('settingsbundle.preference.service')->getEmpData()->getId());
        $provider = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $company_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $form = $this->createForm('ExpenseBundle\Form\hrm_expenses_otherType', $other, array(
            'user_id' => $company_id,
            'provider' => $provider
        ));
        $form->handleRequest($request);
        return $this->render('hrm_expense/hrm_expenses_other/modal_edit.html.twig', array(
            'hrm_expenses_other' => $other,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_expenses_other entity.
     *
     */
    public function showAction(hrm_expenses_other $other)
    {
        $deleteForm = $this->createDeleteForm($other);

        return $this->render('hrm_expense/hrm_expenses_other/show.html.twig', array(
            'hrm_expenses_other' => $other,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_expenses_other entity.
     *
     */
    public function editAction(Request $request, hrm_expenses_other $other)
    {
        $deleteForm = $this->createDeleteForm($other);
        $editForm = $this->createForm('ExpenseBundle\Form\hrm_expenses_otherType', $other);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('other_edit', array('id' => $other->getId()));
        }

        return $this->render('hrm_expense/hrm_expenses_other/edit.html.twig', array(
            'hrm_expenses_other' => $other,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a hrm_expenses_other entity.
     *
     */
    public function deleteAction(Request $request, hrm_expenses_other $other)
    {
        $form = $this->createDeleteForm($other);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($other);
            $em->flush();
        }

        return $this->redirectToRoute('other_index');
    }

    /**
     * Creates a form to delete a hrm_expenses_other entity.
     *
     * @param hrm_expenses_other $other The hrm_expenses_other entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_expenses_other $other)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('other_delete', array('id' => $other->getUid())))
            ->setMethod('DELETE')
            ->getForm();
    }

    // try to generate input form of tax model
    public function generateModelTaxAction($type, $nature, $expense, $action)
    {
        $em = $this->getDoctrine()->getManager();

        $_generateTaxModelInputs = new GenerateTaxModelInputs($this->container);
        $ret = $_generateTaxModelInputs->createFormInputs($em, $type, $nature, $expense, $action);
        return array("data" => $ret['data'], "scope_list" => $ret['scope']);
    }

}
