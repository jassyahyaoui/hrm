<?php

namespace ExpenseBundle\Tests;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class TransportTest extends WebTestCase {

    private $client = null;

    public function setUp() {
     //  echo "\n --------Run Unit test of SettingsPayPeriod-------- \n";
       $this->client = static::createClient();
       
       /*** user login **********/
        $this->client->request('GET', '/logout');
       $crawler = $this->client->request('GET', '/login');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('html:contains("Veuillez")')->count() > 0);

        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => "responsable",
            '_password' => "responsable",
        ));
        $this->client->submit($form);
    }

    public function testAdd() {

        $json = '{'
                . '"contributor":{"id":"2"},'
                . '"date":"17/08/2017 ",'
                . '"requester":"Responsable",'
                . '"type":"Transport",'
                . '"settingsExpDining":"Restauration",'
                . '"collaborateur":"Responsable",'
                . '"settings_pay_period":2,'
                . '"nature":"4",'
                . '"tax_list":{"tva_2_10":"4.3","tva_3_20":"2.3"},'
                . '"untaxedAmountTrans":37.4,'
                . '"untaxedAmount":0,'
                . '"untaxedAmountDin":0,'
                . '"untaxedAmountOth":0,'
                . '"amountTran":44,'
                . '"displayName":"frais_Trans_09_2017",'
                . '"validator":"2",'
                . '"status":"4"}';
       $this->client->request('POST', '/expenses/api/transport/add', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'CONTENT_TYPE' => 'application/json',
                ), $json);
       echo($this->client->getResponse());
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());        
    }
  
   public function testTransportTaxes() {

       $this->client->request('GET', '/expenses/api/transport/Transport/4');
       echo($this->client->getResponse());
       $this->assertSame(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());        
    }
    
    public function testConfirm() {

        $json = '{"confirm":"1"}';
       $this->client->request('PUT', '/expenses/api/transport/785/confirm', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'CONTENT_TYPE' => 'application/json',
                ), $json);
       echo($this->client->getResponse()->getContent());
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());
        
    }  
    
    public function testCancel() {

        $json = '{"cancel":"3"}';
       $this->client->request('PUT', '/expenses/api/transport/4/cancel', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'CONTENT_TYPE' => 'application/json',
                ), $json);
       echo($this->client->getResponse()->getContent());
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());
        
    }  
    
    public function testRemove() {

       $this->client->request('DELETE', '/expenses/api/transport/4/remove');
       echo($this->client->getResponse()->getContent());
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());
        
    }      
}
