<?php

namespace ExpenseBundle\Tests;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class IkTest extends WebTestCase {

    private $client = null;

    public function setUp() {
     //  echo "\n --------Run Unit test of SettingsPayPeriod-------- \n";
       $this->client = static::createClient();
       
       /*** user login **********/
        $this->client->request('GET', '/logout');
       $crawler = $this->client->request('GET', '/login');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('html:contains("Veuillez")')->count() > 0);

        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => "responsable",
            '_password' => "responsable",
        ));
        $this->client->submit($form);
    }

    public function testAdd() {

        $json = '{"contributor":{"id":"2"},'
                . '"date":"16/08/2017 ",'
                . '"requester":"Responsable",'
                . '"type":"Ik",'
                . '"settingsExpDining":"Restauration",'
                . '"collaborateur":"Responsable",'
                . '"settings_pay_period":"09/2017",'
                . '"displayName":"ddd",'
                . '"validator":"2",'
                . '"kilometer":500,'
                . '"coefficient":"1",'
                . '"amountIk":205,'
                . '"status":"4"}';
       $this->client->request('POST', '/expenses/api/ik/add', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'CONTENT_TYPE' => 'application/json',
                ), $json);
       //echo($this->client->getResponse());
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());        
    }
    
    public function testUpdate() {

        $json = '{"uid":4,'
                . '"contributor":{"id":"2"},'
                . '"date":"16/08/2017 ",'
                . '"requester":"Responsable",'
                . '"type":"Ik",'
                . '"settingsExpDining":"Restauration",'
                . '"collaborateur":"Responsable",'
                . '"settings_pay_period":"09/2017",'
                . '"displayName":"ddd",'
                . '"validator":"2",'
                . '"kilometer":500,'
                . '"coefficient":"1",'
                . '"amountIk":205,'
                . '"status":"4"}';
       $this->client->request('PUT', '/expenses/api/ik/4/update', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'CONTENT_TYPE' => 'application/json',
                ), $json);
       //echo($this->client->getResponse()->getContent());
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());
        
    }  
            
    public function testConfirm() {

        
        $json = '{"confirm":"1ss"}';
       $this->client->request('PUT', '/expenses/api/ik/785/confirm', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'CONTENT_TYPE' => 'application/json',
                ), $json);
       echo($this->client->getResponse()->getContent());
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());
        
    }  
    
    public function testCancel() {

        
        $json = '{"cancel":"3"}';
       $this->client->request('PUT', '/expenses/api/ik/4/cancel', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'CONTENT_TYPE' => 'application/json',
                ), $json);
       echo($this->client->getResponse()->getContent());
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());
        
    }  
    
    public function testRemove() {

        
       $this->client->request('DELETE', '/expenses/api/ik/4/remove');
       echo($this->client->getResponse()->getContent());
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());
        
    }     
    
    
    
}
