<?php

namespace ExpenseBundle\Tests;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class AccomodationTest extends WebTestCase {

    private $client = null;

    public function setUp() {
     //  echo "\n --------Run Unit test of SettingsPayPeriod-------- \n";
       $this->client = static::createClient();
       
       /*** user login **********/
        $this->client->request('GET', '/logout');
       $crawler = $this->client->request('GET', '/login');
        $this->assertSame(200, $this->client->getResponse()->getStatusCode());
        $this->assertTrue($crawler->filter('html:contains("Veuillez")')->count() > 0);

        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => "responsable",
            '_password' => "responsable",
        ));
        $this->client->submit($form);
    }

   public function testAccomodationTaxes() {

       $this->client->request('GET', '/expenses/api/accomodation/Hebergement/4');
       echo($this->client->getResponse());
       $this->assertSame(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());        
    }
    
    
    public function testConfirm() {

        $json = '{"confirm":"1"}';
       $this->client->request('PUT', '/expenses/api/accomodation/785/confirm', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'CONTENT_TYPE' => 'application/json',
                ), $json);
       echo($this->client->getResponse()->getContent());
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());
        
    }  
    
    public function testCancel() {

        $json = '{"cancel":"3"}';
       $this->client->request('PUT', '/expenses/api/accomodation/4/cancel', array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'CONTENT_TYPE' => 'application/json',
                ), $json);
       echo($this->client->getResponse()->getContent());
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());
        
    }  
    
    public function testRemove() {

       $this->client->request('DELETE', '/expenses/api/accomodation/4/remove');
       echo($this->client->getResponse()->getContent());
       $this->assertEquals(200, $this->client->getResponse()->getStatusCode(), $this->client->getResponse()->getContent());
        
    }    
   
}
