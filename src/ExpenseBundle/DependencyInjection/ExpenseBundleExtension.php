<?php

namespace ExpenseBundleExtension\DependencyInjection;

use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class ExpenseBundleExtension extends Extension {

    public function load(array $configs, ContainerBuilder $container) {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $ymlValidationFile = $container->getParameter('validator.mapping.loader.yaml_files_loader.mapping_files');
        $ymlValidationFile[] = __DIR__ . '/../Resources/config/validation.yml';
        $container->setParameter('validator.mapping.loader.yaml_files_loader.mapping_files', $ymlValidationFile);
    }

}
