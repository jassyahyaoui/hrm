<?php

namespace ExpenseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Doctrine\ORM\EntityRepository;

class hrm_expense_ikType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $type = array("Ik" => "Ik", "Transport" => "Transport", "Hebergement" => "Hebergement", "Restauration" => "Restauration", "Autres" => "Autres");

        $builder
                ->add('contributor', 'entity', array(
                    'label' => 'Collaborateur',
//                    'empty_value' => 'Sélectionner un collaborateur',
                    'required' => true,
                    'class' => 'UserBundle\Entity\HrmEmployee',
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder('u')
                                ->andwhere("usr.roles LIKE '%COLLABORATEUR%' OR usr.roles LIKE '%RESPONSABLE%'")
                                ->leftjoin('u.user', 'usr')
                                // ->join('e.company', 'c')
                                ->andwhere('u.company = :value')
                                ->setParameter('value', (int) $options['user_id']);
//                                ->orderBy('u.username', 'ASC');
                    },
                    'attr' => array(
                        'class' => 'form-control ',
                        'type-suggestions' => '',
                        'datasource' => '[]',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
            ->add('validator', 'entity', array(
                'label' => 'type',
                'required' => false,
                'class' => 'UserBundle\Entity\HrmEmployee',
//                    'property' => 'display_name',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->where("e.roles LIKE '%RESPONSABLE%'")
                        ->andwhere('u.company = :value')
                        ->setParameter('value', (int)$options['user_id']);
                },
            ))
                ->add('displayName', 'text', array(
                    'label' => 'Nom du frais',
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('type', 'choice', array(
                    'label' => 'Type de frais',
                    'choices' => $type,
                    'required' => true,
                    'attr' => array('class' => 'form-control',
                     //   'ng-model' => 'postData.type',
                        'ng-change' => 'appear()',
                        'class' => 'form-control'
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                        )
                )
                ->add('comment', 'textarea', array(
                    'label' => 'Commentaire',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right'
                    )
                ))
                ->add('date', 'date', array(
                    'label' => 'Date du frais',
                    'required' => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'date-directive' => ''
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('settings_pay_period', 'text', array(
                    // 'class' => 'PayPeriodBundle\Entity\SettingsPayPeriod',
                    'label' => 'Remboursement en',
                    'required' => true,
//                    'read_only' => true,
                    //'widget' => 'single_text',
                    // 'format' => 'MM/yyyy',
                    'attr' => array(
//                       'class' => 'form-control',
                        'class' => 'form-control date-picker-expense',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('kilometer', 'text', array(
                    'label' => 'Nombre KM',
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix',
                    )
                ))
                ->add('amountIk', 'text', array(
                    'label' => 'TTC',
                    'required' => true,
                    'read_only' => true,
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right'
                    )
                ))
                ->add('settingsProjects', 'entity', array(
                    'label' => 'Projet',
                    'empty_value' => 'Sélectionner un projet',
                    'required' => true,
                    'class' => 'AdminBundle\Entity\SettingsProjects',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'chosen-select form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', 2)
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('settingsExpIk', 'entity', array(
                    'label' => 'Coefficient KM',
                    'empty_value' => 'Sélectionner un coefficient',
                    'required' => true,
                    'class' => 'AdminBundle\Entity\hrm_settings_exp_ik',
                    'attr' => array(
                        'class' => 'form-control',
                            'type-suggestions'=>'',
                        'datasource'=>'[]',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('SettingsStatus', 'entity', array(
                    'label' => 'Statut',
                    'empty_value' => 'Sélectionner un statut',
                    'required' => true,
                    'class' => 'AdminBundle\Entity\SettingsStatus',
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder('s')
                                ->leftJoin('s.provider', 'p')
                                ->where("s.id != :statut")
                                ->setParameter('statut', 4)
//                                ->AndWhere('p.display_name = :provider')
//                                ->setParameter('provider', (int) $options['provider'])
                        ;
                    },
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('requester', 'entity', array(
                    'label' => 'Collaborateur',
                    'empty_value' => 'Sélectionner un requester',
                    'required' => true,
                    'disabled' => true,
                    'read_only' => true,
                    'class' => 'UserBundle\Entity\HrmEmployee',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('u')
                                ->leftjoin('u.user', 'e')
                                ->Where('u.company = :value')
//                        ->andwhere('e.id = :valueUser')
                                ->setParameter('value', (int) $options['user_id']);
//                        ->setParameter('valueUser', (int)$options['currentUser']);
                    },
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'ExpenseBundle\Entity\hrm_expense_ik',
            'user_id' => null,
            'provider' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'expensebundle_ik';
    }

}
