<?php

namespace ExpenseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class hrm_expenses_diningType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $type = array("Ik" => "Ik", "Transport" => "Transport", "Hebergement" => "Hebergement", "Restauration" => "Restauration", "Autres" => "Autres");
        $dinning_type = array("Restauration" => "Restauration", "Consommation" => "Consommation");

        $builder
                ->add('contributor', 'entity', array(
                    'label' => 'Collaborateur',
                    'required' => true,
                    'class' => 'UserBundle\Entity\HrmEmployee',
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder('u')
                                ->andwhere("usr.roles LIKE '%COLLABORATEUR%' OR usr.roles LIKE '%RESPONSABLE%'")
                                ->leftjoin('u.user', 'usr')
                                // ->join('e.company', 'c')
                                ->andwhere('u.company = :value')
                                ->setParameter('value', (int) $options['user_id']);
//                                ->orderBy('u.username', 'ASC');
                    },
                    'attr' => array(
                        'class' => 'form-control',
                        'type-suggestions' => '',
                        'datasource' => '[]',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('validator', 'entity', array(
                    'label' => 'Valideur de la demande',
                    'empty_value' => 'Sélectionner un valideur',
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control',
                        'id' => 'id',
                        'required' => true,
                        'type-suggestions' => '',
                        'datasource' => '[]',
                    ),
                    'class' => 'UserBundle\Entity\HrmEmployee',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('u')
                                ->leftjoin('u.user', 'e')
                                ->where("e.roles LIKE '%RESPONSABLE%'")
                                ->andwhere('u.company = :value')
                                ->setParameter('value', (int) $options['user_id']);
                    },
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'))
                )
                ->add('requester', 'entity', array(
                    'label' => 'Collaborateur',
                    'empty_value' => 'Sélectionner un requester',
                    'required' => true,
                    'disabled' => true,
                    'read_only' => true,
                    'class' => 'UserBundle\Entity\HrmEmployee',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('u')
                                ->leftjoin('u.user', 'e')
                                ->Where('u.company = :value')
//                        ->andwhere('e.id = :valueUser')
                                ->setParameter('value', (int) $options['user_id']);
//                        ->setParameter('valueUser', (int)$options['currentUser']);
                    },
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('displayName', 'text', array(
                    'label' => 'Nom du frais',
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control'
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('type', 'choice', array(
                    'label' => 'Type de frais',
                    'choices' => $type,
                    'required' => true,
                    'attr' => array('class' => 'form-control',
                    //    'ng-model' => 'postData.type',
                        'ng-change' => 'appear()',
                        'class' => 'form-control'


                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                        )
                )
                ->add('comment', 'textarea', array(
                    'label' => 'Commentaire',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right'
                    )
                ))
                ->add('date', 'date', array(
                    'label' => 'Date du frais',
                    'required' => true,
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array(
                        'class' => 'form-control datepicker',
                        'date-directive' => ''
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('settings_pay_period', 'text', array(
                    //   'class' => 'PayPeriodBundle\Entity\SettingsPayPeriod',
                    'label' => 'Remboursement en',
                    'required' => true,
//                    'read_only' => true,
                    //'widget' => 'single_text',
                    //'format' => 'MM/yyyy',
                    'attr' => array(
                        'class' => 'form-control date-picker-expense',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('settingsExpDining', 'entity', array(
                    'label' => 'Nature',
                    'empty_value' => 'Sélectionner une nature',
                    'required' => true,
                    'class' => 'AdminBundle\Entity\hrm_settings_exp_dinning',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                        'ng-change' => 'convivesappear()',
                        'type-suggestions' => '',
                        'datasource' => '[]',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('amountDin', 'text', array(
                    'label' => 'Montant à rembourser',
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
//                ->add('taxe20Din', 'text', array(
//                    'label' => 'Dont tva à 20%',
//                    'required' => false,
//                    'attr' => array(
//                        'class' => 'form-control',
//                    ),
//                    'label_attr' => array(
//                        'class' => 'col-sm-3 control-label no-padding-right'
//                    )
//                ))
//                ->add('taxe10Din', 'text', array(
//                    'label' => 'Dont tva à 10%',
//                    'required' => false,
//                    'attr' => array(
//                        'class' => 'form-control',
//                    ),
//                    'label_attr' => array(
//                        'class' => 'col-sm-3 control-label no-padding-right'
//                    )
//                ))
//                ->add('taxe5Din', 'text', array(
//                    'label' => 'Dont tva à 5,50%',
//                    'required' => false,
//                    'attr' => array(
//                        'class' => 'form-control',
//                    ),
//                    'label_attr' => array(
//                        'class' => 'col-sm-3 control-label no-padding-right'
//                    )
//                ))
                ->add('untaxedAmountDin', 'text', array(
                    'label' => 'Montant HT',
                    'required' => true,
                    'read_only' => true,
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right'
                    )
                ))
                ->add('collaborateur', 'text', array(
                    'label' => 'Collaborateur',
                    'required' => false,
                    'read_only' => true,
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right'
                    )
                ))
                ->add('settingsProjects', 'entity', array(
                    'label' => 'Projet',
                    'empty_value' => 'Sélectionner un projet',
                    'required' => true,
                    'class' => 'AdminBundle\Entity\SettingsProjects',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                    ,
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('SettingsStatus', 'entity', array(
                    'label' => 'Statut',
                    'empty_value' => 'Sélectionner un statut',
                    'required' => true,
                    'class' => 'AdminBundle\Entity\SettingsStatus',
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder('s')
                                ->where("s.id != :statut")
                                ->setParameter('statut', 4);
                    },
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('hrmExpensesGuests', 'collection', array(
                    'type' => new hrm_expenses_guestsType(),
                    'allow_add' => true,
                    'allow_delete' => false,
                    'by_reference' => false,
                    'label' => false
                ))

            ->add('amountDinBase', 'text', array(
                'label' => 'Montant à rembourser de Base',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('company_currency', 'entity', array(
                'label' => 'Currency',
//                'empty_value' => 'Sélectionner un currency',
                'required' => true,
                'class' => 'UserBundle\Entity\hrm_company_currency',
                'property' => 'currency_iso_code',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('currencyIsoCode')

        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'ExpenseBundle\Entity\hrm_expenses_dining',
            'user_id' => null, 'provider' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'expensebundle_hrm_expenses_dining';
    }

}
