<?php

namespace ExpenseBundle\Service;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use ExpenseBundle\Model\GenerateTaxModelInputs;

class expenseService {

    protected $em;
    private $container;

    public function __construct(EntityManager $entityManager, Container $container) {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function getRequestAbsence() {
    }

    public function getNbrExpense() {
    }

    public function getLastRequestAbsence() {
    }

    public function deleteAllExpenses() {
        $qb = $this->em->createQuery('DELETE ExpenseBundle:hrm_expense_ik p')->execute();
//        $qb = $this->em->createQuery('DELETE ExpenseBundle:hrm_expense_transport p')->execute();
//        $qb = $this->em->createQuery('DELETE ExpenseBundle:hrm_expenses_accomodation p')->execute();
//        $qb = $this->em->createQuery('DELETE ExpenseBundle:hrm_expenses_dining p')->execute();
//        $qb = $this->em->createQuery('DELETE ExpenseBundle:hrm_expenses_guests p')->execute();
        
        $expensesOther = $this->container->get('doctrine')->getEntityManager()->getRepository('ExpenseBundle:hrm_expenses_other')->findAll();
        foreach ($expensesOther as $expenseOther) {
            $_generateTaxModelInputs = new GenerateTaxModelInputs($this->container);            
            $_generateTaxModelInputs->DeleteAllExpensesTaxes($this->container->get('doctrine')->getEntityManager(), 'other', $expenseOther);
        }   
        $qb = $this->em->createQuery('DELETE ExpenseBundle:hrm_expenses_other p')->execute();

    }

    public function findExpense($expenseName) {
        $expense = $this->container->get('doctrine')->getEntityManager()->getRepository('ExpenseBundle:hrm_expense_ik')
                ->findOneBy(array('displayName' => $expenseName));
        if (!$expense)
            $expense = $this->container->get('doctrine')->getEntityManager()->getRepository('ExpenseBundle:hrm_expenses_other')
                    ->findOneBy(array('displayName' => $expenseName));
        return $expense;
    }

    public function findExpenseOther($expenseName) {
        $expense = $this->container->get('doctrine')->getEntityManager()->getRepository('ExpenseBundle:hrm_expenses_other')
                  ->findOneBy(array('displayName' => $expenseName));
        if ($expense)
            return $expense;
        else 
            return null;
    }
    
    
     public function findListExpensesWithName($expenseName) {
        $expenses = $this->container->get('doctrine')->getEntityManager()->getRepository('ExpenseBundle:hrm_expense_ik')
                ->findBy(array('displayName' => $expenseName));
        if (!$expenses)
            $expenses = $this->container->get('doctrine')->getEntityManager()->getRepository('ExpenseBundle:hrm_expenses_other')
                    ->findBy(array('displayName' => $expenseName));
        return $expenses;
    }

}
