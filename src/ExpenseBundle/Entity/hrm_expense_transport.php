<?php

namespace ExpenseBundle\Entity;

/**
 * transport
 */
class hrm_expense_transport
{

    /**
     * @var integer
     */
    private $uid;

    /**
     * @var string
     */
    private $displayName;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * @var integer
     */
    private $createUid;

    /**
     * @var \DateTime
     */
    private $lastUpdate;

    /**
     * @var integer
     */
    private $lastUpdateUid;

    /**
     * @var float
     */
    private $amountTran;

    /**
     * @var float
     */
    private $untaxedAmountTrans;
    
    /**
     * @var string
     */
    private $requester;

    /**
     * @var \FileBundle\Entity\attachments
     */
    private $attachments;

    /**
     * @var \AdminBundle\Entity\SettingsProjects
     */
    private $settings_projects;

    /**
     * @var \AdminBundle\Entity\hrm_settings_exp_transport
     */
    private $settings_exp_transport;

    /**
     * @var \AdminBundle\Entity\SettingsStatus
     */
    private $settings_status;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $user;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $contributor;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $validator;

    /**
     * @var float
     */
    private $amountTranBase;

    /**
     * @var string
     */
    private $currencyIsoCode;

    /**
     * @var \UserBundle\Entity\hrm_company_currency
     */
    private $company_currency;

    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return hrm_expense_transport
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return hrm_expense_transport
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return hrm_expense_transport
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return hrm_expense_transport
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_expense_transport
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_expense_transport
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_expense_transport
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_expense_transport
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->lastUpdateUid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->lastUpdateUid;
    }

    /**
     * Set amountTran
     *
     * @param float $amountTran
     *
     * @return hrm_expense_transport
     */
    public function setAmountTran($amountTran)
    {
        $this->amountTran = $amountTran;

        return $this;
    }

    /**
     * Get amountTran
     *
     * @return float
     */
    public function getAmountTran()
    {
        return $this->amountTran;
    }

    /**
     * Set requester
     *
     * @param string $requester
     *
     * @return hrm_expense_transport
     */
    public function setRequester($requester)
    {
        $this->requester = $requester;

        return $this;
    }

    /**
     * Get requester
     *
     * @return string
     */
    public function getRequester()
    {
        return $this->requester;
    }

    /**
     * Set attachments
     *
     * @param \FileBundle\Entity\attachments $attachments
     *
     * @return hrm_expense_transport
     */
    public function setAttachments(\FileBundle\Entity\attachments $attachments = null)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Get attachments
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Set settingsProjects
     *
     * @param \AdminBundle\Entity\SettingsProjects $settingsProjects
     *
     * @return hrm_expense_transport
     */
    public function setSettingsProjects(\AdminBundle\Entity\SettingsProjects $settingsProjects = null)
    {
        $this->settings_projects = $settingsProjects;

        return $this;
    }

    /**
     * Get settingsProjects
     *
     * @return \AdminBundle\Entity\SettingsProjects
     */
    public function getSettingsProjects()
    {
        return $this->settings_projects;
    }

    /**
     * Set settingsStatus
     *
     * @param \AdminBundle\Entity\SettingsStatus $settingsStatus
     *
     * @return hrm_expense_transport
     */
    public function setSettingsStatus(\AdminBundle\Entity\SettingsStatus $settingsStatus = null)
    {
        $this->settings_status = $settingsStatus;

        return $this;
    }

    /**
     * Get settingsStatus
     *
     * @return \AdminBundle\Entity\SettingsStatus
     */
    public function getSettingsStatus()
    {
        return $this->settings_status;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\HrmEmployee $user
     *
     * @return hrm_expense_transport
     */
    public function setUser(\UserBundle\Entity\HrmEmployee $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set contributor
     *
     * @param \UserBundle\Entity\HrmEmployee $contributor
     *
     * @return hrm_expense_transport
     */
    public function setContributor(\UserBundle\Entity\HrmEmployee $contributor = null)
    {
        $this->contributor = $contributor;

        return $this;
    }

    /**
     * Get contributor
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getContributor()
    {
        return $this->contributor;
    }

    /**
     * Set validator
     *
     * @param \UserBundle\Entity\HrmEmployee $validator
     *
     * @return hrm_expense_transport
     */
    public function setValidator(\UserBundle\Entity\HrmEmployee $validator = null)
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * Get validator
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getValidator()
    {
        return $this->validator;
    }
    /**
     * @var \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    private $settings_pay_period;


    /**
     * Set settingsPayPeriod
     *
     * @param \PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod
     *
     * @return hrm_expense_transport
     */
    public function setSettingsPayPeriod(\PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod = null)
    {
        $this->settings_pay_period = $settingsPayPeriod;

        return $this;
    }

    /**
     * Get settingsPayPeriod
     *
     * @return \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    public function getSettingsPayPeriod()
    {
        return $this->settings_pay_period;
    }

    /**
     * Set settingsExpTransport
     *
     * @param \AdminBundle\Entity\hrm_settings_exp_transport $settingsExpTransport
     *
     * @return hrm_expense_transport
     */
    public function setSettingsExpTransport(\AdminBundle\Entity\hrm_settings_exp_transport $settingsExpTransport = null)
    {
        $this->settings_exp_transport = $settingsExpTransport;

        return $this;
    }

    /**
     * Get settingsExpTransport
     *
     * @return \AdminBundle\Entity\hrm_settings_exp_transport
     */
    public function getSettingsExpTransport()
    {
        return $this->settings_exp_transport;
    }

    /**
     * Set untaxedAmountTrans
     *
     * @param float $untaxedAmountTrans
     *
     * @return hrm_expense_transport
     */
    public function setUntaxedAmountTrans($untaxedAmountTrans)
    {
        $this->untaxedAmountTrans = $untaxedAmountTrans;

        return $this;
    }

    /**
     * Get untaxedAmountTrans
     *
     * @return float
     */
    public function getUntaxedAmountTrans()
    {
        return $this->untaxedAmountTrans;
    }

    /**
     * Set amountTranBase
     *
     * @param float $amountTranBase
     *
     * @return hrm_expense_transport
     */
    public function setAmountTranBase($amountTranBase)
    {
        $this->amountTranBase = $amountTranBase;

        return $this;
    }

    /**
     * Get amountTranBase
     *
     * @return float
     */
    public function getAmountTranBase()
    {
        return $this->amountTranBase;
    }

    /**
     * Set currencyIsoCode
     *
     * @param string $currencyIsoCode
     *
     * @return hrm_expense_transport
     */
    public function setCurrencyIsoCode($currencyIsoCode)
    {
        $this->currencyIsoCode = $currencyIsoCode;

        return $this;
    }

    /**
     * Get currencyIsoCode
     *
     * @return string
     */
    public function getCurrencyIsoCode()
    {
        return $this->currencyIsoCode;
    }

    /**
     * Set companyCurrency
     *
     * @param \UserBundle\Entity\hrm_company_currency $companyCurrency
     *
     * @return hrm_expense_transport
     */
    public function setCompanyCurrency(\UserBundle\Entity\hrm_company_currency $companyCurrency = null)
    {
        $this->company_currency = $companyCurrency;

        return $this;
    }

    /**
     * Get companyCurrency
     *
     * @return \UserBundle\Entity\hrm_company_currency
     */
    public function getCompanyCurrency()
    {
        return $this->company_currency;
    }
}
