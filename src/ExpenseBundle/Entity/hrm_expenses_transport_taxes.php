<?php

namespace ExpenseBundle\Entity;

/**
 * hrm_expenses_transport_taxes
 */
class hrm_expenses_transport_taxes
{
    /**
     * @var int
     */
    private $uid;

    /**
     * @var hrm_expense_transport
     */
    private $expense;
    
    /**
     * @var \AdminBundle\Entity\hrm_settings_tax
     */
    private $tax;

    /**
     * @var string
     */
    private $currencyIsoCode;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var float
     */
    private $amountBase;

    /**
     * @var string
     */
    private $currency;
    
    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * @var integer
     */
    private $createUid;

    /**
     * @var \DateTime
     */
    private $lastUpdate;

    /**
     * @var integer
     */
    private $lastUpdateUid;
    
  

    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return hrm_expenses_transport_taxes
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set currencyIsoCode
     *
     * @param string $currencyIsoCode
     *
     * @return hrm_expenses_transport_taxes
     */
    public function setCurrencyIsoCode($currencyIsoCode)
    {
        $this->currencyIsoCode = $currencyIsoCode;

        return $this;
    }

    /**
     * Get currencyIsoCode
     *
     * @return string
     */
    public function getCurrencyIsoCode()
    {
        return $this->currencyIsoCode;
    }

    /**
     * Set amountBase
     *
     * @param float $amountBase
     *
     * @return hrm_expenses_transport_taxes
     */
    public function setAmountBase($amountBase)
    {
        $this->amountBase = $amountBase;

        return $this;
    }

    /**
     * Get amountBase
     *
     * @return float
     */
    public function getAmountBase()
    {
        return $this->amountBase;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return hrm_expenses_transport_taxes
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_expenses_transport_taxes
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_expenses_transport_taxes
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_expenses_transport_taxes
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_expenses_transport_taxes
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->lastUpdateUid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->lastUpdateUid;
    }

    /**
     * Set expense
     *
     * @param \ExpenseBundle\Entity\hrm_expense_transport $expense
     *
     * @return hrm_expenses_transport_taxes
     */
    public function setExpense(\ExpenseBundle\Entity\hrm_expense_transport $expense = null)
    {
        $this->expense = $expense;

        return $this;
    }

    /**
     * Get expense
     *
     * @return \ExpenseBundle\Entity\hrm_expense_transport
     */
    public function getExpense()
    {
        return $this->expense;
    }

    /**
     * Set tax
     *
     * @param \AdminBundle\Entity\hrm_settings_tax $tax
     *
     * @return hrm_expenses_transport_taxes
     */
    public function setTax(\AdminBundle\Entity\hrm_settings_tax $tax = null)
    {
        $this->tax = $tax;

        return $this;
    }

    /**
     * Get tax
     *
     * @return \AdminBundle\Entity\hrm_settings_tax
     */
    public function getTax()
    {
        return $this->tax;
    }
}
