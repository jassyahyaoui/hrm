<?php

namespace ExpenseBundle\Entity;

/**
 * hrm_expenses_accomodation
 */
class hrm_expenses_accomodation
{

    /**
     * @var integer
     */
    private $uid;

    /**
     * @var string
     */
    private $displayName;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var float
     */
    private $amountAcc;

    /**
     * @var float
     */
    private $untaxedAmount;

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * @var integer
     */
    private $createUid;

    /**
     * @var \DateTime
     */
    private $lastUpdate;

    /**
     * @var integer
     */
    private $lastUpdateUid;

    /**
     * @var \FileBundle\Entity\attachments
     */
    private $attachments;

    /**
     * @var \AdminBundle\Entity\SettingsProjects
     */
    private $settings_projects;

    /**
     * @var \AdminBundle\Entity\hrm_settings_exp_accommodation
     */
    private $settings_exp_accomodation;

    /**
     * @var \AdminBundle\Entity\SettingsStatus
     */
    private $settings_status;

    /**
     * @var \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    private $settings_pay_period;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $user;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $contributor;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $validator;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $requester;

    /**
     * @var float
     */
    private $amountAccBase;

    /**
     * @var string
     */
    private $currencyIsoCode;

    /**
     * @var \UserBundle\Entity\hrm_company_currency
     */
    private $company_currency;

    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return hrm_expenses_accomodation
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return hrm_expenses_accomodation
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return hrm_expenses_accomodation
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return hrm_expenses_accomodation
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set amountAcc
     *
     * @param float $amountAcc
     *
     * @return hrm_expenses_accomodation
     */
    public function setAmountAcc($amountAcc)
    {
        $this->amountAcc = $amountAcc;

        return $this;
    }

    /**
     * Get amountAcc
     *
     * @return float
     */
    public function getAmountAcc()
    {
        return $this->amountAcc;
    }

    /**
     * Set untaxedAmount
     *
     * @param float $untaxedAmount
     *
     * @return hrm_expenses_accomodation
     */
    public function setUntaxedAmount($untaxedAmount)
    {
        $this->untaxedAmount = $untaxedAmount;

        return $this;
    }

    /**
     * Get untaxedAmount
     *
     * @return float
     */
    public function getUntaxedAmount()
    {
        return $this->untaxedAmount;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_expenses_accomodation
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_expenses_accomodation
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_expenses_accomodation
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_expenses_accomodation
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->lastUpdateUid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->lastUpdateUid;
    }

    /**
     * Set attachments
     *
     * @param \FileBundle\Entity\attachments $attachments
     *
     * @return hrm_expenses_accomodation
     */
    public function setAttachments(\FileBundle\Entity\attachments $attachments = null)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Get attachments
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Set settingsProjects
     *
     * @param \AdminBundle\Entity\SettingsProjects $settingsProjects
     *
     * @return hrm_expenses_accomodation
     */
    public function setSettingsProjects(\AdminBundle\Entity\SettingsProjects $settingsProjects = null)
    {
        $this->settings_projects = $settingsProjects;

        return $this;
    }

    /**
     * Get settingsProjects
     *
     * @return \AdminBundle\Entity\SettingsProjects
     */
    public function getSettingsProjects()
    {
        return $this->settings_projects;
    }

    /**
     * Set settingsExpAccomodation
     *
     * @param \AdminBundle\Entity\hrm_settings_exp_accommodation $settingsExpAccomodation
     *
     * @return hrm_expenses_accomodation
     */
    public function setSettingsExpAccomodation(\AdminBundle\Entity\hrm_settings_exp_accommodation $settingsExpAccomodation = null)
    {
        $this->settings_exp_accomodation = $settingsExpAccomodation;

        return $this;
    }

    /**
     * Get settingsExpAccomodation
     *
     * @return \AdminBundle\Entity\hrm_settings_exp_accommodation
     */
    public function getSettingsExpAccomodation()
    {
        return $this->settings_exp_accomodation;
    }

    /**
     * Set settingsStatus
     *
     * @param \AdminBundle\Entity\SettingsStatus $settingsStatus
     *
     * @return hrm_expenses_accomodation
     */
    public function setSettingsStatus(\AdminBundle\Entity\SettingsStatus $settingsStatus = null)
    {
        $this->settings_status = $settingsStatus;

        return $this;
    }

    /**
     * Get settingsStatus
     *
     * @return \AdminBundle\Entity\SettingsStatus
     */
    public function getSettingsStatus()
    {
        return $this->settings_status;
    }

    /**
     * Set settingsPayPeriod
     *
     * @param \PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod
     *
     * @return hrm_expenses_accomodation
     */
    public function setSettingsPayPeriod(\PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod = null)
    {
        $this->settings_pay_period = $settingsPayPeriod;

        return $this;
    }

    /**
     * Get settingsPayPeriod
     *
     * @return \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    public function getSettingsPayPeriod()
    {
        return $this->settings_pay_period;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\HrmEmployee $user
     *
     * @return hrm_expenses_accomodation
     */
    public function setUser(\UserBundle\Entity\HrmEmployee $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set contributor
     *
     * @param \UserBundle\Entity\HrmEmployee $contributor
     *
     * @return hrm_expenses_accomodation
     */
    public function setContributor(\UserBundle\Entity\HrmEmployee $contributor = null)
    {
        $this->contributor = $contributor;

        return $this;
    }

    /**
     * Get contributor
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getContributor()
    {
        return $this->contributor;
    }

    /**
     * Set validator
     *
     * @param \UserBundle\Entity\HrmEmployee $validator
     *
     * @return hrm_expenses_accomodation
     */
    public function setValidator(\UserBundle\Entity\HrmEmployee $validator = null)
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * Get validator
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * Set requester
     *
     * @param \UserBundle\Entity\HrmEmployee $requester
     *
     * @return hrm_expenses_accomodation
     */
    public function setRequester(\UserBundle\Entity\HrmEmployee $requester = null)
    {
        $this->requester = $requester;

        return $this;
    }

    /**
     * Get requester
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getRequester()
    {
        return $this->requester;
    }


    /**
     * Set currencyIsoCode
     *
     * @param string $currencyIsoCode
     *
     * @return hrm_expenses_accomodation
     */
    public function setCurrencyIsoCode($currencyIsoCode)
    {
        $this->currencyIsoCode = $currencyIsoCode;

        return $this;
    }

    /**
     * Get currencyIsoCode
     *
     * @return string
     */
    public function getCurrencyIsoCode()
    {
        return $this->currencyIsoCode;
    }

    /**
     * Set companyCurrency
     *
     * @param \UserBundle\Entity\hrm_company_currency $companyCurrency
     *
     * @return hrm_expenses_accomodation
     */
    public function setCompanyCurrency(\UserBundle\Entity\hrm_company_currency $companyCurrency = null)
    {
        $this->company_currency = $companyCurrency;

        return $this;
    }

    /**
     * Get companyCurrency
     *
     * @return \UserBundle\Entity\hrm_company_currency
     */
    public function getCompanyCurrency()
    {
        return $this->company_currency;
    }


    /**
     * Set $amountAccBase
     *
     * @param float $amountAccBase
     *
     * @return hrm_expenses_accomodation
     */
    public function setAmountAccBase($amountAccBase)
    {
        $this->amountAccBase = $amountAccBase;

        return $this;
    }

    /**
     * Get amountAccBase
     *
     * @return float
     */
    public function getAmountAccBase()
    {
        return $this->amountAccBase;
    }


}
