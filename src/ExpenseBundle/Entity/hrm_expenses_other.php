<?php

namespace ExpenseBundle\Entity;

/**
 * hrm_expenses_other
 */
class hrm_expenses_other
{
    /**
     * @var integer
     */
    private $uid;

    /**
     * @var string
     */
    private $displayName;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var float
     */
    private $amountOth;

    /**
     * @var float
     */
    private $amountOthBase;

    /**
     * @var string
     */
    private $currencyIsoCode;

    /**
     * @var float
     */
    private $untaxedAmountOth;

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * @var integer
     */
    private $createUid;

    /**
     * @var \DateTime
     */
    private $lastUpdate;

    /**
     * @var integer
     */
    private $lastUpdateUid;

    /**
     * @var \FileBundle\Entity\attachments
     */
    private $attachments;

    /**
     * @var \AdminBundle\Entity\SettingsProjects
     */
    private $settings_projects;

    /**
     * @var \AdminBundle\Entity\hrm_settings_exp_other
     */
    private $settings_expenses;

    /**
     * @var \AdminBundle\Entity\SettingsStatus
     */
    private $settings_status;

    /**
     * @var \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    private $settings_pay_period;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $user;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $contributor;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $validator;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $requester;

    /**
     * @var \UserBundle\Entity\hrm_company_currency
     */
    private $company_currency;


    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return hrm_expenses_other
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return hrm_expenses_other
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return hrm_expenses_other
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return hrm_expenses_other
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set amountOth
     *
     * @param float $amountOth
     *
     * @return hrm_expenses_other
     */
    public function setAmountOth($amountOth)
    {
        $this->amountOth = $amountOth;

        return $this;
    }

    /**
     * Get amountOth
     *
     * @return float
     */
    public function getAmountOth()
    {
        return $this->amountOth;
    }

    /**
     * Set amountOthBase
     *
     * @param float $amountOthBase
     *
     * @return hrm_expenses_other
     */
    public function setAmountOthBase($amountOthBase)
    {
        $this->amountOthBase = $amountOthBase;

        return $this;
    }

    /**
     * Get amountOthBase
     *
     * @return float
     */
    public function getAmountOthBase()
    {
        return $this->amountOthBase;
    }

    /**
     * Set currencyIsoCode
     *
     * @param string $currencyIsoCode
     *
     * @return hrm_expenses_other
     */
    public function setCurrencyIsoCode($currencyIsoCode)
    {
        $this->currencyIsoCode = $currencyIsoCode;

        return $this;
    }

    /**
     * Get currencyIsoCode
     *
     * @return string
     */
    public function getCurrencyIsoCode()
    {
        return $this->currencyIsoCode;
    }

    /**
     * Set untaxedAmountOth
     *
     * @param float $untaxedAmountOth
     *
     * @return hrm_expenses_other
     */
    public function setUntaxedAmountOth($untaxedAmountOth)
    {
        $this->untaxedAmountOth = $untaxedAmountOth;

        return $this;
    }

    /**
     * Get untaxedAmountOth
     *
     * @return float
     */
    public function getUntaxedAmountOth()
    {
        return $this->untaxedAmountOth;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_expenses_other
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_expenses_other
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_expenses_other
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_expenses_other
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->lastUpdateUid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->lastUpdateUid;
    }

    /**
     * Set attachments
     *
     * @param \FileBundle\Entity\attachments $attachments
     *
     * @return hrm_expenses_other
     */
    public function setAttachments(\FileBundle\Entity\attachments $attachments = null)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Get attachments
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Set settingsProjects
     *
     * @param \AdminBundle\Entity\SettingsProjects $settingsProjects
     *
     * @return hrm_expenses_other
     */
    public function setSettingsProjects(\AdminBundle\Entity\SettingsProjects $settingsProjects = null)
    {
        $this->settings_projects = $settingsProjects;

        return $this;
    }

    /**
     * Get settingsProjects
     *
     * @return \AdminBundle\Entity\SettingsProjects
     */
    public function getSettingsProjects()
    {
        return $this->settings_projects;
    }

    /**
     * Set settingsExpenses
     *
     * @param \AdminBundle\Entity\hrm_settings_exp_other $settingsExpenses
     *
     * @return hrm_expenses_other
     */
    public function setSettingsExpenses(\AdminBundle\Entity\hrm_settings_exp_other $settingsExpenses = null)
    {
        $this->settings_expenses = $settingsExpenses;

        return $this;
    }

    /**
     * Get settingsExpenses
     *
     * @return \AdminBundle\Entity\hrm_settings_exp_other
     */
    public function getSettingsExpenses()
    {
        return $this->settings_expenses;
    }

    /**
     * Set settingsStatus
     *
     * @param \AdminBundle\Entity\SettingsStatus $settingsStatus
     *
     * @return hrm_expenses_other
     */
    public function setSettingsStatus(\AdminBundle\Entity\SettingsStatus $settingsStatus = null)
    {
        $this->settings_status = $settingsStatus;

        return $this;
    }

    /**
     * Get settingsStatus
     *
     * @return \AdminBundle\Entity\SettingsStatus
     */
    public function getSettingsStatus()
    {
        return $this->settings_status;
    }

    /**
     * Set settingsPayPeriod
     *
     * @param \PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod
     *
     * @return hrm_expenses_other
     */
    public function setSettingsPayPeriod(\PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod = null)
    {
        $this->settings_pay_period = $settingsPayPeriod;

        return $this;
    }

    /**
     * Get settingsPayPeriod
     *
     * @return \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    public function getSettingsPayPeriod()
    {
        return $this->settings_pay_period;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\HrmEmployee $user
     *
     * @return hrm_expenses_other
     */
    public function setUser(\UserBundle\Entity\HrmEmployee $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set contributor
     *
     * @param \UserBundle\Entity\HrmEmployee $contributor
     *
     * @return hrm_expenses_other
     */
    public function setContributor(\UserBundle\Entity\HrmEmployee $contributor = null)
    {
        $this->contributor = $contributor;

        return $this;
    }

    /**
     * Get contributor
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getContributor()
    {
        return $this->contributor;
    }

    /**
     * Set validator
     *
     * @param \UserBundle\Entity\HrmEmployee $validator
     *
     * @return hrm_expenses_other
     */
    public function setValidator(\UserBundle\Entity\HrmEmployee $validator = null)
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * Get validator
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * Set requester
     *
     * @param \UserBundle\Entity\HrmEmployee $requester
     *
     * @return hrm_expenses_other
     */
    public function setRequester(\UserBundle\Entity\HrmEmployee $requester = null)
    {
        $this->requester = $requester;

        return $this;
    }

    /**
     * Get requester
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getRequester()
    {
        return $this->requester;
    }

    /**
     * Set companyCurrency
     *
     * @param \UserBundle\Entity\hrm_company_currency $companyCurrency
     *
     * @return hrm_expenses_other
     */
    public function setCompanyCurrency(\UserBundle\Entity\hrm_company_currency $companyCurrency = null)
    {
        $this->company_currency = $companyCurrency;

        return $this;
    }

    /**
     * Get companyCurrency
     *
     * @return \UserBundle\Entity\hrm_company_currency
     */
    public function getCompanyCurrency()
    {
        return $this->company_currency;
    }
}
