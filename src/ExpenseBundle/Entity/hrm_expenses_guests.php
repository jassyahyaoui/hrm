<?php

namespace ExpenseBundle\Entity;

/**
 * hrm_expenses_guests
 */
class hrm_expenses_guests {

    /**
     * Set hrmExpensesDining
     *
     * @param \ExpenseBundle\Entity\hrm_expenses_dining $hrmExpensesDining
     *
     * @return hrm_expenses_guests
     */
    public function setHrmExpensesDining(\ExpenseBundle\Entity\hrm_expenses_dining $hrmExpensesDining = null) {
        if (!$hrmExpensesDining instanceof hrm_expenses_dining && $hrmExpensesDining !== null) {
            throw new \InvalidArgumentException('$hrmExpensesDining must be an instance of hrm_expenses_dining, or null');
        }
        $this->hrm_expenses_dining = $hrmExpensesDining;
        return $this;
    }

    /**
     * Get hrmExpensesDining
     *
     * @return \ExpenseBundle\Entity\hrm_expenses_dining
     */
    public function getHrmExpensesDining() {
        return $this->hrm_expenses_dining;
    }

    public function AddGuest($lastId, $field, $field2, $current_user_id, $em) {

        $guests = new hrm_expenses_guests();
        $guests->setHrmExpensesDining($em->getRepository('ExpenseBundle:hrm_expenses_dining')->find($lastId));
        $guests->setFirstName($field);
        $guests->setLastName($field2);
//$guests->setHrmExpensesDining()
//            
        //Add default value on create Action
        $guests->setCreateDate(new \DateTime('now'));
        $guests->setCreateUid($current_user_id);
        $guests->setLastUpdateUid($current_user_id);
        $guests->setLastUpdate(new \DateTime('now'));

        $em->persist($guests);
        $em->flush();
    }

    public function EditGuest($uid, $field, $field2, $em) {
        $qb = $em->createQueryBuilder();
        $q = $qb->update('ExpenseBundle\Entity\hrm_expenses_guests', 'g')
                ->set('g.firstName', ':field')
                ->set('g.lastName', ':field2')
                ->where('g.uid = :uid')
                ->setParameter('field', $field)
                ->setParameter('field2', $field2)
                ->setParameter('uid', $uid)
                ->getQuery();
        $ex = $q->execute();
    }

    /**
     * @var integer
     */
    private $uid;

    /**
     * @var string
     */
    private $firstName;

    /**
     * @var string
     */
    private $lastName;

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * @var integer
     */
    private $createUid;

    /**
     * @var \DateTime
     */
    private $lastUpdate;

    /**
     * @var integer
     */
    private $lastUpdateUid;

    /**
     * @var \ExpenseBundle\Entity\hrm_expenses_dining
     */
    private $hrm_expenses_dining;


        /**
     * @var \FileBundle\Entity\attachments
     */
    private $attachments;

    
    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return hrm_expenses_guests
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return hrm_expenses_guests
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_expenses_guests
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_expenses_guests
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_expenses_guests
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_expenses_guests
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->lastUpdateUid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->lastUpdateUid;
    }
       /**
     * Set attachments
     *
     * @param \FileBundle\Entity\attachments $attachments
     *
     * @return hrm_expense_transport
     */
    public function setAttachments(\FileBundle\Entity\attachments $attachments = null)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Get attachments
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getAttachments()
    {
        return $this->attachments;
    }
}
