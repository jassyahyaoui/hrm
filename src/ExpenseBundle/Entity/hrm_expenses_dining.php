<?php

namespace ExpenseBundle\Entity;

use Doctrine\Common\Collections\Collection;

/**
 * hrm_expenses_dining
 */
class hrm_expenses_dining {

    /**
     * @var integer
     */
    private $uid;

    /**
     * @var string
     */
    private $displayName;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var float
     */
    private $untaxedAmountDin;

    /**
     * @var float
     */
    private $amountDin;

    /**
     * @var \AdminBundle\Entity\hrm_settings_exp_dinning
     */
    private $settings_exp_dining;

    /**
     * @var string
     */
    private $collaborateur;

    /**
     * @var float
     */
    private $amountDinBase;

    /**
     * @var string
     */
    private $currencyIsoCode;

    /**
     * @var \UserBundle\Entity\hrm_company_currency
     */
    private $company_currency;

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * @var integer
     */
    private $createUid;

    /**
     * @var \DateTime
     */
    private $lastUpdate;

    /**
     * @var integer
     */
    private $lastUpdateUid;

    /**
     * @var \Doctrine\Common\Collections\Collection
     */
    private $hrm_expenses_guests;

    /**
     * @var \AdminBundle\Entity\SettingsProjects
     */
    private $settings_projects;

    /**
     * @var \AdminBundle\Entity\SettingsStatus
     */
    private $settings_status;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $user;

        /**
     * @var \FileBundle\Entity\attachments
     */
    private $attachments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->hrm_expenses_guests = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return hrm_expenses_dining
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return hrm_expenses_dining
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return hrm_expenses_dining
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return hrm_expenses_dining
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set untaxedAmountDin
     *
     * @param float $untaxedAmountDin
     *
     * @return hrm_expenses_dining
     */
    public function setUntaxedAmountDin($untaxedAmountDin)
    {
        $this->untaxedAmountDin = $untaxedAmountDin;

        return $this;
    }

    /**
     * Get untaxedAmountDin
     *
     * @return float
     */
    public function getUntaxedAmountDin()
    {
        return $this->untaxedAmountDin;
    }

    /**
     * Set amountDin
     *
     * @param float $amountDin
     *
     * @return hrm_expenses_dining
     */
    public function setAmountDin($amountDin)
    {
        $this->amountDin = $amountDin;

        return $this;
    }

    /**
     * Get amountDin
     *
     * @return float
     */
    public function getAmountDin()
    {
        return $this->amountDin;
    }

    /**
     * Set collaborateur
     *
     * @param string $collaborateur
     *
     * @return hrm_expenses_dining
     */
    public function setCollaborateur($collaborateur)
    {
        $this->collaborateur = $collaborateur;

        return $this;
    }

    /**
     * Get collaborateur
     *
     * @return string
     */
    public function getCollaborateur()
    {
        return $this->collaborateur;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_expenses_dining
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_expenses_dining
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_expenses_dining
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_expenses_dining
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->lastUpdateUid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->lastUpdateUid;
    }

    /**
     * Add hrmExpensesGuest
     *
     * @param \ExpenseBundle\Entity\hrm_expenses_guests $hrmExpensesGuest
     *
     * @return hrm_expenses_dining
     */
    public function addHrmExpensesGuest(\ExpenseBundle\Entity\hrm_expenses_guests $hrmExpensesGuest)
    {
        $this->hrm_expenses_guests[] = $hrmExpensesGuest;

        return $this;
    }

    /**
     * Remove hrmExpensesGuest
     *
     * @param \ExpenseBundle\Entity\hrm_expenses_guests $hrmExpensesGuest
     */
    public function removeHrmExpensesGuest(\ExpenseBundle\Entity\hrm_expenses_guests $hrmExpensesGuest)
    {
        $this->hrm_expenses_guests->removeElement($hrmExpensesGuest);
    }

    /**
     * Get hrmExpensesGuests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHrmExpensesGuests()
    {
        return $this->hrm_expenses_guests;
    }

    /**
     * Set settingsProjects
     *
     * @param \AdminBundle\Entity\SettingsProjects $settingsProjects
     *
     * @return hrm_expenses_dining
     */
    public function setSettingsProjects(\AdminBundle\Entity\SettingsProjects $settingsProjects = null)
    {
        $this->settings_projects = $settingsProjects;

        return $this;
    }

    /**
     * Get settingsProjects
     *
     * @return \AdminBundle\Entity\SettingsProjects
     */
    public function getSettingsProjects()
    {
        return $this->settings_projects;
    }

    /**
     * Set settingsStatus
     *
     * @param \AdminBundle\Entity\SettingsStatus $settingsStatus
     *
     * @return hrm_expenses_dining
     */
    public function setSettingsStatus(\AdminBundle\Entity\SettingsStatus $settingsStatus = null)
    {
        $this->settings_status = $settingsStatus;

        return $this;
    }

    /**
     * Get settingsStatus
     *
     * @return \AdminBundle\Entity\SettingsStatus
     */
    public function getSettingsStatus()
    {
        return $this->settings_status;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\HrmEmployee $user
     *
     * @return hrm_expenses_dining
     */
    public function setUser(\UserBundle\Entity\HrmEmployee $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Set attachments
     *
     * @param \FileBundle\Entity\attachments $attachments
     *
     * @return hrm_expense_transport
     */
    public function setAttachments(\FileBundle\Entity\attachments $attachments = null)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Get attachments
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getAttachments()
    {
        return $this->attachments;
    }
    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $contributor;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $validator;


    /**
     * Set contributor
     *
     * @param \UserBundle\Entity\HrmEmployee $contributor
     *
     * @return hrm_expenses_dining
     */
    public function setContributor(\UserBundle\Entity\HrmEmployee $contributor = null)
    {
        $this->contributor = $contributor;

        return $this;
    }

    /**
     * Get contributor
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getContributor()
    {
        return $this->contributor;
    }

    /**
     * Set validator
     *
     * @param \UserBundle\Entity\HrmEmployee $validator
     *
     * @return hrm_expenses_dining
     */
    public function setValidator(\UserBundle\Entity\HrmEmployee $validator = null)
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * Get validator
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getValidator()
    {
        return $this->validator;
    }


    /**
     * @var string
     */
    private $requester;


    /**
     * Set requester
     *
     * @param string $requester
     *
     * @return hrm_expenses_dining
     */
    public function setRequester($requester)
    {
        $this->requester = $requester;

        return $this;
    }

    /**
     * Get requester
     *
     * @return string
     */
    public function getRequester()
    {
        return $this->requester;
    }
    /**
     * @var \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    private $settings_pay_period;


    /**
     * Set settingsPayPeriod
     *
     * @param \PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod
     *
     * @return hrm_expenses_dining
     */
    public function setSettingsPayPeriod(\PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod = null)
    {
        $this->settings_pay_period = $settingsPayPeriod;

        return $this;
    }

    /**
     * Get settingsPayPeriod
     *
     * @return \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    public function getSettingsPayPeriod()
    {
        return $this->settings_pay_period;
    }

    /**
     * Set settingsExpDining
     *
     * @param \AdminBundle\Entity\hrm_settings_exp_dinning $settingsExpDining
     *
     * @return hrm_expenses_dining
     */
    public function setSettingsExpDining(\AdminBundle\Entity\hrm_settings_exp_dinning $settingsExpDining = null)
    {
        $this->settings_exp_dining = $settingsExpDining;

        return $this;
    }

    /**
     * Get settingsExpDining
     *
     * @return \AdminBundle\Entity\hrm_settings_exp_dinning
     */
    public function getSettingsExpDining()
    {
        return $this->settings_exp_dining;
    }

    /**
     * Set amountDinBase
     *
     * @param float $amountDinBase
     *
     * @return hrm_expenses_dining
     */
    public function setAmountDinBase($amountDinBase)
    {
        $this->amountDinBase = $amountDinBase;

        return $this;
    }

    /**
     * Get amountDinBase
     *
     * @return float
     */
    public function getAmountDinBase()
    {
        return $this->amountDinBase;
    }

    /**
     * Set currencyIsoCode
     *
     * @param string $currencyIsoCode
     *
     * @return hrm_expenses_dining
     */
    public function setCurrencyIsoCode($currencyIsoCode)
    {
        $this->currencyIsoCode = $currencyIsoCode;

        return $this;
    }

    /**
     * Get currencyIsoCode
     *
     * @return string
     */
    public function getCurrencyIsoCode()
    {
        return $this->currencyIsoCode;
    }

    /**
     * Set companyCurrency
     *
     * @param \UserBundle\Entity\hrm_company_currency $companyCurrency
     *
     * @return hrm_expenses_dining
     */
    public function setCompanyCurrency(\UserBundle\Entity\hrm_company_currency $companyCurrency = null)
    {
        $this->company_currency = $companyCurrency;

        return $this;
    }

    /**
     * Get companyCurrency
     *
     * @return \UserBundle\Entity\hrm_company_currency
     */
    public function getCompanyCurrency()
    {
        return $this->company_currency;
    }
}
