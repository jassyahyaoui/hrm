<?php

namespace ExpenseBundle\Model;

use ExpenseBundle\Entity\hrm_expenses_transport_taxes;
use ExpenseBundle\Entity\hrm_expenses_dining_taxes;
use ExpenseBundle\Entity\hrm_expenses_accomodation_taxes;
use ExpenseBundle\Entity\hrm_expenses_other_taxes;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GenerateTaxModelInputs
 *
 * @author MAZ_USER2
 */
class GenerateTaxModelInputs
{
    //put your code here
    private $container;

    /**
     * GenerateTaxModelInputs constructor.
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    public function createFormInputs($em, $type, $nature, $expense_id, $action)
    {

        $entity = $this->loadRepository($em, $type, $nature);
        if (COUNT($entity) != 0) {
            return $this->createModelFromEntity($em, $entity, $nature, $type, $expense_id, $action);
        }
        return null;
    }

    private function loadRepository($em, $type, $nature)
    {
        if ($type == 'Transport')
            return $em->getRepository('AdminBundle:hrm_settings_exp_tax_model')->findTaxByNatureId($nature, 'hrm_settings_exp_transport');
        if ($type == 'Hebergement')
            return $em->getRepository('AdminBundle:hrm_settings_exp_tax_model')->findTaxByNatureId($nature, 'hrm_settings_exp_accommodation');
        if ($type == 'Restauration')
            return $em->getRepository('AdminBundle:hrm_settings_exp_tax_model')->findTaxByNatureId($nature, 'hrm_settings_exp_dinning');
        if ($type == 'Autres')
            return $em->getRepository('AdminBundle:hrm_settings_exp_tax_model')->findTaxByNatureId($nature, 'hrm_settings_exp_other');

        return NULL;
    }

    private function createModelFromEntity($em, $entity, $nature, $type, $expense_id, $action)
    { 
        $preference = $this->container->get('settingsbundle.preference.service')->getEmpData()->getPreference()->getFormat();

        $str = "";
        $data = array("data" => "", "scope" => array("tax"=>array(), "rate"=>array()));
        $scope_action = "new";
        foreach ($entity as $key => $value) {
            if ($value['tax_model'] !== null) {

                foreach ($value['tax_model']['tax'] as $k => $v) {
                    if (($expense_id !== "null") && (!empty($this->getObjectSubExpense($em, $type, $expense_id, $nature)))) {
                        $scope_action = "edit";
                        $ret = $this->getModelTaxGroupsInputValues($em, $v['id'], $expense_id, $type);
                        if (!empty($ret))
                        {
                            $valInput = $ret[0]['amount'];
                            $rateInput = $ret[0]['amountBase'];
                        }
                        else
                        {
                            $valInput = 0;
                            $rateInput = 0;
                        }
                    } else {
                        $valInput = 0;
                        $rateInput = 0;
                    }


                    if($action == 'show')
                    {
                        $input = "<p class='form-control ng-binding'>{[{".$valInput."|number:2}]}</p>"
                            . "<p class='form-control ng-binding'><b>Montant de base : {[{".$rateInput."|number:2}]} Euros</b></p> ";
                        $asterix = '';
                    }
                    else {
                        $input = "<input type='text'  preference='" . $preference . "' id='expensebundle_".$type."_tva_".$nature."_".$v['rate']."' name='expensebundle_".$type."[".$v['rate']."]'"
                            . "value ='".$valInput."'"
                            . " required='required' class='form-control ng-pristine ng-empty ng-invalid ng-invalid-required ng-valid-pattern ng-touched' "
                            . "ng-model='".$scope_action.".tax_list.tax.tva_".$v['id']."_".$v['rate']."'>"
                            . "<span ng-if='currencies.result.length>0'>Montant de base : {[{".$scope_action . ".tax_list.amount_rate.tva_" . $v['id'] . "_" . $v['rate']."=" . $scope_action . ".tax_list.tax.tva_" . $v['id'] . "_" . $v['rate'] . " * ".$scope_action.".taxRate|number:2 || '0'}]} Euros</span> ";
                        $asterix = 'asterix';
                    }
                    $data['scope']['tax'] =  array_merge($data['scope']['tax'],  array("tva_".$v['id']."_".$v['rate'] => $valInput));
                    $data['scope']['rate'] = array_merge($data['scope']['rate'], array("rate_" . $v['id'] . "_" . $v['rate'] => $rateInput));

                    $data['data'] .= "<div class='space-4'></div>
                            <div class='form-group'>
                                <label class='col-sm-3 control-label no-padding-right ".$asterix."' required for='expensebundle_".$type."_tva_".$v['rate']."'>"
                        . "{[{'TAXES' | translate}]} (".$v['rate']."%)</label>

                                <div class='col-sm-8'>
                                 ".$input."

                                </div>
                        </div>";
                }
            }
        }
        return $data;

    }

    public function saveExpensesTaxes($em, $expense, $tax_list, $entity_to_load, $user_id, $action = 'save')
    {
        if ($action == 'update') {
            $this->DeleteAllExpensesTaxes($em, $entity_to_load, $expense->getUid());
        }
        foreach ($tax_list['tax_list']['tax'] as $key => $value) {
            $taxId = explode('_', $key);
            $entity = $this->getClass($entity_to_load);
            $entity->setExpense($expense);
            $entity->setAmount($value);
            if(!isset($tax_list['tax_list']['amount_rate'][$key])){
                $entity->setAmountBase($value);
            }else{
                $entity->setAmountBase($tax_list['tax_list']['amount_rate'][$key]);
            }
            if(!isset($tax_list['currencyIsoCode'])){
                $entity->setCurrencyIsoCode("EUR - Euro");
            }else{
                $entity->setCurrencyIsoCode($tax_list['currencyIsoCode']);
            }
            $entity->setCreateDate(new \DateTime('now'));
            $entity->setLastUpdate(new \DateTime('now'));
            $entity->setCreateUid($user_id);
            $entity->setLastUpdateUid($user_id);
            $entity->setTax($em->getRepository('AdminBundle:hrm_settings_tax')->find((int)$taxId[1]));
            $em->persist($entity);
            unset($entity);
        }
        $em->flush();
    }


    private function getClass($c)
    {
        if ($c == 'transport')
            return new hrm_expenses_transport_taxes();
        if ($c == 'accomodation')
            return new hrm_expenses_accomodation_taxes();
        if ($c == 'dining')
            return new hrm_expenses_dining_taxes();
        if ($c == 'other')
            return new hrm_expenses_other_taxes();
    }

    private function getEntityRepository($em, $e)
    {
        if ($e == 'transport')
            return $em->getRepository('ExpenseBundle:hrm_expenses_transport_taxes');
        if ($e == 'accomodation')
            return $em->getRepository('ExpenseBundle:hrm_expenses_accomodation_taxes');
        if ($e == 'dining')
            return $em->getRepository('ExpenseBundle:hrm_expenses_dining_taxes');
        if ($e == 'other')
            return $em->getRepository('ExpenseBundle:hrm_expenses_other_taxes');
    }

    private function getModelTaxGroupsInputValues($em, $tax_id, $expense_id, $type)
    {

        if ($type == 'Transport')
            return $em->getRepository('AdminBundle:hrm_settings_exp_tax_model')->findTaxValueByNatureId($tax_id, (int)$expense_id, 'hrm_expenses_transport_taxes');
        if ($type == 'Hebergement')
            return $em->getRepository('AdminBundle:hrm_settings_exp_tax_model')->findTaxValueByNatureId($tax_id, (int)$expense_id, 'hrm_expenses_accomodation_taxes');
        if ($type == 'Restauration')
            return $em->getRepository('AdminBundle:hrm_settings_exp_tax_model')->findTaxValueByNatureId($tax_id, (int)$expense_id, 'hrm_expenses_dining_taxes');
        if ($type == 'Autres')
            return $em->getRepository('AdminBundle:hrm_settings_exp_tax_model')->findTaxValueByNatureId($tax_id, (int)$expense_id, 'hrm_expenses_other_taxes');

        return NULL;
    }

    private function getObjectSubExpense($em, $type, $expense_id, $nature)
    {

        if ($type == 'Transport')
            return $em->getRepository('AdminBundle:hrm_settings_exp_tax_model')->findObjectSubExpense((int)$expense_id, $nature, 'hrm_expense_transport', 'settings_exp_transport');
        if ($type == 'Hebergement')
            return $em->getRepository('AdminBundle:hrm_settings_exp_tax_model')->findObjectSubExpense((int)$expense_id, $nature, 'hrm_expenses_accomodation', 'settings_exp_accomodation');
        if ($type == 'Restauration')
            return $em->getRepository('AdminBundle:hrm_settings_exp_tax_model')->findObjectSubExpense((int)$expense_id, $nature, 'hrm_expenses_dining', 'settings_exp_dining');
        if ($type == 'Autres')
            return $em->getRepository('AdminBundle:hrm_settings_exp_tax_model')->findObjectSubExpense((int)$expense_id, $nature, 'hrm_expenses_other', 'settings_expenses');

        return NULL;
    }

    public function DeleteAllExpensesTaxes($em, $e, $entity_id)
    {
        $list_obj = $this->getEntityRepository($em, $e)->findBy(array('expense' => $entity_id));

        if ($list_obj) {
            foreach ($list_obj as $obj) {
                $em->remove($obj);
            }
            $em->flush();
        }
    }
}
