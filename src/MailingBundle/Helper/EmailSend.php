<?php

namespace MailingBundle\Helper;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use MailingBundle\Helper\EmailHelper as Mailhelper;
use SettingsBundle\DependencyInjection\SettingsPreferences as Preference;
use Symfony\Component\DependencyInjection\ContainerInterface;


class EmailSend
{
    private $em;
    private $token_storage;
    private $Mailhelper;
    private $Preference;
    private $templating;

    public function __construct(EntityManager $entityManager, TokenStorage $token_storage, Mailhelper $Mailhelper, Preference $Preference, $templating)
    {
        $this->em = $entityManager;
        $this->token_storage = $token_storage;
        $this->Mailhelper = $Mailhelper;
        $this->Preference = $Preference;
        $this->templating = $templating;
    }

    public function TagsTranslateBenefitCar($tag)
    {
        $viewName = 'email';
        $folder = 'BenefitCar';
        if ($this->Preference->getLocal() == 'fr') {
            $titleNew = 'Nouvelle demande d\'avantage en nature';
            $titleUpdate = 'Modification d\'avantage en nature';
            $etatNew = 'déposé';
            $etatUpdate = 'modifié';
            $confirmMsg = 'Confirmation d\'une demande d\'avantage en nature';
            $confirmEtat = 'validé';
            $cancelMsg = 'Refus d\'une demande d\'avantage en nature';
            $cancelEtat = 'refusé';
            $typeRequest = 'une demande d\'avantage en nature';
            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        } else if ($this->Preference->getLocal() == 'en') {
            $titleNew = 'New Benefit request';
            $titleUpdate = 'Updated Benefit';
            $etatNew = 'deposit';
            $etatUpdate = 'updated';
            $confirmMsg = 'Confirmation Benefit request';
            $confirmEtat = 'validated';
            $cancelMsg = 'Rejcted Benefit request';
            $cancelEtat = 'rejected';
            $typeRequest = 'Benefit request';

            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        }
        return $TagsArray[$tag];
    }

    public function TagsTranslateBenefit($tag)
    {
        $viewName = 'email';
        $folder = 'Benefit';
        if ($this->Preference->getLocal() == 'fr') {
            $titleNew = 'Nouvelle demande d\'avantage en nature';
            $titleUpdate = 'Modification d\'avantage en nature';
            $etatNew = 'déposé';
            $etatUpdate = 'modifié';
            $confirmMsg = 'Confirmation d\'une demande d\'avantage en nature';
            $confirmEtat = 'validé';
            $cancelMsg = 'Refus d\'une demande d\'avantage en nature';
            $cancelEtat = 'refusé';
            $typeRequest = 'une demande d\'avantage en nature';
            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        } else if ($this->Preference->getLocal() == 'en') {
            $titleNew = 'New Benefit request';
            $titleUpdate = 'Updated Benefit';
            $etatNew = 'deposit';
            $etatUpdate = 'updated';
            $confirmMsg = 'Confirmation Benefit request';
            $confirmEtat = 'validated';
            $cancelMsg = 'Rejcted Benefit request';
            $cancelEtat = 'rejected';
            $typeRequest = 'Benefit request';

            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        }
        return $TagsArray[$tag];
    }

    public function TagsTranslateExtraHour($tag)
    {
        $viewName = 'email';
        $folder = 'ExtraHour';
        if ($this->Preference->getLocal() == 'fr') {
            $titleNew = 'Nouveau heure supplémentaire';
            $titleUpdate = 'Modification heure supplémentaire';
            $etatNew = 'déposé';
            $etatUpdate = 'modifié';
            $confirmMsg = 'Confirmation d\'une demande d\'heure supplémentaire';
            $confirmEtat = 'validé';
            $cancelMsg = 'Refus d\'une demande d\'heure supplémentaire';
            $cancelEtat = 'refusé';
            $typeRequest = 'une heure supplémentaire';
            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        } else if ($this->Preference->getLocal() == 'en') {
            $titleNew = 'New extra hour';
            $titleUpdate = 'Updated extra hour';
            $etatNew = 'deposit';
            $etatUpdate = 'updated';
            $confirmMsg = 'Confirmation extra hour';
            $confirmEtat = 'validated';
            $cancelMsg = 'Rejcted extra hour';
            $cancelEtat = 'rejected';
            $typeRequest = 'Extra hour';

            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        }
        return $TagsArray[$tag];
    }

    public function TagsTranslateTraining($tag)
    {
        $viewName = 'email';
        $folder = 'Training';
        if ($this->Preference->getLocal() == 'fr') {
            $titleNew = 'Nouvelle demande de formation';
            $titleUpdate = 'Modification de formation';
            $etatNew = 'déposé';
            $etatUpdate = 'modifié';
            $confirmMsg = 'Confirmation d\'une demande de formation';
            $confirmEtat = 'validé';
            $cancelMsg = 'Refus d\'une demande de formation';
            $cancelEtat = 'refusé';
            $typeRequest = 'une demande de formation';
            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        } else if ($this->Preference->getLocal() == 'en') {
            $titleNew = 'New training request';
            $titleUpdate = 'Updated Training';
            $etatNew = 'deposit';
            $etatUpdate = 'updated';
            $confirmMsg = 'Confirmation Training request';
            $confirmEtat = 'validated';
            $cancelMsg = 'Rejcted Training request';
            $cancelEtat = 'rejected';
            $typeRequest = 'Training request';

            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        }
        return $TagsArray[$tag];
    }

    public function TagsTranslateBonus($tag)
    {
        $viewName = 'email';
        $folder = 'Bonus';
        if ($this->Preference->getLocal() == 'fr') {
            $titleNew = 'Nouvelle demande de prime';
            $titleUpdate = 'Modification de prime';
            $etatNew = 'déposé';
            $etatUpdate = 'modifié';
            $confirmMsg = 'Confirmation d\'une demande de prime';
            $confirmEtat = 'validé';
            $cancelMsg = 'Refus d\'une demande de prime';
            $cancelEtat = 'refusé';
            $typeRequest = 'une demande de prime';
            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        } else if ($this->Preference->getLocal() == 'en') {
            $titleNew = 'New bonus request';
            $titleUpdate = 'Updated Bonus';
            $etatNew = 'deposit';
            $etatUpdate = 'updated';
            $confirmMsg = 'Confirmation Bonus request';
            $confirmEtat = 'validated';
            $cancelMsg = 'Rejcted Bonus request';
            $cancelEtat = 'rejected';
            $typeRequest = 'Bonus request';

            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        }
        return $TagsArray[$tag];
    }

    public function TagsTranslateTransport($tag)
    {
        $viewName = 'email';
        $folder = 'Transport';
        if ($this->Preference->getLocal() == 'fr') {
            $titleNew = 'Nouvelle demande de transport';
            $titleUpdate = 'Modification transport';
            $etatNew = 'déposé';
            $etatUpdate = 'modifié';
            $confirmMsg = 'Confirmation d\'une demande de transport';
            $confirmEtat = 'validé';
            $cancelMsg = 'Refus d\'une demande de transport';
            $cancelEtat = 'refusé';
            $typeRequest = 'une demande de transport';
            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        } else if ($this->Preference->getLocal() == 'en') {
            $titleNew = 'New transport request';
            $titleUpdate = 'Updated Transport';
            $etatNew = 'deposit';
            $etatUpdate = 'updated';
            $confirmMsg = 'Confirmation Transport request';
            $confirmEtat = 'validated';
            $cancelMsg = 'Rejcted Transport request';
            $cancelEtat = 'rejected';
            $typeRequest = 'Transport request';

            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        }
        return $TagsArray[$tag];
    }

    public function TagsTranslatePayDay($tag)
    {
        $viewName = 'email';
        $folder = 'Payday';
        if ($this->Preference->getLocal() == 'fr') {
            $titleNew = 'Nouveau intervention sur salaire';
            $titleUpdate = 'Modification intervention sur salaire';
            $etatNew = 'déposé';
            $etatUpdate = 'modifié';
            $confirmMsg = 'Confirmation d\'une demande d\'intervention sur salaire';
            $confirmEtat = 'validé';
            $cancelMsg = 'Refus d\'une demande d\'intervention sur salaire';
            $cancelEtat = 'refusé';
            $typeRequest = 'une intervention sur salaire';
            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        } else if ($this->Preference->getLocal() == 'en') {
            $titleNew = 'New Pay day Variation';
            $titleUpdate = 'Updated Pay day Variation';
            $etatNew = 'deposit';
            $etatUpdate = 'updated';
            $confirmMsg = 'Confirmation Pay day Variation request';
            $confirmEtat = 'validated';
            $cancelMsg = 'Rejcted Pay day Variation request';
            $cancelEtat = 'rejected';
            $typeRequest = 'Pay day variations';

            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        }
        return $TagsArray[$tag];
    }

    public function TagsTranslateExpense($tag)
    {
        $viewName = 'email';
        $folder = 'Html';
        if ($this->Preference->getLocal() == 'fr') {
            $titleNew = 'Nouvelle demande';
            $etatNew = 'déposé';
            $confirmMsg = 'Confirmation d\'une demande de frais';
            $confirmEtat = 'validé';
            $cancelMsg = 'Refus d\'une demande de frais';
            $cancelEtat = 'refusé';
            $typeRequest = 'une demande de frais';

            $TagsArray = array(
                'titleNew' => $titleNew,
                'etatNew' => $etatNew,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        } else if ($this->Preference->getLocal() == 'en') {
            $titleNew = 'New request';
            $etatNew = 'deposit';
            $confirmMsg = 'Confirmation request expense';
            $confirmEtat = 'validated';
            $cancelMsg = 'Rejcted request expense';
            $cancelEtat = 'rejected';
            $typeRequest = 'request expense';

            $TagsArray = array(
                'titleNew' => $titleNew,
                'etatNew' => $etatNew,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        }
        return $TagsArray[$tag];
    }

    public function SendBonusMailToResponsable($title, $etat, $entity, $validator_entity, $amount)
    {
        //get current connected user
        $current_user_id = $this->token_storage->getToken()->getUser()->getId();
        $infos_user = $this->em->getRepository('UserBundle:hrm_user')->find($current_user_id);
//        $company_id = $infos_user->getCompanyId();
        $company_id = $infos_user->getEmployee()->first()->getCompany()->getId();

        //valideur informations
        $mail_to = $validator_entity->getUser()->getEmail();
        $validator_FN = $validator_entity->getUser()->getFirstName();
        $validator_LN = $validator_entity->getUser()->getLastName();
        //  return ($entity->getEmployee());
        //return ($this->container->get('settingsbundle.preference.service')->getEmpData());
        //requester informations
        $requester_FN = $entity->getContributor()->getUser()->getFirstName();
        $requester_LN = $entity->getContributor()->getUser()->getLastName();
        $date = $entity->getDate()->format('d/m/Y');
//        $projet = $entity->getSettingsProjects()->getDisplayName();
        $bonusType = $entity->getSettingsBonusType()->getDisplayName();
        $bonusName = $entity->getSettingsBonusType()->getDisplayName();

        //contributor informations
        $contributor_FN = $entity->getContributor()->getUser()->getFirstName();
        $contributor_LN = $entity->getContributor()->getUser()->getLastName();
        $contributor_username = $entity->getContributor()->getUser()->getUsername();

        $local = $this->Preference->getLocal();
        $Renderview = 'MailingBundle:bonus:bonusemailToResponsable_' . $local . '.html.twig';
        $redirect_uri = $this->Preference->getEmpData()->getProvider()->getPortalReturnUrl();

        $html = $this->templating->render($Renderview, array(
            'requester_FN' => $requester_FN,
            'requester_LN' => $requester_LN,
            'contributor_FN' => $contributor_FN,
            'contributor_LN' => $contributor_LN,
            'contributor_username' => $contributor_username,
            'date' => $date,
//            'projet' => $projet,
            'bonusType' => $bonusType,
            'bonusName' => $bonusName,
            'validator_FN' => $validator_FN,
            'validator_LN' => $validator_LN,
            'etat' => $etat,
            'amount' => $amount,
            'title' => $title,
            'redirect_uri' => $redirect_uri,
        ));
        return $this->Mailhelper->sendEmail($mail_to, $html, $title);
    }

    public function SendMailToResponsable($title, $etat, $entity, $validator_entity, $amount)
    {
        //get current connected user
        $current_user_id = $this->token_storage->getToken()->getUser()->getId();
        $infos_user = $this->em->getRepository('UserBundle:hrm_user')->find($current_user_id);
//        $company_id = $infos_user->getCompanyId();
        $company_id = $infos_user->getEmployee()->first()->getCompany()->getId();

        //valideur informations
        $mail_to = $validator_entity->getEmail();
        $validator_FN = $validator_entity->getFirstName();
        $validator_LN = $validator_entity->getLastName();

        //requester informations
        $requester_FN = $entity->getUser()->getFirstName();
        $requester_LN = $entity->getUser()->getLastName();
        $date = $entity->getDate()->format('d/m/Y');
//        $projet = $entity->getSettingsProjects()->getDisplayName();
        $type = $entity->getType();

        //contributor informations
        $contributor_FN = $entity->getContributor()->getFirstName();
        $contributor_LN = $entity->getContributor()->getLastName();
        $contributor_username = $entity->getContributor()->getUsername();

        $local = $this->Preference->getLocal();
        $Renderview = 'MailingBundle:Html:emailToResponsable_' . $local . '.html.twig';

        $html = $this->templating->render($Renderview, array(
            'requester_FN' => $requester_FN,
            'requester_LN' => $requester_LN,
            'contributor_FN' => $contributor_FN,
            'contributor_LN' => $contributor_LN,
            'contributor_username' => $contributor_username,
            'date' => $date,
//            'projet' => $projet,
            'type' => $type,
            'validator_FN' => $validator_FN,
            'validator_LN' => $validator_LN,
            'etat' => $etat,
            'amount' => $amount,
            'title' => $title
        ));
        return $this->Mailhelper->sendEmail($mail_to, $html, $title);
    }

    public function SendMail($entity, $title, $etat, $amount)
    {
        $mail_to = $entity->getContributor()->getEmail();
        $to_FName = $entity->getContributor()->getFirstName();
        $to_LName = $entity->getContributor()->getLastName();
//        $projet = $entity->getSettingsProjects()->getDisplayName();
        $type = $entity->getType();
        $validator_FName = $this->token_storage->getToken()->getUser()->getFirstName();
        $validator_LName = $this->token_storage->getToken()->getUser()->getLastName();

        $expense_date = $entity->getDate()->format('d/m/Y');

//        var_dump($to_FName,$to_LName,$expense_date,$etat,$validator_FName,$validator_LName,$amount,$projet,$type,$title);
//        die();
        $local = $this->Preference->getLocal();
        $html = $this->templating->render('MailingBundle:Html:email_' . $local . '.html.twig', array(
                'to_FName' => $to_FName,
                'to_LName' => $to_LName,
                'expense_date' => $expense_date,
                'etat' => $etat,
                'validator_FName' => $validator_FName,
                'validator_LName' => $validator_LName,
                'amount' => $amount,
//            'projet' => $projet,
                'type' => $type,
                'title' => $title
            )
        );

        $this->Mailhelper->sendEmail($mail_to, $html, $title, $etat);
    }

    public function Notify($entity, $ActionByRole, $title, $etat, $typeRequest, $folder, $viewName)
    {

        if ($entity->getValidator()->getId() != $entity->getContributor()->getId()) {

            if ($ActionByRole == 'RESPONSABLE') {
                $mail_to = $entity->getContributor()->getUser()->getEmail();
            } else if ($ActionByRole == 'COLLABORATEUR') {
                $mail_to = $entity->getValidator()->getUser()->getEmail();
            }
            $local = $this->Preference->getLocal();
            $redirect_uri = $this->Preference->getEmpData()->getCompany()->getProvider()->getPortalReturnUrl();
            $html = $this->templating->render('MailingBundle:' . $folder . ':' . $viewName . '_' . $local . '.html.twig', array(
                    'role' => $ActionByRole,
                    'entity' => $entity,
                    'etat' => $etat,
                    'title' => $title,
                    'typeRequest' => $typeRequest,
                    'redirect_uri' => $redirect_uri,
                )
            );
            $this->Mailhelper->sendEmail($mail_to, $html, $title, $etat);
        }
    }
    
     public function Notify2($entity, $ActionByRole, $title, $etat, $typeRequest, $folder, $viewName)
    {

        if ($entity->getManager()->getId() != $entity->getRequester()->getId()) {

            if ($ActionByRole == 'RESPONSABLE') {
                $mail_to = $entity->getRequester()->getUser()->getEmail();
            } else if ($ActionByRole == 'COLLABORATEUR') {
                $mail_to = $entity->getManager()->getUser()->getEmail();
            }
            $local = $this->Preference->getLocal();
            $redirect_uri = $this->Preference->getEmpData()->getCompany()->getProvider()->getPortalReturnUrl();
            $html = $this->templating->render('MailingBundle:' . $folder . ':' . $viewName . '_' . $local . '.html.twig', array(
                    'role' => $ActionByRole,
                    'entity' => $entity,
                    'etat' => $etat,
                    'title' => $title,
                    'typeRequest' => $typeRequest,
                    'redirect_uri' => $redirect_uri,
                )
            );
            $this->Mailhelper->sendEmail($mail_to, $html, $title, $etat);
        }
    }
    
     public function TagsTranslateRequestAbsence($tag)
    {
        $viewName = 'email';
        $folder = 'RequestAbsence';
        if ($this->Preference->getLocal() == 'fr') {
            $titleNew = 'Nouvelle demande d\'absence';
            $titleUpdate = 'Modification de demande d\'absence';
            $etatNew = 'déposé';
            $etatUpdate = 'modifié';
            $confirmMsg = 'Confirmation d\'une demande d\'absence';
            $confirmEtat = 'validé';
            $cancelMsg = 'Refus d\'une demande d\'absence';
            $cancelEtat = 'refusé';
            $typeRequest = 'une demande d\'absence';
            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        } else if ($this->Preference->getLocal() == 'en') {
            $titleNew = 'New Absence request';
            $titleUpdate = 'Updated Absence';
            $etatNew = 'deposit';
            $etatUpdate = 'updated';
            $confirmMsg = 'Confirmation absence request';
            $confirmEtat = 'validated';
            $cancelMsg = 'Rejcted absence request';
            $cancelEtat = 'rejected';
            $typeRequest = 'absence request';

            $TagsArray = array(
                'titleNew' => $titleNew,
                'titleUpdate' => $titleUpdate,
                'etatNew' => $etatNew,
                'etatUpdate' => $etatUpdate,
                'confirmMsg' => $confirmMsg,
                'confirmEtat' => $confirmEtat,
                'cancelMsg' => $cancelMsg,
                'cancelEtat' => $cancelEtat,
                'typeRequest' => $typeRequest,
                'viewName' => $viewName,
                'folder' => $folder,
            );
        }
        return $TagsArray[$tag];
    }
    
    public function simpleMailSend($mail_to, $html, $subject, $cc, $dataBase64, $file_name) {
       return $this->Mailhelper->sendEmail($mail_to, $html, $subject, null, $cc, $dataBase64, $file_name);
    }

}