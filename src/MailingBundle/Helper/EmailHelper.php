<?php

/**
 * Test Helper
 *
 * @package Test
 * @version 0.1
 * @copyright Copyright 2013 (C) Greenacorn Web. - All Rights Reserved
 */

namespace MailingBundle\Helper;

class EmailHelper
{
    /**
     * admin email address
     */
    const EMAIL_FROM = 'notification-hrm@fabereo.com' ;
 
    /**
     * service container
     *
     * @var object
     */
    protected $service;
 
    /**
     * init service
     *
     * @param object $service
     * @return $this
     */
    public function __construct($service) {
        $this->service = $service;
 
        return $this;
    }
 
    /**
     * Send email
     *
     * @param string $to
     * @param twig $view
     * @param string $subject
     */
    public function sendEmail($to, $view, $subject = 'Email from website', $etat = null, $cc = null, $dataBase64 = null, $file_name = null)
    {
        if ($to) {
            $message = \Swift_Message::newInstance()->setContentType('text/html')
                    ->setSubject($subject)
                    ->setFrom(self::EMAIL_FROM)
                    ->setTo($to);
           if($cc  != null )
               $message->addCc($cc);
           if($dataBase64 != null)
               $message->attach(\Swift_Attachment::newInstance($dataBase64, $file_name, 'application/pdf'));
               $message->setBody($view, 'text/html');
 
           return $this->service->get('mailer')->send($message);
        }
    }
 
}