<?php
namespace UserBundle\Redirection;
 
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
class AfterLoginRedirection implements AuthenticationSuccessHandlerInterface
{
    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;
 
    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;
    /**
     * @param RouterInterface $router
     */
    public function __construct(RouterInterface $router,Container $container)
    {
        $this->router = $router;
        $this->container = $container;
    }
 
    /**
     * @param Request $request
     * @param TokenInterface $token
     * @return RedirectResponse
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        $roles = $token->getRoles();
        $rolesTab = array_map(function($role){ 
          return $role->getRole(); 
        }, $roles);
        $user = $this->container->get('security.context')->getToken()->getUser();
        $company = $user->getEmployee()->first()->getCompany()->getId();             
        $this->container->get('session')->set('company', $company);        
        if (in_array('ROLE_ADMIN', $rolesTab, true) || in_array('ROLE_SUPER_ADMIN', $rolesTab, true))
            $redirection = new RedirectResponse($this->router->generate('adminCategories_layout'));
        elseif ((in_array('ROLE_RESPONSABLE', $rolesTab, true)) || (in_array('ROLE_COLLABORATEUR', $rolesTab, true)))
        {

           if (count($provider = $this->container->get('settingsbundle.preference.service')->getProviders())>1)
               $redirection = new RedirectResponse($this->router->generate('predashboard'));
           else
           {
                $active_menu_item = $this->container->get('settingsbundle.preference.service')->providersMenuList(1);
                if(empty($active_menu_item))
                    $redirection = new RedirectResponse($this->router->generate('dashboard_index'));
                else
                    $redirection = new RedirectResponse($this->router->generate($active_menu_item['setting_menu']['routeLinks']));
                
           }
               
        }
        return $redirection;
    }
}