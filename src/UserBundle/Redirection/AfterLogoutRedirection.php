<?php

namespace UserBundle\Redirection;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Http\Logout\LogoutSuccessHandlerInterface;
use Symfony\Component\DependencyInjection\Container;

class AfterLogoutRedirection implements LogoutSuccessHandlerInterface {

    /**
     * @var \Symfony\Component\Routing\RouterInterface
     */
    private $router;

    /**
     * @var \Symfony\Component\Security\Core\SecurityContextInterface
     */
    private $security;
    
    /**
     * @var \Symfony\Component\DependencyInjection\Container
     */
    private $container;    

    /**
     * @param SecurityContextInterface $security
     */
    public function __construct(RouterInterface $router, SecurityContextInterface $security, Container $container) {
        $this->router = $router;
        $this->security = $security;
        $this->container = $container;
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function onLogoutSuccess(Request $request) {
        // On récupère la liste des rôles d'un utilisateur
        if (empty($this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getPortalReturnUrl())) {
            $redirect_url = 'https://www.fabereo.com/';
        } else {
            $redirect_url = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getPortalReturnUrl();            
        }
        return new RedirectResponse($redirect_url);
    }

}
