<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class HrmCompanyType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('employeeNumber')
            ->add('adress')->add('city')->add('state')->add('postalCode')
            ->add('country', 'country', array('data' => 'FR'))
            ->add('phoneNumber')->add('fax')->add('email')->add('webSite')->add('siret')->add('siren')->add('capitalSocial')->add('tva')->add('naf')->add('rcs')->add('rm')->add('description')->add('external_ref')
//                ->add('industrySector')
//                ->add('customerType')

            ->add('customerType', 'entity', array(
                'label' => 'Part time day periodicity',
                'required' => true,
                'class' => 'AdminBundle\Entity\hrm_settings_customertype',
                'property' => 'display_name',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder("t")
                        ->leftJoin('t.provider', 'p')
                        ->Where('p.id = :provider')
                        ->setParameter('provider', (int)$options['provider'])
                        ->orderBy('t.seqno', 'ASC');
                },
            ))
            ->add('industrySector', 'entity', array(
                'label' => 'Part time day periodicity',
                'required' => true,
                'class' => 'AdminBundle\Entity\hrm_settings_industrysector',
                'property' => 'display_name',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder("t")
                        ->leftJoin('t.provider', 'p')
                        ->Where('p.id = :provider')
                        ->setParameter('provider', (int)$options['provider'])
                        ->orderBy('t.seqno', 'ASC');
                },
            ))
            ->add('provider')
            ->add('base_currency_iso_code', 'text', array(
                'label' => 'Currency',
                'required' => false,
                'read_only' => true,
                'disabled' => true,
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right'
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\HrmCompany',
            'provider' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'company';
    }


}
