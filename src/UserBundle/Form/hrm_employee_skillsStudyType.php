<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class hrm_employee_skillsStudyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('displayName')
            ->add('skills_study', 'entity', array(
                'property' => 'display_name',
                'empty_value' => 'Sélectionner une etude',
                'required' => true,
                'class' => 'AdminBundle\Entity\SettingsStudyType',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder("t")
                        ->leftJoin('t.provider', 'p')
                        ->Where('p.id = :provider')
                        ->setParameter('provider', (int)$options['provider'])
                        ->orderBy('t.seqno', 'ASC');
                },
            ))
            ->add('study_level', 'entity', array(
                'property' => 'display_name',
                'empty_value' => 'Sélectionner un niveau',
                'required' => true,
                'class' => 'AdminBundle\Entity\SettingsStudyLevel',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder("t")
                        ->leftJoin('t.provider', 'p')
                        ->Where('p.id = :provider')
                        ->setParameter('provider', (int)$options['provider'])
                        ->orderBy('t.seqno', 'ASC');
                },
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'provider' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'userbundle_hrm_employee_skillsstudy';
    }


}
