<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class hrm_company_currencyType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('currencyIsoCode')
            ->add('rate')
            ->add('date', 'date', array(
                'label' => 'Date du devise',
                'required' => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'class' => 'form-control datepicker',
                    'date-directive'=>''
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right '
                )
            ))
//            ->add('company', 'entity', array(
//                'label' => 'Part time day periodicity',
//                'required' => true,
//                'class' => 'UserBundle\Entity\HrmCompany',
//                'property' => 'name',
//                'attr' => array(
//                    'class' => 'form-control',
//                ),
//                'label_attr' => array(
//                    'class' => 'col-sm-3 control-label no-padding-right asterix'
//                ),
//            ))
            ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\hrm_company_currency'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'userbundle_hrm_company_currency';
    }


}
