<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Form\hrm_employee_skillsStudyType;
use UserBundle\Form\hrm_employee_skillsCompanyType;
use UserBundle\Form\hrm_employee_skillsLanguageType;
use UserBundle\Form\hrm_employee_skillsTechnologyType;
use UserBundle\Form\hrm_employee_skillsOthersCompetenciesTagsType;

class hrm_employee_skillsType extends AbstractType
{

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('skills_study', new hrm_employee_skillsStudyType(), array(
                'provider' => $options['provider'],
            ))
            ->add('studyComment', 'textarea', array(
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right'
                )
            ))
            ->add('skills_company', new hrm_employee_skillsCompanyType(), array(
                'provider' => $options['provider'],
            ))
            ->add('skills_language', new hrm_employee_skillsLanguageType(), array(
                'provider' => $options['provider'],
            ))
            ->add('skills_technology', new hrm_employee_skillsTechnologyType(), array(
                'provider' => $options['provider'],
            ))
            ->add('skills_other', new hrm_employee_skillsOthersCompetenciesTagsType())
            ->add('experienceComment', 'textarea', array(
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right'
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\hrm_employee_skills',
            'provider' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'userbundle_hrm_employee_skills';
    }


}
