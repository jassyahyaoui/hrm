<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class IsPublicProfilsType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('isPubGender')
                ->add('isPubNameUsage')
                ->add('isPubBirthDate')
                ->add('isPubDepartmentBirth')
                ->add('isPubPlaceBirth')
                ->add('isPubNationCode')
                ->add('isPubCounNation') 
                ->add('isPubAddress')
                ->add('isPubZipCode')
                ->add('isPubCountryCode')
                ->add('isPubCommune')
                ->add('isPubStudyType')
                ->add('isPubStudyLevel')
                ->add('employee');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\IsPublicProfils'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'userbundle_ispublicprofils';
    }

}
