<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HrmEmployeeType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('birth_date', 'date', array(
                    'label' => 'Date de naissance',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array(
                        'id' => 'datepicker_requestdate',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                ->add('domiciliation')
                ->add('employee_number')
                ->add('nation_code')
                ->add('address')
                ->add('city_code')
                ->add('zip_code')
                ->add('twitter')
                ->add('province_code')
                ->add('country_code')
                ->add('sin_num')
                ->add('ssn_num')
                ->add('is_handicape')
                ->add('num_children')
                ->add('iban')
                ->add('bic')
                ->add('provider')
                ->add('company')
                ->add('commune')
                ->add('avatars')
                ->add('department_birth')
                ->add('place_birth')
                ->add('country_nationality')
                ->add('comments')
                ->add('employee_contract')
                ->add('gender')
                ->add('payment_method')
                ->add('status')
                ->add('role')
                ->add('study_level')
                ->add('study_type')
                ->add('public_profil')
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\HrmEmployee'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'userbundle_hrmemployee';
    }

}
