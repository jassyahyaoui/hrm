<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class EmployeeContractType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                ->add('empStatus', 'entity', array(
                    'label' => 'Statut',
                    'required' => false,
                    'class' => 'AdminBundle\Entity\SettingsEmpStatus',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('contractType', 'entity', array(
                    'label' => 'Nature du contrat',
                    'required' => false,
                    'class' => 'AdminBundle\Entity\TypeContracts',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->Where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                                ->orderBy('t.seqno', 'ASC');
                    },
                ))
                ->add('convention', null, array('required' => false))
                ->add('contractCustomCondition')
                ->add('salClassification', null, array('required' => false))
                ->add('salGrade', null, array('required' => false))
                ->add('salEchelon', null, array('required' => false))
                ->add('salJobTitle', null, array('required' => false))
//                ->add('salJoinedDate', 'text', array('required' => false))
                              ->add('salJoinedDate', 'date', array(
                     'required' => false,
                    'label' => 'salJoinedDate',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array(
                        'id' => 'datepicker_requestdate',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                   
                                          ->add('salJobTitle', null, array('required' => false))
                              ->add('salLeftDate', 'date', array(
                     'required' => false,
                    'label' => 'salLeftDate',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array(
                        'id' => 'datepicker_requestdate',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                            
                                          ->add('salJobTitle', null, array('required' => false))
                              ->add('salSeniorityDate', 'date', array(
                     'required' => false,
                    'label' => 'salSeniorityDate',
                    'widget' => 'single_text',
                    'format' => 'dd/MM/yyyy',
                    'attr' => array(
                        'id' => 'datepicker_requestdate',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
                            
                            
   
                            
                            
                            
                            
                            
                            
                ->add('insuranceContractRef1', null, array('required' => false))
                ->add('insuranceContractRef2', null, array('required' => false))
                ->add('notes', 'textarea', array('required' => false))
                ->add('work_timetable', null, array('required' => false))
                ->add('contract_number', null, array('required' => false))
                ->add('contract_number', null, array('required' => false))
                ->add('work_timetable', null, array('required' => false))
                ->add('working_time', null, array('required' => false))
                ->add('fixed_rest_days_agreed', null, array('required' => false))
                ->add('gross_annual_base_salary', null, array('required' => false))
                ->add('nb_of_installments', null, array('required' => false))




        //    ->add('ribAttachments',null, array('required' => false))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\EmployeeContract',
            'user_id' => null,
            'provider' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'userbundle_employeecontract';
    }

}
