<?php

namespace UserBundle\Form\Handler;

use FOS\UserBundle\Form\Handler\RegistrationFormHandler as BaseHandler;
use FOS\UserBundle\Model\UserInterface;

class RegistrationFormHandler extends BaseHandler
{
    /**
     * @param boolean $confirmation
     */
    public function process($confirmation = false)
    {
        $user = $this->createUser();
        $this->form->setData($user);
        
        if ('POST' === $this->request->getMethod()) {
            $this->form->bind($this->request);
        
            if ($this->form->isValid()) {
                $user->setRoles(['ROLE_RESPONSABLE']);
                $this->onSuccess($user, $confirmation);

                return true;
            }
        }

        return false;
    }
}