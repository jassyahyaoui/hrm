<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use UserBundle\Form\ProfilsType;

class hrm_userType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
                //Informations Personnelles
                ->add('first_name', null, array('required' => true))
                ->add('last_name', null, array('required' => true))
                ->add('email', 'email', array('required' => true))
                ->add('name_usage')
                ->add('employee',  new ProfilsType($options['provider'], $options['user_id']), array('mapped' => false));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\hrm_user',
            'user_id' => null,
            'provider' => null,
        ));
    }
    
    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'userbundle_profils_data';
    }

}
