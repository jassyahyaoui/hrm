<?php

namespace UserBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class hrm_employee_skillsLanguageType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('language', 'entity', array(
                'property' => 'display_name',
                'empty_value' => 'Sélectionner une langue',
                'required' => true,
                'class' => 'AdminBundle\Entity\hrm_settings_language',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder("t")
                        ->leftJoin('t.provider', 'p')
                        ->Where('p.id = :provider')
                        ->setParameter('provider', (int)$options['provider'])
                        ->orderBy('t.seqno', 'ASC');
                },
            ))
            ->add('language_level', 'entity', array(
                'property' => 'display_name',
                'empty_value' => 'Sélectionner un niveau',
                'required' => true,
                'class' => 'AdminBundle\Entity\hrm_settings_language_level',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder("t")
                        ->leftJoin('t.provider', 'p')
                        ->Where('p.id = :provider')
                        ->setParameter('provider', (int)$options['provider'])
                        ->orderBy('t.seqno', 'ASC');
                },
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => null,
            'provider' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'userbundle_hrm_employee_skillslanguage';
    }


}
