<?php

namespace UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Regex;
use Doctrine\ORM\EntityRepository;

class ProfilsType extends AbstractType
{

    private $provider;
    private $user_id;

    public function __construct($provider = null, $user_id = null)
    {
        $this->provider = $provider;
        $this->user_id = $user_id;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //Informations Personnelles
            ->add('gender', 'entity', array(
                'label' => 'Sexe',
                'required' => true,
                'class' => 'AdminBundle\Entity\Gender',
                'property' => 'display_name',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder("t")
                        ->leftJoin('t.provider', 'p')
                        ->Where('p.id = :provider')
                        ->setParameter('provider', (int)$this->provider)
                        ->orderBy('t.seqno', 'ASC');
                },
            ))
            ->add('isPubGender', 'checkbox', array('required' => false, 'mapped' => false))
            // ->add('name_usage')
            ->add('isPubNameUsage', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('isPubDepartmentBirth', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('isPubPlaceBirth', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('birth_date', 'date', array(
                'required' => false,
                'label' => 'Date de naissance',
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'id' => 'datepicker_requestdate',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('isPubBirthDate', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('nation_code', 'country', array('data' => 'FR'))
            ->add('isPubNationCode', 'checkbox', array('required' => false, 'mapped' => false))
            //Adresse postale
            ->add('address', 'textarea', array('required' => false))
            ->add('isPubAddress', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('isPubCommune', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('zip_code')
            ->add('isPubZipCode', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('country_code', 'country')
            ->add('isPubCountryCode', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('isPubCounNation', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('ssn_num', 'text', array('required' => false))
            ->add('is_handicape')
            ->add('num_children')
            ->add('department_birth')
            ->add('place_birth')
            ->add('country_birth')
            ->add('family_status', 'entity', array(
                'label' => 'Sexe',
                'required' => true,
                'class' => 'AdminBundle\Entity\hrm_settings_family_status',
                'property' => 'display_name',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder("t")
                        ->leftJoin('t.provider', 'p')
                        ->Where('p.id = :provider')
                        ->setParameter('provider', (int)$this->provider)
                        ->orderBy('t.seqno', 'ASC');
                },
            ))
            //Informations bancaire
            ->add('payment_method', 'entity', array(
                'label' => 'Sexe',
                'required' => true,
                'class' => 'AdminBundle\Entity\SettingsPayment',
                'property' => 'display_name',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder("t")
                        ->leftJoin('t.provider', 'p')
                        ->Where('p.id = :provider')
                        ->setParameter('provider', (int)$this->provider)
                        ->orderBy('t.seqno', 'ASC');
                },
            ))
            ->add('employee_number', 'text', array('required' => false))
            ->add('country_nationality', 'country', array('data' => 'FR'))
            ->add('iban', 'text', array('required' => false))
            ->add('bic', 'text', array('required' => false))
            //Autres
            ->add('study_type', 'entity', array(
                'label' => 'Sexe',
                'required' => true,
                'class' => 'AdminBundle\Entity\SettingsStudyType',
                'property' => 'display_name',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder("t")
                        ->leftJoin('t.provider', 'p')
                        ->Where('p.id = :provider')
                        ->setParameter('provider', (int)$this->provider)
                        ->orderBy('t.seqno', 'ASC');
                },
            ))
            ->add('study_level', 'entity', array(
                'label' => 'Sexe',
                'required' => true,
                'class' => 'AdminBundle\Entity\SettingsStudyLevel',
                'property' => 'display_name',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder("t")
                        ->leftJoin('t.provider', 'p')
                        ->Where('p.id = :provider')
                        ->setParameter('provider', (int)$this->provider)
                        ->orderBy('t.seqno', 'ASC');
                },
            ))
            ->add('isPubStudyType', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('isPubStudyLevel', 'checkbox', array('required' => false, 'mapped' => false))
            ->add('comments')
            ->add('homePhone')
            ->add('mobilePhone')
            ->add('skype')
            ->add('linkedin')
            ->add('viadeo')
            ->add('twitter')
            ->add('departement', null, array('required' => false))
            ->add('commune', null, array('required' => false))
            ->add('domiciliation', null, array('required' => false));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'UserBundle\Entity\HrmEmployee',
            'user_id' => null,
            'provider' => null,
        ));
    }

    public function getDefaultOptions(array $options)
    {
        $collectionConstraint = new Collection(array(
            'ssn_num' => new Regex(array(
                'pattern' => '[0-9]{15}',
                'match' => true,
                'message' => '15 digit sociale securite number'
            ))
        ));

        return array(
            'validation_constraint' => $collectionConstraint,
            'show_legend' => false,
            'render_fieldset' => false
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'userbundle_profils';
    }

}
