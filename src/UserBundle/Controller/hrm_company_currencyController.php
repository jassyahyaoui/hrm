<?php

namespace UserBundle\Controller;

use UserBundle\Entity\hrm_company_currency;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Hrm_company_currency controller.
 *
 */
class hrm_company_currencyController extends Controller
{

    public function indexAction()
    {
        return $this->render('hrm_company_currency/index.html.twig');
    }

    public function showAction()
    {
        return $this->render('hrm_company_currency/show.html.twig');
    }

    public function newAction(Request $request)
    {
        $hrm_company_currency = new Hrm_company_currency();
        $form = $this->createForm('UserBundle\Form\hrm_company_currencyType', $hrm_company_currency);
        $form->handleRequest($request);

        return $this->render('hrm_company_currency/new.html.twig', array(
            'hrm_company_currency' => $hrm_company_currency,
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request)
    {
        $hrm_company_currency = new Hrm_company_currency();
        $form = $this->createForm('UserBundle\Form\hrm_company_currencyType', $hrm_company_currency);
        $form->handleRequest($request);

        return $this->render('hrm_company_currency/edit.html.twig', array(
            'hrm_company_currency' => $hrm_company_currency,
            'form' => $form->createView(),
        ));
    }

    public function injectEuro($em,$currencyInfos,$CompanyId,$current_user_id)
    {
        if (empty($currencyInfos)) {
            $hrm_company_currency = new hrm_company_currency();
            $hrm_company_currency->setName("Euro");
            $hrm_company_currency->setRate(1);
            $hrm_company_currency->setCurrencyIsoCode("EUR - Euro");
            $hrm_company_currency->setCompany($em->getRepository('UserBundle:HrmCompany')->find($CompanyId));
            $hrm_company_currency->setDate(new \DateTime('now'));
            $hrm_company_currency->setCreateDate(new \DateTime('now'));
            $hrm_company_currency->setCreateUid($current_user_id);
            $hrm_company_currency->setLastUpdateUid($current_user_id);
            $hrm_company_currency->setLastUpdate(new \DateTime('now'));
            $em->persist($hrm_company_currency);
            $em->flush();
        }
    }

    public function addAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();
        $CompanyId = $this->container->get('settingsbundle.preference.service')->getCompany()->getId();
        $currencyInfos = $em->getRepository('UserBundle:hrm_company_currency')->findAll();
        $this->injectEuro($em,$currencyInfos,$CompanyId,$current_user_id);
        $hrm_company_currency = new hrm_company_currency();
        $form = $this->createForm('UserBundle\Form\hrm_company_currencyType', $hrm_company_currency);
        $form->handleRequest($request);
        $body = $request->getContent();
        $data = json_decode($body, true);
        //set default status if ROLE_COLLABORATEUR else set with the submited value
        if ($request->isMethod('POST')) {
            // will be get error if not deleted settings_pay_period from FormType before submit
            $form->submit($data);
            //Add values for all forgein Keys
            $hrm_company_currency->setCompany($em->getRepository('UserBundle:HrmCompany')->find($CompanyId));

            //Add default value on create Action
            $hrm_company_currency->setCreateDate(new \DateTime('now'));
            $hrm_company_currency->setCreateUid($current_user_id);
            $hrm_company_currency->setLastUpdateUid($current_user_id);
            $hrm_company_currency->setLastUpdate(new \DateTime('now'));

            $em->persist($hrm_company_currency);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function SavedCurrenciesAction()
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $companyData = $empData->getCompany();
        $company_id = $companyData->getId();

        $result = $em->getRepository('UserBundle:hrm_company_currency')->FindSavedCurrencies($company_id);
//        dump($result);
//        die();
        return array('result' => $result);
    }

    public function removeAction(Request $request, hrm_company_currency $hrm_company_currency)
    {
        if ($request->isMethod('DELETE')) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_company_currency);
            $em->flush($hrm_company_currency);
            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }

    public function updateCurrenciesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, true);
        $hrm_company_currency = $em->getRepository('UserBundle:hrm_company_currency')->find($data['id']);
        $update_form = $this->createForm('UserBundle\Form\hrm_company_currencyType', $hrm_company_currency);
        if ($request->isMethod('PUT')) {
            $update_form->submit($data);

            $CompanyId = $this->container->get('settingsbundle.preference.service')->getCompany()->getId();
            $hrm_company_currency->setCompany($em->getRepository('UserBundle:HrmCompany')->find($CompanyId));
            $hrm_company_currency->setLastUpdate(new \DateTime('now'));
            $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $current_user_id = $empData->getId();
            $hrm_company_currency->setLastUpdateUid($current_user_id);

            $em->persist($hrm_company_currency);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }
}
