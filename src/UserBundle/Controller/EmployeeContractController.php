<?php

namespace UserBundle\Controller;

use UserBundle\Entity\EmployeeContract;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Form\EmployeeContractType;
use Symfony\Component\HttpFoundation\JsonResponse;
use FileBundle\Entity\attachments;
use ExpenseBundle\Repository\hrm_attachmentsRepository;
use UserBundle\Entity\HrmEmployee;

/**
 * Employeecontract controller.
 *
 */
class EmployeeContractController extends Controller {

    public function getEmployeeContractAction(Request $request) {
        $user = $this->get('security.context')->getToken()->getUser();
        $em = $this->getDoctrine()->getEntityManager();

        $employeeContract = $em->getRepository('UserBundle:EmployeeContract')->findOneBy(array("employee" => $user->getId()));
        $ret = array();

        if ($employeeContract) {
            if ($employeeContract->getEmpStatus() != null)
                $ret['empStatus'] = (int) $employeeContract->getEmpStatus()->getId();

            if ($employeeContract->getContractType() != null)
                $ret['contractType'] = (int) $employeeContract->getContractType()->getId();
            $ret['contractCustomCondition'] = $employeeContract->getContractCustomCondition();
            $ret['salClassification'] = $employeeContract->getSalClassification();
            $ret['salGrade'] = $employeeContract->getSalGrade();
            $ret['salEchelon'] = $employeeContract->getSalEchelon();
            $ret['salJobTitle'] = $employeeContract->getSalJobTitle();

            $ret['convention'] = (int) $employeeContract->getConvention();

            $ret['salJoinedDate'] = (($employeeContract->getSalJoinedDate() == "" || (!$this->validateDate($employeeContract->getSalJoinedDate()->format('Y-m-d')))) ?
                    '' : $employeeContract->getSalJoinedDate()->format('d-m-Y') );



            $ret['salLeftDate'] = (($employeeContract->getSalLeftDate() == "" || (!$this->validateDate($employeeContract->getSalLeftDate()->format('Y-m-d')))) ?
                    '' : $employeeContract->getSalLeftDate()->format('d-m-Y') );

            $ret['salSeniorityDate'] = (($employeeContract->getSalSeniorityDate() == "" || (!$this->validateDate($employeeContract->getSalSeniorityDate()->format('Y-m-d')))) ?
                    '' : $employeeContract->getSalSeniorityDate()->format('d-m-Y') );
            $ret['insuranceContractRef1'] = $employeeContract->getInsuranceContractRef1();
            $ret['insuranceContractRef2'] = $employeeContract->getInsuranceContractRef2();
            $ret['notes'] = $employeeContract->getNotes();

            if ($employeeContract->getRibAttachments() != null)
                $ret['ribAttachments'] = $employeeContract->getRibAttachments()->getPath();


            return new JsonResponse($ret);
        }
        return new JsonResponse(null);
    }

    /**
     * Displays a form to edit an existing employeeContract entity.
     *
     */
    public function editAction(Request $request) {
        $employeeContract = new EmployeeContract();
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
        $infos_user = $em->getRepository('UserBundle:HrmEmployee')->find($current_user_id);
        $provider = $infos_user->getCompany()->getProvider()->getId();
        $company = $infos_user->getCompany()->getId();
        $editForm = $this->createForm('UserBundle\Form\EmployeeContractType', $employeeContract, array(
            'user_id' => $company, 'provider' => $provider
        ));
        return $this->render('employeecontract/index.html.twig', array(
                    'form' => $editForm->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $usrCnt = $this->get('security.context')->getToken()->getUser();
        if (in_array($request->getMethod(), array('PUT', 'POST'))) {
            $em = $this->getDoctrine()->getEntityManager();
            $employeeContract = $em->getRepository('UserBundle:EmployeeContract')->findOneBy(array("employee" => $usrCnt->getId()));
            if (!$employeeContract) {
                return $this->applyToSaveEmployeeContract($request, $usrCnt);
            } else {
                return $this->applyToUpdateEmployeeContract($request, $employeeContract);
            }
        }
        return new JsonResponse(array('error' => array('code' => 'error_method', 'message' => 'Only allow PUT requests')));
    }

    public function updateContractAction(Request $request, EmployeeContract $employeeContract) { // var_dump($employeeContract);die();
        $em = $this->getDoctrine()->getManager();
        $employeeContract = $em->getRepository('UserBundle:EmployeeContract')->find($employeeContract->getId());
        $json_data = json_decode($request->getContent(), true);
        $form = $this->createForm(new EmployeeContractType(), $employeeContract);
        $form->submit($json_data);

        if (isset($json_data['insuranceContractRef1']))
            $employeeContract->setInsuranceContractRef1($json_data['insuranceContractRef1']);
        if (isset($json_data['insuranceContractRef2']))
            $employeeContract->setInsuranceContractRef2($json_data['insuranceContractRef2']);

        if (isset($json_data['salGrade']))
            $employeeContract->setSalGrade($json_data['salGrade']);

        if (isset($json_data['empStatus']))
            $employeeContract->setEmpStatus($em->getRepository('AdminBundle:SettingsEmpStatus')->find($json_data['empStatus']['id']));

        if (isset($json_data['contractType']))
            $employeeContract->setContractType($em->getRepository('AdminBundle:TypeContracts')->find($json_data['contractType']['id']));
//   Pas besoin de convertir les dates
//        if (isset($json_data['salJoinedDate']))
//            $employeeContract->setSalJoinedDate(new \DateTime($json_data['salJoinedDate']));
//
//        if (isset($json_data['salLeftDate']))
//            $employeeContract->setSalLeftDate(new \DateTime($json_data['salLeftDate']));
//        if (isset($json_data['salSeniorityDate']))
//            $employeeContract->setSalSeniorityDate(new \DateTime($json_data['salSeniorityDate']));


        if (isset($json_data['contractAttachment']['path'])) {
//         if( $employeeContract->getContractAttachment()!= null) $attachment = $employeeContract->getContractAttachment();  else   $attachment = new attachments();
            $attachment = new attachments();
            $attachment->setPath($json_data['contractAttachment']['path']);
            if (isset($json_data['contractAttachment']['mimetype']))
                $attachment->setMimetype($json_data['contractAttachment']['mimetype']);
            $em->persist($attachment);
            $employeeContract->setContractAttachment($attachment);
        } else {
            $employeeContract->setContractAttachment(null);
        }
        if (isset($json_data['identificationAttachment']['path'])) {
            $attachment = new attachments();
            $attachment->setPath($json_data['identificationAttachment']['path']);
            if (isset($json_data['identificationAttachment']['mimetype']))
                $attachment->setMimetype($json_data['identificationAttachment']['mimetype']);
            $em->persist($attachment);
            $employeeContract->setIdentificationAttachment($attachment);
        } else {
            $employeeContract->setIdentificationAttachment(null);
        }
        if (isset($json_data['autorsationAttachment']['path'])) {
            $attachment = new attachments();
            $attachment->setPath($json_data['autorsationAttachment']['path']);
            if (isset($json_data['autorsationAttachment']['mimetype']))
                $attachment->setMimetype($json_data['autorsationAttachment']['mimetype']);
            $em->persist($attachment);
            $employeeContract->setAutorsationAttachment($attachment);
        } else {
            $employeeContract->setAutorsationAttachment(null);
        }
        if (isset($json_data['vitalCardAttachment']['path'])) {
            $attachment = new attachments();
            $attachment->setPath($json_data['vitalCardAttachment']['path']);
            if (isset($json_data['vitalCardAttachment']['mimetype']))
                $attachment->setMimetype($json_data['vitalCardAttachment']['mimetype']);
            $em->persist($attachment);
            $employeeContract->setVitalCardAttachment($attachment);
        } else {
            $employeeContract->setVitalCardAttachment(null);
        }
        if (isset($json_data['singleDeclarationAttachment']['path'])) {
            $attachment = new attachments();
            $attachment->setPath($json_data['singleDeclarationAttachment']['path']);
            if (isset($json_data['singleDeclarationAttachment']['mimetype']))
                $attachment->setMimetype($json_data['singleDeclarationAttachment']['mimetype']);
            $em->persist($attachment);
            $employeeContract->setSingleDeclarationAttachment($attachment);
        } else {
            $employeeContract->setSingleDeclarationAttachment(null);
        }
        if (isset($json_data['autorsationAttachment']['path'])) {
            $attachment = new attachments();
            $attachment->setPath($json_data['autorsationAttachment']['path']);
            if (isset($json_data['autorsationAttachment']['mimetype']))
                $attachment->setMimetype($json_data['autorsationAttachment']['mimetype']);
            $em->persist($attachment);
            $employeeContract->setAutorsationAttachment($attachment);
        } else {
            $employeeContract->setAutorsationAttachment(null);
        }
        if (isset($json_data['affiliationPensionAttachment']['path'])) {
            $attachment = new attachments();
            $attachment->setPath($json_data['affiliationPensionAttachment']['path']);
            if (isset($json_data['affiliationPensionAttachment']['mimetype']))
                $attachment->setMimetype($json_data['affiliationPensionAttachment']['mimetype']);
            $em->persist($attachment);
            $employeeContract->SetAffiliationPensionAttachment($attachment);
        } else {
            $employeeContract->setAffiliationPensionAttachment(null);
        }
        if (isset($json_data['affiliationHealthAttachment']['path'])) {
            $attachment = new attachments();
            $attachment->setPath($json_data['affiliationHealthAttachment']['path']);
            if (isset($json_data['affiliationHealthAttachment']['mimetype']))
                $attachment->setMimetype($json_data['affiliationHealthAttachment']['mimetype']);
            $em->persist($attachment);
            $employeeContract->setAffiliationHealthAttachment($attachment);
        } else {
            $employeeContract->setAffiliationHealthAttachment(null);
        }
        if (isset($json_data['affiliationComplementaryPensionAttachment']['path'])) {
            $attachment = new attachments();
            $attachment->setPath($json_data['affiliationComplementaryPensionAttachment']['path']);
            if (isset($json_data['affiliationComplementaryPensionAttachment']['mimetype']))
                $attachment->setMimetype($json_data['affiliationComplementaryPensionAttachment']['mimetype']);
            $em->persist($attachment);
            $employeeContract->setAffiliationComplementaryPensionAttachment($attachment);
        } else {
            $employeeContract->setAffiliationComplementaryPensionAttachment(null);
        }
        if (isset($json_data['otherAttachment']['path'])) {
            $attachment = new attachments();
            $attachment->setPath($json_data['otherAttachment']['path']);
            if (isset($json_data['otherAttachment']['mimetype']))
                $attachment->setMimetype($json_data['otherAttachment']['mimetype']);
            $em->persist($attachment);
            $employeeContract->setOtherAttachment($attachment);
        } else {
            $employeeContract->setOtherAttachment(null);
        }

        $employeeContract->setLastUpdate(new \DateTime('now'));
        $employeeContract->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($employeeContract);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    private function applyToUpdateEmployeeContract($request, EmployeeContract $employeeContract) {

        $json_data = json_decode($request->getContent(), true);
        $form = $this->createForm(new EmployeeContractType(), $employeeContract);
        $form->submit($json_data);
        $em = $this->getDoctrine()->getManager();

        if (isset($json_data['salJoinedDate']))
            $employeeContract->setSalJoinedDate(new \DateTime($json_data['salJoinedDate']));

        if (isset($json_data['salLeftDate']))
            $employeeContract->setSalLeftDate(new \DateTime($json_data['salLeftDate']));
        if (isset($json_data['salSeniorityDate']))
            $employeeContract->setSalSeniorityDate(new \DateTime($json_data['salSeniorityDate']));


        if (isset($json_data['ribAttachments'])) {
            if (is_string($json_data['ribAttachments'])) {
                if ($employeeContract->getRibAttachments() != null) {
                    $attachment = $employeeContract->getRibAttachments();
                } else
                    $attachment = new attachments();
                $attachment->setPath($json_data['ribAttachments']);
                $em->persist($attachment);
                $em->flush($attachment);
                $employeeContract->setRibAttachments($attachment);
            }
        } else
            $employeeContract->setRibAttachments(null);
        $employeeContract->setLastUpdate(new \DateTime('now'));
        $employeeContract->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();

        /** update user contrat id * */
        $hrmEmployee = $em->getRepository('UserBundle:HrmEmployee')->find($employeeContract->getEmployee());
        $hrmEmployee->setEmployeeContract($employeeContract);
        $em->persist($hrmEmployee);
        $em->flush();

        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    private function applyToSaveEmployeeContract($request, $usrCnt) {
        $employeeContract = new EmployeeContract();

        $json_data = json_decode($request->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $form = $this->createForm(new EmployeeContractType(), $employeeContract);
        $form->submit($json_data);
        $employeeContract->setEmployee($usrCnt);
        $employeeContract->setProvider($em->getRepository('AdminBundle:SettingsProvider')->find(1));
        $employeeContract->setCreateDate(new \DateTime('now'));
        if (isset($json_data['salJoinedDate']))
            $employeeContract->setSalJoinedDate(new \DateTime($json_data['salJoinedDate']));

        if (isset($json_data['salLeftDate']))
            $employeeContract->setSalLeftDate(new \DateTime($json_data['salLeftDate']));

        if (isset($json_data['salSeniorityDate']))
            $employeeContract->setSalSeniorityDate(new \DateTime($json_data['salSeniorityDate']));
        $employeeContract->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        if ((isset($json_data['ribAttachments'])) && (is_string($json_data['ribAttachments']))) {
            $attachment = new attachments();
            $attachment->setPath($json_data['ribAttachments']);
            $em->persist($attachment);
            $em->flush($attachment);
            $employeeContract->setRibAttachments($attachment);
        }
        $em->persist($employeeContract);

        /** save user contrat id * */
        $hrmEmployee = $em->getRepository('UserBundle:HrmEmployee')->find($employeeContract->getEmployee());
        $hrmEmployee->setEmployeeContract($employeeContract);
        $em->persist($hrmEmployee);
        $em->flush();

        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    private function validateDate($date) {
        $d = \DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') === $date;
    }

}
