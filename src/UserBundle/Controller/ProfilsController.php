<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Form\ProfilsType;
use UserBundle\Entity\IsPublicProfils;
use UserBundle\Entity\HrmEmployee;
use FileBundle\Entity\attachments;
use ExpenseBundle\Repository\hrm_attachmentsRepository;
use UserBundle\Entity\EmployeeContract;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use UserBundle\Entity\hrm_employee_skills;
use SettingsBundle\Entity\hrm_employee_settings;

/**
 * Profils controller.
 *
 */
class ProfilsController extends Controller
{

    /**
     * Displays a form to update an existing ik entity.
     *
     */
    public function updateAction(Request $request, HrmEmployee $employee)
    {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);

        if ($request->isMethod('PUT')) {
            $user = $em->getRepository('UserBundle:hrm_user')->find($json_data['user']['id']);
            $update_form = $this->createForm('UserBundle\Form\hrm_userType', $user);
            $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
            $update_form->submit($json_data);
            $employee = $em->getRepository('UserBundle:HrmEmployee')->find($json_data['id']);
            $update_employee = $this->createForm('UserBundle\Form\HrmEmployeeType', $employee);
            $update_employee->submit($json_data);
            $user->setLastUpdate(new \DateTime('now'));
            $user->setLastUpdateUid($current_user_id);


            $employee->setMobilePhone((isset($json_data['mobilePhone']) ? $json_data['mobilePhone'] : NULL));
            $employee->setHomePhone((isset($json_data['homePhone']) ? $json_data['homePhone'] : NULL));
            $employee->setLinkedin((isset($json_data['linkedin']) ? $json_data['linkedin'] : NULL));
            $employee->setViadeo((isset($json_data['viadeo']) ? $json_data['viadeo'] : NULL));
            $employee->setSkype((isset($json_data['skype']) ? $json_data['skype'] : NULL));
            $employee->setTwitter((isset($json_data['twitter']) ? $json_data['twitter'] : NULL));

            if ((isset($json_data['sexe'])) && (is_string($json_data['sexe'])))
                $employee->setGender($em->getRepository('AdminBundle:Gender')->find($json_data['sexe']));
            if ((isset($json_data['paymentmethod'])) && (is_string($json_data['paymentmethod'])))
                $employee->setPaymentMethod($em->getRepository('AdminBundle:SettingsPayment')->find($json_data['paymentmethod']));
            if ((isset($json_data['studylevel'])) && (is_string($json_data['studylevel'])))
                $employee->setStudyLevel($em->getRepository('AdminBundle:SettingsStudyLevel')->find($json_data['studylevel']));
            if ((isset($json_data['studytype'])) && (is_string($json_data['studytype'])))
                $employee->setStudyType($em->getRepository('AdminBundle:SettingsStudyType')->find($json_data['studytype']));
            if ((isset($json_data['family_status'])) && (is_string($json_data['family_status'])))
                $employee->setFamilyStatus($em->getRepository('AdminBundle:hrm_settings_family_status')->find($json_data['family_status']));

            if (isset($json_data['ribAttachments'])) {
                if (is_string($json_data['ribAttachments'])) {
                    if ($employee->getRibAttachments() != null) {
                        $attachment = $employee->getRibAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($json_data['ribAttachments']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $employee->setRibAttachments($attachment);
                }
            }
            $em->persist($employee);
            $em->persist($user);
            $em->flush();

            $response = new \Symfony\Component\BrowserKit\Response('Profile update', 200);
            return $response;
        }
    }

    public function layoutAction()
    {
        return $this->render('profils/layout.html.twig');
    }

    public function getProfilsAction(Request $request)
    {
        $user = $this->get('security.context')->getToken()->getUser();

        if ($user->getAvatars() == "") {
            $user->setAvatars($this->container->get('request')->getBasePath() . '/uploads/avatars/avatar.png');
        } else {
            $user->setAvatars($user->getAvatars());
        }

        return $user;
    }

    public function saveAvatarAction(Request $request)
    {
        sleep(1);
        $ajax = isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
        if ($ajax) {
            $file = $_FILES['avatar'];
//            $user = $_POST['user'];
            if (isset($_POST['user']))
                $user = $_POST['user'];
            else
                $user = $this->get('security.context')->getToken()->getUser()->getId();

            $ret = $this->validateAndSave($file, $user);
            if ($ret != false) {
                return new JsonResponse($ret);
            } else {
                return new JsonResponse(array('status' => 'KO'));
            }
        } else {
            return new JsonResponse(array('status' => 'KO'));
        }
    }

    public function editProfilsAction()
    {
        $usrCnt = $this->get('security.context')->getToken()->getUser();

        if (($usrCnt->getBirthDate() == "") || (!$this->validateDate($usrCnt->getBirthDate()->format('Y-m-d')))) {
            $usrCnt->setBirthDate('');
        } else {
            $usrCnt->setBirthDate($usrCnt->getBirthDate()->format('d-m-Y'));
        }
        $usrCnt->setPassword('');
        $form = $this->createForm(new ProfilsType());

        return $this->render('profils/profils.html.twig', array("form" => $form->createView(), 'data' => $usrCnt));
    }

    public function saveProfilsAction(Request $request)
    {
        $usrCnt = $this->get('security.context')->getToken()->getUser();
        if ($request->getMethod() == 'PUT') {
            $json_data = json_decode($request->getContent(), true);
            $em = $this->getDoctrine()->getEntityManager();
            $retUser = $em->getRepository('UserBundle:hrm_user')->find($usrCnt->getId());
            $form = $this->createForm(new \UserBundle\Form\hrm_userType(), $retUser);
            $form->submit($json_data);
            //defualt employee contract = 1
            //$retUser->setEmployeeContract(1);
            if (isset($json_data['avatars'])) {
                $retUser->setAvatars($json_data['avatars']);
            }
//            if (isset($json_data['birth_date'])) {
//                if (is_string($json_data['birth_date'])) {
//                    $retUser->setBirthDate(new \DateTime($json_data['birth_date']));
//                }
//            } else
//                $retUser->setBirthDate(null);
            //   $retUser->setPlainPassword($json_data['password']);
            $retUser->setLastUpdate(new \DateTime('now'));
            $retUser->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
            if (isset($json_data['ribAttachments'])) {
                if (is_string($json_data['ribAttachments'])) {
                    if ($retUser->getEmployee()->first()->getRibAttachments() != null) {
                        $attachment = $retUser->getEmployee()->first()->getRibAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($json_data['ribAttachments']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $retUser->getEmployee()->first()->setRibAttachments($attachment);
                }
            } else
                $retUser->setRibAttachments(null);
//               if ($request->request->has('attachments')) {
//                if (is_string($request->request->get('attachments'))) {
//                    if ($entity->getAttachments() != null) {
//                        $attachment = $entity->getAttachments();
//                    } else
//                        $attachment = new attachments();
//                    $attachment->setPath($request->request->get('attachments'));
//                    $em->persist($attachment);
//                    $em->flush($attachment);
//                    $entity->setAttachments($attachment);
//                }
//            } else
//                $entity->setAttachments(null);
            // check if exist public profil
            if (!$em->getRepository('UserBundle:IsPublicProfils')->findBy(array('employee' => $usrCnt->getId()))) {
                $this->addNewPPublicProfil($usrCnt);
            }
            // update public profil
            if (isset($json_data['public_profil   ']))
                if ($json_data['public_profil'] != "") {
                    $em->getRepository('UserBundle:IsPublicProfils')->updatePublicProfil(array('employee' => $usrCnt->getId(), 'a_profil' => $json_data['public_profil']));
                }
            $this->getDoctrine()->getManager()->flush();

            return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
        }
        return new JsonResponse(array('error' => array('code' => 'error_method', 'message' => 'Only allow PUT requests')));
    }

    private function validateDate($date)
    {
        $d = \DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') === $date;
    }

    public function getGendersAction()
    {
        $em = $this->getDoctrine()->getManager();
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getProvider()->getId();
        $genders = $this->getDoctrine()->getRepository('AdminBundle:Gender')->SettingsGenderByProvider($provider_id);
        return array('genders' => $genders);
    }

    public function getCountryListAction()
    {
        $countrylist = array(
            'France' => 'France',
            'Tunisia' => 'Tunisia',
            'USA' => 'USA'
        );
        return array('CountryList' => $countrylist);
    }

    private function validateAndSave($file, $user)
    {
        $result = array();
        if (!preg_match('/^image\//', $file['type'])
            //if file type is not an image
            || !preg_match('/\.(jpe?g|gif|png)$/', $file['name'])
            //or extension is not valid
            || getimagesize($file['tmp_name']) === FALSE
            //or file info such as its size can't be determined, so probably an invalid image file
        ) {
            //then there is an error
            return false;
        } else if ($file['size'] > 110000) {
            //if size is larger than what we expect
            return false;
        } else if ($file['error'] != 0 || !is_uploaded_file($file['tmp_name'])) {
            //if there is an unknown error or temporary uploaded file is not what we thought it was
            return false;
        } else {
            //save file inside current directory using a safer version of its name
            $save_path = preg_replace('/[^\w\.\- ]/', '', $file['name']);
            $thumb_path = preg_replace('/\.(.+)$/', '', $save_path) . '-thumb.jpg';

            // Save User Avatar
            $usrCnt = $this->get('security.context')->getToken()->getUser();
            $filename = basename($save_path);
            $filename_thumb = basename($thumb_path);
            $extension = pathinfo($filename, PATHINFO_EXTENSION);
            $newAvatars = md5($filename) . '.' . $extension;
            $newAvatars_thumb = md5($filename) . '__thumb.' . $extension;
            $uploadDir = $this->container->getParameter('web_dir') . '/uploads/avatars/';

            // delete if exist avatars
            $this->deleteAsUserProfils();
            // upload new avatars
            move_uploaded_file($file['tmp_name'], $uploadDir . $newAvatars);
            $this->resize($uploadDir . $newAvatars, $uploadDir . $newAvatars_thumb, 150, 148);
            unlink($uploadDir . $newAvatars);
            // save in DB user table
            $this->saveAsUserProfils($newAvatars_thumb, $user);
            //everything seems OK
            //everything seems OK
            $result['status'] = 'OK';
            $result['message'] = 'Avatar changed successfully!';
            //include new thumbnails `url` in our result and send to browser
            $result['url'] = $this->container->get('request')->getBasePath() . '/uploads/avatars/' . $newAvatars_thumb;
            return $result;
        }
    }

    private function saveAsUserProfils($avatars, $user)
    {
        if ($user)
            $usrCnt = $user;
        else
            $usrCnt = $this->get('security.context')->getToken()->getUser()->getId();

        $em = $this->getDoctrine()->getEntityManager();
        $retUser = $em->getRepository('UserBundle:HrmEmployee')->find($usrCnt);
        $retUser->setAvatars($avatars);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('success' => 'saved!'));
    }

    private function deleteAsUserProfils()
    {
        $usrCnt = $this->get('security.context')->getToken()->getUser();
        $uploadDir = $this->container->getParameter('web_dir') . '/uploads/avatars/';
        if (file_exists($uploadDir . $usrCnt->getAvatars()) && ($usrCnt->getAvatars() != 'avatar.png')) {
            @unlink($uploadDir . $usrCnt->getAvatars());
        }
    }

    private function resize($in_file, $out_file, $new_width, $new_height = FALSE)
    {
        $image = null;
        $extension = strtolower(preg_replace('/^.*\./', '', $in_file));
        switch ($extension) {
            case 'jpg':
            case 'jpeg':
                $image = imagecreatefromjpeg($in_file);
                break;
            case 'png':
                $image = imagecreatefrompng($in_file);
                break;
            case 'gif':
                $image = imagecreatefromgif($in_file);
                break;
        }
        if (!$image || !is_resource($image))
            return false;
        $width = imagesx($image);
        $height = imagesy($image);
        if ($new_height === FALSE) {
            $new_height = (int)(($height * $new_width) / $width);
        }

        $new_image = imagecreatetruecolor($new_width, $new_height);
        imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
        $ret = imagejpeg($new_image, $out_file, 80);
        imagedestroy($new_image);
        imagedestroy($image);
        return $ret;
    }

    private function addNewPPublicProfil($user)
    {
        $c_publicProfil = new IsPublicProfils();
        $c_publicProfil->setEmployee($user);
        $em = $this->getDoctrine()->getManager();
        $em->persist($c_publicProfil);
        $em->flush($c_publicProfil);
    }

    public function postemployeeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, true);
        if ($request->isMethod('POST')) {
            $userManager = $this->get('fos_user.user_manager');
            $email_exist = $userManager->findUserByEmail($data['email']);
            $current_user_id = $this->get('security.context')->getToken()->getUser()->getId();
            $infos_user = $em->getRepository('UserBundle:hrm_user')->find($current_user_id);
            if ($email_exist) {
                return false;
            }
            $user = $userManager->createUser();
            $user->setUsername($data['first_name']);
            $user->setEmail($data['email']);
            $user->setEmailCanonical($data['email']);
            $user->setLocked(0);
            $user->setEnabled(1);
            $user->setCreateuid($this->get('security.context')->getToken()->getUser()->getId());
            $user->setPlainPassword("password");
            $NewEmployee = $this->setNewEmployee($em, $infos_user, $data, $user->getId());

            // $user->setProvider($infos_user->getEmployee->first()->getCompany()->getProvider());
            $user->addRole("ROLE_COLLABORATEUR");
            $username = $user->getUsername();
            $userManager->updateUser($user);
            $form = $this->createForm(new \UserBundle\Form\hrm_userType(), $user);
            $form->submit($data);
            $employee = $em->getRepository('UserBundle:hrm_user')->findOneByUsername($username);
            if (!empty($user)) {
                $employeeContract = new EmployeeContract();
                $employeeContract->setEmployee($NewEmployee);
                $employeeContract->setProvider($infos_user->getEmployee()->first()->getCompany()->getProvider());
                $employeeContract->setCreateDate(new \DateTime('now'));
                $NewEmployee->setEmployeeContract($employeeContract);
                $em->persist($employeeContract);
                $em->flush($employeeContract);
            }
            $NewEmployee->setEmployeeContract($em->getRepository('UserBundle:EmployeeContract')->find($employeeContract->getId()));
            $user->setLastUpdate(new \DateTime('now'));
            $user->setLastUpdateUid($current_user_id);
            if ((isset($data['sexe'])) && (is_string($data['sexe'])))
                $NewEmployee->setGender($em->getRepository('AdminBundle:Gender')->find($data['sexe']));
            if ((isset($data['paymentmethod'])) && (is_string($data['paymentmethod'])))
                $NewEmployee->setPaymentMethod($em->getRepository('AdminBundle:SettingsPayment')->find($data['paymentmethod']));
            if ((isset($data['studylevel'])) && (is_string($data['studylevel'])))
                $NewEmployee->setStudyLevel($em->getRepository('AdminBundle:SettingsStudyLevel')->find($data['studylevel']));
            if ((isset($data['studytype'])) && (is_string($data['studytype'])))
                $NewEmployee->setStudyType($em->getRepository('AdminBundle:SettingsStudyType')->find($data['studytype']));
            if ((isset($data['family_status'])) && (is_string($data['family_status'])))
                $NewEmployee->setFamilyStatus($em->getRepository('AdminBundle:hrm_settings_family_status')->find($data['family_status']));

            if (isset($data['ribAttachments'])) {
                if (is_string($data['ribAttachments']["path"])) {
                    if ($user->getRibAttachments() != null) {
                        $attachment = $user->getRibAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['ribAttachments']['path']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $NewEmployee->setRibAttachments($attachment);
                }
            }

            if (isset($data['avatars'])) {
                if (is_string($data['avatars']["path"])) {
                    $attachment = new attachments();
                    $attachment->setPath($data['avatars']['path']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $NewEmployee->setAvatars($attachment);
                }
            }
            $em->persist($employee);
            $em->persist($user);
            $NewEmployee->setUser($user);
            $em->persist($NewEmployee);
            $em->flush();
            $title = "Votre compte HRM est activé";
            $mail_to = $user->getEmail();
            $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
            $current_user_FName = $this->container->get('security.context')->getToken()->getUser()->getFirstName();
            $current_user_LName = $this->container->get('security.context')->getToken()->getUser()->getLastName();
            $current_user_username = $this->container->get('security.context')->getToken()->getUser()->getUsername();
            if ($user->getUsername()) {
                $to_FName = $user->getFirstName();
                $to_LName = $user->getLastName();
            } else {
                $to_FName = "";
                $to_LName = "";
            }
            $html = $this->renderView('MailingBundle:profil_mails:employee.html.twig', array('to_FName' => $to_FName, 'to_LName' => $to_LName,
                'current_user_FName' => $current_user_FName, 'current_user_LName' => $current_user_LName,
                'username' => $user->getUsername(), 'password' => 'password',
                'current_user_username' => $current_user_username));
            $this->get('mailing.helper.email')->sendEmail($mail_to, $html, $title);
        }
//        $employeeId = $user->getEmployee()->first()->getId();
        $employeeId = $NewEmployee->getId();
        return new JsonResponse(array('employee' => $employeeContract->getId(),
            'userId' => $employee->getId(),
            'employeeId' => $employeeId
        ));
//        return new JsonResponse(array('employee' => $employeeContract->getId()));
    }

    public function collaborateurAction()
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->get('security.context')->getToken()->getUser();
        $infos_user = $em->getRepository('UserBundle:hrm_user')->find($current_user_id);
        $company_id = $infos_user->getEmployee()->first()->getCompany()->getId();
        if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
            $collaborateur = $em->getRepository('UserBundle:HrmEmployee')->FindEmployeesById($current_user_id);
        } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
            $collaborateur = $em->getRepository('UserBundle:HrmEmployee')->FindEmployeeByCompanyId($company_id);
        } else {
            $collaborateur = null;
        }
        $users = $em->getRepository('UserBundle:HrmEmployee')->FindCollaborateurByCompanyId($company_id);
        return array('collaborateur' => $collaborateur, 'users' => $users);
    }

    private function setNewEmployee($em, $infos_user, $data, $user_id)
    {
        $employee = new HrmEmployee();
        $employee->setAddress((isset($data['address']) ? $data['address'] : NULL));
        $employee->setBic((isset($data['bic']) ? $data['bic'] : NULL));
        $employee->setComments((isset($data['comments']) ? $data['comments'] : NULL));
        $employee->setCommune((isset($data['commune']) ? $data['commune'] : NULL));
        $employee->setCompany($infos_user->getEmployee()->first()->getCompany());
        $employee->setCountryCode((isset($data['country_code']) ? $data['country_code'] : NULL));

        $employee->setMobilePhone((isset($data['mobilePhone']) ? $data['mobilePhone'] : NULL));
        $employee->setHomePhone((isset($data['homePhone']) ? $data['homePhone'] : NULL));
        $employee->setLinkedin((isset($data['linkedin']) ? $data['linkedin'] : NULL));
        $employee->setViadeo((isset($data['viadeo']) ? $data['viadeo'] : NULL));
        $employee->setSkype((isset($data['skype']) ? $data['skype'] : NULL));
        $employee->setTwitter((isset($data['twitter']) ? $data['twitter'] : NULL));

        $employee->setDepartmentBirth((isset($data['department_birth']) ? $data['department_birth'] : NULL));
        $employee->setDomiciliation((isset($data['domiciliation']) ? $data['domiciliation'] : NULL));
        $employee->setEmployeeNumber((isset($data['employee_number']) ? $data['employee_number'] : NULL));

        if ((isset($data['sexe'])) && (is_string($data['sexe'])))
            $employee->setGender($em->getRepository('AdminBundle:Gender')->find($data['sexe']));
        if ((isset($data['paymentmethod'])) && (is_string($data['paymentmethod'])))
            $employee->setPaymentMethod($em->getRepository('AdminBundle:SettingsPayment')->find($data['paymentmethod']));
        if ((isset($data['studylevel'])) && (is_string($data['studylevel'])))
            $employee->setStudyLevel($em->getRepository('AdminBundle:SettingsStudyLevel')->find($data['studylevel']));
        if ((isset($data['studytype'])) && (is_string($data['studytype'])))
            $employee->setStudyType($em->getRepository('AdminBundle:SettingsStudyType')->find($data['studytype']));
        if ((isset($data['family_status'])) && (is_string($data['family_status'])))
            $employee->setFamilyStatus($em->getRepository('AdminBundle:hrm_settings_family_status')->find($data['family_status']));

        $employee->setIban((isset($data['iban']) ? $data['iban'] : NULL));
        $employee->setIsHandicape((isset($data['is_handicape']) ? $data['is_handicape'] : NULL));
        $employee->setNationCode((isset($data['nation_code']) ? $data['nation_code'] : NULL));
        $employee->setNumChildren((isset($data['num_children']) ? $data['num_children'] : NULL));
        $employee->setPaymentMethod((isset($data['payment_method']) ? $data['payment_method'] : NULL));
        $employee->setPlaceBirth((isset($data['place_birth']) ? $data['place_birth'] : NULL));
        $employee->setPreference($infos_user->getEmployee()->first()->getPreference());
        $preference = $this->container->get('settingsbundle.preference.service')->getEmpData()->getPreference();
        $newPreference = new hrm_employee_settings();
        $newPreference->setLocal($preference->getLocal());
        $newPreference->setDate($preference->getDate());
        $newPreference->setAmount($preference->getAmount());
        $newPreference->setCreateDate((new \DateTime('now')));
        $newPreference->setLastUpdate((new \DateTime('now')));
        $newPreference->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $newPreference->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $newPreference->setFormat($preference->getFormat());
        $em->persist($newPreference);
        $em->flush();
//        var_dump($preference);die();    
        $employee->setPreference($newPreference);
        $employee->setProvider($this->container->get('settingsbundle.preference.service')->getEmpData()->getProvider());
        $employee->setSinNum((isset($data['sin_num']) ? $data['sin_num'] : NULL));
        $employee->setSsnNum((isset($data['ssn_num']) ? $data['ssn_num'] : NULL));
        $employee->setUser($user_id);
        $employee->setZipCode((isset($data['zip_code']) ? $data['zip_code'] : NULL));
        $em->persist($employee);
        $em->flush();

        return $employee;

    }

    /**
     * Creates a new dashboard entity.
     *
     */
    public function editAction(Request $request)
    {
        $employee = new \UserBundle\Entity\hrm_user();
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
        $infos_user = $em->getRepository('UserBundle:hrm_user')->find($current_user_id);
        $provider = $infos_user->getEmployee()->first()->getCompany()->getProvider()->getId();
        $company = $infos_user->getEmployee()->first()->getCompany()->getId();
        $form = $this->createForm('UserBundle\Form\hrm_userType', $employee, array(
            'user_id' => $company, 'provider' => $provider
        ));
        $form->handleRequest($request);
        return $this->render('collaborator/profile/edit.html.twig', array(
            'employee' => $employee,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new dashboard entity.
     *
     */
    public function newAction(Request $request)
    {
        $employee = new \UserBundle\Entity\hrm_user();
        $em = $this->getDoctrine()->getManager();
        $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
        $infos_user = $em->getRepository('UserBundle:hrm_user')->find($current_user_id);
        $provider = $infos_user->getEmployee()->first()->getCompany()->getProvider()->getId();
        $company = $infos_user->getEmployee()->first()->getCompany()->getId();
        $form = $this->createForm('UserBundle\Form\hrm_userType', $employee, array(
            'user_id' => $company, 'provider' => $provider
        ));
        $form->handleRequest($request);
        return $this->render('collaborator/profile/new.html.twig', array(
            'employee' => $employee,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a new dashboard entity.
     *
     */
    public function editContractAction(Request $request)
    {
        $employeeContract = new \UserBundle\Entity\EmployeeContract();
        $form = $this->createForm('UserBundle\Form\EmployeeContractType', $employeeContract);
        $form->handleRequest($request);
        return $this->render('collaborator/contract/edit.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * Lists all dashboard entities.
     *
     */
    public function predashboardAction()
    {
        if (!$this->get('security.context')->getToken()->getUser())
            return $this->redirectToRoute('fos_user_security_login');
        if (isset($_POST['company'])) {
            $company = $_POST['company'];
            $this->get('session')->set('company', $company);
            return $this->redirectToRoute('dashboard_index');
        } else
            return $this->render('dashboard/company.html.twig');
    }

}
