<?php

namespace UserBundle\Controller;

use UserBundle\Entity\HrmCompany;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Form\HrmCompanyType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Hrmcompany controller.
 *
 */
class HrmCompanyController extends Controller {

    public function layoutAction() {
        return $this->render('company/layout.html.twig');
    }

    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsCustomerType = $this->getDoctrine()->getRepository('UserBundle:HrmCompany')->CompanyByProvider($provider_id);
        return new JsonResponse(array(
            'settingsCustomerType' => $settingsCustomerType,
        ));
    }

    public function getCompanyAction() {

        $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $hrmCompany = $this->getDoctrine()->getRepository('UserBundle:HrmCompany')->CompanyById($companyId);
        return new JsonResponse(array(
            'company' => $hrmCompany,
        ));
    }

    /**
     * Finds and displays a hrmCompany entity.
     *
     */
    public function showAction() {
        $companyId = $this->container->get('settingsbundle.preference.service')->getCompanyId();
        $company = $this->getDoctrine()->getRepository('UserBundle:HrmCompany')->find($companyId); //->CompanyByProvider($providerId);
        return $this->render('company/show.html.twig', array(
                    'company' => $company,
        ));
    }

    public function saveAction(Request $request) {
        $company = new Hrmcompany();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new HrmCompanyType(), $company);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $company->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        if ((isset($data['customerType'])) && is_int($data['customerType'])) {
            $customerType = $em->getRepository('AdminBundle:hrm_settings_customertype')->find($data['customerType']);
            $company->setCustomerType($customerType);
        }
        if ((isset($data['industrySector'])) && is_int($data['industrySector'])) {
            $industrySectorret = $em->getRepository('AdminBundle:hrm_settings_industrysector')->find($data['industrySector']);
            $company->setIndustrySector($industrySectorret);
        }
        $CurrentUser = $this->get('security.context')->getToken()->getUser();
        $company->setCreateDate(new \DateTime('now'));
        $company->setCreateUid($CurrentUser);
        $company->setLastUpdateUid($CurrentUser);
        $company->setLastUpdate(new \DateTime('now'));
        $em->persist($company);
        $em->flush();
        return new JsonResponse(array('http_code' => 200,
            "message" => array("Élément Ajouté avec succès"),
            "company" => array($company->getId())));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }
        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $companyId = $this->container->get('settingsbundle.preference.service')->getCompanyId();
        $HrmCompany = $this->getDoctrine()->getRepository('UserBundle:HrmCompany')->find($companyId); //->CompanyByProvider($providerId);
        $form = $this->createForm('UserBundle\Form\HrmCompanyType', $HrmCompany);
        $form->submit($data);
        $CurrentUser = $this->get('security.context')->getToken()->getUser();
        if (isset($data['customerType'])) {
            $customerType = $em->getRepository('AdminBundle:hrm_settings_customertype')->find($data['customerType']);
            $HrmCompany->setCustomerType($customerType);
        }
        if (isset($data['industrySector'])) {
            $industrySector = $em->getRepository('AdminBundle:hrm_settings_industrysector')->find($data['industrySector']);
            $HrmCompany->setIndustrySector($industrySector);
        }
        $HrmCompany->setCreateDate(new \DateTime('now'));
        $HrmCompany->setCreateUid($CurrentUser);
        $HrmCompany->setLastUpdateUid($CurrentUser);
        $HrmCompany->setLastUpdate(new \DateTime('now'));
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    /**
     * Lists all hrmCompany entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $hrmCompanies = $em->getRepository('UserBundle:HrmCompany')->findAll();
        return $this->render('company/index.html.twig', array(
                    'hrmCompanies' => $hrmCompanies,
        ));
    }

    /**
     * Creates a new hrmCompany entity.
     *
     */
    public function newAction(Request $request) {
        $hrmCompany = new Hrmcompany();
        $provider = $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $form = $this->createForm('HiringBundle\Form\HrmCompanyType', null, array(
            'provider' => $provider
        ));

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($hrmCompany);
            $em->flush();
            return $this->redirectToRoute('hrmcompany_show', array('id' => $hrmCompany->getId()));
        }
        return $this->render('company/new.html.twig', array(
                    'hrmCompany' => $hrmCompany,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrmCompany entity.
     *
     */
    public function editAction() {
        $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $hrmCompany = $this->getDoctrine()->getRepository('UserBundle:HrmCompany')->find($companyId);
        $provider = $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $form = $this->createForm('UserBundle\Form\HrmCompanyType', null, array(
            'provider' => $provider
        ));
        return $this->render('company/edit.html.twig', array(
                    'hrmCompany' => $hrmCompany,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Deletes a hrmCompany entity.
     *
     */
    public function deleteAction(Request $request, HrmCompany $hrmCompany) {
        $form = $this->createDeleteForm($hrmCompany);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrmCompany);
            $em->flush();
        }

        return $this->redirectToRoute('hrmcompany_index');
    }

    /**
     * Creates a form to delete a hrmCompany entity.
     *
     * @param HrmCompany $hrmCompany The hrmCompany entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(HrmCompany $hrmCompany) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('hrmcompany_delete', array('id' => $hrmCompany->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    public function singlePageLayoutAction() {
        return $this->render('company/single_page_layout.html.twig');
    }

}
