<?php

namespace UserBundle\Controller;

use FOS\UserBundle\Controller\RegistrationController as BaseController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RegistrationController extends BaseController
{

    /**
     * Override Action Request register user 
     */
    public function registerAction()
    {
        $form = $this->container->get('fos_user.registration.form');
        $formHandler = $this->container->get('fos_user.registration.form.handler');
        $confirmationEnabled = $this->container->getParameter('fos_user.registration.confirmation.enabled');

        $process = $formHandler->process($confirmationEnabled);
        if ($process) {
            $user = $form->getData();

            $authUser = false;
            if ($confirmationEnabled) {
                $this->container->get('session')->set('fos_user_send_confirmation_email/email', $user->getEmail());
                $route = 'fos_user_registration_check_email';
            } else {
                $authUser = true;
                $route = 'fos_user_registration_confirmed';
            }
            /*************** ADD SOME DATA *******************/
            $em = $this->container->get('doctrine')->getEntityManager();
            // company
            $company = new \UserBundle\Entity\HrmCompany();
            $company->setProvider($em->getRepository('AdminBundle:SettingsProvider')->find(1));
            $em->persist($company);
            // employee
            $employee = new \UserBundle\Entity\HrmEmployee();
            $employee->setCompany($company);
            $employee->setUser($user);
            $employee->setProvider($em->getRepository('AdminBundle:SettingsProvider')->find(1));
            $em->persist($employee);
            $em->flush();
            $this->container->get('PayPeriodBundle.service')->openPayPeriod($em, $employee, $company);
             /*************** ADD SOME DATA *******************/
            
            $this->setFlash('fos_user_success', 'L\'utilisateur a été créé avec succès');
            $url = $this->container->get('router')->generate($route);
            $response = new RedirectResponse($url);

//            if ($authUser) {
//                $this->authenticateUser($user, $response);
//            }

            return $response;
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:register.html.'.$this->getEngine(), array(
            'form' => $form->createView(),
        ));
    } 
    
    /**
     * Tell the user to check his email provider
     */
    public function checkEmailAction()
    {
        $email = $this->container->get('session')->get('fos_user_send_confirmation_email/email');
        $this->container->get('session')->remove('fos_user_send_confirmation_email/email');
        $user = $this->container->get('fos_user.user_manager')->findUserByEmail($email);

        if (null === $user) {
           return new RedirectResponse($this->container->get('router')->generate('fos_user_security_login'));
          //throw new NotFoundHttpException(sprintf('The user with email "%s" does not exist', $email));
        }

        return $this->container->get('templating')->renderResponse('FOSUserBundle:Registration:checkEmail.html.'.$this->getEngine(), array(
            'user' => $user,
        ));
    }  
    
    /**
     * Receive the confirmation token from user email provider, login the user
     */
    public function confirmAction($token)
    {
        $user = $this->container->get('fos_user.user_manager')->findUserByConfirmationToken($token);

        if (null === $user) {
            return new RedirectResponse($this->container->get('router')->generate('fos_user_security_login'));
           // throw new NotFoundHttpException(sprintf('The user with confirmation token "%s" does not exist', $token));
        }

        $user->setConfirmationToken(null);
        $user->setEnabled(true);
        $user->setLastLogin(new \DateTime());

        $this->container->get('fos_user.user_manager')->updateUser($user);
        $response = new RedirectResponse($this->container->get('router')->generate('fos_user_registration_confirmed'));
        $this->authenticateUser($user, $response);

        return $response;
    }    
}
