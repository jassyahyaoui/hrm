<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UserBundle\Entity\HrmEmployee;
use UserBundle\Entity\hrm_employee_skills;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\hrm_employee_skillsCompany;
use UserBundle\Entity\hrm_employee_skillsLanguage;
use UserBundle\Entity\hrm_employee_skillsOthersCompetenciesTags;
use UserBundle\Entity\hrm_employee_skillsStudy;
use UserBundle\Entity\hrm_employee_skillsTechnology;

/**
 * hrm_employee_skills controller.
 *
 */
class hrm_employee_skillsController extends Controller
{

    public function newAction(Request $request)
    {
        $employee_skills = new hrm_employee_skills();

        $provider = $this->container->get('settingsbundle.preference.service')->getProviderId();
        $form = $this->createForm('UserBundle\Form\hrm_employee_skillsType', $employee_skills, array(
            'provider' => $provider
        ));
        $form->handleRequest($request);
        return $this->render('collaborator/skills/new.html.twig', array(
            'hrm_employee_skills' => $employee_skills,
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request)
    {
        $employee_skills = new hrm_employee_skills();

        $provider = $this->container->get('settingsbundle.preference.service')->getProviderId();
        $form = $this->createForm('UserBundle\Form\hrm_employee_skillsType', $employee_skills, array(
            'provider' => $provider
        ));
        $form->handleRequest($request);
        return $this->render('collaborator/skills/edit.html.twig', array(
            'hrm_employee_skills' => $employee_skills,
            'form' => $form->createView(),
        ));
    }

    public function updateAddAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, true);

        if (isset($data['id'])) {
            $hrm_employee_skills = $em->getRepository('UserBundle:hrm_employee_skills')->find($data['id']);

            if ($request->isMethod('POST')) {

                $SkillsID = $hrm_employee_skills->getId();
                $this->SaveStudy($em, $data['study'], $SkillsID);
                $this->SaveJobs($em, $data['company'], $SkillsID);
                $this->SaveLanguages($em, $data['language'], $SkillsID);
                $this->SaveTechnologies($em, $data['technology'], $SkillsID);
                $this->SaveTags($em, $data['other'], $SkillsID);

                $hrm_employee_skills->setStudyComment($data['studyComment']);
                $hrm_employee_skills->setExperienceComment($data['experienceComment']);
                $em->persist($hrm_employee_skills);
                $em->flush();

                $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
                return $response;

            }
        } else {
            $hrm_employee_skills = new hrm_employee_skills();
            $form = $this->createForm('UserBundle\Form\hrm_employee_skillsType', $hrm_employee_skills);
            $form->handleRequest($request);
            $body = $request->getContent();
            $data = json_decode($body, true);
            $em = $this->getDoctrine()->getManager();
            if ($request->isMethod('POST')) {

                $form->submit($data);
                $hrm_employee_skills->setEmployee($em->getRepository('UserBundle:HrmEmployee')->find($data['employeeId']));
                $em->persist($hrm_employee_skills);
                $em->flush();
                $SkillsID = $hrm_employee_skills->getId();

                $this->SetSkillsInEmployee($em, $data['employeeId'], $SkillsID);
                $this->SaveStudy($em, $data['study'], $SkillsID);
                $this->SaveJobs($em, $data['company'], $SkillsID);
                $this->SaveLanguages($em, $data['language'], $SkillsID);
                $this->SaveTechnologies($em, $data['technology'], $SkillsID);
                $this->SaveTags($em, $data['other'], $SkillsID);

                $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
                return $response;

            }
        }

    }



    public function addAction(Request $request)
    {
        $hrm_employee_skills = new hrm_employee_skills();
        $form = $this->createForm('UserBundle\Form\hrm_employee_skillsType', $hrm_employee_skills);
        $form->handleRequest($request);
        $body = $request->getContent();
        $data = json_decode($body, true);
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {

            $form->submit($data);
            $hrm_employee_skills->setEmployee($em->getRepository('UserBundle:HrmEmployee')->find($data['employeeId']));
            $em->persist($hrm_employee_skills);
            $em->flush();
            $SkillsID = $hrm_employee_skills->getId();

            $this->SetSkillsInEmployee($em, $data['employeeId'], $SkillsID);
            $this->SaveStudy($em, $data['study'], $SkillsID);
            $this->SaveJobs($em, $data['company'], $SkillsID);
            $this->SaveLanguages($em, $data['language'], $SkillsID);
            $this->SaveTechnologies($em, $data['technology'], $SkillsID);
            $this->SaveTags($em, $data['other']['tags'], $SkillsID);

            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;

        }
    }

    public function SetSkillsInEmployee($em, $data, $SkillsID)
    {
        $hrm_employee_entity = $em->getRepository('UserBundle:HrmEmployee')->find($data);
        $hrm_employee_entity->setSkills($em->getRepository('UserBundle:hrm_employee_skills')->find($SkillsID));
        $em->persist($hrm_employee_entity);
        $em->flush();
    }

    public function SaveStudy($em, $data, $SkillsID)
    {
        $StudyID = array();
        for ($i = 0; $i < sizeof($data); $i++) {
            $skills_study = new hrm_employee_skillsStudy();
            $skills_study->setDisplayName($data[$i]['displayName']);
            $skills_study->setStudyType($em->getRepository('AdminBundle:SettingsStudyType')->find($data[$i]['study_type']));
            $skills_study->setStudyLevel($em->getRepository('AdminBundle:SettingsStudyLevel')->find($data[$i]['study_level']));
            $skills_study->setEmployeeSkills($em->getRepository('UserBundle:hrm_employee_skills')->find($SkillsID));
            $em->persist($skills_study);
            $em->flush();
            array_push($StudyID, $skills_study->getId());
        }
        return $StudyID;
    }

    public function SaveJobs($em, $data, $SkillsID)
    {
        $CompanyID = array();
        for ($i = 0; $i < sizeof($data); $i++) {
            $skills_company = new hrm_employee_skillsCompany();
            $skills_company->setDisplayName($data[$i]['displayName']);
            $skills_company->setJob($em->getRepository('AdminBundle:hrm_settings_job')->find($data[$i]['job']));
            $skills_company->setIndustrysector($em->getRepository('AdminBundle:hrm_settings_industrysector')->find($data[$i]['sector']));
            $skills_company->setEmployeeSkills($em->getRepository('UserBundle:hrm_employee_skills')->find($SkillsID));
            $em->persist($skills_company);
            $em->flush();
            array_push($CompanyID, $skills_company->getId());
        }
        return $CompanyID;
    }

    public function SaveLanguages($em, $data, $SkillsID)
    {
        $LanguageID = array();
        for ($i = 0; $i < sizeof($data); $i++) {
            $skills_language = new hrm_employee_skillsLanguage();
            $skills_language->setLanguage($em->getRepository('AdminBundle:hrm_settings_language')->find($data[$i]['language']));
            $skills_language->setLanguageLevel($em->getRepository('AdminBundle:hrm_settings_language_level')->find($data[$i]['language']));
            $skills_language->setEmployeeSkills($em->getRepository('UserBundle:hrm_employee_skills')->find($SkillsID));
            $em->persist($skills_language);
            $em->flush();
            array_push($LanguageID, $skills_language->getId());
        }
        return $LanguageID;
    }

    public function SaveTechnologies($em, $data, $SkillsID)
    {
        $TechnologyID = array();
        for ($i = 0; $i < sizeof($data); $i++) {
            $skills_technology = new hrm_employee_skillsTechnology();
            $skills_technology->setTechnology($em->getRepository('AdminBundle:hrm_settings_technology')->find($data[$i]['technology']));
            $skills_technology->setTechnologyLevel($em->getRepository('AdminBundle:hrm_settings_technology_level')->find($data[$i]['technology_level']));
            $skills_technology->setEmployeeSkills($em->getRepository('UserBundle:hrm_employee_skills')->find($SkillsID));
            $em->persist($skills_technology);
            $em->flush();
            array_push($TechnologyID, $skills_technology->getId());
        }
        return $TechnologyID;
    }

    public function SaveTags($em, $data, $SkillsID)
    {
        $TagsID = array();
        for ($i = 0; $i < sizeof($data); $i++) {
            $skills_tags = new hrm_employee_skillsOthersCompetenciesTags();
            $skills_tags->setTag($data[$i]['text']);
            $skills_tags->setEmployeeSkills($em->getRepository('UserBundle:hrm_employee_skills')->find($SkillsID));
            $em->persist($skills_tags);
            $em->flush();
            array_push($TagsID, $skills_tags->getId());
        }
        return $TagsID;
    }

    public function removeSkillsAction(Request $request, hrm_employee_skills $skills_study)
    {
        $id = $skills_study->getId();
        if ($request->isMethod('DELETE')) {

            $em = $this->getDoctrine()->getManager();
            $entities =
                array(
                    0 => "hrm_employee_skillsStudy",
                    1 => "hrm_employee_skillsCompany",
                    2 => "hrm_employee_skillsLanguage",
                    3 => "hrm_employee_skillsTechnology",
                    4 => "hrm_employee_skillsOthersCompetenciesTags"
                );

            foreach ($entities as $entity) {
                $query = $em->createQuery('DELETE UserBundle:' . str_replace('"', "", $entity) . ' e WHERE e.employeeSkills = :id')
                    ->setParameter("id", $id);
                $result = $query->execute();
            }

            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }

}
