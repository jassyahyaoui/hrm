<?php

namespace UserBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="hrm_user")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\hrm_userRepository")
 * @ORM\AttributeOverrides({
 *     @ORM\AttributeOverride(name="emailCanonical",
 *         column=@ORM\Column(
 *             name="email_canonical",
 *             type="string",
 *             length=255,
 *             unique=false
 *         )
 *     ),
 *     @ORM\AttributeOverride(name="username",
 *         column=@ORM\Column(
 *             name="username",
 *             type="string",
 *             length=255,
 *             nullable=true
 *         )
 *     ),
 * })
 */
class hrm_user extends BaseUser {

    /**
     * @ORM\Id
     * @ORM\Column(name="uid", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var int
     *
     * @ORM\Column(name="seqno", type="integer",nullable=true)
     */
    protected $seqno;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\HrmEmployee", mappedBy="user")
     */
    protected $employee;
    
    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=45,nullable=true)
     */
    protected $first_name;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=45,nullable=true)
     */
    protected $last_name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name_usage", type="string", length=45,nullable=true)
     */
    protected $name_usage;
    
    /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsProvider")
    * @ORM\JoinColumn(name="provider_id", referencedColumnName="uid")
    */
    protected $provider;
	
    /**
     * @var DateTime
     *
     * @ORM\Column(name="create_date", type="datetime",nullable=true)
     */
    protected $create_date;

    /**
     * @var int
     *
     * @ORM\Column(name="create_uid", type="integer", nullable=true)
     */
    protected $createUid;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    protected $last_update;

    /**
     * @var int
     *
     * @ORM\Column(name="last_update_uid", type="integer", nullable=true)
     */
    protected $last_update_uid;    
    /**
     * Constructor
     */
    public function __construct()
    {
        parent::__construct();
        // your own logic
    
        $this->employee = new \Doctrine\Common\Collections\ArrayCollection();
    }

 

    /**
     * Set seqno
     *
     * @param integer $seqno
     *
     * @return hrm_user
     */
    public function setSeqno($seqno)
    {
        $this->seqno = $seqno;

        return $this;
    }

    /**
     * Get seqno
     *
     * @return integer
     */
    public function getSeqno()
    {
        return $this->seqno;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return hrm_user
     */
    public function setFirstName($firstName)
    {
        $this->first_name = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->first_name;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return hrm_user
     */
    public function setLastName($lastName)
    {
        $this->last_name = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->last_name;
    }

    /**
     * Set nameUsage
     *
     * @param string $nameUsage
     *
     * @return hrm_user
     */
    public function setNameUsage($nameUsage)
    {
        $this->name_usage = $nameUsage;

        return $this;
    }

    /**
     * Get nameUsage
     *
     * @return string
     */
    public function getNameUsage()
    {
        return $this->name_usage;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_user
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_user
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_user
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_user
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->last_update_uid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->last_update_uid;
    }

    /**
     * Add employee
     *
     * @param \UserBundle\Entity\HrmEmployee $employee
     *
     * @return hrm_user
     */
    public function addEmployee(\UserBundle\Entity\HrmEmployee $employee)
    {
        $this->employee[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param \UserBundle\Entity\HrmEmployee $employee
     */
    public function removeEmployee(\UserBundle\Entity\HrmEmployee $employee)
    {
        $this->employee->removeElement($employee);
    }

    /**
     * Get employee
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set provider
     *
     * @param \AdminBundle\Entity\SettingsProvider $provider
     *
     * @return hrm_user
     */
    public function setProvider(\AdminBundle\Entity\SettingsProvider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \AdminBundle\Entity\SettingsProvider
     */
    public function getProvider()
    {
        return $this->provider;
    }
}
