<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_employee_skills
 *
 * @ORM\Table(name="hrm_employee_skills")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\hrm_employee_skillsRepository")
 */
class hrm_employee_skills
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\HrmEmployee", inversedBy="skills")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="uid")
     */
    private $employee;

    /**
     * @var string
     *
     * @ORM\Column(name="studyComment", type="text", nullable=true)
     */
    private $studyComment;

    /**
     * @var string
     *
     * @ORM\Column(name="experienceComment", type="text", nullable=true)
     */
    private $experienceComment;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\hrm_employee_skillsStudy", mappedBy="employeeSkills")
     */
    private $skills_study;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\hrm_employee_skillsCompany", mappedBy="employeeSkills")
     */
    private $skills_company;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\hrm_employee_skillsLanguage", mappedBy="employeeSkills")
     */
    private $skills_language;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\hrm_employee_skillsTechnology", mappedBy="employeeSkills")
     */
    private $skills_technology;
//
    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\hrm_employee_skillsOthersCompetenciesTags", mappedBy="employeeSkills")
     */
    private $skills_other;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->skills_study = new \Doctrine\Common\Collections\ArrayCollection();
        $this->skills_company = new \Doctrine\Common\Collections\ArrayCollection();
        $this->skills_language = new \Doctrine\Common\Collections\ArrayCollection();
        $this->skills_technology = new \Doctrine\Common\Collections\ArrayCollection();
        $this->skills_other = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set studyComment
     *
     * @param string $studyComment
     *
     * @return hrm_employee_skills
     */
    public function setStudyComment($studyComment)
    {
        $this->studyComment = $studyComment;

        return $this;
    }

    /**
     * Get studyComment
     *
     * @return string
     */
    public function getStudyComment()
    {
        return $this->studyComment;
    }

    /**
     * Set experienceComment
     *
     * @param string $experienceComment
     *
     * @return hrm_employee_skills
     */
    public function setExperienceComment($experienceComment)
    {
        $this->experienceComment = $experienceComment;

        return $this;
    }

    /**
     * Get experienceComment
     *
     * @return string
     */
    public function getExperienceComment()
    {
        return $this->experienceComment;
    }


    /**
     * Add skillsStudy
     *
     * @param \UserBundle\Entity\hrm_employee_skillsStudy $skillsStudy
     *
     * @return hrm_employee_skills
     */
    public function addSkillsStudy(\UserBundle\Entity\hrm_employee_skillsStudy $skillsStudy)
    {
        $this->skills_study[] = $skillsStudy;

        return $this;
    }

    /**
     * Remove skillsStudy
     *
     * @param \UserBundle\Entity\hrm_employee_skillsStudy $skillsStudy
     */
    public function removeSkillsStudy(\UserBundle\Entity\hrm_employee_skillsStudy $skillsStudy)
    {
        $this->skills_study->removeElement($skillsStudy);
    }

    /**
     * Get skillsStudy
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSkillsStudy()
    {
        return $this->skills_study;
    }

    /**
     * Add skillsCompany
     *
     * @param \UserBundle\Entity\hrm_employee_skillsCompany $skillsCompany
     *
     * @return hrm_employee_skills
     */
    public function addSkillsCompany(\UserBundle\Entity\hrm_employee_skillsCompany $skillsCompany)
    {
        $this->skills_company[] = $skillsCompany;

        return $this;
    }

    /**
     * Remove skillsCompany
     *
     * @param \UserBundle\Entity\hrm_employee_skillsCompany $skillsCompany
     */
    public function removeSkillsCompany(\UserBundle\Entity\hrm_employee_skillsCompany $skillsCompany)
    {
        $this->skills_company->removeElement($skillsCompany);
    }

    /**
     * Get skillsCompany
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSkillsCompany()
    {
        return $this->skills_company;
    }

    /**
     * Add skillsLanguage
     *
     * @param \UserBundle\Entity\hrm_employee_skillsLanguage $skillsLanguage
     *
     * @return hrm_employee_skills
     */
    public function addSkillsLanguage(\UserBundle\Entity\hrm_employee_skillsLanguage $skillsLanguage)
    {
        $this->skills_language[] = $skillsLanguage;

        return $this;
    }

    /**
     * Remove skillsLanguage
     *
     * @param \UserBundle\Entity\hrm_employee_skillsLanguage $skillsLanguage
     */
    public function removeSkillsLanguage(\UserBundle\Entity\hrm_employee_skillsLanguage $skillsLanguage)
    {
        $this->skills_language->removeElement($skillsLanguage);
    }

    /**
     * Get skillsLanguage
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSkillsLanguage()
    {
        return $this->skills_language;
    }

    /**
     * Add skillsTechnology
     *
     * @param \UserBundle\Entity\hrm_employee_skillsTechnology $skillsTechnology
     *
     * @return hrm_employee_skills
     */
    public function addSkillsTechnology(\UserBundle\Entity\hrm_employee_skillsTechnology $skillsTechnology)
    {
        $this->skills_technology[] = $skillsTechnology;

        return $this;
    }

    /**
     * Remove skillsTechnology
     *
     * @param \UserBundle\Entity\hrm_employee_skillsTechnology $skillsTechnology
     */
    public function removeSkillsTechnology(\UserBundle\Entity\hrm_employee_skillsTechnology $skillsTechnology)
    {
        $this->skills_technology->removeElement($skillsTechnology);
    }

    /**
     * Get skillsTechnology
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSkillsTechnology()
    {
        return $this->skills_technology;
    }

    /**
     * Add skillsOther
     *
     * @param \UserBundle\Entity\hrm_employee_skillsOthersCompetenciesTags $skillsOther
     *
     * @return hrm_employee_skills
     */
    public function addSkillsOther(\UserBundle\Entity\hrm_employee_skillsOthersCompetenciesTags $skillsOther)
    {
        $this->skills_other[] = $skillsOther;

        return $this;
    }

    /**
     * Remove skillsOther
     *
     * @param \UserBundle\Entity\hrm_employee_skillsOthersCompetenciesTags $skillsOther
     */
    public function removeSkillsOther(\UserBundle\Entity\hrm_employee_skillsOthersCompetenciesTags $skillsOther)
    {
        $this->skills_other->removeElement($skillsOther);
    }

    /**
     * Get skillsOther
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSkillsOther()
    {
        return $this->skills_other;
    }


    /**
     * Set employee
     *
     * @param \UserBundle\Entity\HrmEmployee $employee
     *
     * @return hrm_employee_skills
     */
    public function setEmployee(\UserBundle\Entity\HrmEmployee $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getEmployee()
    {
        return $this->employee;
    }
}
