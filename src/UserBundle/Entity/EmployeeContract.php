<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmployeeContract
 *
 * @ORM\Table(name="hrm_employee_contract")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\EmployeeContractRepository")
 */
class EmployeeContract
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   /**
    * @ORM\OneToOne(targetEntity="UserBundle\Entity\HrmEmployee")
    * @ORM\JoinColumn(name="employee_id", referencedColumnName="uid")
    */
    private $employee;   
         
   /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsEmpStatus")
    * @ORM\JoinColumn(name="emp_status_id", referencedColumnName="uid")
    */
    private $empStatus;

   /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\TypeContracts")
    * @ORM\JoinColumn(name="contract_type_id", referencedColumnName="uid")
    */    
    private $contractType;

    /**
     * @var string
     *
     * @ORM\Column(name="convention", type="string", length=255, nullable=true)
     */
    private $convention;
    
    
    /**
     * @var string
     *
     * @ORM\Column(name="contract_custom_condition", type="string", length=45,nullable=true))
     */
    private $contractCustomCondition;

    /**
     * @var string
     *
     * @ORM\Column(name="sal_classification", type="string", length=45, nullable=true)
     */
    private $salClassification;

    /**
     * @var string
     *
     * @ORM\Column(name="sal_grade", type="string", length=45, nullable=true)
     */
    private $salGrade;

    /**
     * @var string
     *
     * @ORM\Column(name="sal_echelon", type="string", length=45, nullable=true)
     */
    private $salEchelon;

    /**
     * @var string
     *
     * @ORM\Column(name="sal_job_title", type="string", length=45, nullable=true)
     */
    private $salJobTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="sal_payment_currency_id", type="string", length=45,nullable=true)
     */
    private $salPaymentCurrencyId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sal_joined_date", type="datetime",nullable=true)
     */
    private $salJoinedDate;
    
   /**
     * @var \DateTime
     *
     * @ORM\Column(name="sal_left_date", type="datetime",nullable=true)
     */
    private $salLeftDate;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sal_seniority_date", type="datetime",nullable=true)
     */
    private $salSeniorityDate;

    /**
     * @var string
     *
     * @ORM\Column(name="insurance_contract_ref1", type="string", length=45,nullable=true)
     */
    private $insuranceContractRef1;

    /**
     * @var string
     *
     * @ORM\Column(name="insurance_contract_ref2", type="string", length=45,nullable=true)
     */
    private $insuranceContractRef2;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="string", length=45,nullable=true)
     */
    private $notes;


       /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsProvider")
    * @ORM\JoinColumn(name="provider_id", referencedColumnName="uid")
    */
    private $provider;
	
    /**
     * @var DateTime
     *
     * @ORM\Column(name="create_date", type="datetime",nullable=true)
     */
    private $create_date;
    
    /**
     * @var int
     *
     * @ORM\Column(name="create_uid", type="integer", nullable=true)
     */
    private $createUid;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $last_update;

    /**
     * @var int
     *
     * @ORM\Column(name="last_update_uid", type="integer", nullable=true)
     */
    private $last_update_uid;

     /**
     * @var string
     *
     * @ORM\Column(name="contract_number", type="string", length=45,nullable=true)
     */
    private $contract_number;
    
    /**
     * @var string
     *
     * @ORM\Column(name="work_timetable", type="string", length=45,nullable=true)
     */
    private $work_timetable;
    
     /**
     * @var string
     *
     * @ORM\Column(name="working_time", type="string", length=45,nullable=true)
     */
    private $working_time;
    
     /**
     * @var int
     *
     * @ORM\Column(name="fixed_rest_days_agreed", type="integer", nullable=true)
     */
    private $fixed_rest_days_agreed;
    
     /**
     * @var int
     *
     * @ORM\Column(name="gross_annual_base_salary", type="integer", nullable=true)
     */
    private $gross_annual_base_salary;
    
     /**
     * @var int
     *
     * @ORM\Column(name="nb_of_installments", type="integer", nullable=true)
     */
    private $nb_of_installments;
  
    /**
    * @ORM\OneToOne(targetEntity="\FileBundle\Entity\attachments")
    * @ORM\JoinColumn(name="hrm_employee_contrat_attachment_1_id", referencedColumnName="uid")
    */
    private $contractAttachment;
    
    /**
    * @ORM\OneToOne(targetEntity="\FileBundle\Entity\attachments")
    * @ORM\JoinColumn(name="hrm_employee_contrat_attachment_2_id", referencedColumnName="uid")
    */
    private $identificationAttachment;
    
      /**
    * @ORM\OneToOne(targetEntity="\FileBundle\Entity\attachments")
    * @ORM\JoinColumn(name="hrm_employee_contrat_attachment_3_id", referencedColumnName="uid")
    */
    private $autorsationAttachment;
    
      /**
    * @ORM\OneToOne(targetEntity="\FileBundle\Entity\attachments")
    * @ORM\JoinColumn(name="hrm_employee_contrat_attachment_4_id", referencedColumnName="uid")
    */
    private $vitalCardAttachment;
    
      /**
    * @ORM\OneToOne(targetEntity="\FileBundle\Entity\attachments")
    * @ORM\JoinColumn(name="hrm_employee_contrat_attachment_5_id", referencedColumnName="uid")
    */
    private $singleDeclarationAttachment;
    
      /**
    * @ORM\OneToOne(targetEntity="\FileBundle\Entity\attachments")
    * @ORM\JoinColumn(name="hrm_employee_contrat_attachment_6_id", referencedColumnName="uid")
    */
    private $affiliationPensionAttachment;
        
      /**
    * @ORM\OneToOne(targetEntity="\FileBundle\Entity\attachments")
    * @ORM\JoinColumn(name="hrm_employee_contrat_attachment_7_id", referencedColumnName="uid")
    */
    private $affiliationHealthAttachment;
    
      /**
    * @ORM\OneToOne(targetEntity="\FileBundle\Entity\attachments")
    * @ORM\JoinColumn(name="hrm_employee_contrat_attachment_8_id", referencedColumnName="uid")
    */
    private $affiliationComplementaryPensionAttachment;
    
      /**
    * @ORM\OneToOne(targetEntity="\FileBundle\Entity\attachments")
    * @ORM\JoinColumn(name="hrm_employee_contrat_attachment_9_id", referencedColumnName="uid")
    */
    private $otherAttachment;
    
    
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contractCustomCondition
     *
     * @param string $contractCustomCondition
     *
     * @return EmployeeContract
     */
    public function setContractCustomCondition($contractCustomCondition)
    {
        $this->contractCustomCondition = $contractCustomCondition;

        return $this;
    }

    /**
     * Get contractCustomCondition
     *
     * @return string
     */
    public function getContractCustomCondition()
    {
        return $this->contractCustomCondition;
    }

    /**
     * Set salClassification
     *
     * @param string $salClassification
     *
     * @return EmployeeContract
     */
    public function setSalClassification($salClassification)
    {
        $this->salClassification = $salClassification;

        return $this;
    }

    /**
     * Get salClassification
     *
     * @return string
     */
    public function getSalClassification()
    {
        return $this->salClassification;
    }

    /**
     * Set salGrade
     *
     * @param string $salGrade
     *
     * @return EmployeeContract
     */
    public function setSalGrade($salGrade)
    {
        $this->salGrade = $salGrade;

        return $this;
    }

    /**
     * Get salGrade
     *
     * @return string
     */
    public function getSalGrade()
    {
        return $this->salGrade;
    }

    /**
     * Set salEchelon
     *
     * @param string $salEchelon
     *
     * @return EmployeeContract
     */
    public function setSalEchelon($salEchelon)
    {
        $this->salEchelon = $salEchelon;

        return $this;
    }

    /**
     * Get salEchelon
     *
     * @return string
     */
    public function getSalEchelon()
    {
        return $this->salEchelon;
    }

    /**
     * Set salJobTitle
     *
     * @param string $salJobTitle
     *
     * @return EmployeeContract
     */
    public function setSalJobTitle($salJobTitle)
    {
        $this->salJobTitle = $salJobTitle;

        return $this;
    }

    /**
     * Get salJobTitle
     *
     * @return string
     */
    public function getSalJobTitle()
    {
        return $this->salJobTitle;
    }

    /**
     * Set salPaymentCurrencyId
     *
     * @param string $salPaymentCurrencyId
     *
     * @return EmployeeContract
     */
    public function setSalPaymentCurrencyId($salPaymentCurrencyId)
    {
        $this->salPaymentCurrencyId = $salPaymentCurrencyId;

        return $this;
    }

    /**
     * Get salPaymentCurrencyId
     *
     * @return string
     */
    public function getSalPaymentCurrencyId()
    {
        return $this->salPaymentCurrencyId;
    }

    /**
     * Set salJoinedDate
     *
     * @param \DateTime $salJoinedDate
     *
     * @return EmployeeContract
     */
    public function setSalJoinedDate($salJoinedDate)
    {
        $this->salJoinedDate = $salJoinedDate;

        return $this;
    }

    /**
     * Get salJoinedDate
     *
     * @return \DateTime
     */
    public function getSalJoinedDate()
    {
        return $this->salJoinedDate;
    }

     /**
     * Get salJoinedDate
     *
     * @return string
     */
    public function getSalJoinedDateS()
    {   
        if($this->salJoinedDate != null)
        return $this->getSalJoinedDate()->format('d/m/Y');
        else
        return "";
    }

       /**
     * Get salLeftDate
     *
     * @return string
     */
    public function getSalLeftDateS()
    {   
        if($this->salLeftDate != null)
        return $this->getSalLeftDate()->format('d/m/Y');
        else
        return "";
    }
    
       /**
     * Get salSeniorityDate
     *
     * @return string
     */
    public function getSalSeniorityDateS()
    {   
        if($this->salSeniorityDate != null)
        return $this->getSalSeniorityDate()->format('d/m/Y');
        else
        return "";
    }
    
    /**
     * Set salSeniorityDate
     *
     * @param \DateTime $salSeniorityDate
     *
     * @return EmployeeContract
     */
    public function setSalSeniorityDate($salSeniorityDate)
    {
        $this->salSeniorityDate = $salSeniorityDate;

        return $this;
    }

    /**
     * Get salSeniorityDate
     *
     * @return \DateTime
     */
    public function getSalSeniorityDate()
    {
        return $this->salSeniorityDate;
    }

    /**
     * Set insuranceContractRef1
     *
     * @param string $insuranceContractRef1
     *
     * @return EmployeeContract
     */
    public function setInsuranceContractRef1($insuranceContractRef1)
    {
        $this->insuranceContractRef1 = $insuranceContractRef1;

        return $this;
    }

    /**
     * Get insuranceContractRef1
     *
     * @return string
     */
    public function getInsuranceContractRef1()
    {
        return $this->insuranceContractRef1;
    }

    /**
     * Set insuranceContractRef2
     *
     * @param string $insuranceContractRef2
     *
     * @return EmployeeContract
     */
    public function setInsuranceContractRef2($insuranceContractRef2)
    {
        $this->insuranceContractRef2 = $insuranceContractRef2;

        return $this;
    }

    /**
     * Get insuranceContractRef2
     *
     * @return string
     */
    public function getInsuranceContractRef2()
    {
        return $this->insuranceContractRef2;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return EmployeeContract
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

  

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return EmployeeContract
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return EmployeeContract
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return EmployeeContract
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return EmployeeContract
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->last_update_uid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->last_update_uid;
    }

    /**
     * Set employee
     *
     * @param \UserBundle\Entity\HrmEmployee $employee
     *
     * @return EmployeeContract
     */
    public function setEmployee(\UserBundle\Entity\HrmEmployee $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getEmployee()
    {
        return $this->employee;
    }

    /**
     * Set empStatus
     *
     * @param \AdminBundle\Entity\SettingsEmpStatus $empStatus
     *
     * @return EmployeeContract
     */
    public function setEmpStatus(\AdminBundle\Entity\SettingsEmpStatus $empStatus = null)
    {
        $this->empStatus = $empStatus;

        return $this;
    }

    /**
     * Get empStatus
     *
     * @return \AdminBundle\Entity\SettingsEmpStatus
     */
    public function getEmpStatus()
    {
        return $this->empStatus;
    }

    /**
     * Set contractType
     *
     * @param \AdminBundle\Entity\TypeContracts $contractType
     *
     * @return EmployeeContract
     */
    public function setContractType(\AdminBundle\Entity\TypeContracts $contractType = null)
    {
        $this->contractType = $contractType;

        return $this;
    }

    /**
     * Get contractType
     *
     * @return \AdminBundle\Entity\TypeContracts
     */
    public function getContractType()
    {
        return $this->contractType;
    }

    /**
     * Set salLeftDate
     *
     * @param \DateTime $salLeftDate
     *
     * @return EmployeeContract
     */
    public function setSalLeftDate($salLeftDate)
    {
        $this->salLeftDate = $salLeftDate;

        return $this;
    }

    /**
     * Get salLeftDate
     *
     * @return \DateTime
     */
    public function getSalLeftDate()
    {
        return $this->salLeftDate;
    }

    
    /**
     * Set provider
     *
     * @param \AdminBundle\Entity\SettingsProvider $provider
     *
     * @return EmployeeContract
     */
    public function setProvider(\AdminBundle\Entity\SettingsProvider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \AdminBundle\Entity\SettingsProvider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set convention
     *
     * @param string $convention
     *
     * @return EmployeeContract
     */
    public function setConvention($convention)
    {
        $this->convention = $convention;

        return $this;
    }

    /**
     * Get convention
     *
     * @return string
     */
    public function getConvention()
    {
        return $this->convention;
    }

    /**
     * Set contractNumber
     *
     * @param string $contractNumber
     *
     * @return EmployeeContract
     */
    public function setContractNumber($contractNumber)
    {
        $this->contract_number = $contractNumber;

        return $this;
    }

    /**
     * Get contractNumber
     *
     * @return string
     */
    public function getContractNumber()
    {
        return $this->contract_number;
    }

    /**
     * Set workTimetable
     *
     * @param string $workTimetable
     *
     * @return EmployeeContract
     */
    public function setWorkTimetable($workTimetable)
    {
        $this->work_timetable = $workTimetable;

        return $this;
    }

    /**
     * Get workTimetable
     *
     * @return string
     */
    public function getWorkTimetable()
    {
        return $this->work_timetable;
    }

    /**
     * Set workingTime
     *
     * @param string $workingTime
     *
     * @return EmployeeContract
     */
    public function setWorkingTime($workingTime)
    {
        $this->working_time = $workingTime;

        return $this;
    }

    /**
     * Get workingTime
     *
     * @return string
     */
    public function getWorkingTime()
    {
        return $this->working_time;
    }

    /**
     * Set fixedRestDaysAgreed
     *
     * @param integer $fixedRestDaysAgreed
     *
     * @return EmployeeContract
     */
    public function setFixedRestDaysAgreed($fixedRestDaysAgreed)
    {
        $this->fixed_rest_days_agreed = $fixedRestDaysAgreed;

        return $this;
    }

    /**
     * Get fixedRestDaysAgreed
     *
     * @return integer
     */
    public function getFixedRestDaysAgreed()
    {
        return $this->fixed_rest_days_agreed;
    }

    /**
     * Set grossAnnualBaseSalary
     *
     * @param integer $grossAnnualBaseSalary
     *
     * @return EmployeeContract
     */
    public function setGrossAnnualBaseSalary($grossAnnualBaseSalary)
    {
        $this->gross_annual_base_salary = $grossAnnualBaseSalary;

        return $this;
    }

    /**
     * Get grossAnnualBaseSalary
     *
     * @return integer
     */
    public function getGrossAnnualBaseSalary()
    {
        return $this->gross_annual_base_salary;
    }

    /**
     * Set nbOfInstallments
     *
     * @param integer $nbOfInstallments
     *
     * @return EmployeeContract
     */
    public function setNbOfInstallments($nbOfInstallments)
    {
        $this->nb_of_installments = $nbOfInstallments;

        return $this;
    }

    /**
     * Get nbOfInstallments
     *
     * @return integer
     */
    public function getNbOfInstallments()
    {
        return $this->nb_of_installments;
    }

    /**
     * Set contractAttachment
     *
     * @param \FileBundle\Entity\attachments $contractAttachment
     *
     * @return EmployeeContract
     */
    public function setContractAttachment(\FileBundle\Entity\attachments $contractAttachment = null)
    {
        $this->contractAttachment = $contractAttachment;

        return $this;
    }

    /**
     * Get contractAttachment
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getContractAttachment()
    {
        return $this->contractAttachment;
    }

    /**
     * Set identificationAttachment
     *
     * @param \FileBundle\Entity\attachments $identificationAttachment
     *
     * @return EmployeeContract
     */
    public function setIdentificationAttachment(\FileBundle\Entity\attachments $identificationAttachment = null)
    {
        $this->identificationAttachment = $identificationAttachment;

        return $this;
    }

    /**
     * Get identificationAttachment
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getIdentificationAttachment()
    {
        return $this->identificationAttachment;
    }

    /**
     * Set autorsationAttachment
     *
     * @param \FileBundle\Entity\attachments $autorsationAttachment
     *
     * @return EmployeeContract
     */
    public function setAutorsationAttachment(\FileBundle\Entity\attachments $autorsationAttachment = null)
    {
        $this->autorsationAttachment = $autorsationAttachment;

        return $this;
    }

    /**
     * Get autorsationAttachment
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getAutorsationAttachment()
    {
        return $this->autorsationAttachment;
    }

    /**
     * Set vitalCardAttachment
     *
     * @param \FileBundle\Entity\attachments $vitalCardAttachment
     *
     * @return EmployeeContract
     */
    public function setVitalCardAttachment(\FileBundle\Entity\attachments $vitalCardAttachment = null)
    {
        $this->vitalCardAttachment = $vitalCardAttachment;

        return $this;
    }

    /**
     * Get vitalCardAttachment
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getVitalCardAttachment()
    {
        return $this->vitalCardAttachment;
    }

    /**
     * Set singleDeclarationAttachment
     *
     * @param \FileBundle\Entity\attachments $singleDeclarationAttachment
     *
     * @return EmployeeContract
     */
    public function setSingleDeclarationAttachment(\FileBundle\Entity\attachments $singleDeclarationAttachment = null)
    {
        $this->singleDeclarationAttachment = $singleDeclarationAttachment;

        return $this;
    }

    /**
     * Get singleDeclarationAttachment
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getSingleDeclarationAttachment()
    {
        return $this->singleDeclarationAttachment;
    }

    /**
     * Set affiliationPensionAttachment
     *
     * @param \FileBundle\Entity\attachments $affiliationPensionAttachment
     *
     * @return EmployeeContract
     */
    public function setAffiliationPensionAttachment(\FileBundle\Entity\attachments $affiliationPensionAttachment = null)
    {
        $this->affiliationPensionAttachment = $affiliationPensionAttachment;

        return $this;
    }

    /**
     * Get affiliationPensionAttachment
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getAffiliationPensionAttachment()
    {
        return $this->affiliationPensionAttachment;
    }

    /**
     * Set affiliationHealthAttachment
     *
     * @param \FileBundle\Entity\attachments $affiliationHealthAttachment
     *
     * @return EmployeeContract
     */
    public function setAffiliationHealthAttachment(\FileBundle\Entity\attachments $affiliationHealthAttachment = null)
    {
        $this->affiliationHealthAttachment = $affiliationHealthAttachment;

        return $this;
    }

    /**
     * Get affiliationHealthAttachment
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getAffiliationHealthAttachment()
    {
        return $this->affiliationHealthAttachment;
    }

    /**
     * Set affiliationComplementaryPensionAttachment
     *
     * @param \FileBundle\Entity\attachments $affiliationComplementaryPensionAttachment
     *
     * @return EmployeeContract
     */
    public function setAffiliationComplementaryPensionAttachment(\FileBundle\Entity\attachments $affiliationComplementaryPensionAttachment = null)
    {
        $this->affiliationComplementaryPensionAttachment = $affiliationComplementaryPensionAttachment;

        return $this;
    }

    /**
     * Get affiliationComplementaryPensionAttachment
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getAffiliationComplementaryPensionAttachment()
    {
        return $this->affiliationComplementaryPensionAttachment;
    }

    /**
     * Set otherAttachment
     *
     * @param \FileBundle\Entity\attachments $otherAttachment
     *
     * @return EmployeeContract
     */
    public function setOtherAttachment(\FileBundle\Entity\attachments $otherAttachment = null)
    {
        $this->otherAttachment = $otherAttachment;

        return $this;
    }

    /**
     * Get otherAttachment
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getOtherAttachment()
    {
        return $this->otherAttachment;
    }
}
