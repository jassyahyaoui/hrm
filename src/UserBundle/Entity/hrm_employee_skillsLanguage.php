<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_employee_skillsLanguage
 *
 * @ORM\Table(name="hrm_employee_skills_language")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\hrm_employee_skillsLanguageRepository")
 */
class hrm_employee_skillsLanguage
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\hrm_employee_skills", inversedBy="hrm_employee_skillsLanguage")
     * @ORM\JoinColumn(name="employeeSkills", referencedColumnName="id")
     */
    private $employeeSkills;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="uid")
     */
    private $language;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_language_level")
     * @ORM\JoinColumn(name="language_level_id", referencedColumnName="uid")
     */
    private $language_level;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set employeeSkills
     *
     * @param \UserBundle\Entity\hrm_employee_skills $employeeSkills
     *
     * @return hrm_employee_skillsLanguage
     */
    public function setEmployeeSkills(\UserBundle\Entity\hrm_employee_skills $employeeSkills = null)
    {
        $this->employeeSkills = $employeeSkills;

        return $this;
    }

    /**
     * Get employeeSkills
     *
     * @return \UserBundle\Entity\hrm_employee_skills
     */
    public function getEmployeeSkills()
    {
        return $this->employeeSkills;
    }

    /**
     * Set language
     *
     * @param \AdminBundle\Entity\hrm_settings_language $language
     *
     * @return hrm_employee_skillsLanguage
     */
    public function setLanguage(\AdminBundle\Entity\hrm_settings_language $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return \AdminBundle\Entity\hrm_settings_language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set languageLevel
     *
     * @param \AdminBundle\Entity\hrm_settings_language_level $languageLevel
     *
     * @return hrm_employee_skillsLanguage
     */
    public function setLanguageLevel(\AdminBundle\Entity\hrm_settings_language_level $languageLevel = null)
    {
        $this->language_level = $languageLevel;

        return $this;
    }

    /**
     * Get languageLevel
     *
     * @return \AdminBundle\Entity\hrm_settings_language_level
     */
    public function getLanguageLevel()
    {
        return $this->language_level;
    }
}
