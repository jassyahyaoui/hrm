<?php

// src/AppBundle/Entity/User.php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="hrm_employee")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\EmployeeRepository")
 */
class HrmEmployee
{

    /**
     * @ORM\Id
     * @ORM\Column(name="uid", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="hrm_user" , inversedBy="employee")
     * @ORM\JoinColumn(name="user_uid", referencedColumnName="uid")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="EmployeeContract")
     */
    protected $employee_contract;

    /**
     * @var string
     *
     * @ORM\Column(name="employee_number", type="string", length=45,nullable=true)
     */
    protected $employee_number;

    /**
     *
     * @ORM\OneToOne(targetEntity="SettingsBundle\Entity\hrm_employee_settings", inversedBy="employee")
     *
     * */
    protected $preference;

    /**
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\hrm_employee_skills", inversedBy="employee")
     * @ORM\JoinColumn(name="skills_id", referencedColumnName="id")
     */
    protected $skills;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\Gender")
     * @ORM\JoinColumn(referencedColumnName="uid")
     */
    protected $gender;

    /**
     * @var date
     *
     * @ORM\Column(name="birthdate", type="date",nullable=true)
     */
    protected $birth_date;

    /**
     * @var string
     *
     * @ORM\Column(name="country_birth", type="string", length=45,nullable=true)
     */
    protected $country_birth;

    /**
     * @var string
     *
     * @ORM\Column(name="department_birth", type="string", length=45,nullable=true)
     */
    protected $department_birth;

    /**
     * @var string
     *
     * @ORM\Column(name="place_birth", type="string", length=45,nullable=true)
     */
    protected $place_birth;

    /**
     * @var string
     *
     * @ORM\Column(name="nation_code", type="string", length=45,nullable=true)
     */
    protected $nation_code;

    /**
     * @var string
     *
     * @ORM\Column(name="country_nationality", type="string", length=45,nullable=true)
     */
    protected $country_nationality;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=200,nullable=true)
     */
    protected $address;

    /**
     * @var string
     *
     * @ORM\Column(name="city_code", type="string", length=45,nullable=true)
     */
    protected $city_code;

    /**
     * @var integer
     *
     * @ORM\Column(name="zipcode", type="integer", length=8,nullable=true)
     */
    protected $zip_code;

    /**
     * @var string
     *
     * @ORM\Column(name="homePhone", type="string", length=200,nullable=true)
     */
    protected $homePhone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobilePhone", type="string", length=200,nullable=true)
     */
    protected $mobilePhone;

    /**
     * @var string
     *
     * @ORM\Column(name="skype", type="string", length=200,nullable=true)
     */
    protected $skype;

    /**
     * @var string
     *
     * @ORM\Column(name="linkedin", type="string", length=200,nullable=true)
     */
    protected $linkedin;

    /**
     * @var string
     *
     * @ORM\Column(name="viadeo", type="string", length=200,nullable=true)
     */
    protected $viadeo;

    /**
     * @var string
     *
     * @ORM\Column(name="twitter", type="string", length=200,nullable=true)
     */
    protected $twitter;

    /**
     * @var string
     *
     * @ORM\Column(name="province_code", type="string", length=45,nullable=true)
     */
    protected $province_code;

    /**
     * @var string
     *
     * @ORM\Column(name="country_code", type="string", length=45,nullable=true)
     */
    protected $country_code;

    /**
     * @var integer
     *
     * @ORM\Column(name="sin_num", type="integer", length=4,nullable=true)
     */
    protected $sin_num;

    /**
     * @var string
     *
     * @ORM\Column(name="ssn_num", type="string", length=15,nullable=true)
     */
    protected $ssn_num;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_handicape", type="boolean",nullable=true)
     */
    protected $is_handicape;

    /**
     * @var integer
     *
     * @ORM\Column(name="num_children", type="integer",nullable=true,options={"default" = "0"})
     */
    protected $num_children;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsPayment")
     * @ORM\JoinColumn(referencedColumnName="uid")
     */
    protected $payment_method;

    /**
     * @var integer
     *
     * @ORM\Column(name="IBAN", type="string", length=45,nullable=true)
     */
    protected $iban;

    /**
     * @var integer
     *
     * @ORM\Column(name="BIC", type="string", length=45,nullable=true)
     */
    protected $bic;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsStatus")
     * @ORM\JoinColumn(referencedColumnName="uid")
     */
    protected $status;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsRoles")
     * @ORM\JoinColumn(referencedColumnName="uid")
     */
    protected $role;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsProvider" , inversedBy="employee")
     * @ORM\JoinColumn(name="provider_id", referencedColumnName="uid")
     */
    private $provider;

    /**
     * @ORM\ManyToOne(targetEntity="HrmCompany")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    protected $company;

    /**
     * @var string
     *
     * @ORM\Column(name="avatars", type="string", length=255,nullable=true)
     */
    protected $avatars;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsStudyLevel")
     * @ORM\JoinColumn(referencedColumnName="uid")
     */
    protected $study_level;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsStudyType")
     * @ORM\JoinColumn(referencedColumnName="uid")
     */
    protected $study_type;

    /**
     * @var text
     *
     * @ORM\Column(name="comments", type="text",nullable=true)
     */
    protected $comments;

    /**
     * @var string
     *
     * @ORM\Column(name="commune", type="string", length=45,nullable=true)
     */
    protected $commune;

    /**
     * @var string
     *
     * @ORM\Column(name="departement", type="string", length=45,nullable=true)
     */
    protected $departement;

    /**
     * @var string
     *
     * @ORM\Column(name="domiciliation", type="string", length=45,nullable=true)
     */
    protected $domiciliation;

    /**
     * @ORM\OneToOne(targetEntity="\FileBundle\Entity\attachments")
     * @ORM\JoinColumn(name="rib_attachments", referencedColumnName="uid")
     */
    private $ribAttachments;

    /**
     *
     * @ORM\OneToOne(targetEntity="IsPublicProfils", mappedBy="employee")
     *
     * */
    protected $public_profil;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_family_status")
     * @ORM\JoinColumn(name="family_status_id", referencedColumnName="uid")
     */
    protected $family_status;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set employeeNumber
     *
     * @param string $employeeNumber
     *
     * @return HrmEmployee
     */
    public function setEmployeeNumber($employeeNumber)
    {
        $this->employee_number = $employeeNumber;

        return $this;
    }

    /**
     * Get employeeNumber
     *
     * @return string
     */
    public function getEmployeeNumber()
    {
        return $this->employee_number;
    }

    /**
     * Set birthDate
     *
     * @param \DateTime $birthDate
     *
     * @return HrmEmployee
     */
    public function setBirthDate($birthDate)
    {
        $this->birth_date = $birthDate;

        return $this;
    }

    /**
     * Get birthDate
     *
     * @return \DateTime
     */
    public function getBirthDate()
    {
        return $this->birth_date;
    }

    /**
     * Set countryBirth
     *
     * @param string $countryBirth
     *
     * @return HrmEmployee
     */
    public function setCountryBirth($countryBirth)
    {
        $this->country_birth = $countryBirth;

        return $this;
    }

    /**
     * Get countryBirth
     *
     * @return string
     */
    public function getCountryBirth()
    {
        return $this->country_birth;
    }

    /**
     * Set departmentBirth
     *
     * @param string $departmentBirth
     *
     * @return HrmEmployee
     */
    public function setDepartmentBirth($departmentBirth)
    {
        $this->department_birth = $departmentBirth;

        return $this;
    }

    /**
     * Get departmentBirth
     *
     * @return string
     */
    public function getDepartmentBirth()
    {
        return $this->department_birth;
    }

    /**
     * Set placeBirth
     *
     * @param string $placeBirth
     *
     * @return HrmEmployee
     */
    public function setPlaceBirth($placeBirth)
    {
        $this->place_birth = $placeBirth;

        return $this;
    }

    /**
     * Get placeBirth
     *
     * @return string
     */
    public function getPlaceBirth()
    {
        return $this->place_birth;
    }

    /**
     * Set nationCode
     *
     * @param string $nationCode
     *
     * @return HrmEmployee
     */
    public function setNationCode($nationCode)
    {
        $this->nation_code = $nationCode;

        return $this;
    }

    /**
     * Get nationCode
     *
     * @return string
     */
    public function getNationCode()
    {
        return $this->nation_code;
    }

    /**
     * Set countryNationality
     *
     * @param string $countryNationality
     *
     * @return HrmEmployee
     */
    public function setCountryNationality($countryNationality)
    {
        $this->country_nationality = $countryNationality;

        return $this;
    }

    /**
     * Get countryNationality
     *
     * @return string
     */
    public function getCountryNationality()
    {
        return $this->country_nationality;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return HrmEmployee
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set cityCode
     *
     * @param string $cityCode
     *
     * @return HrmEmployee
     */
    public function setCityCode($cityCode)
    {
        $this->city_code = $cityCode;

        return $this;
    }

    /**
     * Get cityCode
     *
     * @return string
     */
    public function getCityCode()
    {
        return $this->city_code;
    }

    /**
     * Set zipCode
     *
     * @param integer $zipCode
     *
     * @return HrmEmployee
     */
    public function setZipCode($zipCode)
    {
        $this->zip_code = $zipCode;

        return $this;
    }

    /**
     * Get zipCode
     *
     * @return integer
     */
    public function getZipCode()
    {
        return $this->zip_code;
    }

    /**
     * Set homePhone
     *
     * @param string $homePhone
     *
     * @return HrmEmployee
     */
    public function setHomePhone($homePhone)
    {
        $this->homePhone = $homePhone;

        return $this;
    }

    /**
     * Get homePhone
     *
     * @return string
     */
    public function getHomePhone()
    {
        return $this->homePhone;
    }

    /**
     * Set mobilePhone
     *
     * @param string $mobilePhone
     *
     * @return HrmEmployee
     */
    public function setMobilePhone($mobilePhone)
    {
        $this->mobilePhone = $mobilePhone;

        return $this;
    }

    /**
     * Get mobilePhone
     *
     * @return string
     */
    public function getMobilePhone()
    {
        return $this->mobilePhone;
    }

    /**
     * Set skype
     *
     * @param string $skype
     *
     * @return HrmEmployee
     */
    public function setSkype($skype)
    {
        $this->skype = $skype;

        return $this;
    }

    /**
     * Get skype
     *
     * @return string
     */
    public function getSkype()
    {
        return $this->skype;
    }

    /**
     * Set linkedin
     *
     * @param string $linkedin
     *
     * @return HrmEmployee
     */
    public function setLinkedin($linkedin)
    {
        $this->linkedin = $linkedin;

        return $this;
    }

    /**
     * Get linkedin
     *
     * @return string
     */
    public function getLinkedin()
    {
        return $this->linkedin;
    }

    /**
     * Set viadeo
     *
     * @param string $viadeo
     *
     * @return HrmEmployee
     */
    public function setViadeo($viadeo)
    {
        $this->viadeo = $viadeo;

        return $this;
    }

    /**
     * Get viadeo
     *
     * @return string
     */
    public function getViadeo()
    {
        return $this->viadeo;
    }

    /**
     * Set twitter
     *
     * @param string $twitter
     *
     * @return HrmEmployee
     */
    public function setTwitter($twitter)
    {
        $this->twitter = $twitter;

        return $this;
    }

    /**
     * Get twitter
     *
     * @return string
     */
    public function getTwitter()
    {
        return $this->twitter;
    }

    /**
     * Set provinceCode
     *
     * @param string $provinceCode
     *
     * @return HrmEmployee
     */
    public function setProvinceCode($provinceCode)
    {
        $this->province_code = $provinceCode;

        return $this;
    }

    /**
     * Get provinceCode
     *
     * @return string
     */
    public function getProvinceCode()
    {
        return $this->province_code;
    }

    /**
     * Set countryCode
     *
     * @param string $countryCode
     *
     * @return HrmEmployee
     */
    public function setCountryCode($countryCode)
    {
        $this->country_code = $countryCode;

        return $this;
    }

    /**
     * Get countryCode
     *
     * @return string
     */
    public function getCountryCode()
    {
        return $this->country_code;
    }

    /**
     * Set sinNum
     *
     * @param integer $sinNum
     *
     * @return HrmEmployee
     */
    public function setSinNum($sinNum)
    {
        $this->sin_num = $sinNum;

        return $this;
    }

    /**
     * Get sinNum
     *
     * @return integer
     */
    public function getSinNum()
    {
        return $this->sin_num;
    }

    /**
     * Set ssnNum
     *
     * @param string $ssnNum
     *
     * @return HrmEmployee
     */
    public function setSsnNum($ssnNum)
    {
        $this->ssn_num = $ssnNum;

        return $this;
    }

    /**
     * Get ssnNum
     *
     * @return string
     */
    public function getSsnNum()
    {
        return $this->ssn_num;
    }

    /**
     * Set isHandicape
     *
     * @param boolean $isHandicape
     *
     * @return HrmEmployee
     */
    public function setIsHandicape($isHandicape)
    {
        $this->is_handicape = $isHandicape;

        return $this;
    }

    /**
     * Get isHandicape
     *
     * @return boolean
     */
    public function getIsHandicape()
    {
        return $this->is_handicape;
    }

    /**
     * Set numChildren
     *
     * @param integer $numChildren
     *
     * @return HrmEmployee
     */
    public function setNumChildren($numChildren)
    {
        $this->num_children = $numChildren;

        return $this;
    }

    /**
     * Get numChildren
     *
     * @return integer
     */
    public function getNumChildren()
    {
        return $this->num_children;
    }

    /**
     * Set iban
     *
     * @param string $iban
     *
     * @return HrmEmployee
     */
    public function setIban($iban)
    {
        $this->iban = $iban;

        return $this;
    }

    /**
     * Get iban
     *
     * @return string
     */
    public function getIban()
    {
        return $this->iban;
    }

    /**
     * Set bic
     *
     * @param string $bic
     *
     * @return HrmEmployee
     */
    public function setBic($bic)
    {
        $this->bic = $bic;

        return $this;
    }

    /**
     * Get bic
     *
     * @return string
     */
    public function getBic()
    {
        return $this->bic;
    }

    /**
     * Set avatars
     *
     * @param string $avatars
     *
     * @return HrmEmployee
     */
    public function setAvatars($avatars)
    {
        $this->avatars = $avatars;

        return $this;
    }

    /**
     * Get avatars
     *
     * @return string
     */
    public function getAvatars()
    {
        return $this->avatars;
    }

    /**
     * Set comments
     *
     * @param string $comments
     *
     * @return HrmEmployee
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set commune
     *
     * @param string $commune
     *
     * @return HrmEmployee
     */
    public function setCommune($commune)
    {
        $this->commune = $commune;

        return $this;
    }

    /**
     * Get commune
     *
     * @return string
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * Set departement
     *
     * @param string $departement
     *
     * @return HrmEmployee
     */
    public function setDepartement($departement)
    {
        $this->departement = $departement;

        return $this;
    }

    /**
     * Get departement
     *
     * @return string
     */
    public function getDepartement()
    {
        return $this->departement;
    }

    /**
     * Set domiciliation
     *
     * @param string $domiciliation
     *
     * @return HrmEmployee
     */
    public function setDomiciliation($domiciliation)
    {
        $this->domiciliation = $domiciliation;

        return $this;
    }

    /**
     * Get domiciliation
     *
     * @return string
     */
    public function getDomiciliation()
    {
        return $this->domiciliation;
    }

    /**
     * Set user
     *
     * @param \UserBundle\Entity\hrm_user $user
     *
     * @return HrmEmployee
     */
    public function setUser(\UserBundle\Entity\hrm_user $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \UserBundle\Entity\hrm_user
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set employeeContract
     *
     * @param \UserBundle\Entity\EmployeeContract $employeeContract
     *
     * @return HrmEmployee
     */
    public function setEmployeeContract(\UserBundle\Entity\EmployeeContract $employeeContract = null)
    {
        $this->employee_contract = $employeeContract;

        return $this;
    }

    /**
     * Get employeeContract
     *
     * @return \UserBundle\Entity\EmployeeContract
     */
    public function getEmployeeContract()
    {
        return $this->employee_contract;
    }

    /**
     * Set preference
     *
     * @param \SettingsBundle\Entity\hrm_employee_settings $preference
     *
     * @return HrmEmployee
     */
    public function setPreference(\SettingsBundle\Entity\hrm_employee_settings $preference = null)
    {
        $this->preference = $preference;

        return $this;
    }

    /**
     * Get preference
     *
     * @return \SettingsBundle\Entity\hrm_employee_settings
     */
    public function getPreference()
    {
        return $this->preference;
    }

    /**
     * Set gender
     *
     * @param \AdminBundle\Entity\Gender $gender
     *
     * @return HrmEmployee
     */
    public function setGender(\AdminBundle\Entity\Gender $gender = null)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return \AdminBundle\Entity\Gender
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set paymentMethod
     *
     * @param \AdminBundle\Entity\SettingsPayment $paymentMethod
     *
     * @return HrmEmployee
     */
    public function setPaymentMethod(\AdminBundle\Entity\SettingsPayment $paymentMethod = null)
    {
        $this->payment_method = $paymentMethod;

        return $this;
    }

    /**
     * Get paymentMethod
     *
     * @return \AdminBundle\Entity\SettingsPayment
     */
    public function getPaymentMethod()
    {
        return $this->payment_method;
    }

    /**
     * Set status
     *
     * @param \AdminBundle\Entity\SettingsStatus $status
     *
     * @return HrmEmployee
     */
    public function setStatus(\AdminBundle\Entity\SettingsStatus $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return \AdminBundle\Entity\SettingsStatus
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set role
     *
     * @param \AdminBundle\Entity\SettingsRoles $role
     *
     * @return HrmEmployee
     */
    public function setRole(\AdminBundle\Entity\SettingsRoles $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return \AdminBundle\Entity\SettingsRoles
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set provider
     *
     * @param \AdminBundle\Entity\SettingsProvider $provider
     *
     * @return HrmEmployee
     */
    public function setProvider(\AdminBundle\Entity\SettingsProvider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \AdminBundle\Entity\SettingsProvider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set company
     *
     * @param \UserBundle\Entity\HrmCompany $company
     *
     * @return HrmEmployee
     */
    public function setCompany(\UserBundle\Entity\HrmCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \UserBundle\Entity\HrmCompany
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set studyLevel
     *
     * @param \AdminBundle\Entity\SettingsStudyLevel $studyLevel
     *
     * @return HrmEmployee
     */
    public function setStudyLevel(\AdminBundle\Entity\SettingsStudyLevel $studyLevel = null)
    {
        $this->study_level = $studyLevel;

        return $this;
    }

    /**
     * Get studyLevel
     *
     * @return \AdminBundle\Entity\SettingsStudyLevel
     */
    public function getStudyLevel()
    {
        return $this->study_level;
    }

    /**
     * Set studyType
     *
     * @param \AdminBundle\Entity\SettingsStudyType $studyType
     *
     * @return HrmEmployee
     */
    public function setStudyType(\AdminBundle\Entity\SettingsStudyType $studyType = null)
    {
        $this->study_type = $studyType;

        return $this;
    }

    /**
     * Get studyType
     *
     * @return \AdminBundle\Entity\SettingsStudyType
     */
    public function getStudyType()
    {
        return $this->study_type;
    }

    /**
     * Set ribAttachments
     *
     * @param \FileBundle\Entity\attachments $ribAttachments
     *
     * @return HrmEmployee
     */
    public function setRibAttachments(\FileBundle\Entity\attachments $ribAttachments = null)
    {
        $this->ribAttachments = $ribAttachments;

        return $this;
    }

    /**
     * Get ribAttachments
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getRibAttachments()
    {
        return $this->ribAttachments;
    }

    /**
     * Set publicProfil
     *
     * @param \UserBundle\Entity\IsPublicProfils $publicProfil
     *
     * @return HrmEmployee
     */
    public function setPublicProfil(\UserBundle\Entity\IsPublicProfils $publicProfil = null)
    {
        $this->public_profil = $publicProfil;

        return $this;
    }

    /**
     * Get publicProfil
     *
     * @return \UserBundle\Entity\IsPublicProfils
     */
    public function getPublicProfil()
    {
        return $this->public_profil;
    }

    /**
     * Set familyStatus
     *
     * @param \AdminBundle\Entity\hrm_settings_family_status $familyStatus
     *
     * @return HrmEmployee
     */
    public function setFamilyStatus(\AdminBundle\Entity\hrm_settings_family_status $familyStatus = null)
    {
        $this->family_status = $familyStatus;

        return $this;
    }

    /**
     * Get familyStatus
     *
     * @return \AdminBundle\Entity\hrm_settings_family_status
     */
    public function getFamilyStatus()
    {
        return $this->family_status;
    }

    public function getName()
    {
        return $this->getUser()->getFirstName() . " " . $this->getUser()->getLastName();
    }

    public function __toString()
    {
        return $this->getUser()->getFirstName() . " " . $this->getUser()->getLastName();
    }


    /**
     * Set skills
     *
     * @param \UserBundle\Entity\hrm_employee_skills $skills
     *
     * @return HrmEmployee
     */
    public function setSkills(\UserBundle\Entity\hrm_employee_skills $skills = null)
    {
        $this->skills = $skills;

        return $this;
    }

    /**
     * Get skills
     *
     * @return \UserBundle\Entity\hrm_employee_skills
     */
    public function getSkills()
    {
        return $this->skills;
    }
}
