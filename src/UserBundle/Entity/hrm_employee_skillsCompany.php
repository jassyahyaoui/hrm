<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_employee_skillsCompany
 *
 * @ORM\Table(name="hrm_employee_skills_company")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\hrm_employee_skillsCompanyRepository")
 */
class hrm_employee_skillsCompany
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\hrm_employee_skills", inversedBy="hrm_employee_skillsCompany")
     * @ORM\JoinColumn(name="employeeSkills", referencedColumnName="id")
     */
    private $employeeSkills;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=255)
     */
    private $displayName;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_job")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="uid")
     */
    private $job;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_industrysector")
     * @ORM\JoinColumn(name="industrysector_id", referencedColumnName="uid")
     */
    private $industrysector;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return hrm_employee_skillsCompany
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set job
     *
     * @param \AdminBundle\Entity\hrm_settings_job $job
     *
     * @return hrm_employee_skillsCompany
     */
    public function setJob(\AdminBundle\Entity\hrm_settings_job $job = null)
    {
        $this->job = $job;

        return $this;
    }

    /**
     * Get job
     *
     * @return \AdminBundle\Entity\hrm_settings_job
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * Set industrysector
     *
     * @param \AdminBundle\Entity\hrm_settings_industrysector $industrysector
     *
     * @return hrm_employee_skillsCompany
     */
    public function setIndustrysector(\AdminBundle\Entity\hrm_settings_industrysector $industrysector = null)
    {
        $this->industrysector = $industrysector;

        return $this;
    }

    /**
     * Get industrysector
     *
     * @return \AdminBundle\Entity\hrm_settings_industrysector
     */
    public function getIndustrysector()
    {
        return $this->industrysector;
    }

    /**
     * Set employeeSkills
     *
     * @param \UserBundle\Entity\hrm_employee_skills $employeeSkills
     *
     * @return hrm_employee_skillsCompany
     */
    public function setEmployeeSkills(\UserBundle\Entity\hrm_employee_skills $employeeSkills = null)
    {
        $this->employeeSkills = $employeeSkills;

        return $this;
    }

    /**
     * Get employeeSkills
     *
     * @return \UserBundle\Entity\hrm_employee_skills
     */
    public function getEmployeeSkills()
    {
        return $this->employeeSkills;
    }
}
