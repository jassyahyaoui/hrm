<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HrmCompany
 *
 * @ORM\Table(name="hrm_company")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\HrmCompanyRepository")
 */
class HrmCompany
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_industrysector")
     * @ORM\JoinColumn(name="industry_sector_id", referencedColumnName="uid")
     */
    private $industrySector;

    /**
     * @var int
     *
     * @ORM\Column(name="employee_number", type="integer",nullable=true)
     */
    private $employeeNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="adress", type="text", nullable=true)
     */
    private $adress;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="postalCode", type="string", length=255, nullable=true)
     */
    private $postalCode;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="phoneNumber", type="string", length=255, nullable=true)
     */
    private $phoneNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="fax", type="string", length=255, nullable=true)
     */
    private $fax;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="webSite", type="string", length=255, nullable=true)
     */
    private $webSite;

    /**
     * @var string
     *
     * @ORM\Column(name="siret", type="string", length=255, nullable=true)
     */
    private $siret;

    /**
     * @var string
     *
     * @ORM\Column(name="siren", type="string", length=255, nullable=true)
     */
    private $siren;

    /**
     * @var string
     *
     * @ORM\Column(name="capitalSocial", type="string", length=255, nullable=true)
     */
    private $capitalSocial;

    /**
     * @var string
     *
     * @ORM\Column(name="tva", type="string", length=255, nullable=true)
     */
    private $tva;

    /**
     * @var string
     *
     * @ORM\Column(name="naf", type="string", length=255, nullable=true)
     */
    private $naf;

    /**
     * @var string
     *
     * @ORM\Column(name="rcs", type="string", length=255, nullable=true)
     */
    private $rcs;

    /**
     * @var string
     *
     * @ORM\Column(name="rm", type="string", length=255, nullable=true)
     */
    private $rm;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_customertype")
     * @ORM\JoinColumn(name="customer_type", referencedColumnName="uid")
     */
    private $customerType;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsProvider")
     * @ORM\JoinColumn(name="provider_id", referencedColumnName="uid")
     */
    private $provider;

    /**
     * @var string
     *
     * @ORM\Column(name="externalRef", type="string", length=255, nullable=true)
     */
    private $external_ref;


    /**
     * @var Date
     *
     * @ORM\Column(name="create_date", type="date", nullable=true)
     */
    private $create_date;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\hrm_user")
     * @ORM\JoinColumn(name="create_uid", referencedColumnName="uid")
     */
    private $createUid;

    /**
     * @var Date
     *
     * @ORM\Column(name="last_update", type="date", nullable=true)
     */
    private $last_update;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\hrm_user")
     * @ORM\JoinColumn(name="last_update_uid", referencedColumnName="uid")
     */
    private $last_update_uid;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\hrm_company_currency", mappedBy="company")
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="base_currency_iso_code", type="string", length=255)
     */
    private $base_currency_iso_code;

    /**
     * @var string
     *
     * @ORM\Column(name="locale_code", type="string", length=255)
     */
    private $locale_code;

    /**
     * @return string
     */
    public function getLocaleCode()
    {
        return $this->locale_code;
    }

    /**
     * @param string $locale_code
     */
    public function setLocaleCode($locale_code)
    {
        $this->locale_code = $locale_code;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return HrmCompany
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set adress
     *
     * @param string $adress
     *
     * @return HrmCompany
     */
    public function setAdress($adress)
    {
        $this->adress = $adress;

        return $this;
    }

    /**
     * Get adress
     *
     * @return string
     */
    public function getAdress()
    {
        return $this->adress;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return HrmCompany
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return HrmCompany
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set postalCode
     *
     * @param string $postalCode
     *
     * @return HrmCompany
     */
    public function setPostalCode($postalCode)
    {
        $this->postalCode = $postalCode;

        return $this;
    }

    /**
     * Get postalCode
     *
     * @return string
     */
    public function getPostalCode()
    {
        return $this->postalCode;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return HrmCompany
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set phoneNumber
     *
     * @param string $phoneNumber
     *
     * @return HrmCompany
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;

        return $this;
    }

    /**
     * Get phoneNumber
     *
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * Set fax
     *
     * @param string $fax
     *
     * @return HrmCompany
     */
    public function setFax($fax)
    {
        $this->fax = $fax;

        return $this;
    }

    /**
     * Get fax
     *
     * @return string
     */
    public function getFax()
    {
        return $this->fax;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return HrmCompany
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set webSite
     *
     * @param string $webSite
     *
     * @return HrmCompany
     */
    public function setWebSite($webSite)
    {
        $this->webSite = $webSite;

        return $this;
    }

    /**
     * Get webSite
     *
     * @return string
     */
    public function getWebSite()
    {
        return $this->webSite;
    }

    /**
     * Set siret
     *
     * @param string $siret
     *
     * @return HrmCompany
     */
    public function setSiret($siret)
    {
        $this->siret = $siret;

        return $this;
    }

    /**
     * Get siret
     *
     * @return string
     */
    public function getSiret()
    {
        return $this->siret;
    }

    /**
     * Set siren
     *
     * @param string $siren
     *
     * @return HrmCompany
     */
    public function setSiren($siren)
    {
        $this->siren = $siren;

        return $this;
    }

    /**
     * Get siren
     *
     * @return string
     */
    public function getSiren()
    {
        return $this->siren;
    }

    /**
     * Set capitalSocial
     *
     * @param string $capitalSocial
     *
     * @return HrmCompany
     */
    public function setCapitalSocial($capitalSocial)
    {
        $this->capitalSocial = $capitalSocial;

        return $this;
    }

    /**
     * Get capitalSocial
     *
     * @return string
     */
    public function getCapitalSocial()
    {
        return $this->capitalSocial;
    }

    /**
     * Set tva
     *
     * @param string $tva
     *
     * @return HrmCompany
     */
    public function setTva($tva)
    {
        $this->tva = $tva;

        return $this;
    }

    /**
     * Get tva
     *
     * @return string
     */
    public function getTva()
    {
        return $this->tva;
    }

    /**
     * Set naf
     *
     * @param string $naf
     *
     * @return HrmCompany
     */
    public function setNaf($naf)
    {
        $this->naf = $naf;

        return $this;
    }

    /**
     * Get naf
     *
     * @return string
     */
    public function getNaf()
    {
        return $this->naf;
    }

    /**
     * Set rcs
     *
     * @param string $rcs
     *
     * @return HrmCompany
     */
    public function setRcs($rcs)
    {
        $this->rcs = $rcs;

        return $this;
    }

    /**
     * Get rcs
     *
     * @return string
     */
    public function getRcs()
    {
        return $this->rcs;
    }

    /**
     * Set rm
     *
     * @param string $rm
     *
     * @return HrmCompany
     */
    public function setRm($rm)
    {
        $this->rm = $rm;

        return $this;
    }

    /**
     * Get rm
     *
     * @return string
     */
    public function getRm()
    {
        return $this->rm;
    }
    public function __toString() {
        if($this->getName())
            return $this->getName();
        else return "";

    }

    /**
     * Set provider
     *
     * @param \AdminBundle\Entity\SettingsProvider $provider
     *
     * @return HrmCompany
     */
    public function setProvider(\AdminBundle\Entity\SettingsProvider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \AdminBundle\Entity\SettingsProvider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Set externalRef
     *
     * @param string $externalRef
     *
     * @return HrmCompany
     */
    public function setExternalRef($externalRef)
    {
        $this->external_ref = $externalRef;

        return $this;
    }

    /**
     * Get externalRef
     *
     * @return string
     */
    public function getExternalRef()
    {
        return $this->external_ref;
    }

    /**
     * Set employeeNumber
     *
     * @param integer $employeeNumber
     *
     * @return HrmCompany
     */
    public function setEmployeeNumber($employeeNumber)
    {
        $this->employeeNumber = $employeeNumber;

        return $this;
    }

    /**
     * Get employeeNumber
     *
     * @return integer
     */
    public function getEmployeeNumber()
    {
        return $this->employeeNumber;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return HrmCompany
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set industrySector
     *
     * @param \AdminBundle\Entity\hrm_settings_industrysector $industrySector
     *
     * @return HrmCompany
     */
    public function setIndustrySector(\AdminBundle\Entity\hrm_settings_industrysector $industrySector = null)
    {
        $this->industrySector = $industrySector;

        return $this;
    }

    /**
     * Get industrySector
     *
     * @return \AdminBundle\Entity\hrm_settings_industrysector
     */
    public function getIndustrySector()
    {
        return $this->industrySector;
    }

    /**
     * Set customerType
     *
     * @param \AdminBundle\Entity\hrm_settings_customertype $customerType
     *
     * @return HrmCompany
     */
    public function setCustomerType(\AdminBundle\Entity\hrm_settings_customertype $customerType = null)
    {
        $this->customerType = $customerType;

        return $this;
    }

    /**
     * Get customerType
     *
     * @return \AdminBundle\Entity\hrm_settings_customertype
     */
    public function getCustomerType()
    {
        return $this->customerType;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return HrmCompany
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return HrmCompany
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set createUid
     *
     * @param \UserBundle\Entity\hrm_user $createUid
     *
     * @return HrmCompany
     */
    public function setCreateUid(\UserBundle\Entity\hrm_user $createUid = null)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return \UserBundle\Entity\hrm_user
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdateUid
     *
     * @param \UserBundle\Entity\hrm_user $lastUpdateUid
     *
     * @return HrmCompany
     */
    public function setLastUpdateUid(\UserBundle\Entity\hrm_user $lastUpdateUid = null)
    {
        $this->last_update_uid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return \UserBundle\Entity\hrm_user
     */
    public function getLastUpdateUid()
    {
        return $this->last_update_uid;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->base_currency_iso_code = "EUR - Euro";
        $this->locale_code = "fr-fr";
        $this->currency = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add currency
     *
     * @param \UserBundle\Entity\hrm_company_currency $currency
     *
     * @return HrmCompany
     */
    public function addCurrency(\UserBundle\Entity\hrm_company_currency $currency)
    {
        $this->currency[] = $currency;

        return $this;
    }

    /**
     * Remove currency
     *
     * @param \UserBundle\Entity\hrm_company_currency $currency
     */
    public function removeCurrency(\UserBundle\Entity\hrm_company_currency $currency)
    {
        $this->currency->removeElement($currency);
    }

    /**
     * Get currency
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set baseCurrencyIsoCode
     *
     * @param string $baseCurrencyIsoCode
     *
     * @return HrmCompany
     */
    public function setBaseCurrencyIsoCode($baseCurrencyIsoCode)
    {
        $this->base_currency_iso_code = $baseCurrencyIsoCode;

        return $this;
    }

    /**
     * Get baseCurrencyIsoCode
     *
     * @return string
     */
    public function getBaseCurrencyIsoCode()
    {
        return $this->base_currency_iso_code;
    }
}
