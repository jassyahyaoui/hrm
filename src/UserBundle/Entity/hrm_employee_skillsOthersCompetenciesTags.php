<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_employee_skillsOthersCompetenciesTags
 *
 * @ORM\Table(name="hrm_employee_skills_others_competencies_tags")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\hrm_employee_skillsOthersCompetenciesTagsRepository")
 */
class hrm_employee_skillsOthersCompetenciesTags
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\hrm_employee_skills", inversedBy="hrm_employee_skillsOthersCompetenciesTags")
     * @ORM\JoinColumn(name="employeeSkills", referencedColumnName="id")
     */
    private $employeeSkills;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=100)
     */
    private $tag;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tag
     *
     * @param string $tag
     *
     * @return hrm_employee_skillsOthersCompetenciesTags
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set employeeSkills
     *
     * @param \UserBundle\Entity\hrm_employee_skills $employeeSkills
     *
     * @return hrm_employee_skillsOthersCompetenciesTags
     */
    public function setEmployeeSkills(\UserBundle\Entity\hrm_employee_skills $employeeSkills = null)
    {
        $this->employeeSkills = $employeeSkills;

        return $this;
    }

    /**
     * Get employeeSkills
     *
     * @return \UserBundle\Entity\hrm_employee_skills
     */
    public function getEmployeeSkills()
    {
        return $this->employeeSkills;
    }
}
