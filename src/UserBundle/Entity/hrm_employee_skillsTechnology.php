<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_employee_skillsTechnology
 *
 * @ORM\Table(name="hrm_employee_skills_technology")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\hrm_employee_skillsTechnologyRepository")
 */
class hrm_employee_skillsTechnology
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\hrm_employee_skills", inversedBy="hrm_employee_skillsTechnology")
     * @ORM\JoinColumn(name="employeeSkills", referencedColumnName="id")
     */
    private $employeeSkills;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_technology")
     * @ORM\JoinColumn(name="technology_id", referencedColumnName="uid")
     */
    private $technology;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\hrm_settings_technology_level")
     * @ORM\JoinColumn(name="technology_level_id", referencedColumnName="uid")
     */
    private $technology_level;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set technology
     *
     * @param \AdminBundle\Entity\hrm_settings_technology $technology
     *
     * @return hrm_employee_skillsTechnology
     */
    public function setTechnology(\AdminBundle\Entity\hrm_settings_technology $technology = null)
    {
        $this->technology = $technology;

        return $this;
    }

    /**
     * Get technology
     *
     * @return \AdminBundle\Entity\hrm_settings_technology
     */
    public function getTechnology()
    {
        return $this->technology;
    }

    /**
     * Set technologyLevel
     *
     * @param \AdminBundle\Entity\hrm_settings_technology_level $technologyLevel
     *
     * @return hrm_employee_skillsTechnology
     */
    public function setTechnologyLevel(\AdminBundle\Entity\hrm_settings_technology_level $technologyLevel = null)
    {
        $this->technology_level = $technologyLevel;

        return $this;
    }

    /**
     * Get technologyLevel
     *
     * @return \AdminBundle\Entity\hrm_settings_technology_level
     */
    public function getTechnologyLevel()
    {
        return $this->technology_level;
    }

    /**
     * Set employeeSkills
     *
     * @param \UserBundle\Entity\hrm_employee_skills $employeeSkills
     *
     * @return hrm_employee_skillsTechnology
     */
    public function setEmployeeSkills(\UserBundle\Entity\hrm_employee_skills $employeeSkills = null)
    {
        $this->employeeSkills = $employeeSkills;

        return $this;
    }

    /**
     * Get employeeSkills
     *
     * @return \UserBundle\Entity\hrm_employee_skills
     */
    public function getEmployeeSkills()
    {
        return $this->employeeSkills;
    }
}
