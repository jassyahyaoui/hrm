<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_employee_skillsStudy
 *
 * @ORM\Table(name="hrm_employee_skills_study")
 * @ORM\Entity(repositoryClass="UserBundle\Repository\hrm_employee_skillsStudyRepository")
 */
class hrm_employee_skillsStudy
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\hrm_employee_skills", inversedBy="hrm_employee_skillsStudy")
     * @ORM\JoinColumn(name="employeeSkills", referencedColumnName="id")
     */
    private $employeeSkills;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=255)
     */
    private $displayName;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsStudyLevel")
     * @ORM\JoinColumn(name="study_level_id", referencedColumnName="uid")
     */
    private $study_level;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsStudyType")
     * @ORM\JoinColumn(name="study_type_id", referencedColumnName="uid")
     */
    private $study_type;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return hrm_employee_skillsStudy
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set studyLevel
     *
     * @param \AdminBundle\Entity\SettingsStudyLevel $studyLevel
     *
     * @return hrm_employee_skillsStudy
     */
    public function setStudyLevel(\AdminBundle\Entity\SettingsStudyLevel $studyLevel = null)
    {
        $this->study_level = $studyLevel;

        return $this;
    }

    /**
     * Get studyLevel
     *
     * @return \AdminBundle\Entity\SettingsStudyLevel
     */
    public function getStudyLevel()
    {
        return $this->study_level;
    }

    /**
     * Set studyType
     *
     * @param \AdminBundle\Entity\SettingsStudyType $studyType
     *
     * @return hrm_employee_skillsStudy
     */
    public function setStudyType(\AdminBundle\Entity\SettingsStudyType $studyType = null)
    {
        $this->study_type = $studyType;

        return $this;
    }

    /**
     * Get studyType
     *
     * @return \AdminBundle\Entity\SettingsStudyType
     */
    public function getStudyType()
    {
        return $this->study_type;
    }

    /**
     * Set employeeSkills
     *
     * @param \UserBundle\Entity\hrm_employee_skills $employeeSkills
     *
     * @return hrm_employee_skillsStudy
     */
    public function setEmployeeSkills(\UserBundle\Entity\hrm_employee_skills $employeeSkills = null)
    {
        $this->employeeSkills = $employeeSkills;

        return $this;
    }

    /**
     * Get employeeSkills
     *
     * @return \UserBundle\Entity\hrm_employee_skills
     */
    public function getEmployeeSkills()
    {
        return $this->employeeSkills;
    }
}
