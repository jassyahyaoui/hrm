<?php

namespace UserBundle\Tests;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Request;

class UserTest extends WebTestCase {

    private $client = null;
    private $container = null;

    public function setUp() {
     //  echo "\n -------- Run Unit test of User -------- \n";
      /*  $this->client = static::createClient();
        $this->container = $this->client->getContainer();  
        $this->client->request('GET', '/logout');
        $userProvider = $this->container->get('fos_user.user_provider.username');
        $user = $userProvider->loadUserByUsername('responsable');
        
        $this->assertInstanceOf(\UserBundle\Entity\hrm_user::class, $user, 'Object isn\'t instance of User');
        $token = new UsernamePasswordToken($user, $user->getPassword(), "main", $user->getRoles());
        $this->container->get("security.context")->setToken($token); //now the user is logged in
        //now dispatch the login event
        $request = new Request;
        $event = new InteractiveLoginEvent($request, $token);
        $this->container->get("event_dispatcher")->dispatch("security.interactive_login", $event);*/
        $this->client = static::createClient();
        $crawler = $this->client->request('GET', '/login');

        $form = $crawler->selectButton('_submit')->form(array(
            '_username' => "responsable",
            '_password' => "responsable",
        ));

        $this->client->submit($form);        
        $this->container = $this->client->getContainer();  
        //dump(gettype($this->container->get("security.context")->getToken()));return;
        $token = $this->container->get("security.context")->getToken();
        $this->assertNotEquals($token, NULL,  'Login failed for user');
        $this->container->get('session')->set('company',  $this->container->get("security.context")->getToken()->getUser()->getEmployee()->first()->getCompany()->getId());
    }
    
   
    public function testGetEmployeeBelongCompanySession() {

        $ret = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $this->assertInstanceOf(\UserBundle\Entity\HrmEmployee::class, $ret, 'Object isn\'t instance of HrmEmployee');        
    }
    
   public function testGetDataDonutBelongCompanySession() {

        $this->client->request('GET', '/expenses/api/ik/static/donut/charts/'. date('Y-m'));
        $this->assertArrayHasKey('result', json_decode($this->client->getResponse()->getContent(), true), 'Error call api ik for DONUT chart data');       
    }    

    public function testGetDataGridBelongCompanySession() {

        $this->client->request('GET', '/expenses/api/ik/static/grid/charts/'.date('Y-m'));
        $this->assertArrayHasKey('result', json_decode($this->client->getResponse()->getContent(), true), 'Error call api ik for GRID chart data');       
    }    
  
    public function testGetDataBarBelongCompanySession() {

        // expenses
        $this->client->request('GET', '/expenses/api/ik/static/bar/charts/'.date('Y'));
        $this->assertArrayHasKey('result', json_decode($this->client->getResponse()->getContent(), true), 'Error call api ik for BAR chart data');      
        
        // absences
        $this->client->request('GET', '/api/requestabsence/static/bar/charts/'.date('Y'));
        $this->assertArrayHasKey('result', json_decode($this->client->getResponse()->getContent(), true), 'Error call api requestabsence for BAR chart data');          
    } 
    
    public function testExportAbsenceByPayPeriod() {
        
       $this->client->request('GET', '/payperiod/payperiod/export/'.date('Y-m'));
        $this->assertEquals('application/zip', $this->client->getResponse()->headers->get('Content-Type'), 'Error export csv file absences');
    } 
    
    public function testExportVPIByPayPeriod() {
        
       $this->client->request('GET', '/export/vpi/'.date('Y-m'));
       $this->assertEquals('text/csv; charset=utf-8', $this->client->getResponse()->headers->get('Content-Type'), 'Error export VPI file');
    }   
    
    public function testExportCollaborators() {
        
       $this->client->request('GET', '/export/exportemployees');
       $this->assertEquals('text/csv; charset=utf-8', $this->client->getResponse()->headers->get('Content-Type'), 'Error export COLLABORATORS file');
    }
    
    public function testChangeEmployeeCompanySession() {
        
        $oldcompany = $this->container->get('session')->get('company');
        $this->assertGreaterThan(1, count($this->container->get("security.context")->getToken()->getUser()->getEmployee()));

        $this->container->get('session')->set('company', $this->container->get("security.context")->getToken()->getUser()->getEmployee()->next()->getCompany()->getId());  
        $this->assertNotEquals($oldcompany, $this->container->get('session')->get('company'));

    }
    
    public function testAbsencesList() {
        
       $this->client->request('GET', '/api/payperiod/payperiod/absence/list/'.date('Y-m'));
       $this->assertArrayHasKey('users',  json_decode($this->client->getResponse()->getContent(), true), $this->client->getResponse()->getContent());
    } 
    
    public function testExpensesBonusList() {
        
       $this->client->request('GET', '/api/payperiod/payperiod/expensesbonus/list/'.date('Y-m'));
       $this->assertArrayHasKey('users',  json_decode($this->client->getResponse()->getContent(), true), $this->client->getResponse()->getContent());
    }    
    
}
