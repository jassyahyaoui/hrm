<?php

namespace UserBundle\Tests;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
USE UserBundle\Entity\HrmEmployee;

class EmployeeRepositoryTest extends KernelTestCase {

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;    

    public function setUp() {
        self::bootKernel();

        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager();       
    }
    
    public function testFindResponsableByCompanyId()
    {
        $ret = $this->em
            ->getRepository(HrmEmployee::class)
            ->FindResponsableByCompanyId(1)
        ;

        $this->assertGreaterThanOrEqual(1, count($ret));
    }
  
    public function testFindCollaborateurById()
    {
        $ret = $this->em
            ->getRepository(HrmEmployee::class)
            ->FindCollaborateurById(1)
        ;
        
        $this->assertNotEmpty($ret);        
    }
    
    public function testExportEmployees()
    {
        $ret = $this->em
            ->getRepository(HrmEmployee::class)
            ->ExportEmployees(1)
        ;
         $this->assertNotEmpty($ret); 
        // $this->assertInstanceOf(HrmEmployee::class, $ret);        
    }
    
    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();

        $this->em->close();
        $this->em = null; // avoid memory leaks
    }    
}
