<?php

namespace UserBundle\Service;

use UserBundle\Entity\HrmEmployee;
use UserBundle\Entity\EmployeeContract;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\DependencyInjection\Container;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

class UserService {

    protected $em;
    private $container;

    public function __construct(EntityManager $entityManager, Container $container) {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function getCurrentUser() {
        $securityContext = $this->container->get('security.authorization_checker');
//    if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
        $user = $this->container->get('security.context')->getToken()->getUser();
            return $user;
//    }
//    else 
//        return null;
    }
    
    public function findCurrentEmployee($username) {
        $qb = $this->em->createQueryBuilder();
        $qb
                ->select('u')
                ->from('UserBundle:hrm_user', 'u')
                ->where('u.username = :username')
                ->setParameter('username', $username)
                ->getQuery()
                ->getOneOrNullResult();
        return $qb->getQuery()->getOneOrNullResult();
    }
    
    

    public function findEmployee($email) {
        $qb = $this->em->createQueryBuilder();
        $qb
                ->select('u,e,t,g,p,st,sl,c,s,at,f')
                ->from('UserBundle:HrmEmployee', 'u')
                ->leftJoin('u.employee_contract', 'e')
                ->leftJoin('u.ribAttachments', 'at')
                ->leftJoin('e.contractType', 't')
                ->leftJoin('e.empStatus', 's')
                ->leftJoin('u.gender', 'g')
                ->leftJoin('u.payment_method', 'p')
                ->leftJoin('u.study_type', 'st')
                ->leftJoin('u.study_level', 'sl')
                ->leftJoin('u.company', 'c')
                ->leftJoin('u.family_status', 'f')
                ->where('u.email = :email')
                ->setParameter('email', $email)
                ->getQuery()
                ->getOneOrNullResult();
        return $qb->getQuery()->getOneOrNullResult();
    }
    
    public function findContrat($id) {
        $qb = $this->em->createQueryBuilder();
        $qb
                ->select('c')
                ->from('UserBundle:EmployeeContract', 'c')
                ->leftJoin('c.employee', 'e')
                ->where('e.id = :id')
                ->setParameter('id', $id)
                ->getQuery()
                ->getOneOrNullResult();
        
        return $qb->getQuery()->getOneOrNullResult();
    }

    public function deleteEmployee($email) {
        $qb = $this->em->createQuery('DELETE UserBundle:HrmEmployee u WHERE u.email = :email')
                ->setParameter("email", $email);
        $result = $qb->execute();
        return $result;
    }

    public function deleteEmployeesettings($id) {
        $qb = $this->em->createQuery('DELETE SettingsBundle:hrm_employee_settings u WHERE u.id = :id')
                ->setParameter("id", $id);
        $result = $qb->execute();
        return $result;
    }
    
    public function findValidators($company) {
//        $company = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
//         $company  = $this->container->get('session')->get('company');
      $qb = $this->em->createQueryBuilder();
             $qb->select('u')
                ->from('UserBundle:HrmEmployee', 'u')
                ->leftJoin('u.company', 'c')
                ->leftJoin('c.provider', 'p')
                ->leftJoin('u.user', 'ue')
                ->where("ue.roles LIKE '%ROLE_RESPONSABLE%'")
                ->AndWhere('c.id = :companyId')
                ->setParameter('companyId', $company)
                ->setMaxResults(1)
                ->getQuery()
                ->getArrayResult();
        return $qb->getQuery()->getOneOrNullResult();
    }

    public function findEmployeeByUsername($username) {
        $qb = $this->em->createQueryBuilder();
        $qb
                ->select('u,e,t,g,p,st,sl,c,s,at,f')
                ->from('UserBundle:HrmEmployee', 'u')
                ->leftJoin('u.employee_contract', 'e')
                ->leftJoin('u.ribAttachments', 'at')
                ->leftJoin('e.contractType', 't')
                ->leftJoin('e.empStatus', 's')
                ->leftJoin('u.user', 'uu')
                ->leftJoin('u.gender', 'g')
                ->leftJoin('u.payment_method', 'p')
                ->leftJoin('u.study_type', 'st')
                ->leftJoin('u.study_level', 'sl')
                ->leftJoin('u.company', 'c')
                ->leftJoin('u.family_status', 'f')
                ->where('uu.username = :username')
                ->setParameter('username', $username)
                ->getQuery()
                ->getOneOrNullResult();
        return $qb->getQuery()->getOneOrNullResult();
    }
    
    public function logIn($username, $providerId){
        $user = $this->em->getRepository("UserBundle:hrm_user")->findOneBy(array("username" => $username, 'provider'=>$providerId));
        $token = new UsernamePasswordToken($user, $user->getPassword(), "main", $user->getRoles());
         $this->container->get("security.context")->setToken($token); //now the user is logged in
         //now dispatch the login event
         $request = $this->container->get("request");
         $event = new InteractiveLoginEvent($request, $token);
         $this->container->get("event_dispatcher")->dispatch("security.interactive_login", $event);
    }
    
    
      public function findUserByUsernameAndCompany($username,$company) {
           $qb = $this->em->createQueryBuilder();
          return $qb  ->select('e,u,c')
                ->from('UserBundle:HrmEmployee', 'e')
                ->leftJoin('e.user', 'u')
                ->leftJoin('e.company', 'c')
                ->where('u.username = :username')
                ->setParameter('username', $username)   
                ->AndWhere('c.id = :company')
                ->setParameter('company', $company)
                ->getQuery()->getOneOrNullResult();
        
    }
    
}
