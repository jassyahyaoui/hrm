<?php
namespace FOSOAuth2ServerBundle\Entity;

use FOS\OAuthServerBundle\Entity\Client as BaseClient;

class Client extends BaseClient
{
    protected $id;
    protected $name;
    protected $scope;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }
    
    public function setScope($name)
    {
        $this->scope = $name;
    }

    public function getScope()
    {
        return $this->scope;
    }    
}
