<?php

namespace FOSOAuth2ServerBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class FOSOAuth2ServerController extends Controller
{
    public function meAction(Request $request)
    {
         $authenticationErrorResponse = $this->checkAuthAndGetErrorResponse($request);
        if ($authenticationErrorResponse) {
            return $authenticationErrorResponse;
        }
        $token = $this->container->get('security.context')->getToken()->getUser();

        $accessToken = $this->container->get('fos_oauth_server.auth_code_manager.default')
                                  ->findAuthCodeBy(array('user' => $token));
        return $accessToken->getClient();
    }
    
    private function checkAuthAndGetErrorResponse(Request $request)
    {
        $token = $this->container->get('security.context')->getToken()->getToken();

        $accessToken = $this->container->get('fos_oauth_server.access_token_manager.default')
                                  ->findTokenBy(array('token' => $token));

        if (!$accessToken) {
            return new JsonResponse(['status' => 400, 'message' => 'Bearer token not valid'], 400);
        }

        if ($accessToken->hasExpired()) {
            return new JsonResponse(['status' => 400, 'message' => 'Access token has expired'], 400);
        }

        // may want to validate something else about the client, but that is beyond OAuth2 scope
        //$client = $accessToken->getClient();

        return null;
    }    
}
