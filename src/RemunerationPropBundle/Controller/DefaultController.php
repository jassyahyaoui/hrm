<?php

namespace RemunerationPropBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('RemunerationPropBundle:Default:index.html.twig');
    }
}
