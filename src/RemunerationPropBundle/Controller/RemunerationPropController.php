<?php

namespace RemunerationPropBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RemunerationPropController extends Controller
{
    public function layoutAction() {
        return $this->render('remuneration_prop/layout.html.twig');
    }    
}
