<?php

namespace BaseBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    //index function
    public function indexAction()
    {
        return $this->render('BaseBundle:Default:index.html.twig');


    }
}
