<?php

namespace DashboardBundle\Controller;

use DashboardBundle\Entity\dashboard;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Dashboard controller.
 *
 */
class dashboardController extends Controller {
  public function collaborateurindexAction()
    {
        return $this->render('collaborator/index.html.twig');
    }
    
    public function collaborateurAction() {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->get('security.context')->getToken()->getUser();
        $infos_user = $em->getRepository('UserBundle:hrm_user')->find($current_user_id);
        
          $company = $this->container->get('settingsbundle.preference.service')->getCompany(); 
          if($company) 
          $company_id = $company->getId();
          else
          $company_id = $infos_user->getEmployee()->first()->getCompany()->getId();    
          
        if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
            $collaborateur = $em->getRepository('UserBundle:HrmEmployee')->FindEmployeesById($current_user_id);
        } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
//            $collaborateur = $em->getRepository('UserBundle:HrmEmployee')->FindCollaborateurByCompanyId($company_id);
            $collaborateur = $em->getRepository('UserBundle:HrmEmployee')->FindEmployeeByCompanyId($company_id);
        } else {
            $collaborateur = null;
        }
        
        $users = $em->getRepository('UserBundle:HrmEmployee')->FindCollaborateurByCompanyId($company_id);
        //Get roles Collaborateur By Company Id
//        $collaborateur = $em->getRepository('UserBundle:HrmEmployee')->FindCollaborateurByCompanyId($company_id);
        return array('collaborateur' => $collaborateur,'users'=>$users);
    }

    /**
     * Lists all dashboard entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        return $this->render('dashboard/index.html.twig');
    }

    
   
    
    /**
     * Creates a new dashboard entity.
     *
     */
    public function newAction(Request $request) {
        $dashboard = new Dashboard();
        $form = $this->createForm('DashboardBundle\Form\dashboardType', $dashboard);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dashboard);
            $em->flush($dashboard);

            return $this->redirectToRoute('dashboard_show', array('id' => $dashboard->getId()));
        }

        return $this->render('dashboard/new.html.twig', array(
                    'dashboard' => $dashboard,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a dashboard entity.
     *
     */
    public function showAction(dashboard $dashboard) {
        $deleteForm = $this->createDeleteForm($dashboard);

        return $this->render('dashboard/show.html.twig', array(
                    'dashboard' => $dashboard,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing dashboard entity.
     *
     */
    public function editAction(Request $request, dashboard $dashboard) {
        $deleteForm = $this->createDeleteForm($dashboard);
        $editForm = $this->createForm('DashboardBundle\Form\dashboardType', $dashboard);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('dashboard_edit', array('id' => $dashboard->getId()));
        }

        return $this->render('dashboard/edit.html.twig', array(
                    'dashboard' => $dashboard,
                    'edit_form' => $editForm->createView(),
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a dashboard entity.
     *
     */
    public function deleteAction(Request $request, dashboard $dashboard) {
        $form = $this->createDeleteForm($dashboard);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dashboard);
            $em->flush();
        }

        return $this->redirectToRoute('dashboard_index');
    }

    /**
     * Creates a form to delete a dashboard entity.
     *
     * @param dashboard $dashboard The dashboard entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(dashboard $dashboard) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('dashboard_delete', array('id' => $dashboard->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

    public function singlePageLayoutAction() {
        return $this->render('dashboard/single_page_layout.html.twig');
    }
    public function collaboratorsinglePageLayoutAction() {
        return $this->render('collaborator/single_page_layout.html.twig');
    }


}
