<?php

namespace DashboardBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;

/**
 * dashboardRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class dashboardRepository extends \Doctrine\ORM\EntityRepository
{

    public function FindCollaborateurByCompanyId($companyId) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('u')
                        ->from('UserBundle:HrmEmployee', 'u')
//                        ->where('u.company_id = :value')
                              ->leftJoin('c.company', 'co')
                        ->where('co.id = :value')
                        ->setParameter('value', $companyId)
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY); 
    }
}
