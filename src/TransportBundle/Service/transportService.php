<?php

namespace TransportBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class transportService {

    protected $em;
    private $container;

    public function __construct(EntityManager $entityManager, Container $container) {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function deleteAllTransport() {
        $this->em->createQuery('DELETE TransportBundle:hrm_transport h')->execute();
    }

    public function findTransport($value) {
        $transport = $this->container->get('doctrine')->getEntityManager()->getRepository('TransportBundle:hrm_transport')
                ->findOneBy(array('amount' => $value));
        if ($transport)
            return $transport;
        else
            return null;
    }

    public function findListTransport($value) {
        $transport = $this->container->get('doctrine')->getEntityManager()->getRepository('TransportBundle:hrm_transport')
                ->findBy(array('amount' => $value));
        return $transport;
    }

}
