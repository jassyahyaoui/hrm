<?php

namespace TransportBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
/**
 * hrm_transportRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class hrm_transportRepository extends \Doctrine\ORM\EntityRepository
{

    public function FindForCollab($CurrentUserId) {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('i,t,s,a,v,vu,vc,vcp,c,cu,cc,ccp,r,ru,rc,rcp')
            ->from('TransportBundle:hrm_transport', 'i')
            ->leftJoin('i.type', 't')
            ->leftJoin('i.settings_status', 's')
            // manager Bloc
            ->leftJoin('i.validator', 'v')
            ->leftJoin('v.user', 'vu')
            ->leftJoin('v.company', 'vc')
            ->leftJoin('vc.provider', 'vcp')
            // employee Bloc
            ->leftJoin('i.contributor', 'c')
            ->leftJoin('c.user', 'cu')
            ->leftJoin('c.company', 'cc')
            ->leftJoin('cc.provider', 'ccp')
            // Requester Bloc
            ->leftJoin('i.requester', 'r')
            ->leftJoin('r.user', 'ru')
            ->leftJoin('r.company', 'rc')
            ->leftJoin('rc.provider', 'rcp')
            // Attachment Bloc
            ->leftJoin('i.attachments', 'a')
            // Condition Bloc
            ->andWhere('s.id != :s OR r.id = :CurrentUserId ')
            ->andWhere('r.id = :CurrentUserId OR c.id = :CurrentUserId')
            ->setParameter('s', 4)
            ->setParameter('CurrentUserId', $CurrentUserId)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }


    public function FindForResp($companyId, $CurrentUserId) {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('i,t,s,a,v,vu,vc,vcp,c,cu,cc,ccp,r,ru,rc,rcp')
            ->from('TransportBundle:hrm_transport', 'i')
            ->leftJoin('i.type', 't')
            ->leftJoin('i.settings_status', 's')
            // manager Bloc
            ->leftJoin('i.validator', 'v')
            ->leftJoin('v.user', 'vu')
            ->leftJoin('v.company', 'vc')
            ->leftJoin('vc.provider', 'vcp')
            // employee Bloc
            ->leftJoin('i.contributor', 'c')
            ->leftJoin('c.user', 'cu')
            ->leftJoin('c.company', 'cc')
            ->leftJoin('cc.provider', 'ccp')
            // Requester Bloc
            ->leftJoin('i.requester', 'r')
            ->leftJoin('r.user', 'ru')
            ->leftJoin('r.company', 'rc')
            ->leftJoin('rc.provider', 'rcp')
            // Attachment Bloc
            ->leftJoin('i.attachments', 'a')
            // Condition Bloc
            ->where('vc.id = :value')
            ->andWhere('s.id != :s OR r.id = :CurrentUserId')
            ->andWhere('r.id = :CurrentUserId OR v.id = :CurrentUserId OR c.id = :CurrentUserId')
            ->setParameter('value', (int) $companyId)
            ->setParameter('CurrentUserId', $CurrentUserId)
            ->setParameter('s', 4)
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }

    /**
     * get list Transport belong payperiod for export data
     * 
     */
    public function findByPayPeriodBelongPublicTransport($obj_payperiod, $user)
    {
        return $this->getEntityManager()
                ->createQueryBuilder()
                ->select('i')
                ->from('TransportBundle:hrm_transport', 'i')
                ->leftJoin('i.settings_status', 's')
                ->leftJoin('i.contributor', 'e')
                ->leftJoin('e.company', 'c')
                ->andwhere('i.settings_pay_period = :pay_period_id')
                ->andWhere('s.id = :value')
                ->setParameter('value', 2)
                ->andwhere('c.id = :company_id')
                ->setParameter('pay_period_id', $obj_payperiod->getId())
                ->setParameter('company_id', $user->getCompany()->getId())
                ->getQuery()
                ->getResult();
    }  
    
    public function transportForPayPeriodList($array_data) {

            return $this->getEntityManager()
                            ->createQueryBuilder()
                            ->from('TransportBundle:hrm_transport', 'i')
                            ->addselect("SUM(i.amount) AS publictransport_amount")
                            ->addselect('u.first_name')
                            ->addselect('u.id emp')
                            ->leftJoin('i.contributor', 'e')
                            ->leftJoin('e.user', 'u')                        
                            ->leftJoin('i.settings_status', 's')
                            ->leftJoin('i.settings_pay_period', 'pp')
                            ->andWhere('s.id = :value')
                            ->setParameter('value', 2) // if status is valid
                            ->andWhere('e.company = :company_id')
                            ->setParameter('company_id', $array_data['company_id'])
                            ->andWhere('pp.id = :pay_period_id')
                            ->setParameter('pay_period_id', $array_data['pay_period_id'])
                            ->groupBy('e.id')
                            ->getQuery()
                            ->getResult(Query::HYDRATE_ARRAY);   
    }   
    
    /*************** get total Hrm_Transport by status *******************/
     public function PublicTransportStaticDataFindStaticDByUserCompanyId($Id, $pay_period_id, $isbelongCompany = TRUE) {
        $ret  = $this->getEntityManager()
                    ->createQueryBuilder()
                    ->addselect("SUM(CASE WHEN s.id = 2 THEN 1 ELSE 0 END) AS valid")
                        ->addselect("SUM(CASE WHEN s.id = 1 THEN 1 ELSE 0 END) AS waiting")
                        ->addselect("SUM(CASE WHEN s.id = 4 THEN 1 ELSE 0 END) AS draft")
                        ->from('TransportBundle:hrm_transport', 'i')
                        ->leftJoin('i.settings_status', 's')
                        ->leftJoin('i.settings_pay_period', 'pp')
                        ->leftJoin('i.contributor', 'e');
                        if($isbelongCompany)
                        {
                        $ret->leftJoin('e.company', 'co')
                            ->andWhere('co.id = :value')
                            ->setParameter('value', (int)$Id);
                        }
                        else
                        {
                         $ret->andWhere('e.id = :CurrentUserId')
                             ->setParameter('CurrentUserId', $Id);            
                        }
                        $ret->andWhere('pp.id = :pay_period_id')
                            ->setParameter('pay_period_id', $pay_period_id);
            return $ret->getQuery()
                       ->getResult(Query::HYDRATE_ARRAY);
    }  
    
     /*************** get Grid data Hrm_Transport by status belong PayPeriod*******************/
    public function PublicTransportStaticDataOfGridChartsFindByUserCompanyId($Id, $CurrentUserId, $obj_Payperiod, $isbelongCompany = TRUE) {
       $ret = $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select("u.id as user_id, Count(i) nbr_request,SUM(i.amount) as total, u.first_name, u.last_name , 'TRANSPORT' as type, '€' as fixer")
                        ->from('TransportBundle:hrm_transport', 'i')
                        ->leftJoin('i.settings_status', 's')
                        ->leftJoin('i.settings_pay_period', 'pp')
                        ->leftJoin('i.contributor', 'con')
                        ->leftJoin('con.user', 'u');
                    
                        if($isbelongCompany)
                        {    
                           $ret->leftJoin('i.validator', 'v')
                            ->leftJoin('con.company', 'co')
                            ->andwhere('co.id = :value')            
                            ->andWhere('i.createUid = :CurrentUserId OR v.id = :CurrentUserId OR con.id = :CurrentUserId')
                            ->setParameter('value', (int)$Id)
                            ->setParameter('CurrentUserId', $CurrentUserId);
                        }else{
                            $ret->andWhere('con.id = :CurrentUserId')
                               ->setParameter('CurrentUserId', $Id);                 
                        }                 
                        $ret->andWhere('s.id = :s')
                        ->setParameter('s', 2)
                        ->andWhere('pp.id = :pay_period_id')
                        ->setParameter('pay_period_id', $obj_Payperiod->getId())
                        ->groupBy('u');
                    return $ret->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }
    /**
     * get list Transport belong payperiod for export data
     *
     */
    public function findByPayPeriodBelongTransport($obj_payperiod, $user)
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('i,t,e')
            ->from('TransportBundle:hrm_transport', 'i')
            ->leftJoin('i.type', 't')
            ->leftJoin('i.settings_status', 's')
            ->leftJoin('i.contributor', 'e')
            ->leftJoin('e.company', 'c')
            ->andwhere('i.settings_pay_period = :pay_period_id')
            ->andWhere('s.id = :value')
            ->setParameter('value', 2)
            ->andwhere('c.id = :company_id')
            ->setParameter('pay_period_id', $obj_payperiod->getId())
            ->setParameter('company_id', $user->getCompany()->getId())
            ->getQuery()
            ->getResult();
    }

}
