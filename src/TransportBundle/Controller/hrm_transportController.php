<?php

namespace TransportBundle\Controller;

use TransportBundle\Entity\hrm_transport;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FileBundle\Entity\attachments;
use TransportBundle\Form\hrm_transportType;

/**
 * Hrm_transport controller.
 *
 */
class hrm_transportController extends Controller {

    public function layoutAction() {
        return $this->render('transport/layout.html.twig');
    }

    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $hrm_transports = $em->getRepository('TransportBundle:hrm_transport')->findAll();

        return $this->render('transport/index.html.twig', array(
            'hrm_transports' => $hrm_transports,
        ));
    }

    public function newAction(Request $request) {
        $hrm_transport = new hrm_transport();
        $provider = $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $company = $this->container->get('settingsbundle.preference.service')->getCompanyId();
        $form = $this->createForm(new hrm_transportType(), null, array(
            'provider' => $provider, 'company' => $company,
        ));
        $form->handleRequest($request);
        return $this->render('transport/new.html.twig', array(
            'hrm_transport' => $hrm_transport,
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request) {
        $hrm_transport = new hrm_transport();
        $provider = $this->container->get('settingsbundle.preference.service')->getProviderId();
        $company = $this->container->get('settingsbundle.preference.service')->getCompanyId();
        $form = $this->createForm(new hrm_transportType(), $hrm_transport, array(
            'provider' => $provider, 'company' => $company,
        ));
        $form->handleRequest($request);
        return $this->render('transport/edit.html.twig', array(
            'hrm_transport' => $hrm_transport,
            'form' => $form->createView(),
        ));
    }

    public function addAction(Request $request) {
        $hrm_transport = new hrm_transport();
        $form = $this->createForm('TransportBundle\Form\hrm_transportType', $hrm_transport);
        $form->handleRequest($request);
        $body = $request->getContent();
        $data = json_decode($body, true);
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');
        //set default status if ROLE_COLLABORATEUR else set with the submited value
        if ($request->isMethod('POST')) {

            $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $current_user_id = $empData->getId();
            $form->submit($data);

            $hrm_transport->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $hrm_transport->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $hrm_transport->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));


//            if (isset($data['type'])) {
                $type = $em->getRepository('AdminBundle:SettingsTransports')->find($data['type']);
                if ($type)
                    $hrm_transport->setType($type);
//            }

            $hrm_transport->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));

            if ($request->request->has('attachments')) {
                if ($data['attachments'] != null) {
//                    if ($request->request->has('attachments')) {
                    if ($hrm_transport->getAttachments() != null) {
                        $attachment = $hrm_transport->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if ((isset($data['attachments']['mimetype'])) && ($data['attachments']['mimetype'] != null))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $hrm_transport->setAttachments($attachment);
//                    }
                } else {

                    $hrm_transport->setAttachments(null);
                }
            }
            //Add default value on create Action
            $hrm_transport->setCreateDate(new \DateTime('now'));
            $hrm_transport->setCreateUid($current_user_id);
            $hrm_transport->setLastUpdateUid($current_user_id);
            $hrm_transport->setLastUpdate(new \DateTime('now'));

            //  try to add first open one of pay period
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $hrm_transport);
            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }

            //send mail to responsable

            $TranslateMail = $this->get('mailingbundle.email.send.service');
            if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $this->get('mailingbundle.email.send.service')->Notify($hrm_transport, 'RESPONSABLE', $TranslateMail->TagsTranslateTransport('titleNew'), $TranslateMail->TagsTranslateTransport('etatNew'), $TranslateMail->TagsTranslateTransport('typeRequest'), $TranslateMail->TagsTranslateTransport('folder'), $TranslateMail->TagsTranslateTransport('viewName'));
            } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $this->get('mailingbundle.email.send.service')->Notify($hrm_transport, 'COLLABORATEUR', $TranslateMail->TagsTranslateTransport('titleNew'), $TranslateMail->TagsTranslateTransport('etatNew'), $TranslateMail->TagsTranslateTransport('typeRequest'), $TranslateMail->TagsTranslateTransport('folder'), $TranslateMail->TagsTranslateTransport('viewName'));
            }


            $em->persist($hrm_transport);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function updateAction(Request $request, hrm_transport $hrm_transport) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, true);
        $hrm_transport = $em->getRepository('TransportBundle:hrm_transport')->find($data['id']);
        $update_form = $this->createForm('TransportBundle\Form\hrm_transportType', $hrm_transport);
        if ($request->isMethod('PUT')) {
            $update_form->submit($data);

            $collab_id = $hrm_transport->getCreateUid();

            $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $current_user_id = $empData->getId();
            $hrm_transport->setLastUpdate(new \DateTime('now'));
            $hrm_transport->setLastUpdateUid($current_user_id);

            if ($data['attachments'] != null) {
                if ($request->request->has('attachments')) {
                    if ($hrm_transport->getAttachments() != null) {
                        $attachment = $hrm_transport->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if (isset($data['attachments']['mimetype']))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $hrm_transport->setAttachments($attachment);
                }
            } else {
                $hrm_transport->setAttachments(null);
            }
            if ($data['publicMandatory'] == false) {
                $hrm_transport->setPublicMandatory(0);
            } else {
                $hrm_transport->setPublicMandatory(1);
            }
            $hrm_transport->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $hrm_transport->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $hrm_transport->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));

            $hrm_transport->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));

            // try to add first open one of pay period
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $hrm_transport);
            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }

            $em->persist($hrm_transport);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function allAction() {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');
//        $currentEmployeeId = $this->container->get('settingsbundle.preference.service')->getEmployeeId();
//        $companyId = $this->container->get('settingsbundle.preference.service')->getCompanyId();
//        $provider = $this->container->get('settingsbundle.preference.service')->getProviderId();

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();

        $current_user_id = $empData->getId();
        $companyData = $empData->getCompany();
        $company_id = $companyData->getId();
        $providerId = $companyData->getProvider();
        if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
            $result = $em->getRepository('TransportBundle:hrm_transport')->FindForResp($company_id, $current_user_id);
        } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
            $result = $em->getRepository('TransportBundle:hrm_transport')->FindForCollab($current_user_id);
        }
        $users = $em->getRepository('UserBundle:HrmEmployee')->FindCollaborateurByCompanyId($company_id);
        return array('result' => $result, 'users' => $users);
    }

    public function removeAction(Request $request, hrm_transport $hrm_transport) {
        if ($request->isMethod('DELETE')) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_transport);
            $em->flush($hrm_transport);
            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }

    public function ConfirmAction(Request $request, hrm_transport $hrm_transport) {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $hrm_transport->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['confirm']));

        $entity = $em->getRepository('TransportBundle:hrm_transport')->find($hrm_transport);

        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $entity);

        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }

        //send mail to responsable
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($hrm_transport, 'RESPONSABLE', $TranslateMail->TagsTranslateTransport('confirmMsg'), $TranslateMail->TagsTranslateTransport('confirmEtat'), $TranslateMail->TagsTranslateTransport('typeRequest'), $TranslateMail->TagsTranslateTransport('folder'), $TranslateMail->TagsTranslateTransport('viewName'));



        $em->persist($hrm_transport);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function CancelAction(Request $request, hrm_transport $hrm_transport) {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $hrm_transport->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['cancel']));

        $entity = $em->getRepository('TransportBundle:hrm_transport')->find($hrm_transport);

        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $entity);
        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }

        //send mail to responsable
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($hrm_transport, 'RESPONSABLE', $TranslateMail->TagsTranslateTransport('cancelMsg'), $TranslateMail->TagsTranslateTransport('cancelEtat'), $TranslateMail->TagsTranslateTransport('typeRequest'), $TranslateMail->TagsTranslateTransport('folder'), $TranslateMail->TagsTranslateTransport('viewName'));

        $em->persist($hrm_transport);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    /**
     *
     * @param type $em
     * @param type $entity
     * @param type $payPeriodDate celui envoyÃ© depuis from
     * @return boolean/object
     */
    private function setOpenedSettingsPayPeriod($em, $entity, $payPeriodDate = null) {
        $ret = '';
        // affect first pay period

        if ($payPeriodDate == null) {
            $searchBy = array("companyId" => $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId(),
                "status" => "open");
        } else {
            $searchBy = array("companyId" => $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId());
        }

        $payperiodlist = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')
            ->findBy($searchBy, array('startDate' => 'ASC'));
        if ($payperiodlist) {

            if ($payPeriodDate == null) {
                $entity->setSettingsPayPeriod($payperiodlist[0]);
                $ret = true;
            } else {
                foreach ($payperiodlist as $key => $value) {
                    if ($value->getStartDate()->format('m/Y') == $payPeriodDate && ($value->getStatus() === 'open')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = true;
                        break;
                    }
                    if (($value->getStartDate()->format('m/Y') === $payPeriodDate ) && ($value->getStatus() === 'close')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = false;
                        break;
                    }
                }
            }

            if ($ret === true)
                return true;
            if ($ret === false)
                return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_CLOSED_PAY_PERIOD"));

            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_NOT_PAY_PERIOD"));
        }
        else {
            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_EMPTY_PAY_PERIOD"));
        }
    }

}
