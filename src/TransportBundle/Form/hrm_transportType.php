<?php

namespace TransportBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class hrm_transportType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
//        $type = array("Transport public" => "Transport public", "Transport personnel" => "Transport personnel");

        $builder
            ->add('date', 'date', array(
                'label' => 'Date du frais',
                'required' => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'class' => 'form-control datepicker',
                    'date-directive' => ''
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('startDate', 'date', array(
                'label' => 'Date du frais',
                'required' => false,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'class' => 'form-control datepicker',
                    'date-directive' => ''
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right'
                )
            ))
            ->add('endDate', 'date', array(
                'label' => 'Date du frais',
                'required' => false,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'class' => 'form-control datepicker',
                    'date-directive' => ''
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right'
                )
            ))
            ->add('contributor', 'entity', array(
                'label' => 'Collaborateur',
//                'empty_value' => 'Sélectionner un collaborateur',
                'required' => true,
                'class' => 'UserBundle\Entity\HrmEmployee',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->where("e.roles LIKE '%COLLABORATEUR%' OR e.roles LIKE '%RESPONSABLE%'")
                        ->andwhere('u.company = :value')
                        ->setParameter('value', (int)$options['company']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))

            ->add('validator', 'entity', array(
                'label' => 'type',
                'required' => false,
                'class' => 'UserBundle\Entity\HrmEmployee',
//                    'property' => 'display_name',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->where("e.roles LIKE '%RESPONSABLE%'")
                        ->andwhere('u.company = :value')
                        ->setParameter('value', (int)$options['company']);
                },
            ))
            ->add('requester', 'entity', array(
                'label' => 'Collaborateur',
//                'empty_value' => 'Sélectionner un requester',
                'required' => true,
                'disabled' => true,
                'read_only' => true,
                'class' => 'UserBundle\Entity\HrmEmployee',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->Where('u.company = :value')
//                        ->andwhere('e.id = :valueUser')
                        ->setParameter('value', (int)$options['company']);
//                        ->setParameter('valueUser', (int)$options['currentUser']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
//            ->add('type', 'choice', array(
//                    'label' => 'Type de transport',
//                    'choices' => $type,
//                    'required' => true,
//                    'attr' => array('class' => 'form-control',
//                        'class' => 'form-control',
//                    ),
//                    'label_attr' => array(
//                        'class' => 'col-sm-3 control-label no-padding-right asterix'
//                    )
//                )
//            )

            ->add('type', 'entity', array(
                'label' => 'type',
                'required' => false,
                'class' => 'AdminBundle\Entity\SettingsTransports',
                'property' => 'display_name',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                ),
                'query_builder' => function (EntityRepository $er) use($options) {
                    return $er->createQueryBuilder("t")
                        ->leftJoin('t.provider', 'p')
                        ->Where('p.id = :provider')
                        ->setParameter('provider', (int) $options['provider'])
                        ->orderBy('t.seqno', 'ASC');
                },
            ))

            ->add('publicMandatory', 'choice', array(
                'label' => 'Le salarié est-il contraint d\'utiliser sa voiture personnelle
                soit à cause de difficultés d\'horaires,
                soit à causede l\'inexistance de transports en commun',
                'choices' => array(1 => 'Oui', 0 => 'Non'),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'attr' => array(
                    'ng-model' => 'new.choice',
                    'class' => 'form-control radio',
                ),
                'label_attr' => array(
                    'class' => 'radio'
                ),
            ))
//            ->add('publicMandatory', 'choice', array(
//                'choices'  => array(
//                    'Yes' => 1,
//                    'No' => 2,
//                ),
//                'choices_as_values' => true,'multiple'=>false,'expanded'=>true
//            ))



            ->add('amount', 'text', array('required' => true,
                'label' => 'Montant ',
                'attr' => array(
                    'class' => 'form-control',
                    'id' => 'amount',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('comment', 'textarea', array(
                'label' => 'Commentaire',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right'
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'TransportBundle\Entity\hrm_transport',
            'provider' => null,
            'company' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'transportbundle_hrm_transport';
    }


}
