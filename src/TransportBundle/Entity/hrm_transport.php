<?php

namespace TransportBundle\Entity;

/**
 * hrm_transport
 */
class hrm_transport
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var float
     */
    private $amount;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * @var integer
     */
    private $createUid;

    /**
     * @var \DateTime
     */
    private $lastUpdate;

    /**
     * @var integer
     */
    private $lastUpdateUid;

    /**
     * @var boolean
     */
    private $publicMandatory;

    /**
     * @var \DateTime
     */
    private $startDate;

    /**
     * @var \DateTime
     */
    private $endDate;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \FileBundle\Entity\attachments
     */
    private $attachments;

    /**
     * @var \AdminBundle\Entity\SettingsTransports
     */
    private $typetransport;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $contributor;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $validator;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $requester;

    /**
     * @var \AdminBundle\Entity\SettingsStatus
     */
    private $settings_status;

    /**
     * @var \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    private $settings_pay_period;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set amount
     *
     * @param float $amount
     *
     * @return hrm_transport
     */
    public function setAmount($amount)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount
     *
     * @return float
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return hrm_transport
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_transport
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_transport
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_transport
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_transport
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->lastUpdateUid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->lastUpdateUid;
    }

    /**
     * Set publicMandatory
     *
     * @param boolean $publicMandatory
     *
     * @return hrm_transport
     */
    public function setPublicMandatory($publicMandatory)
    {
        $this->publicMandatory = $publicMandatory;

        return $this;
    }

    /**
     * Get publicMandatory
     *
     * @return boolean
     */
    public function getPublicMandatory()
    {
        return $this->publicMandatory;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return hrm_transport
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }

    /**
     * Set endDate
     *
     * @param \DateTime $endDate
     *
     * @return hrm_transport
     */
    public function setEndDate($endDate)
    {
        $this->endDate = $endDate;

        return $this;
    }

    /**
     * Get endDate
     *
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return hrm_transport
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set attachments
     *
     * @param \FileBundle\Entity\attachments $attachments
     *
     * @return hrm_transport
     */
    public function setAttachments(\FileBundle\Entity\attachments $attachments = null)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Get attachments
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Set typetransport
     *
     * @param \AdminBundle\Entity\SettingsTransports $typetransport
     *
     * @return hrm_transport
     */
    public function setTypetransport(\AdminBundle\Entity\SettingsTransports $typetransport = null)
    {
        $this->typetransport = $typetransport;

        return $this;
    }

    /**
     * Get typetransport
     *
     * @return \AdminBundle\Entity\SettingsTransports
     */
    public function getTypetransport()
    {
        return $this->typetransport;
    }

    /**
     * Set contributor
     *
     * @param \UserBundle\Entity\HrmEmployee $contributor
     *
     * @return hrm_transport
     */
    public function setContributor(\UserBundle\Entity\HrmEmployee $contributor = null)
    {
        $this->contributor = $contributor;

        return $this;
    }

    /**
     * Get contributor
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getContributor()
    {
        return $this->contributor;
    }

    /**
     * Set validator
     *
     * @param \UserBundle\Entity\HrmEmployee $validator
     *
     * @return hrm_transport
     */
    public function setValidator(\UserBundle\Entity\HrmEmployee $validator = null)
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * Get validator
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * Set requester
     *
     * @param \UserBundle\Entity\HrmEmployee $requester
     *
     * @return hrm_transport
     */
    public function setRequester(\UserBundle\Entity\HrmEmployee $requester = null)
    {
        $this->requester = $requester;

        return $this;
    }

    /**
     * Get requester
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getRequester()
    {
        return $this->requester;
    }

    /**
     * Set settingsStatus
     *
     * @param \AdminBundle\Entity\SettingsStatus $settingsStatus
     *
     * @return hrm_transport
     */
    public function setSettingsStatus(\AdminBundle\Entity\SettingsStatus $settingsStatus = null)
    {
        $this->settings_status = $settingsStatus;

        return $this;
    }

    /**
     * Get settingsStatus
     *
     * @return \AdminBundle\Entity\SettingsStatus
     */
    public function getSettingsStatus()
    {
        return $this->settings_status;
    }

    /**
     * Set settingsPayPeriod
     *
     * @param \PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod
     *
     * @return hrm_transport
     */
    public function setSettingsPayPeriod(\PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod = null)
    {
        $this->settings_pay_period = $settingsPayPeriod;

        return $this;
    }

    /**
     * Get settingsPayPeriod
     *
     * @return \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    public function getSettingsPayPeriod()
    {
        return $this->settings_pay_period;
    }
    /**
     * @var \AdminBundle\Entity\SettingsTransports
     */
    private $type;


    /**
     * Set type
     *
     * @param \AdminBundle\Entity\SettingsTransports $type
     *
     * @return hrm_transport
     */
    public function setType(\AdminBundle\Entity\SettingsTransports $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \AdminBundle\Entity\SettingsTransports
     */
    public function getType()
    {
        return $this->type;
    }
}
