<?php

namespace PaydayVariationsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class PaydayVariationsType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        
        $roles = $options['user_roles']->getRoles();
        $rolesTab = array_map(function($role){ 
          return $role->getRole(); 
        }, $roles);
        if(in_array('ROLE_COLLABORATEUR', $rolesTab, true)){            
              $type = array("Avance" => "Avance", "Acompte" => "Acompte"); 
        }else{$type = array("Saisie" => "Saisie", "Avance" => "Avance", "Acompte" => "Acompte");}

        $builder
            ->add('amount', 'text', array(
                'label' => 'TTC',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right'
                )
            ))
            ->add('nbre_months', 'text', array(
                'label' => 'nbr months',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right'
                )
            ))
            ->add('type', 'choice', array(
                    'label' => 'Type de la demande',
//                    'empty_value' => 'Sélectionner un type',
                    'choices' => $type,
                    'required' => true,
                    'attr' => array('class' => 'form-control',
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                )
            )
            ->add('date', 'date', array(
                'label' => 'Date du frais',
                'required' => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'class' => 'form-control datepicker',
                    'date-directive' => ''
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('comment', 'textarea', array(
                'label' => 'Commentaire',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right'
                )
            ))
            ->add('settings_status', 'entity', array(
                'label' => 'Statut',
//                'empty_value' => 'Sélectionner un statut',
                'required' => true,
                'class' => 'AdminBundle\Entity\SettingsStatus',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('s')
                        ->leftJoin('s.provider', 'p')
                        ->where("s.id != :statut")
                        ->setParameter('statut', 4)
                        ;
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('requester', 'entity', array(
                'label' => 'Collaborateur',
//                'empty_value' => 'Sélectionner un requester',
                'required' => true,
                'disabled' => true,
                'read_only' => true,
                'class' => 'UserBundle\Entity\HrmEmployee',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->Where('u.company = :value')
//                        ->andwhere('e.id = :valueUser')
                        ->setParameter('value', (int)$options['company']);
//                        ->setParameter('valueUser', (int)$options['currentUser']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('contributor', 'entity', array(
                'label' => 'Collaborateur',
//                'empty_value' => 'Sélectionner un collaborateur',
                'required' => true,
                'class' => 'UserBundle\Entity\HrmEmployee',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->where("e.roles LIKE '%COLLABORATEUR%' OR e.roles LIKE '%RESPONSABLE%'")
                        ->andwhere('u.company = :value')
                        ->setParameter('value', (int)$options['company']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('validator', 'entity', array(
                'label' => 'Valideur de la demande',
//                'empty_value' => 'Sélectionner un valideur',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control',
                    'id' => 'id',
                    'required' => true,
                ),
                'class' => 'UserBundle\Entity\HrmEmployee',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->andwhere("e.roles LIKE '%RESPONSABLE%'")
                        ->andwhere('u.company = :value')
                        ->setParameter('value', (int)$options['company']);
                },
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'))
            )

            ->add('nature', 'entity', array(
                'label' => 'Valideur de la demande',
                'class' => 'AdminBundle\Entity\settings_paydayvar_nature',
                'property'=>'displayName',
                'required'=>false,
                'attr' => array(
                    'class' => 'form-control',
                    'id' => 'id',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix')))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'PaydayVariationsBundle\Entity\PaydayVariations',
            'company' => null,
            'provider' => null,
            'user_roles' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'paydayvariationsbundle_paydayvariations';
    }


}
