<?php

namespace PaydayVariationsBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class paydayvariationsService {

    protected $em;
    private $container;

    public function __construct(EntityManager $entityManager, Container $container) {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function deleteAllPaydayVariations() {
        $qb = $this->em->createQuery('DELETE PaydayVariationsBundle:PaydayVariations h')->execute();
    }

    public function findPaydayVariations($value) {
        $paydayvariations = $this->container->get('doctrine')->getEntityManager()->getRepository('PaydayVariationsBundle:PaydayVariations')
                ->findOneBy(array('amount' => $value));
        if ($paydayvariations)
            return $paydayvariations;
        else
            return null;
    }

    public function findListPaydayVariations($value) {
        $paydayvariations = $this->container->get('doctrine')->getEntityManager()->getRepository('PaydayVariationsBundle:PaydayVariations')
                ->findBy(array('amount' => $value));
        return $paydayvariations;
    }

}
