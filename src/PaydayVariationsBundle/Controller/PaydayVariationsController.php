<?php

namespace PaydayVariationsBundle\Controller;

use PaydayVariationsBundle\Entity\PaydayVariations;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FileBundle\Entity\attachments;

/**
 * Paydayvariation controller.
 *
 */
class PaydayVariationsController extends Controller
{
    public function layoutAction()
    {
        return $this->render('paydayvariations/layout.html.twig');
    }

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $paydayVariations = $em->getRepository('PaydayVariationsBundle:PaydayVariations')->findAll();

        return $this->render('paydayvariations/index.html.twig', array(
            'paydayVariations' => $paydayVariations,
        ));
    }

    public function newAction(Request $request)
    {
        $paydayVariation = new PaydayVariations();
        $em = $this->getDoctrine()->getManager();
        $companyId = $this->container->get('settingsbundle.preference.service')->getCompanyId();
        $providerId = $this->container->get('settingsbundle.preference.service')->getProviderId();
        $form = $this->createForm('PaydayVariationsBundle\Form\PaydayVariationsType', $paydayVariation, array(
            'provider' => $providerId, 'company' => $companyId,
            'user_roles' => $this->get('security.context')->getToken()
        ));
        $form->handleRequest($request);
        return $this->render('paydayvariations/new.html.twig', array(
            'paydayVariation' => $paydayVariation,
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request)
    {
        $paydayVariation = new PaydayVariations();
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');
        $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
        $infos_user = $this->container->get('settingsbundle.preference.service')->getEmpData();

        $company_id = $infos_user->getCompany()->getId();
        $provider = $infos_user->getCompany()->getProvider()->getId();
        $form = $this->createForm('PaydayVariationsBundle\Form\PaydayVariationsType', $paydayVariation, array(
            'provider' => $provider, 'company' => $company_id,
            'user_roles' => $this->get('security.context')->getToken()
//            'currentUser' => $current_user_id,
        ));

        $form->handleRequest($request);


        return $this->render('paydayvariations/edit.html.twig', array(
            'paydayVariation' => $paydayVariation,
            'form' => $form->createView(),
        ));
    }


    public function removeAction(Request $request, PaydayVariations $payday)
    {
        if ($request->isMethod('DELETE')) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($payday);
            $em->flush($payday);
            return new \Symfony\Component\BrowserKit\Response('Remove success', 200);
        }
    }

    public function payDayVariationListAction()
    {
        $em = $this->getDoctrine()->getManager();
        $payday = new PaydayVariations();
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();
        $companyData = $empData->getCompany();

        $company_id = $companyData->getId();

        $securityContext = $this->container->get('security.authorization_checker');

        if ($securityContext->isGranted('ROLE_RESPONSABLE')) {

            $result = $em->getRepository('PaydayVariationsBundle:PaydayVariations')->FindForResp($company_id, $current_user_id);

        } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {

            $result = $em->getRepository('PaydayVariationsBundle:PaydayVariations')->FindForCollab($current_user_id);

        }

        $users = $em->getRepository('UserBundle:HrmEmployee')->FindCollaborateurByCompanyId($company_id);

        return array('result' => $result, 'users' => $users);
    }

    public function addAction(Request $request)
    {
        $payday = new PaydayVariations();
        $form = $this->createForm('PaydayVariationsBundle\Form\PaydayVariationsType', $payday, array('user_roles' => $this->get('security.context')->getToken()));
        $form->handleRequest($request);

        $body = $request->getContent();
        $data = json_decode($body, true);

        $em = $this->getDoctrine()->getManager();
        //set default status if ROLE_COLLABORATEUR else set with the submited value
        $securityContext = $this->container->get('security.authorization_checker');

        if ($request->isMethod('POST')) {
            // will be get error if not deleted settings_pay_period from FormType before submit
            $form->remove('settings_pay_period');
            $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $current_user_id = $empData->getId();
//            $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();
            $form->submit($data);
            $payday->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $payday->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']));//['id']
            $payday->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));

            if (isset($data['nature'])) {
                $payday->setNature($em->getRepository('AdminBundle:settings_paydayvar_nature')->find($data['nature']['id']));
            }

            $payday->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));

            if ($request->request->has('attachments')) {

                if ($data['attachments'] != null) {
                    if ($payday->getAttachments() != null) {
                        $attachment = $payday->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if ((isset($data['attachments']['mimetype'])) && ($data['attachments']['mimetype'] != null))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $payday->setAttachments($attachment);
                } else {
                    $payday->setAttachments(null);
                }

            }

            //Add default value on create Action
            $payday->setCreateDate(new \DateTime('now'));
            $payday->setCreateUid($current_user_id);
            $payday->setLastUpdateUid($current_user_id);
            $payday->setLastUpdate(new \DateTime('now'));

           //  try to add first open one of pay period
            $ret_payperiod =  $this->setOpenedSettingsPayPeriod($em, $payday);
            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }
            
            //send mail to responsable
//            $TranslateMail = $this->get('mailingbundle.email.send.service');
//            $this->get('mailingbundle.email.send.service')->SendBonusMailToResponsable($TranslateMail->TagsTranslateBonus('titleNew'), $TranslateMail->TagsTranslateBonus('etatNew'), $payday, $em->getRepository('UserBundle:HrmEmployee')->find($data['manager']), $payday->getAmount());

            $TranslateMail = $this->get('mailingbundle.email.send.service');
            if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $this->get('mailingbundle.email.send.service')->Notify($payday, 'RESPONSABLE', $TranslateMail->TagsTranslatePayDay('titleNew'), $TranslateMail->TagsTranslatePayDay('etatNew'), $TranslateMail->TagsTranslatePayDay('typeRequest'), $TranslateMail->TagsTranslatePayDay('folder'), $TranslateMail->TagsTranslatePayDay('viewName'));
            } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $this->get('mailingbundle.email.send.service')->Notify($payday, 'COLLABORATEUR', $TranslateMail->TagsTranslatePayDay('titleNew'), $TranslateMail->TagsTranslatePayDay('etatNew'), $TranslateMail->TagsTranslatePayDay('typeRequest'), $TranslateMail->TagsTranslatePayDay('folder'), $TranslateMail->TagsTranslatePayDay('viewName'));
            }



//            try {
//            dump($data);
//            die();
            $em->persist($payday);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
//            } catch (\Exception $exc) {
//                return new \Symfony\Component\BrowserKit\Response('Error to save data', 201);
//            }
        }
    }

    public function updateAction(Request $request, PaydayVariations $payday)
    {
        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $entity = $em->getRepository('PaydayVariationsBundle:PaydayVariations')->find($data['id']);

        if (!is_object($entity)) {
            return new \Symfony\Component\BrowserKit\Response('DATA_NOT_FOUND', 404);
        }

        $update_form = $this->createForm('PaydayVariationsBundle\Form\PaydayVariationsType', $entity, array('user_roles' => $this->get('security.context')->getToken()));

        if ($request->isMethod('PUT')) {

            // will be get error if not deleted settings_pay_period from FormType before submit
            $update_form->remove('settings_pay_period');

            $securityContext = $this->container->get('security.authorization_checker');
            //get current connected user
            $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $current_user_id = $empData->getId();
//            $current_user_id = $this->container->get('security.context')->getToken()->getUser()->getId();

            $update_form->submit($data);

            //get the Id of user who request ik
            $infos_user = $em->getRepository('UserBundle:hrm_user')->find($current_user_id);
            $collab_id = $entity->getCreateUid();
//
//
//            $payday->setUser($em->getRepository('UserBundle:hrm_user')->find($collab_id));


//
            if ($data['attachments'] != null) {
                if ($request->request->has('attachments')) {
                    if ($entity->getAttachments() != null) {
                        $attachment = $entity->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if (isset($data['attachments']['mimetype']))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $payday->setAttachments($attachment);
                }
            } else {

                $payday->setAttachments(null);
            }


//            if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
//                $payday->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
//                $this->statusInsertion($data['status'], $payday);
//                if ($data['status'] == 1) {
////                    $this->SendMailToResponsable('Nouvelle demande');
//                }
//            } else if ($securityContext->isGranted('ROLE_ADMIN')) {
//                $payday->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
//                $this->statusInsertion($data['status'], $payday);
//            } else if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
//                $payday->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
//                $this->statusInsertion($data['status'], $payday);
//
//                switch ($data['status']) {
//                    case 2:
//                        $TranslateMail = $this->get('mailingbundle.email.send.service');
//                        $this->get('mailingbundle.email.send.service')->SendMail($entity, $TranslateMail->TagsTranslateExpense('confirmMsg'), $TranslateMail->TagsTranslateExpense('confirmEtat'));
//
//                        break;
//                    case 3:
//                        $TranslateMail = $this->get('mailingbundle.email.send.service');
//                        $this->get('mailingbundle.email.send.service')->SendMail($entity, $TranslateMail->TagsTranslateExpense('cancelMsg'), $TranslateMail->TagsTranslateExpense('cancelEtat'));
//                        break;
//                }
//            }
            $payday->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $payday->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $payday->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));
            if (isset($data['nature'])) {
                $payday->setNature($em->getRepository('AdminBundle:settings_paydayvar_nature')->find($data['nature']['id']));
            }
            $payday->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));


            //Add default value on create Action
            $payday->setCreateDate(new \DateTime('now'));
            $payday->setCreateUid($current_user_id);
            $payday->setLastUpdateUid($current_user_id);
            $payday->setLastUpdate(new \DateTime('now'));

            // try to add first open one of pay period
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $payday);
            if($ret_payperiod !== true)
            {
                return $ret_payperiod;
            }

            $em->persist($entity);
            $em->flush();

            return new \Symfony\Component\BrowserKit\Response('Update done', 200);
        }
    }


    public function ConfirmAction(Request $request, PaydayVariations $payday)
    {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);
        $payday->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['confirm']));
        $entity = $em->getRepository('PaydayVariationsBundle:PaydayVariations')->find($payday);
        
        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $entity);

        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }
        
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($payday, 'RESPONSABLE', $TranslateMail->TagsTranslatePayDay('confirmMsg'), $TranslateMail->TagsTranslatePayDay('confirmEtat'), $TranslateMail->TagsTranslatePayDay('typeRequest'), $TranslateMail->TagsTranslatePayDay('folder'), $TranslateMail->TagsTranslatePayDay('viewName'));



        $em->persist($payday);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }


    public function CancelAction(Request $request, PaydayVariations $payday)
    {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $payday->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['cancel']));

        $entity = $em->getRepository('PaydayVariationsBundle:PaydayVariations')->find($payday);
        
        // try to add first open one of pay period
         $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $entity);
         if($ret_payperiod !== true)
         {
             return $ret_payperiod;
         }
         
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($entity, 'RESPONSABLE', $TranslateMail->TagsTranslatePayDay('cancelMsg'), $TranslateMail->TagsTranslatePayDay('cancelEtat'), $TranslateMail->TagsTranslatePayDay('typeRequest'), $TranslateMail->TagsTranslatePayDay('folder'), $TranslateMail->TagsTranslatePayDay('viewName'));


        $em->persist($payday);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }
    
    /**
     *
     * @param type $em
     * @param type $entity
     * @param type $payPeriodDate celui envoyÃ© depuis from
     * @return boolean/object
     */
    private function setOpenedSettingsPayPeriod($em, $entity, $payPeriodDate = null) {
        $ret = '';
        // affect first pay period

        if ($payPeriodDate == null) {
            $searchBy = array("companyId" => $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId(),
                "status" => "open");
        } else {
            $searchBy = array("companyId" => $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId());
        }

        $payperiodlist = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')
            ->findBy($searchBy, array('startDate' => 'ASC'));
        if ($payperiodlist) {

            if ($payPeriodDate == null) {
                $entity->setSettingsPayPeriod($payperiodlist[0]);
                $ret = true;
            } else {
                foreach ($payperiodlist as $key => $value) {
                    if ($value->getStartDate()->format('m/Y') == $payPeriodDate && ($value->getStatus() === 'open')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = true;
                        break;
                    }
                    if (($value->getStartDate()->format('m/Y') === $payPeriodDate ) && ($value->getStatus() === 'close')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = false;
                        break;
                    }
                }
            }

            if ($ret === true)
                return true;
            if ($ret === false)
                return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_CLOSED_PAY_PERIOD"));

            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_NOT_PAY_PERIOD"));
        }
        else {
            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_EMPTY_PAY_PERIOD"));
        }
    }
}
