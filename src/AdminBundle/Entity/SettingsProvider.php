<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SettingsProvider
 *
 * @ORM\Table(name="hrm_settings_provider")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\SettingsProviderRepository")
 */
class SettingsProvider {

    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="seqno", type="integer",nullable=true)
     */
    private $seqno;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=100)
     */
    private $display_name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_company_allowed", type="boolean",nullable=true)
     */
    private $is_company_allowed;

    /**
     * @var Date
     *
     * @ORM\Column(name="create_date", type="date", nullable=true)
     */
    private $create_date;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\hrm_user")
     * @ORM\JoinColumn(name="create_uid", referencedColumnName="uid")
     */
    private $createUid;

    /**
     * @var Date
     *
     * @ORM\Column(name="last_update", type="date", nullable=true)
     */
    private $last_update;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\hrm_user")
     * @ORM\JoinColumn(name="last_update_uid", referencedColumnName="uid")
     */
    private $last_update_uid;

    /**
     * @var string
     *
     * @ORM\Column(name="portal_return_url", type="string", length=100, nullable=true)
     */
    private $portal_return_url;

    /**
     * @var string
     *
     * @ORM\Column(name="help_url", type="string", length=100, nullable=true)
     */
    private $help_url;
    
    /**
     * @var string
     *
     * @ORM\Column(name="base_currency_iso_code", type="string", length=100, nullable=true)
     */
    private $base_currency_iso_code;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\HrmEmployee", mappedBy="provider")
     */
    private $employee;

    /**
     * Constructor
     */
    public function __construct() {
        $this->employee = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set seqno
     *
     * @param integer $seqno
     *
     * @return SettingsProvider
     */
    public function setSeqno($seqno) {
        $this->seqno = $seqno;

        return $this;
    }

    /**
     * Get seqno
     *
     * @return integer
     */
    public function getSeqno() {
        return $this->seqno;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return SettingsProvider
     */
    public function setDisplayName($displayName) {
        $this->display_name = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName() {
        return $this->display_name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SettingsProvider
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return SettingsProvider
     */
    public function setCreateDate($createDate) {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate() {
        return $this->create_date;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return SettingsProvider
     */
    public function setLastUpdate($lastUpdate) {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate() {
        return $this->last_update;
    }

    /**
     * Set portalReturnUrl
     *
     * @param string $portalReturnUrl
     *
     * @return SettingsProvider
     */
    public function setPortalReturnUrl($portalReturnUrl) {
        $this->portal_return_url = $portalReturnUrl;

        return $this;
    }

    /**
     * Get portalReturnUrl
     *
     * @return string
     */
    public function getPortalReturnUrl() {
        return $this->portal_return_url;
    }

    /**
     * Set helpUrl
     *
     * @param string $helpUrl
     *
     * @return SettingsProvider
     */
    public function setHelpUrl($helpUrl) {
        $this->help_url = $helpUrl;

        return $this;
    }

    /**
     * Get helpUrl
     *
     * @return string
     */
    public function getHelpUrl() {
        return $this->help_url;
    }

    /**
     * Add employee
     *
     * @param \UserBundle\Entity\HrmEmployee $employee
     *
     * @return SettingsProvider
     */
    public function addEmployee(\UserBundle\Entity\HrmEmployee $employee) {
        $this->employee[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param \UserBundle\Entity\HrmEmployee $employee
     */
    public function removeEmployee(\UserBundle\Entity\HrmEmployee $employee) {
        $this->employee->removeElement($employee);
    }

    /**
     * Get employee
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployee() {
        return $this->employee;
    }

    public function __toString() {
        return $this->getDisplayName();
    }

    /**
     * Set isCompanyAllowed
     *
     * @param boolean $isCompanyAllowed
     *
     * @return SettingsProvider
     */
    public function setIsCompanyAllowed($isCompanyAllowed)
    {
        $this->is_company_allowed = $isCompanyAllowed;

        return $this;
    }

    /**
     * Get isCompanyAllowed
     *
     * @return boolean
     */
    public function getIsCompanyAllowed()
    {
        return $this->is_company_allowed;
    }

    /**
     * Set baseCurrencyIsoCode
     *
     * @param string $baseCurrencyIsoCode
     *
     * @return SettingsProvider
     */
    public function setBaseCurrencyIsoCode($baseCurrencyIsoCode)
    {
        $this->base_currency_iso_code = $baseCurrencyIsoCode;

        return $this;
    }

    /**
     * Get baseCurrencyIsoCode
     *
     * @return string
     */
    public function getBaseCurrencyIsoCode()
    {
        return $this->base_currency_iso_code;
    }

    /**
     * Set createUid
     *
     * @param \UserBundle\Entity\hrm_user $createUid
     *
     * @return SettingsProvider
     */
    public function setCreateUid(\UserBundle\Entity\hrm_user $createUid = null)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return \UserBundle\Entity\hrm_user
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdateUid
     *
     * @param \UserBundle\Entity\hrm_user $lastUpdateUid
     *
     * @return SettingsProvider
     */
    public function setLastUpdateUid(\UserBundle\Entity\hrm_user $lastUpdateUid = null)
    {
        $this->last_update_uid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return \UserBundle\Entity\hrm_user
     */
    public function getLastUpdateUid()
    {
        return $this->last_update_uid;
    }
}
