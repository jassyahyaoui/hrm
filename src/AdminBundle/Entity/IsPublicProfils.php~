<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * IsPublicProfils
 *
 * @ORM\Table(name="hrm_is_public_profils")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\IsPublicProfilsRepository")
 */
class IsPublicProfils
{
    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_pub_gender", type="boolean", nullable=true)
     */
    private $isPubGender;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_pub_name_usage", type="boolean", nullable=true)
     */
    private $isPubNameUsage;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_pub_birth_date", type="boolean", nullable=true)
     */
    private $isPubBirthDate;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_pub_nation_code", type="boolean", nullable=true)
     */
    private $isPubNationCode;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_pub_address", type="boolean", nullable=true)
     */
    private $isPubAddress;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_pub_zip_code", type="boolean", nullable=true)
     */
    private $isPubZipCode;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_pub_country_code", type="boolean", nullable=true)
     */
    private $isPubCountryCode;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_pub_study_type", type="boolean", nullable=true)
     */
    private $isPubStudyType;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_pub_study_level", type="boolean", nullable=true)
     */
    private $isPubStudyLevel;


    /**
     * @ORM\OneToOne(targetEntity="UserBundle\Entity\HrmEmployee", inversedBy="public_profil")
     * @ORM\JoinColumn(name="employee_id", referencedColumnName="uid")
     * 
     **/
    private $employee;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isPubGender
     *
     * @param boolean $isPubGender
     *
     * @return IsPublicProfils
     */
    public function setIsPubGender($isPubGender)
    {
        $this->isPubGender = $isPubGender;

        return $this;
    }

    /**
     * Get isPubGender
     *
     * @return boolean
     */
    public function getIsPubGender()
    {
        return $this->isPubGender;
    }

    /**
     * Set isPubNameUsage
     *
     * @param boolean $isPubNameUsage
     *
     * @return IsPublicProfils
     */
    public function setIsPubNameUsage($isPubNameUsage)
    {
        $this->isPubNameUsage = $isPubNameUsage;

        return $this;
    }

    /**
     * Get isPubNameUsage
     *
     * @return boolean
     */
    public function getIsPubNameUsage()
    {
        return $this->isPubNameUsage;
    }

    /**
     * Set isPubBirthDate
     *
     * @param boolean $isPubBirthDate
     *
     * @return IsPublicProfils
     */
    public function setIsPubBirthDate($isPubBirthDate)
    {
        $this->isPubBirthDate = $isPubBirthDate;

        return $this;
    }

    /**
     * Get isPubBirthDate
     *
     * @return boolean
     */
    public function getIsPubBirthDate()
    {
        return $this->isPubBirthDate;
    }

    /**
     * Set isPubNationCode
     *
     * @param boolean $isPubNationCode
     *
     * @return IsPublicProfils
     */
    public function setIsPubNationCode($isPubNationCode)
    {
        $this->isPubNationCode = $isPubNationCode;

        return $this;
    }

    /**
     * Get isPubNationCode
     *
     * @return boolean
     */
    public function getIsPubNationCode()
    {
        return $this->isPubNationCode;
    }

    /**
     * Set isPubAddress
     *
     * @param boolean $isPubAddress
     *
     * @return IsPublicProfils
     */
    public function setIsPubAddress($isPubAddress)
    {
        $this->isPubAddress = $isPubAddress;

        return $this;
    }

    /**
     * Get isPubAddress
     *
     * @return boolean
     */
    public function getIsPubAddress()
    {
        return $this->isPubAddress;
    }

    /**
     * Set isPubZipCode
     *
     * @param boolean $isPubZipCode
     *
     * @return IsPublicProfils
     */
    public function setIsPubZipCode($isPubZipCode)
    {
        $this->isPubZipCode = $isPubZipCode;

        return $this;
    }

    /**
     * Get isPubZipCode
     *
     * @return boolean
     */
    public function getIsPubZipCode()
    {
        return $this->isPubZipCode;
    }

    /**
     * Set isPubCountryCode
     *
     * @param boolean $isPubCountryCode
     *
     * @return IsPublicProfils
     */
    public function setIsPubCountryCode($isPubCountryCode)
    {
        $this->isPubCountryCode = $isPubCountryCode;

        return $this;
    }

    /**
     * Get isPubCountryCode
     *
     * @return boolean
     */
    public function getIsPubCountryCode()
    {
        return $this->isPubCountryCode;
    }

    /**
     * Set isPubStudyType
     *
     * @param boolean $isPubStudyType
     *
     * @return IsPublicProfils
     */
    public function setIsPubStudyType($isPubStudyType)
    {
        $this->isPubStudyType = $isPubStudyType;

        return $this;
    }

    /**
     * Get isPubStudyType
     *
     * @return boolean
     */
    public function getIsPubStudyType()
    {
        return $this->isPubStudyType;
    }

    /**
     * Set isPubStudyLevel
     *
     * @param boolean $isPubStudyLevel
     *
     * @return IsPublicProfils
     */
    public function setIsPubStudyLevel($isPubStudyLevel)
    {
        $this->isPubStudyLevel = $isPubStudyLevel;

        return $this;
    }

    /**
     * Get isPubStudyLevel
     *
     * @return boolean
     */
    public function getIsPubStudyLevel()
    {
        return $this->isPubStudyLevel;
    }

    /**
     * Set employee
     *
     * @param \UserBundle\Entity\HrmEmployee $employee
     *
     * @return IsPublicProfils
     */
    public function setEmployee(\UserBundle\Entity\HrmEmployee $employee = null)
    {
        $this->employee = $employee;

        return $this;
    }

    /**
     * Get employee
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getEmployee()
    {
        return $this->employee;
    }
}
