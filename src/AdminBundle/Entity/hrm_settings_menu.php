<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_settings_menu
 *
 * @ORM\Table(name="hrm_settings_menu")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\hrm_settings_menuRepository")
 */
class hrm_settings_menu
{
    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="seqno", type="integer", nullable=true)
     */
    private $seqno;


    /**
     * @ORM\OneToMany(targetEntity="hrm_settings_menu", mappedBy="associatedTo")
     */
    private $children;

    /**
     * @ORM\ManyToOne(targetEntity="hrm_settings_menu", inversedBy="children")
     * @ORM\JoinColumn(name="associated_to_id", referencedColumnName="uid")
     */
    private $associatedTo;
    

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="icon_links", type="string", length=255)
     */
    private $iconLinks;

    /**
     * @var string
     *
     * @ORM\Column(name="route_links", type="string", length=255)
     */
    private $routeLinks;


    /**
     * @var DateTime
     *
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $create_date;
    
    /**
     * @var int
     *
     * @ORM\Column(name="create_uid", type="integer", nullable=true)
     */
    private $createUid;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $last_update;
    

    /**
     * @var int
     *
     * @ORM\Column(name="last_update_uid", type="integer", nullable=true)
     */
    private $last_update_uid;
    
    public function __construct() {
        $this->children = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set seqno
     *
     * @param integer $seqno
     *
     * @return hrm_settings_menu
     */
    public function setSeqno($seqno)
    {
        $this->seqno = $seqno;

        return $this;
    }

    /**
     * Get seqno
     *
     * @return int
     */
    public function getSeqno()
    {
        return $this->seqno;
    }

    /**
     * Set associatedTo
     *
     * @param string $associatedTo
     *
     * @return hrm_settings_menu
     */
    public function setAssociatedTo($associatedTo)
    {
        $this->associatedTo = $associatedTo;

        return $this;
    }

    /**
     * Get associatedTo
     *
     * @return string
     */
    public function getAssociatedTo()
    {
        return $this->associatedTo;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return hrm_settings_menu
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set iconLinks
     *
     * @param string $iconLinks
     *
     * @return hrm_settings_menu
     */
    public function setIconLinks($iconLinks)
    {
        $this->iconLinks = $iconLinks;

        return $this;
    }

    /**
     * Get iconLinks
     *
     * @return string
     */
    public function getIconLinks()
    {
        return $this->iconLinks;
    }

    /**
     * Set routeLinks
     *
     * @param string $routeLinks
     *
     * @return hrm_settings_menu
     */
    public function setRouteLinks($routeLinks)
    {
        $this->routeLinks = $routeLinks;

        return $this;
    }

    /**
     * Get routeLinks
     *
     * @return string
     */
    public function getRouteLinks()
    {
        return $this->routeLinks;
    }

    /**
     * Add child
     *
     * @param \AdminBundle\Entity\hrm_settings_menu $child
     *
     * @return hrm_settings_menu
     */
    public function addChild(\AdminBundle\Entity\hrm_settings_menu $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param \AdminBundle\Entity\hrm_settings_menu $child
     */
    public function removeChild(\AdminBundle\Entity\hrm_settings_menu $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_settings_menu
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_settings_menu
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_settings_menu
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_settings_menu
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->last_update_uid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->last_update_uid;
    }
}
