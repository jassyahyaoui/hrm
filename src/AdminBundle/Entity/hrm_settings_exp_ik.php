<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_settings_exp_ik
 *
 * @ORM\Table(name="hrm_settings_exp_ik")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\hrm_settings_exp_ikRepository")
 */
class hrm_settings_exp_ik {

    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="seqno", type="integer",nullable=true)
     */
    private $seqno;

    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=100)
     */
    private $display_name;

    /**
     * @var string
     *
     * @ORM\Column(name="year", type="string", length=10)
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsProvider")
     * @ORM\JoinColumn(name="provider_id", referencedColumnName="uid")
     */
    private $provider;

    /**
     * @var float
     *
     * @ORM\Column(name="coeff_value", type="float")
     */
    private $coeffValue;

    /**
     * @var string
     *
     * @ORM\Column(name="external_code", type="string", nullable=true)
     */
    private $external_code;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $create_date;

    /**
     * @var int
     *
     * @ORM\Column(name="create_uid", type="integer", nullable=true)
     */
    private $createUid;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $last_update;

    /**
     * @var int
     *
     * @ORM\Column(name="last_update_uid", type="integer", nullable=true)
     */
    private $last_update_uid;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set seqno
     *
     * @param integer $seqno
     *
     * @return hrm_settings_exp_ik
     */
    public function setSeqno($seqno) {
        $this->seqno = $seqno;

        return $this;
    }

    /**
     * Get seqno
     *
     * @return integer
     */
    public function getSeqno() {
        return $this->seqno;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return hrm_settings_exp_ik
     */
    public function setDisplayName($displayName) {
        $this->display_name = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName() {
        return $this->display_name;
    }

    /**
     * Set year
     *
     * @param string $year
     *
     * @return hrm_settings_exp_ik
     */
    public function setYear($year) {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return string
     */
    public function getYear() {
        return $this->year;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return hrm_settings_exp_ik
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set coeffValue
     *
     * @param float $coeffValue
     *
     * @return hrm_settings_exp_ik
     */
    public function setCoeffValue($coeffValue) {
        $this->coeffValue = $coeffValue;

        return $this;
    }

    /**
     * Get coeffValue
     *
     * @return float
     */
    public function getCoeffValue() {
        return $this->coeffValue;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_settings_exp_ik
     */
    public function setCreateDate($createDate) {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate() {
        return $this->create_date;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_settings_exp_ik
     */
    public function setCreateUid($createUid) {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid() {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_settings_exp_ik
     */
    public function setLastUpdate($lastUpdate) {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate() {
        return $this->last_update;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_settings_exp_ik
     */
    public function setLastUpdateUid($lastUpdateUid) {
        $this->last_update_uid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid() {
        return $this->last_update_uid;
    }

    /**
     * Set provider
     *
     * @param \AdminBundle\Entity\SettingsProvider $provider
     *
     * @return hrm_settings_exp_ik
     */
    public function setProvider(\AdminBundle\Entity\SettingsProvider $provider = null) {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \AdminBundle\Entity\SettingsProvider
     */
    public function getProvider() {
        return $this->provider;
    }

    public function __toString() {
        return $this->getDisplayName() . " (" . $this->getCoeffValue() . ")";
    }

    /**
     * Set externalCode
     *
     * @param string $externalCode
     *
     * @return hrm_settings_exp_ik
     */
    public function setExternalCode($externalCode)
    {
        $this->external_code = $externalCode;

        return $this;
    }

    /**
     * Get externalCode
     *
     * @return string
     */
    public function getExternalCode()
    {
        return $this->external_code;
    }
}
