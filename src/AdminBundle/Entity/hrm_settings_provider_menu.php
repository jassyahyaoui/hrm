<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * hrm_settings_provider_menu
 *
 * @ORM\Table(name="hrm_settings_provider_menu")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\hrm_settings_provider_menuRepository")
 */
class hrm_settings_provider_menu
{
    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="seqno", type="integer", nullable=true)
     */
    private $seqno;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="display_menu", type="boolean")
     */
    private $displayMenu;


    /**
     * @ORM\ManyToOne(targetEntity="hrm_settings_menu")
     * @ORM\JoinColumn(name="setting_menu_id", referencedColumnName="uid")
     */ 
    private $setting_menu;

    /**
     * @ORM\ManyToOne(targetEntity="SettingsProvider")
     * @ORM\JoinColumn(name="settings_provider_id", referencedColumnName="uid")
     */ 
    private $settings_provider;
    
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $create_date;
    
    /**
     * @var int
     *
     * @ORM\Column(name="create_uid", type="integer", nullable=true)
     */
    private $createUid;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $last_update;
    

    /**
     * @var int
     *
     * @ORM\Column(name="last_update_uid", type="integer", nullable=true)
     */
    private $last_update_uid;
  

    
    

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set seqno
     *
     * @param integer $seqno
     *
     * @return hrm_settings_provider_menu
     */
    public function setSeqno($seqno)
    {
        $this->seqno = $seqno;

        return $this;
    }

    /**
     * Get seqno
     *
     * @return integer
     */
    public function getSeqno()
    {
        return $this->seqno;
    }

    /**
     * Set displayMenu
     *
     * @param boolean $displayMenu
     *
     * @return hrm_settings_provider_menu
     */
    public function setDisplayMenu($displayMenu)
    {
        $this->displayMenu = $displayMenu;

        return $this;
    }

    /**
     * Get displayMenu
     *
     * @return boolean
     */
    public function getDisplayMenu()
    {
        return $this->displayMenu;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_settings_provider_menu
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_settings_provider_menu
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_settings_provider_menu
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_settings_provider_menu
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->last_update_uid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->last_update_uid;
    }

    /**
     * Set settingMenu
     *
     * @param \AdminBundle\Entity\hrm_settings_menu $settingMenu
     *
     * @return hrm_settings_provider_menu
     */
    public function setSettingMenu(\AdminBundle\Entity\hrm_settings_menu $settingMenu = null)
    {
        $this->setting_menu = $settingMenu;

        return $this;
    }

    /**
     * Get settingMenu
     *
     * @return \AdminBundle\Entity\hrm_settings_menu
     */
    public function getSettingMenu()
    {
        return $this->setting_menu;
    }

    /**
     * Set settingsProvider
     *
     * @param \AdminBundle\Entity\SettingsProvider $settingsProvider
     *
     * @return hrm_settings_provider_menu
     */
    public function setSettingsProvider(\AdminBundle\Entity\SettingsProvider $settingsProvider = null)
    {
        $this->settings_provider = $settingsProvider;

        return $this;
    }

    /**
     * Get settingsProvider
     *
     * @return \AdminBundle\Entity\SettingsProvider
     */
    public function getSettingsProvider()
    {
        return $this->settings_provider;
    }
}
