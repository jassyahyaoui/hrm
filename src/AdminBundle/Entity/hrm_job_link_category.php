<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * hrm_job_link_category
 *
 * @ORM\Table(name="hrm_job_link_category")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\hrm_job_link_categoryRepository")
 */
class hrm_job_link_category
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;


    /**
     * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsProvider")
     * @ORM\JoinColumn(name="provider_id", referencedColumnName="uid")
     */
    private $provider;

    /**
     * @var int
     *
     * @ORM\Column(name="seqno", type="integer",nullable=true)
     */
    private $seqno;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $create_date;

    /**
     * @var int
     *
     * @ORM\Column(name="create_uid", type="integer", nullable=true)
     */
    private $createUid;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $last_update;

    /**
     * @var int
     *
     * @ORM\Column(name="last_update_uid", type="integer", nullable=true)
     */
    private $last_update_uid;
    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="AdminBundle\Entity\hrm_job_link_data", mappedBy="hrm_job_link_category")
     */
    private $hrm_job_link_data;

    /**
     * hrm_job_link_category constructor.
     * @param $hrm_job_link_data
     */
    public function __construct()
    {
        $this->hrm_job_link_data = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return hrm_job_link_category
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return hrm_job_link_category
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set seqno
     *
     * @param integer $seqno
     *
     * @return hrm_job_link_category
     */
    public function setSeqno($seqno)
    {
        $this->seqno = $seqno;

        return $this;
    }

    /**
     * Get seqno
     *
     * @return integer
     */
    public function getSeqno()
    {
        return $this->seqno;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_job_link_category
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_job_link_category
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_job_link_category
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_job_link_category
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->last_update_uid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->last_update_uid;
    }

    /**
     * Set provider
     *
     * @param \AdminBundle\Entity\SettingsProvider $provider
     *
     * @return hrm_job_link_category
     */
    public function setProvider(\AdminBundle\Entity\SettingsProvider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \AdminBundle\Entity\SettingsProvider
     */
    public function getProvider()
    {
        return $this->provider;
    }

    /**
     * Add hrmJobLinkDatum
     *
     * @param \AdminBundle\Entity\hrm_job_link_data $hrmJobLinkDatum
     *
     * @return hrm_job_link_category
     */
    public function addHrmJobLinkDatum(\AdminBundle\Entity\hrm_job_link_data $hrmJobLinkDatum)
    {
        $this->hrm_job_link_data[] = $hrmJobLinkDatum;

        return $this;
    }

    /**
     * Remove hrmJobLinkDatum
     *
     * @param \AdminBundle\Entity\hrm_job_link_data $hrmJobLinkDatum
     */
    public function removeHrmJobLinkDatum(\AdminBundle\Entity\hrm_job_link_data $hrmJobLinkDatum)
    {
        $this->hrm_job_link_data->removeElement($hrmJobLinkDatum);
    }

    /**
     * Get hrmJobLinkData
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getHrmJobLinkData()
    {
        return $this->hrm_job_link_data;
    }
}
