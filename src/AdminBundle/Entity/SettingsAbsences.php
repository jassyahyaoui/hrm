<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SettingsAbsence
 *
 * @ORM\Table(name="hrm_settings_absence_type")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\SettingsAbsencesRepository")
 */
class SettingsAbsences
{
   /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="seqno", type="integer",nullable=true)
     */
    private $seqno;
    
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=100)
     */
    private $type;

    /**
     * @var bool
     *
     * @ORM\Column(name="negative_balance", type="boolean")
     */
    private $NegativeBalance;

    /**
     * @var int
     *
     * @ORM\Column(name="number_of_days", type="integer")
     */
    private $numberOfDays;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    private $description;
    
   /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsProvider")
    * @ORM\JoinColumn(name="provider_id", referencedColumnName="uid")
    */
    private $provider;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $create_date;
    
    /**
     * @var int
     *
     * @ORM\Column(name="create_uid", type="integer", nullable=true)
     */
    private $createUid;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $last_update;
    

    /**
     * @var int
     *
     * @ORM\Column(name="last_update_uid", type="integer", nullable=true)
     */
    private $last_update_uid;   


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set seqno
     *
     * @param integer $seqno
     *
     * @return SettingsAbsences
     */
    public function setSeqno($seqno)
    {
        $this->seqno = $seqno;

        return $this;
    }

    /**
     * Get seqno
     *
     * @return integer
     */
    public function getSeqno()
    {
        return $this->seqno;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return SettingsAbsences
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set negativeBalance
     *
     * @param boolean $negativeBalance
     *
     * @return SettingsAbsences
     */
    public function setNegativeBalance($negativeBalance)
    {
        $this->NegativeBalance = $negativeBalance;

        return $this;
    }

    /**
     * Get negativeBalance
     *
     * @return boolean
     */
    public function getNegativeBalance()
    {
        return $this->NegativeBalance;
    }

    /**
     * Set numberOfDays
     *
     * @param integer $numberOfDays
     *
     * @return SettingsAbsences
     */
    public function setNumberOfDays($numberOfDays)
    {
        $this->numberOfDays = $numberOfDays;

        return $this;
    }

    /**
     * Get numberOfDays
     *
     * @return integer
     */
    public function getNumberOfDays()
    {
        return $this->numberOfDays;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return SettingsAbsences
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }



    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return SettingsAbsences
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return SettingsAbsences
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return SettingsAbsences
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return SettingsAbsences
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->last_update_uid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->last_update_uid;
    }
    public function __toString() {
        return $this->getType();
    }

    /**
     * Set provider
     *
     * @param \AdminBundle\Entity\SettingsProvider $provider
     *
     * @return SettingsAbsences
     */
    public function setProvider(\AdminBundle\Entity\SettingsProvider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \AdminBundle\Entity\SettingsProvider
     */
    public function getProvider()
    {
        return $this->provider;
    }
}
