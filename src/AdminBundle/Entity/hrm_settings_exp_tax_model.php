<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection,
    Doctrine\Common\Collections\ArrayCollection;

/**
 * hrm_settings_exp_tax_model
 *
 * @ORM\Table(name="hrm_settings_exp_tax_model")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\hrm_settings_exp_tax_modelRepository")
 */
class hrm_settings_exp_tax_model
{
    /**
     * @var int
     *
     * @ORM\Column(name="uid", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToMany(targetEntity="hrm_settings_tax", inversedBy="model_tax")
     * @ORM\JoinTable(name="hrm_settings_model_tax_groups",
     * joinColumns={
     *      @ORM\JoinColumn(name="model_tax_id", referencedColumnName="uid")
     *  },
     *  inverseJoinColumns={
     *      @ORM\JoinColumn(name="tax_id", referencedColumnName="uid")
     *  })
     */    
    private $tax;
    
   /**
    * @ORM\ManyToOne(targetEntity="AdminBundle\Entity\SettingsProvider")
    * @ORM\JoinColumn(name="provider_id", referencedColumnName="uid")
    */
    private $provider;
    
    /**
     * @var string
     *
     * @ORM\Column(name="display_name", type="string", length=100)
     */
    private $display_name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text",nullable=true)
     */
    private $description;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="create_date", type="datetime")
     */
    private $create_date;
    
    /**
     * @var int
     *
     * @ORM\Column(name="create_uid", type="integer", nullable=true)
     */
    
    private $createUid;
    
    /**
     * @var DateTime
     *
     * @ORM\Column(name="last_update", type="datetime", nullable=true)
     */
    private $last_update;
    

    /**
     * @var int
     *
     * @ORM\Column(name="last_update_uid", type="integer", nullable=true)
     */
    
    private $last_update_uid;    

    public function __construct() {
        $this->tax = new ArrayCollection();
    } 

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set displayName
     *
     * @param string $displayName
     *
     * @return hrm_settings_exp_tax_model
     */
    public function setDisplayName($displayName)
    {
        $this->display_name = $displayName;

        return $this;
    }

    /**
     * Get displayName
     *
     * @return string
     */
    public function getDisplayName()
    {
        return $this->display_name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return hrm_settings_exp_tax_model
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_settings_exp_tax_model
     */
    public function setCreateDate($createDate)
    {
        $this->create_date = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->create_date;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_settings_exp_tax_model
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_settings_exp_tax_model
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->last_update = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->last_update;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_settings_exp_tax_model
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->last_update_uid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->last_update_uid;
    }

    /**
     * Add tax
     *
     * @param \AdminBundle\Entity\hrm_settings_tax $tax
     *
     * @return hrm_settings_exp_tax_model
     */
    public function addTax(\AdminBundle\Entity\hrm_settings_tax $tax)
    {
        $this->tax[] = $tax;

        return $this;
    }

    /**
     * Remove tax
     *
     * @param \AdminBundle\Entity\hrm_settings_tax $tax
     */
    public function removeTax(\AdminBundle\Entity\hrm_settings_tax $tax)
    {
        $this->tax->removeElement($tax);
    }

    /**
     * Get tax
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set provider
     *
     * @param \AdminBundle\Entity\SettingsProvider $provider
     *
     * @return hrm_settings_exp_tax_model
     */
    public function setProvider(\AdminBundle\Entity\SettingsProvider $provider = null)
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * Get provider
     *
     * @return \AdminBundle\Entity\SettingsProvider
     */
    public function getProvider()
    {
        return $this->provider;
    }
}
