<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsStudyType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\SettingsStudyTypeType;

/**
 * Settingsstudytype controller.
 *
 */
class SettingsStudyTypeController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/settingsstudytype/layout.html.twig');
    }

    /**
     * Lists all settingsstudytype entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/settingsstudytype/index.html.twig');
    }

    public function listAction() {
                $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsStudyTypes = $this->getDoctrine()->getRepository('AdminBundle:SettingsStudyType')->SettingsStudyTypeByProvider($provider_id);
        return new JsonResponse(array(
            'settingsStudyTypes' => $settingsStudyTypes,
        ));
    }

    /**
     * Creates a new settingsstudytype entity.
     *
     */
    public function newAction(Request $request) {
        $settingsStudyType = new SettingsStudyType();
        $form = $this->createForm('AdminBundle\Form\SettingsStudyTypeType', $settingsStudyType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsStudyType);
            $em->flush($settingsStudyType);

            return $this->redirectToRoute('adminSettingsStudyType_show', array('id' => $settingsStudyType->getId()));
        }

        return $this->render('Admin/settingsstudytype/new.html.twig', array(
                    'settingsstudytype' => $settingsStudyType,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $settingsStudyType = new SettingsStudyType();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsStudyTypeType(), $settingsStudyType);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsStudyType->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsStudyType->setCreateDate(new \DateTime('now'));
        $settingsStudyType->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsStudyType->setLastUpdate(new \DateTime('now'));
        $settingsStudyType->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());        
        $em->persist($settingsStudyType);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing settingsstudytype entity.
     *
     */
    public function editAction(Request $request) {
        $settingsStudyType = new SettingsStudyType();
        $form = $this->createForm('AdminBundle\Form\SettingsStudyTypeType', $settingsStudyType);

        return $this->render('Admin/settingsstudytype/edit.html.twig', array(
                    'settingsStudyType' => $settingsStudyType,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retSettingsStudyType = $em->getRepository('AdminBundle:SettingsStudyType')->find($json_data ['id']);
        $form = $this->createForm(new SettingsStudyTypeType(), $retSettingsStudyType);
        $form->submit($json_data);
        $retSettingsStudyType->setLastUpdate(new \DateTime('now'));
        $retSettingsStudyType->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsStudyTypeAction(Request $request, SettingsStudyType $settingsStudyType) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsStudyType');
        $settingsStudyTypes = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $settingsStudyType->getId())
                ->getQuery()
                ->getArrayResult();

        if ($settingsStudyTypes)
            return new JsonResponse($settingsStudyTypes[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a settingsstudytype entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $settingsStudyType = $this->getDoctrine()->getRepository('AdminBundle:SettingsStudyType')->find($id);
        if ($settingsStudyType) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsStudyType);
            $em->flush($settingsStudyType);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }
    public function seqnoUpdateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
//        dump($json_data[0]);
//        die();
        foreach ($json_data as $key => $value) {
           
                $settingsStudyType = $em->getRepository('AdminBundle:SettingsStudyType')->find($json_data[$key]['id']);
                $settingsStudyType->setSeqno($key+1);
            
        }
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
}
