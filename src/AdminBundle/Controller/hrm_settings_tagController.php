<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_tag;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Hrm_settings_tag controller.
 *
 */
class hrm_settings_tagController extends Controller
{
    
      public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $tags = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_tag')->SettingsTagyByProvider($provider_id);
        return new JsonResponse(array(
            'tags' => $tags,
        ));
    }
    
    /**
     * Lists all hrm_settings_tag entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hrm_settings_tags = $em->getRepository('AdminBundle:hrm_settings_tag')->findAll();

        return $this->render('hrm_settings_tag/index.html.twig', array(
            'hrm_settings_tags' => $hrm_settings_tags,
        ));
    }

    /**
     * Creates a new hrm_settings_tag entity.
     *
     */
    public function newAction(Request $request)
    {
        $hrm_settings_tag = new Hrm_settings_tag();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_tagType', $hrm_settings_tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($hrm_settings_tag);
            $em->flush();

            return $this->redirectToRoute('hrm_settings_tag_show', array('id' => $hrm_settings_tag->getId()));
        }

        return $this->render('hrm_settings_tag/new.html.twig', array(
            'hrm_settings_tag' => $hrm_settings_tag,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_settings_tag entity.
     *
     */
    public function showAction(hrm_settings_tag $hrm_settings_tag)
    {
        $deleteForm = $this->createDeleteForm($hrm_settings_tag);

        return $this->render('hrm_settings_tag/show.html.twig', array(
            'hrm_settings_tag' => $hrm_settings_tag,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_settings_tag entity.
     *
     */
    public function editAction(Request $request, hrm_settings_tag $hrm_settings_tag)
    {
        $deleteForm = $this->createDeleteForm($hrm_settings_tag);
        $editForm = $this->createForm('AdminBundle\Form\hrm_settings_tagType', $hrm_settings_tag);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('hrm_settings_tag_edit', array('id' => $hrm_settings_tag->getId()));
        }

        return $this->render('hrm_settings_tag/edit.html.twig', array(
            'hrm_settings_tag' => $hrm_settings_tag,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a hrm_settings_tag entity.
     *
     */
    public function deleteAction(Request $request, hrm_settings_tag $hrm_settings_tag)
    {
        $form = $this->createDeleteForm($hrm_settings_tag);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_settings_tag);
            $em->flush();
        }

        return $this->redirectToRoute('hrm_settings_tag_index');
    }

    /**
     * Creates a form to delete a hrm_settings_tag entity.
     *
     * @param hrm_settings_tag $hrm_settings_tag The hrm_settings_tag entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_settings_tag $hrm_settings_tag)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('hrm_settings_tag_delete', array('id' => $hrm_settings_tag->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
