<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_technology_level;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\hrm_settings_technology_levelType;

/**
 * Hrm_settings_technology_level controller.
 *
 */
class hrm_settings_technology_levelController extends Controller
{
    
    public function layoutAction() {
        return $this->render('Admin/hrm_settings_technology_level/layout.html.twig');
    }

    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsTechnologyLevel = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_technology_level')->SettingsTechnologyLevelByProvider($provider_id);
        return new JsonResponse(array(
            'settingsTechnologyLevel' => $settingsTechnologyLevel,
        ));
    }

    public function saveAction(Request $request) {
        $settingsTechnologyLevel = new Hrm_settings_technology_level();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_technology_levelType(), $settingsTechnologyLevel);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsTechnologyLevel->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsTechnologyLevel->setCreateDate(new \DateTime('now'));
        $settingsTechnologyLevel->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsTechnologyLevel->setLastUpdate(new \DateTime('now'));
        $settingsTechnologyLevel->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($settingsTechnologyLevel);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }
        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $settingsTechnologyLevel = $em->getRepository('AdminBundle:hrm_settings_technology_level')->find($data ['id']);
        $form = $this->createForm('AdminBundle\Form\hrm_settings_technology_levelType', $settingsTechnologyLevel);
        $form->submit($data);
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();
        $settingsTechnologyLevel->setCreateDate(new \DateTime('now'));
        $settingsTechnologyLevel->setCreateUid($current_user_id);
        $settingsTechnologyLevel->setLastUpdateUid($current_user_id);
        $settingsTechnologyLevel->setLastUpdate(new \DateTime('now'));
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    /**
     * Deletes a typetransport entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }
        $settingsTechnologyLevel = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_technology_level')->find($id);
        if ($settingsTechnologyLevel) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsTechnologyLevel);
            $em->flush($settingsTechnologyLevel);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

    public function seqnoUpdateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
        foreach ($json_data as $key => $value) {
            $settingsTechnologyLevel = $em->getRepository('AdminBundle:hrm_settings_technology_level')->find($json_data[$key]['id']);
            $settingsTechnologyLevel->setSeqno($key + 1);
        }
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    /**
     * Lists all hrm_settings_technology_level entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hrm_settings_technology_levels = $em->getRepository('AdminBundle:hrm_settings_technology_level')->findAll();

        return $this->render('hrm_settings_technology_level/index.html.twig', array(
            'hrm_settings_technology_levels' => $hrm_settings_technology_levels,
        ));
    }

    /**
     * Creates a new hrm_settings_technology_level entity.
     *
     */
    public function newAction()
    {
        $hrm_settings_technology_level = new Hrm_settings_technology_level();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_technology_levelType', $hrm_settings_technology_level);
        return $this->render('Admin\hrm_settings_technology_level/new.html.twig', array(
            'hrm_settings_technology_level' => $hrm_settings_technology_level,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_settings_technology_level entity.
     *
     */
    public function showAction(hrm_settings_technology_level $hrm_settings_technology_level)
    {
        $deleteForm = $this->createDeleteForm($hrm_settings_technology_level);

        return $this->render('hrm_settings_technology_level/show.html.twig', array(
            'hrm_settings_technology_level' => $hrm_settings_technology_level,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_settings_technology_level entity.
     *
     */
    public function editAction()
    {
        $hrm_settings_technology_level = new Hrm_settings_technology_level();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_technology_levelType', $hrm_settings_technology_level);
        return $this->render('Admin\hrm_settings_technology_level/edit.html.twig', array(
            'hrm_settings_technology_level' => $hrm_settings_technology_level,
            'form' => $form->createView(),
        ));
    }

   

    /**
     * Creates a form to delete a hrm_settings_technology_level entity.
     *
     * @param hrm_settings_technology_level $hrm_settings_technology_level The hrm_settings_technology_level entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_settings_technology_level $hrm_settings_technology_level)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('hrm_settings_technology_level_delete', array('id' => $hrm_settings_technology_level->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
