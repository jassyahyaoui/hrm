<?php


namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsTransports;
use AdminBundle\Form\SettingsTransportsType;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Typetransport controller.
 *
 */
class SettingsTransportsController extends Controller
{

    public function layoutAction()
    {
        return $this->render('Admin/settingstransports/layout.html.twig');
    }

    /**
     * Lists all typetransport entities.
     *
     */
    public function indexAction()
    {
        return $this->render('Admin/settingstransports/index.html.twig');
    }

    public function listAction()
    {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $typetransports = $this->getDoctrine()->getRepository('AdminBundle:SettingsTransports')->SettingsTransportsByProvider($provider_id);
        return new JsonResponse(array(
            'typetransports' => $typetransports,
        ));
    }

    /**
     * Creates a new typetransport entity.
     *
     */
    public function newAction(Request $request)
    {
        
        $typetransport = new SettingsTransports();
        $form = $this->createForm('AdminBundle\Form\SettingsTransportsType', $typetransport);
        $form->handleRequest($request);

        return $this->render('Admin/settingstransports/new.html.twig', array(
            'typetransport' => $typetransport,
            'form' => $form->createView(),
        ));           
        
        
//        $typetransport = new SettingsTransports();
//        $form = $this->createForm('AdminBundle\Form\SettingsTransportsType', $typetransport);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->persist($typetransport);
//            $em->flush($typetransport);
//
//            return $this->redirectToRoute('adminTypeTransports_show', array('id' => $typetransport->getId()));
//        }
//
//        return $this->render('Admin/settingstransports/new.html.twig', array(
//            'typetransport' => $typetransport,
//            'form' => $form->createView(),
//        ));
    }

    public function saveAction(Request $request)
    {
        $typetransport = new SettingsTransports();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsTransportsType(), $typetransport);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $typetransport->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $typetransport->setCreateDate(new \DateTime('now'));
        $typetransport->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $typetransport->setLastUpdate(new \DateTime('now'));
        $typetransport->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($typetransport);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing typetransport entity.
     *
     */
    public function editAction(Request $request)
    {
        $typetransport = new SettingsTransports();
        $form = $this->createForm('AdminBundle\Form\SettingsTransportsType', $typetransport);
        $form->handleRequest($request);

        return $this->render('Admin/settingstransports/edit.html.twig', array(
            'typetransport' => $typetransport,
            'form' => $form->createView(),
        ));
//
//        
//        
//        $_Newtypetransport = new SettingsTransports();
//        $form = $this->createForm('AdminBundle\Form\SettingsTransportsType', $_Newtypetransport);
//
//        return $this->render('Admin/settingstransports/edit.html.twig', array(
//            'typetransport' => $typetransport,
//            'form' => $form->createView(),
//        ));
    }

    public function updateAction(Request $request)
    {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retTypeTransport = $em->getRepository('AdminBundle:SettingsTransports')->find($data ['id']);
        $form = $this->createForm('AdminBundle\Form\SettingsTransportsType', $retTypeTransport);
        $form->submit($data);

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();

        //Add default value on create Action
        $retTypeTransport->setCreateDate(new \DateTime('now'));
        $retTypeTransport->setCreateUid($current_user_id);
        $retTypeTransport->setLastUpdateUid($current_user_id);
        $retTypeTransport->setLastUpdate(new \DateTime('now'));

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));        

        
        
        
//        
//        if ($this->get('request')->getMethod() != 'PUT') {
//            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
//        }
//
//        $json_data = json_decode($this->get('request')->getContent(), true);
//
//        $em = $this->getDoctrine()->getEntityManager();
//        $retTypeTransport = $em->getRepository('AdminBundle:SettingsTransports')->find($json_data ['id']);
//        $form = $this->createForm(new SettingsTransportsType(), $retTypeTransport);
//        $form->submit($json_data);
//        $retTypeTransport->setLastUpdate(new \DateTime('now'));
//        $retTypeTransport->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
//        $this->getDoctrine()->getManager()->flush();
//        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsTransportsAction(Request $request, SettingsTransports $typetransport)
    {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsTransports');
        $typetransports = $repo->createQueryBuilder('r')
            ->where('r.id = :id')
            ->setParameter('id', $typetransport->getId())
            ->getQuery()
            ->getArrayResult();

        if ($typetransports)
            return new JsonResponse($typetransports[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a typetransport entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $typetransport = $this->getDoctrine()->getRepository('AdminBundle:SettingsTransports')->find($id);
        if ($typetransport) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typetransport);
            $em->flush($typetransport);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

    public function seqnoUpdateAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);

        foreach ($json_data as $key => $value) {
            $typetransport = $em->getRepository('AdminBundle:SettingsTransports')->find($json_data[$key]['id']);
            $typetransport->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
}
