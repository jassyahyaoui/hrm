<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsPayment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\SettingsPaymentType;

/**
 * SettingsPayment controller.
 *
 */
class SettingsPaymentController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/settingspayment/layout.html.twig');
    }

    /**
     * Lists all SettingsPayment entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/settingspayment/index.html.twig');
    }

    public function listAction() {
                       $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $paymentmethod = $this->getDoctrine()->getRepository('AdminBundle:SettingsPayment')->SettingsPaymentByProvider($provider_id);
        return new JsonResponse(array(
            'paymentmethods' => $paymentmethod,
        ));
    }

    /**
     * Creates a new paymentmethod entity.
     *
     */
    public function newAction(Request $request) {
        $paymentmethod = new SettingsPayment();
        $form = $this->createForm('AdminBundle\Form\SettingsPaymentType', $paymentmethod);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($paymentmethod);
            $em->flush($paymentmethod);

            return $this->redirectToRoute('adminPaymentMethod_show', array('id' => $paymentmethod->getId()));
        }

        return $this->render('Admin/settingspayment/new.html.twig', array(
                    'paymentmethod' => $paymentmethod,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $paymentmethod = new SettingsPayment();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $form = $this->createForm('AdminBundle\Form\SettingsPaymentType', $paymentmethod);
        $form->handleRequest($request);
        $body = $request->getContent();
        $json_data = json_decode($body, true);
        $form->submit($json_data);
        $em = $this->getDoctrine()->getManager();
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $paymentmethod->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $paymentmethod->setCreateDate(new \DateTime('now'));
        $paymentmethod->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $paymentmethod->setLastUpdate(new \DateTime('now'));
        $paymentmethod->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());   
//        if (isset($json_data['seqno']))
//        $paymentmethod->setSeqno($json_data['seqno']);
        $em->persist($paymentmethod);
        $em->flush($paymentmethod);
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing paymentmethod entity.
     *
     */
    public function editAction(Request $request) {
        $paymentmethod = new SettingsPayment();
        $form = $this->createForm('AdminBundle\Form\SettingsPaymentType', $paymentmethod);

        return $this->render('Admin/settingspayment/edit.html.twig', array(
                    'paymentmethod' => $paymentmethod,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {

        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retPaymentMethod = $em->getRepository('AdminBundle:SettingsPayment')->find($json_data ['id']);
        $form = $this->createForm(new SettingsPaymentType(), $retPaymentMethod);
        $form->submit($json_data);
//        $retPaymentMethod->setSeqno($json_data['seqno']);
        $retPaymentMethod->setLastUpdate(new \DateTime('now'));
        $retPaymentMethod->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsPaymentAction(Request $request, SettingsPayment $paymentmethod) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsPayment');
        $paymentmethod = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $paymentmethod->getId())
                ->getQuery()
                ->getArrayResult();

        if ($paymentmethod)
            return new JsonResponse($paymentmethod[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a paymentmethod entity.
     *
     */
    public function deleteAction(Request $request, $id) {

        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $paymentmethod = $this->getDoctrine()->getRepository('AdminBundle:SettingsPayment')->find($id);
        if ($paymentmethod) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($paymentmethod);
            $em->flush($paymentmethod);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

    public function seqnoUpdateAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
        
        foreach ($json_data as $key => $value) {
            $retPaymentMethod = $em->getRepository('AdminBundle:SettingsPayment')->find($json_data[$key]['id']);
            $retPaymentMethod->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }    
    
    
}
