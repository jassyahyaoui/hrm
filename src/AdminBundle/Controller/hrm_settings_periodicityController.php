<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_periodicity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\hrm_settings_periodicityType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

/**
 * Hrm_settings_periodicity controller.
 *
 */
class hrm_settings_periodicityController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/hrm_settings_periodicity/layout.html.twig');
    }

    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsPeriodicity = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_periodicity')->SettingsPeriodicityByProvider($provider_id);
        return new JsonResponse(array(
            'settingsPeriodicity' => $settingsPeriodicity,
        ));
    }

    public function saveAction(Request $request) {
        $settingsPeriodicity = new Hrm_settings_periodicity();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_periodicityType(), $settingsPeriodicity);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsPeriodicity->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsPeriodicity->setCreateDate(new \DateTime('now'));
        $settingsPeriodicity->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsPeriodicity->setLastUpdate(new \DateTime('now'));
        $settingsPeriodicity->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($settingsPeriodicity);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }
        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $settingsPeriodicity = $em->getRepository('AdminBundle:hrm_settings_periodicity')->find($data ['id']);
        $form = $this->createForm('AdminBundle\Form\hrm_settings_periodicityType', $settingsPeriodicity);
        $form->submit($data);
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();
        $settingsPeriodicity->setCreateDate(new \DateTime('now'));
        $settingsPeriodicity->setCreateUid($current_user_id);
        $settingsPeriodicity->setLastUpdateUid($current_user_id);
        $settingsPeriodicity->setLastUpdate(new \DateTime('now'));
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function seqnoUpdateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
        foreach ($json_data as $key => $value) {
            $settingsPeriodicity = $em->getRepository('AdminBundle:hrm_settings_periodicity')->find($json_data[$key]['id']);
            $settingsPeriodicity->setSeqno($key + 1);
        }
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    /**
     * Deletes a typetransport entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }
        $settingsPeriodicity = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_periodicity')->find($id);
        if ($settingsPeriodicity) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsPeriodicity);
            $em->flush($settingsPeriodicity);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }
        return new JsonResponse(null);
    }

    /**
     * Lists all hrm_settings_periodicity entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();
        $hrm_settings_periodicities = $em->getRepository('AdminBundle:hrm_settings_periodicity')->findAll();
        return $this->render('hrm_settings_periodicity/index.html.twig', array(
                    'hrm_settings_periodicities' => $hrm_settings_periodicities,
        ));
    }

    /**
     * Creates a new hrm_settings_periodicity entity.
     *
     */
    public function newAction() {
        $hrm_settings_periodicity = new Hrm_settings_periodicity();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_periodicityType', $hrm_settings_periodicity);
        return $this->render('Admin\hrm_settings_periodicity/new.html.twig', array(
                    'hrm_settings_periodicity' => $hrm_settings_periodicity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_settings_periodicity entity.
     *
     */
    public function showAction(hrm_settings_periodicity $hrm_settings_periodicity) {
        $deleteForm = $this->createDeleteForm($hrm_settings_periodicity);
        return $this->render('Admin\hrm_settings_periodicity/show.html.twig', array(
                    'hrm_settings_periodicity' => $hrm_settings_periodicity,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_settings_periodicity entity.
     *
     */
    public function editAction() {
        $hrm_settings_periodicity = new Hrm_settings_periodicity();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_periodicityType', $hrm_settings_periodicity);
        return $this->render('Admin\hrm_settings_periodicity/edit.html.twig', array(
                    'hrm_settings_periodicity' => $hrm_settings_periodicity,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to delete a hrm_settings_periodicity entity.
     *
     * @param hrm_settings_periodicity $hrm_settings_periodicity The hrm_settings_periodicity entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_settings_periodicity $hrm_settings_periodicity) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('hrm_settings_periodicity_delete', array('id' => $hrm_settings_periodicity->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
