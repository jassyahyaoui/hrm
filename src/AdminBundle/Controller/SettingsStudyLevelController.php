<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsStudyLevel;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\SettingsStudyLevelType;

/**
 * Settingsstudylevel controller.
 *
 */
class SettingsStudyLevelController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/settingsstudylevel/layout.html.twig');
    }

    /**
     * Lists all settingsstudylevel entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/settingsstudylevel/index.html.twig');
    }

    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
//        $provider_id =  $this->container->get('settingsbundle.preference.service')->getProviderId();
       //var_dump($provider_id);die();
        $settingsStudyLevels = $this->getDoctrine()->getRepository('AdminBundle:SettingsStudyLevel')->SettingsStudyLevelByProvider($provider_id);
        return new JsonResponse(array(
            'settingsStudyLevels' => $settingsStudyLevels,
        ));
    }

    /**
     * Creates a new settingsstudylevel entity.
     *
     */
    public function newAction(Request $request) {
        $settingsStudyLevel = new SettingsStudyLevel();
        $form = $this->createForm('AdminBundle\Form\SettingsStudyLevelType', $settingsStudyLevel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsStudyLevel);
            $em->flush($settingsStudyLevel);

            return $this->redirectToRoute('adminSettingsStudyLevel_show', array('id' => $settingsStudyLevel->getId()));
        }

        return $this->render('Admin/settingsstudylevel/new.html.twig', array(
                    'settingsstudylevel' => $settingsStudyLevel,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $settingsStudyLevel = new SettingsStudyLevel();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsStudyLevelType(), $settingsStudyLevel);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsStudyLevel->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsStudyLevel->setCreateDate(new \DateTime('now'));
        $settingsStudyLevel->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsStudyLevel->setLastUpdate(new \DateTime('now'));
        $settingsStudyLevel->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());        
        $em->persist($settingsStudyLevel);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing settingsstudylevel entity.
     *
     */
    public function editAction(Request $request) {
        $settingsStudyLevel = new SettingsStudyLevel();
        $form = $this->createForm('AdminBundle\Form\SettingsStudyLevelType', $settingsStudyLevel);

        return $this->render('Admin/settingsstudylevel/edit.html.twig', array(
                    'settingsStudyLevel' => $settingsStudyLevel,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retSettingsStudyLevel = $em->getRepository('AdminBundle:SettingsStudyLevel')->find($json_data ['id']);
        $form = $this->createForm(new SettingsStudyLevelType(), $retSettingsStudyLevel);
        $form->submit($json_data);
        $retSettingsStudyLevel->setLastUpdate(new \DateTime('now'));
        $retSettingsStudyLevel->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsStudyLevelAction(Request $request, SettingsStudyLevel $settingsStudyLevel) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsStudyLevel');
        $settingsStudyLevels = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $settingsStudyLevel->getId())
                ->getQuery()
                ->getArrayResult();

        if ($settingsStudyLevels)
            return new JsonResponse($settingsStudyLevels[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a settingsstudylevel entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $settingsStudyLevel = $this->getDoctrine()->getRepository('AdminBundle:SettingsStudyLevel')->find($id);
        if ($settingsStudyLevel) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsStudyLevel);
            $em->flush($settingsStudyLevel);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }
    public function seqnoUpdateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
//        dump($json_data[0]);
//        die();
        foreach ($json_data as $key => $value) {
           
                $settingsStudyLevel = $em->getRepository('AdminBundle:SettingsStudyLevel')->find($json_data[$key]['id']);
                $settingsStudyLevel->setSeqno($key+1);
            
        }
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
}
