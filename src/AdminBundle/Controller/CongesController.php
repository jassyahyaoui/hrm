<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\Conges;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\CongesType;

/**
 * Conge controller.
 *
 */
class CongesController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/conges/layout.html.twig');
    }

    public function indexAction() {
        return $this->render('Admin/conges/index.html.twig');
    }

    // for angurlajs Rest
    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $conges = $this->getDoctrine()->getRepository('AdminBundle:Conges')->CongesByProvider($provider_id);
        return new JsonResponse(array(
            'conges' => $conges,
        ));
    }

    /**
     * Creates a new conge entity.
     *
     */
    public function newAction(Request $request) {
        $conges = new Conges();
        $form = $this->createForm('AdminBundle\Form\CongesType', $conges);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($conges);
            $em->flush($conges);

            return $this->redirectToRoute('adminConges_show', array('id' => $conges->getId()));
        }

        return $this->render('Admin/conges/new.html.twig', array(
                    'conges' => $conges,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $conges = new Conges();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new CongesType(), $conges);
        $form->handleRequest($request);
        $form->submit($json_data);

        $conges->setStartDate(new \DateTime($json_data ['start_date']));
        $conges->setEndDate(new \DateTime($json_data ['end_date']));
        $em->persist($conges);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing conge entity.
     *
     */
    public function editAction(Request $request, Conges $conges) {
        $_Newconges = new Conges();
        $form = $this->createForm('AdminBundle\Form\CongesType', $_Newconges);

        return $this->render('Admin/conges/edit.html.twig', array(
                    'conge' => $conges,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {

        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);

        $em = $this->getDoctrine()->getEntityManager();
        $retConges = $em->getRepository('AdminBundle:Conges')->find($json_data ['id']);
        $form = $this->createForm(new CongesType(), $retConges);
        $form->submit($json_data);
        $retConges->setStartDate(new \DateTime($json_data ['start_date']));
        $retConges->setEndDate(new \DateTime($json_data ['end_date']));
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getCongesAction(Request $request, $id) {
        $conges = $this->getDoctrine()->getRepository('AdminBundle:Conges')->find($id);

        if ($conges) {
            return new JsonResponse(array("id" => $conges->getId(),
                "start_date" => $conges->getStartDate()->format('Y-m-d'),
                "end_date" => $conges->getEndDate()->format('Y-m-d'),
                "note" => $conges->getNote(),
                "type" => (string) $conges->getType()->getId(),
            ));
        }

        return new JsonResponse($null);
    }

    /**
     * Deletes a conge entity.
     *
     */
    public function deleteAction(Request $request, $id) {

        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $conges = $this->getDoctrine()->getRepository('AdminBundle:Conges')->find($id);
        if ($conges) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($conges);
            $em->flush($conges);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

}
