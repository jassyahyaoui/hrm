<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_industrysector;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\hrm_settings_industrysectorType;
/**
 * Hrm_settings_industrysector controller.
 *
 */
class hrm_settings_industrysectorController extends Controller
{
    
    
        public function layoutAction() {
        return $this->render('Admin/hrm_settings_industrysector/layout.html.twig');
    }

    // for angurlajs Rest
    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsIndustrySectorsList = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_industrysector')->SettingsIndustrySectorByProvider($provider_id);
        return new JsonResponse(array(
            'settingsIndustrySectorsList' => $settingsIndustrySectorsList,
        ));
    }
    
     public function saveAction(Request $request) {
        $settingsIndustrySector = new Hrm_settings_industrysector();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_industrysectorType(), $settingsIndustrySector);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsIndustrySector->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsIndustrySector->setCreateDate(new \DateTime('now'));
        $settingsIndustrySector->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsIndustrySector->setLastUpdate(new \DateTime('now'));
        $settingsIndustrySector->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($settingsIndustrySector);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    
        public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }
        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $settingsIndustrySector = $em->getRepository('AdminBundle:hrm_settings_industrysector')->find($data ['id']);
        $form = $this->createForm('AdminBundle\Form\hrm_settings_industrysectorType', $settingsIndustrySector);
        $form->submit($data);
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();
        $settingsIndustrySector->setCreateDate(new \DateTime('now'));
        $settingsIndustrySector->setCreateUid($current_user_id);
        $settingsIndustrySector->setLastUpdateUid($current_user_id);
        $settingsIndustrySector->setLastUpdate(new \DateTime('now'));
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
    
    /**
     * Lists all hrm_settings_industrysector entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hrm_settings_industrysectors = $em->getRepository('AdminBundle:hrm_settings_industrysector')->findAll();

        return $this->render('admin/hrm_settings_industrysector/index.html.twig', array(
            'hrm_settings_industrysectors' => $hrm_settings_industrysectors,
        ));
    }

    /**
     * Creates a new hrm_settings_industrysector entity.
     *
     */
    public function newAction()
    {
        $hrm_settings_industrysector = new Hrm_settings_industrysector();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_industrysectorType', $hrm_settings_industrysector);
        return $this->render('admin/hrm_settings_industrysector/new.html.twig', array(
            'hrm_settings_industrysector' => $hrm_settings_industrysector,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_settings_industrysector entity.
     *
     */
    public function showAction(hrm_settings_industrysector $hrm_settings_industrysector)
    {
        $deleteForm = $this->createDeleteForm($hrm_settings_industrysector);

        return $this->render('admin/hrm_settings_industrysector/show.html.twig', array(
            'hrm_settings_industrysector' => $hrm_settings_industrysector,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_settings_industrysector entity.
     *
     */
    public function editAction()
    {
        $hrm_settings_industrysector = new Hrm_settings_industrysector();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_industrysectorType', $hrm_settings_industrysector);
        return $this->render('admin/hrm_settings_industrysector/edit.html.twig', array(
            'hrm_settings_industrysector' => $hrm_settings_industrysector,
            'form' => $form->createView(),
        ));
    }

   /**
     * Deletes a typetransport entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }
        $hrm_settings_industrysector = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_industrysector')->find($id);
        if ($hrm_settings_industrysector) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_settings_industrysector);
            $em->flush($hrm_settings_industrysector);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

     public function seqnoUpdateAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);

        foreach ($json_data as $key => $value) {
            $hrm_settings_industrysector = $em->getRepository('AdminBundle:hrm_settings_industrysector')->find($json_data[$key]['id']);
            $hrm_settings_industrysector->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
    
    /**
     * Creates a form to delete a hrm_settings_industrysector entity.
     *
     * @param hrm_settings_industrysector $hrm_settings_industrysector The hrm_settings_industrysector entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_settings_industrysector $hrm_settings_industrysector)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('hrm_settings_industrysector_delete', array('id' => $hrm_settings_industrysector->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
