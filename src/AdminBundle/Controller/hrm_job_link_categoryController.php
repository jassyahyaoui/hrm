<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_job_link_category;
use AdminBundle\Form\hrm_job_link_categoryType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Hrm_job_link_category controller.
 *
 */
class hrm_job_link_categoryController extends Controller
{
    public function layoutAction()
    {
        return $this->render('Admin/job_link/hrm_job_link_category/layout.html.twig');
    }
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hrm_job_link_categories = $em->getRepository('AdminBundle:hrm_job_link_category')->findAll();

        return $this->render('Admin/job_link/hrm_job_link_category/index.html.twig', array(
            'hrm_job_link_categories' => $hrm_job_link_categories,
        ));
    }

    /**
     * Creates a new hrm_job_link_category entity.
     *
     */
    public function newAction(Request $request)
    {
        $hrm_job_link_category = new Hrm_job_link_category();
        $form = $this->createForm('AdminBundle\Form\hrm_job_link_categoryType', $hrm_job_link_category);
        $form->handleRequest($request);

        return $this->render('Admin/job_link/hrm_job_link_category/new.html.twig', array(
            'hrm_job_link_category' => $hrm_job_link_category,
            'form' => $form->createView(),
        ));
    }
    public function editAction(Request $request)
    {
        $hrm_job_link_category = new Hrm_job_link_category();
        $form = $this->createForm('AdminBundle\Form\hrm_job_link_categoryType', $hrm_job_link_category);
        $form->handleRequest($request);

        return $this->render('Admin/job_link/hrm_job_link_category/edit.html.twig', array(
            'hrm_job_link_category' => $hrm_job_link_category,
            'form' => $form->createView(),
        ));
    }


    public function allAction()
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();
        $companyData = $empData->getCompany();
        $company_id = $companyData->getId();
        $providerId = $companyData->getProvider();

        $result = $em->getRepository('AdminBundle:hrm_job_link_category')->FindJobLinkCategory($providerId);
//$result = "vide";
        return new JsonResponse(array(
            'result' => $result,
        ));
//        return $result;

    }

    public function saveAction(Request $request)
    {
        $category = new hrm_job_link_category();
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm('AdminBundle\Form\hrm_job_link_categoryType', $category);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $empData->getCompany()->getProvider()->getId();
        $category->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $category->setCreateDate(new \DateTime('now'));
        $category->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $category->setLastUpdate(new \DateTime('now'));
        $category->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($category);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    public function updateAction(Request $request)
    {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $category = $em->getRepository('AdminBundle:hrm_job_link_category')->find($json_data ['id']);
        $form = $this->createForm('AdminBundle\Form\hrm_job_link_categoryType', $category);
        $form->submit($json_data);
        $category->setLastUpdate(new \DateTime('now'));
        $category->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $category = $this->getDoctrine()->getRepository('AdminBundle:hrm_job_link_category')->find($id);
        if ($category) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush($category);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }


    public function seqnoUpdateAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
        foreach ($json_data as $key => $value) {
            $SettingsBonus = $em->getRepository('AdminBundle:hrm_job_link_category')->find($json_data[$key]['id']);
            $SettingsBonus->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
}
