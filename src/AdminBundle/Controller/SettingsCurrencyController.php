<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsCurrency;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\SettingsCurrencyType;

/**
 * Settingscurrency controller.
 *
 */
class SettingsCurrencyController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/settingscurrency/layout.html.twig');
    }

    /**
     * Lists all settingscurrency entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/settingscurrency/index.html.twig');
    }

    // for angurlajs Rest
    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsCurrencies = $this->getDoctrine()->getRepository('AdminBundle:SettingsCurrency')->SettingsCurrencyByProvider($provider_id);
        return new JsonResponse(array(
            'settingsCurrencies' => $settingsCurrencies,
        ));
    }

    /**
     * Creates a new settingscurrency entity.
     *
     */
    public function newAction(Request $request) {
        $settingsCurrency = new SettingsCurrency();
        $form = $this->createForm('AdminBundle\Form\SettingsCurrencyType', $settingsCurrency);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsCurrency);
            $em->flush($settingsCurrency);

            return $this->redirectToRoute('adminSettingsCurrency_show', array('id' => $settingsCurrency->getId()));
        }

        return $this->render('Admin/settingscurrency/new.html.twig', array(
                    'settingsCurrency' => $settingsCurrency,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $settingsCurrency = new SettingsCurrency();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsCurrencyType(), $settingsCurrency);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsCurrency->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsCurrency->setCreateDate(new \DateTime('now'));
        $settingsCurrency->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsCurrency->setLastUpdate(new \DateTime('now'));
        $settingsCurrency->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());        
        $em->persist($settingsCurrency);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing settingscurrency entity.
     *
     */
    public function editAction(Request $request, SettingsCurrency $settingsCurrency) {
        $_Newsettingscurrency = new SettingsCurrency();
        $form = $this->createForm('AdminBundle\Form\SettingsCurrencyType', $_Newsettingscurrency);

        return $this->render('Admin/settingscurrency/edit.html.twig', array(
                    'settingsCurrency' => $settingsCurrency,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retSettingsCurrency = $em->getRepository('AdminBundle:SettingsCurrency')->find($json_data ['id']);
        $form = $this->createForm(new SettingsCurrencyType(), $retSettingsCurrency);
        $form->submit($json_data);
        $retSettingsCurrency->setLastUpdate(new \DateTime('now'));
        $retSettingsCurrency->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsCurrencyAction(Request $request, SettingsCurrency $settingsCurrency) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsCurrency');
        $settingsCurrencies = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $settingsCurrency->getId())
                ->getQuery()
                ->getArrayResult();

        if ($settingsCurrencies)
            return new JsonResponse($settingsCurrencies[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a settingscurrency entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $settingsCurrency = $this->getDoctrine()->getRepository('AdminBundle:SettingsCurrency')->find($id);
        if ($settingsCurrency) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsCurrency);
            $em->flush($settingsCurrency);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

}
