<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsStatus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\SettingsStatusType;

/**
 * SettingsStatus controller.
 *
 */
class SettingsStatusController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/settingsstatus/layout.html.twig');
    }

    /**
     * Lists all status entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/settingsstatus/index.html.twig');
    }

    public function listAction() {
                $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $status = $this->getDoctrine()->getRepository('AdminBundle:SettingsStatus')->SettingsStatusByProvider($provider_id);
        return new JsonResponse(array(
            'status' => $status,
        ));
    }

    /**
     * Creates a new status entity.
     *
     */
    public function newAction(Request $request) {
        $status = new SettingsStatus();
        $form = $this->createForm('AdminBundle\Form\SettingsStatusType', $status);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($status);
            $em->flush($status);

            return $this->redirectToRoute('adminStatus_show', array('id' => $status->getId()));
        }

        return $this->render('Admin/settingsstatus/new.html.twig', array(
                    'status' => $status,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $status = new SettingsStatus();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsStatusType(), $status);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $status->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $status->setCreateDate(new \DateTime('now'));
        $status->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $status->setLastUpdate(new \DateTime('now'));
        $status->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());        
        $em->persist($status);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing status entity.
     *
     */
    public function editAction(Request $request) {
        $status = new SettingsStatus();
        $form = $this->createForm('AdminBundle\Form\SettingsStatusType', $status);

        return $this->render('Admin/settingsstatus/edit.html.twig', array(
                    'status' => $status,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {

        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retStatus = $em->getRepository('AdminBundle:SettingsStatus')->find($json_data ['id']);
        $form = $this->createForm(new SettingsStatusType(), $retStatus);
        $form->submit($json_data);
        $retStatus->setLastUpdate(new \DateTime('now'));
        $retStatus->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsStatusAction(Request $request, SettingsStatus $status) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsStatus');
        $status = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $status->getId())
                ->getQuery()
                ->getArrayResult();

        if ($status)
            return new JsonResponse($status[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a status entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $status = $this->getDoctrine()->getRepository('AdminBundle:SettingsStatus')->find($id);
        if ($status) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($status);
            $em->flush($status);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

}
