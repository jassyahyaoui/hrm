<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\TypeConges;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\TypeCongesType;

/**
 * Typeconge controller.
 *
 */
class TypeCongesController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/typeconges/layout.html.twig');
    }

    /**
     * Lists all typeConge entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/typeconges/index.html.twig');
    }

    // for angurlajs Rest
    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $typeconges= $this->getDoctrine()->getRepository('AdminBundle:TypeConges')->TypeCongesByProvider($provider_id);
        return new JsonResponse(array(
            'typeconges' => $typeconges,
        ));
    }

    /**
     * Creates a new typeConge entity.
     *
     */
    public function newAction(Request $request) {
        $typeConge = new Typeconges();
        $form = $this->createForm('AdminBundle\Form\TypeCongesType', $typeConge);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typeConge);
            $em->flush($typeConge);

            return $this->redirectToRoute('adminTypeConges_show', array('id' => $typeConge->getId()));
        }

        return $this->render('Admin/typeconges/new.html.twig', array(
                    'typeConge' => $typeConge,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $typeconges = new TypeConges();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $typeconges->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $form = $this->createForm(new TypeCongesType(), $typeconges);
        $form->handleRequest($request);
        $form->submit($json_data);
        $em->persist($typeconges);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    public function getTypeCongesAction(Request $request, $id) {
        $typesconge = $this->getDoctrine()->getRepository('AdminBundle:TypeConges')->find($id);

        if ($typesconge) {
            return new JsonResponse(array("id" => $typesconge->getId(),
                "type" => $typesconge->getType(),
                "soldeNegative" => $typesconge->getSoldeNegative(),
                "nbJours" => $typesconge->getNbJours()));
        }

        return new JsonResponse($null);
    }

    /**
     * Displays a form to edit an existing typeConge entity.
     *
     */
    public function editAction(Request $request) {
        $typeConge = new TypeConges();
        $form = $this->createForm('AdminBundle\Form\TypeCongesType', $typeConge);

        return $this->render('Admin/typeconges/edit.html.twig', array(
                    'typeconges' => $typeConge,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retTypeConges = $em->getRepository('AdminBundle:TypeConges')->find($json_data['id']);
        $form = $this->createForm(new TypeCongesType(), $retTypeConges);
        $form->submit($json_data);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    /**
     * Deletes a typeConge entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $typeconges = $this->getDoctrine()->getRepository('AdminBundle:TypeConges')->find($id);
        if ($typeconges) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typeconges);
            $em->flush($typeconges);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

}
