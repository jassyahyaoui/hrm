<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class expense_settings_typesController extends Controller
{
    public function layoutAction()
    {
        return $this->render('Admin/expense_settings_types/expense_layout.html.twig');
    }

    public function settingsExpenseListAction()
    {

        $securityContext = $this->container->get('security.authorization_checker');
        if ($securityContext->isGranted('ROLE_ADMIN')) {
            $provider = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();

//            $ik = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_ik')->settings_ik_list($provider);
            $transport = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_transport')->settings_transport_list($provider);
            $accommodation = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_accommodation')->settings_accommodation_list($provider);
            $other = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_other')->settings_other_list($provider);
            $dinning = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_dinning')->settings_dinning_list($provider);
            $result = array_merge($transport, $accommodation, $other, $dinning);
            return array('result' => $result);
        } else {
            return new Response('Role non autorisé');
        }

    }

    public function modelTaxListAction()
    {
        $provider = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $models = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_tax_model')->hrm_settings_exp_tax_modelByProvider($provider);
        return array('models' => $models);
    }
}
