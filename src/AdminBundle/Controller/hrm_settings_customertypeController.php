<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_customertype;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\hrm_settings_customertypeType;

/**
 * Hrm_settings_customertype controller.
 *
 */
class hrm_settings_customertypeController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/hrm_settings_customertype/layout.html.twig');
    }

    // for angurlajs Rest
    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsCustomerType = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_customertype')->SettingsCustomertypeByProvider($provider_id);
        return new JsonResponse(array(
            'settingsCustomerType' => $settingsCustomerType,
        ));
    }

    /**
     * Lists all hrm_settings_customertype entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $hrm_settings_customertypes = $em->getRepository('AdminBundle:hrm_settings_customertype')->findAll();

        return $this->render('admin/hrm_settings_customertype/index.html.twig', array(
                    'hrm_settings_customertypes' => $hrm_settings_customertypes,
        ));
    }

    
    public function saveAction(Request $request) {
        $hrm_settings_customertype = new Hrm_settings_customertype();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_customertypeType(), $hrm_settings_customertype);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $hrm_settings_customertype->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $hrm_settings_customertype->setCreateDate(new \DateTime('now'));
        $hrm_settings_customertype->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $hrm_settings_customertype->setLastUpdate(new \DateTime('now'));
        $hrm_settings_customertype->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($hrm_settings_customertype);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    
        public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }
        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $hrm_settings_customertype = $em->getRepository('AdminBundle:hrm_settings_customertype')->find($data ['id']);
        $form = $this->createForm('AdminBundle\Form\hrm_settings_customertypeType', $hrm_settings_customertype);
        $form->submit($data);
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();
        $hrm_settings_customertype->setCreateDate(new \DateTime('now'));
        $hrm_settings_customertype->setCreateUid($current_user_id);
        $hrm_settings_customertype->setLastUpdateUid($current_user_id);
        $hrm_settings_customertype->setLastUpdate(new \DateTime('now'));
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    /**
     * Creates a new hrm_settings_customertype entity.
     *
     */
    public function newAction() {
//        $hrm_settings_customertype = new Hrm_settings_customertype();
//        $provider = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
//
//        $form = $this->createForm('AdminBundle\Form\hrm_settings_customertypeType', $hrm_settings_customertype, array(
//            'provider' => $provider
//        ));
//
//        $form->handleRequest($request);

        
        $hrm_settings_customertype = new Hrm_settings_customertype();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_customertypeType', $hrm_settings_customertype);
        return $this->render('admin/hrm_settings_customertype/new.html.twig', array(
                    'hrm_settings_customertype' => $hrm_settings_customertype,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_settings_customertype entity.
     *
     */
    public function showAction(hrm_settings_customertype $hrm_settings_customertype) {
        $deleteForm = $this->createDeleteForm($hrm_settings_customertype);

        return $this->render('admin/hrm_settings_customertype/show.html.twig', array(
                    'hrm_settings_customertype' => $hrm_settings_customertype,
                    'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_settings_customertype entity.
     *
     */
    public function editAction() {
        $hrm_settings_customertype = new Hrm_settings_customertype();
        $editForm = $this->createForm('AdminBundle\Form\hrm_settings_customertypeType', $hrm_settings_customertype);

        return $this->render('admin/hrm_settings_customertype/edit.html.twig', array(
                    'hrm_settings_customertype' => $hrm_settings_customertype,
                    'form' => $editForm->createView(),
        ));
    }

    /**
     * Deletes a typetransport entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $typetransport = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_customertype')->find($id);
        if ($typetransport) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typetransport);
            $em->flush($typetransport);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

    /**
     * Creates a form to delete a hrm_settings_customertype entity.
     *
     * @param hrm_settings_customertype $hrm_settings_customertype The hrm_settings_customertype entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_settings_customertype $hrm_settings_customertype) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('hrm_settings_customertype_delete', array('id' => $hrm_settings_customertype->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }
    
    public function seqnoUpdateAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);

        foreach ($json_data as $key => $value) {
            $hrm_settings_customertype = $em->getRepository('AdminBundle:hrm_settings_customertype')->find($json_data[$key]['id']);
            $hrm_settings_customertype->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

}
