<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsTrainingType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\SettingsTrainingTypeType;

/**
 * SettingsTrainingType  controller.
 *
 */
class SettingsTrainingTypeController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/settingstrainingtypes/layout.html.twig');
    }

    /**
     * Lists all settingstrainingtypes entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/settingstrainingtypes/index.html.twig');
    }

    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $trainingtypes = $this->getDoctrine()->getRepository('AdminBundle:SettingsTrainingType')->SettingsTrainingTypeByProvider($provider_id);
        return new JsonResponse(array(
            'trainingtypes' => $trainingtypes,
        ));
    }

    /**
     * Creates a new trainingtype entity.
     *
     */
    public function newAction(Request $request) {
        $trainingtype = new SettingsTrainingType();
        $form = $this->createForm('AdminBundle\Form\SettingsTrainingTypeType', $trainingtype);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($trainingtype);
            $em->flush($trainingtype);

            return $this->redirectToRoute('adminTrainingType_show', array('id' => $trainingtype->getId()));
        }

        return $this->render('Admin/settingstrainingtypes/new.html.twig', array(
                    'trainingtype' => $trainingtype,
                    'form' => $form->createView(),
        ));
       
    }

    public function saveAction(Request $request) {
        $trainingtype = new SettingsTrainingType();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsTrainingTypeType(), $trainingtype);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $trainingtype->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $trainingtype->setCreateDate(new \DateTime('now'));
        $trainingtype->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $trainingtype->setLastUpdate(new \DateTime('now'));
        $trainingtype->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId()); 
        $em->persist($trainingtype);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing trainingtype entity.
     *
     */
    public function editAction(Request $request) {
//        $_Newtrainingtype = new SettingsTrainingType();
        $trainingtype = new SettingsTrainingType();
        $form = $this->createForm('AdminBundle\Form\SettingsTrainingTypeType', $trainingtype);

        return $this->render('Admin/settingstrainingtypes/edit.html.twig', array(
                    'trainingtype' => $trainingtype,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request)
   {

        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retTrainingType = $em->getRepository('AdminBundle:SettingsTrainingType')->find($json_data['id']);
        $form = $this->createForm(new SettingsTrainingTypeType(), $retTrainingType);
        $form->submit($json_data);
        $retTrainingType->setLastUpdate(new \DateTime('now'));
        $retTrainingType->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());        
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsTrainingTypeAction(Request $request, SettingsTrainingType $trainingtype) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsTrainingType');
        $trainingtypes = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $trainingtype->getId())
                ->getQuery()
                ->getArrayResult();

        if ($trainingtypes)
            return new JsonResponse($trainingtypes[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a trainingtype entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $trainingtype = $this->getDoctrine()->getRepository('AdminBundle:SettingsTrainingType')->find($id);
        if ($trainingtype) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($trainingtype);
            $em->flush($trainingtype);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

}
