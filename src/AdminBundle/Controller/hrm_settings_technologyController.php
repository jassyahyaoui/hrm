<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_technology;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\hrm_settings_technologyType;

/**
 * Hrm_settings_technology controller.
 *
 */
class hrm_settings_technologyController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/hrm_settings_technology/layout.html.twig');
    }

    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsTechnology = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_technology')->SettingsTechnologyByProvider($provider_id);
        return new JsonResponse(array(
            'settingsTechnology' => $settingsTechnology,
        ));
    }

    public function saveAction(Request $request) {
        $settingsTechnology = new Hrm_settings_technology();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_technologyType(), $settingsTechnology);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsTechnology->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsTechnology->setCreateDate(new \DateTime('now'));
        $settingsTechnology->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsTechnology->setLastUpdate(new \DateTime('now'));
        $settingsTechnology->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($settingsTechnology);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }
        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $settingsTechnology = $em->getRepository('AdminBundle:hrm_settings_technology')->find($data ['id']);
        $form = $this->createForm('AdminBundle\Form\hrm_settings_technologyType', $settingsTechnology);
        $form->submit($data);
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();
        $settingsTechnology->setCreateDate(new \DateTime('now'));
        $settingsTechnology->setCreateUid($current_user_id);
        $settingsTechnology->setLastUpdateUid($current_user_id);
        $settingsTechnology->setLastUpdate(new \DateTime('now'));
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    /**
     * Deletes a typetransport entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }
        $settingsTechnology = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_technology')->find($id);
        if ($settingsTechnology) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsTechnology);
            $em->flush($settingsTechnology);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

    public function seqnoUpdateAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);

        foreach ($json_data as $key => $value) {
            $settingsTechnology = $em->getRepository('AdminBundle:hrm_settings_technology')->find($json_data[$key]['id']);
            $settingsTechnology->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    /**
     * Lists all hrm_settings_technology entities.
     *
     */
    public function indexAction() {
        $em = $this->getDoctrine()->getManager();

        $hrm_settings_technologies = $em->getRepository('AdminBundle:hrm_settings_technology')->findAll();

        return $this->render('Admin/hrm_settings_technology/index.html.twig', array(
                    'hrm_settings_technologies' => $hrm_settings_technologies,
        ));
    }

    /**
     * Creates a new hrm_settings_technology entity.
     *
     */
    public function newAction() {
        $hrm_settings_technology = new Hrm_settings_technology();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_technologyType', $hrm_settings_technology);
        return $this->render('Admin\hrm_settings_technology/new.html.twig', array(
                    'hrm_settings_technology' => $hrm_settings_technology,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_settings_technology entity.
     *
     */
    public function showAction(hrm_settings_technology $hrm_settings_technology) {
        $hrm_settings_technology = new Hrm_settings_technology();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_technologyType', $hrm_settings_technology);
        return $this->render('Admin\hrm_settings_technology/edit.html.twig', array(
                    'hrm_settings_technology' => $hrm_settings_technology,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_settings_technology entity.
     *
     */
    public function editAction() {
        $hrm_settings_technology = new Hrm_settings_technology();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_technologyType', $hrm_settings_technology);
        return $this->render('Admin\hrm_settings_technology/edit.html.twig', array(
                    'hrm_settings_technology' => $hrm_settings_technology,
                    'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to delete a hrm_settings_technology entity.
     *
     * @param hrm_settings_technology $hrm_settings_technology The hrm_settings_technology entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_settings_technology $hrm_settings_technology) {
        return $this->createFormBuilder()
                        ->setAction($this->generateUrl('hrm_settings_technology_delete', array('id' => $hrm_settings_technology->getId())))
                        ->setMethod('DELETE')
                        ->getForm()
        ;
    }

}
