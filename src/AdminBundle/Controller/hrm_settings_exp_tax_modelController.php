<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_exp_tax_model;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\hrm_settings_exp_tax_modelType;

/**
 * hrm_settings_exp_tax_model controller.
 *
 */
class hrm_settings_exp_tax_modelController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/hrm_settings_exp_tax_model/layout.html.twig');
    }

    /**
     * Lists all hrm_settings_exp_tax_model entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/hrm_settings_exp_tax_model/index.html.twig');
    }

    // for angurlajs Rest
    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $hrm_settings_exp_tax_models = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_tax_model')->hrm_settings_exp_tax_modelByProvider($provider_id);
        return new JsonResponse(array(
            'hrm_settings_exp_tax_models' => $hrm_settings_exp_tax_models,
        ));
    }

    /**
     * Creates a new hrm_settings_exp_tax_model entity.
     *
     */
    public function newAction(Request $request) {
        $hrm_settings_exp_tax_model = new hrm_settings_exp_tax_model();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_exp_tax_modelType', $hrm_settings_exp_tax_model);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($hrm_settings_exp_tax_model);
            $em->flush($hrm_settings_exp_tax_model);

            return $this->redirectToRoute('admin_hrm_settings_exp_tax_model_show', array('id' => $hrm_settings_exp_tax_model->getId()));
        }

        return $this->render('Admin/hrm_settings_exp_tax_model/new.html.twig', array(
                    'hrm_settings_exp_tax_model' => $hrm_settings_exp_tax_model,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $hrm_settings_exp_tax_model = new hrm_settings_exp_tax_model();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_exp_tax_modelType(), $hrm_settings_exp_tax_model);
        $form->handleRequest($request);
        $form->submit($json_data);

        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        //$hrm_settings_exp_tax_model->setTax($this->getDoctrine()->getRepository('AdminBundle:SettingsTaxType')->find($json_data['tax']));
        $hrm_settings_exp_tax_model->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $hrm_settings_exp_tax_model->setCreateDate(new \DateTime('now'));
        $hrm_settings_exp_tax_model->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $hrm_settings_exp_tax_model->setLastUpdate(new \DateTime('now'));
        $hrm_settings_exp_tax_model->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());        
        $em->persist($hrm_settings_exp_tax_model);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing hrm_settings_exp_tax_model entity.
     *
     */
    public function editAction(Request $request) {
        $hrm_settings_exp_tax_model = new hrm_settings_exp_tax_model();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_exp_tax_modelType', $hrm_settings_exp_tax_model);

        return $this->render('Admin/hrm_settings_exp_tax_model/edit.html.twig', array(
                    'hrm_settings_exp_tax_model' => $hrm_settings_exp_tax_model,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $rethrm_settings_exp_tax_model = $em->getRepository('AdminBundle:hrm_settings_exp_tax_model')->find($json_data ['id']);
        $form = $this->createForm(new hrm_settings_exp_tax_modelType(), $rethrm_settings_exp_tax_model);
        $form->submit($json_data);
        $rethrm_settings_exp_tax_model->setLastUpdate(new \DateTime('now'));
        $rethrm_settings_exp_tax_model->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function get_hrm_settings_exp_tax_modelAction(Request $request, hrm_settings_exp_tax_model $hrm_settings_exp_tax_model) {
       $em = $this->getDoctrine()->getEntityManager();
        $hrm_settings_exp_tax_models = $em->createQueryBuilder()
                ->addSelect( 'r.id' )
                ->addSelect( 'tx.id as tax' )
                ->addSelect( 'r.display_name' )
                ->addSelect( 'r.description' )
                ->from('AdminBundle:hrm_settings_exp_tax_model', 'r')
                ->leftJoin('r.tax','tx')
                ->where('r.id = :id')
                ->setParameter('id', $hrm_settings_exp_tax_model->getId())
                ->getQuery()
                ->getArrayResult();

        if ($hrm_settings_exp_tax_models)
            return new JsonResponse($hrm_settings_exp_tax_models[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a hrm_settings_exp_tax_model entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $hrm_settings_exp_tax_model = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_tax_model')->find($id);
        if ($hrm_settings_exp_tax_model) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_settings_exp_tax_model);
            $em->flush($hrm_settings_exp_tax_model);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

}
