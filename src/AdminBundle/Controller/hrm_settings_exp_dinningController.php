<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_exp_dinning;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\hrm_settings_exp_dinningType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Settingsexpdinning controller.
 *
 */
class hrm_settings_exp_dinningController extends Controller
{

    public function layoutAction()
    {
        return $this->render('Admin/hrm_settings_exp_dinning/layout.html.twig');
    }

    /**
     * Lists all settingsExpDinning entities.
     *
     */
    public function indexAction()
    {
        return $this->render('Admin/hrm_settings_exp_dinning/index.html.twig');
    }

    // for angurlajs Rest
    public function listAction()
    {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsExpDinnings = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_dinning')->hrm_settings_exp_dinningByProvider($provider_id);
        return new JsonResponse(array(
            'settingsExpDinnings' => $settingsExpDinnings,
        ));
    }

    /**
     * Creates a new settingsExpDinning entity.
     *
     */
    public function newAction(Request $request)
    {
        $settingsExpDinning = new hrm_settings_exp_dinning();
        $provider = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();

        $form = $this->createForm('AdminBundle\Form\hrm_settings_exp_dinningType', $settingsExpDinning, array(
            'provider' => $provider
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsExpDinning);
            $em->flush($settingsExpDinning);

            return $this->redirectToRoute('adminhrm_settings_exp_dinning_show', array('id' => $settingsExpDinning->getId()));
        }

        return $this->render('Admin/hrm_settings_exp_dinning/new.html.twig', array(
            'settingsExpDinning' => $settingsExpDinning,
            'form' => $form->createView(),
        ));
    }
    public function editAction(Request $request)
    {
        $settingsExpDinning = new hrm_settings_exp_dinning();
        $provider = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();

        $form = $this->createForm('AdminBundle\Form\hrm_settings_exp_dinningType', $settingsExpDinning, array(
            'provider' => $provider
        ));
        $form->handleRequest($request);

        return $this->render('Admin/hrm_settings_exp_dinning/edit.html.twig', array(
            'settingsExpDinning' => $settingsExpDinning,
            'form' => $form->createView(),
        ));
    }
    public function saveAction(Request $request)
    {
        $settingsExpDinning = new hrm_settings_exp_dinning();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_exp_dinningType(), $settingsExpDinning);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        if(isset($json_data['tax_model']))
        {
            $settingsExpDinning->setTaxModel($this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_tax_model')->find($json_data['tax_model']));
        }
        $settingsExpDinning->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsExpDinning->setCreateDate(new \DateTime('now'));
        $settingsExpDinning->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsExpDinning->setLastUpdate(new \DateTime('now'));
        $settingsExpDinning->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());

        $em->persist($settingsExpDinning);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing settingsExpDinning entity.
     *
     */


    public function updateAction(Request $request)
    {

        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retsettingsExpDinnings = $em->getRepository('AdminBundle:hrm_settings_exp_dinning')->find($json_data ['id']);
        $form = $this->createForm(new hrm_settings_exp_dinningType(), $retsettingsExpDinnings);
        $form->submit($json_data);

        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        if(isset($json_data['tax_model']['id']))
        {
            $retsettingsExpDinnings->setTaxModel($this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_tax_model')->find($json_data['tax_model']['id']));
        }
        $retsettingsExpDinnings->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $retsettingsExpDinnings->setCreateDate(new \DateTime('now'));
        $retsettingsExpDinnings->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $retsettingsExpDinnings->setLastUpdate(new \DateTime('now'));
        $retsettingsExpDinnings->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function get_hrm_settings_exp_dinningAction(Request $request, hrm_settings_exp_dinning $settingsExpDinning)
    {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_dinning');
        $settingsExpDinnings = $repo->createQueryBuilder('r')
            ->where('r.id = :id')
            ->setParameter('id', $settingsExpDinning->getId())
            ->getQuery()
            ->getArrayResult();

        if ($settingsExpDinnings)
            return new JsonResponse($settingsExpDinnings[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a settingsExpDinning entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $settingsExpDinning = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_dinning')->find($id);
        if ($settingsExpDinning) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsExpDinning);
            $em->flush($settingsExpDinning);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }
    
    public function seqnoUpdateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
//        dump($json_data[0]);
//        die();
        foreach ($json_data as $key => $value) {
            if ($json_data[$key]['type'] == 'Restauration') {
                $settingsExpDinning = $em->getRepository('AdminBundle:hrm_settings_exp_dinning')->find($json_data[$key]['id']);
                $settingsExpDinning->setSeqno($key+1);
            }
        }
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }   

}
