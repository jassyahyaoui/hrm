<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsFamilyStatus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Settingsfamilystatus controller.
 *
 * @Route("settingsfamilystatus")
 */
class SettingsFamilyStatusController extends Controller
{
    /**
     * Lists all settingsFamilyStatus entities.
     *
     * @Route("/", name="settingsfamilystatus_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $settingsFamilyStatuses = $em->getRepository('AdminBundle:SettingsFamilyStatus')->findAll();

        return $this->render('Admin/settingsfamilystatus/index.html.twig', array(
            'settingsFamilyStatuses' => $settingsFamilyStatuses,
        ));
    }

    /**
     * Creates a new settingsFamilyStatus entity.
     *
     * @Route("/new", name="settingsfamilystatus_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $settingsFamilyStatus = new Settingsfamilystatus();
        $form = $this->createForm('AdminBundle\Form\SettingsFamilyStatusType', $settingsFamilyStatus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsFamilyStatus);
            $em->flush();

            return $this->redirectToRoute('settingsfamilystatus_show', array('id' => $settingsFamilyStatus->getId()));
        }

        return $this->render('Admin/settingsfamilystatus/new.html.twig', array(
            'settingsFamilyStatus' => $settingsFamilyStatus,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a settingsFamilyStatus entity.
     *
     * @Route("/{id}", name="settingsfamilystatus_show")
     * @Method("GET")
     */
    public function showAction(SettingsFamilyStatus $settingsFamilyStatus)
    {
        $deleteForm = $this->createDeleteForm($settingsFamilyStatus);

        return $this->render('Admin/settingsfamilystatus/show.html.twig', array(
            'settingsFamilyStatus' => $settingsFamilyStatus,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing settingsFamilyStatus entity.
     *
     * @Route("/{id}/edit", name="settingsfamilystatus_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SettingsFamilyStatus $settingsFamilyStatus)
    {
        $deleteForm = $this->createDeleteForm($settingsFamilyStatus);
        $editForm = $this->createForm('AdminBundle\Form\SettingsFamilyStatusType', $settingsFamilyStatus);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('settingsfamilystatus_edit', array('id' => $settingsFamilyStatus->getId()));
        }

        return $this->render('Admin/settingsfamilystatus/edit.html.twig', array(
            'settingsFamilyStatus' => $settingsFamilyStatus,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a settingsFamilyStatus entity.
     *
     * @Route("/{id}", name="settingsfamilystatus_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SettingsFamilyStatus $settingsFamilyStatus)
    {
        $form = $this->createDeleteForm($settingsFamilyStatus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsFamilyStatus);
            $em->flush();
        }

        return $this->redirectToRoute('settingsfamilystatus_index');
    }

    /**
     * Creates a form to delete a settingsFamilyStatus entity.
     *
     * @param SettingsFamilyStatus $settingsFamilyStatus The settingsFamilyStatus entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SettingsFamilyStatus $settingsFamilyStatus)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('settingsfamilystatus_delete', array('id' => $settingsFamilyStatus->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
