<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsBonus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\SettingsBonusType;

/**
 * SettingsBonus controller.
 *
 */
class SettingsBonusController extends Controller
{

    public function layoutAction()
    {
        return $this->render('Admin/settingsbonus/layout.html.twig');
    }

    /**
     * Lists all subpremiumtype entities.
     *
     */
    public function indexAction()
    {
        return $this->render('Admin/settingsbonus/index.html.twig');
    }

    public function listAction()
    {
        $provider_id = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $subpremiumtypes = $this->getDoctrine()->getRepository('AdminBundle:SettingsBonus')->SettingsBonusByProvider($provider_id);
        return new JsonResponse(array(
            'subpremiumtypes' => $subpremiumtypes,
        ));
    }

    /**
     * Creates a new subpremiumtype entity.
     *
     */
    public function newAction(Request $request)
    {
        $subPremiumType = new SettingsBonus();
        $form = $this->createForm('AdminBundle\Form\SettingsBonusType', $subPremiumType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($subPremiumType);
            $em->flush($subPremiumType);

            return $this->redirectToRoute('adminSubPremiumType_show', array('id' => $subPremiumType->getId()));
        }

        return $this->render('Admin/settingsbonus/new.html.twig', array(
            'subpremiumtype' => $subPremiumType,
            'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request)
    {
        $subpremiumtypes = new SettingsBonus();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsBonusType(), $subpremiumtypes);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
//        if (isset($json_data['seqno']))
//            $subpremiumtypes->setSeqno($json_data['seqno']);
        $subpremiumtypes->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $subpremiumtypes->setBonusType($em->getRepository('AdminBundle:SettingsBonusType')->find($json_data ['bonus_type']));
        $subpremiumtypes->setCreateDate(new \DateTime('now'));
        $subpremiumtypes->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $subpremiumtypes->setLastUpdate(new \DateTime('now'));
        $subpremiumtypes->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($subpremiumtypes);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    public function getSettingsBonusAction(Request $request, $id)
    {
        $subpremiumtype = $this->getDoctrine()->getRepository('AdminBundle:SettingsBonus')->find($id);
        if ($subpremiumtype) {
            return new JsonResponse(array("id" => $subpremiumtype->getId(),
                "display_name" => $subpremiumtype->getDisplayName(),
                "description" => $subpremiumtype->getDescription(),
                "bonus_type" => $subpremiumtype->getBonusType()->getId()));
        }
        return new JsonResponse($null);
    }

    /**
     * Displays a form to edit an existing subpremiumtype entity.
     *
     */
    public function editAction(Request $request)
    {
        $subPremiumType = new SettingsBonus();
        $form = $this->createForm('AdminBundle\Form\SettingsBonusType', $subPremiumType);

        return $this->render('Admin/settingsbonus/edit.html.twig', array(
            'subpremiumtypes' => $subPremiumType,
            'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request)
    {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retSubPremiumType = $em->getRepository('AdminBundle:SettingsBonus')->find($json_data['id']);
        $form = $this->createForm(new SettingsBonusType(), $retSubPremiumType);
        $form->submit($json_data);
//        $retSubPremiumType->setSeqno($json_data['seqno']);
        $retSubPremiumType->setLastUpdate(new \DateTime('now'));
        $retSubPremiumType->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    /**
     * Deletes a subpremiumtype entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $subpremiumtypes = $this->getDoctrine()->getRepository('AdminBundle:SettingsBonus')->find($id);
        if ($subpremiumtypes) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($subpremiumtypes);
            $em->flush($subpremiumtypes);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }


    public function seqnoUpdateAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
        foreach ($json_data as $key => $value) {
            $SettingsBonus = $em->getRepository('AdminBundle:SettingsBonus')->find($json_data[$key]['id']);
            $SettingsBonus->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
}
