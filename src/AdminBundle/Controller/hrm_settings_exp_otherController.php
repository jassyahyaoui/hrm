<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_exp_other;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\hrm_settings_exp_otherType;

/**
 * hrm_settings_exp_other controller.
 *
 */
class hrm_settings_exp_otherController extends Controller
{

    public function layoutAction()
    {
        return $this->render('Admin/hrm_settings_exp_other/layout.html.twig');
    }

    /**
     * Lists all hrm_settings_exp_other entities.
     *
     */
    public function indexAction()
    {
        return $this->render('Admin/hrm_settings_exp_other/index.html.twig');
    }

    public function listAction()
    {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $hrm_settings_exp_others = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_other')->hrm_settings_exp_otherByProvider($provider_id);
        return new JsonResponse(array(
            'hrm_settings_exp_others' => $hrm_settings_exp_others,
        ));
    }

    /**
     * Creates a new hrm_settings_exp_other entity.
     *
     */
    public function newAction(Request $request)
    {
        $hrm_settings_exp_other = new hrm_settings_exp_other();
        $provider = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();

        $form = $this->createForm('AdminBundle\Form\hrm_settings_exp_otherType', $hrm_settings_exp_other, array(
            'provider' => $provider
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($hrm_settings_exp_other);
            $em->flush($hrm_settings_exp_other);

            return $this->redirectToRoute('adminTypeExpenses_show', array('id' => $hrm_settings_exp_other->getId()));
        }

        return $this->render('Admin/hrm_settings_exp_other/new.html.twig', array(
            'hrm_settings_exp_other' => $hrm_settings_exp_other,
            'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request)
    {
        $hrm_settings_exp_other = new hrm_settings_exp_other();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_exp_otherType(), $hrm_settings_exp_other);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        if(isset($json_data['tax_model']))
        {
            $hrm_settings_exp_other->setTaxModel($this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_tax_model')->find($json_data['tax_model']));
        }
        $hrm_settings_exp_other->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $hrm_settings_exp_other->setCreateDate(new \DateTime('now'));
        $hrm_settings_exp_other->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $hrm_settings_exp_other->setLastUpdate(new \DateTime('now'));
        $hrm_settings_exp_other->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($hrm_settings_exp_other);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing hrm_settings_exp_other entity.
     *
     */
    public function editAction(Request $requestr)
    {
        $hrm_settings_exp_other = new hrm_settings_exp_other();
        $provider = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();

        $form = $this->createForm('AdminBundle\Form\hrm_settings_exp_otherType', $hrm_settings_exp_other, array(
            'provider' => $provider
        ));
        return $this->render('Admin/hrm_settings_exp_other/edit.html.twig', array(
            'hrm_settings_exp_other' => $hrm_settings_exp_other,
            'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request)
    {

        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);

        $em = $this->getDoctrine()->getEntityManager();
        $retTypeExpense = $em->getRepository('AdminBundle:hrm_settings_exp_other')->find($json_data ['id']);
        $form = $this->createForm(new hrm_settings_exp_otherType(), $retTypeExpense);
        $form->submit($json_data);

        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        if(isset($json_data['tax_model']['id']))
        {
            $retTypeExpense->setTaxModel($this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_tax_model')->find($json_data['tax_model']['id']));
        }
        $retTypeExpense->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $retTypeExpense->setCreateDate(new \DateTime('now'));
        $retTypeExpense->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $retTypeExpense->setLastUpdate(new \DateTime('now'));
        $retTypeExpense->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function get_hrm_settings_exp_otherAction(Request $request, hrm_settings_exp_other $hrm_settings_exp_other)
    {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_other');
        $hrm_settings_exp_others = $repo->createQueryBuilder('r')
            ->where('r.id = :id')
            ->setParameter('id', $hrm_settings_exp_other->getId())
            ->getQuery()
            ->getArrayResult();

        if ($hrm_settings_exp_others)
            return new JsonResponse($hrm_settings_exp_others[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a hrm_settings_exp_other entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $hrm_settings_exp_other = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_other')->find($id);
        if ($hrm_settings_exp_other) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_settings_exp_other);
            $em->flush($hrm_settings_exp_other);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

    public function seqnoUpdateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
//        dump($json_data[0]);
//        die();
        foreach ($json_data as $key => $value) {
            if ($json_data[$key]['type'] == 'Autres') {
                $hrm_settings_exp_other = $em->getRepository('AdminBundle:hrm_settings_exp_other')->find($json_data[$key]['id']);
                $hrm_settings_exp_other->setSeqno($key+1);
            }
        }
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }     

}
