<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsEmpStatus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\SettingsEmpStatusType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Settingsempstatus controller.
 *
 */
class SettingsEmpStatusController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/settingsempstatus/layout.html.twig');
        /***/
    }

    /**
     * Lists all SettingsEmpStatus entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/settingsempstatus/index.html.twig');
    }

    // for angurlajs Rest
    public function listAction() {
                $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsEmpStatus = $this->getDoctrine()->getRepository('AdminBundle:SettingsEmpStatus')->SettingsEmpStatusByProvider($provider_id);
        
        return new JsonResponse(array(
            'settingsEmpStatus' => $settingsEmpStatus,
        ));
    }

    /**
     * Creates a new SettingsEmpStatus entity.
     *
     */
    public function newAction(Request $request) {
        $settingsEmpStatu = new SettingsEmpStatus();
        $form = $this->createForm('AdminBundle\Form\SettingsEmpStatusType', $settingsEmpStatu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsEmpStatu);
            $em->flush($settingsEmpStatu);

            return $this->redirectToRoute('adminSettingsEmpStatus_show', array('id' => $settingsEmpStatu->getId()));
        }

        return $this->render('Admin/settingsempstatus/new.html.twig', array(
                    'settingsEmpStatu' => $settingsEmpStatu,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $settingsEmpStatu = new SettingsEmpStatus();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsEmpStatusType(), $settingsEmpStatu);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsEmpStatu->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsEmpStatu->setCreateDate(new \DateTime('now'));
        $settingsEmpStatu->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsEmpStatu->setLastUpdate(new \DateTime('now'));
        $settingsEmpStatu->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());      
//          if (isset($data['seqno']))
//        $setsettingsEmpStatu->setSeqno($json_data['seqno']);
        $em->persist($settingsEmpStatu);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing SettingsEmpStatus entity.
     *
     */
    public function editAction(Request $request) {
        $settingsEmpStatu = new SettingsEmpStatus();
        $form = $this->createForm('AdminBundle\Form\SettingsEmpStatusType', $settingsEmpStatu);

        return $this->render('Admin/settingsempstatus/edit.html.twig', array(
                    'settingsEmpStatu' => $settingsEmpStatu,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {

        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retsettingsEmpStatu = $em->getRepository('AdminBundle:SettingsEmpStatus')->find($json_data ['id']);
        $form = $this->createForm(new SettingsEmpStatusType(), $retsettingsEmpStatu);
        $form->submit($json_data);
        $retsettingsEmpStatu->setLastUpdate(new \DateTime('now'));
        $retsettingsEmpStatu->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
//          if (isset($data['seqno']))
//        $retsettingsEmpStatu->setSeqno($json_data['seqno']);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsEmpStatusAction(Request $request, SettingsEmpStatus $settingsEmpStatu) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsEmpStatus');
        $settingsEmpStatus = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $settingsEmpStatu->getId())
                ->getQuery()
                ->getArrayResult();

        if ($settingsEmpStatus)
            return new JsonResponse($settingsEmpStatus[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a SettingsEmpStatus entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $settingsEmpStatu = $this->getDoctrine()->getRepository('AdminBundle:SettingsEmpStatus')->find($id);
        if ($settingsEmpStatu) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsEmpStatu);
            $em->flush($settingsEmpStatu);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }
    public function seqnoUpdateAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
        
        foreach ($json_data as $key => $value) {
            $retPaymentMethod = $em->getRepository('AdminBundle:SettingsEmpStatus')->find($json_data[$key]['id']);
            $retPaymentMethod->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
}
