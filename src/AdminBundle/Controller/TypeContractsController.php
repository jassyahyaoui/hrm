<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\TypeContracts;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\TypeContractsType;

/**
 * Typecontract controller.
 *
 */
class TypeContractsController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/typecontracts/layout.html.twig');
    }

    /**
     * Lists all typecontract entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/typecontracts/index.html.twig');
    }

    // for angurlajs Rest
    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $typecontracts = $this->getDoctrine()->getRepository('AdminBundle:TypeContracts')->TypeContractsByProvider($provider_id);
        return new JsonResponse(array(
            'typecontracts' => $typecontracts,
        ));
    }

    /**
     * Creates a new typecontract entity.
     *
     */
    public function newAction(Request $request) {
        $typecontract = new TypeContracts();
        $form = $this->createForm('AdminBundle\Form\TypeContractsType', $typecontract);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($typecontract);
            $em->flush($typecontract);

            return $this->redirectToRoute('adminTypeContracts_show', array('id' => $typecontract->getId()));
        }

        return $this->render('Admin/typecontracts/new.html.twig', array(
                    'typecontract' => $typecontract,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $typecontract = new TypeContracts();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new TypeContractsType(), $typecontract);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $typecontract->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $typecontract->setCreateDate(new \DateTime('now'));
        $typecontract->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $typecontract->setLastUpdate(new \DateTime('now'));
        $typecontract->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
//        $typecontract->setSeqno($json_data['seqno']);
        $em->persist($typecontract);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing typecontract entity.
     *
     */
    public function editAction(Request $request) {
        $typecontract = new TypeContracts();
        $form = $this->createForm('AdminBundle\Form\TypeContractsType', $typecontract);
        return $this->render('Admin/typecontracts/edit.html.twig', array(
                    'typecontract' => $typecontract,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retTypeContract = $em->getRepository('AdminBundle:TypeContracts')->find($json_data['id']);
        $form = $this->createForm(new TypeContractsType(), $retTypeContract);
        $form->submit($json_data);
        $retTypeContract->setLastUpdate(new \DateTime('now'));
        $retTypeContract->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
//        $retTypeContract->setSeqno($json_data['seqno']);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getTypeContractsAction(Request $request, TypeContracts $typecontract) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:TypeContracts');
        $typecontracts = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $typecontract->getId())
                ->getQuery()
                ->getArrayResult();

        if ($typecontracts)
            return new JsonResponse($typecontracts[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a typecontract entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $typecontract = $this->getDoctrine()->getRepository('AdminBundle:TypeContracts')->find($id);
        if ($typecontract) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($typecontract);
            $em->flush($typecontract);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

//    public function seqnoUpdateAction(Request $request) {
//        $em = $this->getDoctrine()->getManager();
//        $body = $request->getContent();
//        $data = json_decode($body, true);
//        $TypeContracts = $em->getRepository('AdminBundle:TypeContracts')->find($data['id']);
//        $update_form = $this->createForm('AdminBundle\Form\TypeContractsType', $TypeContracts);
//        
//        if ($request->isMethod('PUT')) {     
////            dump($data);
////            die();
//            $em->persist($TypeContracts);
//            $em->flush();
//            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
//            return $response;
//        }
//    }

    public function seqnoUpdateAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
        
        foreach ($json_data as $key => $value) {
            $retTypeContract = $em->getRepository('AdminBundle:TypeContracts')->find($json_data[$key]['id']);
            $retTypeContract->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

}
