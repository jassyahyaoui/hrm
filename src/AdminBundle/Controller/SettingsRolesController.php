<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsRoles;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\SettingsRolesType;

/**
 * SettingsRoles controller.
 *
 */
class SettingsRolesController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/settingsroles/layout.html.twig');
    }

    /**
     * Lists all role entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/settingsroles/index.html.twig');
    }

    public function listAction() {
                $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $roles = $this->getDoctrine()->getRepository('AdminBundle:SettingsRoles')->SettingsRolesByProvider($provider_id);
        return new JsonResponse(array(
            'roles' => $roles,
        ));
    }

    /**
     * Creates a new role entity.
     *
     */
    public function newAction(Request $request) {
        $role = new SettingsRoles();
        $form = $this->createForm('AdminBundle\Form\SettingsRolesType', $role);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($role);
            $em->flush($role);

            return $this->redirectToRoute('adminRoles_show', array('id' => $role->getId()));
        }

        return $this->render('Admin/settingsroles/new.html.twig', array(
                    'role' => $role,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $role = new SettingsRoles();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsRolesType(), $role);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $role->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $role->setCreateDate(new \DateTime('now'));
        $role->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $role->setLastUpdate(new \DateTime('now'));
        $role->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());        
        $em->persist($role);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing role entity.
     *
     */
    public function editAction(Request $request) {
        $role = new SettingsRoles();
        $form = $this->createForm('AdminBundle\Form\SettingsRolesType', $role);

        return $this->render('Admin/settingsroles/edit.html.twig', array(
                    'role' => $role,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retRole = $em->getRepository('AdminBundle:SettingsRoles')->find($json_data ['id']);
        $form = $this->createForm(new SettingsRolesType(), $retRole);
        $form->submit($json_data);
        $retRole->setLastUpdate(new \DateTime('now'));
        $retRole->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsRolesAction(Request $request, SettingsRoles $role) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsRoles');
        $roles = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $role->getId())
                ->getQuery()
                ->getArrayResult();

        if ($roles)
            return new JsonResponse($roles[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a role entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $role = $this->getDoctrine()->getRepository('AdminBundle:SettingsRoles')->find($id);
        if ($role) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($role);
            $em->flush($role);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

}
