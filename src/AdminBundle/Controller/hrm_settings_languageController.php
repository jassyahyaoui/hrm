<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_language;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\hrm_settings_languageType;

/**
 * Hrm_settings_language controller.
 *
 */
class hrm_settings_languageController extends Controller
{
    public function layoutAction() {
        return $this->render('Admin/hrm_settings_language/layout.html.twig');
    }

    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsTechnology = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_language')->SettingsTechnologyByProvider($provider_id);
        return new JsonResponse(array(
            'settingsLanguage' => $settingsTechnology,
        ));
    }

    public function saveAction(Request $request) {
        $settingsLanguage = new Hrm_settings_language();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_languageType(), $settingsLanguage);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsLanguage->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsLanguage->setCreateDate(new \DateTime('now'));
        $settingsLanguage->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsLanguage->setLastUpdate(new \DateTime('now'));
        $settingsLanguage->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($settingsLanguage);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }
        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $settingLanguage = $em->getRepository('AdminBundle:hrm_settings_language')->find($data ['id']);
        $form = $this->createForm('AdminBundle\Form\hrm_settings_languageType', $settingLanguage);
        $form->submit($data);
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();
        $settingLanguage->setCreateDate(new \DateTime('now'));
        $settingLanguage->setCreateUid($current_user_id);
        $settingLanguage->setLastUpdateUid($current_user_id);
        $settingLanguage->setLastUpdate(new \DateTime('now'));
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    /**
     * Deletes a typetransport entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }
        $settingLanguage = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_language')->find($id);
        if ($settingLanguage) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingLanguage);
            $em->flush($settingLanguage);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

    public function seqnoUpdateAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);

        foreach ($json_data as $key => $value) {
            $settingsTechnology = $em->getRepository('AdminBundle:hrm_settings_technology')->find($json_data[$key]['id']);
            $settingsTechnology->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    /**
     * Lists all hrm_settings_language entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hrm_settings_languages = $em->getRepository('AdminBundle:hrm_settings_language')->findAll();

        return $this->render('hrm_settings_language/index.html.twig', array(
            'hrm_settings_languages' => $hrm_settings_languages,
        ));
    }

    /**
     * Creates a new hrm_settings_language entity.
     *
     */
    public function newAction()
    {
        $hrm_settings_language = new Hrm_settings_language();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_languageType', $hrm_settings_language);
        return $this->render('Admin/hrm_settings_language/new.html.twig', array(
            'hrm_settings_language' => $hrm_settings_language,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_settings_language entity.
     *
     */
    public function showAction(hrm_settings_language $hrm_settings_language)
    {
        $deleteForm = $this->createDeleteForm($hrm_settings_language);

        return $this->render('hrm_settings_language/show.html.twig', array(
            'hrm_settings_language' => $hrm_settings_language,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_settings_language entity.
     *
     */
    public function editAction()
    {
        $hrm_settings_language = new Hrm_settings_language();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_languageType', $hrm_settings_language);
        return $this->render('Admin/hrm_settings_language/edit.html.twig', array(
            'hrm_settings_language' => $hrm_settings_language,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to delete a hrm_settings_language entity.
     *
     * @param hrm_settings_language $hrm_settings_language The hrm_settings_language entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_settings_language $hrm_settings_language)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('hrm_settings_language_delete', array('id' => $hrm_settings_language->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
