<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_exp_ik;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\hrm_settings_exp_ikType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Settingsexpcoeff controller.
 *
 */
class hrm_settings_exp_ikController extends Controller
{

    public function layoutAction()
    {
        return $this->render('Admin/hrm_settings_exp_ik/layout.html.twig');
    }

    /**
     * Lists all settingsExpCoeff entities.
     *
     */
    public function indexAction()
    {
        return $this->render('Admin/hrm_settings_exp_ik/index.html.twig');
    }

    // for angurlajs Rest
    public function listAction()
    {
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $provider_id = $empData->getCompany()->getProvider()->getId();
        $settingsExpCoeffs = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_ik')->hrm_settings_exp_ikByProvider($provider_id);
        return new JsonResponse(array(
            'settingsExpCoeffs' => $settingsExpCoeffs,
        ));
    }

    /**
     * Creates a new settingsExpCoeff entity.
     *
     */
    public function newAction(Request $request)
    {
        $settingsExpCoeff = new hrm_settings_exp_ik();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_exp_ikType', $settingsExpCoeff);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsExpCoeff);
            $em->flush($settingsExpCoeff);

            return $this->redirectToRoute('adminhrm_settings_exp_ik_show', array('id' => $settingsExpCoeff->getId()));
        }

        return $this->render('Admin/hrm_settings_exp_ik/new.html.twig', array(
            'settingsExpCoeff' => $settingsExpCoeff,
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request)
    {
        $settingsExpCoeff = new hrm_settings_exp_ik();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_exp_ikType', $settingsExpCoeff);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsExpCoeff);
            $em->flush($settingsExpCoeff);

            return $this->redirectToRoute('adminhrm_settings_exp_ik_show', array('id' => $settingsExpCoeff->getId()));
        }

        return $this->render('Admin/hrm_settings_exp_ik/edit.html.twig', array(
            'settingsExpCoeff' => $settingsExpCoeff,
            'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request)
    {
        $settingsExpCoeff = new hrm_settings_exp_ik();
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_exp_ikType(), $settingsExpCoeff);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $empData->getCompany()->getProvider()->getId();
        $settingsExpCoeff->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsExpCoeff->setCreateDate(new \DateTime('now'));
        $settingsExpCoeff->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsExpCoeff->setLastUpdate(new \DateTime('now'));
        $settingsExpCoeff->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($settingsExpCoeff);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing settingsExpCoeff entity.
     *
     */
    public function edit2Action(Request $request, hrm_settings_exp_ik $settingsExpCoeff)
    {
        $_NewsettingsExpCoeff = new hrm_settings_exp_ik();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_exp_ikType', $_NewsettingsExpCoeff);

        return $this->render('Admin/hrm_settings_exp_ik/edit.html.twig', array(
            'settingsExpCoeff' => $settingsExpCoeff,
            'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request)
    {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retsettingsExpCoeff = $em->getRepository('AdminBundle:hrm_settings_exp_ik')->find($json_data ['id']);
        $form = $this->createForm(new hrm_settings_exp_ikType(), $retsettingsExpCoeff);
        $form->submit($json_data);
        $retsettingsExpCoeff->setLastUpdate(new \DateTime('now'));
        $retsettingsExpCoeff->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function gethrm_settings_exp_ikAction(Request $request, hrm_settings_exp_ik $settingsExpCoeff)
    {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_ik');
        $settingsExpCoeffs = $repo->createQueryBuilder('r')
            ->where('r.id = :id')
            ->setParameter('id', $settingsExpCoeff->getId())
            ->getQuery()
            ->getArrayResult();

        if ($settingsExpCoeffs)
            return new JsonResponse($settingsExpCoeffs[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a settingsExpCoeff entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $settingsExpCoeff = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_ik')->find($id);
        if ($settingsExpCoeff) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsExpCoeff);
            $em->flush($settingsExpCoeff);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }
        public function seqnoUpdateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
//        dump($json_data[0]);
//        die();
        foreach ($json_data as $key => $value) {
           
                $settingsExpCoeff = $em->getRepository('AdminBundle:hrm_settings_exp_ik')->find($json_data[$key]['id']);
                $settingsExpCoeff->setSeqno($key+1);
            
        }
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

}
