<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\Categories;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\CategoriesType;

/**
 * Category controller.
 *
 */
class CategoriesController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/categories/layout.html.twig');
    }

    public function indexAction() {
        return $this->render('Admin/categories/index.html.twig');
    }

    // for angurlajs Rest
    public function listAction() {

        $repo = $this->getDoctrine()->getRepository('AdminBundle:Categories');
        $categories = $repo->createQueryBuilder('r')
                ->getQuery()
                ->getArrayResult();
        return new JsonResponse(array(
            'categories' => $categories,
        ));
    }

    /**
     * Creates a new categorie entity.
     *
     */
    public function newAction(Request $request) {
        $categorie = new Categories();
        $form = $this->createForm('AdminBundle\Form\CategoriesType', $categorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($categorie);
            $em->flush($categorie);

            return $this->redirectToRoute('adminCategories_show', array('id' => $categorie->getId()));
        }

        return $this->render('Admin/categories/new.html.twig', array(
                    'categorie' => $categorie,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $categorie = new Categories();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new CategoriesType(), $categorie);
        $form->handleRequest($request);
        $form->submit($json_data);
        $em->persist($categorie);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing categorie entity.
     *
     */
    public function editAction(Request $request, Categories $categorie) {
        $_Newcategorie = new Categories();
        $form = $this->createForm('AdminBundle\Form\CategoriesType', $_Newcategorie);

        return $this->render('Admin/categories/edit.html.twig', array(
                    'categorie' => $categorie,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retCategorie = $em->getRepository('AdminBundle:Categories')->find($json_data['id']);
        $form = $this->createForm(new CategoriesType(), $retCategorie);
        $form->submit($json_data);
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getCategorieAction(Request $request, Categories $categorie) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:Categories');
        $categories = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $categorie->getId())
                ->getQuery()
                ->getArrayResult();

        if ($categories)
            return new JsonResponse($categories[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a categorie entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        $categorie = new Categories();

        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $categorie = $this->getDoctrine()->getRepository('AdminBundle:Categories')->find($id);
        if ($categorie) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($categorie);
            $em->flush($categorie);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

}
