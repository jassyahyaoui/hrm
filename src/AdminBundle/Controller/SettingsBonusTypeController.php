<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsBonusType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\SettingsBonusTypeType;

/**
 * Premiumtype controller.
 *
 */
class SettingsBonusTypeController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/settingsbonustype/layout.html.twig');
    }

    /**
     * Lists all premiumtype entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/settingsbonustype/index.html.twig');
    }

    // for angurlajs Rest
    public function listAction() {

        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsBonusType');
        $premiumtypes = $repo->createQueryBuilder('r')
                          ->orderBy('r.seqno', 'ASC')
                ->getQuery()
       
                ->getArrayResult();
        return new JsonResponse(array(
            'premiumtypes' => $premiumtypes,
        ));
    }

    /**
     * Creates a new premiumtype entity.
     *
     */
    public function newAction(Request $request) {
        $premiumtype = new SettingsBonusType();
        $form = $this->createForm('AdminBundle\Form\SettingsBonusTypeType', $premiumtype);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($premiumtype);
            $em->flush($premiumtype);

            return $this->redirectToRoute('adminPremiumType_show', array('id' => $premiumtype->getId()));
        }

        return $this->render('Admin/settingsbonustype/new.html.twig', array(
                    'premiumtype' => $premiumtype,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $premiumtype = new SettingsBonusType();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsBonusTypeType(), $premiumtype);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $premiumtype->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $premiumtype->setCreateDate(new \DateTime('now'));
        $premiumtype->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $premiumtype->setLastUpdate(new \DateTime('now'));
        $premiumtype->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());        
        $em->persist($premiumtype);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing premiumtype entity.
     *
     */
    public function editAction(Request $request) {
        $premiumtype = new SettingsBonusType();
        $form = $this->createForm('AdminBundle\Form\SettingsBonusTypeType', $premiumtype);

        return $this->render('Admin/settingsbonustype/edit.html.twig', array(
                    'premiumtype' => $premiumtype,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retPremiumType = $em->getRepository('AdminBundle:SettingsBonusType')->find($json_data ['id']);
        $form = $this->createForm(new SettingsBonusTypeType(), $retPremiumType);
        $form->submit($json_data);
        $retPremiumType->setLastUpdate(new \DateTime('now'));
        $retPremiumType->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsBonusTypeAction(Request $request, SettingsBonusType $premiumtype) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsBonusType');
        $premiumtypes = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $premiumtype->getId())
                ->getQuery()
                ->getArrayResult();

        if ($premiumtypes)
            return new JsonResponse($premiumtypes[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a premiumtype entity.
     *
     */
    public function deleteAction(Request $request, $id) {

        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $premiumtype = $this->getDoctrine()->getRepository('AdminBundle:SettingsBonusType')->find($id);
        if ($premiumtype) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($premiumtype);
            $em->flush($premiumtype);
            /*****/
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }


    public function seqnoUpdateAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
        foreach ($json_data as $key => $value) {
            $SettingsBonusType = $em->getRepository('AdminBundle:SettingsBonusType')->find($json_data[$key]['id']);
            $SettingsBonusType->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
}



