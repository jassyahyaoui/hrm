<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_benefit_periodicity;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Hrm_settings_benefit_periodicity controller.
 *
 */
class hrm_settings_benefit_periodicityController extends Controller
{
    /**
     * Lists all hrm_settings_benefit_periodicity entities.
     *
     */
//    public function indexAction()
//    {
//        $em = $this->getDoctrine()->getManager();
//
//        $hrm_settings_benefit_periodicities = $em->getRepository('AdminBundle:hrm_settings_benefit_periodicity')->findAll();
//
//        return $this->render('hrm_settings_benefit_periodicity/index.html.twig', array(
//            'hrm_settings_benefit_periodicities' => $hrm_settings_benefit_periodicities,
//        ));
//    }

    public function layoutAction() {
        return $this->render('Admin/hrm_settings_benefit_periodicity/layout.html.twig');
    }

    /**
     * Lists all settingsstudytype entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/hrm_settings_benefit_periodicity/index.html.twig');
    }

    public function listAction() {
                $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $hrm_settings_benefit_periodicity = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_benefit_periodicity')->SettingsBenenfitPeriodicityByProvider($provider_id);
        return new JsonResponse(array(
            'hrm_settings_benefit_periodicity' => $hrm_settings_benefit_periodicity,
        ));
    }    
    
    
    /**
     * Creates a new hrm_settings_benefit_periodicity entity.
     *
     */
    public function newAction(Request $request)
    {
        
        $hrm_settings_benefit_periodicity = new Hrm_settings_benefit_periodicity();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_benefit_periodicityType', $hrm_settings_benefit_periodicity);
        $form->handleRequest($request);

        return $this->render('Admin/hrm_settings_benefit_periodicity/new.html.twig', array(
            'hrm_settings_benefit_periodicity' => $hrm_settings_benefit_periodicity,
            'form' => $form->createView(),
        ));        
     
    }
    
    public function saveAction(Request $request) {
        
        $hrm_settings_benefit_periodicity = new hrm_settings_benefit_periodicity();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_benefit_periodicityType', $hrm_settings_benefit_periodicity);
        $form->handleRequest($request);
        $form->submit($data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $hrm_settings_benefit_periodicity->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();

        //Add default value on create Action
        $hrm_settings_benefit_periodicity->setCreateDate(new \DateTime('now'));
        $hrm_settings_benefit_periodicity->setCreateUid($current_user_id);
        $hrm_settings_benefit_periodicity->setLastUpdateUid($current_user_id);
        $hrm_settings_benefit_periodicity->setLastUpdate(new \DateTime('now'));

        $em->persist($hrm_settings_benefit_periodicity);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));        
            
    }
    /**
     * Finds and displays a hrm_settings_benefit_periodicity entity.
     *
     */
    public function showAction(hrm_settings_benefit_periodicity $hrm_settings_benefit_periodicity)
    {
        $deleteForm = $this->createDeleteForm($hrm_settings_benefit_periodicity);

        return $this->render('Admin/hrm_settings_benefit_periodicity/show.html.twig', array(
            'hrm_settings_benefit_periodicity' => $hrm_settings_benefit_periodicity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_settings_benefit_periodicity entity.
     *
     */
    public function editAction(Request $request)
    {
        $hrm_settings_benefit_periodicity = new hrm_settings_benefit_periodicity();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_benefit_periodicityType', $hrm_settings_benefit_periodicity);
        $form->handleRequest($request);

        return $this->render('Admin/hrm_settings_benefit_periodicity/edit.html.twig', array(
            'hrm_settings_benefit_periodicity' => $hrm_settings_benefit_periodicity,
            'form' => $form->createView(),
        ));
        
//        
//        
//        
//        $deleteForm = $this->createDeleteForm($hrm_settings_benefit_periodicity);
//        $editForm = $this->createForm('AdminBundle\Form\hrm_settings_benefit_periodicityType', $hrm_settings_benefit_periodicity);
//        $editForm->handleRequest($request);
//
//        if ($editForm->isSubmitted() && $editForm->isValid()) {
//            $this->getDoctrine()->getManager()->flush();
//
//            return $this->redirectToRoute('periodicity_edit', array('id' => $hrm_settings_benefit_periodicity->getId()));
//        }
//
//        return $this->render('hrm_settings_benefit_periodicity/edit.html.twig', array(
//            'hrm_settings_benefit_periodicity' => $hrm_settings_benefit_periodicity,
//            'edit_form' => $editForm->createView(),
//            'delete_form' => $deleteForm->createView(),
//        ));
    }
    
    public function updateAction(Request $request) {
        
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $hrm_settings_benefit_periodicity = $em->getRepository('AdminBundle:hrm_settings_benefit_periodicity')->find($data ['id']);
        $form = $this->createForm('AdminBundle\Form\hrm_settings_benefit_periodicityType', $hrm_settings_benefit_periodicity);
        $form->submit($data);

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();

        //Add default value on create Action
        $hrm_settings_benefit_periodicity->setCreateDate(new \DateTime('now'));
        $hrm_settings_benefit_periodicity->setCreateUid($current_user_id);
        $hrm_settings_benefit_periodicity->setLastUpdateUid($current_user_id);
        $hrm_settings_benefit_periodicity->setLastUpdate(new \DateTime('now'));

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));        

    }

//    public function getSettingsBenefitPeriodicityAction(Request $request, hrm_settings_benefit_periodicity $hrm_settings_benefit_periodicity) {
//        $repo = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_benefit_periodicity');
//        $hrm_settings_benefit_periodicity = $repo->createQueryBuilder('r')
//                ->where('r.id = :id')
//                ->setParameter('id', $hrm_settings_benefit_periodicity->getId())
//                ->getQuery()
//                ->getArrayResult();
//
//        if ($hrm_settings_benefit_periodicity)
//            return new JsonResponse($hrm_settings_benefit_periodicity[0]);
//
//        return new JsonResponse($null);
//    }
    /**
     * Deletes a hrm_settings_benefit_periodicity entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $hrm_settings_benefit_periodicity = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_benefit_periodicity')->find($id);
        if ($hrm_settings_benefit_periodicity) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_settings_benefit_periodicity);
            $em->flush($hrm_settings_benefit_periodicity);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
        
        
        
//        $form = $this->createDeleteForm($hrm_settings_benefit_periodicity);
//        $form->handleRequest($request);
//
//        if ($form->isSubmitted() && $form->isValid()) {
//            $em = $this->getDoctrine()->getManager();
//            $em->remove($hrm_settings_benefit_periodicity);
//            $em->flush();
//        }
//
//        return $this->redirectToRoute('periodicity_index');
    }

    /**
     * Creates a form to delete a hrm_settings_benefit_periodicity entity.
     *
     * @param hrm_settings_benefit_periodicity $hrm_settings_benefit_periodicity The hrm_settings_benefit_periodicity entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_settings_benefit_periodicity $hrm_settings_benefit_periodicity)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('periodicity_delete', array('id' => $hrm_settings_benefit_periodicity->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
    
     public function seqnoUpdateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
//        dump($json_data[0]);
//        die();
        foreach ($json_data as $key => $value) {
           
                $hrm_settings_benefit_periodicity = $em->getRepository('AdminBundle:hrm_settings_benefit_periodicity')->find($json_data[$key]['id']);
                $hrm_settings_benefit_periodicity->setSeqno($key+1);
            
        }
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }   
}
