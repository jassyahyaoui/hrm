<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsAbsences;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\SettingsAbsencesType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Settingsabsence controller.
 *
 */
class SettingsAbsencesController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/settingsabsences/layout.html.twig');
    }

    /**
     * Lists all settingsabsence entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/settingsabsences/index.html.twig');
    }

    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsabsences = $this->getDoctrine()->getRepository('AdminBundle:SettingsAbsences')->SettingsAbsencesByProvider($provider_id);
        return new JsonResponse(array(
            'settingsabsences' => $settingsabsences,
        ));
    }

    /**
     * Creates a new settingsabsence entity.
     *
     */
    public function newAction(Request $request) {
        $settingsabsence = new SettingsAbsences();
        $form = $this->createForm('AdminBundle\Form\SettingsAbsencesType', $settingsabsence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsabsence);
            $em->flush($settingsabsence);

            return $this->redirectToRoute('adminSettingsAbsences_show', array('id' => $settingsabsence->getId()));
        }

        return $this->render('Admin/settingsabsences/new.html.twig', array(
                    'settingsabsence' => $settingsabsence,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $settingsabsence = new SettingsAbsences();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsAbsencesType(), $settingsabsence);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsabsence->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsabsence->setCreateDate(new \DateTime('now'));
        $settingsabsence->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsabsence->setLastUpdate(new \DateTime('now'));
        $settingsabsence->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId()); 
//        if (isset($json_data['seqno']))
//        $settingsabsence->setSeqno($json_data['seqno']);
        $em->persist($settingsabsence);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing settingsabsence entity.
     *
     */
    public function editAction(Request $request) {
        $settingsabsence = new SettingsAbsences();
        $form = $this->createForm('AdminBundle\Form\SettingsAbsencesType', $settingsabsence);
        return $this->render('Admin/settingsabsences/edit.html.twig', array(
                    'settingsabsence' => $settingsabsence,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retSettingsAbsence = $em->getRepository('AdminBundle:SettingsAbsences')->find($json_data ['id']);
        $form = $this->createForm(new SettingsAbsencesType(), $retSettingsAbsence);
        $form->submit($json_data);
//        $retSettingsAbsence->setSeqno($json_data['seqno']);
        $retSettingsAbsence->setLastUpdate(new \DateTime('now'));
        $retSettingsAbsence->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsAbsenceAction(Request $request, SettingsAbsences $settingsabsence) {
        $typesconge = $this->getDoctrine()->getRepository('AdminBundle:SettingsAbsences')->find($settingsabsence);

        if ($settingsabsence) {
            return new JsonResponse(array("id" => $settingsabsence->getId(),
                "type" => $settingsabsence->getType(),
                "NegativeBalance" => $settingsabsence->getNegativeBalance(),
                "numberOfDays" => $settingsabsence->getNumberOfDays(),
                "description" => $settingsabsence->getDescription()));
        }

        return new JsonResponse($null);
    }

    /**
     * Deletes a settingsabsence entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $settingsabsence = $this->getDoctrine()->getRepository('AdminBundle:SettingsAbsences')->find($id);
        if ($settingsabsence) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsabsence);
            $em->flush($settingsabsence);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }


    public function seqnoUpdateAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);

        foreach ($json_data as $key => $value) {
            $SettingsAbsences = $em->getRepository('AdminBundle:SettingsAbsences')->find($json_data[$key]['id']);
            $SettingsAbsences->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
}
