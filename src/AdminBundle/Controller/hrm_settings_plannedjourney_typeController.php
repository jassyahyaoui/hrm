<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_plannedjourney_type;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use AdminBundle\Form\hrm_settings_plannedjourney_typeType;

/**
 * Hrm_settings_plannedjourney_type controller.
 *
 */
class hrm_settings_plannedjourney_typeController extends Controller
{
        public function layoutAction() {
        return $this->render('Admin/hrm_settings_plannedjourney_type/layout.html.twig');
    }

    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $SettingsPlannedJourney = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_plannedjourney_type')->SettingsPlannedJourneyByProvider($provider_id);
        return new JsonResponse(array(
            'settingsPlannedJourney' => $SettingsPlannedJourney,
        ));
    }
    
     public function saveAction(Request $request) {
        $SettingsPlannedJourney = new Hrm_settings_plannedjourney_type();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_plannedjourney_typeType(), $SettingsPlannedJourney);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $SettingsPlannedJourney->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $SettingsPlannedJourney->setCreateDate(new \DateTime('now'));
        $SettingsPlannedJourney->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $SettingsPlannedJourney->setLastUpdate(new \DateTime('now'));
        $SettingsPlannedJourney->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($SettingsPlannedJourney);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    
        public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }
        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $settingsPlannedJourney = $em->getRepository('AdminBundle:hrm_settings_plannedjourney_type')->find($data ['id']);
        $form = $this->createForm('AdminBundle\Form\hrm_settings_plannedjourney_typeType', $settingsPlannedJourney);
        $form->submit($data);
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();
        $settingsPlannedJourney->setCreateDate(new \DateTime('now'));
        $settingsPlannedJourney->setCreateUid($current_user_id);
        $settingsPlannedJourney->setLastUpdateUid($current_user_id);
        $settingsPlannedJourney->setLastUpdate(new \DateTime('now'));
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
    
      public function seqnoUpdateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
        foreach ($json_data as $key => $value) {
            $plannedjourney = $em->getRepository('AdminBundle:hrm_settings_plannedjourney_type')->find($json_data[$key]['id']);
            $plannedjourney->setSeqno($key + 1);
        }
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
    
    /**
     * Lists all hrm_settings_plannedjourney_type entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hrm_settings_plannedjourney_types = $em->getRepository('AdminBundle:hrm_settings_plannedjourney_type')->findAll();

        return $this->render('Admin/hrm_settings_plannedjourney_type/index.html.twig', array(
            'hrm_settings_plannedjourney_types' => $hrm_settings_plannedjourney_types,
        ));
    }

    /**
     * Creates a new hrm_settings_plannedjourney_type entity.
     *
     */
    public function newAction()
    {
        $hrm_settings_plannedjourney_type = new Hrm_settings_plannedjourney_type();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_plannedjourney_typeType', $hrm_settings_plannedjourney_type);
        return $this->render('Admin/hrm_settings_plannedjourney_type/new.html.twig', array(
            'hrm_settings_plannedjourney_type' => $hrm_settings_plannedjourney_type,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_settings_plannedjourney_type entity.
     *
     */
    public function showAction(hrm_settings_plannedjourney_type $hrm_settings_plannedjourney_type)
    {
        $deleteForm = $this->createDeleteForm($hrm_settings_plannedjourney_type);

        return $this->render('Admin/hrm_settings_plannedjourney_type/show.html.twig', array(
            'hrm_settings_plannedjourney_type' => $hrm_settings_plannedjourney_type,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_settings_plannedjourney_type entity.
     *
     */
    public function editAction()
    {
        $hrm_settings_plannedjourney_type = new Hrm_settings_plannedjourney_type();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_plannedjourney_typeType', $hrm_settings_plannedjourney_type);
        return $this->render('Admin/hrm_settings_plannedjourney_type/edit.html.twig', array(
            'hrm_settings_plannedjourney_type' => $hrm_settings_plannedjourney_type,
            'form' => $form->createView(),
        ));
    }

     /**
     * Deletes a typetransport entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }
        $plannedjourney = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_plannedjourney_type')->find($id);
        if ($plannedjourney) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($plannedjourney);
            $em->flush($plannedjourney);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }
        return new JsonResponse(null);
    }

    /**
     * Creates a form to delete a hrm_settings_plannedjourney_type entity.
     *
     * @param hrm_settings_plannedjourney_type $hrm_settings_plannedjourney_type The hrm_settings_plannedjourney_type entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_settings_plannedjourney_type $hrm_settings_plannedjourney_type)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('hrm_settings_plannedjourney_type_delete', array('id' => $hrm_settings_plannedjourney_type->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
