<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_exp_transport;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\hrm_settings_exp_transportType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Settingsexptransport controller.
 *
 */
class hrm_settings_exp_transportController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/hrm_settings_exp_transport/layout.html.twig');
    }

    /**
     * Lists all hrm_settings_exp_transport entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/hrm_settings_exp_transport/index.html.twig');
    }

    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsExpTransports = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_transport')->hrm_settings_exp_transportByProvider($provider_id);
        return new JsonResponse(array(
            'settingsExpTransports' => $settingsExpTransports,
        ));
    }

    /**
     * Creates a new hrm_settings_exp_transport entity.
     *
     */
    public function newAction(Request $request) {
        $settingsExpTransport = new hrm_settings_exp_transport();
        $provider = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();

        $form = $this->createForm('AdminBundle\Form\hrm_settings_exp_transportType', $settingsExpTransport, array(
            'provider' => $provider
        ));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsExpTransport);
            $em->flush($settingsExpTransport);

            return $this->redirectToRoute('adminhrm_settings_exp_transport_show', array('id' => $settingsExpTransport->getId()));
        }

        return $this->render('Admin/hrm_settings_exp_transport/new.html.twig', array(
                    'settingsExpTransport' => $settingsExpTransport,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $settingsExpTransport = new hrm_settings_exp_transport();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_exp_transportType(), $settingsExpTransport);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        if (isset($json_data['tax_model'])) {
            $settingsExpTransport->setTaxModel($this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_tax_model')->find($json_data['tax_model']));
        }

        $settingsExpTransport->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsExpTransport->setCreateDate(new \DateTime('now'));
        $settingsExpTransport->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsExpTransport->setLastUpdate(new \DateTime('now'));
        $settingsExpTransport->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($settingsExpTransport);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing hrm_settings_exp_transport entity.
     *
     */
    public function editAction(Request $request) {
        $settingsExpTransport = new hrm_settings_exp_transport();
        $provider = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();

        $form = $this->createForm('AdminBundle\Form\hrm_settings_exp_transportType', $settingsExpTransport, array(
            'provider' => $provider,
        ));
        return $this->render('Admin/hrm_settings_exp_transport/edit.html.twig', array(
                    'settingsExpTransport' => $settingsExpTransport,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {

        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retsettingsExpTransport = $em->getRepository('AdminBundle:hrm_settings_exp_transport')->find($json_data ['id']);
        $form = $this->createForm(new hrm_settings_exp_transportType(), $retsettingsExpTransport);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        if (isset($json_data['tax_model']['id'])) {
            $retsettingsExpTransport->setTaxModel($this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_tax_model')->find($json_data['tax_model']['id']));
        }
        $retsettingsExpTransport->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $retsettingsExpTransport->setCreateDate(new \DateTime('now'));
        $retsettingsExpTransport->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $retsettingsExpTransport->setLastUpdate(new \DateTime('now'));
        $retsettingsExpTransport->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function get_hrm_settings_exp_transportAction(Request $request, hrm_settings_exp_transport $settingsExpTransport) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_transport');
        $settingsExpTransports = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $settingsExpTransport->getId())
                ->getQuery()
                ->getArrayResult();

        if ($settingsExpTransports)
            return new JsonResponse($settingsExpTransports[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a hrm_settings_exp_transport entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $settingsExpTransport = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_transport')->find($id);
        if ($settingsExpTransport) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsExpTransport);
            $em->flush($settingsExpTransport);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

    public function seqnoUpdateAction(Request $request) {
        $i = 0;
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
//        dump($json_data[0]);
//        die();
        foreach ($json_data as $key => $value) {
            if ($json_data[$key]['type'] == 'Transport') {
                $settingsExpTransport = $em->getRepository('AdminBundle:hrm_settings_exp_transport')->find($json_data[$key]['id']);
                $settingsExpTransport->setSeqno($key+1);
            }
        }
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

}
