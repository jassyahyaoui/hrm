<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsAdvVehicle;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\SettingsAdvVehicleType;

/**
 * Settingsadvvehicle controller.
 *
 */
class SettingsAdvVehicleController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/settingsadvvehicle/layout.html.twig');
    }

    /**
     * Lists all settingsAdvVehicle entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/settingsadvvehicle/index.html.twig');
    }

    // for angurlajs Rest
    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsAdvVehicles = $this->getDoctrine()->getRepository('AdminBundle:SettingsAdvVehicle')->SettingsAdvVehicleByProvider($provider_id);
        
//        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsAdvVehicle');
//        $settingsAdvVehicles = $repo->createQueryBuilder('r')
//                ->getQuery()
//                ->getArrayResult();
        return new JsonResponse(array(
            'settingsAdvVehicles' => $settingsAdvVehicles,
        ));
    }

    /**
     * Creates a new settingsAdvVehicle entity.
     *
     */
    public function newAction(Request $request) {
        $settingsAdvVehicle = new SettingsAdvVehicle();
        $form = $this->createForm('AdminBundle\Form\SettingsAdvVehicleType', $settingsAdvVehicle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsAdvVehicle);
            $em->flush($settingsAdvVehicle);

            return $this->redirectToRoute('adminSettingsAdvVehicle_show', array('id' => $settingsAdvVehicle->getId()));
        }

        return $this->render('Admin/settingsadvvehicle/new.html.twig', array(
                    'settingsAdvVehicle' => $settingsAdvVehicle,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $settingsAdvVehicle = new SettingsAdvVehicle();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsAdvVehicleType(), $settingsAdvVehicle);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsAdvVehicle->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsAdvVehicle->setCreateDate(new \DateTime('now'));
        $settingsAdvVehicle->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsAdvVehicle->setLastUpdate(new \DateTime('now'));
        $settingsAdvVehicle->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());        
        $em->persist($settingsAdvVehicle);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing settingsAdvVehicle entity.
     *
     */
    public function editAction(Request $request, SettingsAdvVehicle $settingsAdvVehicle) {
        $_NewsettingsAdvVehicle = new SettingsAdvVehicle();
        $form = $this->createForm('AdminBundle\Form\SettingsAdvVehicleType', $_NewsettingsAdvVehicle);

        return $this->render('Admin/settingsadvvehicle/edit.html.twig', array(
                    'settingsAdvVehicle' => $settingsAdvVehicle,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retsettingsAdvVehicle = $em->getRepository('AdminBundle:SettingsAdvVehicle')->find($json_data ['id']);
        $form = $this->createForm(new SettingsAdvVehicleType(), $retsettingsAdvVehicle);
        $form->submit($json_data);
        $retsettingsAdvVehicle->setLastUpdate(new \DateTime('now'));
        $retsettingsAdvVehicle->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsAdvVehicleAction(Request $request, SettingsAdvVehicle $settingsAdvVehicle) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsAdvVehicle');
        $settingsAdvVehicles = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $settingsAdvVehicle->getId())
                ->getQuery()
                ->getArrayResult();

        if ($settingsAdvVehicles)
            return new JsonResponse($settingsAdvVehicles[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a settingsAdvVehicle entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $settingsAdvVehicle = $this->getDoctrine()->getRepository('AdminBundle:SettingsAdvVehicle')->find($id);
        if ($settingsAdvVehicle) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsAdvVehicle);
            $em->flush($settingsAdvVehicle);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

}
