<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsConvention;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;


/**
 * Settingsconvention controller.
 *
 */
class SettingsConventionController extends Controller
{
    /**
     * Lists all settingsConvention entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsConventions = $this->getDoctrine()->getRepository('AdminBundle:SettingsConvention')->SettingsConventionByProvider($provider_id);
        return $this->render('Admin/settingsconvention/index.html.twig', array(
            'settingsConventions' => $settingsConventions,
        ));
    }

    
     // for angurlajs Rest
    public function listAction() {

        $repo = $this->getDoctrine()->getRepository('AdminBundle:Settingsconvention');
        $settingsConventions = $repo->createQueryBuilder('r')
                ->getQuery()
                ->getArrayResult();
        return new JsonResponse(array(
            'conventions' => $settingsConventions,
        ));
    }
    
    
       public function getSettingsConventionAction(Request $request, Settingsconvention $settingsConvention) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:Settingsconvention');
        $settingsConventions = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $settingsConvention->getId())
                ->getQuery()
                ->getArrayResult();

        if ($settingsConventions)
            return new JsonResponse($settingsConventions[0]);

        return new JsonResponse($null);
    }
    
    
    /**
     * Creates a new settingsConvention entity.
     *
     */
    public function newAction(Request $request)
    {
        $settingsConvention = new Settingsconvention();
        $form = $this->createForm('AdminBundle\Form\SettingsConventionType', $settingsConvention);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
                           $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsConvention->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
            $em->persist($settingsConvention);
            $em->flush();

            return $this->redirectToRoute('settingsconvention_show', array('id' => $settingsConvention->getId()));
        }

        return $this->render('Admin/settingsconvention/new.html.twig', array(
            'settingsConvention' => $settingsConvention,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a settingsConvention entity.
     *
     */
    public function showAction(SettingsConvention $settingsConvention)
    {
        $deleteForm = $this->createDeleteForm($settingsConvention);

        return $this->render('Admin/settingsconvention/show.html.twig', array(
            'settingsConvention' => $settingsConvention,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing settingsConvention entity.
     *
     */
    public function editAction(Request $request, SettingsConvention $settingsConvention)
    {
        $deleteForm = $this->createDeleteForm($settingsConvention);
        $editForm = $this->createForm('AdminBundle\Form\SettingsConventionType', $settingsConvention);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('settingsconvention_edit', array('id' => $settingsConvention->getId()));
        }

        return $this->render('Admin/settingsconvention/edit.html.twig', array(
            'settingsConvention' => $settingsConvention,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a settingsConvention entity.
     *
     */
    public function deleteAction(Request $request, SettingsConvention $settingsConvention)
    {
        $form = $this->createDeleteForm($settingsConvention);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsConvention);
            $em->flush();
        }

        return $this->redirectToRoute('settingsconvention_index');
    }

    /**
     * Creates a form to delete a settingsConvention entity.
     *
     * @param SettingsConvention $settingsConvention The settingsConvention entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SettingsConvention $settingsConvention)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('settingsconvention_delete', array('id' => $settingsConvention->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
