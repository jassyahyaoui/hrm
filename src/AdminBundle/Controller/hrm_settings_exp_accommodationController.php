<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_exp_accommodation;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\hrm_settings_exp_accommodationType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Settingsexpaccommodation controller.
 *
 */
class hrm_settings_exp_accommodationController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/hrm_settings_exp_accommodation/layout.html.twig');
    }

    /**
     * Lists all settingsExpAccommodation entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/hrm_settings_exp_accommodation/index.html.twig');
    }

    // for angurlajs Rest
    public function listAction() {
                $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsExpAccommodations = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_accommodation')->hrm_settings_exp_accommodationByProvider($provider_id);
        return new JsonResponse(array(
            'settingsExpAccommodations' => $settingsExpAccommodations,
        ));
    }

    /**
     * Creates a new settingsExpAccommodation entity.
     *
     */
    public function newAction(Request $request) {
        $settingsExpAccommodation = new hrm_settings_exp_accommodation();
        $provider = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();

        $form = $this->createForm('AdminBundle\Form\hrm_settings_exp_accommodationType', $settingsExpAccommodation, array(
            'provider' => $provider
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsExpAccommodation);
            $em->flush($settingsExpAccommodation);

            return $this->redirectToRoute('adminhrm_settings_exp_accommodation_show', array('id' => $settingsExpAccommodation->getId()));
        }

        return $this->render('Admin/hrm_settings_exp_accommodation/new.html.twig', array(
                    'settingsExpAccommodation' => $settingsExpAccommodation,
                    'form' => $form->createView(),
        ));
    }



    public function saveAction(Request $request) {
        $settingsExpAccommodation = new hrm_settings_exp_accommodation();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_exp_accommodationType(), $settingsExpAccommodation);
        $form->handleRequest($request);
        $form->submit($json_data); 
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        if(isset($json_data['tax_model']))
        {
            $settingsExpAccommodation->setTaxModel($this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_tax_model')->find($json_data['tax_model']));        
        }
        $settingsExpAccommodation->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsExpAccommodation->setCreateDate(new \DateTime('now'));
        $settingsExpAccommodation->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsExpAccommodation->setLastUpdate(new \DateTime('now'));
        $settingsExpAccommodation->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());        
        $em->persist($settingsExpAccommodation);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing settingsExpAccommodation entity.
     *
     */
    public function editAction(Request $request) {
        $settingsExpAccommodation = new hrm_settings_exp_accommodation();
        $provider = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();

        $form = $this->createForm('AdminBundle\Form\hrm_settings_exp_accommodationType', $settingsExpAccommodation, array(
            'provider' => $provider
        ));

        return $this->render('Admin/hrm_settings_exp_accommodation/edit.html.twig', array(
                    'settingsExpAccommodation' => $settingsExpAccommodation,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {

        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retsettingsExpAccommodation = $em->getRepository('AdminBundle:hrm_settings_exp_accommodation')->find($json_data ['id']);
        $form = $this->createForm(new hrm_settings_exp_accommodationType(), $retsettingsExpAccommodation);
        $form->submit($json_data);

        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        if(isset($json_data['tax_model']['id']))
        {
            $retsettingsExpAccommodation->setTaxModel($this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_tax_model')->find($json_data['tax_model']['id']));
        }
        $retsettingsExpAccommodation->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $retsettingsExpAccommodation->setCreateDate(new \DateTime('now'));
        $retsettingsExpAccommodation->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $retsettingsExpAccommodation->setLastUpdate(new \DateTime('now'));
        $retsettingsExpAccommodation->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function get_hrm_settings_exp_accommodationAction(Request $request, hrm_settings_exp_accommodation $settingsExpAccommodation) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_accommodation');
        $settingsExpAccommodations = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $settingsExpAccommodation->getId())
                ->getQuery()
                ->getArrayResult();

        if ($settingsExpAccommodations)
            return new JsonResponse($settingsExpAccommodations[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a settingsExpAccommodation entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $settingsExpAccommodation = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_exp_accommodation')->find($id);
        if ($settingsExpAccommodation) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsExpAccommodation);
            $em->flush($settingsExpAccommodation);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

    public function seqnoUpdateAction(Request $request) {
        $i = 0;
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
//        dump($json_data[0]);
//        die();
        foreach ($json_data as $key => $value) {
            if ($json_data[$key]['type'] == 'Hebergement') {
                $settingsExpAccommodation = $em->getRepository('AdminBundle:hrm_settings_exp_accommodation')->find($json_data[$key]['id']);
                $settingsExpAccommodation->setSeqno($key+1);
            }
        }
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

}
