<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_tax;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\hrm_settings_taxType;

/**
 * Settingstax controller.
 *
 */
class hrm_settings_taxController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/hrm_settings_tax/layout.html.twig');
    }

    /**
     * Lists all hrm_settings_tax entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/hrm_settings_tax/index.html.twig');
    }

    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsTaxs = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_tax')->hrm_settings_taxByProvider($provider_id);
        return new JsonResponse(array(
            'hrm_settings_taxs' => $settingsTaxs,
        ));
    }

    /**
     * Creates a new hrm_settings_tax entity.
     *
     */
    public function newAction(Request $request) {
        $settingsTax = new hrm_settings_tax();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_taxType', $settingsTax);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsTax);
            $em->flush($settingsTax);

            return $this->redirectToRoute('adminhrm_settings_tax_show', array('id' => $settingsTax->getId()));
        }

        return $this->render('Admin/hrm_settings_tax/new.html.twig', array(
                    'hrm_settings_tax' => $settingsTax,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $settingsTax = new hrm_settings_tax();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_taxType(), $settingsTax);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsTax->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsTax->setCreateDate(new \DateTime('now'));
        $settingsTax->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsTax->setLastUpdate(new \DateTime('now'));
        $settingsTax->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());        
        $em->persist($settingsTax);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing hrm_settings_tax entity.
     *
     */
    public function editAction(Request $request) {
        $settingsTax = new hrm_settings_tax();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_taxType', $settingsTax);

        return $this->render('Admin/hrm_settings_tax/edit.html.twig', array(
                    'hrm_settings_tax' => $settingsTax,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {

        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $rethrm_settings_tax = $em->getRepository('AdminBundle:hrm_settings_tax')->find($json_data ['id']);
        $form = $this->createForm(new hrm_settings_taxType(), $rethrm_settings_tax);
        $form->submit($json_data);
        $rethrm_settings_tax->setLastUpdate(new \DateTime('now'));
        $rethrm_settings_tax->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function get_hrm_settings_taxAction(Request $request, hrm_settings_tax $settingsTax) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_tax');
        $settingsTaxs = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $settingsTax->getId())
                ->getQuery()
                ->getArrayResult();

        if ($settingsTaxs)
            return new JsonResponse($settingsTaxs[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a hrm_settings_tax entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $settingsTax = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_tax')->find($id);
        if ($settingsTax) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsTax);
            $em->flush($settingsTax);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

}
