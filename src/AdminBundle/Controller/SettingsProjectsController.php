<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsProjects;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\SettingsProjectsType;

/**
 * SettingsProjects controller.
 *
 */
class SettingsProjectsController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/settingsprojects/layout.html.twig');
    }

    /**
     * Lists all projet entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/settingsprojects/index.html.twig');
    }

    public function listAction() {
                $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $projets = $this->getDoctrine()->getRepository('AdminBundle:SettingsProjects')->SettingsProjectsByProvider($provider_id);
        return new JsonResponse(array(
            'projets' => $projets,
        ));
    }

    /**
     * Creates a new projet entity.
     *
     */
    public function newAction(Request $request) {
        $projet = new SettingsProjects();
        $form = $this->createForm('AdminBundle\Form\SettingsProjectsType', $projet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projet);
            $em->flush($projet);

            return $this->redirectToRoute('adminProjects_show', array('id' => $projet->getId()));
        }

        return $this->render('Admin/settingsprojects/new.html.twig', array(
                    'projet' => $projet,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $projet = new SettingsProjects();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        // check post data true
        $json_data = json_decode($this->get('request')->getContent(), true);

        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsProjectsType(), $projet);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $projet->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $projet->setCreateDate(new \DateTime('now'));
        $projet->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $projet->setLastUpdate(new \DateTime('now'));
        $projet->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());        
        $em->persist($projet);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing projet entity.
     *
     */
    public function editAction(Request $request, SettingsProjects $projet) {
        $_Newprojet = new SettingsProjects();
        $form = $this->createForm('AdminBundle\Form\SettingsProjectsType', $_Newprojet);

        return $this->render('Admin/settingsprojects/edit.html.twig', array(
                    'projet' => $projet,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {

        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        // check post data true
        $json_data = json_decode($this->get('request')->getContent(), true);

        $em = $this->getDoctrine()->getEntityManager();
        $retProjet = $em->getRepository('AdminBundle:SettingsProjects')->find($json_data ['id']);
        $form = $this->createForm(new SettingsProjectsType(), $retProjet);
        $form->submit($json_data);
        $retProjet->setLastUpdate(new \DateTime('now'));
        $retProjet->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsProjectsAction(Request $request, SettingsProjects $projet) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsProjects');
        $projet = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $projet->getId())
                ->getQuery()
                ->getArrayResult();

        if ($projet)
            return new JsonResponse($projet[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a projet entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $projet = $this->getDoctrine()->getRepository('AdminBundle:SettingsProjects')->find($id);
        if ($projet) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($projet);
            $em->flush($projet);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

}
