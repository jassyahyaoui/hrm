<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_tax;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AdminBundle\Form\hrm_settings_taxType;
use AdminBundle\Entity\hrm_settings_provider_menu;
/**
 * hrm_settings_menu controller.
 *
 */
class hrm_settings_menuController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/hrm_settings_menu/layout.html.twig');
    }
    
    public function indexAction() {
        return $this->render('Admin/hrm_settings_menu/index.html.twig');
    }
    
    
    public function listProvidersAction() {
        $providers_list = $this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->findAllProviders();
        return array(
            'providers' => $providers_list,
        );
    }
    
    public function editAction() {
        $menu_list = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_menu')->findAll();
        return $this->render('Admin/hrm_settings_menu/edit.html.twig', array('menus'=>$menu_list));
    }

    public function getAllMenusAction() {
       $menu_list =  $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_menu')->findBy(array(), array('seqno' => 'ASC'));
       return array('result'=>$menu_list);
    }
    
    public function getMenuShowingByProviderAction($provider_id) {
        $menu_list = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_provider_menu')->findAllMenuShowingByProvidres($provider_id);
        return array('providre'=>array('uid'=>$provider_id,'menus'=>$menu_list));
    }  
    
    public function saveMenuShowingByProviderAction(Request $request) {
        
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        
        if ($this->get('request')->getMethod() != 'POST') {
            return array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported'));
        }
        
        
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $provider_obj = $this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($json_data['provider_id']);
        unset($json_data['provider_id']);
        // delete all setting menu before save a new settings
        $this->deleteMenuSettingsByProvider($provider_obj->getId());
        
        foreach ($json_data as $key => $value) {
            if($value == true)
            {
                $settings_provider_menu = new hrm_settings_provider_menu();
                $id_menu = substr( $key, strrpos( $key, '_' )+1 );
                $menu_list = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_menu')->find($id_menu);  
                $settings_provider_menu->setSettingMenu($menu_list);
                $settings_provider_menu->setSettingsProvider($provider_obj);
                $settings_provider_menu->setDisplayMenu($value);
                $settings_provider_menu->setCreateDate(new \DateTime('now'));
                $settings_provider_menu->setLastUpdate(new \DateTime('now'));
                $settings_provider_menu->setCreateUid($empData->getId()); 
                $em->persist($settings_provider_menu);
                $settings_provider_menu->setSeqno($settings_provider_menu->getId());                
            }
        }
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    private function deleteMenuSettingsByProvider($provider_id) {
        $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_provider_menu')->removeAllMenus($provider_id);
    }

    public function seqnoUpdateAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
//        dump($json_data);
//        die();
        foreach ($json_data as $key => $value) {
            $SettingsBonus = $em->getRepository('AdminBundle:hrm_settings_menu')->find($json_data[$key]['id']);
            $SettingsBonus->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }    
}
