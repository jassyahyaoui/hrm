<?php

namespace AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
    public function layoutAction()
    {
        return $this->render('AdminBundle:Default:layout.html.twig');
    }
}
