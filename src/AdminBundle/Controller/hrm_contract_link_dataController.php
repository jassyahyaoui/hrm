<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_contract_link_data;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Hrm_contract_link_datum controller.
 *
 */
class hrm_contract_link_dataController extends Controller
{
    public function layoutAction()
    {
        return $this->render('Admin/contract_link/hrm_contract_link_data/layout.html.twig');
    }

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hrm_contract_link_datas = $em->getRepository('AdminBundle:hrm_contract_link_data')->findAll();

        return $this->render('Admin/contract_link/hrm_contract_link_data/index.html.twig', array(
            'hrm_contract_link_datas' => $hrm_contract_link_datas,
        ));
    }

    /**
     * Creates a new link_data entity.
     *
     */

    public function newAction(Request $request)
    {
        $link_data = new hrm_contract_link_data();

        $providerId = $this->container->get('settingsbundle.preference.service')->getProviderId();
        $form = $this->createForm('AdminBundle\Form\hrm_contract_link_dataType', $link_data, array(
            'provider' => $providerId
        ));
        $form->handleRequest($request);

        return $this->render('Admin/contract_link/hrm_contract_link_data/new.html.twig', array(
            'link_data' => $link_data,
            'form' => $form->createView(),
        ));
    }


    public function editAction(Request $request)
    {
        $link_data = new hrm_contract_link_data();

        $providerId = $this->container->get('settingsbundle.preference.service')->getProviderId();
        $form = $this->createForm('AdminBundle\Form\hrm_contract_link_dataType', $link_data, array(
            'provider' => $providerId
        ));
        $form->handleRequest($request);

        return $this->render('Admin/contract_link/hrm_contract_link_data/edit.html.twig', array(
            'link_data' => $link_data,
            'form' => $form->createView(),
        ));
    }


    /**
     * Finds and displays a link_data entity.
     *
     */
    public function showAction(hrm_contract_link_data $link_data)
    {
        $deleteForm = $this->createDeleteForm($link_data);

        return $this->render('Admin/contract_link/hrm_contract_link_data/show.html.twig', array(
            'link_data' => $link_data,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function allAction()
    {
        $em = $this->getDoctrine()->getManager();

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $companyData = $empData->getCompany();
        $providerId = $companyData->getProvider();

        $result = $em->getRepository('AdminBundle:hrm_contract_link_data')->FindContractLinkData($providerId);

        return new JsonResponse(array(
            'result' => $result,
        ));
    }



    public function saveAction(Request $request)
    {
        $link_data = new hrm_contract_link_data();
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm('AdminBundle\Form\hrm_contract_link_dataType', $link_data);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $empData->getCompany()->getProvider()->getId();
        $link_data->setHrmcontractLinkCategory($this->getDoctrine()->getRepository('AdminBundle:hrm_contract_link_category')->find($json_data['hrm_contract_link_category']));
        $link_data->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $link_data->setCreateDate(new \DateTime('now'));
        $link_data->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $link_data->setLastUpdate(new \DateTime('now'));
        $link_data->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($link_data);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    public function updateAction(Request $request)
    {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $link_data = $em->getRepository('AdminBundle:hrm_contract_link_data')->find($json_data ['id']);
        $form = $this->createForm('AdminBundle\Form\hrm_contract_link_dataType', $link_data);
        $form->submit($json_data);
        $link_data->setHrmcontractLinkCategory($this->getDoctrine()->getRepository('AdminBundle:hrm_contract_link_category')->find($json_data['hrm_contract_link_category']['id']));
        $link_data->setLastUpdate(new \DateTime('now'));
        $link_data->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $link_data = $this->getDoctrine()->getRepository('AdminBundle:hrm_contract_link_data')->find($id);
        if ($link_data) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($link_data);
            $em->flush($link_data);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }


    public function seqnoUpdateAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
        foreach ($json_data as $key => $value) {
            $SettingsBonus = $em->getRepository('AdminBundle:hrm_contract_link_data')->find($json_data[$key]['id']);
            $SettingsBonus->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function ShowByCategoryAction()
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $companyData = $empData->getCompany();
        $providerId = $companyData->getProvider();

        $result = $em->getRepository('AdminBundle:hrm_contract_link_data')->FindByCategory($providerId);

        return new JsonResponse(array(
            'result' => $result,
        ));
    }
}
