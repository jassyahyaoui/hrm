<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\SettingsGiftContext;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\SettingsGiftContextType;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Settingsgiftcontext controller.
 *
 */
class SettingsGiftContextController extends Controller {

    public function layoutAction() {
        return $this->render('Admin/settingsgiftcontext/layout.html.twig');
    }

    /**
     * Lists all settingsgiftcontext entities.
     *
     */
    public function indexAction() {
        return $this->render('Admin/settingsgiftcontext/index.html.twig');
    }

    public function listAction() {
                $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsgiftcontexts = $this->getDoctrine()->getRepository('AdminBundle:SettingsGiftContext')->SettingsGiftContextByProvider($provider_id);
        return new JsonResponse(array(
            'settingsgiftcontexts' => $settingsgiftcontexts,
        ));
    }

    /**
     * Creates a new settingsgiftcontext entity.
     *
     */
    public function newAction(Request $request) {
        $settingsgiftcontext = new SettingsGiftContext();
        $form = $this->createForm('AdminBundle\Form\SettingsGiftContextType', $settingsgiftcontext);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($settingsgiftcontext);
            $em->flush($settingsgiftcontext);

            return $this->redirectToRoute('adminSettingsGiftContext_show', array('id' => $settingsgiftcontext->getId()));
        }

        return $this->render('Admin/settingsgiftcontext/new.html.twig', array(
                    'settingsgiftcontext' => $settingsgiftcontext,
                    'form' => $form->createView(),
        ));
    }

    public function saveAction(Request $request) {
        $settingsgiftcontext = new SettingsGiftContext();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new SettingsGiftContextType(), $settingsgiftcontext);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsgiftcontext->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsgiftcontext->setCreateDate(new \DateTime('now'));
        $settingsgiftcontext->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsgiftcontext->setLastUpdate(new \DateTime('now'));
        $settingsgiftcontext->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());        
        $em->persist($settingsgiftcontext);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }

    /**
     * Displays a form to edit an existing settingsgiftcontext entity.
     *
     */
    public function editAction(Request $request, SettingsGiftContext $settingsgiftcontext) {
        $_Newsettingsgiftcontext = new SettingsGiftContext();
        $form = $this->createForm('AdminBundle\Form\SettingsGiftContextType', $_Newsettingsgiftcontext);

        return $this->render('Admin/settingsgiftcontext/edit.html.twig', array(
                    'settingsgiftcontext' => $settingsgiftcontext,
                    'form' => $form->createView(),
        ));
    }

    public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $retSettingsGiftContext = $em->getRepository('AdminBundle:SettingsGiftContext')->find($json_data ['id']);
        $form = $this->createForm(new SettingsGiftContextType(), $retSettingsGiftContext);
        $form->submit($json_data);
        $retSettingsGiftContext->setLastUpdate(new \DateTime('now'));
        $retSettingsGiftContext->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }

    public function getSettingsGiftContextAction(Request $request, SettingsGiftContext $settingsgiftcontext) {
        $repo = $this->getDoctrine()->getRepository('AdminBundle:SettingsGiftContext');
        $settingsgiftcontexts = $repo->createQueryBuilder('r')
                ->where('r.id = :id')
                ->setParameter('id', $settingsgiftcontext->getId())
                ->getQuery()
                ->getArrayResult();

        if ($settingsgiftcontexts)
            return new JsonResponse($settingsgiftcontexts[0]);

        return new JsonResponse($null);
    }

    /**
     * Deletes a settingsgiftcontext entity.
     *
     */
    public function deleteAction(Request $request, $id) {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $settingsgiftcontext = $this->getDoctrine()->getRepository('AdminBundle:SettingsGiftContext')->find($id);
        if ($settingsgiftcontext) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsgiftcontext);
            $em->flush($settingsgiftcontext);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

}
