<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\settings_paydayvar_nature;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Settings_paydayvar_nature controller.
 *
 */
class settings_paydayvar_natureController extends Controller
{
    public function layoutAction(){

        return $this->render('Admin/settings_paydayvar_nature/layout.html.twig');
    }
    /**
     * Lists all settings_paydayvar_nature entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $settings_paydayvar_natures = $em->getRepository('AdminBundle:settings_paydayvar_nature')->findAll();

        return $this->render('Admin/settings_paydayvar_nature/index.html.twig', array(
            'settings_paydayvar_natures' => $settings_paydayvar_natures,
        ));
    }

    /**
     * Creates a new settings_paydayvar_nature entity.
     *
     */
    public function newAction(Request $request)
    {
        $settings_paydayvar_nature = new Settings_paydayvar_nature();
        $form = $this->createForm('AdminBundle\Form\settings_paydayvar_natureType', $settings_paydayvar_nature);
        $form->handleRequest($request);

        return $this->render('Admin/settings_paydayvar_nature/new.html.twig', array(
            'settings_paydayvar_nature' => $settings_paydayvar_nature,
            'form' => $form->createView(),
        ));
    }
    public function editAction(Request $request)
    {
        $settings_paydayvar_nature = new Settings_paydayvar_nature();
        $form = $this->createForm('AdminBundle\Form\settings_paydayvar_natureType', $settings_paydayvar_nature);
        $form->handleRequest($request);

        return $this->render('Admin/settings_paydayvar_nature/edit.html.twig', array(
            'settings_paydayvar_nature' => $settings_paydayvar_nature,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a settings_paydayvar_nature entity.
     *
     */
    public function showAction(settings_paydayvar_nature $settings_paydayvar_nature)
    {
        $deleteForm = $this->createDeleteForm($settings_paydayvar_nature);

        return $this->render('Admin/settings_paydayvar_nature/show.html.twig', array(
            'settings_paydayvar_nature' => $settings_paydayvar_nature,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $settings_paydayvar_nature = $this->getDoctrine()->getRepository('AdminBundle:settings_paydayvar_nature')->find($id);
        if ($settings_paydayvar_nature) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settings_paydayvar_nature);
            $em->flush($settings_paydayvar_nature);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

    public function listAction()
    {
        $providerId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $paydayvar_nature = $this->getDoctrine()->getRepository('AdminBundle:settings_paydayvar_nature')->dayvarNatureByProvider($providerId);
        return new JsonResponse(array(
            'payDayVarNature' => $paydayvar_nature,
        ));
    }

    public function saveAction(Request $request)
    {
        $settings_paydayvar_nature = new Settings_paydayvar_nature();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm('AdminBundle\Form\settings_paydayvar_natureType', $settings_paydayvar_nature);
        $form->handleRequest($request);
        $form->submit($data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settings_paydayvar_nature->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();

        //Add default value on create Action
        $settings_paydayvar_nature->setCreateDate(new \DateTime('now'));
        $settings_paydayvar_nature->setCreateUid($current_user_id);
        $settings_paydayvar_nature->setLastUpdateUid($current_user_id);
        $settings_paydayvar_nature->setLastUpdate(new \DateTime('now'));

        $em->persist($settings_paydayvar_nature);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }


    public function updateAction(Request $request)
    {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $settings_paydayvar_nature = $em->getRepository('AdminBundle:settings_paydayvar_nature')->find($data ['id']);
        $form = $this->createForm('AdminBundle\Form\settings_paydayvar_natureType', $settings_paydayvar_nature);
        $form->submit($data);

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();

        //Add default value on create Action
        $settings_paydayvar_nature->setCreateDate(new \DateTime('now'));
        $settings_paydayvar_nature->setCreateUid($current_user_id);
        $settings_paydayvar_nature->setLastUpdateUid($current_user_id);
        $settings_paydayvar_nature->setLastUpdate(new \DateTime('now'));

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }


    public function seqnoUpdateAction(Request $request) {

        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
        foreach ($json_data as $key => $value) {
            $settings_paydayvar_nature = $em->getRepository('AdminBundle:settings_paydayvar_nature')->find($json_data[$key]['id']);
            $settings_paydayvar_nature->setSeqno($key + 1);
        }

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
}
