<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_job;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AdminBundle\Form\hrm_settings_jobType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
/**
 * Hrm_settings_job controller.
 *
 */
class hrm_settings_jobController extends Controller
{
    public function layoutAction() {
        return $this->render('Admin/hrm_settings_job/layout.html.twig');
    }

    public function listAction() {
        $provider_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsJob= $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_job')->SettingsJobByProvider($provider_id);
        return new JsonResponse(array(
            'settingsJob' => $settingsJob,
        ));
    }
    
     public function saveAction(Request $request) {
         
        $settingsJob = new hrm_settings_job();
        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }
        $json_data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(new hrm_settings_jobType(), $settingsJob);
        $form->handleRequest($request);
        $form->submit($json_data);
        $user_provide_id = $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getProvider()->getId();
        $settingsJob->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));
        $settingsJob->setCreateDate(new \DateTime('now'));
        $settingsJob->setCreateUid($this->get('security.context')->getToken()->getUser()->getId());
        $settingsJob->setLastUpdate(new \DateTime('now'));
        $settingsJob->setLastUpdateUid($this->get('security.context')->getToken()->getUser()->getId());
        $em->persist($settingsJob);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }
    
        public function updateAction(Request $request) {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }
        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $settingsJob = $em->getRepository('AdminBundle:hrm_settings_job')->find($data ['id']);
        $form = $this->createForm('AdminBundle\Form\hrm_settings_jobType', $settingsJob);
        $form->submit($data);
        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();
        $settingsJob->setCreateDate(new \DateTime('now'));
        $settingsJob->setCreateUid($current_user_id);
        $settingsJob->setLastUpdateUid($current_user_id);
        $settingsJob->setLastUpdate(new \DateTime('now'));
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
    
      public function seqnoUpdateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
        foreach ($json_data as $key => $value) {
            $settingsJob = $em->getRepository('AdminBundle:hrm_settings_job')->find($json_data[$key]['id']);
            $settingsJob->setSeqno($key + 1);
        }
        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
    
     /**
     * Deletes a typetransport entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }
        $settingsJob = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_job')->find($id);
        if ($settingsJob) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($settingsJob);
            $em->flush($settingsJob);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }
        return new JsonResponse(null);
    }
    
    /**
     * Lists all hrm_settings_job entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hrm_settings_jobs = $em->getRepository('AdminBundle:hrm_settings_job')->findAll();

        return $this->render('hrm_settings_job/index.html.twig', array(
            'hrm_settings_jobs' => $hrm_settings_jobs,
        ));
    }

    /**
     * Creates a new hrm_settings_job entity.
     *
     */
    public function newAction()
    {
        $hrm_settings_job = new hrm_settings_job();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_jobType', $hrm_settings_job);
        return $this->render('hrm_settings_job/new.html.twig', array(
            'hrm_settings_job' => $hrm_settings_job,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_settings_job entity.
     *
     */
    public function showAction(hrm_settings_job $hrm_settings_job)
    {
        $deleteForm = $this->createDeleteForm($hrm_settings_job);

        return $this->render('hrm_settings_job/show.html.twig', array(
            'hrm_settings_job' => $hrm_settings_job,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_settings_job entity.
     *
     */
    public function editAction()
    {
        $hrm_settings_job = new hrm_settings_job();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_jobType', $hrm_settings_job);
        return $this->render('hrm_settings_job/edit.html.twig', array(
            'hrm_settings_job' => $hrm_settings_job,
            'edit_form' => $hrm_settings_job->createView(),
        ));
    }

    /**
     * Creates a form to delete a hrm_settings_job entity.
     *
     * @param hrm_settings_job $hrm_settings_job The hrm_settings_job entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_settings_job $hrm_settings_job)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('hrm_settings_job_delete', array('id' => $hrm_settings_job->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
