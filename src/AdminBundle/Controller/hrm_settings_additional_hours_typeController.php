<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\hrm_settings_additional_hours_type;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Hrm_settings_additional_hours_type controller.
 *
 */
class hrm_settings_additional_hours_typeController extends Controller
{
    public function layoutAction(){
        return $this->render('Admin/extra_hours_type/layout.html.twig');
    }

    /**
     * Lists all hrm_settings_additional_hours_type entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hrm_settings_additional_hours_types = $em->getRepository('AdminBundle:hrm_settings_additional_hours_type')->findAll();

        return $this->render('Admin/extra_hours_type/index.html.twig', array(
            'hrm_settings_additional_hours_types' => $hrm_settings_additional_hours_types,
        ));
    }

    /**
     * Creates a new hrm_settings_additional_hours_type entity.
     *
     */
    public function newAction(Request $request)
    {
        $hrm_settings_additional_hours_type = new hrm_settings_additional_hours_type();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_additional_hours_typeType', $hrm_settings_additional_hours_type);
        $form->handleRequest($request);

        return $this->render('Admin/extra_hours_type/new.html.twig', array(
            'hrm_settings_additional_hours_type' => $hrm_settings_additional_hours_type,
            'form' => $form->createView(),
        ));
    }
    public function editAction(Request $request)
    {
        $hrm_settings_additional_hours_type = new hrm_settings_additional_hours_type();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_additional_hours_typeType', $hrm_settings_additional_hours_type);
        $form->handleRequest($request);

        return $this->render('Admin/extra_hours_type/edit.html.twig', array(
            'hrm_settings_additional_hours_type' => $hrm_settings_additional_hours_type,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_settings_additional_hours_type entity.
     *
     */
    public function showAction(hrm_settings_additional_hours_type $hrm_settings_additional_hours_type)
    {
        $deleteForm = $this->createDeleteForm($hrm_settings_additional_hours_type);

        return $this->render('Admin/extra_hours_type/show.html.twig', array(
            'hrm_settings_additional_hours_type' => $hrm_settings_additional_hours_type,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    public function deleteAction(Request $request, $id)
    {
        if ($this->get('request')->getMethod() != 'DELETE') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only DELETE methods supported')));
        }

        $hrm_settings_additional_hours_type = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_additional_hours_type')->find($id);
        if ($hrm_settings_additional_hours_type) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_settings_additional_hours_type);
            $em->flush($hrm_settings_additional_hours_type);
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément supprimé avec succès")));
        }

        return new JsonResponse(null);
    }

    public function listAction()
    {
                $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $companyData = $empData->getCompany();
        $providerId = $companyData->getProvider();
        $extra_hours_type = $this->getDoctrine()->getRepository('AdminBundle:hrm_settings_additional_hours_type')->FindTypesByProvider($providerId);
        return new JsonResponse(array(
            'extraHoursType' => $extra_hours_type,
        ));
    }

    public function saveAction(Request $request)
    {
        $hrm_settings_additional_hours_type = new hrm_settings_additional_hours_type();

        if ($this->get('request')->getMethod() != 'POST') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only POST methods supported')));
        }

        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm('AdminBundle\Form\hrm_settings_additional_hours_typeType', $hrm_settings_additional_hours_type);
        $form->handleRequest($request);
        $form->submit($data);

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $companyData = $empData->getCompany();
        $user_provide_id = $companyData->getProvider();
        $hrm_settings_additional_hours_type->setProvider($this->getDoctrine()->getRepository('AdminBundle:SettingsProvider')->find($user_provide_id));

        $current_user_id = $empData->getId();

        //Add default value on create Action
        $hrm_settings_additional_hours_type->setCreateDate(new \DateTime('now'));
        $hrm_settings_additional_hours_type->setCreateUid($current_user_id);
        $hrm_settings_additional_hours_type->setLastUpdateUid($current_user_id);
        $hrm_settings_additional_hours_type->setLastUpdate(new \DateTime('now'));

        $em->persist($hrm_settings_additional_hours_type);
        $em->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément Ajouté avec succès")));
    }


    public function updateAction(Request $request)
    {
        if ($this->get('request')->getMethod() != 'PUT') {
            return new JsonResponse(array('http_code' => 405, 'message' => array('error' => 'Method not allowed', 'debug' => 'Only PUT methods supported')));
        }

        $data = json_decode($this->get('request')->getContent(), true);
        $em = $this->getDoctrine()->getEntityManager();
        $hrm_settings_additional_hours_type = $em->getRepository('AdminBundle:hrm_settings_additional_hours_type')->find($data ['id']);
        $form = $this->createForm('AdminBundle\Form\hrm_settings_additional_hours_typeType', $hrm_settings_additional_hours_type);
        $form->submit($data);

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();

        //Add default value on create Action
        $hrm_settings_additional_hours_type->setCreateDate(new \DateTime('now'));
        $hrm_settings_additional_hours_type->setCreateUid($current_user_id);
        $hrm_settings_additional_hours_type->setLastUpdateUid($current_user_id);
        $hrm_settings_additional_hours_type->setLastUpdate(new \DateTime('now'));

        $this->getDoctrine()->getManager()->flush();
        return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }
    
    public function seqnoUpdateAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $json_data = json_decode($body, true);
//        dump($json_data[0]['id']);
//        die();
        foreach ($json_data as $key => $value) {
           
                $hrm_settings_additional_hours_type = $em->getRepository('AdminBundle:hrm_settings_additional_hours_type')->find($json_data[$key]['id']);
                $hrm_settings_additional_hours_type->setSeqno($key+1);
            
        }
            $this->getDoctrine()->getManager()->flush();
            return new JsonResponse(array('http_code' => 200, "message" => array("Élément modifié avec succès")));
    }    
}
