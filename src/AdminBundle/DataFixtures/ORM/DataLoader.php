<?php

namespace AppBundle\DataFixtures\ORM\Dev;

use Hautelook\AliceBundle\Doctrine\DataFixtures\AbstractLoader;

class DataLoader extends AbstractLoader {

    /**
     * {@inheritdoc}
     */
    public function getFixtures() {
        return [
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_provider.yml',
            '@OAuth2WpBundle/Resources/fixtures/orm/hrm_settings_provider_oauth.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_genderData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_family_statusData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_taxData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_exp_tax_modelData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_bonus_typeData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_bonusData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_absence_typeData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_emp_statusData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_paymentData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_statusData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_study_levelData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_study_typeData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_contract_typeData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_exp_ikData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_exp_dinningData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_exp_accommodationData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_exp_otherData.yml',
            '@AdminBundle/Resources/fixtures/orm/hrm_settings_exp_transportData.yml',
            '@UserBundle/Resources/fixtures/orm/HrmCompany.yml',
            '@UserBundle/Resources/fixtures/orm/EmployeeContract.yml',
            '@UserBundle/Resources/fixtures/orm/hrm_employee.yml',
            '@UserBundle/Resources/fixtures/orm/hrm_user.yml',
            '@PayPeriodBundle/Resources/fixtures/orm/SettingsPayPeriod.yml',
        ];
    }

}