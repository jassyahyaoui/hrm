<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SettingsBonusType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('display_name')
                ->add('description')
      ->add('seqno','choice',array('choices' =>array()))
                         ->add('external_code')
    // ->add('bonus_type', null, array('label' => 'Type de la demande',
                    //'required' => true,
//                    'query_builder' => function(\Doctrine\ORM\EntityRepository $er) {
//                        return $er->createQueryBuilder("t")
//                                ->leftJoin('t.provider', 'p')
//                                ->Where('p.display_name = :provider')
//                                ->setParameter('provider', $this->provider)
//                                ->orderBy('t.seqno', 'ASC');
//                        }
                       // ))
                
            ->add('bonus_type', 'entity', array(
                'label' => 'Type bonus',
                'empty_value' => 'Sélectionner un type de bonus',
                'required' => true,
                'class' => 'AdminBundle\Entity\SettingsBonusType',
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
                        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\SettingsBonus'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_subpremiumtype';
    }


}
