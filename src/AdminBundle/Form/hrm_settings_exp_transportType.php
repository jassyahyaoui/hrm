<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class hrm_settings_exp_transportType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $type = array("Transport" => "Transport", "Hebergement" => "Hebergement", "Restauration" => "Restauration", "Autres" => "Autres");

        $builder
                ->add('type', 'choice', array(
                    'label' => 'Type de frais',
                    'choices' => $type,
                    'required' => true,
                    'attr' => array('class' => 'form-control',
                        'ng-model' => 'data.type',
                        'ng-change' => 'appear()',
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                        )
                )
                ->add('display_name')
//            ->add('taxModel', 'entity', array(
//                'label' => 'Model Tax',
//                'empty_value' => 'Sélectionner un modele',
//                'required' => true,
//                'class' => 'AdminBundle\Entity\hrm_settings_exp_tax_model',
//                'property' => 'display_name',
//                'attr' => array(
//                    'class' => 'chosen-select form-control',
//                )))
                ->add('taxModel', 'entity', array(
                    'label' => 'Modele',
                    'empty_value' => 'Sélectionner un modele',
                    'required' => false,
                    'class' => 'AdminBundle\Entity\hrm_settings_exp_tax_model',
                    'property' => 'display_name',
                    'attr' => array(
                        'class' => 'chosen-select form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right'
                    ),
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("t")
                                ->leftJoin('t.provider', 'p')
                                ->where('p.id = :provider')
                                ->setParameter('provider', (int) $options['provider'])
                        ;
                    },
                ))
                ->add('description')
                ->add('seqno')
                ->add('external_code');
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\hrm_settings_exp_transport',
            'provider' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'adminbundle_settingsexptransport';
    }

}
