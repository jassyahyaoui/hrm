<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;


class hrm_contract_link_dataType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array('required' => true,
                'label' => 'Title ',
                'attr' => array(
                    'class' => 'form-control',
                    'id' => 'amount',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('href', 'text', array('required' => true,
                'label' => 'href ',
                'attr' => array(
                    'class' => 'form-control',
                    'id' => 'amount',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('description', 'textarea', array(
                'label' => 'Description',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right'
                )
            ))
            ->add('target', 'choice', array(
//                'label' => 'Lors de l\'utilisation du véhicule en dehors du temps de travail',
                'choices' => array('_blank' => '_blank', '_self' => '_self'),
                'expanded' => true,
                'multiple' => false,
                'required' => true,
                'label' => 'Car ',
                'attr' => array(
                    'class' => 'form-control',
                    'id' => 'car',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )))
            ->add('hrm_contract_link_category', 'entity', array(
                'property' => 'title',
                'empty_value' => 'Sélectionner une categorie',
                'required' => true,
                'class' => 'AdminBundle\Entity\hrm_contract_link_category',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('c')
                        ->leftjoin('c.provider', 'p')
                        ->where('p.id = :value')
                        ->setParameter('value', (int)$options['provider']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\hrm_contract_link_data',
            'provider' => null
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_hrm_contract_link_data';
    }


}
