<?php

namespace AdminBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class hrm_settings_exp_tax_modelType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('display_name')
                ->add('description')
                ->add('tax', 'entity', array(
                    'multiple' => true,
//                    'empty_value' => 'Choisir le modèle tva',
//                    'required' => true,
//                    select2-tags form-control
                    'class' => 'AdminBundle\Entity\hrm_settings_tax',
                    'property' => 'id',
                    ))
        ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AdminBundle\Entity\hrm_settings_exp_tax_model'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'adminbundle_hrm_settings_exp_tax_model';
    }


}
