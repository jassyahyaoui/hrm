<?php

namespace AdminBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
/**
 * SettingsStatusRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SettingsStatusRepository extends \Doctrine\ORM\EntityRepository
{       public function SettingsStatusByProvider($provider) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('sa,p')
                        ->from('AdminBundle:SettingsStatus', 'sa')
                        ->leftJoin('sa.provider','p')
                        ->where('p.id = :value')
                        ->setParameter('value', (int) $provider)
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }
}
