<?php

namespace AdminBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
/**
 * SettingsAbsencesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SettingsAbsencesRepository extends \Doctrine\ORM\EntityRepository
{
        public function SettingsAbsencesByProvider($provider) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('sa,p')
                        ->from('AdminBundle:SettingsAbsences', 'sa')
                        ->leftJoin('sa.provider','p')
                        ->where('p.id = :value')
                        ->setParameter('value', (int) $provider)
                        ->orderBy('sa.seqno')
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }
}
