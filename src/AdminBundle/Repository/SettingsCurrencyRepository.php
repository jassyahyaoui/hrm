<?php

namespace AdminBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
/**
 * SettingsCurrencyRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SettingsCurrencyRepository extends \Doctrine\ORM\EntityRepository
{
          public function SettingsCurrencyByProvider($provider) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('sc,p')
                        ->from('AdminBundle:SettingsCurrency', 'sc')
                        ->leftJoin('sc.provider','p')
                        ->where('p.id = :value')
                        ->setParameter('value', (int) $provider)
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }
}
