<?php

namespace AdminBundle\Repository;
use Doctrine\ORM\Query;
/**
 * providerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class hrm_settings_technology_levelRepository extends \Doctrine\ORM\EntityRepository
{
         public function SettingsTechnologyLevelByProvider($provider) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('sa,p')
                        ->from('AdminBundle:hrm_settings_technology_level', 'sa')
                        ->leftJoin('sa.provider','p')
                        ->where('p.id = :value')
                        ->setParameter('value', (int) $provider)
                        ->orderBy('sa.seqno')
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }
    
    public function TechnologyLevelByProvider($provider) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('sa.id, sa.display_name')
                        ->from('AdminBundle:hrm_settings_technology_level', 'sa')
                        ->leftJoin('sa.provider','p')
                        ->where('p.id = :value')
                        ->setParameter('value', (int) $provider)
                        ->orderBy('sa.seqno')
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }    
}
