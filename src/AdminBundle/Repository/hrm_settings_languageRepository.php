<?php

namespace AdminBundle\Repository;
use Doctrine\ORM\Query;
/**
 * providerRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class hrm_settings_languageRepository extends \Doctrine\ORM\EntityRepository
{
         public function SettingsTechnologyByProvider($provider) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('sa,p')
                        ->from('AdminBundle:hrm_settings_language', 'sa')
                        ->leftJoin('sa.provider','p')
                        ->where('p.id = :value')
                        ->setParameter('value', (int) $provider)
                        ->orderBy('sa.seqno')
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }
    
    public function TechnologyByProvider($provider) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('sa.id, sa.display_name')
                        ->from('AdminBundle:hrm_settings_language', 'sa')
                        ->leftJoin('sa.provider','p')
                        ->where('p.id = :value')
                        ->setParameter('value', (int) $provider)
                        ->orderBy('sa.seqno')
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }    
}
