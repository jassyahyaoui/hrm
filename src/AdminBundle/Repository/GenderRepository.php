<?php

namespace AdminBundle\Repository;
use Doctrine\ORM\Query;

/**
 * GenderRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class GenderRepository extends \Doctrine\ORM\EntityRepository
{
              public function SettingsGenderByProvider($provider) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('sa,p')
                        ->from('AdminBundle:Gender', 'sa')
                        ->leftJoin('sa.provider','p')
                        ->where('p.id = :value')
                        ->setParameter('value', (int) $provider)
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }
}
