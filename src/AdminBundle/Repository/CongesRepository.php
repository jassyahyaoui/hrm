<?php

namespace AdminBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * CongesRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CongesRepository extends EntityRepository {

    public function CongesByProvider($provider) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('c,p')
                        ->from('AdminBundle:Conges', 'c')
                        ->leftJoin('c.provider', 'p')
                        ->where('p.id = :value')
                        ->setParameter('value', (int) $provider)
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }

}
