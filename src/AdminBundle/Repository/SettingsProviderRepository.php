<?php

namespace AdminBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
/**
 * SettingsProviderRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SettingsProviderRepository extends \Doctrine\ORM\EntityRepository
{ 
    
    public function findAllProviders() {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('p')
            ->from('AdminBundle:SettingsProvider', 'p')
            ->orderBy('p.seqno', 'ASC')
            ->getQuery()
            ->getResult(Query::HYDRATE_ARRAY);
    }    
}
