<?php

namespace AdminBundle\Repository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
/**
 * SettingsConventionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class SettingsConventionRepository extends \Doctrine\ORM\EntityRepository
{
        public function SettingsConventionByProvider($provider) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('sc,p')
                        ->from('AdminBundle:SettingsConvention', 'sc')
                        ->leftJoin('sc.provider','p')
                        ->where('p.id = :value')
                        ->setParameter('value', (int) $provider)
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }
}
