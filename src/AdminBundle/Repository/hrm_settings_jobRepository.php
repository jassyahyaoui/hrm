<?php

namespace AdminBundle\Repository;
use Doctrine\ORM\Query;
/**
 * hrm_settings_jobRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */

class hrm_settings_jobRepository extends \Doctrine\ORM\EntityRepository

{       public function SettingsJobByProvider($provider) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('sa,p')
                        ->from('AdminBundle:hrm_settings_job', 'sa')
                        ->leftJoin('sa.provider','p')
                        ->where('p.id = :value')
                        ->setParameter('value', (int) $provider)
                        ->orderBy('sa.seqno', 'ASC')
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }
    
 public function JobByProvider($provider) {
        return $this->getEntityManager()
                        ->createQueryBuilder()
                        ->select('sa.id, sa.display_name')
                        ->from('AdminBundle:hrm_settings_job', 'sa')
                        ->leftJoin('sa.provider','p')
                        ->where('p.id = :value')
                        ->setParameter('value', (int) $provider)
                        ->orderBy('sa.seqno', 'ASC')
                        ->getQuery()
                        ->getResult(Query::HYDRATE_ARRAY);
    }    
}
