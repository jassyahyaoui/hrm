<?php

namespace ExtraHoursBundle\Controller;

use ExtraHoursBundle\Entity\hrm_additional_hours;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use FileBundle\Entity\attachments;

/**
 * Hrm_additional_hour controller.
 *
 */
class hrm_additional_hoursController extends Controller
{
    public function layoutAction()
    {
        return $this->render('extra_hours/layout.html.twig');
    }

    /**
     * Lists all hrm_additional_hour entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $hrm_additional_hours = $em->getRepository('ExtraHoursBundle:hrm_additional_hours')->findAll();

        return $this->render('extra_hours/index.html.twig', array(
            'hrm_additional_hours' => $hrm_additional_hours,
        ));
    }

    /**
     * Creates a new hrm_additional_hour entity.
     *
     */

    public function newAction(Request $request)
    {
        $hrm_additional_hour = new hrm_additional_hours();
        $em = $this->getDoctrine()->getManager();
        $companyId = $this->container->get('settingsbundle.preference.service')->getCompanyId();
        $providerId = $this->container->get('settingsbundle.preference.service')->getProviderId();

//        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
//        $companyData = $empData->getCompany();
//        $companyId = $companyData->getId();
//        $providerId = $companyData->getProvider();

        $form = $this->createForm('ExtraHoursBundle\Form\hrm_additional_hoursType', $hrm_additional_hour, array(
            'provider' => $providerId, 'company' => $companyId,
        ));
        $form->handleRequest($request);
        return $this->render('extra_hours/new.html.twig', array(
            'hrm_additional_hour' => $hrm_additional_hour,
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request)
    {
        $hrm_additional_hour = new hrm_additional_hours();
        $em = $this->getDoctrine()->getManager();

        $companyId = $this->container->get('settingsbundle.preference.service')->getCompanyId();
        $providerId = $this->container->get('settingsbundle.preference.service')->getProviderId();
//        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
//        $companyData = $empData->getCompany();
//        $companyId = $companyData->getId();
//        $providerId = $companyData->getProvider();

        $form = $this->createForm('ExtraHoursBundle\Form\hrm_additional_hoursType', $hrm_additional_hour, array(
            'provider' => $providerId, 'company' => $companyId,
        ));
        $form->handleRequest($request);
        return $this->render('extra_hours/edit.html.twig', array(
            'hrm_additional_hour' => $hrm_additional_hour,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a hrm_additional_hour entity.
     *
     */
    public function showAction(hrm_additional_hours $hrm_additional_hour)
    {
        $deleteForm = $this->createDeleteForm($hrm_additional_hour);

        return $this->render('extra_hours/show.html.twig', array(
            'hrm_additional_hour' => $hrm_additional_hour,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing hrm_additional_hour entity.
     *
     */

    /**
     * Deletes a hrm_additional_hour entity.
     *
     */
    public function deleteAction(Request $request, hrm_additional_hours $hrm_additional_hour)
    {
        $form = $this->createDeleteForm($hrm_additional_hour);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_additional_hour);
            $em->flush();
        }

        return $this->redirectToRoute('hours_index');
    }

    /**
     * Creates a form to delete a hrm_additional_hour entity.
     *
     * @param hrm_additional_hours $hrm_additional_hour The hrm_additional_hour entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(hrm_additional_hours $hrm_additional_hour)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('hours_delete', array('id' => $hrm_additional_hour->getId())))
            ->setMethod('DELETE')
            ->getForm();
    }

    public function removeAction(Request $request, hrm_additional_hours $hrm_additional_hour)
    {
        $form = $this->createDeleteForm($hrm_additional_hour);
        $form->handleRequest($request);

        if ($request->isMethod('DELETE')) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($hrm_additional_hour);
            $em->flush($hrm_additional_hour);
            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }
    public function extraHoursListAction()
    {

        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();

        $current_user_id = $empData->getId();
        $companyData = $empData->getCompany();
        $company_id = $companyData->getId();
        $providerId = $companyData->getProvider();

        if ($securityContext->isGranted('ROLE_RESPONSABLE')) {

            $result = $em->getRepository('ExtraHoursBundle:hrm_additional_hours')->FindForResp($company_id, $current_user_id);

        } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {

            $result = $em->getRepository('ExtraHoursBundle:hrm_additional_hours')->FindForCollab($current_user_id);

        }
        $users = $em->getRepository('UserBundle:HrmEmployee')->FindCollaborateurByCompanyId($company_id);
        $types = $em->getRepository('AdminBundle:hrm_settings_additional_hours_type')->FindTypesByProvider($providerId);


        return array('result' => $result, 'users' => $users, 'types' => $types);
    }

    public function addAction(Request $request)
    {
        $hrm_additional_hour = new hrm_additional_hours();
        $form = $this->createForm('ExtraHoursBundle\Form\hrm_additional_hoursType', $hrm_additional_hour);
        $form->handleRequest($request);

        $body = $request->getContent();
        $data = json_decode($body, true);

        $em = $this->getDoctrine()->getManager();
        //set default status if ROLE_COLLABORATEUR else set with the submited value
        $securityContext = $this->container->get('security.authorization_checker');

        if ($request->isMethod('POST')) {
            // will be get error if not deleted settings_pay_period from FormType before submit
            $form->remove('settings_pay_period');

            $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $current_user_id = $empData->getId();
            $form->submit($data);

            $hrm_additional_hour->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $hrm_additional_hour->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $hrm_additional_hour->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));

            $hrm_additional_hour->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            $hrm_additional_hour->setSettingsAdditionalHoursType($em->getRepository('AdminBundle:hrm_settings_additional_hours_type')->find($data['type']['id']));

            if ($request->request->has('attachments')) {

                if ($data['attachments'] != null) {
                    if ($hrm_additional_hour->getAttachments() != null) {
                        $attachment = $hrm_additional_hour->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if ((isset($data['attachments']['mimetype'])) && ($data['attachments']['mimetype'] != null))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $hrm_additional_hour->setAttachments($attachment);
                } else {
                    $hrm_additional_hour->setAttachments(null);
                }

            }

            //Add default value on create Action
            $hrm_additional_hour->setCreateDate(new \DateTime('now'));
            $hrm_additional_hour->setCreateUid($current_user_id);
            $hrm_additional_hour->setLastUpdateUid($current_user_id);
            $hrm_additional_hour->setLastUpdate(new \DateTime('now'));
            if ($request->request->has('attachments')) {
                if ($data['attachments'] != null) {
//                    if ($request->request->has('attachments')) {
                    if ($hrm_additional_hour->getAttachments() != null) {
                        $attachment = $hrm_additional_hour->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if ((isset($data['attachments']['mimetype'])) && ($data['attachments']['mimetype'] != null))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $hrm_additional_hour->setAttachments($attachment);
//                    }
                } else {

                    $hrm_additional_hour->setAttachments(null);
                }
            }
            //send mail to responsable

            $TranslateMail = $this->get('mailingbundle.email.send.service');
            if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $this->get('mailingbundle.email.send.service')->Notify($hrm_additional_hour, 'RESPONSABLE', $TranslateMail->TagsTranslateExtraHour('titleNew'), $TranslateMail->TagsTranslateExtraHour('etatNew'), $TranslateMail->TagsTranslateExtraHour('typeRequest'), $TranslateMail->TagsTranslateExtraHour('folder'), $TranslateMail->TagsTranslateExtraHour('viewName'));
            } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $this->get('mailingbundle.email.send.service')->Notify($hrm_additional_hour, 'COLLABORATEUR', $TranslateMail->TagsTranslateExtraHour('titleNew'), $TranslateMail->TagsTranslateExtraHour('etatNew'), $TranslateMail->TagsTranslateExtraHour('typeRequest'), $TranslateMail->TagsTranslateExtraHour('folder'), $TranslateMail->TagsTranslateExtraHour('viewName'));
            }

//              try to add first open one of pay period
            $ret_payperiod =  $this->setOpenedSettingsPayPeriod($em, $hrm_additional_hour);
            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }

//            try {
//            dump($data);
//            die();
            $em->persist($hrm_additional_hour);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
//            } catch (\Exception $exc) {
//                return new \Symfony\Component\BrowserKit\Response('Error to save data', 201);
//            }
        }
    }


    public function updateAction(Request $request, hrm_additional_hours $hrm_additional_hour) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, true);
        $entity = $em->getRepository('ExtraHoursBundle:hrm_additional_hours')->find($data['id']);

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
        $current_user_id = $empData->getId();

        $update_form = $this->createForm('ExtraHoursBundle\Form\hrm_additional_hoursType', $entity);
        if ($request->isMethod('PUT')) {
            $update_form->remove('settings_pay_period');
            $update_form->submit($data);


            if ($data['attachments'] != null) {
                if ($request->request->has('attachments')) {
                    if ($entity->getAttachments() != null) {
                        $attachment = $entity->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if (isset($data['attachments']['mimetype']))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $hrm_additional_hour->setAttachments($attachment);
                }
            } else {

                $hrm_additional_hour->setAttachments(null);
            }
            //return new JsonResponse(array("type" => $data['settings_bonus_type']['id'],"name" => $data['settings_bonus_name']['id']));


            $hrm_additional_hour->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $hrm_additional_hour->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $hrm_additional_hour->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));

            $hrm_additional_hour->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            $hrm_additional_hour->setSettingsAdditionalHoursType($em->getRepository('AdminBundle:hrm_settings_additional_hours_type')->find($data['type']['id']));


            //Add default value on create Action
            $hrm_additional_hour->setCreateDate(new \DateTime('now'));
            $hrm_additional_hour->setCreateUid($current_user_id);
            $hrm_additional_hour->setLastUpdateUid($current_user_id);
            $hrm_additional_hour->setLastUpdate(new \DateTime('now'));

//            $this->statusInsertion($data['status'], $hrm_additional_hour);
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $hrm_additional_hour, $data['payPeriod']);
            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }
            $em->persist($hrm_additional_hour);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function ConfirmAction(Request $request, hrm_additional_hours $hrm_additional_hours) {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $hrm_additional_hours->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['confirm']));

        $entity = $em->getRepository('ExtraHoursBundle:hrm_additional_hours')->find($hrm_additional_hours);

//         try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $entity);
        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }

        //send mail to responsable
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($hrm_additional_hours, 'RESPONSABLE', $TranslateMail->TagsTranslateExtraHour('confirmMsg'), $TranslateMail->TagsTranslateExtraHour('confirmEtat'), $TranslateMail->TagsTranslateExtraHour('typeRequest'), $TranslateMail->TagsTranslateExtraHour('folder'), $TranslateMail->TagsTranslateExtraHour('viewName'));



        $em->persist($hrm_additional_hours);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function CancelAction(Request $request, hrm_additional_hours $hrm_additional_hours) {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $hrm_additional_hours->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['cancel']));

        $entity = $em->getRepository('ExtraHoursBundle:hrm_additional_hours')->find($hrm_additional_hours);

        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $hrm_additional_hours);
        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }
        //send mail to responsable
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($hrm_additional_hours, 'RESPONSABLE', $TranslateMail->TagsTranslateExtraHour('cancelMsg'), $TranslateMail->TagsTranslateExtraHour('cancelEtat'), $TranslateMail->TagsTranslateExtraHour('typeRequest'), $TranslateMail->TagsTranslateExtraHour('folder'), $TranslateMail->TagsTranslateExtraHour('viewName'));

        $em->persist($hrm_additional_hours);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    private function setOpenedSettingsPayPeriod($em, $entity, $payPeriodDate = null) {
        $ret = '';
        // affect first pay period

        if ($payPeriodDate == null) {
            $searchBy = array("companyId" => $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getCompany(),
                "status" => "open");
        } else {
            $searchBy = array("companyId" => $this->container->get('security.context')->getToken()->getUser()->getEmployee()->first()->getCompany());
        }

        $payperiodlist = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')
            ->findBy($searchBy, array('startDate' => 'ASC'));
        if ($payperiodlist) {

            if ($payPeriodDate == null) {
                $entity->setSettingsPayPeriod($payperiodlist[0]);
                $ret = true;
            } else {
                foreach ($payperiodlist as $key => $value) {
                    if ($value->getStartDate()->format('m/Y') == $payPeriodDate && ($value->getStatus() === 'open')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = true;
                        break;
                    }
                    if (($value->getStartDate()->format('m/Y') === $payPeriodDate) && ($value->getStatus() === 'close')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = false;
                        break;
                    }
                }
            }

            if ($ret === true)
                return true;
            if ($ret === false)
                return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_CLOSED_PAY_PERIOD"));

            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_NOT_PAY_PERIOD"));
        } else {
            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_EMPTY_PAY_PERIOD"));
        }
    }
}
