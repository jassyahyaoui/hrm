<?php

namespace ExtraHoursBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class hrm_additional_hoursType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('hour25')
            ->add('hour50', 'text', array(
                'required' => false              
            ))
            ->add('date', 'date', array(
                'label' => 'Date du frais',
                'required' => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'class' => 'form-control datepicker',
                    'date-directive' => ''
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('comment', 'textarea', array(
                'label' => 'Commentaire',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right'
                )
            ))
            ->add('settings_status', 'entity', array(
                'label' => 'Statut',
                'empty_value' => 'Sélectionner un statut',
                'required' => true,
                'class' => 'AdminBundle\Entity\SettingsStatus',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('s')
                        ->leftJoin('s.provider', 'p')
                        ->where("s.id != :statut")
                        ->setParameter('statut', 4);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('requester', 'entity', array(
                'label' => 'Collaborateur',
                'required' => true,
                'disabled' => true,
                'read_only' => true,
                'class' => 'UserBundle\Entity\HrmEmployee',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->Where('u.company = :value')
                        ->setParameter('value', (int)$options['company']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('contributor', 'entity', array(
                'label' => 'Collaborateur',
                'required' => true,
                'class' => 'UserBundle\Entity\HrmEmployee',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->where("e.roles LIKE '%COLLABORATEUR%' OR e.roles LIKE '%RESPONSABLE%'")
                        ->andwhere('u.company = :value')
                        ->setParameter('value', (int)$options['company']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('validator', 'entity', array(
                    'label' => 'Valideur de la demande',
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control',
                        'id' => 'id',
                        'required' => true,
                    ),
                    'class' => 'UserBundle\Entity\HrmEmployee',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('u')
                            ->leftjoin('u.user', 'e')
                            ->where("e.roles LIKE '%RESPONSABLE%'")
                            ->andwhere('u.company = :value')
                            ->setParameter('value', (int)$options['company']);
                    },
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'))
            )
            ->add('settings_additional_hours_type', 'entity', array(
                    'label' => 'Valideur de la demande',
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control',
                        'id' => 'id',
                    ),
                    'class' => 'AdminBundle\Entity\hrm_settings_additional_hours_type',
                    'property' => 'displayName',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('u')
                            ->leftjoin('u.provider', 'e')
                            ->Where('e.id = :value')
                            ->setParameter('value', (int)$options['provider']);
                    },
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'))
            )
            ->add('settings_pay_period', 'text', array(
                'label' => 'Remboursement en',
                'required' => true,
                'attr' => array(
                    'class' => 'form-control date-picker-expense',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ExtraHoursBundle\Entity\hrm_additional_hours',
            'company' => null,
            'provider' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'extrahoursbundle_hrm_additional_hours';
    }


}
