<?php

namespace ExtraHoursBundle\Entity;

/**
 * hrm_additional_hours
 */
class hrm_additional_hours
{

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @var float
     */
    private $hour25;

    /**
     * @var float
     */
    private $hour50;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \DateTime
     */
    private $createDate;

    /**
     * @var integer
     */
    private $createUid;

    /**
     * @var \DateTime
     */
    private $lastUpdate;

    /**
     * @var integer
     */
    private $lastUpdateUid;

    /**
     * @var \FileBundle\Entity\attachments
     */
    private $attachments;

    /**
     * @var \AdminBundle\Entity\SettingsStatus
     */
    private $settings_status;

    /**
     * @var \AdminBundle\Entity\hrm_settings_additional_hours_type
     */
    private $settings_additional_hours_type;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $requester;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $contributor;

    /**
     * @var \UserBundle\Entity\HrmEmployee
     */
    private $validator;

    /**
     * @var \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    private $settings_pay_period;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return hrm_additional_hours
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set hour25
     *
     * @param float $hour25
     *
     * @return hrm_additional_hours
     */
    public function setHour25($hour25)
    {
        $this->hour25 = $hour25;

        return $this;
    }

    /**
     * Get hour25
     *
     * @return float
     */
    public function getHour25()
    {
        return $this->hour25;
    }

    /**
     * Set hour50
     *
     * @param float $hour50
     *
     * @return hrm_additional_hours
     */
    public function setHour50($hour50)
    {
        $this->hour50 = $hour50;

        return $this;
    }

    /**
     * Get hour50
     *
     * @return float
     */
    public function getHour50()
    {
        return $this->hour50;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return hrm_additional_hours
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set createDate
     *
     * @param \DateTime $createDate
     *
     * @return hrm_additional_hours
     */
    public function setCreateDate($createDate)
    {
        $this->createDate = $createDate;

        return $this;
    }

    /**
     * Get createDate
     *
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * Set createUid
     *
     * @param integer $createUid
     *
     * @return hrm_additional_hours
     */
    public function setCreateUid($createUid)
    {
        $this->createUid = $createUid;

        return $this;
    }

    /**
     * Get createUid
     *
     * @return integer
     */
    public function getCreateUid()
    {
        return $this->createUid;
    }

    /**
     * Set lastUpdate
     *
     * @param \DateTime $lastUpdate
     *
     * @return hrm_additional_hours
     */
    public function setLastUpdate($lastUpdate)
    {
        $this->lastUpdate = $lastUpdate;

        return $this;
    }

    /**
     * Get lastUpdate
     *
     * @return \DateTime
     */
    public function getLastUpdate()
    {
        return $this->lastUpdate;
    }

    /**
     * Set lastUpdateUid
     *
     * @param integer $lastUpdateUid
     *
     * @return hrm_additional_hours
     */
    public function setLastUpdateUid($lastUpdateUid)
    {
        $this->lastUpdateUid = $lastUpdateUid;

        return $this;
    }

    /**
     * Get lastUpdateUid
     *
     * @return integer
     */
    public function getLastUpdateUid()
    {
        return $this->lastUpdateUid;
    }

    /**
     * Set attachments
     *
     * @param \FileBundle\Entity\attachments $attachments
     *
     * @return hrm_additional_hours
     */
    public function setAttachments(\FileBundle\Entity\attachments $attachments = null)
    {
        $this->attachments = $attachments;

        return $this;
    }

    /**
     * Get attachments
     *
     * @return \FileBundle\Entity\attachments
     */
    public function getAttachments()
    {
        return $this->attachments;
    }

    /**
     * Set settingsStatus
     *
     * @param \AdminBundle\Entity\SettingsStatus $settingsStatus
     *
     * @return hrm_additional_hours
     */
    public function setSettingsStatus(\AdminBundle\Entity\SettingsStatus $settingsStatus = null)
    {
        $this->settings_status = $settingsStatus;

        return $this;
    }

    /**
     * Get settingsStatus
     *
     * @return \AdminBundle\Entity\SettingsStatus
     */
    public function getSettingsStatus()
    {
        return $this->settings_status;
    }

    /**
     * Set settingsAdditionalHoursType
     *
     * @param \AdminBundle\Entity\hrm_settings_additional_hours_type $settingsAdditionalHoursType
     *
     * @return hrm_additional_hours
     */
    public function setSettingsAdditionalHoursType(\AdminBundle\Entity\hrm_settings_additional_hours_type $settingsAdditionalHoursType = null)
    {
        $this->settings_additional_hours_type = $settingsAdditionalHoursType;

        return $this;
    }

    /**
     * Get settingsAdditionalHoursType
     *
     * @return \AdminBundle\Entity\hrm_settings_additional_hours_type
     */
    public function getSettingsAdditionalHoursType()
    {
        return $this->settings_additional_hours_type;
    }

    /**
     * Set requester
     *
     * @param \UserBundle\Entity\HrmEmployee $requester
     *
     * @return hrm_additional_hours
     */
    public function setRequester(\UserBundle\Entity\HrmEmployee $requester = null)
    {
        $this->requester = $requester;

        return $this;
    }

    /**
     * Get requester
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getRequester()
    {
        return $this->requester;
    }

    /**
     * Set contributor
     *
     * @param \UserBundle\Entity\HrmEmployee $contributor
     *
     * @return hrm_additional_hours
     */
    public function setContributor(\UserBundle\Entity\HrmEmployee $contributor = null)
    {
        $this->contributor = $contributor;

        return $this;
    }

    /**
     * Get contributor
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getContributor()
    {
        return $this->contributor;
    }

    /**
     * Set validator
     *
     * @param \UserBundle\Entity\HrmEmployee $validator
     *
     * @return hrm_additional_hours
     */
    public function setValidator(\UserBundle\Entity\HrmEmployee $validator = null)
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * Get validator
     *
     * @return \UserBundle\Entity\HrmEmployee
     */
    public function getValidator()
    {
        return $this->validator;
    }

    /**
     * Set settingsPayPeriod
     *
     * @param \PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod
     *
     * @return hrm_additional_hours
     */
    public function setSettingsPayPeriod(\PayPeriodBundle\Entity\SettingsPayPeriod $settingsPayPeriod = null)
    {
        $this->settings_pay_period = $settingsPayPeriod;

        return $this;
    }

    /**
     * Get settingsPayPeriod
     *
     * @return \PayPeriodBundle\Entity\SettingsPayPeriod
     */
    public function getSettingsPayPeriod()
    {
        return $this->settings_pay_period;
    }
    /**
     * @var \AdminBundle\Entity\hrm_settings_additional_hours_type
     */
    private $hours_type;


    /**
     * Set hoursType
     *
     * @param \AdminBundle\Entity\hrm_settings_additional_hours_type $hoursType
     *
     * @return hrm_additional_hours
     */
    public function setHoursType(\AdminBundle\Entity\hrm_settings_additional_hours_type $hoursType = null)
    {
        $this->hours_type = $hoursType;

        return $this;
    }

    /**
     * Get hoursType
     *
     * @return \AdminBundle\Entity\hrm_settings_additional_hours_type
     */
    public function getHoursType()
    {
        return $this->hours_type;
    }
    /**
     * @var \AdminBundle\Entity\hrm_settings_additional_hours_type
     */
    private $hoursType;


}
