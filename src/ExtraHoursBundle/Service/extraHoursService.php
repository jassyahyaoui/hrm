<?php

namespace ExtraHoursBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

class extraHoursService {

    protected $em;
    private $container;

    public function __construct(EntityManager $entityManager, Container $container) {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function deleteAllextraHours() {
        $qb = $this->em->createQuery('DELETE ExtraHoursBundle:hrm_additional_hours h')->execute();
    }

    public function findExtraHours($value) {
        $extrahour = $this->container->get('doctrine')->getEntityManager()->getRepository('ExtraHoursBundle:hrm_additional_hours')
                ->findOneBy(array('hour25' => $value));
        if ($extrahour)
            return $extrahour;
        else
            return null;
    }

    public function findListExtraHours($value) {
        $extrahours = $this->container->get('doctrine')->getEntityManager()->getRepository('ExtraHoursBundle:hrm_additional_hours')
                ->findBy(array('hour25' => $value));
        return $extrahours;
    }

}
