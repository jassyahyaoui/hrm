<?php

namespace TrainingBundle\Controller;

use TrainingBundle\Entity\training;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use FileBundle\Entity\attachments;

/**
 * trainingController.
 *
 */
class trainingController extends Controller
{

    public function layoutAction()
    {
        return $this->render('training/training_layout.html.twig');
    }

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $traininges = $em->getRepository('TrainingBundle:training')->findAll();
        return $this->render('training/index.html.twig', array(
            'trainings' => $trainings,
        ));
    }

    public function newAction(Request $request)
    {
        $training = new training();
        $providerId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $form = $this->createForm('TrainingBundle\Form\trainingType', $training, array(
            'companyId' => $companyId,
            'providerId' => $providerId
        ));
        $form->handleRequest($request);

        return $this->render('training/new.html.twig', array(
            'training' => $training,
            'form' => $form->createView(),
        ));
    }

    public function editAction(Request $request)
    {
        $training = new training();
        $providerId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getProvider()->getId();
        $companyId = $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId();
        $form = $this->createForm('TrainingBundle\Form\trainingType', $training, array(
            'companyId' => $companyId,
            'providerId' => $providerId
        ));
        $form->handleRequest($request);

        return $this->render('training/edit.html.twig', array(
            'training' => $training,
            'form' => $form->createView(),
        ));
    }

    public function addAction(Request $request)
    {
        $training = new training();
        $form = $this->createForm('TrainingBundle\Form\trainingType', $training);
        $form->handleRequest($request);
        $body = $request->getContent();
        $data = json_decode($body, true);
        $em = $this->getDoctrine()->getManager();

        $securityContext = $this->container->get('security.authorization_checker');
        //set default status if ROLE_COLLABORATEUR else set with the submited value
        if ($request->isMethod('POST')) {

            $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $current_user_id = $empData->getId();
            $form->submit($data);

            $training->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $training->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $training->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));

            $training->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));

            $training->setSettingsTrainingType($em->getRepository('AdminBundle:settingsTrainingType')->find($data['settingsTrainingType']));
            if ($request->request->has('attachments')) {
                if ($data['attachments'] != null) {
//                    if ($request->request->has('attachments')) {
                    if ($training->getAttachments() != null) {
                        $attachment = $training->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if ((isset($data['attachments']['mimetype'])) && ($data['attachments']['mimetype'] != null))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $training->setAttachments($attachment);
//                    }
                } else {

                    $training->setAttachments(null);
                }
            }
            //Add default value on create Action
            $training->setCreateDate(new \DateTime('now'));
            $training->setCreateUid($current_user_id);
            $training->setLastUpdateUid($current_user_id);
            $training->setLastUpdate(new \DateTime('now'));

            // try to add first open one of pay period
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $training);
            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }
            
            //send mail to responsable

            $TranslateMail = $this->get('mailingbundle.email.send.service');
            if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
                $this->get('mailingbundle.email.send.service')->Notify($training, 'RESPONSABLE', $TranslateMail->TagsTranslateTraining('titleNew'), $TranslateMail->TagsTranslateTraining('etatNew'), $TranslateMail->TagsTranslateTraining('typeRequest'), $TranslateMail->TagsTranslateTraining('folder'), $TranslateMail->TagsTranslateTraining('viewName'));
            } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
                $this->get('mailingbundle.email.send.service')->Notify($training, 'COLLABORATEUR', $TranslateMail->TagsTranslateTraining('titleNew'), $TranslateMail->TagsTranslateTraining('etatNew'), $TranslateMail->TagsTranslateTraining('typeRequest'), $TranslateMail->TagsTranslateTraining('folder'), $TranslateMail->TagsTranslateTraining('viewName'));
            }


            $em->persist($training);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }


    public function allAction()
    {
        $em = $this->getDoctrine()->getManager();
        $securityContext = $this->container->get('security.authorization_checker');

        $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();

        $current_user_id = $empData->getId();
        $companyData = $empData->getCompany();
        $company_id = $companyData->getId();
        $providerId = $companyData->getProvider();
        if ($securityContext->isGranted('ROLE_RESPONSABLE')) {
            $result = $em->getRepository('TrainingBundle:training')->FindForResp($company_id, $current_user_id);
        } else if ($securityContext->isGranted('ROLE_COLLABORATEUR')) {
            $result = $em->getRepository('TrainingBundle:training')->FindForCollab($current_user_id);
        }

        $trainingTypes = $em->getRepository('AdminBundle:settingsTrainingType')->findAll();
        $users = $em->getRepository('UserBundle:HrmEmployee')->FindCollaborateurByCompanyId($company_id);
        return array('result' => $result, 'users' => $users, 'trainingTypes' => $trainingTypes);
    }

    public function updateAction(Request $request, training $training) {
        $em = $this->getDoctrine()->getManager();
        $body = $request->getContent();
        $data = json_decode($body, true);
        $training = $em->getRepository('TrainingBundle:training')->find($data['id']);
        $update_form = $this->createForm('TrainingBundle\Form\trainingType', $training);
        if ($request->isMethod('PUT')) {
            $update_form->submit($data);

            $collab_id = $training->getCreateUid();

            $empData = $this->container->get('settingsbundle.preference.service')->getEmpData();
            $current_user_id = $empData->getId();
            $training->setLastUpdate(new \DateTime('now'));
            $training->setLastUpdateUid($current_user_id);

            if ($data['attachments'] != null) {
                if ($request->request->has('attachments')) {
                    if ($training->getAttachments() != null) {
                        $attachment = $training->getAttachments();
                    } else
                        $attachment = new attachments();
                    $attachment->setPath($data['attachments']['path']);
                    if (isset($data['attachments']['mimetype']))
                        $attachment->setMimetype($data['attachments']['mimetype']);
                    $em->persist($attachment);
                    $em->flush($attachment);
                    $training->setAttachments($attachment);
                }
            } else {
                $training->setAttachments(null);
            }

            $training->setContributor($em->getRepository('UserBundle:HrmEmployee')->find($data['contributor']['id']));
            $training->setValidator($em->getRepository('UserBundle:HrmEmployee')->find($data['validator']['id']));
            $training->setRequester($em->getRepository('UserBundle:HrmEmployee')->find($data['requester']['id']));

            $training->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['status']));
            $training->setSettingsTrainingType($em->getRepository('AdminBundle:settingsTrainingType')->find($data['settingsTrainingType']['id']));
            
            // try to add first open one of pay period
            $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $training);
            if ($ret_payperiod !== true) {
                return $ret_payperiod;
            }
            
            $em->persist($training);
            $em->flush();
            $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
            return $response;
        }
    }

    public function removeAction(Request $request, training $training)
    {
        if ($request->isMethod('DELETE')) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($training);
            $em->flush($training);
            $response = new \Symfony\Component\BrowserKit\Response('Remove success', 200);
            return $response;
        }
    }

    public function ConfirmAction(Request $request, training $training)
    {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $training->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['confirm']));

        $entity = $em->getRepository('TrainingBundle:training')->find($training);

        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $training);
        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }
        //send mail to responsable
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($entity, 'RESPONSABLE', $TranslateMail->TagsTranslateTraining('confirmMsg'), $TranslateMail->TagsTranslateTraining('confirmEtat'), $TranslateMail->TagsTranslateTraining('typeRequest'), $TranslateMail->TagsTranslateTraining('folder'), $TranslateMail->TagsTranslateTraining('viewName'));



        $em->persist($training);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }

    public function CancelAction(Request $request, training $training)
    {

        $em = $this->getDoctrine()->getManager();

        $body = $request->getContent();
        $data = json_decode($body, true);

        $training->setSettingsStatus($em->getRepository('AdminBundle:SettingsStatus')->find($data['cancel']));

        $entity = $em->getRepository('TrainingBundle:training')->find($training);

        // try to add first open one of pay period
        $ret_payperiod = $this->setOpenedSettingsPayPeriod($em, $entity);

        if ($ret_payperiod !== true) {
            return $ret_payperiod;
        }
        
        //send mail to responsable
        $TranslateMail = $this->get('mailingbundle.email.send.service');
        $this->get('mailingbundle.email.send.service')->Notify($entity, 'RESPONSABLE', $TranslateMail->TagsTranslateTraining('cancelMsg'), $TranslateMail->TagsTranslateTraining('cancelEtat'), $TranslateMail->TagsTranslateTraining('typeRequest'), $TranslateMail->TagsTranslateTraining('folder'), $TranslateMail->TagsTranslateTraining('viewName'));

        $em->persist($training);
        $em->flush();

        $response = new \Symfony\Component\BrowserKit\Response('It worked. Believe me - I\'m an API', 200);
        return $response;
    }
    
    /**
     *
     * @param type $em
     * @param type $entity
     * @param type $payPeriodDate celui envoyÃ© depuis from
     * @return boolean/object
     */
    private function setOpenedSettingsPayPeriod($em, $entity, $payPeriodDate = null) {
        $ret = '';
        if ($payPeriodDate == null) {
            $searchBy = array("companyId" => $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId(),
                "status" => "open");
        } else {
            $searchBy = array("companyId" => $this->container->get('settingsbundle.preference.service')->getEmpData()->getCompany()->getId());
        }
        $payperiodlist = $em->getRepository('PayPeriodBundle:SettingsPayPeriod')
                ->findBy($searchBy, array('startDate' => 'ASC'));
        if ($payperiodlist) {

            if ($payPeriodDate == null) {
                $entity->setSettingsPayPeriod($payperiodlist[0]);
                $ret = true;
            } else {
                foreach ($payperiodlist as $key => $value) {
                    if ($value->getStartDate()->format('m/Y') == $payPeriodDate && ($value->getStatus() === 'open')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = true;
                        break;
                    }
                    if (($value->getStartDate()->format('m/Y') === $payPeriodDate ) && ($value->getStatus() === 'close')) {
                        $entity->setSettingsPayPeriod($value);
                        $ret = false;
                        break;
                    }
                }
            }
            if ($ret === true)
                return true;
            if ($ret === false)
                return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_CLOSED_PAY_PERIOD"));
            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_NOT_PAY_PERIOD"));
        }
        else {
            return new JsonResponse(array('content' => 'PAY_PERIOD', "status" => "ERR_EMPTY_PAY_PERIOD"));
        }
    }    

}
