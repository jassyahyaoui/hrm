<?php

namespace TrainingBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Doctrine\ORM\EntityRepository; 
use Symfony\Component\Security\Core\SecurityContext;
use SettingsBundle\DependencyInjection\SettingsPreferences;

class trainingType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('amount', 'text', array('required' => true,
                'label' => 'Montant ',
                'attr' => array(
                    'class' => 'form-control',
                    'id' => 'amount',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
                ->add('settingsTrainingType', 'entity', array(
                    'label' => 'trainingType',
                    'empty_value' => 'Sélectionner un type',
                    'required' => true,
                    'class' => 'AdminBundle\Entity\SettingsTrainingType',
                    'query_builder' => function (EntityRepository $er) use($options) {
                        return $er->createQueryBuilder("b")
                                ->leftJoin('b.provider', 'p')
                                ->Where('p.id = :providerId')
                                ->setParameter('providerId', (int) $options['providerId']);
                    },
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'
                    )
                ))
            ->add('contributor', 'entity', array(
                'label' => 'Collaborateur',
                'empty_value' => 'Sélectionner un collaborateur',
                'required' => true,
                'class' => 'UserBundle\Entity\HrmEmployee',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->where("e.roles LIKE '%COLLABORATEUR%' OR e.roles LIKE '%RESPONSABLE%'")
                        ->andwhere('u.company = :value')
                        ->setParameter('value', (int)$options['companyId']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('validator', 'entity', array(
                    'label' => 'Valideur de la demande',
                    'empty_value' => 'Sélectionner un valideur',
                    'required' => true,
                    'attr' => array(
                        'class' => 'form-control',
                        'id' => 'id',
                        'required' => true,
                    ),
                    'class' => 'UserBundle\Entity\HrmEmployee',
                    'query_builder' => function (EntityRepository $er) use ($options) {
                        return $er->createQueryBuilder('u')
                            ->leftjoin('u.user', 'e')
                            ->where("e.roles LIKE '%RESPONSABLE%'")
                            ->andwhere('u.company = :value')
                            ->setParameter('value', (int)$options['companyId']);
                    },
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right asterix'))
            )
            ->add('requester', 'entity', array(
                'label' => 'Collaborateur',
                'empty_value' => 'Sélectionner un requester',
                'required' => true,
                'disabled' => true,
                'read_only' => true,
                'class' => 'UserBundle\Entity\HrmEmployee',
                'query_builder' => function (EntityRepository $er) use ($options) {
                    return $er->createQueryBuilder('u')
                        ->leftjoin('u.user', 'e')
                        ->Where('u.company = :value')
//                        ->andwhere('e.id = :valueUser')
                        ->setParameter('value', (int)$options['companyId']);
//                        ->setParameter('valueUser', (int)$options['currentUser']);
                },
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
                ->add('displayName', 'text', array(
                    'label' => 'Commentaire',
                    'required' => false,
                    'attr' => array(
                        'class' => 'form-control',
                    ),
                    'label_attr' => array(
                        'class' => 'col-sm-3 control-label no-padding-right'
                    )
                ))
            ->add('startDate', 'date', array(
                'label' => 'Date du frais',
                'required' => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'class' => 'form-control datepicker',
                    'date-directive' => ''
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('endDate', 'date', array(
                'label' => 'Date du frais',
                'required' => true,
                'widget' => 'single_text',
                'format' => 'dd/MM/yyyy',
                'attr' => array(
                    'class' => 'form-control datepicker',
                    'date-directive' => ''
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right asterix'
                )
            ))
            ->add('comment', 'textarea', array(
                'label' => 'Commentaire',
                'required' => false,
                'attr' => array(
                    'class' => 'form-control',
                ),
                'label_attr' => array(
                    'class' => 'col-sm-3 control-label no-padding-right'
                )
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'TrainingBundle\Entity\training',
            'providerId' => null,
            'companyId' => null,
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'trainingbundle_training';
    }

}
