<?php

namespace FileBundle\Controller;

use FileBundle\Entity\attachments;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Attachment controller.
 *
 */
class attachmentsController extends Controller
{
    /**
     * Lists all attachment entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $attachments = $em->getRepository('FileBundle:attachments')->findAll();
        return $this->render('attachments/index.html.twig', array(
            'attachments' => $attachments,
        ));
    }

    /**
     * Creates a new attachment entity.
     *
     */
    public function newAction(Request $request)
    {
        $attachment = new Attachment();
        $form = $this->createForm('FileBundle\Form\attachmentsType', $attachment);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($attachment);
            $em->flush($attachment);
            return $this->redirectToRoute('attachments_show', array('id' => $attachment->getId()));
        }
        return $this->render('attachments/new.html.twig', array(
            'attachment' => $attachment,
            'form' => $form->createView(),
        ));
    }
    
      
    /**
     * Lists all attachment entities.
     *
     */
    public function postFileAttachmentAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $attachment = new hrm_attachments();
        $attachment->setFile("test");
        $attachment->setPath($request->request->get('url'));
        return new \Symfony\Component\HttpFoundation\JsonResponse(array('success' => true,'id'=>$request->request->get('url')));
        $attachment->preUpload();
        $attachment->upload();
        $em->persist($attachment);
        $em->flush($attachment);
        return new \Symfony\Component\HttpFoundation\JsonResponse(array('success' => true,'id'=>$attachment->getUid()));
    }
    

    /**
     * Finds and displays a attachment entity.
     *
     */
    public function showAction(attachments $attachment)
    {
        $deleteForm = $this->createDeleteForm($attachment);
        return $this->render('attachments/show.html.twig', array(
            'attachment' => $attachment,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing attachment entity.
     *
     */
    public function editAction(Request $request, attachments $attachment)
    {
        $deleteForm = $this->createDeleteForm($attachment);
        $editForm = $this->createForm('FileBundle\Form\attachmentsType', $attachment);
        $editForm->handleRequest($request);
        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('attachments_edit', array('id' => $attachment->getId()));
        }

        return $this->render('attachments/edit.html.twig', array(
            'attachment' => $attachment,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a attachment entity.
     *
     */
    public function deleteAction(Request $request, attachments $attachment)
    {
        $form = $this->createDeleteForm($attachment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($attachment);
            $em->flush();
        }

        return $this->redirectToRoute('attachments_index');
    }

    /**
     * Creates a form to delete a attachment entity.
     *
     * @param attachments $attachment The attachment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(attachments $attachment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('attachments_delete', array('id' => $attachment->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
