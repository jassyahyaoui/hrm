app.controller('requestAbsenceCtrl', function ($scope, $resource, $http, $filter, $translate, uiCalendarConfig, $timeout, $compile, Ie11, Global) {

    $scope.RemoveAll = function () {
        Global.Remove($scope);
    };
    $scope.uploadNewAttachment = function (file) {
        $scope.requestabsence.attachments = {};
        $scope.requestabsence.attachments.path = file.url;
        $scope.requestabsence.attachments.mimetype = file.mimetype;
    };

    $scope.uploadEditAttachment = function (file) {
        $scope.edit_requestabsence.attachments = {};
        $scope.edit_requestabsence.attachments.path = file.url;
        $scope.edit_requestabsence.attachments.mimetype = file.mimetype;
    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };

    $scope.putrequestabsence = function (action) {
        $scope.IsDisabled = true;
        $scope.edit_requestabsence.action = action;
//        if ($scope.valid($scope.edit_requestabsence.startdate, $scope.edit_requestabsence.enddate) == false)
//            return;
        var update = $resource(Routing.generate('api_putrequestabsence', {id: $scope.edit_requestabsence.id}), null,
            {
                'update': {method: 'PUT'}
            });
        update.update(null, $scope.edit_requestabsence,
            function (data) {
                $scope.datatable();
                $("#EditAbsenceRequest").on('hidden.bs.modal', function (e) {
                    $("#EditAbsenceRequest").off();
                });
                $("#EditAbsenceRequest").modal('hide');
                flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
            },
            function (error) {
                flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
        $scope.IsDisabled = false;
    };

    $scope.absenceTypeSearch = '';
    $scope.filterByAbsenceType = function (absencetype) {
        if (absencetype === 'All') {
            $scope.absenceTypeSearch = '';
        } else {
            $scope.absenceTypeSearch = absencetype;
        }
    };

    $scope.dateAbsenceSearch = '';
    $scope.filterByAbsenceDatePicker = function (type) {
        if (type === 'All') {
            $scope.dateAbsenceSearch = '';
        }
    };

    $scope.changeLanguage = function (key) {
        $rootScope.$broadcast('changeLanguage', key);
    };

    $scope.$on('changeLanguage', function (event, args) {
        $translate.use(args);
    });

    $scope.changeLanguage = function (key) {
        $translate.use('fr');
    };

    $scope.userData = {};

// RESPONSABLE Role Global actions
//     $scope.RemoveAll = function () {
//         var ActionTab = [];
//         var IdTab = [];
//         var c = 0;
//         $("input:checkbox:checked").each(function () {
//             if (($(this).attr('data-statut') == '4')) {
//                 ActionTab.push("remove_" + $(this).attr('id'));
//                 IdTab.push($(this).attr('name'));
//             }
//         });
//
//         if (ActionTab.length > 0) {
//             for (var i = 0; i < ActionTab.length; i++) {
//                 c++;
//                 if (c == ActionTab.length) {
//                     var remove = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}));
//                     remove.delete(
//                         function (data) {
//                             $('.close').click();
//                             $scope.data = data;
//                             $scope.datatable();
//                             flash($scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');
//                         },
//                         function (error) {
//                             flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
//                         });
//                 } else {
//                     var remove = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}));
//                     remove.delete(
//                         function (data) {
//                         },
//                         function (error) {
//                             flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
//                         });
//                 }
//             }
//         } else {
//             $('.close').click();
//         }
//     };

    $scope.ValidateAll = function () {
        var ActionTab = [];
        var IdTab = [];
        var ScopeTab = [];
        var c = 0;
        var ScopeValue;
        $("input:checkbox:checked").each(function () {
            if ($(this).attr('data-statut') != '2') {
                ActionTab.push("confirm_" + $(this).attr('id'));
                IdTab.push($(this).attr('name'));
                if ($(this).attr('data-statut') == '4') {
                    ScopeValue = 1;
                } else {
                    ScopeValue = 2;
                }
                ScopeTab.push(ScopeValue);
            }
        });

        if (ActionTab.length > 0) {
            for (var i = 0; i < ActionTab.length; i++) {
                c++;
                $scope.confirmed = {};
                if (c == ActionTab.length) {
                    var linkconfirm = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.confirmed.confirm = ScopeTab[i];
                    linkconfirm.update(null, $scope.confirmed,
                        function (data) {
                            $('.close').click();
                            $scope.data = data;
                            $scope.datatable();
                            flash($scope.flashTranslate('FLASH_VALID_CONFIRM'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                } else {
                    var linkconfirm = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.confirmed.confirm = ScopeTab[i];
                    linkconfirm.update(null, $scope.confirmed,
                        function (data) {
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                }
            }
        } else {
            $('.close').click();
        }
    };


    $scope.CancelAll = function () {
        var ActionTab = [];
        var IdTab = [];
        var c = 0;
        $("input:checkbox:checked").each(function () {
            if (($(this).attr('data-statut') != '2') && ($(this).attr('data-statut') != '3')) {
                ActionTab.push("cancel_" + $(this).attr('id'));
                IdTab.push($(this).attr('name'));
            }
        });
        if (ActionTab.length > 0) {
            for (var i = 0; i < ActionTab.length; i++) {
                c++;
                $scope.canceled = {};
                if (c == ActionTab.length) {
                    var linkcancel = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.canceled.cancel = "3";
                    linkcancel.update(null, $scope.canceled,
                        function (data) {
                            $('.close').click();
                            $scope.data = data;
                            $scope.datatable();
                            flash($scope.flashTranslate('FLASH_VALID_REFUS'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                } else {
                    var linkcancel = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.canceled.cancel = "3";
                    linkcancel.update(null, $scope.canceled,
                        function (data) {
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                }
            }
        } else {
            $('.close').click();
        }
    };
    //END RESPONSABLE Role Global Actions

    //Collaborateur Role Global Actions
    $scope.CollaborateurRemoveAll = function () {
        var ActionTab = [];
        var IdTab = [];
        var c = 0;
        $("input:checkbox:checked").each(function () {
            if ($(this).attr('data-statut') == '4') {
                ActionTab.push("remove_" + $(this).attr('id'));
                IdTab.push($(this).attr('name'));
            }
        });
        if (ActionTab.length > 0) {
            for (var i = 0; i < ActionTab.length; i++) {
                c++;
                if (c == ActionTab.length) {
                    var remove = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}));
                    remove.delete(
                        function (data) {
                            $('.close').click();
                            $scope.data = data;
                            $scope.datatable();
                            flash($scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                } else {
                    var remove = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}));
                    remove.delete(
                        function (data) {
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                }
            }
        } else {
            $('.close').click();
        }
    };
    $scope.CollaborateurValidateAll = function () {
        var ActionTab = [];
        var IdTab = [];
        var c = 0;
        $("input:checkbox:checked").each(function () {
            if (($(this).attr('data-statut') == '4') && ($(this).attr('data-user') == 'true')) {
                ActionTab.push("confirm_" + $(this).attr('id'));
                IdTab.push($(this).attr('name'));
            }
        });
        if (ActionTab.length > 0) {
            for (var i = 0; i < ActionTab.length; i++) {
                c++;
                $scope.confirmed = {};
                if (c == ActionTab.length) {
                    var linkconfirm = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.confirmed.confirm = "1";
                    linkconfirm.update(null, $scope.confirmed,
                        function (data) {
                            $('.close').click();
                            $scope.data = data;
                            $scope.datatable();
                            flash($scope.flashTranslate('FLASH_VALID_CONFIRM'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                } else {
                    var linkconfirm = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.confirmed.confirm = "1";
                    linkconfirm.update(null, $scope.confirmed,
                        function (data) {
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                }
            }
        } else {
            $('.close').click();
        }
    };
    $scope.AnnulerAll = function () {
        var ActionTab = [];
        var IdTab = [];
        var c = 0;
        $("input:checkbox:checked").each(function () {
            if ((($(this).attr('data-statut') == '1') || ($(this).attr('data-statut') == '2') && ($(this).attr('data-period') == 'open')) && ($(this).attr('data-user') == 'true')) {
                ActionTab.push("cancel_" + $(this).attr('id'));
                IdTab.push($(this).attr('name'));
            }
        });
        if (ActionTab.length > 0) {
            for (var i = 0; i < ActionTab.length; i++) {
                c++;
                $scope.canceled = {};

                if (c == ActionTab.length) {
                    var linkcancel = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.canceled.cancel = "4";
                    linkcancel.update(null, $scope.canceled,
                        function (data) {
                            $('.close').click();
                            $scope.data = data;
                            $scope.datatable();
                            flash($scope.flashTranslate('FLASH_VALID_CANCEL'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                } else {
                    var linkcancel = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.canceled.cancel = "4";
                    linkcancel.update(null, $scope.canceled,
                        function (data) {
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                }
            }
        } else {
            $('.close').click();
        }
    };

    //END Collaborateur Role Global Actions

// RESPONSABLE Role second actions
//make status Validé
    $scope.confirm = function (route, id, action) {
        $scope.confirmed = {};
        var linkconfirm = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        if (action == "confirmer") {
            $scope.confirmed.confirm = "2";
        } else if (action == "valider") {
            $scope.confirmed.confirm = "1";
        }
        linkconfirm.update(null, $scope.confirmed,
            function (data) {
                if (data.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                flash($scope.flashTranslate('FLASH_VALID_VALID'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    }


//make status Refusé 
    $scope.cancel = function (route, id) {
        $scope.canceled = {};
        var linkcancel = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.canceled.cancel = "3";
        linkcancel.update(null, $scope.canceled,
            function (data) {
                if (data.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }

                flash($scope.flashTranslate('FLASH_VALID_REFUS'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    }
// END RESPONSABLE Role second actions


// Collaborateur Role second actions
//make status Validé
    $scope.CollaborateurValidate = function (route, id) {
        $scope.confirmed = {};
        var linkconfirm = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.confirmed.confirm = "1";
        linkconfirm.update(null, $scope.confirmed,
            function (data) {
                flash($scope.flashTranslate('FLASH_VALID_VALID'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    }

//make status Refusé 
    $scope.Annuler = function (route, id) {
        $scope.canceled = {};
        var linkcancel = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.canceled.cancel = "4";
        linkcancel.update(null, $scope.canceled,
            function (data) {
                if (data.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                flash($scope.flashTranslate('FLASH_VALID_CANCEL'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    }

// END Collaborateur Role second actions


    $scope.InitFields = function () {
        $scope.CurrentDate = $filter('date')(new Date(), "dd/MM/yyyy ");
        $("#requestabsencebundle_requestabsence_manager").val($("#requestabsencebundle_requestabsence_manager option:eq(1)").val());
        if (typeof angular.element($('#NewAbsenceRequest')).scope().requestabsence !== "undefined")
            $scope.requestabsence.manager = $("#requestabsencebundle_requestabsence_manager option:eq(1)").val();
    }
// RESPONSABLE Role Global actions


    $scope.convertDateTime_Y_M_D_HH_MM_SS = function (date) {
        date_sp = date.split(' ');
        date_slash = date_sp[0].split('/');
        return date_slash[2] + '-' + date_slash[1] + '-' + date_slash[0] + ' ' + date_sp[1] + ':00';
    }

    $scope.validateField = function () {

    };

    $scope.saverequestabsence = function () {

        $scope.etat('#ValiderAbsence', '#SauvegarderAbsence', 'New');
        $scope.IsDisabled = true;
        $scope.newrequestabsence = $scope.requestabsence;
      //  $scope.newrequestabsence.action = action;
        if ($scope.newrequestabsence == null)
            return;
        console.log("Validation befor posting form");
        if ($scope.valid($scope.newrequestabsence.startdate, $scope.newrequestabsence.enddate) == false)
            return;

        var postaction = $resource(Routing.generate('api_postrequestabsence'), null,
            {
                'post': {method: 'POST'}
            });
        postaction.post(null, $scope.newrequestabsence,
            function (data) {
                if (data.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                $scope.datatable();
                $scope.requestabsence.requestdate = $filter('date')(new Date(), 'dd/MM/yyyy');
                $scope.requestabsence.manager = "";
                $scope.requestabsence.absencetype = "";
                $scope.requestabsence.startdate = "";
                $scope.requestabsence.enddate = "";
                $scope.requestabsence.numberdays = 0;
                $scope.requestabsence.notes = "";
                $scope.requestabsence.attachments = null;
                $scope.IsDisabled = false;
                $scope.InitFields();
                if ($scope.createother == true) {
                    return;
                } else {
                    $('.close').click();
                }
                flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.IsDisabled = false;
            },
            function (error) {
                OnError("Votre demande est invalide");
            });
        $scope.IsDisabled = false;
    };

    $scope.changestatus = function (id_demande, action) {
        var payload = {
            id: id_demande,
            action: "annuler",
        };
        var url = Routing.generate('api_confirmabsencerequest');
        $http.post(url + '/' + id_demande + '/' + action, payload).then(function (response) {
        }).then(function successCallback(response) {
            $scope.datatable();
            if (action == "refuser")
                flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
            else if (action == "valider")
                OnSuccess($scope.flashTranslate('FLASH_VALID_VALID'));
            else if (action == "annuler")
                OnSuccess($scope.flashTranslate('FLASH_VALID_CANCEL'));

        }, function errorCallback(response) {
        });
    }

    $scope.updaterequestabsence = function (edit) {
        $scope.IsDisabled = true;
        $scope.editt = $scope.scopeedit;
        if ($scope.valid($scope.editt.startdate, $scope.editt.enddate) == false)
            return;

        var update = $resource(Routing.generate('api_putrequestabsence', {id: $scope.scopeedit.id}), null,
            {
                'update': {method: 'PUT'}
            });
        update.update(null, $scope.scopeedit,
            function (data) {
                if (data.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                $scope.IsDisabled = false;
                $scope.datatable();
                flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
            },
            function (error) {
                $scope.IsDisabled = false;
                OnError($scope.flashTranslate('UNABLE_CHANGE_REQUEST'));
            });
    };

    $scope.getrequestabsenc = function (id_demande) {
        var url = Routing.generate('api_getrequestabsence');
        $http({
            method: 'GET',
            url: url + '/' + id_demande,
        }).then(function successCallback(response) {
            $scope.requestabsence.employee = (response.data.employee.id).toString();
            if (response.data.attachments != null) {
                if (response.data.attachments.path.length > 0) {
                    $scope.requestabsence.attachments = response.data.attachments.path;
                }
            }

        }, function errorCallback(response) {
            flash($scope.flashTranslate('UNABLE_DISPLAY_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
        });
    };

    $scope.refresh = function () {
        var url = Routing.generate('api_getrequestsabsence');
        $http({
            method: 'GET',
            url: url,
        }).then(function successCallback(response) {
            $scope.requestabsences = response.data.requestabsences;
            $scope.users = response.data.users;
            $scope.liststatus = response.data.status;
        }, function errorCallback(response) {
        });
    };

    $scope.delete = function (id) {
        var url = Routing.generate('api_deleteabsencerequest');
        $http({
            method: 'DELETE',
            url: url + "/" + id
        }).then(function successCallback(response) {
            $scope.datatable();
            flash($scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');
        }, function errorCallback(response) {
            flash($scope.flashTranslate('UNABLE_DELETE_BONUS'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
        });
    };

    $scope.valid = function (startdate, enddate) {
        startdate = startdate.split('/');
        enddate = enddate.split('/');
        /*-----*/
        start_actual_time = startdate[1] + '/' + startdate[0] + '/' + startdate[2];
        end_actual_time = enddate[1] + '/' + enddate[0] + '/' + enddate[2];
        start_actual_time = new Date(start_actual_time);
        end_actual_time = new Date(end_actual_time);
        var diff = end_actual_time - start_actual_time;
        var diffSeconds = diff / 1000;
        if (diffSeconds < 0) {
            flash($scope.flashTranslate('ERROR_CHECK_DATE'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            return false;
        }

        return true;
    }

    $scope.save = function (type) {
        var url = $scope.getUrlByType(type);
        $scope.etat('#ValiderIK', '#SauvegarderIK', 'New');
        $scope.etat('#ValiderTRANSPORT', '#SauvegarderTRANSPORT', 'New');
        $scope.etat('#ValiderACCOMODATION', '#SauvegarderACCOMODATION', 'New');
        $scope.etat('#ValiderOTHER', '#SauvegarderOTHER', 'New');
        url.save($scope.new,
            function (successResult) {
                $scope.IsDisabled = false;
                if (successResult.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (successResult.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (successResult.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }

                flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
                if ($('#check' + type).is(":checked")) {
                    $scope.nullable(type);
                    return;
                } else {
                    $scope.nullable(type);
                    $('.close').click();
                }
            },
            function (errorResult) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    }

    $scope.currentDate = function () {
        $scope.requestabsence.requestdate = $filter('date')(new Date(), 'dd/MM/yyyy');
    }

    $scope.datatable = function () {
        $scope.loading = true;
        var infos = $resource(Routing.generate('api_getrequestsabsence'));
        infos.get(
            function (data) {
                $scope.TotalRefuse();
                $scope.TotalValide();
                $scope.TotalBrouillon();
                $scope.TotalAttente();
                var username = [];
                var Types = [];
                var Status = [];
                var tasks = [];
                $scope.FiltersData = data.requestabsences;
                $scope.requestsabsences = data.requestabsences; // for calendar ----------------
                $scope.data = data.requestabsences;
                $scope.calendarRequestAbsences = angular.copy($scope.data);
                $scope.AbsencesTypesArrayValues = data.absences_type;
                for (var k = 0; k < data.requestabsences.length; k++) {
                    tasks.push(data.requestabsences[k]);
                }

                angular.forEach(data.users, function (u, key) {
//                    username.push(u.user.username);
                    username.push({"username": u.user.username , "name":u.user.first_name+" "+u.user.last_name});
                });
                $scope.CollabArrayValues = username;
                for (var i = 0; i < data.absences_type.length; i++) {
                    Types.push(data.absences_type[i].type);
                }
                $scope.AbsencesTypesArrayValues = Types;
                if (data.status)
                    for (var i = 0; i < data.status.length; i++)
                        Status.push(data.status[i].description);
                $scope.StatusTypesArrayValues = Status;
                if ($scope.data != null)
                    for (var i = 0; i < $scope.data.length; i++) {
                        if (!jQuery.isEmptyObject($scope.data[i].startdate)) {
                            $scope.data[i].startdate_month = moment($filter('timezone')(data.requestabsences[i].settings_pay_period.startDate)).format('MM/YYYY');
                            $scope.data[i].startdate_year = moment($filter('timezone')(data.requestabsences[i].startdate)).format('YYYY');
                        }
                        $scope.data[i].requestdate = moment($filter('timezone')($scope.data[i].requestdate)).format('DD/MM/YYYY');
                        $scope.data[i].startdate = moment($filter('timezone')($scope.data[i].startdate)).format('DD/MM/YYYY h:mm A');
                        $scope.data[i].enddate = moment($filter('timezone')($scope.data[i].enddate)).format('DD/MM/YYYY h:mm A');
                        $scope.tasks = $scope.data;
                        $scope.perPage = 10;
                        $scope.maxSize = 5;
                        $scope.setPage = function (pageNo) {
                            $scope.currentPage = pageNo;
                        };
                        $scope.$watch('searchText', function (term) {
                            var obj = term;
                            $scope.filterList = $filter('filter')(tasks, obj);
                            $scope.currentPage = 1;
                        });
                    }
                $scope.loading = false;
            },
            function (error) {
            });
    };

    $scope.TotalAttente = function (filtered) {

        if (typeof filtered !== "undefined") {
            var sum = 0;
            for (var i = 0; i < filtered.length; i++) {
                if ((filtered[i].status != null) && (filtered[i].status.id == 1)) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };
    $scope.TotalValide = function (filtered) {
        if (typeof filtered !== "undefined") {
            var sum = 0;
            for (var i = 0; i < filtered.length; i++) {
                if ((filtered[i].status != null) && (filtered[i].status.id == 2)) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };
    $scope.TotalRefuse = function (filtered) {
        if (typeof filtered !== "undefined") {
            var sum = 0;
            for (var i = 0; i < filtered.length; i++) {
                if ((filtered[i].status != null) && (filtered[i].status.id == 3)) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };
    $scope.TotalBrouillon = function (filtered) {
        if (typeof filtered !== "undefined") {
            var sum = 0;
            for (var i = 0; i < filtered.length; i++) {
                if ((filtered[i].status != null) && (filtered[i].status.id == 4)) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };
    $scope.changestatus = function (id_demande, action) {
        var payload = {
            id: id_demande,
            action: "valider",
        };
        var url = Routing.generate('api_confirmabsencerequest');
        $http.post(url + '/' + id_demande + '/' + action, payload).then(function (response) {
        }).then(function successCallback(response) {
            $scope.datatable();
            if (action == "refuser")
                flash($scope.flashTranslate('FLASH_VALID_REFUS'), '', 'success', 'glyphicon  glyphicon-ok-sign');
            else if (action == "valider")
                flash($scope.flashTranslate('FLASH_VALID_VALID'), '', 'success', 'glyphicon  glyphicon-ok-sign');
        }, function errorCallback(response) {
        });
    }

    $scope.pushToArray = function (array, value) {
        if (array.indexOf(value) === -1) {
            array.push(value);
        }
        return array;
    };
    $scope.SelectInfos = function (id, type) {
        for (var k = 0; k < $scope.tasks.length; k++) {
            if (typeof $scope.tasks[k].id !== "undefined") {
                if ($scope.tasks[k].id == id && 'Absence' == type) {
                    $scope.edit = angular.copy($scope.tasks[k]);
                    $scope.edit.requestdate = $filter('date')($scope.tasks[k].requestdate, "dd/MM/yyyy");
                    $scope.edit.startdate = $filter('date')($scope.tasks[k].startdate, "dd/MM/yyyy");
                    $scope.edit.enddate = $filter('date')($scope.tasks[k].enddate, "dd/MM/yyyy");
                    $scope.edit_requestabsence = angular.copy($scope.tasks[k]);
                    $scope.edit_requestabsence.id = id;
                    $scope.edit_requestabsence.requestdate = $filter('date')($scope.tasks[k].requestdate, "dd/MM/yyyy");
                    $scope.edit_requestabsence.startdate = $filter('date')($scope.tasks[k].startdate, "dd/MM/yyyy h:mm A");
                    $scope.edit_requestabsence.enddate = $filter('date')($scope.tasks[k].enddate, "dd/MM/yyyy h:mm A");
                    if ($scope.tasks[k].requester != null)
                        $scope.edit_requestabsence.requester = ($scope.tasks[k].requester.user.username).toString();
                    //       (($scope.tasks[k].requester == null) ?  $scope.edit_requestabsence.requester = ($scope.tasks[k].requester.user.username).toString() : '');
                    if ($scope.tasks[k].manager != null)
                        $scope.edit_requestabsence.manager = ($scope.tasks[k].manager.id).toString();
                    $scope.edit_requestabsence.absencetype = ($scope.tasks[k].absencetype.id).toString();
                    if ($scope.tasks[k].employee != null)
                        $scope.edit_requestabsence.employee = ($scope.tasks[k].employee.id).toString();
                }
            }
        }
    }

    var date = new Date();
    var d = date.getDate();
    var m = date.getMonth();
    var y = date.getFullYear();
    var currentView = "month";
    $scope.changeTo = 'English';

    $scope.renderCalendar = function (calendar) {
        $timeout(function () {
            if (uiCalendarConfig.calendars[calendar]) {
                uiCalendarConfig.calendars[calendar].fullCalendar('render');
            }
        });
    };

    $scope.removeEvent = function (index) {
        $scope.events.splice(index, 1);
    };

    $scope.alertOnEventClick = function (date, jsEvent, view) {
        $scope.alertMessage = (date.title + ' was clicked ');
    };

    $scope.clearCalendar = function () {
        uiCalendarConfig.calendars['myCalendar3'].fullCalendar('removeEvents');
    };

    $scope.showcalendar = function (calendar) {
        $timeout(function () {
            $scope.renderCalendar('myCalendar3');
        }, 2000);

        if (uiCalendarConfig.calendars['myCalendar3']) {
            $scope.clearCalendar();
            angular.forEach($scope.calendarRequestAbsences, function (demande, key) {
                switch (demande.status.display_name) {
                    case 'Brouillon':
                        color = "#f6d7b8";
                        textColor = '#f57c00';
                        break;
                    case 'Refusé':
                        color = "#f6b8b3";
                        textColor = '#F44336';
                        break;
                    case 'Validé':
                        color = "#bbdbea";
                        textColor = '#3A87AD';
                        break;
                    case 'En attente de validation':
                        color = "rgba(92,184,92,0.15)";
                        textColor = '#5cb85c';
                        break;
                }
                $scope.events.push({
                    id: demande.employee.id,
                    title: demande.employee.user.username,
                    start: new Date(demande.startdate),
                    end: new Date(demande.enddate),
                    color: color,
                    textColor: textColor,
                    stick: true
                });
            });
        } else
            console.log("no calendar found");
    };

    $scope.events = [];

    $scope.dayClick = function (date, allDay, jsEvent, view) {
        $scope.$apply(function () {
            $scope.alertMessage = ('Day Clicked ' + date);
        });
    };

    /* Render Tooltip */
    $scope.eventRender = function (event, element, view) {
        element.attr({
            'tooltip': event.title,
            'tooltip-append-to-body': true
        });
        $compile(element)($scope);
    };

    $scope.alertOnDrop = function (event, dayDelta, minuteDelta, allDay, revertFunc, jsEvent, ui, view) {
        $scope.$apply(function () {
            $scope.alertMessage = ('Event Droped to make dayDelta ' + dayDelta);
        });
    };

    $scope.alertOnResize = function (event, dayDelta, minuteDelta, revertFunc, jsEvent, ui, view) {
        $scope.$apply(function () {
            $scope.alertMessage = ('Event Resized to make dayDelta ' + minuteDelta);
        });
    };

    $scope.eventClick = function (event) {
        $scope.$apply(function () {
            $scope.alertMessage = (event.title + ' is clicked');
        });
    };

    $scope.renderView = function (view) {
        var date = new Date(view.calendar.getDate());
        $scope.currentDate = date.toDateString();
        $scope.$apply(function () {
            $scope.alertMessage = ('Page render with date ' + $scope.currentDate);
        });
    };

    var calendarOnLoad = null;
    var calendarOnLoadCallbacks = [];

    $scope.changeView = function (view) {

        if (uiCalendarConfig.calendars.eventsCalendar) {
            uiCalendarConfig.calendars.eventsCalendar.fullCalendar('changeView', view);
        } else {
            if (!calendarOnLoad) {
                calendarOnLoad = $scope.$watch(function () {
                    try {
                        return uiCalendarConfig.calendars.eventsCalendar;
                    } catch (err) {
                        return null;
                    }
                }, function (calendar) {
                    if (calendar) {
                        calendarOnLoad();
                        calendarOnLoadCallbacks.forEach(function (fn) {
                            fn(calendar);
                        });
                    }
                });
            }
            calendarOnLoadCallbacks.push(function (calendar) {
                calendar.fullCalendar('changeView', view);
            });
        }
    }

    $scope.uiConfig = {
        calendar: {
            height: 450,
            editable: false,
            lang: 'fr',
            header: {
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay'
            },
            eventClick: $scope.alertOnEventClick,
            eventDrop: $scope.alertOnDrop,
            eventResize: $scope.alertOnResize,
            eventRender: $scope.eventRender
        }
    };

    $scope.changeLang = function () {
    };

    $scope.eventSources2 = [$scope.events];

    /***************************************** CHART ABSENCES *******************************************/

// show absences Charts
    $scope.absencesMonthDonutChart = function (period) {
        Ie11.Params(period);
        var router = $resource(Routing.generate('absences_static_data_donut_charts', {_period: period}));
        router.get(null,
            function (data) {
                o_data = [];
                o_no_data = [];
                item = [];
                n_item = [];
                x = '';
                if (data.result !== undefined) {
                    $.each(data.result, function (i, val) {
                        if (i == 'valid') {
                            x = $scope.flashTranslate('VALID');
                        }
                        if (i == 'waiting') {
                            x = $scope.flashTranslate('WAITING');
                        }
                        if (i == 'draft') {
                            x = $scope.flashTranslate('DRAFT');
                        }
                        item.push(x);
                        item.push(parseFloat(val));
                        o_data.push(item);
                        item = [];
                    });
                }
                if ((isNaN(parseFloat(o_data[0][1]))) && (isNaN(parseFloat(o_data[1][1]))) && (isNaN(parseFloat(o_data[2][1])))) {
                    console.log("o_data no data absence");
                    console.log(o_data);
                    n_item.push($scope.flashTranslate('NO_DATA'));
                    n_item.push("100");
                    o_no_data.push(n_item);
                    n_item = [];
                    $scope.generateDonutCharts('#donut-chart-absence', o_no_data, 'donut', ["#DCDCDC"]);
                } else {
                    console.log("data :)");
                    console.log("o_data data absence");
                    console.log(o_data);
                    $scope.generateDonutCharts('#donut-chart-absence', o_data, 'donut', ["#3A87AD", "#5cb85c", "#f57c00"]);
                }

            },
            function (error) {
                OnError(error.statusText);
            });
    };

    $scope.generateDonutCharts = function (elmtId, data, type, colors) {
        c3.generate({
            bindto: elmtId,
            data: {
                columns: data,
                type: type
            },
            donut: {
                label: {
                    format: function (value) {
                        return value;
                    }
                }
            },
            tooltip: {
                format: {
                    value: function (value) {
                        return value;
                    }
                }
            },
            color: {pattern: colors}
        });
    };

// show absences Charts
    $scope.absencesYearsBarCharts = function (year) {
        Ie11.Params(year);
        var router = $resource(Routing.generate('absences_static_data_bar_charts', {_year: year}));
        router.get(null,
            function (data) {
                var data_bar_chart = [], x_axis = ['x'], item_valid = [$scope.flashTranslate('VALID_F')],
                    item_waiting = [$scope.flashTranslate('WAITING')];
                if (data.result !== undefined) {
                    var format = '%Y-%m';
                    if (data.format === 'fr-fr')
                        format = '%m-%Y';

                    $.each(data.result, function (i, el) {
                        x_axis.push(el.month + '-01');
                        item_valid.push(parseFloat(el.valid));
                        item_waiting.push(parseFloat(el.waiting));
                    });
                    data_bar_chart.push(item_valid);
                    data_bar_chart.push(item_waiting);
                    /*****/
                    data_bar_chart.push(x_axis);
                    c3.generate({
                        bindto: '#bar-chart-absence',
                        data: {
                            x: 'x',
                            type: 'bar',
                            columns: data_bar_chart,
                            groups: [
                                [$scope.flashTranslate('VALID_F'), $scope.flashTranslate('WAITING')]
                            ],
                        },
                        axis: {
                            x: {
                                type: 'timeseries',
                                tick: {
                                    format: format,
                                    centered: true,
                                }
                            },
                            y: {
                                label: {
                                    text: $scope.flashTranslate('NDAYS'),
                                    position: 'outer-middle'
                                }
                            }
                        },
                        color: {pattern: ["#3A87AD", "#5cb85c"]},
                        tooltip: {
                            format: {
                                //title: function (d) { return 'Data ' + d; },
                                name: function (name, ratio, id, index) {
                                    return name;
                                },
                                value: function (value, ratio, id) {
                                    return value + ' ' + $scope.flashTranslate('DAYS');
                                },
                                //value: d3.format(',') // apply this format to both y and y2
                            }
                        }
                    });
                    /*****/
                }
            },
            function (error) {
                OnError(error.statusText);
            });
    };
    /***************** show data grid  chart **************/
    $scope.absencesMonthGridChart = function (period) {
        Ie11.Params(period);
        $scope.loading = true;
        var infos = $resource(Routing.generate('absences_static_data_grid_charts', {_period: period}));
        infos.get(
            function (data) {

                $scope.data = data.result;
                $scope.loading = false;
            },
            function (error) {
            });
    };
    
    // check the Buttons ID who was submitted
    $scope.etat = function (IDvalider, IDsauvegarde, action) {
        console.log( $scope);
        console.log( $scope.requestabsence);
        if (action == 'New') {
            if ($(IDvalider).is(":focus")) {
                $scope.requestabsence.action = "valider";
            }
            if ($(IDsauvegarde).is(":focus")) {
                $scope.requestabsence.action = "sauvegarder";
            }
        }
        if (action == 'Edit') {
            if ($(IDvalider).is(":focus")) {
                $scope.edit.status = "1";
            }
            if ($(IDsauvegarde).is(":focus")) {
                $scope.edit.status = "4";
            }
        }

    }    
});
