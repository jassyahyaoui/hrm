//"use strict";
app.controller('collaboraterCtrl', function ($scope, $resource, $filter, $http, Utils, $rootScope) {
    //Start Skills Section
    $scope.IsEmptyObject = function (param) {
        var result;
        if (typeof (param) === 'undefined') {
            result = true;
        } else
        if (typeof (param) === 'object') {
            result = true;
        } else {
            result = false;
        }
        return result;

    }

    $scope.new = {};
    $scope.new.skills = {};
    $scope.new.skills.study = [{id: 'study 1'}];
    $scope.new.skills.study.checked = {};
    $scope.new.skills.company = [{id: 'company 1'}];
    $scope.new.skills.company.checked = {};
    $scope.new.skills.language = [{id: 'language 1'}];
    $scope.new.skills.language.checked = {};
    $scope.new.skills.technology = [{id: 'technology 1'}];
    $scope.new.skills.technology.checked = {};

//    $scope.edit = {};
//    $scope.edit.skills = {};
//    $scope.edit.skills.study = [{id: 'study 1'}];    
//    $scope.edit.skills.study.checked = {};

    $scope.getChecked = function (param, type) {
        if (type == 'NEW') {
            $scope.new.skills.study.checked = param;
        } else if (type == 'EDIT') {
            $scope.edit.skills.study.checked = param;
        } else if (type == 'NEWC') {
            $scope.new.skills.company.checked = param;
        } else if (type == 'NEWL') {
            $scope.new.skills.language.checked = param;
        }else if (type == 'NEWT') {
            $scope.new.skills.technology.checked = param;

        } else if (type == 'EDITC') {
            $scope.edit.skills.company.checked = param;

        } else if (type == 'EDITL') {
            $scope.edit.skills.language.checked = param;

        } else if (type == 'EDITT') {
            $scope.edit.skills.technology.checked = param;

        }
    }
    
    $scope.up = function (index, PassedScope) {

        if (index <= 0)
            return;
        var temp = PassedScope[index];
        PassedScope[index] = PassedScope[index - 1];
        PassedScope.checked--;
        PassedScope[index - 1] = temp;
    };

    $scope.down = function (index, PassedScope, ContainerLength) {
        if (index == ContainerLength - 1)
            return;
        var temp = PassedScope[index];
        PassedScope[index] = PassedScope[index + 1];
        PassedScope.checked++;
        PassedScope[index + 1] = temp;
    };


    $scope.addNewChoice = function (param) {
        var newItemNo = param.length + 1;
        param.push({'id': 'choice' + newItemNo});
    };

    $scope.removeChoice = function (param, PasserScope, index, type) {
        param.splice(index, 1);

    };

    $scope.companies = [{id: 'company 1'}];    
//    $scope.companies.checked = {};
    $scope.languages = [{id: 'language 1'}];
    $scope.technlogies = [{id: 'technlogy 1'}];

    $scope.getTags = function () {
        $scope.loading = true;
        var infos = $resource(Routing.generate('api_hrm_settings_tag_list'));
        infos.get(
                function (data) {
                    var tags = [];
                    angular.forEach(data.tags, function (u, key) {
                        tags.push({"text": u.display_name, "id": u.id});
                    });
                    $scope.tags = tags;
                },
                function (error) {
                });
    };

    $scope.loadTags = function (query) {
        var tags = $scope.tags; // $http.get(Routing.generate('api_hrm_settings_tag_list'));
        return tags.filter(function (tag) {
            return tag.text.toLowerCase().indexOf(query.toLowerCase()) != -1;
        });
    };

    var unique = function (origArr) {
        var newArr = [],
                origLen = origArr.length,
                found, x, y;

        for (x = 0; x < origLen; x++) {
            found = undefined;
            for (y = 0; y < newArr.length; y++) {
                if (origArr[x] === newArr[y]) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                newArr.push(origArr[x]);
            }
        }
        return newArr;
    };

    $scope.save = function (type) {
        // $scope.IsDisabled = true;
        var url = $resource(Routing.generate('employee_skills_add'));
        //
        $scope.new.employeeId = $scope.employeeId;
        url.save($scope.new.skills,
                function (successResult) {
                    $scope.IsDisabled = false;
                    $scope.datatable();
                    setTimeout(function () {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    }, 0);
                    setTimeout(function () {
                        delete $scope.new.skills;
                        delete $scope.studies;
                        $scope.studies = [{id: 'study 1'}];
                        delete $scope.companies;
                        $scope.companies = [{id: 'company 1'}];
                        delete $scope.languages;
                        $scope.languages = [{id: 'language 1'}];
                        delete $scope.technlogies;
                        $scope.technlogies = [{id: 'technlogy 1'}];
                        $('.close').click();
                        $scope.IsDisabled = false;
                    }, 0);
                },
                function (errorResult) {
                    // flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    // $scope.IsDisabled = false;
                });
    };

    $scope.updateSkills = function () {
        var enable = $scope.delete('remove_skills', $scope.edit.skills.id);
        setTimeout(function () {
            if (enable) {
                $scope.add('employee_skills_update_add');

            }
        }, 100);
        $scope.datatable();
    };

    $scope.add = function (Route) {
        var url = $resource(Routing.generate(Route));
        url.save($scope.edit.skills,
                function (successResult) {
                    $scope.datatable();
                    $('.close').click();
                },
                function (errorResult) {
                    $scope.datatable();
                });
    };

    $scope.delete = function (route, id) {
        var enable = false;
        if (!isNaN(id)) {
            var remove = $resource(Routing.generate(route, {id: id}));
            remove.delete(
                    function (data) {
                        $scope.datatable();
                    },
                    function (error) {
                    });
        }
        enable = true;
        return enable;
    };

//End Skills Section


    $scope.uploadcontractAttachment = function (file) {
        $scope.editcontrat.contractAttachment = {};
        $scope.editcontrat.contractAttachment.path = file.url;
        $scope.editcontrat.contractAttachment.mimetype = file.mimetype;
    };

    $scope.uploadidentificationAttachment = function (file) {
        $scope.editcontrat.identificationAttachment = {};
        $scope.editcontrat.identificationAttachment.path = file.url;
        $scope.editcontrat.identificationAttachment.mimetype = file.mimetype;
    };

    $scope.uploadautorsationAttachment = function (file) {
        $scope.editcontrat.autorsationAttachment = {};
        $scope.editcontrat.autorsationAttachment.path = file.url;
        $scope.editcontrat.autorsationAttachment.mimetype = file.mimetype;
    };

    $scope.uploadvitalCardAttachment = function (file) {
        $scope.editcontrat.vitalCardAttachment = {};
        $scope.editcontrat.vitalCardAttachment.path = file.url;
        $scope.editcontrat.vitalCardAttachment.mimetype = file.mimetype;
    };

    $scope.uploadsingleDeclarationAttachment = function (file) {
        $scope.editcontrat.singleDeclarationAttachment = {};
        $scope.editcontrat.singleDeclarationAttachment.path = file.url;
        $scope.editcontrat.singleDeclarationAttachment.mimetype = file.mimetype;
    };

    $scope.uploadaffiliationPensionAttachment = function (file) {
        $scope.editcontrat.affiliationPensionAttachment = {};
        $scope.editcontrat.affiliationPensionAttachment.path = file.url;
        $scope.editcontrat.affiliationPensionAttachment.mimetype = file.mimetype;
    };

    $scope.uploadaffiliationHealthAttachment = function (file) {
        $scope.editcontrat.affiliationHealthAttachment = {};
        $scope.editcontrat.affiliationHealthAttachment.path = file.url;
        $scope.editcontrat.affiliationHealthAttachment.mimetype = file.mimetype;
    };

    $scope.uploadaffiliationComplementaryPensionAttachment = function (file) {
        $scope.editcontrat.affiliationComplementaryPensionAttachment = {};
        $scope.editcontrat.affiliationComplementaryPensionAttachment.path = file.url;
        $scope.editcontrat.affiliationComplementaryPensionAttachment.mimetype = file.mimetype;
    };

    $scope.uploadotherAttachment = function (file) {
        $scope.editcontrat.otherAttachment = {};
        $scope.editcontrat.otherAttachment.path = file.url;
        $scope.editcontrat.otherAttachment.mimetype = file.mimetype;
    };

    $scope.contractSearch = '';
    $scope.filterByContract = function (contract) {
        if (contract === 'All') {
            $scope.contractSearch = '';
        } else {
            $scope.contractSearch = contract;
        }
    }

    $scope.changeLanguage = function (key) {
        $rootScope.$broadcast('changeLanguage', key);

    };

    $scope.$on('changeLanguage', function (event, args) {
        $translate.use(args);

    });

    $scope.changeLanguage = function (key) {
        $translate.use('fr');
    };
    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {

        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    }
    $scope.userData = {};

    $scope.newemployee = function (next) {
        var postaction = $resource(Routing.generate('api_post_employee'), null,
                {
                    'post': {method: 'POST'}
                });
        postaction.post(null, $scope.new,
                function (data) {

                    $scope.datatable();
                    $scope.new.skills = {};
                    $scope.new.skills.employeeId = data.employeeId;
                    for (var k = 0; k < $scope.tasks.length; k++) {
                        if ($scope.tasks[k].employee_contract != null) {
                            if ($scope.tasks[k].employee_contract.id = data.employee) {
                                $scope.editcontrat = angular.copy($scope.tasks[k].employee_contract);
                                break;
                            }
                        }
                    }
                    if ($scope.editcontrat) {
                        $scope.editcontrat.empStatus = null;
                        $scope.editcontrat.insuranceContractRef1 = null;
                        $scope.editcontrat.insuranceContractRef2 = null;
                        $scope.editcontrat.contractCustomCondition = null;
                        $scope.editcontrat.salEchelon = null;
                        $scope.editcontrat.salClassification = null;
                        $scope.editcontrat.notes = null;
                        $scope.editcontrat.salJobTitle = null;
                        $scope.editcontrat.salGrade = null;
                        $scope.editcontrat.ribAttachments = null;
                        $scope.editcontrat.salJoinedDate = null;
                        $scope.editcontrat.salLeftDate = null;
                        $scope.editcontrat.salSeniorityDate = null;
                        $scope.editcontrat.empStatus = null;
                        $scope.editcontrat.contractType = null;
                        $scope.editcontrat.empStatus = null;
                        $scope.editcontrat.convention = null;
                        $scope.editcontrat.contract_number = null;
                        $scope.editcontrat.work_timetable = null;
                        $scope.editcontrat.working_time = null;
                        $scope.editcontrat.gross_annual_base_salary = null;
                        $scope.editcontrat.nb_of_installments = null;
                        $scope.editcontrat.fixed_rest_days_agreed = null;
                        $scope.editcontrat.contractAttachment = null;
                        $scope.editcontrat.identificationAttachment = null;
                        $scope.editcontrat.autorsationAttachment = null;
                        $scope.editcontrat.vitalCardAttachment = null;
                        $scope.editcontrat.singleDeclarationAttachment = null;
                        $scope.editcontrat.affiliationPensionAttachment = null;
                        $scope.editcontrat.affiliationHealthAttachment = null;
                        $scope.editcontrat.affiliationComplementaryPensionAttachment = null;
                        $scope.editcontrat.otherAttachment = null;
                    }

                    $scope.new.first_name = null;
                    $scope.new.last_name = null;
                    $scope.new.email = null;
                    $scope.new.gender = null;
                    $scope.new.name_usage = null;
                    $scope.new.birth_date = null;
                    $scope.new.department_birth = null;
                    $scope.new.place_birth = null;
                    $scope.new.nation_code = null;
                    $scope.new.address = null;
                    $scope.new.commune = null;
                    $scope.new.zip_code = null;
                    $scope.new.country_code = null;
                    $scope.new.ssn_num = null;
                    $scope.new.is_handicape = null;
                    $scope.new.num_children = null;
                    $scope.new.payment_method = null;
                    $scope.new.iban = null;
                    $scope.new.bic = null;
                    $scope.new.domiciliation = null;
                    $scope.new.study_type = null;
                    $scope.new.study_level = null;
                    $scope.new.study_level = null;
                    $scope.new.sexe = null;
                    $scope.new.paymentmethod = null;
                    $scope.new.studytype = null;
                    $scope.new.studylevel = null;
                    $scope.new.comments = null;
                    $scope.new.nation_code = 'FR';
                    $scope.new.country_code = 'FR';
                    $scope.new.country_nationality = 'FR';
                    $scope.new.family_status = null;
                    $scope.new.employee_number = null;
                    //   $scope.new.avatars.path= null;
                    if ($scope.createother == true) {
                        return;
                    } else {
                        if (next == "contract") {
                            $scope.switchModals("#newemployee", "#ContratCollaborateur");
                        } else if (next == "skills") {
                            $('#btn_new_validate_collaborator').on('click', function () {
                                $('#formComptencesAdd a[href="#formComptencesAdd"]').tab('show');
                            });
                            $('.CompetenceCLASS')[0].click();
                            $('#carteIdentiteTag').removeAttr('class');
                            $('#CompetenceTag').removeAttr("class");
                            $('#CompetenceTag').attr('class', 'active appear');
                            $('#formCarteIdentiteAdd').removeClass('active');
                            $('#formComptencesAdd').addClass('active');
                        } else {
                            $('.close').click();
                            $scope.datatable();
                        }
                    }
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                },
                function (error) {
                    OnError($scope.flashTranslate('FLASH_INVALID'));
                });
        $scope.datatable();
    };

    $scope.InitFilterContractType = function () {
        var ContractTypes = [];
        var infos = $resource(Routing.generate('apiTypeContracts_list'));
        infos.get(
                function (data) {
                    $scope.ContractTypesArrayValues = data.typecontracts;
                    var long = $scope.ContractTypesArrayValues.length;
                    for (var i = 0; i < long; i++) {
                        ContractTypes.push($scope.ContractTypesArrayValues[i].display_name);
                    }
                    $scope.ContractTypes = ContractTypes;
                },
                function (error) {
                });
    }

    $scope.update = function (route, id) {
        if (!id) {
            flash($scope.flashTranslate('MSG_SELECT_CONTRAT'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            return;
        }
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        edit.update(null, $scope.editcontrat,
                function (data) {
                    $('.close').click();
                    if (typeof $scope.editcontrat !== "undefined")
                        $scope.editcontrat.id = null;
                    $scope.editcontrat.salLeftDate = null;
                    $scope.editcontrat = null;
//                         $scope.editcontrat.salLeftDate = null;
//                         $scope.editcontrat.salLeftDate = null;
                    $scope.datatable();
                    flash($scope.flashTranslate('MSG_CONTRAT_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                },
                function (error) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });

        $scope.datatable();
    };

    $scope.closewindow = function () {
        $('.close').click();
    }

    $scope.updateprofile = function (route, id, action) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        edit.update(null, $scope.edit,
                function (data) {
                    if (action == "contract")
                        $scope.switchModals("#EditCollaborator", "#ContratCollaborateur");
                    else
                        $("#EditCollaborator").modal('hide');
                    if (typeof $scope.new.avatars !== "undefined")
                        $scope.new.avatars.path = null;
                    $scope.edit.avatars = null;
                    $scope.datatable();
                    flash($scope.flashTranslate('MSG_PROFILE_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                },
                function (error) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };

    $scope.switchModals = function (fromModal, toModal) {
        $(fromModal).on('hidden.bs.modal', function (e) {
            $(toModal).modal('show');
            $(fromModal).off();
        });
        $(fromModal).modal('hide');
    }

    $scope.datatable = function () {
        $scope.loading = true;
        var infos = $resource(Routing.generate('api_dashboard_collaborateur'));
        infos.get(
                function (data) {
//                    $scope.tasks = [];
                    //var firstName = [];
                    var username = [];
                    var ContractTypes = [];
                    $scope.response = data.collaborateur;
                    angular.forEach(data.users, function (u, key) {
                        username.push({"username": u.user.username, "name": u.user.first_name + " " + u.user.last_name});
                    });
                    $scope.CollabArrayValues = username;
                    if ($scope.response)
                        for (var i = 0; i < $scope.response.length; i++) {
                            $scope.tasks = $scope.response;
//                            $scope.FirstNameArrayValues = $scope.pushToArray(firstName, $scope.tasks[i].user.first_name);
                            $scope.perPage = 10;
                            $scope.maxSize = 5;
                            $scope.setPage = function (pageNo) {
                                $scope.currentPage = pageNo;
                            };
                            $scope.$watch('searchText', function (term) {
                                var obj = term;
                                $scope.filterList = $filter('filter')($scope.tasks, obj);
                                $scope.currentPage = 1;
                            });
                            // filter by contrat type
                            $scope.$watch('contractTypesSearch', function (term) {
                                var obj = term;
                                $scope.filterList = $filter('filter')($scope.tasks, obj);
                                $scope.currentPage = 1;
                            });
                        }
                    $scope.loading = false;
                },
                function (error) {
                });
    };

    $scope.pushToArray = function (array, value) {
        if (array.indexOf(value) === -1) {
            array.push(value);
        }
        return array;
    };

    $scope.SelectInfos = function (id, type) {
        $scope.editcontrat = null;
        for (var k = 0; k < $scope.tasks.length; k++) {
            if (typeof $scope.tasks[k].id !== "undefined") {
                if ($scope.tasks[k].id == id && 'Collaborateur' == type) {
                    var editStudies = [];
                    $scope.edit = angular.copy($scope.tasks[k]);
// console.log($scope.edit);
                    $scope.edit.skills = angular.copy($scope.tasks[k].skills);

                    if ($scope.edit.skills !== null) {

                        $scope.edit.skills.employeeId = $scope.edit.id;
                        // BEGIN
                        $scope.edit.skills.study = angular.copy($scope.tasks[k].skills.skills_study);
                        $scope.edit.skills.study.checked = {};
                        for (var s = 0; s < $scope.edit.skills.study.length; s++) {
                            $scope.edit.skills.study[s].study_type = ($scope.edit.skills.study[s].study_type.id).toString();
                            $scope.edit.skills.study[s].study_level = ($scope.edit.skills.study[s].study_level.id).toString();
                        }
                        $scope.edit.skills.company = angular.copy($scope.tasks[k].skills.skills_company);
                        $scope.edit.skills.company.checked = {};
                        for (var s = 0; s < $scope.edit.skills.company.length; s++) {
                            $scope.edit.skills.company[s].job = ($scope.edit.skills.company[s].job.id).toString();
                            $scope.edit.skills.company[s].sector = ($scope.edit.skills.company[s].industrysector.id).toString();
                        }

                        $scope.edit.skills.language = angular.copy($scope.tasks[k].skills.skills_language);
                        $scope.edit.skills.language.checked = {};
                        for (var s = 0; s < $scope.edit.skills.language.length; s++) {
                            $scope.edit.skills.language[s].language = ($scope.edit.skills.language [s].language.id).toString();
                            $scope.edit.skills.language[s].language_level = ($scope.edit.skills.language [s].language_level.id).toString();
                        }
                        $scope.edit.skills.technology = angular.copy($scope.tasks[k].skills.skills_technology);
                        $scope.edit.skills.technology.checked = {};
                        for (var s = 0; s < $scope.edit.skills.technology.length; s++) {
                            $scope.edit.skills.technology[s].technology = ($scope.edit.skills.technology [s].technology.id).toString();
                            $scope.edit.skills.technology[s].technology_level = ($scope.edit.skills.technology [s].technology_level.id).toString();
                        }

                        $scope.edit.skills.other = [];
                        for (var s = 0; s < $scope.tasks[k].skills.skills_other.length; s++) {
                            $scope.edit.skills.other.push($scope.tasks[k].skills.skills_other[s].tag);
                        }
                        $scope.edit.skills.other = unique($scope.edit.skills.other);
                    } else {
                        $scope.edit.skills = {};
                        $scope.edit.skills.employeeId = $scope.edit.id;
                        $scope.edit.skills.study = [{id: 'study 1'}];
                        $scope.edit.skills.company = [{id: 'company 1'}];
                        $scope.edit.skills.language = [{id: 'language 1'}];
                        $scope.edit.skills.technology = [{id: 'technlogy 1'}];
                        $scope.edit.skills.other = [];
                    }
                    // END

                    $scope.edit.first_name = $scope.tasks[k].user.first_name;
                    $scope.edit.last_name = $scope.tasks[k].user.last_name;
                    $scope.edit.email = $scope.tasks[k].user.email;
                    $scope.edit.name_usage = $scope.tasks[k].user.name_usage;


                    (($scope.tasks[k].nation_code == null) ? $scope.edit.nation_code = 'FR' : 'FR');
                    (($scope.tasks[k].country_code == null) ? $scope.edit.country_code = 'FR' : 'FR');
                    (($scope.tasks[k].country_nationality == null) ? $scope.edit.country_nationalitye = 'FR' : 'FR');
                    $scope.editcontrat = angular.copy($scope.tasks[k].employee_contract);
                    $scope.editcontrat.id = $scope.tasks[k].employee_contract.id;
                    $scope.editcontrat.contractType = $scope.tasks[k].employee_contract.contractType;
                    $scope.editcontrat.convention = $scope.tasks[k].employee_contract.convention;
                    if ($scope.tasks[k].employee_contract.contract_number != null)
                        $scope.editcontrat.contract_number = $scope.tasks[k].employee_contract.contract_number;
                    if ($scope.tasks[k].employee_contract.empStatus != null)
                        $scope.editcontrat.empStatus = $scope.tasks[k].employee_contract.empStatus;
                    if ($scope.tasks[k].employee_contract.insuranceContractRef1 != null)
                        $scope.editcontrat.insuranceContractRef1 = $scope.tasks[k].employee_contract.insuranceContractRef1;
                    if ($scope.tasks[k].employee_contract.insuranceContractRef2 != null)
                        $scope.editcontrat.insuranceContractRef2 = $scope.tasks[k].employee_contract.insuranceContractRef2;
                    if ($scope.tasks[k].employee_contract.contractCustomCondition != null)
                        $scope.editcontrat.contractCustomCondition = $scope.tasks[k].employee_contract.contractCustomCondition;
                    if ($scope.tasks[k].employee_contract.salEchelon != null)
                        $scope.editcontrat.salEchelon = $scope.tasks[k].employee_contract.salEchelon;
                    if ($scope.tasks[k].employee_contract.salClassification != null)
                        $scope.editcontrat.salClassification = $scope.tasks[k].employee_contract.salClassification;
                    if ($scope.tasks[k].employee_contract.notes != null)
                        $scope.editcontrat.notes = $scope.tasks[k].employee_contract.notes;
                    if ($scope.tasks[k].employee_contract.salJobTitle != null)
                        $scope.editcontrat.salJobTitle = $scope.tasks[k].employee_contract.salJobTitle;
                    if ($scope.tasks[k].employee_contract.salGrade != null)
                        $scope.editcontrat.salGrade = $scope.tasks[k].employee_contract.salGrade;
                    if ($scope.tasks[k].employee_contract.ribAttachments != null)
                        $scope.editcontrat.ribAttachments = $scope.tasks[k].employee_contract.ribAttachments;
                    (($scope.tasks[k].employee_contract.salJoinedDate == null) ? $scope.editcontrat.salJoinedDate = $filter('date')($scope.tasks[k].employee_contract.salJoinedDate, "dd-MM-yyyy") : '');
                    if ($scope.tasks[k].employee_contract.salLeftDate != null)
                        $scope.editcontrat.salLeftDate = $filter('date')($scope.tasks[k].employee_contract.salLeftDate, "dd-MM-yyyy");
                    if ($scope.tasks[k].employee_contract.salSeniorityDate != null)
                        $scope.editcontrat.salSeniorityDate = $filter('date')($scope.tasks[k].employee_contract.salSeniorityDate, "dd-MM-yyyy");
                    $scope.id = $scope.tasks[k].id;
                    $scope.edit.birth_date = $filter('date')($scope.tasks[k].birth_date, "dd/MM/yyyy");
                    if ($scope.tasks[k].gender != null)
                        $scope.edit.sexe = ($scope.tasks[k].gender.id).toString();
                    if ($scope.tasks[k].payment_method != null)
                        $scope.edit.paymentmethod = ($scope.tasks[k].payment_method.id).toString();
                    if ($scope.tasks[k].study_type != null)
                        $scope.edit.studytype = ($scope.tasks[k].study_type.id).toString();
                    if ($scope.tasks[k].study_level != null)
                        $scope.edit.studylevel = ($scope.tasks[k].study_level.id).toString();
                    if ($scope.tasks[k].family_status != null)
                        $scope.edit.family_status = ($scope.tasks[k].family_status.id).toString();
                }
            }
        }
    }

    $scope.SelectInfosShowContrat = function (id, type) {
        for (var k = 0; k < $scope.tasks.length; k++) {
            if (typeof $scope.tasks[k].id !== "undefined") {
                if ($scope.tasks[k].id == id && 'Collaborateur' == type) {
                    $scope.showcontrat = angular.copy($scope.tasks[k].employee_contract);
                    (($scope.tasks[k].employee_contract.empStatus != null) ? $scope.showcontrat.empStatus = $scope.tasks[k].employee_contract.empStatus.display_name : '');
                    (($scope.tasks[k].employee_contract.convention != null) ? $scope.showcontrat.convention = $scope.tasks[k].employee_contract.convention : '');
                    (($scope.tasks[k].employee_contract.contractType != null) ? $scope.showcontrat.contract = $scope.tasks[k].employee_contract.contractType.display_name : '');
                    (($scope.tasks[k].employee_contract != null) ? $scope.edit.contract = angular.copy($scope.tasks[k].employee_contract) : null);

//                    if ($scope.tasks[k].employee_contract.contractType != null)
//                        $scope.showcontrat.contract = $scope.tasks[k].employee_contract.contractType.display_name;
//                    if ($scope.tasks[k].employee_contract != null)
//                        $scope.edit.contract = angular.copy($scope.tasks[k].employee_contract);
                }
            }
        }
    }


    $scope.StatusList = function () {
        var StatusList = $resource(Routing.generate('apiSettingsEmpStatus_list'));
        StatusList.get({display: 'full'},
                function (data) {
                    if (data.settingsEmpStatus.length > 0)
                        $scope.status = data.settingsEmpStatus;
                    else
                        $scope.status = null;
                },
                function (error) {
                    //            flash(error.statusText, '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };
    $scope.ConventionList = function () {

        var ConventionList = $resource(Routing.generate('apiSettingsConvention_list'));
        ConventionList.get({display: 'full'},
                function (data) {
                    if (data.conventions.length > 0) {
                        $scope.conventions = data.conventions;
                    } else
                        $scope.conventions = null;
                },
                function (error) {
                    //    flash(error.statusText, '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };
    $scope.ContractTypeList = function () {
        var contractList = [];
        var ContractTypeList = $resource(Routing.generate('apiTypeContracts_list'));

        $scope.typecontracts = ContractTypeList.get({display: 'full'},
                function (data) {
                    for (var i = 0; i < data.typecontracts.length; i++) {
                        contractList.push(data.typecontracts[i].display_name);
                    }
                    $scope.contractList = contractList;
                },
                function (error) {
                    //            flash(error.statusText, '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };

})
        ;
