app.controller('settingsCtrl', function ($scope, $resource, $filter) {

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {

        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    }
    $scope.IsDisabled = false;

    $scope.initFields = function (local, format) {
        $scope.preference = {};
        $scope.today = {};
        $scope.preference.local = local;
        $scope.preference.format = format;
        $scope.today = new Date();
    };

    $scope.Check = function (id) {
        $scope.loading = true;
        $scope.connectedId = id;
        var getaction = $resource(Routing.generate('preferences_check_action', {id: id}));
        $scope.action = getaction.get(
            function (data) {
                $scope.data = data;

                $scope.initFields(data.content.local, data.content.format);
                if (data.content.action === 'ADD') {
                    $scope.addAction = true;
                    $scope.updateAction = false;
                } else if (data.content.action === 'UPDATE') {
                    $scope.addAction = false;
                    $scope.updateAction = true;
                }

                $scope.loading = false;
            },
            function (error) {
            });
    };

    $scope.Action = function () {
        if ($scope.addAction === true) {
            $scope.save();
        } else if ($scope.updateAction === true) {
            $scope.update($scope.data.content.preferenceId);
            $scope.initFields($scope.preference.local, $scope.preference.format);
        }
    };

    $scope.save = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate('preferences_add'));
        url.save($scope.preference,
            function (successResult) {
                location.reload();
                flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.IsDisabled = false;
                flash('Votre demande a été ajouté avec succès', '', 'success', 'glyphicon  glyphicon-ok-sign');

                    $('#RedirectURL')[0].click();
            },
            function (errorResult) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                $scope.IsDisabled = false;
            });
    }


//update general method
    $scope.update = function (id) {
        $scope.IsDisabled = true;
        var edit = $resource(Routing.generate('preferences_update', {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        edit.update(null, $scope.preference,
            function (data) {
                location.reload();
                $scope.IsDisabled = false;
                flash('Votre demande a été modifié avec succès', '', 'success', 'glyphicon  glyphicon-ok-sign');
                    $('#RedirectURL')[0].click();
            },
            function (error) {
                $scope.IsDisabled = false;
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {

        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    }

});
