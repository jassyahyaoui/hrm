app.controller("paydayvariationsCtrl", function ($scope, $resource, $filter, Global) {
    
    $scope.CurrentDate = function () {
       return $filter('date')(new Date(), "dd/MM/yyyy");
    };
    
    $scope.setValidator = function () {
        console.log("Bonjour je selectionne le validateur");
        $("#paydayvariationsbundle_paydayvariations_validator").val($("#paydayvariationsbundle_paydayvariations_validator option:eq(1)").val());
        if (typeof angular.element($('#paydayvariationsCtrl')).scope() !== undefined)
            if (typeof angular.element($('#paydayvariationsCtrl')).scope().new !== undefined)
                angular.element($('#paydayvariationsCtrl')).scope().new.validator.id = $("#paydayvariationsbundle_paydayvariations_validator option:eq(1)").val();
        console.log("le validateur est selectionné ?");
    };
    
    $scope.getValidator = function () {
       return ($("#paydayvariationsbundle_paydayvariations_validator option:eq(1)").val()).toString();
    };
    
    $scope.RemoveAll = function () {
        Global.Remove($scope);
    }
    $scope.uploadNewIkAttachment = function (file) {
        $scope.new.attachments = {};
        $scope.new.attachments.path = file.url;
        $scope.new.attachments.mimetype = file.mimetype;

    };
    $scope.uploadEditIkAttachment = function (file) {
        $scope.edit.attachments = {};
        $scope.edit.attachments.path = file.url;
        $scope.edit.attachments.mimetype = file.mimetype;
    };
    $scope.flashText = {};

    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    }

    $scope.typeSearch = '';
    $scope.filterByType = function (type) {
        if (type === 'All') {
            $scope.typeSearch = '';
        } else {
            $scope.typeSearch = type;
        }
    }
    $scope.dateSearch = '';
    $scope.filterByDatePicker = function (type) {
        if (type === 'All') {
            $scope.dateSearch = '';
        }
    }


    $scope.datatable = function () {
        $scope.loading = true;
        $scope.blockloader = true;
        var dataResult = $resource(Routing.generate('payday_list'));

        $scope.dataResult = dataResult.get({display: 'full'},
                function (ResponseData) {

                    var tasks = [];
                    var username = [];
                    $scope.tasks = ResponseData.result;

                    $scope.FiltersData = ResponseData.result;
                    $scope.TotalAttente(ResponseData.result);
                    $scope.TotalValide(ResponseData.result);
                    $scope.TotalRefuse(ResponseData.result);
                    $scope.TotalBrouillon(ResponseData.result);
                    // $scope.tasks = $scope.data;
                    for (var i = 0; i < ResponseData.result.length; i++) {

                        ResponseData.result[i].date_month = $filter('date')(ResponseData.result[i].date, 'MM/yyyy');
                        ResponseData.result[i].date = moment($filter('timezone')(ResponseData.result[i].date)).format('DD/MM/YYYY');

                        ResponseData.result[i].contributor.user.username_display = ResponseData.result[i].contributor.user.first_name + ' ' + ResponseData.result[i].contributor.user.last_name;
                        tasks.push(ResponseData.result[i]);
                    }
                    for (var i = 0; i < ResponseData.users.length; i++) {
                        username.push(ResponseData.users[i].user.username);
                    }

                    $scope.CollabArrayValues = username;

                    $scope.perPage = 10;
                    $scope.maxSize = 5;
                    $scope.setPage = function (pageNo) {
                        $scope.currentPage = pageNo;
                    };
                    $scope.$watch('searchText', function (term) {
                        var obj = term;
                        $scope.filterList = $filter('filter')(tasks, obj);
                        $scope.currentPage = 1;
                    });

                    $scope.loading = false;
                    $scope.blockloader = false;
                },
                function (error) {
                });

    };

    $scope.SelectInfos = function (id, type) {
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.tasks.length; k++) {
            if (typeof $scope.tasks[k].id !== "undefined") {
                if ($scope.tasks[k].id == id && 'PaydayVariations' == type) {
                    $scope.edit = $scope.tasks[k];
                    console.log($scope.edit);
                    $scope.edit.contributor.id = ($scope.tasks[k].contributor.id).toString();
                    $scope.edit.validator.id = ($scope.tasks[k].validator.id).toString();
                    $scope.edit.requester.id = ($scope.tasks[k].requester.id).toString();
                    if (typeof $scope.edit.nature !== "undefined") {
                        if ($scope.edit.nature !== null) {
                            $scope.edit.nature.id = ($scope.tasks[k].nature.id).toString();
                        }
                    }
                    $scope.edit.settings_status.id = ($scope.tasks[k].settings_status.id).toString();
                    // $scope.edit.date = moment($filter('timezone')($scope.tasks[k].date)).format('DD/MM/YYYY');
                }
            }
            // console.log($scope.edit);
        }
        $scope.loading = false;
        $scope.blockloader = false;
    }

    // check the Buttons ID who was submitted
    $scope.etat = function (IDvalider, IDsauvegarde, action) {
        if (action == 'New') {
            if ($(IDvalider).is(":focus")) {
                $scope.new.status = "1";
            }
            if ($(IDsauvegarde).is(":focus")) {
                $scope.new.status = "4";
            }
        }
        if (action == 'Edit') {
            if ($(IDvalider).is(":focus")) {
                $scope.edit.status = "1";
            }
            if ($(IDsauvegarde).is(":focus")) {
                $scope.edit.status = "4";
            }
        }

    }

    $scope.save = function (route) {

        $scope.etat('#ValiderPayDay', '#SauvegarderPayDay', 'New');
        $scope.IsDisabled = true;

        var url = $resource(Routing.generate(route));

        url.save($scope.new,
                function (data) {

                    if ($('#checkbox').is(":checked")) {

                        $scope.new.amount = 0;
                        $scope.new.nbre_months = 0;
                        $scope.new.date = null;
                        $scope.new.type = null;
                        $scope.new.comment = null;

                        $('#checkbox').attr('checked', false); // Unchecks it
                        $scope.IsDisabled = false;

                    } else {
                        $scope.new.amount = 0;
                        $scope.new.nbre_months = 0;
                        $scope.new.date = null;
                        $scope.new.type = null;
                        $scope.new.comment = null;

                        $('.close').click();
                        $scope.IsDisabled = false;

                    }
                    setTimeout(function () {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    }, 0);

                    $scope.datatable();
                },
                function (error) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    }

    $scope.update = function (route, id) {
        $scope.IsDisabled = true;

        $scope.etat('#EditValiderPayDay', '#EditSauvegarderPayDay', 'Edit');
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        edit.update(null, $scope.edit,
                function (data) {
                    $scope.datatable();
                    $('.close').click();
                    $scope.IsDisabled = false;

                    flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                },
                function (error) {
                    $scope.IsDisabled = false;
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };

    $scope.delete = function (route, id) {
        var deletescope = $resource(Routing.generate(route, {id: id}));
        deletescope.delete(
                function (data) {

                    $scope.datatable();
                    flash($scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                },
                function (error) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };

    $scope.TotalAttente = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 1) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };
    $scope.TotalValide = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 2) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };
    $scope.TotalRefuse = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 3) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };
    $scope.TotalBrouillon = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 4) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };

    $scope.enableBtn = function (param1, param2) {
        if (param1 == param2) {
            return true;
        } else {
            return false;
        }
    };

    $scope.confirm = function (route, id, action) {
        $scope.confirmed = {};
        var linkconfirm = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        if (action == "confirmer") {
            $scope.confirmed.confirm = "2";
        } else if (action == "valider") {
            $scope.confirmed.confirm = "1";
        }
        linkconfirm.update(null, $scope.confirmed,
                function (data) {
                    flash($scope.flashTranslate('FLASH_VALID_VALID'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    $scope.datatable();
                },
                function (error) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };

//make status Refusé
    $scope.cancel = function (route, id) {
        $scope.canceled = {};
        var linkcancel = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        $scope.canceled.cancel = "3";
        linkcancel.update(null, $scope.canceled,
                function (data) {

                    flash($scope.flashTranslate('FLASH_VALID_REFUS'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    $scope.datatable();
                },
                function (error) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };

//make status Refusé
    $scope.Annuler = function (route, id) {
        $scope.canceled = {};
        var linkcancel = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        $scope.canceled.cancel = "4";
        linkcancel.update(null, $scope.canceled,
                function (data) {
                    flash($scope.flashTranslate('FLASH_VALID_CANCEL'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    $scope.datatable();
                },
                function (error) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };
// END RESPONSABLE Role second actions

    $scope.CollaborateurValidate = function (route, id) {
        $scope.confirmed = {};
        var linkconfirm = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        $scope.confirmed.confirm = "1";
        linkconfirm.update(null, $scope.confirmed,
                function (data) {
                    flash($scope.flashTranslate('FLASH_VALID_VALID'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    $scope.datatable();
                },
                function (error) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };
});
