app.controller('bonusCtrl', function ($scope, $resource, $filter, Ie11, Global) {

    $scope.RemoveAll = function () {
        Global.Remove($scope);
    };

    $scope.uploadNewAttachment = function (file) {
        $scope.new.attachments = {};
        $scope.new.attachments.path = file.url;
        $scope.new.attachments.mimetype = file.mimetype;
    };

    $scope.uploadEditAttachment = function (file) {
        $scope.edit.attachments = {};
        $scope.edit.attachments.path = file.url;
        $scope.edit.attachments.mimetype = file.mimetype;
    };

    $scope.flashText = {};

    $scope.flashTranslate = function (alias) {

        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };

    $scope.dateSearch = '';
    $scope.filterByDatePicker = function (type) {
        if (type === 'All') {
            $scope.dateSearch = '';
        }
    };

    $scope.getOpenMonths = function () {
        var date = new Date();
        var year = date.getFullYear();
        var month1 = 0;
        var month2 = 0;
        var ListOpenMonths = [];
        var i = 0;
        var long = 0
        var Open = $resource(Routing.generate('apiSettingsPayPeriod_get'));
        $scope.openmonths = Open.get(
            function (data) {
                $scope.values = data.payperiodlist;
                if (data.payperiodlist != null) {
                    for (i = 0; i < data.payperiodlist.length; i++) {
                        ListOpenMonths.push(($scope.values[i].start_date).split('-')[1]);
                    }
                    long = ListOpenMonths.length;
                    
                    $('.date-picker-expense').data("DateTimePicker").date(moment($filter('timezone')(data.payperiodlist[0].start_date)).format('MM/YYYY'));
                }
                if (long > 0) {
                    if (long == 1) {
                        month1 = ListOpenMonths.sort()[0] - 1;
                        month2 = ListOpenMonths.sort()[0] - 1;
                    } else {
                        month1 = ListOpenMonths.sort()[0] - 1;
                        month2 = ListOpenMonths.sort()[1] - 1;
                    }
                } else {
                    month1 = date.getMonth();
                    month2 = date.getMonth();
                }
            },
            function (error) {
            });
    };

    $scope.save = function (type) {
        console.log("$scope.save");
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate('bonus_add'));
        $scope.etat('#ValiderBonus', '#SauvegarderBonus', 'New');
        url.save($scope.new,
            function (successResult) {
                $scope.IsDisabled = false;
                $scope.datatable();
                setTimeout(function () {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                }, 0);
                setTimeout(function () {

                    if ($('#check' + 'Bonus').is(":checked")) {
                        $scope.nullable('Bonus');
                        $scope.new.attachments = null;
                        $scope.IsDisabled = false;
                        return;
                    } else {
                        $scope.nullable('Bonus');
                        $('.close').click();
                        $scope.IsDisabled = false;
                    }
                }, 0);
            },
            function (errorResult) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                $scope.IsDisabled = false;
            });
    };

    $scope.datatable = function () {
        $scope.loading = true;
        $scope.blockloader = true;
        $scope.getOpenMonths();
        var infos = $resource(Routing.generate('bonus_all'));
        infos.get(
            function (ResponseData) {
                var tasks = [];
                var username = [];
                var bonusNames = [];
                var bonusTypes = [];
                $scope.data = ResponseData.result;
                $scope.FiltersData = ResponseData.result;
                $scope.TotalAttente(ResponseData.result);
                $scope.TotalValide(ResponseData.result);
                $scope.TotalRefuse(ResponseData.result);
                $scope.TotalBrouillon(ResponseData.result);
                 angular.forEach(ResponseData.users, function (u, key) {
                    username.push({"username": u.user.username , "name":u.user.first_name+" "+u.user.last_name});
                });
                for (var j = 0; j < ResponseData.BonusTypes.length; j++) {
                    bonusTypes.push(ResponseData.BonusTypes[j].display_name);
                    // bonusTypes.push({'id':ResponseData.BonusTypes[j].id ,'display_name':ResponseData.BonusTypes[j].display_name});
                }

                $scope.BonusTypes = bonusTypes;
                $scope.CollabArrayValues = username;
                $scope.tasks = $scope.data;
                for (var k = 0; k < ResponseData.result.length; k++) {
                    ResponseData.result[k].settings_pay_period_month = moment($filter('timezone')(ResponseData.result[k].settings_pay_period.startDate)).format('MM/YYYY');
                    ResponseData.result[k].settings_pay_period_year = $filter('date')(ResponseData.result[k].settings_pay_period.startDate, 'yyyy');
                    ResponseData.result[k].date = $filter('date')(ResponseData.result[k].date, 'dd/MM/yyyy');
                    ResponseData.result[k].contributor.user.username_display = ResponseData.result[k].contributor.user.first_name+' '+ ResponseData.result[k].contributor.user.last_name
                    tasks.push(ResponseData.result[k]);
                }
                $scope.perPage = 10;
                $scope.maxSize = 5;
                $scope.setPage = function (pageNo) {
                    $scope.currentPage = pageNo;
                };
                $scope.$watch('searchText', function (term) {
                    var obj = term;
                    $scope.filterList = $filter('filter')(tasks, obj);
                    $scope.currentPage = 1;
                });
                $scope.loading = false;
                $scope.blockloader = false;
            },
            function (error) {
            });
    };

    $scope.TotalAttente = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 1) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };

    $scope.TotalValide = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 2) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };

    $scope.TotalRefuse = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 3) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };

    $scope.TotalBrouillon = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 4) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };

    $scope.etat = function (IDvalider, IDsauvegarde, action) {
        if (action == 'New') {
            if ($(IDvalider).is(":focus")) {
                $scope.new.status = "1";
            }
            if ($(IDsauvegarde).is(":focus")) {
                $scope.new.status = "4";
            }
        }
        if (action == 'Edit') {
            if ($(IDvalider).is(":focus")) {
                $scope.edit.status = "1";
            }
            if ($(IDsauvegarde).is(":focus")) {
                $scope.edit.status = "4";
            }
        }
    };

    $scope.SelectInfos = function (id, type, action) {
        Ie11.Params(action);
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.tasks.length; k++) {
            if (typeof $scope.tasks[k].id !== "undefined") {
                if ($scope.tasks[k].id == id) {
                    $scope.edit = angular.copy($scope.tasks[k]);
                    $scope.edit.attachments = $scope.tasks[k].attachments;
                    $scope.edit.payPeriod = $filter('date')($scope.tasks[k].payPeriod, "MM/yyyy");
                    $scope.edit.contributor.id = ($scope.tasks[k].contributor.id).toString();
                    $scope.edit.validator.id = ($scope.tasks[k].validator.id).toString();
                    $scope.edit.requester.id = ($scope.tasks[k].requester.id).toString();
                    $scope.edit.settings_bonus_type.id = ($scope.tasks[k].settings_bonus_type.id).toString();
                    $scope.changeBonusType(($scope.tasks[k].settings_bonus_type.id).toString());
                    $scope.edit.settings_bonus_name.id = ($scope.tasks[k].settings_bonus_name.id).toString();
                    $scope.edit.status = ($scope.tasks[k].settings_status.id).toString();
                    $scope.edit.date = $filter('date')($scope.tasks[k].date, 'dd/MM/yyyy');
                    $scope.edit.payPeriod = moment($filter('timezone')($scope.tasks[k].settings_pay_period.startDate)).format('MM/YYYY');


                }
            }
        }
        $scope.loading = false;
        $scope.blockloader = false;
    };

    $scope.update = function (route, id) {
        $scope.IsDisabled = true;
        var edit = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.etat('#EditValiderBonus', '#EditSauvegarderBonus', 'Edit');
        edit.update(null, $scope.edit,
            function (data) {
                $scope.IsDisabled = false;
                if (data.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                $scope.datatable();
                $('.close').click();
                $scope.IsDisabled = false;
                flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
            },
            function (error) {
                $scope.IsDisabled = false;
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    };

    $scope.delete = function (route, id, etat) {
        Ie11.Params(etat);
        var remove = $resource(Routing.generate(route, {id: id}));
        remove.delete(
            function (data) {
                if (etat != "all") {
                    $scope.datatable();
                    flash($scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                }
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    };

    $scope.confirm = function (route, id, action) {
        $scope.confirmed = {};
        var linkconfirm = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        if (action == "confirmer") {
            $scope.confirmed.confirm = "2";
        } else if (action == "valider") {
            $scope.confirmed.confirm = "1";
        }
        linkconfirm.update(null, $scope.confirmed,
            function (data) {
                if (data.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }

                flash($scope.flashTranslate('FLASH_VALID_VALID'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    };

    $scope.cancel = function (route, id) {
        $scope.canceled = {};
        var linkcancel = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.canceled.cancel = "3";
        linkcancel.update(null, $scope.canceled,
            function (data) {
                if (data.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                flash($scope.flashTranslate('FLASH_VALID_REFUS'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    }
// END RESPONSABLE Role second actions

    $scope.Annuler = function (route, id) {
        $scope.canceled = {};
        var linkcancel = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.canceled.cancel = "4";
        linkcancel.update(null, $scope.canceled,
            function (data) {
                flash($scope.flashTranslate('FLASH_VALID_CANCEL'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    };

    $scope.InitFields = function () {
        $scope.btn = {};
        $scope.new = {};
        $scope.edit = {};
        $scope.CurrentDate = $filter('date')(new Date(), "dd/MM/yyyy");
        $scope.IsDisabled = false;
    };

    $scope.nullable = function (type) {
        $scope.new.date = $scope.CurrentDate;
        $scope.new.amount = 0;
        $scope.new.status = null;
        $scope.new.settings_bonus_type = null;
        $scope.new.settings_bonus_name = null;
        $scope.new.settings_pay_period = null;
        $scope.new.attachments = null;
        $scope.new.comment = null;
    };

    $scope.pushToArray = function (array, value) {
        if (array.indexOf(value) === -1) {
            array.push(value);
        }
        return array;
    };

    $scope.bonusTypeSearch = '';
    $scope.filterByBonusType = function (bonusType) {
        if (bonusType === 'All') {
            $scope.bonusTypeSearch = '';
        } else {
            $scope.bonusTypeSearch = bonusType;
        }
    };

    $scope.initBonusNull = function () {
        for (var key in $scope.new) {
            $scope.new[key] = null;
        }
        for (var key in $scope.edit) {
            $scope.edit[key] = null;
        }
    };

    $scope.changeBonusType = function (scope) { 
        $scope.listBonusName = [];
        angular.forEach($scope.bonusTypes, function (u, key) {
            if (u.bonus_type.id == scope) {
                $scope.listBonusName.push(u);
            }
        });
    };

    $scope.initBonusType = function () {
        var infos = $resource(Routing.generate('apiSubPremiumType_list'));
        infos.get(
            function (data) {
                $scope.bonusTypes = data.subpremiumtypes;
            });
    };
});
