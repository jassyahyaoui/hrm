app.controller('benefitCtrl', function ($scope, $resource, $filter, Ie11, Global) {

    $scope.RemoveAll = function () {
        // Global.Remove($scope);
        // scope.IsDisabled = true;
        var ActionTab = [];
        var IdTab = [];
        var c = 0;
        $("input:checkbox:checked").each(function () {
            if ($(this).attr('data-status') !== 'Valide') {
                if ($(this).attr('data-user') === 'true') {
                    if ($(this).attr('data-action') == 'Véhicule') {
                        var Route = 'remove_hrm_benefits_car';
                    } else {
                        var Route = 'remove_hrm_benefits';
                    }
                    ActionTab.push(Route);
                    IdTab.push($(this).attr('data-id'));
                }
            }
        });

        if (ActionTab.length > 0) {
            for (var i = 0; i < ActionTab.length; i++) {
                c++;
                if (c == ActionTab.length) {
                    var remove = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}));
                    remove.delete(
                        function (data) {
                            $('.close').click();
                            $scope.data = data;
                            $scope.datatable();
                            flash($scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');

                            $scope.IsDisabled = false;
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');

                            // scope.IsDisabled = false;
                        });
                } else {
                    var remove = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}));
                    remove.delete(
                        function (data) {
                            $scope.IsDisabled = false;
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                            $scope.IsDisabled = false;
                        });
                }
            }
        } else {
            $('.close').click();
        }
    };
//
    $scope.uploadNewAttachment = function (file) {
        $scope.new.attachments = {};
        $scope.new.attachments.path = file.url;
        $scope.new.attachments.mimetype = file.mimetype;
    };

    $scope.uploadEditAttachment = function (file) {
        $scope.edit.attachments = {};
        $scope.edit.attachments.path = file.url;
        $scope.edit.attachments.mimetype = file.mimetype;
    };

    $scope.flashText = {};

    $scope.flashTranslate = function (alias) {

        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };
//
//     $scope.dateSearch = '';
//     $scope.filterByDatePicker = function (type) {
//         if (type === 'All') {
//             $scope.dateSearch = '';
//         }
//     };
//
    $scope.save = function (type) {

        $scope.IsDisabled = true;
        if ($scope.new.type == 'Véhicule') {
            var url = $resource(Routing.generate('hrm_benefit_car_add'));
        } else {
            var url = $resource(Routing.generate('hrm_benefit_add'));
        }
        $scope.etat('#ValiderBenefit', '#SauvegarderBenefit', 'New');
        $scope.etat('#EditValiderBenefit', '#EditSauvegarderBenefit', 'New');
        url.save($scope.new,
            function (successResult) {
                $scope.IsDisabled = false;
                $scope.datatable();
                setTimeout(function () {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                }, 0);
                setTimeout(function () {

                    if ($('#check' + 'Benefit').is(":checked")) {
                        $scope.nullable($scope.new.type);
                        $scope.IsDisabled = false;
                        return;
                    } else {
                        $('.close').click();
                        $scope.nullable($scope.new.type);
                        $scope.IsDisabled = false;
                    }
                }, 0);
            },
            function (errorResult) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                $scope.IsDisabled = false;
            });
    };

    $scope.datatable = function () {
        $scope.loading = true;
        $scope.blockloader = true;
        var infos = $resource(Routing.generate('hrm_benefits_all'));
        infos.get(
            function (ResponseData) {
                var tasks = [];
                var username = [];
                // var bonusNames = [];
                // var bonusTypes = [];
                $scope.data = ResponseData.result;
                $scope.FiltersData = ResponseData.result;
                $scope.TotalAttente(ResponseData.result);
                $scope.TotalValide(ResponseData.result);
                $scope.TotalRefuse(ResponseData.result);
                $scope.TotalBrouillon(ResponseData.result);
                angular.forEach(ResponseData.users, function (u, key) {
                    username.push({"username": u.user.username, "name": u.user.first_name + " " + u.user.last_name});
                });
                $scope.CollabArrayValues = username;
                $scope.tasks = $scope.data;
                for (var k = 0; k < ResponseData.result.length; k++) {
                    ResponseData.result[k].date_month = moment($filter('timezone')(ResponseData.result[k].date)).format('MM/YYYY');
                    ResponseData.result[k].date_year = moment($filter('timezone')(ResponseData.result[k].date)).format('YYYY');
                    ResponseData.result[k].FLname = ResponseData.result[k].contributor.user.first_name + " " + ResponseData.result[k].contributor.user.last_name;

                    tasks.push(ResponseData.result[k]);
                }
                $scope.perPage = 10;
                $scope.maxSize = 5;
                $scope.setPage = function (pageNo) {
                    $scope.currentPage = pageNo;
                };
                $scope.$watch('searchText', function (term) {
                    var obj = term;
                    $scope.filterList = $filter('filter')(tasks, obj);
                    $scope.currentPage = 1;
                });
                $scope.loading = false;
                $scope.blockloader = false;
            },
            function (error) {
            });
    };
//
    $scope.TotalAttente = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 1) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };

    $scope.TotalValide = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 2) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };

    $scope.TotalRefuse = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 3) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };

    $scope.TotalBrouillon = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 4) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };
//
    $scope.etat = function (IDvalider, IDsauvegarde, action) {
        if (action == 'New') {
            if ($(IDvalider).is(":focus")) {
                $scope.new.status = "1";
            }
            if ($(IDsauvegarde).is(":focus")) {
                $scope.new.status = "4";
            }
        }
        if (action == 'Edit') {
            if ($(IDvalider).is(":focus")) {
                $scope.edit.status = "1";
            }
            if ($(IDsauvegarde).is(":focus")) {
                $scope.edit.status = "4";
            }
        }
    };

    $scope.SelectInfos = function (id, type, action) {
        Ie11.Params(action);
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.tasks.length; k++) {
            if (typeof $scope.tasks[k].id !== "undefined") {

                if ($scope.tasks[k].id == id && $scope.tasks[k].type == type) {

                    if ($scope.tasks[k].type == 'Véhicule') {

                        $scope.edit = angular.copy($scope.tasks[k]);
                        $scope.lastType = {};
                        $scope.lastType = $scope.edit.type;
                        $scope.edit.contributor.id = ($scope.tasks[k].contributor.id).toString();
                        $scope.edit.validator.id = ($scope.tasks[k].validator.id).toString();
                        $scope.edit.requester.id = ($scope.tasks[k].requester.id).toString();
                        $scope.edit.status = ($scope.tasks[k].settings_status.id).toString();
                        $scope.edit.date = $filter('date')($scope.tasks[k].date, 'dd/MM/yyyy');
                        $scope.edit.circulationDate = $filter('date')($scope.tasks[k].circulationDate, 'dd/MM/yyyy');

                    } else {
                        $scope.edit = angular.copy($scope.tasks[k]);
                        $scope.lastType = {};
                        $scope.lastType = $scope.edit.type;
                        $scope.edit.periodicity = {};
                        $scope.edit.contributor.id = ($scope.tasks[k].contributor.id).toString();
                        $scope.edit.validator.id = ($scope.tasks[k].validator.id).toString();
                        $scope.edit.requester.id = ($scope.tasks[k].requester.id).toString();
                        $scope.edit.status = ($scope.tasks[k].settings_status.id).toString();
                        $scope.edit.date = $filter('date')($scope.tasks[k].date, 'dd/MM/yyyy');
                        $scope.edit.periodicity.id = ($scope.tasks[k].hrm_settings_benefit_periodicity.id).toString();
                    }

                }
            }
        }
        $scope.loading = false;
        $scope.blockloader = false;
    };

    $scope.update = function (tag, id) {
        if ($scope.lastType == 'Véhicule') {
            if ($scope.edit.type != $scope.lastType) {
                $scope.delete('remove_hrm_benefits_car', $scope.edit.id, 'all');
                $scope.new = $scope.edit;
                $scope.save('type');
                return;
            } else {
                var route = 'hrm_benefits_update_car';
            }
        } else {
            if ($scope.edit.type == 'Véhicule') {
                $scope.delete('remove_hrm_benefits', $scope.edit.id, 'all');
                $scope.new = $scope.edit;
                $scope.save('type');
                return;
            } else {
                var route = 'hrm_benefits_update';
            }
        }
        $scope.IsDisabled = true;
        var edit = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.etat('#EditValiderBenefit', '#EditSauvegarderBenefit', 'Edit');
        edit.update(null, $scope.edit,
            function (data) {
                $scope.IsDisabled = false;

                $scope.datatable();
                $('.close').click();
                $scope.IsDisabled = false;
                flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
            },
            function (error) {
                $scope.IsDisabled = false;
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    };

    $scope.delete = function (route, id, etat) {
        Ie11.Params(etat);
        var remove = $resource(Routing.generate(route, {id: id}));
        remove.delete(
            function (data) {
                if (etat != "all") {
                    $scope.datatable();
                    flash($scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                }
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    };

    $scope.remove = function (tag, id, type, etat) {
        Ie11.Params(etat);
        if (type == 'Véhicule') {
            var route = 'remove_hrm_benefits_car';
        } else {
            var route = 'remove_hrm_benefits';
        }
        var remove = $resource(Routing.generate(route, {id: id}));
        remove.delete(
            function (data) {
                if (etat != "all") {
                    $scope.datatable();
                    flash($scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                }
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    };

    $scope.confirm = function (tag, id, type, action) {
        if (type == 'Véhicule') {
            var route = 'confirm_hrm_benefits_car';
        } else {
            var route = 'confirm_hrm_benefits';
        }
        $scope.confirmed = {};
        var linkconfirm = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        if (action == "confirmer") {
            $scope.confirmed.confirm = "2";
        } else if (action == "valider") {
            $scope.confirmed.confirm = "1";
        }
        linkconfirm.update(null, $scope.confirmed,
            function (data) {
                flash($scope.flashTranslate('FLASH_VALID_VALID'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    };


    $scope.cancel = function (tag, id, type) {
        if (type == 'Véhicule') {
            var route = 'cancel_hrm_benefits_car';
        } else {
            var route = 'cancel_hrm_benefits';
        }
        $scope.canceled = {};
        var linkcancel = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.canceled.cancel = "3";
        linkcancel.update(null, $scope.canceled,
            function (data) {
                flash($scope.flashTranslate('FLASH_VALID_REFUS'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    }
// // // END RESPONSABLE Role second actions
//
    $scope.Annuler = function (tag, id, type) {
        if (type == 'Véhicule') {
            var route = 'cancel_hrm_benefits_car';
        } else {
            var route = 'cancel_hrm_benefits';
        }
        $scope.canceled = {};
        var linkcancel = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.canceled.cancel = "4";
        linkcancel.update(null, $scope.canceled,
            function (data) {
                flash($scope.flashTranslate('FLASH_VALID_CANCEL'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    };

    $scope.InitFields = function () {
//         $scope.btn = {};
//         $scope.new = {};
//         $scope.edit = {};
        $scope.CurrentDate = $filter('date')(new Date(), "dd/MM/yyyy ");
//        $("#benefitbundle_hrm_benefit_validator").val($("#benefitbundle_hrm_benefit_validator option:eq(1)").val());
        if (typeof $scope.new !== "undefined")
            if (typeof $scope.new.validator !== "undefined")
                $scope.new.validator.id = $("#benefitbundle_hrm_benefit_validator option:eq(1)").val();
        $scope.IsDisabled = false;
    };

    $scope.nullable = function (type) {
        if (type == 'Véhicule') {
            $scope.new.car = null;
            $scope.new.calculationMethod = null;
            $scope.new.circulationDate = null;
            $scope.new.date = null;
            $scope.new.buyingPrice = 0;
            $scope.new.fuel = null;
            $scope.new.participation = 0;
            $scope.new.cost = 0;
            $scope.new.totalDistance = 0;
            $scope.new.privateDistance = 0;
        } else {
            $scope.new.date = null;
            $scope.new.realAmount = 0;
            $scope.new.hrm_settings_benefit_periodicity = null;
        }
    };
//
//     $scope.pushToArray = function (array, value) {
//         if (array.indexOf(value) === -1) {
//             array.push(value);
//         }
//         return array;
//     };

    $scope.typeSearch = '';
    $scope.filterByType = function (type) {
        if (type === 'All') {
            $scope.typeSearch = '';
        } else {
            $scope.typeSearch = type;
        }
    }
    $scope.dateSearch = '';
    $scope.filterByDatePicker = function (type) {
        if (type === 'All') {
            $scope.dateSearch = '';
        }
    }

//
//     $scope.initBonusNull = function () {
//         for (var key in $scope.new) {
//             $scope.new[key] = null;
//         }
//         for (var key in $scope.edit) {
//             $scope.edit[key] = null;
//         }
//     };
//
//     $scope.changeBonusType = function (scope) {
//         $scope.listBonusName = [];
//         angular.forEach($scope.bonusTypes, function (u, key) {
//             if (u.bonus_type.id == scope) {
//                 $scope.listBonusName.push(u);
//             }
//         });
//     };
//
//     $scope.initBonusType = function () {
//         var infos = $resource(Routing.generate('apiSubPremiumType_list'));
//         infos.get(
//             function (data) {
//                 $scope.bonusTypes = data.subpremiumtypes;
//             });
//     };
});
