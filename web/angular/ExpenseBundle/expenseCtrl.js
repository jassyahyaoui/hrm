'use strict';
app.controller('expenseCtrl', function ($scope, $resource, $filter, $compile, Ie11, Global, $timeout, $rootScope) {

    $scope.RemoveAll = function () {
        Global.Remove($scope);
    };


    $scope.uploadNewIkAttachment = function (file) {
        $scope.new.attachments = {};
        $scope.new.attachments.path = file.url;
        $scope.new.attachments.mimetype = file.mimetype;
    };
    $scope.uploadEditIkAttachment = function (file) {
        $scope.edit.attachments = {};
        $scope.edit.attachments.path = file.url;
        $scope.edit.attachments.mimetype = file.mimetype;
    };
    $scope.uploadNewTrAttachment = function (file) {
        $scope.new.attachments = {};
        $scope.new.attachments.path = file.url;
        $scope.new.attachments.mimetype = file.mimetype;
    };
    $scope.uploadEditTrAttachment = function (file) {
        $scope.edit.attachments = {};
        $scope.edit.attachments.path = file.url;
        $scope.edit.attachments.mimetype = file.mimetype;
    };
    $scope.uploadNewAcAttachment = function (file) {
        $scope.new.attachments = {};
        $scope.new.attachments.path = file.url;
        $scope.new.attachments.mimetype = file.mimetype;
    };
    $scope.uploadEditAcAttachment = function (file) {

        $scope.edit.attachments = {};
        $scope.edit.attachments.path = file.url;
        $scope.edit.attachments.mimetype = file.mimetype;
    };
    $scope.uploadNewDiAttachment = function (file) {
        $scope.new.attachments = {};
        $scope.new.attachments.path = file.url;
        $scope.new.attachments.mimetype = file.mimetype;
    };
    $scope.uploadEditDiAttachment = function (file) {
        $scope.edit.attachments = {};
        $scope.edit.attachments.path = file.url;
        $scope.edit.attachments.mimetype = file.mimetype;
    };
    $scope.uploadNewOtAttachment = function (file) {
        $scope.new.attachments = {};
        $scope.new.attachments.path = file.url;
        $scope.new.attachments.mimetype = file.mimetype;
    };
    $scope.uploadEditOtAttachment = function (file) {
        $scope.edit.attachments = {};
        $scope.edit.attachments.path = file.url;
        $scope.edit.attachments.mimetype = file.mimetype;
    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {

        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    }

    $scope.typeSearch = '';
    $scope.filterByType = function (type) {
        if (type === 'All') {
            $scope.typeSearch = '';
        } else {
            $scope.typeSearch = type;
        }
    }
    $scope.dateSearch = '';
    $scope.filterByDatePicker = function (type) {
        if (type === 'All') {
            $scope.dateSearch = '';
        }
    }

    $scope.userData = {}


    //Open months
    $scope.getOpenMonths = function () {
        var date = new Date();
        var year = date.getFullYear();
        var month1 = 0;
        var month2 = 0;
        var ListOpenMonths = [];
        var i = 0;
        var long = 0
        var Open = $resource(Routing.generate('apiSettingsPayPeriod_get'));
        $scope.openmonths = Open.get(
            function (data) {
                $scope.values = data.payperiodlist;
                if (data.payperiodlist != null) {
                    for (i = 0; i < data.payperiodlist.length; i++) {
                        ListOpenMonths.push(($scope.values[i].start_date).split('-')[1]);
                    }
                    long = ListOpenMonths.length;
                    // console.log(ListOpenMonths.sort()[0]);
                    $('.date-picker-expense').each(function () {
                        $(this).data("DateTimePicker").date(moment($filter('timezone')(data.payperiodlist[0].start_date)).format('MM/YYYY'));
                    });
                }
                if (long > 0) {
                    if (long == 1) {
                        month1 = ListOpenMonths.sort()[0] - 1;
                        month2 = ListOpenMonths.sort()[0] - 1;
                    } else {
                        month1 = ListOpenMonths.sort()[0] - 1;
                        month2 = ListOpenMonths.sort()[1] - 1;
                    }
                } else {
                    month1 = date.getMonth();
                    month2 = date.getMonth();
                }
            },
            function (error) {
            });
    };


    $scope.InitFields = function (id) {
        $scope.btn = {};
        $scope.new = {};
        $scope.coefficient = {};
        $scope.CurrentDate = $filter('date')(new Date(), "dd/MM/yyyy ");
        $scope.IsDisabled = false;
        $("#expensebundle_ik_validator").val($("#expensebundle_ik_validator option:eq(1)").val());
        if (typeof angular.element($('#New')).scope().new !== "undefined")
            $scope.new.validator = $("#expensebundle_ik_validator option:eq(1)").val();
    }

    $scope.saveDining = function () {
        $scope.IsDisabled = true;
        var Dining = $resource(Routing.generate('dining_add'));
        $scope.etat('#ValiderDINING', '#SauvegarderDINING', 'New');
        $scope.new.ArrayfirstName = [];
        for (var keyFirst in $scope.new.firstName) {
            var value = $scope.new.firstName[keyFirst];
            $scope.new.ArrayfirstName.push(value);
        }
        $scope.new.ArraylastName = [];
        for (var keyLast in $scope.new.lastName) {
            var value = $scope.new.lastName[keyLast];
            $scope.new.ArraylastName.push(value);
        }

        Dining.save($scope.new,
            function (successResult) {
                $scope.datatable();
                if (successResult.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (successResult.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (successResult.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                setTimeout(function () {
                    flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');

                }, 0);
                setTimeout(function () {
                    if ($('#checkDining').is(":checked")) {
                        $scope.new.date = $scope.CurrentDate;
                        $scope.new.displayName = null;
//                        $scope.new.project = null;
                        $scope.new.status = null;
                        $scope.new.comment = null;
                        $scope.new.payPeriod = null;
                        $scope.new.amountDin = 0;
                        $scope.new.taxe20 = 0;
                        $scope.new.taxe10 = 0;
                        $scope.new.taxe5 = 0;
                        $scope.new.untaxedAmountDin = 0;
                        $scope.new.checkDining = null;
                        $scope.new.firstName = null;
                        $scope.new.lastName = null;
                        $scope.new.ArrayfirstName = [];
                        $scope.new.ArraylastName = [];
                        $scope.IsDisabled = false;
                        return;
                    } else {
                        $scope.nullable('Restauration');
                        $('.close').click();
                        $scope.IsDisabled = false;
                    }
                }, 0);

                $scope.datatable();

            },
            function (errorResult) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    }

//update Dining method
    $scope.updateDining = function (route, id) {

        $scope.IsDisabled = true;
        var edit = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.etat('#EditValiderDINING', '#EditSauvegarderDINING', 'Edit');
        $scope.edit.ArrayId = [];
        for (var keyUid in $scope.edit.uid) {
            var Uid = $scope.edit.uid[keyUid];
            $scope.edit.ArrayId.push(Uid);
        }
        $scope.edit.ArrayfirstName = [];
        for (var keyFirst in $scope.edit.firstName) {
            var value = $scope.edit.firstName[keyFirst];
            $scope.edit.ArrayfirstName.push(value);
        }
        $scope.edit.ArraylastName = [];
        for (var keyLast in $scope.edit.lastName) {
            var value = $scope.edit.lastName[keyLast];
            $scope.edit.ArraylastName.push(value);
        }
        edit.update(null, $scope.edit,
            function (data) {
                $scope.IsDisabled = false;
                if (date.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }

                $scope.datatable();
                $('.close').click();
                flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.IsDisabled = false;
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                $scope.IsDisabled = false;
            });
    };
//End Dining
// $scope.new.disabled = false;

    $scope.save = function (type) {
        $scope.IsDisabled = true;
        var url = $scope.getUrlByType(type);
        $scope.etat('#ValiderIK', '#SauvegarderIK', 'New');
        $scope.etat('#ValiderTRANSPORT', '#SauvegarderTRANSPORT', 'New');
        $scope.etat('#ValiderACCOMODATION', '#SauvegarderACCOMODATION', 'New');
        $scope.etat('#ValiderOTHER', '#SauvegarderOTHER', 'New');
        url.save($scope.new,
            function (successResult) {
                $scope.IsDisabled = false;
                if (successResult.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (successResult.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (successResult.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }

                $scope.datatable();

                setTimeout(function () {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                }, 0);

                setTimeout(function () {

                    if ($('#check' + type).is(":checked")) {
                        $scope.nullable(type);
                        $scope.new.attachments = null;
                        $scope.IsDisabled = false;
                        return;
                    } else {
                        $scope.nullable(type);
                        $('.close').click();
                        $scope.IsDisabled = false;
                    }
                }, 0);
            },
            function (errorResult) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                $scope.IsDisabled = false;
            });
    }

//update general method
    $scope.update = function (route, id) {
        $scope.datatable();
        $scope.IsDisabled = true;
        var edit = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.etat('#EditValiderIK', '#EditSauvegarderIK', 'Edit');
        $scope.etat('#EditValiderTRANSPORT', '#EditSauvegarderTRANSPORT', 'Edit');
        $scope.etat('#EditValiderACCOMODATION', '#EditSauvegarderACCOMODATION', 'Edit');
        $scope.etat('#EditValiderOTHER', '#EditSauvegarderOTHER', 'Edit');
        edit.update(null, $scope.edit,
            function (data) {
                $scope.IsDisabled = false;
                if (data.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }

                $scope.datatable();
                $('.close').click();

                $scope.IsDisabled = false;
                flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
            },
            function (error) {
                $scope.IsDisabled = false;
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    };
    //delete general method
    $scope.delete = function (route, id, etat) {
        Ie11.Params(etat);
        var remove = $resource(Routing.generate(route, {id: id}));
        remove.delete(
            function (data) {
                if (etat != "all") {
                    $scope.datatable();
                    flash($scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                }
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    };
//
// // RESPONSABLE Role Global actions
//     $scope.RemoveAll = function () {
//         var ActionTab = [];
//         var IdTab = [];
//         var c = 0;
//         $("input:checkbox:checked").each(function () {
//             if (($(this).attr('data-statut') == '4')) {
//                 ActionTab.push("remove_" + $(this).attr('id'));
//                 IdTab.push($(this).attr('name'));
//             }
//         });
//         if (ActionTab.length > 0) {
//             for (var i = 0; i < ActionTab.length; i++) {
//                 c++;
//                 if (c == ActionTab.length) {
//                     var remove = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}));
//                     remove.delete(
//                         function (data) {
//                             $('.close').click();
//                             $scope.data = data;
//                             $scope.datatable();
//                             flash($scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');
//                         },
//                         function (error) {
//                             flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
//                         });
//                 } else {
//                     var remove = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}));
//                     remove.delete(
//                         function (data) {
//                         },
//                         function (error) {
//                             flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
//                         });
//                 }
//             }
//         } else {
//             $('.close').click();
//         }
//     };

    $scope.ValidateAll = function () {
        var ActionTab = [];
        var IdTab = [];
        var ScopeTab = [];
        var c = 0;
        var ScopeValue;
        $("input:checkbox:checked").each(function () {
            if ($(this).attr('data-statut') != '2') {
                ActionTab.push("confirm_" + $(this).attr('id'));
                IdTab.push($(this).attr('name'));
                if ($(this).attr('data-statut') == '4') {
                    ScopeValue = 1;
                } else {
                    ScopeValue = 2;
                }
                ScopeTab.push(ScopeValue);
            }
        });
        if (ActionTab.length > 0) {
            for (var i = 0; i < ActionTab.length; i++) {
                c++;
                $scope.confirmed = {};
                if (c == ActionTab.length) {
                    var linkconfirm = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.confirmed.confirm = ScopeTab[i];
                    linkconfirm.update(null, $scope.confirmed,
                        function (data) {
                            $('.close').click();
                            $scope.data = data;
                            $scope.datatable();
                            flash($scope.flashTranslate('FLASH_VALID_CONFIRM'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                } else {
                    var linkconfirm = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.confirmed.confirm = ScopeTab[i];
                    linkconfirm.update(null, $scope.confirmed,
                        function (data) {
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                }
            }
        } else {
            $('.close').click();
        }
    };


    $scope.CancelAll = function () {
        var ActionTab = [];
        var IdTab = [];
        var c = 0;
        $("input:checkbox:checked").each(function () {
            if (($(this).attr('data-statut') != '2') && ($(this).attr('data-statut') != '3')) {
                ActionTab.push("cancel_" + $(this).attr('id'));
                IdTab.push($(this).attr('name'));
            }
        });
        if (ActionTab.length > 0) {
            for (var i = 0; i < ActionTab.length; i++) {
                c++;
                $scope.canceled = {};

                if (c == ActionTab.length) {
                    var linkcancel = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.canceled.cancel = "3";
                    linkcancel.update(null, $scope.canceled,
                        function (data) {
                            $('.close').click();
                            $scope.data = data;
                            $scope.datatable();
                            flash($scope.flashTranslate('FLASH_VALID_REFUS'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                } else {
                    var linkcancel = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.canceled.cancel = "3";
                    linkcancel.update(null, $scope.canceled,
                        function (data) {
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                }
            }
        } else {
            $('.close').click();
        }
    };

    //END RESPONSABLE Role Global Actions


    //Collaborateur Role Global Actions
    $scope.CollaborateurRemoveAll = function () {
        var ActionTab = [];
        var IdTab = [];
        var c = 0;
        $("input:checkbox:checked").each(function () {
            if ($(this).attr('data-statut') == '4') {
                ActionTab.push("remove_" + $(this).attr('id'));
                IdTab.push($(this).attr('name'));
            }
        });
        if (ActionTab.length > 0) {
            for (var i = 0; i < ActionTab.length; i++) {
                c++;
                if (c == ActionTab.length) {
                    var remove = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}));
                    remove.delete(
                        function (data) {
                            $('.close').click();
                            $scope.data = data;
                            $scope.datatable();
                            flash($scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                } else {
                    var remove = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}));
                    remove.delete(
                        function (data) {
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                }
            }
        } else {
            $('.close').click();
        }
    };
    $scope.CollaborateurValidateAll = function () {
        var ActionTab = [];
        var IdTab = [];
        var c = 0;
        $("input:checkbox:checked").each(function () {
            if (($(this).attr('data-statut') == '4') && ($(this).attr('data-user') == 'true')) {
                ActionTab.push("confirm_" + $(this).attr('id'));
                IdTab.push($(this).attr('name'));
            }
        });
        if (ActionTab.length > 0) {
            for (var i = 0; i < ActionTab.length; i++) {
                c++;
                $scope.confirmed = {};
                if (c == ActionTab.length) {
                    var linkconfirm = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.confirmed.confirm = "1";
                    linkconfirm.update(null, $scope.confirmed,
                        function (data) {
                            $('.close').click();
                            $scope.data = data;
                            $scope.datatable();
                            flash($scope.flashTranslate('FLASH_VALID_CONFIRM'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                } else {
                    var linkconfirm = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.confirmed.confirm = "1";
                    linkconfirm.update(null, $scope.confirmed,
                        function (data) {
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                }
            }
        } else {
            $('.close').click();
        }
    };
    $scope.AnnulerAll = function () {
        var ActionTab = [];
        var IdTab = [];
        var c = 0;
        $("input:checkbox:checked").each(function () {
            if ((($(this).attr('data-statut') == '1') || ($(this).attr('data-statut') == '2') && ($(this).attr('data-period') == 'open')) && ($(this).attr('data-user') == 'true')) {
                ActionTab.push("cancel_" + $(this).attr('id'));
                IdTab.push($(this).attr('name'));
            }
        });
        if (ActionTab.length > 0) {
            for (var i = 0; i < ActionTab.length; i++) {
                c++;
                $scope.canceled = {};

                if (c == ActionTab.length) {
                    var linkcancel = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.canceled.cancel = "4";
                    linkcancel.update(null, $scope.canceled,
                        function (data) {
                            $('.close').click();
                            $scope.data = data;
                            $scope.datatable();
                            flash($scope.flashTranslate('FLASH_VALID_CANCEL'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                } else {
                    var linkcancel = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}), null,
                        {
                            'update': {method: 'PUT'}
                        });
                    $scope.canceled.cancel = "4";
                    linkcancel.update(null, $scope.canceled,
                        function (data) {
                        },
                        function (error) {
                            flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        });
                }
            }
        } else {
            $('.close').click();
        }
    };

    //END Collaborateur Role Global Actions

// RESPONSABLE Role second actions
//make status Validé
    $scope.confirm = function (route, id, action) {
        $scope.confirmed = {};
        var linkconfirm = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        if (action == "confirmer") {
            $scope.confirmed.confirm = "2";
        } else if (action == "valider") {
            $scope.confirmed.confirm = "1";
        }
        linkconfirm.update(null, $scope.confirmed,
            function (data) {
                if (data.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }

                flash($scope.flashTranslate('FLASH_VALID_VALID'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    }


//make status Refusé 
    $scope.cancel = function (route, id) {
        $scope.canceled = {};
        var linkcancel = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.canceled.cancel = "3";
        linkcancel.update(null, $scope.canceled,
            function (data) {
                if (data.status == 'ERR_CLOSED_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_EMPTY_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_CHOOSE_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                if (data.status == 'ERR_NOT_PAY_PERIOD') {
                    return flash($scope.flashTranslate('FLASH_OPEN_PAYPERIOD'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                }
                flash($scope.flashTranslate('FLASH_VALID_REFUS'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    }
// END RESPONSABLE Role second actions


// Collaborateur Role second actions
//make status Validé
    $scope.CollaborateurValidate = function (route, id) {
        $scope.confirmed = {};
        var linkconfirm = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.confirmed.confirm = "1";
        linkconfirm.update(null, $scope.confirmed,
            function (data) {
                flash($scope.flashTranslate('FLASH_VALID_VALID'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    }

//make status Refusé 
    $scope.Annuler = function (route, id) {
        $scope.canceled = {};
        var linkcancel = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        $scope.canceled.cancel = "4";
        linkcancel.update(null, $scope.canceled,
            function (data) {
                flash($scope.flashTranslate('FLASH_VALID_CANCEL'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.datatable();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    }

// END Collaborateur Role second actions

    $scope.datatable = function () {
        $scope.loading = true;
        $scope.blockloader = true;
        $scope.getOpenMonths();
        var infos = $resource(Routing.generate('ik_all'));
        infos.get(
            function (ResponseData) {

                var tasks = [];
                var username = [];
                $scope.data = ResponseData.result;
                $scope.FiltersData = ResponseData.result;
                $scope.TotalAttente(ResponseData.result);
                $scope.TotalValide(ResponseData.result);
                $scope.TotalRefuse(ResponseData.result);
                $scope.TotalBrouillon(ResponseData.result);
                angular.forEach(ResponseData.users, function (u, key) {
                    username.push({"username": u.user.username, "name": u.user.first_name + " " + u.user.last_name});
                });

                $scope.CollabArrayValues = username;

                $scope.tasks = $scope.data;
                for (var i = 0; i < ResponseData.result.length; i++) {

                    if (!jQuery.isEmptyObject(ResponseData.result[i].settings_pay_period)) {
                        ResponseData.result[i].settings_pay_period_month = moment($filter('timezone')(ResponseData.result[i].settings_pay_period.startDate)).format('MM/YYYY');
                        ResponseData.result[i].settings_pay_period_year = moment($filter('timezone')(ResponseData.result[i].settings_pay_period.startDate)).format('YYYY');
                    }
                    ResponseData.result[i].date = moment($filter('timezone')(ResponseData.result[i].date)).format('DD/MM/YYYY');
                    tasks.push(ResponseData.result[i]);
                }

                $scope.perPage = 10;
                $scope.maxSize = 5;
                $scope.setPage = function (pageNo) {
                    $scope.currentPage = pageNo;
                };
                $scope.$watch('searchText', function (term) {
                    var obj = term;
                    $scope.filterList = $filter('filter')(tasks, obj);
                    $scope.currentPage = 1;
                });

                $scope.loading = false;
                $scope.blockloader = false;
            },
            function (error) {
            });
    };
    $scope.pushToArray = function (array, value) {
        if (array.indexOf(value) === -1) {
            array.push(value);
        }
        return array;
    };
    // $scope.jasser = function (CurrencyName) {
    //     $scope.edit.currency = (CurrencyName).toString();
    //     // return $scope.edit.currency;
    // };
    $scope.SetCurrencyByName = function (ParamCurrencyIsoCode) {
        var IDCurrencyIsoCode;
        var infos = $resource(Routing.generate('currency_saved_currencies'));
        infos.get(
            function (data) {
                $scope.CurrenciesSavedData = data.result;
                console.log($scope.CurrenciesSavedData);
                for (var i = 0; i < $scope.CurrenciesSavedData.length; i++) {
                    if ($scope.CurrenciesSavedData[i].currencyIsoCode == ParamCurrencyIsoCode) {
                        IDCurrencyIsoCode = $scope.CurrenciesSavedData[i].id;
                        $scope.edit.currency = (IDCurrencyIsoCode).toString();
                    }
                }
            },
            function (error) {
            });
    };
    $scope.SelectInfos = function (id, type, action) {
        Ie11.Params(action);

        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.tasks.length; k++) {
            if (typeof $scope.tasks[k].uid !== "undefined") {
                if ($scope.tasks[k].uid == id && $scope.tasks[k].type == type) {

                    $scope.edit = angular.copy($scope.tasks[k]);
                    $scope.edit.payPeriod = $filter('date')($scope.tasks[k].payPeriod, "MM/yyyy");
                    $scope.edit.settings_pay_period = moment($filter('timezone')($scope.tasks[k].settings_pay_period.startDate)).format('MM/YYYY');

                    if ($scope.tasks[k].type == 'Ik') {

                        $scope.edit.contributor.id = ($scope.tasks[k].contributor.id).toString();
                        $scope.edit.validator.id = ($scope.tasks[k].validator.id).toString();
//                        $scope.edit.project = ($scope.tasks[k].settings_projects.id).toString();
                        $scope.edit.requester.id = ($scope.tasks[k].requester.id).toString();
                        $scope.edit.status = ($scope.tasks[k].settings_status.id).toString();
                        $scope.edit.date = $filter('date')($scope.tasks[k].date, "dd/MM/yyyy"); //moment($filter('timezone')($scope.tasks[k].date)).format('DD/MM/YYYY');
                        $scope.edit.coefficient = angular.copy(($scope.tasks[k].settings_exp_ik.id).toString());

                        $scope.edit.settings_pay_period = moment($filter('timezone')($scope.tasks[k].settings_pay_period.startDate)).format('MM/YYYY');

                    }
                    if ($scope.tasks[k].type == 'Transport') {
                        // load tax input
                        $scope.getTaxModelInpout("transport_tax_model", "edit", $scope.tasks[k].type, ($scope.tasks[k].settings_exp_transport.id).toString(), $scope.tasks[k].uid, action);
                        $scope.edit = $scope.tasks[k];
                        $scope.edit.contributor.id = ($scope.tasks[k].contributor.id).toString();
                        $scope.edit.requester.id = ($scope.tasks[k].requester.id).toString();
                        $scope.edit.validator.id = ($scope.tasks[k].validator.id).toString();
//                        $scope.edit.requester = ($scope.tasks[k].requester.id).toString();
                        $scope.edit.nature = ($scope.tasks[k].settings_exp_transport.id).toString();
//                        $scope.edit.project = ($scope.tasks[k].settings_projects.id).toString();
                        $scope.edit.status = ($scope.tasks[k].settings_status.id).toString();
                        $scope.edit.date = $filter('date')($scope.tasks[k].date, "dd/MM/yyyy");
                        ;
                        $scope.edit.settings_pay_period = moment($filter('timezone')($scope.tasks[k].settings_pay_period.startDate)).format('MM/YYYY');
                    }

                    if ($scope.tasks[k].type == 'Hebergement') {
                        // load tax input
                        $scope.getTaxModelInpout("accomodation_tax_model", "edit", $scope.tasks[k].type, ($scope.tasks[k].settings_exp_accomodation.id).toString(), $scope.tasks[k].uid, action);

                        $scope.edit = $scope.tasks[k];
                        $scope.edit.contributor.id = ($scope.tasks[k].contributor.id).toString();
                        $scope.edit.requester.id = ($scope.tasks[k].requester.id).toString();
                        $scope.edit.validator.id = ($scope.tasks[k].validator.id).toString();
                        $scope.edit.natureAcc = ($scope.tasks[k].settings_exp_accomodation.id).toString();
//                        $scope.edit.project = ($scope.tasks[k].settings_projects.id).toString();
                        $scope.edit.status = ($scope.tasks[k].settings_status.id).toString();
                        $scope.edit.date = $filter('date')($scope.tasks[k].date, "dd/MM/yyyy");
                        ;
                        $scope.edit.settings_pay_period = moment($filter('timezone')($scope.tasks[k].settings_pay_period.startDate)).format('MM/YYYY');
                    }

                    if ($scope.tasks[k].type == 'Restauration') {
                        // load tax input
                        $scope.getTaxModelInpout("dining_tax_model", "edit", $scope.tasks[k].type, ($scope.tasks[k].settings_exp_dining.id).toString(), $scope.tasks[k].uid, action);

                        $scope.edit = $scope.tasks[k];
                        $scope.edit.contributor.id = ($scope.tasks[k].contributor.id).toString();
                        $scope.edit.requester.id = ($scope.tasks[k].requester.id).toString();
                        $scope.edit.validator.id = ($scope.tasks[k].validator.id).toString();
                        $scope.edit.settingsExpDining = ($scope.tasks[k].settings_exp_dining.id).toString();
//                        $scope.edit.project = ($scope.tasks[k].settings_projects.id).toString();
                        $scope.edit.status = ($scope.tasks[k].settings_status.id).toString();
                        $scope.edit.date = $filter('date')($scope.tasks[k].date, "dd/MM/yyyy");
                        ;
                        $scope.edit.settings_pay_period = moment($filter('timezone')($scope.tasks[k].settings_pay_period.startDate)).format('MM/YYYY');
                    }

                    if ($scope.tasks[k].type == 'Autres') {
                        // console.log($scope.edit.currencyIsoCode);
                        // $scope.jasser($scope.edit.currencyIsoCode,);
                        // load tax input
                        $scope.getTaxModelInpout("other_tax_model", "edit", $scope.tasks[k].type, ($scope.tasks[k].settings_expenses.id).toString(), $scope.tasks[k].uid, action);

                        $scope.edit = $scope.tasks[k];
                        $scope.SetCurrencyByName($scope.edit.currencyIsoCode);
                        // $scope.jasser(edit.currencyIsoCode);
                        $scope.edit.contributor.id = ($scope.tasks[k].contributor.id).toString();
                        $scope.edit.requester.id = ($scope.tasks[k].requester.id).toString();
                        $scope.edit.validator.id = ($scope.tasks[k].validator.id).toString();
                        $scope.edit.natureOth = ($scope.tasks[k].settings_expenses.id).toString();
//                        $scope.edit.project = ($scope.tasks[k].settings_projects.id).toString();
                        $scope.edit.status = ($scope.tasks[k].settings_status.id).toString();
                        $scope.edit.date = $filter('date')($scope.tasks[k].date, "dd/MM/yyyy");
                        ;
                        $scope.edit.settings_pay_period = moment($filter('timezone')($scope.tasks[k].settings_pay_period.startDate)).format('MM/YYYY');
                    }
                }
            }
        }

        $scope.loading = false;
        $scope.blockloader = false;
    }

    $scope.TotalAttente = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 1) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };
    $scope.TotalValide = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 2) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };
    $scope.TotalRefuse = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 3) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };
    $scope.TotalBrouillon = function (param) {
        if (typeof param !== "undefined") {
            var sum = 0;
            for (var i = 0; i < param.length; i++) {
                if (param[i].settings_status.id == 4) {
                    sum = sum + 1;
                }
            }
            return sum;
        }
    };
    // check the Buttons ID who was submitted
    $scope.etat = function (IDvalider, IDsauvegarde, action) {
        if (action == 'New') {
            if ($(IDvalider).is(":focus")) {
                $scope.new.status = "1";
            }
            if ($(IDsauvegarde).is(":focus")) {
                $scope.new.status = "4";
            }
        }
        if (action == 'Edit') {
            if ($(IDvalider).is(":focus")) {
                $scope.edit.status = "1";
            }
            if ($(IDsauvegarde).is(":focus")) {
                $scope.edit.status = "4";
            }
        }

    }

//Add Dynamic input on Add form
    $scope.addInput = function (newForm0) {
        var newForm = $('#guests').append(newForm0);
        $compile(newForm)($scope);
        $scope.$apply();
    };
    //Remove Dynamic input on Add form
    $scope.removeInput = function (x, index) {
        eval("delete $scope.new.lastName.lastName" + index);
        eval("delete $scope.new.firstName.firstName" + index);
        $compile(x)($scope);
    };
    //Remove Dynamic input on Add form
    $scope.removeInputEdit = function (item) {
        var index = $scope.edit.hrm_expenses_guests.indexOf(item);
        $scope.edit.hrm_expenses_guests.splice(index, 1);
    };
    $scope.getUrlByType = function (type) {
        var url;
        if (type == 'Ik') {
            url = $resource(Routing.generate('ik_add'));
        }
        if (type == 'Transport') {
            url = $resource(Routing.generate('transport_add'));
        }
        if (type == 'Accomodation') {
            url = $resource(Routing.generate('accomodation_add'));
        }
        if (type == 'Other') {
            url = $resource(Routing.generate('other_add'));
        }
        return url;
    }


    $scope.nullable = function (type) {
        $scope.new.displayName = null;
        $scope.new.date = $scope.CurrentDate;
//        $scope.new.project = null;
        $scope.new.status = null;
        $scope.new.payPeriod = null;
        $scope.new.comment = null;
        $scope.new.attachments = null;
//        $scope.new.isDisabled=false;
        if (type == 'Ik' || type == 'All') {
            $scope.new.kilometer = 0;
            $scope.new.coefficient = null;
            $scope.new.amountIk = 0;
            $scope.new.checkIk = null;
        }
        if (type == 'Transport' || type == 'All') {
            $scope.new.nature = null;
            $scope.new.amountTran = 0;
            $scope.new.checkedTransport = null;
        }
        if (type == 'Accomodation' || type == 'All') {
            $scope.new.natureAcc = null;
            $scope.new.amountAcc = 0;
            $scope.new.tva = 0;
            $scope.new.untaxedAmount = 0;
            $scope.new.checkedAccomodation = null;
        }
        if (type == 'Dining' || type == 'All') {
            $scope.new.comment = null;
            $scope.new.payPeriod = null;
            $scope.new.amountDin = 0;
            $scope.new.taxe20 = 0;
            $scope.new.taxe10 = 0;
            $scope.new.taxe5 = 0;
            $scope.new.untaxedAmountDin = 0;
            $scope.new.checkDining = null;
            $scope.new.firstName = null;
            $scope.new.lastName = null;
            $scope.new.ArrayfirstName = [];
            $scope.new.ArraylastName = [];
        }
        if (type == 'Other' || type == 'All') {
            $scope.new.natureOth = null;
            $scope.new.amountOth = 0;
            $scope.new.taxe20 = 0;
            $scope.new.taxe10 = 0;
            $scope.new.taxe5 = 0;
            $scope.new.taxe2 = 0;
            $scope.new.untaxedAmountOth = '';
            $scope.new.checkOther = null;
        }
    }

    $scope.getCurrencies = function () {

        $scope.coefLoad = true;
        var getcurrencies = $resource(Routing.generate('currency_saved_currencies'));
        $scope.currencies = getcurrencies.get(
            function (data) {
                // $scope.coefLoad = false;
            },
            function (error) {
                // $scope.coefLoad = false;
            });
    };
//end Code
//Bareme kilometrique
    $scope.getBareme = function () {

        $scope.coefLoad = true;
        var getbareme = $resource(Routing.generate('apiSettingsExpCoeff_list'));
        $scope.bareme = getbareme.get(
            function (data) {
                $scope.coefLoad = false;
            },
            function (error) {
                $scope.coefLoad = false;
            });
    };

//End Ik methods


//Type appear/disapear
    $scope.appear = function (e) {
 
        if ($scope.new.type == 'Ik') {
            $scope.ik = true;
            $scope.transport = false;
            $scope.accomodation = false;
            $scope.dining = false;
            $scope.other = false;
            $( "#expensebundle_ik_type" ).val($scope.new.type).trigger('change');
            
        }
        if ($scope.new.type == 'Transport') {
            $scope.ik = false;
            $scope.transport = true;
            $scope.accomodation = false;
            $scope.dining = false;
            $scope.other = false;
            $( "#expensebundle_transport_type" ).val($scope.new.type).trigger('change');
        }
        if ($scope.new.type == 'Hebergement') {
            $scope.ik = false;
            $scope.transport = false;
            $scope.accomodation = true;
            $scope.dining = false;
            $scope.other = false;
            $( "#expensebundle_hrm_expenses_accomodation_type" ).val($scope.new.type).trigger('change');
        }
        if ($scope.new.type == 'Restauration') {
            $scope.ik = false;
            $scope.transport = false;
            $scope.accomodation = false;
            $scope.dining = true;
            $scope.other = false;
            $( "#expensebundle_hrm_expenses_dining_type" ).val($scope.new.type).trigger('change');
        }
        if ($scope.new.type == 'Autres') {
            $scope.ik = false;
            $scope.transport = false;
            $scope.accomodation = false;
            $scope.dining = false;
            $scope.other = true;
            $( "#expensebundle_hrm_expenses_other_type" ).val($scope.new.type).trigger('change');
        }
    }
//end type

//Appear/Disappear IN Dining Form
//    $scope.restauration = true;
    $scope.convivesappear = function () {
        if ($scope.new.settingsExpDining == '1' || $scope.edit.settingsExpDining == '1') {
            $scope.restauration = true;
            $scope.consommation = false;
        }
        if ($scope.new.settingsExpDining == '2' || $scope.edit.settingsExpDining == '2') {
            $scope.restauration = false;
            $scope.consommation = true;
        }
    }
//End 

//Appear/Disappear IN Accomodatio Form
//    $scope.accguests = true;
//    $scope.Accconvivesappear = function () {
//        if ($scope.new.natureAcc == 'Hébergement d\'invités') {
//            $scope.accguests = true;
//            $scope.accemployee = false;
//        }
//        if ($scope.new.natureAcc == 'Hébergement de salariés') {
//            $scope.accguests = false;
//            $scope.accemployee = true;
//        }
//    }
//End 
//     $('.datepicker').datepicker({
//         format: 'dd/mm/yyyy',
//         autoclose: true,
//     });
//     $('.datepicker').on("dp.change", function () {
//         $scope.new.date = $('.datepicker').val();
//         alert($scope.new.date);
//     });

//Detect and bind Datpickers values in model
    $('.paypriod-btn').datetimepicker({
        format: 'MM/YYYY',
        viewMode: "months",
    });
    $('.paypriod-btn').on("dp.change", function () {
        $scope.new.payPeriod = $('.paypriod-btn').val();
    });

    $('.edit-paypriod-btn').datetimepicker({
        format: 'MM/YYYY',
        viewMode: "months",
    });
    $('.edit-paypriod-btn').on("dp.change", function () {
        $scope.edit.payPeriod = $('.edit-paypriod-btn').val();
    });
//End functions

// Dynamic create tax model Inputs 
    $scope.getTaxModelInpout = function (path, scope_action, ExpenseType, ExpenseNature, expense, action) {
        Ie11.Params(path);
        Ie11.Params(expense);
        Ie11.Params(action);
        var _id_injct = '.inject-btn-' + ExpenseNature + '-' + scope_action;
        // console.log('try to get tax input');
        if (action === 'show')
            _id_injct = '.inject-btn';

        $(_id_injct).empty();
        if (path === null) {
            return;
        }


        var TaxModelInpout = $resource(Routing.generate(path, {
            expense: expense,
            type: ExpenseType,
            nature: ExpenseNature,
            action: action
        }));

        TaxModelInpout.get(
            function (data) {
                eval('delete $scope.' + scope_action + '.tax_list');
                eval('$scope.' + scope_action + '.tax_list = {}');
                eval('$scope.' + scope_action + '.tax_list.tax = {}');
                eval('$scope.' + scope_action + '.tax_list.rate = {}');
                eval('$scope.' + scope_action + '.tax_list.amount_rate = {}');
                if (data.scope_list !== '') {
                    var el = $(_id_injct).html(data.data);
                    $.each(data.scope_list.tax, function (index, item) {
                        eval("$scope." + scope_action + ".tax_list.tax." + index + " = " + item);
                    });
                    //rate compile
                    $.each(data.scope_list.rate, function (index, item) {
                        eval("$scope." + scope_action + ".tax_list.rate." + index + " = " + item);
                    });
                    $compile(el)($scope);
                }
            },
            function (error) {
                OnError(error.statusText);
            });


    };

// calculate tax value
    $scope.calculate_tax = function (amount, assignToHtamount, scope_action) {
        if (scope_action === undefined) {
            scope_action = 'new';
        }
        if ((eval('$scope.' + scope_action) === undefined)) {
            return;
        }
        /*   if ((eval('$scope.' + scope_action + '.tax_list') === undefined)) {
         return;
         }
         if (eval('$scope.' + scope_action + '.tax_list.length == 0')) {
         return;
         }*/

        var _ht_assign = parseFloat(amount);
        if (eval('$scope.' + scope_action + '.tax_list'))
            $.each(eval('$scope.' + scope_action + '.tax_list.tax'), function (index, item) {
                _ht_assign -= parseFloat(item);
            });
        eval('$scope.' + scope_action + '.' + assignToHtamount + '=' + ((isNaN(_ht_assign.toFixed(2))) ? 0 : _ht_assign.toFixed(2)));
    };
// show expense Charts
    $scope.expenseMonthDonutChart = function (period) {
        Ie11.Params(period);
        var getpayperiod = $resource(Routing.generate('expenses_static_data_donut_charts', {_period: period}));
        getpayperiod.get(null,
            function (data) {
                if (data.result !== undefined) {
                    var o_total_donut = 0;
                    var o_data = [];
                    var o_no_data = [];
                    var item = [];
                    var n_item = [];
                    var x = '';
                    $.each(data.result, function (i, val) {
                        if (i == 'valid') {
                            x = parseFloat(val) + " " + $scope.flashTranslate('VALID_F');
                        }
                        if (i == 'waiting') {
                            x = parseFloat(val) + " " + $scope.flashTranslate('WAITING');
                        }
                        if (i == 'draft') {
                            x = parseFloat(val) + " " + $scope.flashTranslate('IN_DRAFT');
                        }
                        item.push(x);// eval(x(i))
                        item.push(parseFloat(val));
                        o_total_donut += parseFloat(val);
                        o_data.push(item);
                        item = [];
                    });
                }
                if ((isNaN(parseFloat(o_data[0][1]))) && (isNaN(parseFloat(o_data[1][1]))) && (isNaN(parseFloat(o_data[2][1])))) {
                    // console.log("o_data no data expense");
                    // console.log(o_data);
                    n_item.push($scope.flashTranslate('NO_DATA'));
                    n_item.push("100");
                    o_no_data.push(n_item);
                    n_item = [];
                    o_total_donut = 0;
                    $scope.generateDonutCharts('#donut-chart-expense', o_no_data, 'donut', ["#DCDCDC"], o_total_donut);
                } else {
                    // console.log("data :)");
                    // console.log("o_data data expense");
                    // console.log(o_data);
                    $scope.generateDonutCharts('#donut-chart-expense', o_data, 'donut', ["#3A87AD", "#5cb85c", "#f57c00"], o_total_donut);
                }

            },
            function (error) {
                OnError(error.statusText);
            });
    };

    $scope.generateDonutCharts = function (elmtId, data, type, colors, o_total_donut) {
        c3.generate({
            bindto: elmtId,
            data: {
                columns: data,
                type: type
            },
            donut: {
                label: {
                    format: function (value) {
                        return value;
                    },
                    show: false
                }
            },
            tooltip: {
                format: {
                    value: function (value) {
                        // return $filter('number')(value, 2);
                        return value;
                    }
                },
                contents: function (d, defaultTitleFormat, defaultValueFormat, color) {
                    var $$ = this, config = $$.config,
                        titleFormat = config.tooltip_format_title || defaultTitleFormat,
                        nameFormat = config.tooltip_format_name || function (name) {
                            return name;
                        },
                        valueFormat = config.tooltip_format_value || defaultValueFormat,
                        text, i, title, value, name, bgcolor;
                    for (i = 0; i < d.length; i++) {
                        if (!(d[i] && (d[i].value || d[i].value === 0))) {
                            continue;
                        }

                        if (!text) {
                            title = titleFormat ? titleFormat(d[i].x) : d[i].x;
                            text = "<table class='" + $$.CLASS.tooltip + "'>" + (title || title === 0 ? "<tr><th colspan='2'>" + title + "</th></tr>" : "");
                        }

                        name = nameFormat(d[i].name);
                        value = valueFormat(d[i].value, d[i].ratio, d[i].id, d[i].index);
                        bgcolor = $$.levelColor ? $$.levelColor(d[i].value) : color(d[i].id);
                        if (!(data[0].indexOf("0 requests") > -1) && (!(data[0].indexOf("0 demandes") > -1))) {
                            name = name.replace(/\d+/g, '');
                            text += "<tr class='" + $$.CLASS.tooltipName + "-" + d[i].id + "'>";
                            text += "<td class='name'><span style='background-color:" + bgcolor + "'></span>" + value + " " + $scope.flashTranslate('REQUESTS') + " " + name + "</td>";
                            //  text += "<td class='value'>" + 1222 + "</td>";
                            text += "</tr>";
                        }
                    }
                    return text + "</table>";
                }
            },
            color: {pattern: colors}
        });

        var label = d3.select('text.c3-chart-arcs-title');
        label.html(''); // remove existant text
        label.insert('tspan').text(o_total_donut).attr('dy', 0).attr('x', 0).attr('class', 'big-font');
        label.insert('tspan').text($scope.flashTranslate('REQUESTS')).attr('dy', 20).attr('x', 0);
    };

// show absences Charts
    $scope.expenseYearsBarCharts = function (year) {
        Ie11.Params(year);
        var router = $resource(Routing.generate('expenses_static_data_bar_charts', {_year: year}));
        router.get(null,
            function (data) {
                var data_bar_chart = [], x_axis = ['x'], item_valid = [$scope.flashTranslate('VALID_M')],
                    item_waiting = [$scope.flashTranslate('WAITING')];
                if (data.result !== undefined) {
                    var format_axis_x = '%Y-%m';
                    if (data.format === 'fr-fr') {
                        format_axis_x = '%m-%Y';
                    }


                    $.each(data.result, function (i, el) {
                        x_axis.push(el.month + '-01');
                        item_valid.push(parseFloat(el.valid).toFixed(2));
                        item_waiting.push(parseFloat(el.waiting).toFixed(2));
                    });
                    data_bar_chart.push(item_valid);
                    data_bar_chart.push(item_waiting);
                    /*****/
                    data_bar_chart.push(x_axis);
                    c3.generate({
                        bindto: '#bar-chart-expense',
                        data: {
                            x: 'x',
                            type: 'bar',
                            columns: data_bar_chart,
                            groups: [
                                [$scope.flashTranslate('VALID_M'), $scope.flashTranslate('WAITING')]
                            ],

                        },
                        axis: {
                            x: {
                                type: 'timeseries',
                                tick: {
                                    format: format_axis_x,
                                    centered: true
                                }
                            },
                            y: {
                                tick: {
                                    format: function (value) {
                                        return $filter('number')(value, 2);
                                    }
                                },
                                label: {
                                    text: $scope.flashTranslate('AMOUNT'),
                                    position: 'outer-middle'
                                },
                                tooltip: {
                                    format: {
                                        value: function (value) {
                                            return $filter('number')(value, 2);
                                        }
                                    }
                                }
                            }
                        },
                        color: {pattern: ["#3A87AD", "#5cb85c"]}
                    });
                    /*****/
                }
            },
            function (error) {
                OnError(error.statusText);
            });
    };


    /***************** show data grid  chart **************/
    $scope.expenseMonthGridChart = function (period) {
        Ie11.Params(period);
        $scope.loading = true;
        $scope.blockloader = true;

        var infos = $resource(Routing.generate('expenses_static_data_grid_charts', {_period: period}));
        infos.get(
            function (ResponseData) {
                $scope.data = ResponseData.result;
                $scope.perPage = 4;
                $scope.maxSize = 5;
                $scope.setPage = function (pageNo) {
                    $scope.currentPage = pageNo;
                };
                $scope.loading = false;
                $scope.blockloader = false;
            },
            function (error) {
            });
    };

});

/** directive datepicker of payperiod **/

