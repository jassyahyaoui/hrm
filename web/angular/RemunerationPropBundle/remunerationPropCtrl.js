app.controller('remunerationPropCtrl', function ($scope, $resource, $filter, Ie11, Global) {

    $scope.showMasse = false;
    $scope.flashText = {};

    $scope.flashTranslate = function (alias) {

        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };
    
    $scope.showPrime = true;
    $scope.showTransport = true;
    $scope.showIntSalaire = true;
    $scope.showAvgNature = true;
    $scope.showHeureSupp = true;
    $scope.selectAll = function (e) {
        $scope.rowsColumns.forEach(function (v) {
            v.bool = $scope.checkedAll;
        });
    };
    
    $scope.rowsColumns = [
        {'id': 1,'category': 'Prime','item':'A la convention collective A la convention collective',bool: 0}, 
        {'id': 2,'category': 'Prime','item':'Aux performances Prime de bilan',bool: 1}, 
        {'id': 3,'category': 'Prime','item':'Aux performances Prime de rendement',bool: 1},
        {'id': 4,'category': 'Prime','item':'Aux performances Prime sur objectifs',bool: 0},
        {'id': 5,'category': 'Prime','item':'Aux performances Prime d’assiduité',bool: 0},
        {'id': 6,'category': 'Prime','item':'Aux performances Prime de sécurité',bool: 0},
        {'id': 7,'category': 'Prime','item':'Aux performances Autres',bool: 0},
        {'id': 8,'category': 'Prime','item':'Au type de travail effectué Prime de dimanche',bool: 0},
        {'id': 9,'category': 'Prime','item':'Au type de travail effectué Prime de risque/danger',bool: 0},
        {'id': 10,'category': 'Prime','item':'Au type de travail effectué Prime de froid',bool: 0},
        {'id': 11,'category': 'Prime','item':'Au type de travail effectué Prime de pénibilité',bool: 0},
        {'id': 12,'category': 'Prime','item':'Au type de travail effectué Prime de nuit',bool: 0},
        {'id': 13,'category': 'Prime','item':'Au type de travail effectué Prime d’insalubrité',bool: 0},
        {'id': 14,'category': 'Prime','item':'Au type de travail effectué Autre',bool: 0},
        {'id': 15,'category': 'Prime','item':'A un évènement Prime de fin d’année',bool: 0},
        {'id': 16,'category': 'Prime','item':'A un évènement Prime de Noël',bool: 0},
        {'id': 17,'category': 'Prime','item':'A un évènement Prime de naissance',bool: 0},
        {'id': 18,'category': 'Prime','item':'A un évènement Prime de mariage',bool: 0},
        {'id': 19,'category': 'Prime','item':'A un évènement Autre',bool: 0},
        {'id': 20,'category': 'Prime','item':'Autre Prime de 13ème mois',bool: 1},
        {'id': 21,'category': 'Prime','item':'Autre Prime d’habillage/d’outillage',bool: 0},
        {'id': 22,'category': 'Prime','item':'Autre Autre',bool: 0},
        {'id': 23,'category': 'Transport','item':'Public',bool: 1},
        {'id': 24,'category': 'Transport','item':'Personnel',bool: 0},
        {'id': 25,'category': 'Heures supplémentaires','item':'25%',bool: 1},
        {'id': 26,'category': 'Heures supplémentaires','item':'50%',bool: 1},
        {'id': 27,'category': 'Int salaire','item':'Saisie',bool: 0},
        {'id': 28,'category': 'Int salaire','item':'Avance',bool: 0},
        {'id': 29,'category': 'Int salaire','item':'Acompte',bool: 1},
        {'id': 30,'category': 'Avantage en nature','item':'Nourriture',bool: 1},
        {'id': 31,'category': 'Avantage en nature','item':'Logement',bool: 1},
        {'id': 32,'category': 'Avantage en nature','item':'Véhicule',bool: 0},
        {'id': 33,'category': 'Avantage en nature','item':'NTIC',bool: 1},
        {'id': 34,'category': 'Avantage en nature','item':'Cadeau',bool: 0},
        {'id': 35,'category': 'Avantage en nature','item':'Autre',bool: 0}
    ];
    $scope.deleteAllColumns = function(){
        $scope.rowsColumns.forEach(function (x) {
            x.bool = 0;
        });
    };
    $scope.updateItem = function(x, y){
        if (y === 1){
            $scope.rowsColumns.forEach(function (v) {
                if (v === x)
                v.bool = 0;
            });
    }
        else {
            $scope.rowsColumns.forEach(function (v) {
                if (v === x)
                v.bool = 1;
            });
        }
    };
});
