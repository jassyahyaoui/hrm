"use strict";
var app = angular.module('fabereoApp', ['ngResource', 'ui.bootstrap', 'angular-filepicker', 'ngMask', 'pascalprecht.translate','ngRoute','ui.calendar','ngTagsInput', 'ui.sortable','ngTagsInput']);

app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});

//Filepicker Key
app.config(function (filepickerProvider) {
    filepickerProvider.setKey('AYMtrggo0TACjDPBMsQ3uz');
})

app.constant('uiCalendarConfig', {
        calendars : {}
    })
