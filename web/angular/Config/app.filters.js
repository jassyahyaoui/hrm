app.filter('unique', function() {
    return function(collection, keyname) {
        var output = [],
            keys = [];

        angular.forEach(collection, function(item) {
            var key = item[keyname];
            if(keys.indexOf(key) === -1) {
                keys.push(key);
                output.push(item);
            }
        });
        return output;
    };
});
app.filter('start', function () {
    return function (input, start) {
        if (!input || !input.length) {
            return;
        }
        start = +start;
        return input.slice(start);
    };
});

app.filter('timezone', function () {

    return function (val, offset) {
        if ((val !== null) && (val !== undefined) && (val.length > 16)) {
            return val.substring(0, 16);
        }
        return val;
    };
});

app.filter('Contributor', function ($scope, term) {
    var obj = term;
    $scope.contributorText = $filter('filter')($scope.CollabArrayValues, obj);
    $scope.currentPage = 1;
});
app.filter('nospace', function () {
  return function (value) {
    return (!value) ? '' : value.replace(/ /g, '');
  };
});