app.directive('fpCustomDirective', function (filepickerService) {
    return {
        scope: {
            options: '=',
            onSuccess: '&',
            onError: '&',
        },
        template: '<button class="fp__btn" ng-click="openPicker()">Joindre des fichiers</button>',
        link: function (scope, elm, attrs) {
            scope.openPicker = openPicker;
            scope.options = scope.options || {};

            function openPicker() {
                filepickerService.pick(
                    scope.options,
                    function (Blob) {
                        scope.onSuccess({Blob: Blob});
                    },
                    function (Error) {
                        scope.onError({Error: Error});
                    }
                );
            }
        }
    };
});

app.directive("preference", function ($filter) {
    return {
        restrict: "A", // Only usable as an attribute of another HTML element
        require: "?ngModel",
        scope: {
//            decimals: "@",
            preference: "@"
        },
        link: function (scope, element, attr, ngModel) {
            scope.decimals = 2;
            var decimalCount = parseInt(scope.decimals) || 2;
            if (scope.preference === "fr-fr") {
                scope.decimalPoint = ",";
            } else if (scope.preference === "en-us") {
                scope.decimalPoint = ".";
            }
            var decimalPoint = scope.decimalPoint || ".";

            // Run when the model is first rendered and when the model is changed from code
            ngModel.$render = function () {
                if (ngModel.$modelValue != null && ngModel.$modelValue >= 0) {
                    if (typeof decimalCount === "number") {
                        element.val(ngModel.$modelValue.toFixed(decimalCount).toString().replace(".", ","));
                    } else {
                        element.val(ngModel.$modelValue.toString().replace(".", ","));
                    }
                }
            }

            // Run when the view value changes - after each keypress
            // The returned value is then written to the model
            ngModel.$parsers.unshift(function (newValue) {
                if (typeof decimalCount === "number") {
                    var floatValue = parseFloat(newValue.replace(",", "."));
                    if (decimalCount === 0) {
                        return parseInt(floatValue);
                    }
                    return parseFloat(floatValue.toFixed(decimalCount));
                }

                return parseFloat(newValue.replace(",", "."));
            });

            // Formats the displayed value when the input field loses focus
            element.on("change", function (e) {
                var floatValue = parseFloat(element.val().replace(",", "."));
                if (!isNaN(floatValue) && typeof decimalCount === "number") {
                    if (decimalCount === 0) {
                        element.val(parseInt(floatValue));
                    } else {
                        var strValue = floatValue.toFixed(decimalCount);
                        element.val(strValue.replace(".", decimalPoint));
                    }
                }
            });
        }
    }
});

app.directive('paypperiodDatepicker', ['$timeout', '$filter', function ($timeout, $filter) {
    return {
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
            $(el).datetimepicker({
                toolbarPlacement: 'bottom',
                showClear: true,
                showClose: true,
                format: 'MM/YYYY',
                viewMode: "months",
                minDate: '01/01/1900',
                icons: {
                    // time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-arrows ',
                    clear: 'fa fa-trash',
                    close: 'fa fa-times'
                }
            }).on("dp.change", function (e) {
                $timeout(function () {
                    date = moment($filter('timezone')(e.date)).format('MM/YYYY');
                    ngModel.$setViewValue(date);
                });
            });
        }
    }
}]);


app.directive('dateDirective', ['$timeout', '$filter', function ($timeout, $filter) {
    return {
        require: 'ngModel',
        link: function (scope, el, attr, ngModel) {
            $(el).datetimepicker({
                toolbarPlacement: 'bottom',
                showClear: true,
                showClose: true,
                format: 'DD/MM/YYYY',
                minDate: '01/01/1900',
                icons: {
                    // time: 'fa fa-clock-o',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-arrows ',
                    clear: 'fa fa-trash',
                    close: 'fa fa-times'
                }
            }).on('dp.change', function (e) {
                $timeout(function () {
                    date = moment($filter('timezone')(e.date)).format('DD/MM/YYYY');
                    ngModel.$setViewValue($.trim(date));
                });
            });
        }
    };
}]);

app.directive('numberFormat', ['$filter', '$parse', function ($filter, $parse) {
        return {
            restrict: 'AE',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModelController) {
                var decimals = 2;// $parse(attrs.decimals)(scope);
                ngModelController.$parsers.push(function (data) { // lors de l'ecriture retourn valeur si valide si non undefined
                    var parsed = parseFloat(data);
                    return !isNaN(parsed) ? parsed : undefined;
                });
                ngModelController.$formatters.push(function (data) {
                    //convert data from model format to view format
                    return $filter('number')(data, decimals); //converted
                });
                element.bind('focus', function () {
                    element.val(ngModelController.$modelValue);
                });
                element.bind('blur', function () {
                    // Apply formatting on the stored model value for display
                    var formatted = $filter('number')(ngModelController.$modelValue, decimals);
                    element.val(formatted);
                });
            }
        }
    }]);

app.directive("onNew", function() {
    'use strict';
        return {
            restrict: "AE",
            link: function(scope, elem, attrs) {
                elem.on('click', function(){
                     scope.new();
                     scope.$apply();
                });
                
            }
        }
    });

app.directive("typeSuggestions",['$filter', '$parse','$timeout', function($filter, $parse, $timeout) {
    'use strict'; 
        return {
            restrict: "EA",
            require:"ngModel",
            scope: {
                datasource: '=',    
                ngModel : '=',
                ngChange : "&"
            },
            link: function(scope, elem, attrs, ngModel, ctrl) {
                $( elem ).select2({ 
                        placeholder: ((scope.datasource.length > 0)? $filter('translate')(scope.datasource[0]): ''), 
                        width:"100%",
                        allowClear: true
                    }).on('change', function (e) {
                        ngModel.$setViewValue(this.value);
                   });
                    var v;
                   if(undefined !== scope.datasource[1])
                   {
                        if(null !== scope.datasource[1])
                             v = scope.datasource[1];
                        else
                             v = $(elem).children().eq(1).val();
                        $( elem ).val(v).trigger('change');

                   }
        }
    };

}]);