//var app = angular.module('fabereoApp', []);
// 'use strict';
// service
app.service('dataService', function () {
    this.payPeriodSearch = {
        item: undefined
    }
    this.collaborateurSearch = {
        item: undefined
    }
    this.searchText = {
        item: undefined
    }
});

app.factory('Ie11', function () {
    return {
        Params: function (param) {
            if (param === undefined) {
                param = null;
            }
        }
    };
});
// app.service('equal', function (connected, requester) {
//     if (connected === requester) {
//         return true;
//     } else {
//         return false;
//     }
//
// });
app.factory('Global', function ($resource) {
    return {
        Remove:
            function (scope) {
                // scope.IsDisabled = true;
                var ActionTab = [];
                var IdTab = [];
                var c = 0;
                $("input:checkbox:checked").each(function () {
                    if ($(this).attr('data-status') === 'Brouillon') {
                        if ($(this).attr('data-user') === 'true') {
                            ActionTab.push($(this).attr('data-action'));
                            IdTab.push($(this).attr('data-id'));
                        }
                    }
                });
                if (ActionTab.length > 0) {
                    for (var i = 0; i < ActionTab.length; i++) {
                        c++;
                        if (c == ActionTab.length) {
                            var remove = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}));
                            remove.delete(
                                function (data) {
                                    $('.close').click();
                                    scope.data = data;
                                    scope.datatable();
                                    flash(scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');

                                    scope.IsDisabled = false;
                                },
                                function (error) {
                                    flash(scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');

                                    // scope.IsDisabled = false;
                                });
                        } else {
                            var remove = $resource(Routing.generate(ActionTab[i], {id: IdTab[i]}));
                            remove.delete(
                                function (data) {
                                    scope.IsDisabled = false;
                                },
                                function (error) {
                                    flash(scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                                    scope.IsDisabled = false;
                                });
                        }
                    }
                } else {
                    $('.close').click();
                }
            }
    };
});

app.factory('Utils', function ($q) {
    return {
        isImage: function (src) {

            var deferred = $q.defer();

            var image = new Image();
            image.onerror = function () {
                deferred.resolve(false);
            };
            image.onload = function () {
                deferred.resolve(true);
            };
            image.src = src;

            return deferred.promise;
        }
    };
});
