
app.run(function ($rootScope, $translate) {

//     Translate Service 
    $rootScope.translateAction = function (flag) {
        $translate.use(flag);
    };
//     End Translate Service 

    // Toggle button filter sort (up/down)
    $rootScope.up1 = true;
    $rootScope.toggleUp1 = function () {
        $rootScope.up1 ? $rootScope.Downb1() : $rootScope.Upb1();
    };
    $rootScope.Downb1 = function () {
        $rootScope.up1 = false;
    };
    $rootScope.Upb1 = function () {
        $rootScope.up1 = true;
    };

    // Toggle button filter sort (up/down)	
    $rootScope.up2 = true;
    $rootScope.toggleUp2 = function () {
        $rootScope.up2 ? $rootScope.Downb2() : $rootScope.Upb2();
    };
    $rootScope.Downb2 = function () {
        $rootScope.up2 = false;
    };
    $rootScope.Upb2 = function () {
        $rootScope.up2 = true;
    };

    // Toggle button to display card/table presentation 	
    $rootScope.showTable = true;
    $rootScope.showCard = false;
    $rootScope.showTab = function () {
        $rootScope.showTable = true;
        $rootScope.showCard = false;
    };
    $rootScope.showCardP = function () {
        $rootScope.showTable = false;
        $rootScope.showCard = true;
    };

    // Toggle button to display bloc simple/multiple 	
    $rootScope.toggleSimple = true;
    $rootScope.toggleMultiple = false;
    $rootScope.toggleS = function () {
        $rootScope.toggleSimple = true;
        $rootScope.toggleMultiple = false;
    };
    $rootScope.toggleM = function () {
        $rootScope.toggleSimple = false;
        $rootScope.toggleMultiple = true;
    };

    $rootScope.plus = true;
    /*$rootScope.togglePlus = function() {
     $rootScope.toggle-plus = !$rootScope.toggle-plus;
     }*/

    // Toggle button to display ropdown details filters 	
    $rootScope.custom = true;
    $rootScope.toggleCustom = function () {
        $rootScope.custom = $rootScope.custom === false ? true : false;
    };
    $rootScope.custom_search = true;
    $rootScope.toggleCustomSearch = function () {
        $rootScope.custom_search = $rootScope.custom_search === false ? true : false;
    };
    $rootScope.custom_date_search = true;
    $rootScope.toggleCustomDateSearch = function () {
        $rootScope.custom_date_search = $rootScope.custom_date_search === false ? true : false;
    };
    $rootScope.custom_search_bonus_type = true;
    $rootScope.toggleCustomSearchBonusType = function () {
        $rootScope.custom_search_bonus_type = $rootScope.custom_search_bonus_type === false ? true : false;
    };


    $rootScope.statutSearch = '';
    $rootScope.filterByStatus = function (status) {
        if (status === 'All') {
            $rootScope.statutSearch = '';
        } else {
            $rootScope.statutSearch = status;
        }
    }

    $rootScope.transportTypeSearch = '';
    $rootScope.filterByTypeTransport = function (transportType) {
        if (transportType === 'All') {
            $rootScope.transportTypeSearch = '';
        } else {
            $rootScope.transportTypeSearch = transportType;
        }
    }

    $rootScope.contributorSearch = '';
    $rootScope.filterByContributor = function (contributor) {
        if (contributor === 'All') {
            $rootScope.contributorSearch = '';
        } else {
            $rootScope.contributorSearch = contributor;
        }
    }
    $rootScope.equality = function (connected, requester) {
        if (connected === requester) {
            return true;
        } else {
            return false;

        }
    };


});
