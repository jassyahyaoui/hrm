app.controller("contractLinkDataCtrl", function ($scope, $resource, $filter) {
    $scope.sortableOptions = {
        update: function (e, ui) {
            $scope.seqnoUpdate('api_seqno_update_contract_link', ui.item.scope().contract_link.id);
        }
    };

    //Global function Update
    $scope.seqnoUpdate = function (route, id) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        edit.update(null, $scope.tasks,
            function (data) {
            },
            function (error) {
                alert('NOT updated');
            });

    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    }
//
    $scope.ShowByCategory = function () {
        $scope.loading = true;
        $scope.blockloader = true;
        var dataResult = $resource(Routing.generate('api_hrm_contract_link_by_category'));

        $scope.newResult = dataResult.get(
            function (data) {
                console.log(data.result);
                var tasks = [];
                $scope.tasks = data.result;

                $scope.perPage = 10;
                $scope.maxSize = 5;
                $scope.setPage = function (pageNo) {
                    $scope.currentPage = pageNo;
                };
                $scope.$watch('searchText', function (term) {
                    var obj = term;
                    $scope.filterList = $filter('filter')(tasks, obj);
                    $scope.currentPage = 1;
                });

                $scope.loading = false;
                $scope.blockloader = false;
            },
            function (error) {
            });

    };
    $scope.init = function () {
        $scope.loading = true;
        $scope.blockloader = true;
        var dataResult = $resource(Routing.generate('api_hrm_contract_link_all'));

        $scope.newResult = dataResult.get(
            function (data) {
                console.log(data.result);
                var tasks = [];
                $scope.tasks = data.result;

                $scope.perPage = 10;
                $scope.maxSize = 5;
                $scope.setPage = function (pageNo) {
                    $scope.currentPage = pageNo;
                };
                $scope.$watch('searchText', function (term) {
                    var obj = term;
                    $scope.filterList = $filter('filter')(tasks, obj);
                    $scope.currentPage = 1;
                });

                $scope.loading = false;
                $scope.blockloader = false;
            },
            function (error) {
            });

    };
//
    $scope.SelectInfos = function (id, type) {
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.tasks.length; k++) {
            if (typeof $scope.tasks[k].id !== "undefined") {
                if ($scope.tasks[k].id == id && 'contract_link' == type) {
                    $scope.edit = $scope.tasks[k];
                    $scope.edit.hrm_contract_link_category.id = ($scope.tasks[k].hrm_contract_link_category.id).toString();

                }
            }
        }
        console.log($scope.edit);
    }
//
    $scope.save = function (route) {
        // $scope.IsDisabled = true;

        var url = $resource(Routing.generate(route));
        url.save($scope.new,
            function (data) {

                flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                if ($('#checkbox').is(":checked")) {
                    $scope.new.title = null;
                    $scope.new.href = null;
                    $scope.new.target = null;
                    $scope.new.hrm_contract_link_category = null;
                    $scope.new.checkbox = null;
                    $scope.IsDisabled = false;
                    // return;
                } else {
                    $scope.new.title = null;
                    $scope.new.href = null;
                    $scope.new.target = null;
                    $scope.new.hrm_contract_link_category = null;
                    $scope.new.checkbox = null;
                    $('.close').click();
                    $scope.IsDisabled = false;
                }
                $scope.init();
            },
            function (error) {

                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                $scope.IsDisabled = false;

            });
    }


// update general method
    $scope.update = function (route, id) {
        $scope.IsDisabled = true;
        var edit = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        edit.update(null, $scope.edit,
            function (data) {

                flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.init();
                $('.close').click();
                $scope.IsDisabled = false;
            },
            function (error) {

                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                $scope.IsDisabled = false;
            });
    };
//
//
    $scope.delete = function (route,id) {
        var deletescope = $resource(Routing.generate(route, {id: id}));
        deletescope.delete(
            function (data) {

                flash($scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                $scope.init();
            },
            function (error) {
                flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
            });
    };


});