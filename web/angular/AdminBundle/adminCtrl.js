//var app = false;
(function () {
    'use strict';
//    app = angular.module("adminApp", ["ngRoute", "ngResource", "datatables", 'angular-filepicker', 'ngMask']);

    app.controller("adminCtrl", function ($scope, $resource) {



        $scope.differenceInDays = function () {

            if ($scope.datedeb === undefined || $scope.datefin === undefined)
                return 0;
            var dt1 = $scope.datedeb.split('/'),
                    dt2 = $scope.datefin.split('/'),
                    one = new Date(dt1[2], dt1[0], dt1[1]),
                    two = new Date(dt2[2], dt2[0], dt2[1]);

            var millisecondsPerDay = 1000 * 60 * 60 * 24;
            var millisBetween = two.getTime() - one.getTime();
            var days = millisBetween / millisecondsPerDay;

            return Math.floor(days);
        };




//$scope.saveAbsenceSettings = function () {
//        $scope.IsDisabled = true;
//
//        if ($scope.new_settingabsence == null)
//        {   
//            console.log("invalid new_settingabsence")
//            return;
//        }
//        var postaction = $resource(Routing.generate('api_postrequestabsence'), null,
//                {
//                    'post': {method: 'POST'}
//                });
//        postaction.post(null, $scope.new_settingabsence,
//                function (data) {
//                    $scope.IsDisabled = false;
//                    $scope.InitFields();
//                    if ($scope.createother == true)
//                    {
//                        return;
//                    } else
//                    {
//                        $('.close').click();
//                        $scope.IsDisabled = false;
//                    }
//                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
//                },
//                function (error) {
//                    OnError("Votre demande est invalide");
//                });
//    };

        $scope.save = function ()
        {

            var pt = [];
            var emp = [];
            var Prime = $resource(Routing.generate('primes_save'));
            $scope.dateRemb;
            $.each($scope.selectedPrimeTypeIds, function () {
                pt.push(this.id);
            });
            $.each($scope.selectedCollIds, function () {
                emp.push(this.id);
            });

            // if((emp.length === 0) || (pt.length === 0) || ($scope.dateRemb === ''))
            var validator = $("form").kendoValidator().data("kendoValidator");

            if (validator.validate() === false)
                return;

            postData = {employeId: emp, primeId: pt, period: $scope.dateRemb, total: $scope.totalPrimes};

            ret = Prime.save(postData,
                    function (successResult) {
                        alert("Saved!");
                    },
                    function (errorResult) {
                        alert("Error!");
                    });
        };

        $scope.selectColl = {
            placeholder: "Choisir un employé...",
            dataTextField: "name",
            valuePrimitive: true,
            autoBind: true,
            dataSource: {
                serverFiltering: true,
                transport: {
                    read: {
                        dataType: "json",
                        url: Routing.generate('primes_collaborateur')
                    }
                }
            }
        };

        $scope.selectPrimeType = {
            placeholder: "Choisir une prime...",
            dataTextField: "name",
            valuePrimitive: true,
            autoBind: true,
            dataSource: {
                serverFiltering: true,
                transport: {
                    read: {
                        dataType: "json",
                        url: Routing.generate('primes_type')
                    }
                }
            }
        };

        $scope.selectedIds = [4, 7];

        $scope.getTotal = function (selectedPrimeTypeIds)
        {
            $scope.totalPrimes = 0;
            if (jQuery.isEmptyObject(selectedPrimeTypeIds))
                return;
            $.each(selectedPrimeTypeIds, function () {
                $scope.totalPrimes += this.value;
            });
            $scope.totalPrimes;

        };

        // Save conges
        $scope.saveConges = function ()
        {

            var Conges = $resource(Routing.generate('primes_conges_save'));
            $scope.dateRemb;
            $.each($scope.selectedPrimeTypeIds, function () {
                pt.push(this.id);
            });
            $.each($scope.selectedCollIds, function () {
                emp.push(this.id);
            });


            postData = {datedeb: $scope.datedeb, datefin: $scope.datefin, type: $scope.type, note: $scope.note};

            ret = Conges.save(postData,
                    function (successResult) {
                        alert("Saved!");
                    },
                    function (errorResult) {
                        alert("Error!");
                    });
        };

        // Add collaborateur
        $scope.save_collab = function ()
        {
            var acme_collab = $resource(Routing.generate('acme_collaborateur_save'));
            console.log($scope.data);

            postData = $scope.data;

            ret = acme_collab.save(postData,
                    function (successResult) {
                        alert("Saved!");
                    },
                    function (errorResult) {
                        alert("Error!");
                    });
        };

    });

    app.config(function ($interpolateProvider, $locationProvider) {
        $locationProvider.hashPrefix('');
//        $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
    });

//    $(document).ready(function () {
    // create DatePicker from input HTML element
    /*
     $(".js-datepicker").kendoDatePicker({
     start: "year",
     depth: "year",
     format: "MM/yyyy"
     });
     $(".full-js-datepicker").kendoDatePicker({
     format: "MM/dd/yyyy"
     });  */

//    });

//    app.config(function (filepickerProvider) {
//        filepickerProvider.setKey('AwMr7Yc2nQX2zdOcs5Q1Az');
//    })
//
//    app.service('angularFilepicker', function ($window) {
//        return $window.filepicker;
//    });

// Conges controller
//    app.controller("CongesCtrl", function ($scope, $resource) {
//
//        $scope.init = function ()
//        {
//            var CongesList = $resource(Routing.generate('adminConges_list'));
//
//            $scope.data = CongesList.get({display: 'full'},
//                    function (data) {
//                        
//                    },
//                    function (error) {
//                        OnError(error.statusText);
//                    });
//
//        };
//
//        $scope.path = function (route, id = null)
//        {
//            if (id == null)
//                window.location = Routing.generate(route).replace('%23', '#');
//            else
//                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
//        };
//
//        $scope.setCurrentId = function (id)
//        {
//            var CongesId = $resource(Routing.generate('adminConges_get', {id: id}));
//
//            $scope.detail = CongesId.get({display: 'no'},
//                    function (data) {
//                        
//                    },
//                    function (error) {
//                        OnError(error.statusText);
//                    });
//
//        };
//
//        $scope.save = function ()
//        {
//            var CongesSave = $resource(Routing.generate('adminConges_save'));
//            CongesSave.save($scope.data,
//                    function (data) {
//                        if (data.http_code == '200')
//                        {
//                            OnSuccess(data.message);
//                        } else
//                        {
//                            OnError(data.message);
//                        }
//                        if ($('.new-item').is(":checked"))
//                        {
//                            for (var key in $scope.data) {
//                                $scope.data[key] = ""
//                            }
//                            return;
//                        }
//                        window.location = Routing.generate('adminConges_index').replace('%23', '#');
//                    },
//                    function (error) {
//                        OnError(error.statusText);
//                    });
//        };
//
//        $scope.update = function (id)
//        {
//
//            var CongesUp = $resource(Routing.generate('adminConges_update', {id: id}), null,
//                    {
//                        'update': {method: 'PUT'}
//                    });
//
//            CongesUp.update(null, $scope.detail,
//                    function (data) {
//                        if (data.http_code == '200')
//                        {
//                            OnSuccess(data.message);
//                        } else
//                        {
//                            OnError(data.message);
//                        }
//                        window.location = Routing.generate('adminConges_index').replace('%23', '#');
//                    },
//                    function (error) {
//                        OnError(error.statusText);
//                    });
//        };
//
//        $scope.delete = function (id)
//        {
//            var CongesDel = $resource(Routing.generate('adminConges_delete', {id: id}));
//            CongesDel.delete(
//                    function (data) {
//                        if (data.http_code == '200')
//                        {
//                            OnSuccess(data.message);
//                        } else
//                        {
//                            return  OnError(data.message);
//                        }
//
//                        window.location = Routing.generate('adminConges_index').replace('%23', '#');
//                        $scope.init();
//                    },
//                    function (error) {
//                        OnError(error.statusText);
//                    });
//        };
//
//    });

// TypeConges controller
    app.controller("TypeCongesCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }


        $scope.init = function ()
        {
            var TypeCongesList = $resource(Routing.generate('adminTypeConges_list'));

            $scope.data = TypeCongesList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var TypeCongesId = $resource(Routing.generate('adminTypeConges_get', {id: id}));

            $scope.detail = TypeCongesId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {

            var TypeCongesSave = $resource(Routing.generate('adminTypeConges_save'));
            TypeCongesSave.save($scope.new_settingcongetype,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingcongetype) {
                                $scope.new_settingcongetype[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };
        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.typeconges.length; k++) {
                if (typeof $scope.data.typeconges[k].id !== "undefined")
                {
                    if ($scope.data.typeconges[k].id == id && 'Typeconge' == type)
                    {
                        $scope.edit_settingcongetype = angular.copy($scope.data.typeconges[k]);
                    }
                }
            }
        }
        $scope.update = function (id)
        {
            var TypeCongesUp = $resource(Routing.generate('adminTypeConges_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            TypeCongesUp.update(null, $scope.edit_settingcongetype,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingcongetype) {
                            $scope.edit_settingcongetype[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var TypeCongesDel = $resource(Routing.generate('adminTypeConges_delete', {id: id}));
            TypeCongesDel.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

    });

// Projets controller
    app.controller("ProjectsCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }


        $scope.init = function ()
        {
            var ProjetsList = $resource(Routing.generate('adminProjects_list'));

            $scope.data = ProjetsList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var ProjetsId = $resource(Routing.generate('adminProjects_get', {id: id}));

            $scope.detail = ProjetsId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var ProjetsList = $resource(Routing.generate('adminProjects_save'));
            ProjetsList.save($scope.data,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        if ($('.new-item').is(":checked"))
                        {
                            for (var key in $scope.data) {
                                $scope.data[key] = ""
                            }
                            return;
                        }
                        window.location = Routing.generate('adminProjects_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.update = function (id)
        {

            var ProjetsUp = $resource(Routing.generate('adminProjects_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            ProjetsUp.update(null, $scope.detail,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        window.location = Routing.generate('adminProjects_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var ProjetsDel = $resource(Routing.generate('adminProjects_delete', {id: id}));
            ProjetsDel.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            return  OnError(data.message);
                        }

                        window.location = Routing.generate('adminProjects_index').replace('%23', '#');
                        $scope.init();
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

    });

// Roles controller
    app.controller("RolesCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }


        $scope.init = function ()
        {
            var RolesList = $resource(Routing.generate('adminRoles_list'));

            $scope.data = RolesList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var RolesId = $resource(Routing.generate('adminRoles_get', {id: id}));

            $scope.detail = RolesId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var RolesSave = $resource(Routing.generate('adminRoles_save'));
            RolesSave.save($scope.new_settingrole,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingrole) {
                                $scope.new_settingrole[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };
        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.roles.length; k++) {
                if (typeof $scope.data.roles[k].id !== "undefined")
                {
                    if ($scope.data.roles[k].id == id && 'Role' == type)
                    {
                        $scope.edit_settingrole = angular.copy($scope.data.roles[k]);
                    }
                }
            }
        }
        $scope.update = function (id)
        {

            var RolesUp = $resource(Routing.generate('adminRoles_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            RolesUp.update(null, $scope.edit_settingrole,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingrole) {
                            $scope.edit_settingrole[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var RolesDel = $resource(Routing.generate('adminRoles_delete', {id: id}));
            RolesDel.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

    });

// Categories controller
    app.controller("CategoriesCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }


        $scope.init = function ()
        {
            var CategoriesList = $resource(Routing.generate('adminCategories_list'));

            $scope.data = CategoriesList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var CategoriesId = $resource(Routing.generate('adminCategories_get', {id: id}));

            $scope.detail = CategoriesId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var CategoriesSave = $resource(Routing.generate('adminCategories_save'));
            CategoriesSave.save($scope.data,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        if ($('.new-item').is(":checked"))
                        {
                            for (var key in $scope.data) {
                                $scope.data[key] = "";
                            }
                            return;
                        }
                        window.location = Routing.generate('adminCategories_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.update = function (id)
        {

            var CategoriesUp = $resource(Routing.generate('adminCategories_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            CategoriesUp.update(null, $scope.detail,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        window.location = Routing.generate('adminCategories_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var CategoriesDel = $resource(Routing.generate('adminCategories_delete', {id: id}));
            CategoriesDel.delete(
                    function (data) {
                        if (data.http_code)
                        {
                            if (data.http_code)
                            {
                                OnSuccess(data.message);
                            } else
                            {
                                OnError(data.message);
                            }


                            window.location = Routing.generate('adminCategories_index').replace('%23', '#');
                            $scope.init();
                        }

                        console.log(data);
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };


    });

// Type Contracts controller
    app.controller("TypeContractsCtrl", function ($scope, $resource, $filter) {

        $scope.sortableOptions = {
            update: function (e, ui) {
                $scope.seqnoUpdate('api_seqno_update_type_contract', ui.item.scope().typecontract.id);

            }
        };

        //Global function Update
        $scope.seqnoUpdate = function (route, id) {
            var edit = $resource(Routing.generate(route, {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });
            edit.update(null, $scope.data.typecontracts,
                    function (data) {
                    },
                    function (error) {
                        alert('NOT updated');
                    });

        };

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }


        $scope.init = function ()
        {
            var TypeContractsList = $resource(Routing.generate('adminTypeContracts_list'));

            $scope.data = TypeContractsList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var TypeContractsId = $resource(Routing.generate('adminTypeContracts_get', {id: id}));

            $scope.detail = TypeContractsId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var TypeContractsList = $resource(Routing.generate('adminTypeContracts_save'));
            TypeContractsList.save($scope.new_settingtypecontracts,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingtypecontracts) {
                                $scope.new_settingtypecontracts[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                            for (var key in $scope.new_settingtypecontracts) {
                                $scope.new_settingtypecontracts[key] = null;
                            }
                        }
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.getSeqnos = function (data) {
            $scope.seqnos = [];
            if (data)
                for (var i = 1; i < data + 1; i++) {
                    $scope.seqnos.push(i);
                }
            return $scope.seqnos;
        };

        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.typecontracts.length; k++) {
                if (typeof $scope.data.typecontracts[k].id !== "undefined")
                {
                    if ($scope.data.typecontracts[k].id == id && 'TypeContracts' == type)
                    {
                        $scope.edit_settingtypecontracts = angular.copy($scope.data.typecontracts[k]);
                    }
                }
            }
        }
        $scope.update = function (id)
        {

            var TypeContractsUp = $resource(Routing.generate('adminTypeContracts_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            TypeContractsUp.update(null, $scope.edit_settingtypecontracts,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingtypecontracts) {
                            $scope.edit_settingtypecontracts[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var TypeContractsDel = $resource(Routing.generate('adminTypeContracts_delete', {id: id}));
            TypeContractsDel.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

    });

// Conges controller
    app.controller("CongesCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }


        $scope.init = function ()
        {
            var CongesList = $resource(Routing.generate('adminConges_list'));

            $scope.data = CongesList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.TypeCongesList = function ()
        {
            var TypeCongesList = $resource(Routing.generate('adminTypeConges_list'));
            TypeCongesList.get({display: 'full'},
                    function (data) {
                        if (data.typeconges.length > 0)
                            $scope.typeconges = data.typeconges;
                        else
                            $scope.typeconges = null;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var CongesId = $resource(Routing.generate('adminConges_get', {id: id}));

            $scope.detail = CongesId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var CongesSave = $resource(Routing.generate('adminConges_save'));
            CongesSave.save($scope.data,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        if ($('.new-item').is(":checked"))
                        {
                            for (var key in $scope.data) {
                                $scope.data[key] = "";
                            }
                            return;
                        }
                        window.location = Routing.generate('adminConges_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.update = function (id)
        {

            var CongesUp = $resource(Routing.generate('adminConges_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            CongesUp.update(null, $scope.detail,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        window.location = Routing.generate('adminConges_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var CongesDel = $resource(Routing.generate('adminConges_delete', {id: id}));
            CongesDel.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            return  OnError(data.message);
                        }

                        window.location = Routing.generate('adminConges_index').replace('%23', '#');
                        $scope.init();
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

    });

// PaymentMethod controller
    app.controller("PaymentMethodCtrl", function ($scope, $resource, $filter) {
        
        $scope.sortableOptions = {
            update: function (e, ui) {
                $scope.seqnoUpdate('api_seqno_update_payment', ui.item.scope().paymentmethod.id);

            }
        };

        //Global function Update
        $scope.seqnoUpdate = function (route, id) {
            var edit = $resource(Routing.generate(route, {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });
            edit.update(null, $scope.data.paymentmethods,
                    function (data) {
                    },
                    function (error) {
                        alert('NOT updated');
                    });

        };
        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }


        $scope.init = function ()
        {
            var PaymentMethodList = $resource(Routing.generate('adminPaymentMethod_list'));

            $scope.data = PaymentMethodList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.getSeqnos = function (data) {
            $scope.seqnos = [];
            if (data)
                for (var i = 1; i < data + 1; i++) {
                    $scope.seqnos.push(i);
                }
            return $scope.seqnos;
        };

        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var PaymentMethodId = $resource(Routing.generate('adminPaymentMethod_get', {id: id}));

            $scope.detail = PaymentMethodId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var PaymentMethodSave = $resource(Routing.generate('adminPaymentMethod_save'));
            PaymentMethodSave.save($scope.new_settingpayment,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingpayment) {
                                $scope.new_settingpayment[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };
        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.paymentmethods.length; k++) {
                if (typeof $scope.data.paymentmethods[k].id !== "undefined")
                {
                    if ($scope.data.paymentmethods[k].id == id && 'Payment' == type)
                    {
                        $scope.edit_settingpayment = angular.copy($scope.data.paymentmethods[k]);
                    }
                }
            }
        }
        $scope.update = function (id)
        {

            var PaymentMethodUp = $resource(Routing.generate('adminPaymentMethod_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            PaymentMethodUp.update(null, $scope.edit_settingpayment,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingpayment) {
                            $scope.edit_settingpayment[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var PaymentMethodDel = $resource(Routing.generate('adminPaymentMethod_delete', {id: id}));
            PaymentMethodDel.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

    });

    // Training Type controller
    app.controller("TrainingTypeCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var TrainingTypeList = $resource(Routing.generate('adminTrainingType_list'));

            $scope.data = TrainingTypeList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };


        $scope.setCurrentId = function (id)
        {
            var TrainingTypeId = $resource(Routing.generate('adminTrainingType_get', {id: id}));

            $scope.detail = TrainingTypeId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };


        $scope.save = function ()
        {
            var TrainingTypeSave = $resource(Routing.generate('adminTrainingType_save'));
            TrainingTypeSave.save($scope.new_settingtrainingtype,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingtrainingtype) {
                                $scope.new_settingtrainingtype[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.trainingtypes.length; k++) {
                if (typeof $scope.data.trainingtypes[k].id !== "undefined")
                {
                    if ($scope.data.trainingtypes[k].id == id && 'Training' == type)
                    {
                        $scope.edit_settingtraining = angular.copy($scope.data.trainingtypes[k]);
                    }
                }
            }
        }

        $scope.update = function (id)
        {
            var TrainingTypeUp = $resource(Routing.generate('adminTrainingType_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });
            TrainingTypeUp.update(null, $scope.edit_settingtraining,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingtraining) {
                            $scope.edit_settingtraining[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var TrainingTypeDel = $resource(Routing.generate('adminTrainingType_delete', {id: id}));
            TrainingTypeDel.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };







//
//        $scope.save = function ()
//        {
//            var TrainingTypeSave = $resource(Routing.generate('adminTrainingType_save'));
//            TrainingTypeSave.save($scope.data,
//                    function (data) {
//                        if (data.http_code == '200')
//                        {
//                            OnSuccess(data.message);
//                        } else
//                        {
//                            OnError(data.message);
//                        }
//                        if ($('.new-item').is(":checked"))
//                        {
//                            for (var key in $scope.data) {
//                                $scope.data[key] = "";
//                            }
//                            return;
//                        }
//                        window.location = Routing.generate('adminTrainingType_index').replace('%23', '#');
//                    },
//                    function (error) {
//                        OnError(error.statusText);
//                    });
//        };
//
//        $scope.update = function (id)
//        {
//
//            var TrainingTypeUp = $resource(Routing.generate('adminTrainingType_update', {id: id}), null,
//                    {
//                        'update': {method: 'PUT'}
//                    });
//
//            TrainingTypeUp.update(null, $scope.detail,
//                    function (data) {
//                        if (data.http_code == '200')
//                        {
//                            OnSuccess(data.message);
//                        } else
//                        {
//                            OnError(data.message);
//                        }
//                        window.location = Routing.generate('adminTrainingType_index').replace('%23', '#');
//                    },
//                    function (error) {
//                        OnError(error.statusText);
//                    });
//        };
//
//        $scope.delete = function (id)
//        {
//            var TrainingTypeDel = $resource(Routing.generate('adminTrainingType_delete', {id: id}));
//            TrainingTypeDel.delete(
//                    function (data) {
//                        if (data.http_code == '200')
//                        {
//                            OnSuccess(data.message);
//                        } else
//                        {
//                            return  OnError(data.message);
//                        }
//
//                        window.location = Routing.generate('adminTrainingType_index').replace('%23', '#');
//                        $scope.init();
//                    },
//                    function (error) {
//                        OnError(error.statusText);
//                    });
//        };

    });

// Status controller
    app.controller("StatusCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }


        $scope.init = function ()
        {
            var StatusList = $resource(Routing.generate('adminStatus_list'));

            $scope.data = StatusList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var StatusId = $resource(Routing.generate('adminStatus_get', {id: id}));

            $scope.detail = StatusId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var StatusSave = $resource(Routing.generate('adminStatus_save'));
            StatusSave.save($scope.new_settingstatus,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingstatus) {
                                $scope.new_settingstatus[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };
        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.status.length; k++) {
                if (typeof $scope.data.status[k].id !== "undefined")
                {
                    if ($scope.data.status[k].id == id && 'SettingStatus' == type)
                    {
                        $scope.edit_settingstatus = angular.copy($scope.data.status[k]);
                    }
                }
            }
        }
        $scope.update = function (id)
        {

            var StatusUp = $resource(Routing.generate('adminStatus_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            StatusUp.update(null, $scope.edit_settingstatus,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingstatus) {
                            $scope.edit_settingstatus[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var StatusDel = $resource(Routing.generate('adminStatus_delete', {id: id}));
            StatusDel.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

    });

// Premium Type controller
    app.controller("PremiumTypeCtrl", function ($scope, $resource, $filter) {


        $scope.sortableOptions = {
            update: function (e, ui) {
                console.log( ui.item.scope().premiumtype.id);
                $scope.seqnoUpdate('api_seqno_update_premiumtypes', ui.item.scope().premiumtype.id);
            }
        };

        //Global function Update
        $scope.seqnoUpdate = function (route, id) {
            var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
            edit.update(null, $scope.data.premiumtypes,
                function (data) {
                },
                function (error) {
                    alert('NOT updated');
                });

        };
        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }


        $scope.init = function ()
        {
            var PremiumTypeList = $resource(Routing.generate('adminPremiumType_list'));

            $scope.data = PremiumTypeList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var PremiumTypeId = $resource(Routing.generate('adminPremiumType_get', {id: id}));

            $scope.detail = PremiumTypeId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var PremiumTypeSave = $resource(Routing.generate('adminPremiumType_save'));
            PremiumTypeSave.save($scope.new_settingtypebonus,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingstatus) {
                                $scope.new_settingtypebonus[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        console.log(error.PremiumTypeText);
                    });
        };
        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.premiumtypes.length; k++) {
                if (typeof $scope.data.premiumtypes[k].id !== "undefined")
                {
                    if ($scope.data.premiumtypes[k].id == id && 'SettingTypeBonus' == type)
                    {
                        $scope.edit_settingtypebonus = angular.copy($scope.data.premiumtypes[k]);
                    }
                }
            }
        }
        $scope.update = function (id)
        {

            var PremiumTypeUp = $resource(Routing.generate('adminPremiumType_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            PremiumTypeUp.update(null, $scope.edit_settingtypebonus,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingtypebonus) {
                            $scope.edit_settingtypebonus[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        console.log(error.PremiumTypeText);
                    });
        };

        $scope.delete = function (id)
        {
            var PremiumTypeDel = $resource(Routing.generate('adminPremiumType_delete', {id: id}));
            PremiumTypeDel.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

        $scope.getSeqnos = function (data) {
            $scope.seqnos = [];
            if (data)
                for (var i = 1; i < data + 1; i++) {
                    $scope.seqnos.push(i);
                }
            return $scope.seqnos;
        };


    });

// Sub Premium Type controller
    app.controller("SubPremiumTypeCtrl", function ($scope, $resource, $filter) {

        $scope.sortableOptions = {
            update: function (e, ui) {
                console.log( ui.item.scope().subpremiumtype.id);
                $scope.seqnoUpdate('api_seqno_update_subpremiumtype', ui.item.scope().subpremiumtype.id);
            }
        };

        //Global function Update
        $scope.seqnoUpdate = function (route, id) {
            var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
            edit.update(null, $scope.data.subpremiumtypes,
                function (data) {
                },
                function (error) {
                    alert('NOT updated');
                });

        };
        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }


        $scope.init = function ()
        {
            var SubPremiumTypeList = $resource(Routing.generate('adminSubPremiumType_list'));

            $scope.data = SubPremiumTypeList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.PremiumTypeList = function ()
        {
            var PremiumTypeList = $resource(Routing.generate('adminPremiumType_list'));
            PremiumTypeList.get({display: 'full'},
                    function (data) {
                        console.log(112);
                        if (data.premiumtypes.length > 0)
                            $scope.premiumtypes = data.premiumtypes;
                        else
                            $scope.premiumtypes = null;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.setCurrentId = function (id)
        {
            var SubPremiumTypeId = $resource(Routing.generate('adminSubPremiumType_get', {id: id}));

            $scope.detail = SubPremiumTypeId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var SubPremiumTypeSave = $resource(Routing.generate('adminSubPremiumType_save'));
            SubPremiumTypeSave.save($scope.new_settingbonus,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingbonus) {
                                $scope.new_settingbonus[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        console.log(error.SubPremiumTypeText);
                    });
        };
        $scope.SelectInfos = function (id, type) {

            for (var k = 0; k < $scope.data.subpremiumtypes.length; k++) {
                if (typeof $scope.data.subpremiumtypes[k].id !== "undefined")
                {
                    if ($scope.data.subpremiumtypes[k].id == id && 'SettingBonus' == type)
                    {
                        $scope.edit_settingbonus = angular.copy($scope.data.subpremiumtypes[k]);

                        // $scope.edit_settingbonus.bonus_type = ($scope.data.subpremiumtypes[k].bonus_type.id).toString();
                    }
                }
            }
        }
        $scope.update = function (id)
        {

            var SubPremiumTypeUp = $resource(Routing.generate('adminSubPremiumType_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            SubPremiumTypeUp.update(null, $scope.edit_settingbonus,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingbonus) {
                            $scope.edit_settingbonus[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        console.log(error.SubPremiumTypeText);
                    });
        };

        $scope.delete = function (id)
        {
            var SubPremiumTypeDel = $resource(Routing.generate('adminSubPremiumType_delete', {id: id}));
            SubPremiumTypeDel.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

        $scope.getSeqnos = function (data) {
            $scope.seqnos = [];
            if (data)
                for (var i = 1; i < data + 1; i++) {
                    $scope.seqnos.push(i);
                }
            return $scope.seqnos;
        };


    });

    //Profils Controller
    app.controller("ProfilsCtrl", function ($scope, $resource, filepickerService) {

        $scope.get = function ()
        {

            var CurrentSession = $resource(Routing.generate('api_dashboard_collaborateur'));

            CurrentSession.get(
                    function (data) {
                        $scope.detail = data;
                        console.log($scope.detail);
//                $scope.detail.birth_date = moment($scope.detail.birth_date).format("DD-MM-YYYY");
//              $scope.detail.payment_method =  data.payment_method.id ;
                        // $scope.detail.payment_method = 1;


                    },
                    function (error) {
                        OnError(error.statusText);
                    });

            // $scope.detail. = moment($scope.detail.birth_date).format("DD-MM-YYYY");


        };
//    
        $scope.getCurrentSess = function ()
        {
            var CurrentSession = $resource(Routing.generate('_api_profils_get'));
            CurrentSession.get({display: 'no'},
                    function (data) {
                        $scope.detail = data;
                        ((data.payment_method !== null) ? $scope.detail.payment_method = data.payment_method.id : '');
                        ((data.gender !== null) ? $scope.detail.gender = data.gender.id : '');
                        ((data.birth_date !== null) ? $scope.birth_date = moment($scope.detail.birth_date).format("DD-MM-YYYY") : '');
                        ((data.study_level !== null) ? $scope.detail.study_level = data.study_level.id : '');
                        ((data.study_type !== null) ? $scope.detail.study_type = data.study_type.id : '');
                        ((data.nation_code == null) ? $scope.detail.nation_code = 'FR' : 'FR');
                        ((data.country_code == null) ? $scope.detail.country_code = 'FR' : 'FR');

                    },
                    function (error) {
                        OnError(error.statusText);
                    });
            // $scope.detail. = moment($scope.detail.birth_date).format("DD-MM-YYYY");
        };

        $scope.CountryList = function ()
        {
            var StudyCountriesList = $resource(Routing.generate('apiSettingsCountries_list'));

            /*  StudyCountriesList.get({display:'full'},
             function(data) {
             if(data.CountryList.length > 0)
             $scope.settingsCountries = data.CountryList;
             else
             $scope.settingsCountries = null;
             
             console.log( data.CountryList);
             },
             function(error){
             OnError(error.statusText);
             } );
             */
            $scope.settingsCountries = [
                {code: 'US', name: 'United States'},
                {code: 'GB', name: 'United Kingdom'},
                {code: 'CA', name: 'Canada'},
                {code: 'AF', name: 'Afghanistan'},
                {code: 'AX', name: 'Aland Islands'},
                {code: 'AL', name: 'Albania'},
                {code: 'DZ', name: 'Algeria'},
                {code: 'AS', name: 'American Samoa'},
                {code: 'AD', name: 'Andorra'},
                {code: 'AO', name: 'Angola'},
                {code: 'AI', name: 'Anguilla'},
                {code: 'AQ', name: 'Antarctica'},
                {code: 'AG', name: 'Antigua And Barbuda'},
                {code: 'AR', name: 'Argentina'},
                {code: 'AM', name: 'Armenia'},
                {code: 'AN', name: 'Netherlands Antilles'},
                {code: 'AW', name: 'Aruba'},
                {code: 'AU', name: 'Australia'},
                {code: 'AT', name: 'Austria'},
                {code: 'AZ', name: 'Azerbaijan'},
                {code: 'BS', name: 'Bahamas'},
                {code: 'BH', name: 'Bahrain'},
                {code: 'BD', name: 'Bangladesh'},
                {code: 'BB', name: 'Barbados'},
                {code: 'BY', name: 'Belarus'},
                {code: 'BE', name: 'Belgium'},
                {code: 'BZ', name: 'Belize'},
                {code: 'BJ', name: 'Benin'},
                {code: 'BM', name: 'Bermuda'},
                {code: 'BT', name: 'Bhutan'},
                {code: 'BO', name: 'Bolivia'},
                {code: 'BA', name: 'Bosnia And Herzegovina'},
                {code: 'BW', name: 'Botswana'},
                {code: 'BV', name: 'Bouvet Island'},
                {code: 'BR', name: 'Brazil'},
                {code: 'IO', name: 'British Indian Ocean Territory'},
                {code: 'BN', name: 'Brunei Darussalam'},
                {code: 'BG', name: 'Bulgaria'},
                {code: 'BF', name: 'Burkina Faso'},
                {code: 'BI', name: 'Burundi'},
                {code: 'KH', name: 'Cambodia'},
                {code: 'CM', name: 'Cameroon'},
                {code: 'CA', name: 'Canada'},
                {code: 'CV', name: 'Cape Verde'},
                {code: 'KY', name: 'Cayman Islands'},
                {code: 'CF', name: 'Central African Republic'},
                {code: 'TD', name: 'Chad'},
                {code: 'CL', name: 'Chile'},
                {code: 'CN', name: 'China'},
                {code: 'CX', name: 'Christmas Island'},
                {code: 'CC', name: 'Cocos (Keeling) Islands'},
                {code: 'CO', name: 'Colombia'},
                {code: 'KM', name: 'Comoros'},
                {code: 'CG', name: 'Congo'},
                {code: 'CD', name: 'Congo, Democratic Republic'},
                {code: 'CK', name: 'Cook Islands'},
                {code: 'CR', name: 'Costa Rica'},
                {code: 'CI', name: 'Cote D\'Ivoire'},
                {code: 'HR', name: 'Croatia'},
                {code: 'CU', name: 'Cuba'},
                {code: 'CY', name: 'Cyprus'},
                {code: 'CZ', name: 'Czech Republic'},
                {code: 'DK', name: 'Denmark'},
                {code: 'DJ', name: 'Djibouti'},
                {code: 'DM', name: 'Dominica'},
                {code: 'DO', name: 'Dominican Republic'},
                {code: 'EC', name: 'Ecuador'},
                {code: 'EG', name: 'Egypt'},
                {code: 'SV', name: 'El Salvador'},
                {code: 'GQ', name: 'Equatorial Guinea'},
                {code: 'ER', name: 'Eritrea'},
                {code: 'EE', name: 'Estonia'},
                {code: 'ET', name: 'Ethiopia'},
                {code: 'FK', name: 'Falkland Islands (Malvinas)'},
                {code: 'FO', name: 'Faroe Islands'},
                {code: 'FJ', name: 'Fiji'},
                {code: 'FI', name: 'Finland'},
                {code: 'FR', name: 'France'},
                {code: 'GF', name: 'French Guiana'},
                {code: 'PF', name: 'French Polynesia'},
                {code: 'TF', name: 'French Southern Territories'},
                {code: 'GA', name: 'Gabon'},
                {code: 'GM', name: 'Gambia'},
                {code: 'GE', name: 'Georgia'},
                {code: 'DE', name: 'Germany'},
                {code: 'GH', name: 'Ghana'},
                {code: 'GI', name: 'Gibraltar'},
                {code: 'GR', name: 'Greece'},
                {code: 'GL', name: 'Greenland'},
                {code: 'GD', name: 'Grenada'},
                {code: 'GP', name: 'Guadeloupe'},
                {code: 'GU', name: 'Guam'},
                {code: 'GT', name: 'Guatemala'},
                {code: 'GG', name: 'Guernsey'},
                {code: 'GN', name: 'Guinea'},
                {code: 'GW', name: 'Guinea-Bissau'},
                {code: 'GY', name: 'Guyana'},
                {code: 'HT', name: 'Haiti'},
                {code: 'HM', name: 'Heard Island & Mcdonald Islands'},
                {code: 'VA', name: 'Holy See (Vatican City State)'},
                {code: 'HN', name: 'Honduras'},
                {code: 'HK', name: 'Hong Kong'},
                {code: 'HU', name: 'Hungary'},
                {code: 'IS', name: 'Iceland'},
                {code: 'IN', name: 'India'},
                {code: 'ID', name: 'Indonesia'},
                {code: 'IR', name: 'Iran, Islamic Republic Of'},
                {code: 'IQ', name: 'Iraq'},
                {code: 'IE', name: 'Ireland'},
                {code: 'IM', name: 'Isle Of Man'},
                {code: 'IL', name: 'Israel'},
                {code: 'IT', name: 'Italy'},
                {code: 'JM', name: 'Jamaica'},
                {code: 'JP', name: 'Japan'},
                {code: 'JE', name: 'Jersey'},
                {code: 'JO', name: 'Jordan'},
                {code: 'KZ', name: 'Kazakhstan'},
                {code: 'KE', name: 'Kenya'},
                {code: 'KI', name: 'Kiribati'},
                {code: 'KR', name: 'Korea'},
                {code: 'KW', name: 'Kuwait'},
                {code: 'KG', name: 'Kyrgyzstan'},
                {code: 'LA', name: 'Lao People\'s Democratic Republic'},
                {code: 'LV', name: 'Latvia'},
                {code: 'LB', name: 'Lebanon'},
                {code: 'LS', name: 'Lesotho'},
                {code: 'LR', name: 'Liberia'},
                {code: 'LY', name: 'Libyan Arab Jamahiriya'},
                {code: 'LI', name: 'Liechtenstein'},
                {code: 'LT', name: 'Lithuania'},
                {code: 'LU', name: 'Luxembourg'},
                {code: 'MO', name: 'Macao'},
                {code: 'MK', name: 'Macedonia'},
                {code: 'MG', name: 'Madagascar'},
                {code: 'MW', name: 'Malawi'},
                {code: 'MY', name: 'Malaysia'},
                {code: 'MV', name: 'Maldives'},
                {code: 'ML', name: 'Mali'},
                {code: 'MT', name: 'Malta'},
                {code: 'MH', name: 'Marshall Islands'},
                {code: 'MQ', name: 'Martinique'},
                {code: 'MR', name: 'Mauritania'},
                {code: 'MU', name: 'Mauritius'},
                {code: 'YT', name: 'Mayotte'},
                {code: 'MX', name: 'Mexico'},
                {code: 'FM', name: 'Micronesia, Federated States Of'},
                {code: 'MD', name: 'Moldova'},
                {code: 'MC', name: 'Monaco'},
                {code: 'MN', name: 'Mongolia'},
                {code: 'ME', name: 'Montenegro'},
                {code: 'MS', name: 'Montserrat'},
                {code: 'MA', name: 'Morocco'},
                {code: 'MZ', name: 'Mozambique'},
                {code: 'MM', name: 'Myanmar'},
                {code: 'NA', name: 'Namibia'},
                {code: 'NR', name: 'Nauru'},
                {code: 'NP', name: 'Nepal'},
                {code: 'NL', name: 'Netherlands'},
                {code: 'AN', name: 'Netherlands Antilles'},
                {code: 'NC', name: 'New Caledonia'},
                {code: 'NZ', name: 'New Zealand'},
                {code: 'NI', name: 'Nicaragua'},
                {code: 'NE', name: 'Niger'},
                {code: 'NG', name: 'Nigeria'},
                {code: 'NU', name: 'Niue'},
                {code: 'NF', name: 'Norfolk Island'},
                {code: 'MP', name: 'Northern Mariana Islands'},
                {code: 'NO', name: 'Norway'},
                {code: 'OM', name: 'Oman'},
                {code: 'PK', name: 'Pakistan'},
                {code: 'PW', name: 'Palau'},
                {code: 'PS', name: 'Palestinian Territory, Occupied'},
                {code: 'PA', name: 'Panama'},
                {code: 'PG', name: 'Papua New Guinea'},
                {code: 'PY', name: 'Paraguay'},
                {code: 'PE', name: 'Peru'},
                {code: 'PH', name: 'Philippines'},
                {code: 'PN', name: 'Pitcairn'},
                {code: 'PL', name: 'Poland'},
                {code: 'PT', name: 'Portugal'},
                {code: 'PR', name: 'Puerto Rico'},
                {code: 'QA', name: 'Qatar'},
                {code: 'RE', name: 'Reunion'},
                {code: 'RO', name: 'Romania'},
                {code: 'RU', name: 'Russian Federation'},
                {code: 'RW', name: 'Rwanda'},
                {code: 'BL', name: 'Saint Barthelemy'},
                {code: 'SH', name: 'Saint Helena'},
                {code: 'KN', name: 'Saint Kitts And Nevis'},
                {code: 'LC', name: 'Saint Lucia'},
                {code: 'MF', name: 'Saint Martin'},
                {code: 'PM', name: 'Saint Pierre And Miquelon'},
                {code: 'VC', name: 'Saint Vincent And Grenadines'},
                {code: 'WS', name: 'Samoa'},
                {code: 'SM', name: 'San Marino'},
                {code: 'ST', name: 'Sao Tome And Principe'},
                {code: 'SA', name: 'Saudi Arabia'},
                {code: 'SN', name: 'Senegal'},
                {code: 'RS', name: 'Serbia'},
                {code: 'SC', name: 'Seychelles'},
                {code: 'SL', name: 'Sierra Leone'},
                {code: 'SG', name: 'Singapore'},
                {code: 'SK', name: 'Slovakia'},
                {code: 'SI', name: 'Slovenia'},
                {code: 'SB', name: 'Solomon Islands'},
                {code: 'SO', name: 'Somalia'},
                {code: 'ZA', name: 'South Africa'},
                {code: 'GS', name: 'South Georgia And Sandwich Isl.'},
                {code: 'ES', name: 'Spain'},
                {code: 'LK', name: 'Sri Lanka'},
                {code: 'SD', name: 'Sudan'},
                {code: 'SR', name: 'Suriname'},
                {code: 'SJ', name: 'Svalbard And Jan Mayen'},
                {code: 'SZ', name: 'Swaziland'},
                {code: 'SE', name: 'Sweden'},
                {code: 'CH', name: 'Switzerland'},
                {code: 'SY', name: 'Syrian Arab Republic'},
                {code: 'TW', name: 'Taiwan'},
                {code: 'TJ', name: 'Tajikistan'},
                {code: 'TZ', name: 'Tanzania'},
                {code: 'TH', name: 'Thailand'},
                {code: 'TL', name: 'Timor-Leste'},
                {code: 'TG', name: 'Togo'},
                {code: 'TK', name: 'Tokelau'},
                {code: 'TO', name: 'Tonga'},
                {code: 'TT', name: 'Trinidad And Tobago'},
                {code: 'TN', name: 'Tunisia'},
                {code: 'TR', name: 'Turkey'},
                {code: 'TM', name: 'Turkmenistan'},
                {code: 'TC', name: 'Turks And Caicos Islands'},
                {code: 'TV', name: 'Tuvalu'},
                {code: 'UG', name: 'Uganda'},
                {code: 'UA', name: 'Ukraine'},
                {code: 'AE', name: 'United Arab Emirates'},
                {code: 'GB', name: 'United Kingdom'},
                {code: 'US', name: 'United States'},
                {code: 'UM', name: 'United States Outlying Islands'},
                {code: 'UY', name: 'Uruguay'},
                {code: 'UZ', name: 'Uzbekistan'},
                {code: 'VU', name: 'Vanuatu'},
                {code: 'VE', name: 'Venezuela'},
                {code: 'VN', name: 'Viet Nam'},
                {code: 'VG', name: 'Virgin Islands, British'},
                {code: 'VI', name: 'Virgin Islands, U.S.'},
                {code: 'WF', name: 'Wallis And Futuna'},
                {code: 'EH', name: 'Western Sahara'},
                {code: 'YE', name: 'Yemen'},
                {code: 'ZM', name: 'Zambia'},
                {code: 'ZW', name: 'Zimbabwe'},
            ];
        }

        // Update profils
        $scope.update = function ()
        {
            var ProfilsUp = $resource(Routing.generate('_profils_update'), null,
                    {
                        'update': {method: 'PUT'}
                    });
            ProfilsUp.update($scope.detail,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                    },
                    function (statusText) {
                        OnError(statusText.statusText);
                    });
            window.location = Routing.generate('dashboard_index').replace('%23', '#');
            return;
        };

        $scope.StudyTypeList = function ()
        {
            var SettingsStudyTypeList = $resource(Routing.generate('apiSettingsStudyType_list'));

            SettingsStudyTypeList.get({display: 'full'},
                    function (data) {
                        if (data.settingsStudyTypes.length > 0)
                            $scope.settingsStudyTypes = data.settingsStudyTypes;
                        else
                            $scope.settingsStudyTypes = null;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };
        $scope.StudyLevelList = function ()
        {

            var StudyLevelList = $resource(Routing.generate('apiSettingsStudyLevel_list'));

            StudyLevelList.get({display: 'full'},
                    function (data) {
                        if (data.settingsStudyLevels.length > 0)
                            $scope.settingsStudyLevels = data.settingsStudyLevels;
                        else
                            $scope.settingsStudyLevels = null;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        }

        $scope.PaymentMethodList = function ()
        {
            var PaymentMethodList = $resource(Routing.generate('apiPaymentMethod_list'));

            PaymentMethodList.get({display: 'full'},
                    function (data) {
                        if (data.paymentmethods.length > 0)
                        {
                            $scope.paymentmethods = data.paymentmethods;


                        } else
                            $scope.paymentmethods = null;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        }

        $scope.GenderList = function ()
        {
            var GenderList = $resource(Routing.generate('api_profils_list_gender'));

            GenderList.get({display: 'full'},
                    function (data) {
                        if (data.genders.length > 0)
                        {
                            $scope.genders = data.genders;
                        } else
                            $scope.genders = null;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        }

    });


// Type Transports controller
    app.controller("TypeTransportsCtrl", function ($scope, $resource, $filter) {

        $scope.sortableOptions = {
            update: function (e, ui) {
                // console.log(ui.item.scope());
                $scope.seqnoUpdate('api_seqno_update_apiTypeTransportst', ui.item.scope().typetransport.id);

            }
        };

        //Global function Update
        $scope.seqnoUpdate = function (route, id) {
            var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
            edit.update(null, $scope.data.typetransports,
                function (data) {
                },
                function (error) {
                    alert('NOT updated');
                });

        };

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }


        $scope.init = function ()
        {
            var TypeTransportsList = $resource(Routing.generate('adminTypeTransports_list'));

            $scope.data = TypeTransportsList.get(
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var TypeTransportsId = $resource(Routing.generate('adminTypeTransports_get', {id: id}));

            $scope.detail = TypeTransportsId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var TypeTransportsSave = $resource(Routing.generate('adminTypeTransports_save'));
            TypeTransportsSave.save($scope.new_settingtransport,
                    function (data) {
                                            if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingtransport) {
                                $scope.new_settingtransport[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }},
                    function (error) {
                        console.log(error.TypeTransportsText);
                    });
        };
        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.typetransports.length; k++) {
                if (typeof $scope.data.typetransports[k].id !== "undefined")
                {
                    if ($scope.data.typetransports[k].id == id && 'Transport' == type)
                    {
                        $scope.edit_settingtransport = angular.copy($scope.data.typetransports[k]);
                    }
                }
            }
        }
        $scope.update = function (id)
        {

            var TypeTransportsUp = $resource(Routing.generate('adminTypeTransports_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            TypeTransportsUp.update(null, $scope.edit_settingtransport,
                    function (data) {
                                                if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingtransport) {
                            $scope.edit_settingtransport[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;},
                    function (error) {
                        console.log(error.TypeTransportsText);
                    });
        };

        $scope.delete = function (id)
        {
            var TypeTransportsDel = $resource(Routing.generate('adminTypeTransports_delete', {id: id}));
            TypeTransportsDel.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

    });

// Type Expense controller
    app.controller("TypeExpensesCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }


        $scope.init = function ()
        {
            var TypeExpensesList = $resource(Routing.generate('admin_hrm_settings_exp_other_list'));

            $scope.data = TypeExpensesList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var TypeExpensesId = $resource(Routing.generate('admin_hrm_settings_exp_other_get', {id: id}));

            $scope.detail = TypeExpensesId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var TypeExpensesSave = $resource(Routing.generate('admin_hrm_settings_exp_other_save'));
            TypeExpensesSave.save($scope.data,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        if ($('.new-item').is(":checked"))
                        {
                            for (var key in $scope.data) {
                                $scope.data[key] = "";
                            }
                            return;
                        }
                        window.location = Routing.generate('admin_hrm_settings_exp_other_index').replace('%23', '#');
                    },
                    function (error) {
                        console.log(error.TypeExpensesText);
                    });
        };

        $scope.update = function (id)
        {

            var TypeExpensesUp = $resource(Routing.generate('admin_hrm_settings_exp_other_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            TypeExpensesUp.update(null, $scope.detail,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        window.location = Routing.generate('admin_hrm_settings_exp_other_index').replace('%23', '#');
                    },
                    function (error) {
                        console.log(error.TypeExpensesText);
                    });
        };

        $scope.delete = function (id)
        {
            var TypeExpensesDel = $resource(Routing.generate('admin_hrm_settings_exp_other_delete', {id: id}));
            TypeExpensesDel.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            return  OnError(data.message);
                        }

                        window.location = Routing.generate('admin_hrm_settings_exp_other_index').replace('%23', '#');
                        $scope.init();
                    },
                    function (error) {
                        console.log(error.TypeExpensesText);
                    });
        };

    });

// Absences Type controller
    app.controller("SettingsAbsencesCtrl", function ($scope, $resource, $filter) {

        $scope.sortableOptions = {
            update: function (e, ui) {
                console.log(ui.item.scope());
                $scope.seqnoUpdate('api_seqno_update_type_absence', ui.item.scope().settingsabsence.id);
            }
        };

        //Global function Update
        $scope.seqnoUpdate = function (route, id) {
            var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
            edit.update(null, $scope.data.settingsabsences,
                function (data) {
                },
                function (error) {
                    alert('NOT updated');
                });

        };

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var AbsencesTypeList = $resource(Routing.generate('adminSettingsAbsences_list'));

            $scope.data = AbsencesTypeList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };


        $scope.setCurrentId = function (id)
        {
            var AbsencesTypeId = $resource(Routing.generate('adminSettingsAbsences_get', {id: id}));

            $scope.detail = AbsencesTypeId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.getSeqnos = function (data) {
            $scope.seqnos = [];
            if (data)
                for (var i = 1; i < data + 1; i++) {
                    $scope.seqnos.push(i);
                }
            return $scope.seqnos;
        };

        $scope.save = function ()
        {
            var AbsencesTypeList = $resource(Routing.generate('adminSettingsAbsences_save'));
            AbsencesTypeList.save($scope.new_settingabsence,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingabsence) {
                                $scope.new_settingabsence[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.settingsabsences.length; k++) {
                if (typeof $scope.data.settingsabsences[k].id !== "undefined")
                {
                    if ($scope.data.settingsabsences[k].id == id && 'Absence' == type)
                    {
                        $scope.edit_settingabsence = angular.copy($scope.data.settingsabsences[k]);
                    }
                }
            }
        }

        $scope.update = function (id)
        {
            var AbsencesTypeUp = $resource(Routing.generate('adminSettingsAbsences_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });
            AbsencesTypeUp.update(null, $scope.edit_settingabsence,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingabsence) {
                            $scope.edit_settingabsence[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var AbsencesTypeList = $resource(Routing.generate('adminSettingsAbsences_delete', {id: id}));
            AbsencesTypeList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };
    });

// Accommodation controller
    app.controller("SettingsExpAccommodationCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var SettingsExpAccommodationList = $resource(Routing.generate('admin_hrm_settings_exp_accommodation_list'));

            $scope.data = SettingsExpAccommodationList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var SettingsExpAccommodationId = $resource(Routing.generate('adminSettingsExpAccommodation_get', {id: id}));

            $scope.detail = SettingsExpAccommodationId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var SettingsExpAccommodationList = $resource(Routing.generate('adminSettingsExpAccommodation_save'));
            SettingsExpAccommodationList.save($scope.data,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        if ($('.new-item').is(":checked"))
                        {
                            for (var key in $scope.data) {
                                $scope.data[key] = ""
                            }
                            return;
                        }
                        window.location = Routing.generate('adminSettingsExpAccommodation_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.update = function (id)
        {

            var SettingsExpAccommodationUp = $resource(Routing.generate('admin_hrm_settings_exp_accommodation_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            SettingsExpAccommodationUp.update(null, $scope.detail,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        window.location = Routing.generate('adminSettingsExpAccommodation_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var SettingsExpAccommodationList = $resource(Routing.generate('adminSettingsExpAccommodation_delete', {id: id}));
            SettingsExpAccommodationList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            return  OnError(data.message);
                        }

                        window.location = Routing.generate('adminSettingsExpAccommodation_index').replace('%23', '#');
                        $scope.init();
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

    });

    // IK Controller
    app.controller("SettingsExpCoeffCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var SettingsExpCoeffList = $resource(Routing.generate('adminSettingsExpCoeff_list'));

            $scope.data = SettingsExpCoeffList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var SettingsExpCoeffId = $resource(Routing.generate('adminSettingsExpCoeff_get', {id: id}));

            $scope.detail = SettingsExpCoeffId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var SettingsExpCoeffList = $resource(Routing.generate('adminSettingsExpCoeff_save'));
            SettingsExpCoeffList.save($scope.data,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        if ($('.new-item').is(":checked"))
                        {
                            for (var key in $scope.data) {
                                $scope.data[key] = ""
                            }
                            return;
                        }
                        window.location = Routing.generate('adminSettingsExpCoeff_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.update = function (id)
        {

            var SettingsExpCoeffUp = $resource(Routing.generate('adminSettingsExpCoeff_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            SettingsExpCoeffUp.update(null, $scope.detail,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        window.location = Routing.generate('adminSettingsExpCoeff_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var SettingsExpCoeffList = $resource(Routing.generate('adminSettingsExpCoeff_delete', {id: id}));
            SettingsExpCoeffList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            return  OnError(data.message);
                        }

                        window.location = Routing.generate('adminSettingsExpCoeff_index').replace('%23', '#');
                        $scope.init();
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

    });

    // TAX Controller
    app.controller("SettingsTaxCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var SettingsTaxList = $resource(Routing.generate('admin_hrm_settings_tax_list'));

            $scope.data = SettingsTaxList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var SettingsTaxId = $resource(Routing.generate('admin_hrm_settings_tax_get', {id: id}));

            $scope.detail = SettingsTaxId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var SettingsTaxList = $resource(Routing.generate('admin_hrm_settings_tax_save'));
            SettingsTaxList.save($scope.new_settingtax,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingtax) {
                                $scope.new_settingtax[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };
        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.hrm_settings_taxs.length; k++) {
                if (typeof $scope.data.hrm_settings_taxs[k].id !== "undefined")
                {
                    if ($scope.data.hrm_settings_taxs[k].id == id && 'Tax' == type)
                    {
                        $scope.edit_settingltax = angular.copy($scope.data.hrm_settings_taxs[k]);
                    }
                }
            }
        }
        $scope.update = function (id)
        {

            var SettingsTaxUp = $resource(Routing.generate('admin_hrm_settings_tax_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            SettingsTaxUp.update(null, $scope.edit_settingltax,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingltax) {
                            $scope.edit_settingltax[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var SettingsTaxList = $resource(Routing.generate('admin_hrm_settings_tax_delete', {id: id}));
            SettingsTaxList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

    });

    // Settings Exp Transport Controller
    app.controller("SettingsExpTransportCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var SettingsExpTransportList = $resource(Routing.generate('admin_hrm_settings_exp_transport_list'));

            $scope.data = SettingsExpTransportList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var SettingsExpTransportId = $resource(Routing.generate('admin_hrm_settings_exp_transport_get', {id: id}));

            $scope.detail = SettingsExpTransportId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var SettingsExpTransportList = $resource(Routing.generate('admin_hrm_settings_exp_transport_save'));
            SettingsExpTransportList.save($scope.data,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        if ($('.new-item').is(":checked"))
                        {
                            for (var key in $scope.data) {
                                $scope.data[key] = ""
                            }
                            return;
                        }
                        window.location = Routing.generate('admin_hrm_settings_exp_transport_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.update = function (id)
        {

            var SettingsExpTransportUp = $resource(Routing.generate('admin_hrm_settings_exp_transport_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            SettingsExpTransportUp.update(null, $scope.detail,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        window.location = Routing.generate('admin_hrm_settings_exp_transport_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var SettingsExpTransportList = $resource(Routing.generate('admin_hrm_settings_exp_transport_delete', {id: id}));
            SettingsExpTransportList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            return  OnError(data.message);
                        }

                        window.location = Routing.generate('admin_hrm_settings_exp_transport_index').replace('%23', '#');
                        $scope.init();
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

    });

    // Settings Exp Dinning Controller
    app.controller("SettingsExpDinningCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var SettingsExpDinningList = $resource(Routing.generate('admin_hrm_settings_exp_dinning_list'));

            $scope.data = SettingsExpDinningList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var SettingsExpDinningId = $resource(Routing.generate('admin_hrm_settings_exp_dinning_get', {id: id}));

            $scope.detail = SettingsExpDinningId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var SettingsExpDinningList = $resource(Routing.generate('admin_hrm_settings_exp_dinning_save'));
            SettingsExpDinningList.save($scope.data,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        if ($('.new-item').is(":checked"))
                        {
                            for (var key in $scope.data) {
                                $scope.data[key] = ""
                            }
                            return;
                        }
                        window.location = Routing.generate('admin_hrm_settings_exp_dinning_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.update = function (id)
        {

            var SettingsExpDinningUp = $resource(Routing.generate('admin_hrm_settings_exp_dinning_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            SettingsExpDinningUp.update(null, $scope.detail,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        window.location = Routing.generate('admin_hrm_settings_exp_dinning_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var SettingsExpDinningList = $resource(Routing.generate('admin_hrm_settings_exp_dinning_delete', {id: id}));
            SettingsExpDinningList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            return  OnError(data.message);
                        }

                        window.location = Routing.generate('admin_hrm_settings_exp_dinning_index').replace('%23', '#');
                        $scope.init();
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

    });

    // Settings Gift context Controller
    app.controller("SettingsGiftContextCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var SettingsGiftContextList = $resource(Routing.generate('adminSettingsGiftContext_list'));

            $scope.data = SettingsGiftContextList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var SettingsGiftContextId = $resource(Routing.generate('adminSettingsGiftContext_get', {id: id}));

            $scope.detail = SettingsGiftContextId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var SettingsGiftContextList = $resource(Routing.generate('adminSettingsGiftContext_save'));
            SettingsGiftContextList.save($scope.data,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        if ($('.new-item').is(":checked"))
                        {
                            for (var key in $scope.data) {
                                $scope.data[key] = ""
                            }
                            return;
                        }
                        window.location = Routing.generate('adminSettingsGiftContext_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.update = function (id)
        {

            var SettingsGiftContextUp = $resource(Routing.generate('adminSettingsGiftContext_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            SettingsGiftContextUp.update(null, $scope.detail,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        window.location = Routing.generate('adminSettingsGiftContext_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var SettingsGiftContextList = $resource(Routing.generate('adminSettingsGiftContext_delete', {id: id}));
            SettingsGiftContextList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            return  OnError(data.message);
                        }

                        window.location = Routing.generate('adminSettingsGiftContext_index').replace('%23', '#');
                        $scope.init();
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

    });

// Settings Adv Vihecle Controller
    app.controller("SettingsAdvVehicleCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var SettingsAdvVehicleList = $resource(Routing.generate('adminSettingsAdvVehicle_list'));

            $scope.data = SettingsAdvVehicleList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var SettingsAdvVehicleId = $resource(Routing.generate('adminSettingsAdvVehicle_get', {id: id}));

            $scope.detail = SettingsAdvVehicleId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var SettingsAdvVehicleList = $resource(Routing.generate('adminSettingsAdvVehicle_save'));
            SettingsAdvVehicleList.save($scope.data,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        if ($('.new-item').is(":checked"))
                        {
                            for (var key in $scope.data) {
                                $scope.data[key] = ""
                            }
                            return;
                        }
                        window.location = Routing.generate('adminSettingsAdvVehicle_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.update = function (id)
        {

            var SettingsAdvVehicleUp = $resource(Routing.generate('adminSettingsAdvVehicle_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            SettingsAdvVehicleUp.update(null, $scope.detail,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        window.location = Routing.generate('adminSettingsAdvVehicle_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var SettingsAdvVehicleList = $resource(Routing.generate('adminSettingsAdvVehicle_delete', {id: id}));
            SettingsAdvVehicleList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            return  OnError(data.message);
                        }

                        window.location = Routing.generate('adminSettingsAdvVehicle_index').replace('%23', '#');
                        $scope.init();
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

    });

    // Settings study level Controller
    app.controller("SettingsStudyLevelCtrl", function ($scope, $resource, $filter) {

        $scope.sortableOptions = {
            update: function (e, ui) {
                $scope.seqnoUpdate('api_seqno_update_study_level', ui.item.scope().settingsStudyLevel.id);

            }
        };

        //Global function Update
        $scope.seqnoUpdate = function (route, id) {
            var edit = $resource(Routing.generate(route, {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });
            edit.update(null, $scope.data.settingsStudyLevels,
                    function (data) {
                    },
                    function (error) {
                        alert('NOT updated');
                    });

        };
        
        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var SettingsStudyLevelList = $resource(Routing.generate('adminSettingsStudyLevel_list'));

            $scope.data = SettingsStudyLevelList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var SettingsStudyLevelId = $resource(Routing.generate('adminSettingsStudyLevel_get', {id: id}));

            $scope.detail = SettingsStudyLevelId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var SettingsStudyLevelList = $resource(Routing.generate('adminSettingsStudyLevel_save'));
            SettingsStudyLevelList.save($scope.new_settingstudylevel,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingstudylevel) {
                                $scope.new_settingstudylevel[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.settingsStudyLevels.length; k++) {
                if (typeof $scope.data.settingsStudyLevels[k].id !== "undefined")
                {
                    if ($scope.data.settingsStudyLevels[k].id == id && 'StudyLevel' == type)
                    {
                        $scope.edit_settingstudylevel = angular.copy($scope.data.settingsStudyLevels[k]);
                    }
                }
            }
        }
        $scope.update = function (id)
        {

            var SettingsStudyLevelUp = $resource(Routing.generate('adminSettingsStudyLevel_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            SettingsStudyLevelUp.update(null, $scope.edit_settingstudylevel,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingstudylevel) {
                            $scope.edit_settingstudylevel[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var SettingsStudyLevelList = $resource(Routing.generate('adminSettingsStudyLevel_delete', {id: id}));
            SettingsStudyLevelList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

    });

    // Settings study type Controller
    app.controller("SettingsStudyTypeCtrl", function ($scope, $resource, $filter) {
        $scope.sortableOptions = {
            update: function (e, ui) {
//                console.log(ui.item.scope());
                $scope.seqnoUpdate('api_seqno_update_study_type', ui.item.scope().settingsStudyType.id);

            }
        };

        //Global function Update
        $scope.seqnoUpdate = function (route, id) {
            var edit = $resource(Routing.generate(route, {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });
            edit.update(null, $scope.data.settingsStudyTypes,
                    function (data) {
                    },
                    function (error) {
//                        alert('NOT updated');
                    });

        };
        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var SettingsStudyTypeList = $resource(Routing.generate('adminSettingsStudyType_list'));

            $scope.data = SettingsStudyTypeList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var SettingsStudyTypeId = $resource(Routing.generate('adminSettingsStudyType_get', {id: id}));

            $scope.detail = SettingsStudyTypeId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var SettingsStudyTypeList = $resource(Routing.generate('adminSettingsStudyType_save'));
            SettingsStudyTypeList.save($scope.new_settingstudytype,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingstudytype) {
                                $scope.new_settingstudytype[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };
        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.settingsStudyTypes.length; k++) {
                if (typeof $scope.data.settingsStudyTypes[k].id !== "undefined")
                {
                    if ($scope.data.settingsStudyTypes[k].id == id && 'StudyType' == type)
                    {
                        $scope.edit_settingstudytype = angular.copy($scope.data.settingsStudyTypes[k]);
                    }
                }
            }
        }
        $scope.update = function (id)
        {

            var SettingsStudyTypeUp = $resource(Routing.generate('adminSettingsStudyType_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            SettingsStudyTypeUp.update(null, $scope.edit_settingstudytype,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingstudytype) {
                            $scope.edit_settingstudytype[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var SettingsStudyTypeList = $resource(Routing.generate('adminSettingsStudyType_delete', {id: id}));
            SettingsStudyTypeList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

    });

    // Settings currency Controller
    app.controller("SettingsCurrencyCtrl", function ($scope, $resource, $filter) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var SettingsCurrencyList = $resource(Routing.generate('adminSettingsCurrency_list'));

            $scope.data = SettingsCurrencyList.get({display: 'full'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var SettingsCurrencyId = $resource(Routing.generate('adminSettingsCurrency_get', {id: id}));

            $scope.detail = SettingsCurrencyId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var SettingsCurrencyList = $resource(Routing.generate('adminSettingsCurrency_save'));
            SettingsCurrencyList.save($scope.data,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        if ($('.new-item').is(":checked"))
                        {
                            for (var key in $scope.data) {
                                $scope.data[key] = ""
                            }
                            return;
                        }
                        window.location = Routing.generate('adminSettingsCurrency_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.update = function (id)
        {
            var SettingsCurrencyUp = $resource(Routing.generate('adminSettingsCurrency_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });
            SettingsCurrencyUp.update(null, $scope.detail,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                        window.location = Routing.generate('adminSettingsCurrency_index').replace('%23', '#');
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var SettingsCurrencyList = $resource(Routing.generate('adminSettingsCurrency_delete', {id: id}));
            SettingsCurrencyList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            return  OnError(data.message);
                        }

                        window.location = Routing.generate('adminSettingsCurrency_index').replace('%23', '#');
                        $scope.init();
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

    });

    // Settings emp status Controller
    app.controller("SettingsEmpStatusCtrl", function ($scope, $resource, $filter) {
        $scope.sortableOptions = {
            update: function (e, ui) {
                $scope.seqnoUpdate('api_seqno_update_emp_status', ui.item.scope().settingsEmpStatu.id);

            }
        };

        //Global function Update
        $scope.seqnoUpdate = function (route, id) {
            var edit = $resource(Routing.generate(route, {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });
            edit.update(null, $scope.data.settingsEmpStatus,
                    function (data) {
                    },
                    function (error) {
                        alert('NOT updated');
                    });

        };
        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var SettingsEmpStatusList = $resource(Routing.generate('adminSettingsEmpStatus_list'));

            $scope.data = SettingsEmpStatusList.get({display: 'full'},
                    function (data) {
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var SettingsEmpStatusId = $resource(Routing.generate('adminSettingsEmpStatus_get', {id: id}));

            $scope.detail = SettingsEmpStatusId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.getSeqnos = function (data) {
            $scope.seqnos = [];
            if (data)
                for (var i = 1; i < data + 1; i++) {
                    $scope.seqnos.push(i);
                }
            return $scope.seqnos;
        };

        $scope.save = function ()
        {
            var SettingsEmpStatusList = $resource(Routing.generate('adminSettingsEmpStatus_save'));
            SettingsEmpStatusList.save($scope.new_settingempstatus,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingempstatus) {
                                $scope.new_settingempstatus[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };
        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.settingsEmpStatus.length; k++) {
                if (typeof $scope.data.settingsEmpStatus[k].id !== "undefined")
                {
                    if ($scope.data.settingsEmpStatus[k].id == id && 'SettingEmployeeStatus' == type)
                    {
                        $scope.edit_settingempstatus = angular.copy($scope.data.settingsEmpStatus[k]);
                    }
                }
            }
        }
        $scope.update = function (id)
        {
            var SettingsEmpStatusUp = $resource(Routing.generate('adminSettingsEmpStatus_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });
            SettingsEmpStatusUp.update(null, $scope.edit_settingempstatus,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingempstatus) {
                            $scope.edit_settingempstatus[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var SettingsEmpStatusList = $resource(Routing.generate('adminSettingsEmpStatus_delete', {id: id}));
            SettingsEmpStatusList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

    });

    //EmployeeContract Controller
    app.controller("EmployeeContractCtrl", function ($scope, $resource, filepickerService, $window) {

        $scope.get = function ()
        {
            var CurrentSession = $resource(Routing.generate('_EmployeeContract_get'));
            CurrentSession.get({display: 'no'},
                    function (data) {
                        $scope.detail = data;
                    },
                    function (error) {
                        console.log(false);
                    });
        };
        // Update Employee Contract
        $scope.update = function ()
        {
            var EmployeeContractUp = $resource(Routing.generate('_EmployeeContract_save'), null,
                    {
                        'update': {method: 'PUT'}
                    });
            EmployeeContractUp.update($scope.detail,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            OnSuccess(data.message);
                        } else
                        {
                            OnError(data.message);
                        }
                    },
                    function (statusText) {
                        OnError(statusText.statusText);
                    });


            console.log("_EmployeeContract_save and redirect");
            window.location = Routing.generate('dashboard_index').replace('%23', '#');

        };

        $scope.StatusList = function ()
        {
            var StatusList = $resource(Routing.generate('apiSettingsEmpStatus_list'));

            StatusList.get({display: 'full'},
                    function (data) {
                        if (data.settingsEmpStatus.length > 0)
                            $scope.status = data.settingsEmpStatus;
                        else
                            $scope.status = null;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };


//      $scope.ConventionList = function()
//      {
//          
//           var ConventionList = $resource(Routing.generate('apiSettingsConvention_list'));
//
//            ConventionList.get({display:'full'},
//            function(data) {
//                 if(data.conventions.length > 0)
//                 {
//                     $scope.conventions = data.conventions;
//                   
//                  }
//                else
//                    $scope.conventions = null;
//             },
//             function(error){
//                  OnError(error.statusText);
//             } );
//       
//      }; 



        $scope.ContractTypeList = function ()
        {
            var ContractTypeList = $resource(Routing.generate('apiTypeContracts_list'));

            ContractTypeList.get({display: 'full'},
                    function (data) {
                        if (data.typecontracts.length > 0)
                            $scope.typecontracts = data.typecontracts;
                        else
                            $scope.typecontracts = null;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };
    });


    // hrm_settings_exp_tax_model Controller
    app.controller("hrm_settings_exp_tax_modelCtrl", function ($scope, $resource, $filter, $timeout) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var hrm_settings_exp_tax_modelList = $resource(Routing.generate('admin_hrm_settings_exp_tax_model_list'));

            $scope.modelData = hrm_settings_exp_tax_modelList.get({display: 'full'},
                    function (modelData) {
                        
                        console.log($scope.modelData);
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };
        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var hrm_settings_exp_tax_modelId = $resource(Routing.generate('admin_hrm_settings_exp_tax_model_get', {id: id}));

            $scope.detail = hrm_settings_exp_tax_modelId.get({display: 'no'},
                    function (data) {
                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            delete $scope.data.hrm_settings_taxs;
            if ($(".select-multiple-modal-tva-new").val() === null)
                return;
            var _arr_tax = [];
            $.each($(".select-multiple-modal-tva-new").val(), function (i, v) {
                _arr_tax.push(parseInt(v.match(/\d+/)[0]));
            });
            $scope.new_settingmodeltax.tax = _arr_tax;
            var hrm_settings_exp_tax_modelList = $resource(Routing.generate('admin_hrm_settings_exp_tax_model_save'));
            hrm_settings_exp_tax_modelList.save($scope.new_settingmodeltax,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingmodeltax) {
                                $scope.new_settingmodeltax[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };
        $scope.SelectInfos = function (id, type) {
            $scope.modelTVARate = [];

            for (var k = 0; k < $scope.modelData.hrm_settings_exp_tax_models.length; k++) {
                if (typeof $scope.modelData.hrm_settings_exp_tax_models[k].id !== "undefined")
                {
                    if ($scope.modelData.hrm_settings_exp_tax_models[k].id == id && 'ModelTax' == type)
                    {
                        $scope.edit_settinglmodeltax = angular.copy($scope.modelData.hrm_settings_exp_tax_models[k]);


                        for (i = 0; i < $scope.modelData.hrm_settings_exp_tax_models[k].tax.length; i++) {
                            $scope.modelTVARate.push($scope.modelData.hrm_settings_exp_tax_models[k].tax[i].id);
                        }
                        $scope.edit_settinglmodeltax.tax = angular.copy($scope.modelTVARate);
                    }
                }
            }
            $timeout(function () {
                $('.select-multiple-modal-tva-edit').select2({val: $scope.edit_settinglmodeltax.tax}).trigger('change');
                $scope.$apply();
            });

        };

        $scope.update = function ()
        {
            if ($(".select-multiple-modal-tva-edit").val() === null)
                return;
            var _arr_tax = [];
            $.each($(".select-multiple-modal-tva-edit").val(), function (i, v) {
                _arr_tax.push(parseInt(v.match(/\d+/)[0]));
            });
            $scope.edit_settinglmodeltax.tax = _arr_tax;
            var hrm_settings_exp_tax_modelUp = $resource(Routing.generate('admin_hrm_settings_exp_tax_model_update', {id: $scope.edit_settinglmodeltax.id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            hrm_settings_exp_tax_modelUp.update(null, $scope.edit_settinglmodeltax,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.new_settinglmodeltax) {
                            $scope.new_settinglmodeltax[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var hrm_settings_exp_tax_modelList = $resource(Routing.generate('admin_hrm_settings_exp_tax_model_delete', {id: id}));
            hrm_settings_exp_tax_modelList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

        $scope.SettingsTaxList = function ()
        {
            var SettingsTaxList = $resource(Routing.generate('admin_hrm_settings_tax_list'));

            $scope.data = SettingsTaxList.get({display: 'full'},
                    function (data) {
                        $scope.SettingsTaxList = data.hrm_settings_taxs;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        }

    });


    // Settings menu Controller
    app.controller("SettingsMenuCtrl", function ($scope, $resource, $filter, $compile) {

        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

    $scope.sortableOptions = {
        update: function (e, ui) {
            $scope.seqnoUpdate('api_seqno_update_menu_link', ui.item.scope().link.id);
        }
    };

    //Global function Update
    $scope.seqnoUpdate = function (route, id) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        edit.update(null, $scope.tasks,
            function (data) {
            },
            function (error) {
                alert('NOT updated');
            });

    };
    
    $scope.initMenus = function () {
        var dataResult = $resource(Routing.generate('hrm_settings_menu_all_list'));

       dataResult.get({display: 'no'})
            .$promise.then(
            function (data) { 
                $scope.tasks = data.result;
            },
            function (error) {
            });
            

    };
    
        $scope.init = function ()
        {
            var SettingsMenuList = $resource(Routing.generate('hrm_settings_menu_providers_list'));
            $scope.data = SettingsMenuList.get({display: 'full'},
                    function (data) {
                        
                        $scope.perPage = 10;
                        $scope.maxSize = 5;
                        $scope.setPage = function (pageNo) {
                            $scope.currentPage = pageNo;
                        };                        
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            eval('$scope.menus = {}');
            var SettingsMenuId = $resource(Routing.generate('hrm_settings_menu_provider_get', {provider_id: id}));

            SettingsMenuId.get({display: 'no'})
                    .$promise.then(
                    function (data) {
                         
                         eval('$scope.menus["provider_id"] = '+id);
                            $.each(data.providre.menus, function(i, item){
                                 eval('$scope.menus.' + item.setting_menu.routeLinks+'_'+item.setting_menu.id + ' = true');                             
                            });
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.update = function ()
        {           
            var SettingsMenuList = $resource(Routing.generate('hrm_settings_menu_save'));
            SettingsMenuList.save($scope.menus,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                             
                        } else
                        {
                           return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }                        
                        $('.close').click();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };
  
    });

    //settingsPlannedJourney Controller
    app.controller("journeyTypeCtrl", function ($scope, $resource, filepickerService, $window,$filter) {
        
 $scope.sortableOptions = {
        update: function (e, ui) {
            $scope.seqnoUpdate('api_seqno_update_plannedjourney_type', ui.item.scope().plannedJourney.id);
        }
    };

    $scope.seqnoUpdate = function (route, id) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        edit.update(null, $scope.data.settingsPlannedJourney,
                function (data) {
                },
                function (error) {
                });
    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };

    $scope.init = function ()
    {
        var settingsPlannedJourney = $resource(Routing.generate('api_hrm_settings_plannedjourney_type_list'));
        $scope.data = settingsPlannedJourney.get(
                function (data) {
                    console.log(true);
                    console.log(data);
                },
                function (error) {
                    OnError(error.statusText);
                });
    };

    $scope.SelectInfos = function (id, type) {
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.data.settingsPlannedJourney.length; k++) {
            if (typeof $scope.data.settingsPlannedJourney[k].id !== "undefined") {
                if ($scope.data.settingsPlannedJourney[k].id == id && 'settingsPlannedJourney' == type) {
                    $scope.edit = $scope.data.settingsPlannedJourney[k];
                }
            }
        }
    };

    $scope.save = function (route) {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate("api_hrm_settings_plannedjourney_type_save"));
        url.save($scope.new,
                function (data) {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    if ($('#checkbox').is(":checked")) {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $scope.IsDisabled = false;
                         return;
                    } else {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $('.close').click();
                        $scope.IsDisabled = false;
                    }
                    $scope.init();
                },
                function (error) {

                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;

                });
    };

    $scope.update = function (id)
    {
        var settingsPlannedJourney = $resource(Routing.generate('api_hrm_settings_plannedjourney_type_update', {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        settingsPlannedJourney.update(null, $scope.edit,
                function (data) {
                    if (data.http_code == '200')
                    {
                        $scope.init();
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $('.close').click();
                    for (var key in $scope.edit) {
                        $scope.edit[key] = null;
                    }
                    return;

                    $scope.IsDisabled = false;
                },
                function (error) {
                    console.log(error.CustomerTypeText);
                });
    };

    $scope.delete = function (id)
    {
        var TypeTransportsDel = $resource(Routing.generate('api_hrm_settings_plannedjourney_type_delete', {id: id}));
        TypeTransportsDel.delete(
                function (data) {
                    if (data.http_code == '200')
                    {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $scope.init();
                },
                function (error) {
                    return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };
  });
        
       //settingsPlannedJourney Controller
    app.controller("settingsJobCtrl", function ($scope, $resource, filepickerService, $window,$filter) {
        
 $scope.sortableOptions = {
        update: function (e, ui) {
            $scope.seqnoUpdate('api_seqno_update_job_type', ui.item.scope().settingJob.id);
        }
    };

    $scope.seqnoUpdate = function (route, id) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        edit.update(null, $scope.data.settingsJob,
                function (data) {
                },
                function (error) {
                });
    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };

    $scope.init = function ()
    {
        var settingsJob = $resource(Routing.generate('api_hrm_settings_job_list'));
        $scope.data = settingsJob.get(
                function (data) {
                    console.log(true);
                    console.log(data);
                },
                function (error) {
                    OnError(error.statusText);
                });
                console.log($scope.data);
    };

    $scope.SelectInfos = function (id, type) {
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.data.settingsJob.length; k++) {
            if (typeof $scope.data.settingsJob[k].id !== "undefined") {
                if ($scope.data.settingsJob[k].id == id && 'settingsJob' == type) {
                    $scope.edit = $scope.data.settingsJob[k];
                }
            }
        }
    };

    $scope.save = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate("api_hrm_settings_job_save"));
        url.save($scope.new,
                function (data) {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    if ($('#checkbox').is(":checked")) {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $scope.IsDisabled = false;
                         return;
                    } else {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $('.close').click();
                        $scope.IsDisabled = false;
                    }
                    $scope.init();
                },
                function (error) {

                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;

                });
    };

    $scope.update = function (id)
    {
        var settingsJob = $resource(Routing.generate('api_hrm_settings_job_update', {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        settingsJob.update(null, $scope.edit,
                function (data) {
                    if (data.http_code == '200')
                    {
                        $scope.init();
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $('.close').click();
                    for (var key in $scope.edit) {
                        $scope.edit[key] = null;
                    }
                    return;

                    $scope.IsDisabled = false;
                },
                function (error) {
                    console.log(error.CustomerTypeText);
                });
    };

    $scope.delete = function (id)
    {
        var settingsJob = $resource(Routing.generate('api_hrm_settings_job_delete', {id: id}));
        settingsJob.delete(
                function (data) {
                    if (data.http_code == '200')
                    {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $scope.init();
                },
                function (error) {
                    return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };
  });
     
    
    
         //Periodi city Controller
app.controller("settingsPeriodicityCtrl", function ($scope, $resource, filepickerService, $window,$filter) {
    
 $scope.sortableOptions = {
        update: function (e, ui) {
            $scope.seqnoUpdate('api_seqno_update_periodicity_type', ui.item.scope().settingPeriodicity.id);
        }
    };

    $scope.seqnoUpdate = function (route, id) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        edit.update(null, $scope.data.settingsPeriodicity,
                function (data) {
                },
                function (error) {
                });
    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };

    $scope.init = function ()
    {
        var settingsPeriodicity = $resource(Routing.generate('api_hrm_settings_periodicity_list'));
        $scope.data = settingsPeriodicity.get(
                function (data) {
                    console.log(true);
                    console.log(data);
                },
                function (error) {
                    OnError(error.statusText);
                });
                console.log($scope.data);
    };

    $scope.SelectInfos = function (id, type) {
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.data.settingsPeriodicity.length; k++) {
            if (typeof $scope.data.settingsPeriodicity[k].id !== "undefined") {
                if ($scope.data.settingsPeriodicity[k].id == id && 'settingsPeriodicity' == type) {
                    $scope.edit = $scope.data.settingsPeriodicity[k];
                }
            }
        }
    };

    $scope.save = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate("api_hrm_settings_periodicity_save"));
        url.save($scope.new,
                function (data) {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    if ($('#checkbox').is(":checked")) {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $scope.IsDisabled = false;
                         return;
                    } else {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $('.close').click();
                        $scope.IsDisabled = false;
                    }
                    $scope.init();
                },
                function (error) {

                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;

                });
    };

    $scope.update = function (id)
    {
        var settingsJob = $resource(Routing.generate('api_hrm_settings_periodicity_update', {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        settingsJob.update(null, $scope.edit,
                function (data) {
                    if (data.http_code == '200')
                    {
                        $scope.init();
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $('.close').click();
                    for (var key in $scope.edit) {
                        $scope.edit[key] = null;
                    }
                    return;

                    $scope.IsDisabled = false;
                },
                function (error) {
                    console.log(error.CustomerTypeText);
                });
    };

    $scope.delete = function (id)
    {
        var settingsJob = $resource(Routing.generate('api_hrm_settings_periodicity_delete', {id: id}));
        settingsJob.delete(
                function (data) {
                    if (data.http_code == '200')
                    {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $scope.init();
                },
                function (error) {
                    return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };
  });
  
           //settings Technology  Controller
app.controller("settingsTechnologyCtrl", function ($scope, $resource, filepickerService, $window,$filter) {
    
 $scope.sortableOptions = {
        update: function (e, ui) {
            $scope.seqnoUpdate('api_seqno_update_technology', ui.item.scope().settingTechnology.id);
        }
    };

    $scope.seqnoUpdate = function (route, id) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        edit.update(null, $scope.data.settingsTechnology,
                function (data) {
                },
                function (error) {
                });
    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };

    $scope.init = function ()
    {
        var settingsTechnology = $resource(Routing.generate('api_hrm_settings_technology_list'));
        $scope.data = settingsTechnology.get(
                function (data) {
                    console.log(true);
                    console.log(data);
                },
                function (error) {
                    OnError(error.statusText);
                });
                console.log($scope.data);
    };

    $scope.SelectInfos = function (id, type) {
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.data.settingsTechnology.length; k++) {
            if (typeof $scope.data.settingsTechnology[k].id !== "undefined") {
                if ($scope.data.settingsTechnology[k].id == id && 'settingTechnology' == type) {
                    $scope.edit = $scope.data.settingsTechnology[k];
                }
            }
        }
    };

    $scope.save = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate("api_hrm_settings_technology_save"));
        url.save($scope.new,
                function (data) {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    if ($('#checkbox').is(":checked")) {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $scope.IsDisabled = false;
                         return;
                    } else {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $('.close').click();
                        $scope.IsDisabled = false;
                    }
                    $scope.init();
                },
                function (error) {

                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;

                });
    };

    $scope.update = function (id)
    {
        var settingsTechnology = $resource(Routing.generate('api_hrm_settings_technology_update', {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        settingsTechnology.update(null, $scope.edit,
                function (data) {
                    if (data.http_code == '200')
                    {
                        $scope.init();
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $('.close').click();
                    for (var key in $scope.edit) {
                        $scope.edit[key] = null;
                    }
                    return;

                    $scope.IsDisabled = false;
                },
                function (error) {
                    console.log(error.CustomerTypeText);
                });
    };

    $scope.delete = function (id)
    {
        var settingTechnology = $resource(Routing.generate('api_hrm_settings_technology_delete', {id: id}));
        settingTechnology.delete(
                function (data) {
                    if (data.http_code == '200')
                    {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $scope.init();
                },
                function (error) {
                    return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };
  });

  
   //settings Technology  Controller
app.controller("settingsTechnologyLevelCtrl", function ($scope, $resource, filepickerService, $window,$filter) {

    $scope.sortableOptions = {
        update: function (e, ui) {
            $scope.seqnoUpdate('api_seqno_update_technology_level', ui.item.scope().settingTechnologyLevel.id);
        }
    };

    $scope.seqnoUpdate = function (route, id) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        edit.update(null, $scope.data.settingsTechnologyLevel,
                function (data) {
                },
                function (error) {
                });
    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };

    $scope.init = function ()
    {
        var settingsTechnologyLevel = $resource(Routing.generate('api_hrm_settings_technology_level_list'));
        $scope.data = settingsTechnologyLevel.get(
                function (data) {
                    console.log(true);
                    console.log(data);
                },
                function (error) {
                    OnError(error.statusText);
                });
                console.log($scope.data);
    };

    $scope.SelectInfos = function (id, type) {
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.data.settingsTechnologyLevel.length; k++) {
            if (typeof $scope.data.settingsTechnologyLevel[k].id !== "undefined") {
                if ($scope.data.settingsTechnologyLevel[k].id == id && 'settingTechnologyLevel' == type) {
                    $scope.edit = $scope.data.settingsTechnologyLevel[k];
                }
            }
        }
    };

    $scope.save = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate("api_hrm_settings_technology_level_save"));
        url.save($scope.new,
                function (data) {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    if ($('#checkbox').is(":checked")) {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $scope.IsDisabled = false;
                         return;
                    } else {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $('.close').click();
                        $scope.IsDisabled = false;
                    }
                    $scope.init();
                },
                function (error) {

                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;

                });
    };

    $scope.update = function (id)
    {
        var settingsTechnologyLevel = $resource(Routing.generate('api_hrm_settings_technology_level_update', {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        settingsTechnologyLevel.update(null, $scope.edit,
                function (data) {
                    if (data.http_code == '200')
                    {
                        $scope.init();
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $('.close').click();
                    for (var key in $scope.edit) {
                        $scope.edit[key] = null;
                    }
                    return;

                    $scope.IsDisabled = false;
                },
                function (error) {
                    console.log(error.CustomerTypeText);
                });
    };

    $scope.delete = function (id)
    {
        var settingTechnologyLevel = $resource(Routing.generate('api_hrm_settings_technology_level_delete', {id: id}));
        settingTechnologyLevel.delete(
                function (data) {
                    if (data.http_code == '200')
                    {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $scope.init();
                },
                function (error) {
                    return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };
  });

       // settings language  Controller
app.controller("settingsLanguageCtrl", function ($scope, $resource, filepickerService, $window,$filter) {

    $scope.sortableOptions = {
        update: function (e, ui) {
            $scope.seqnoUpdate('api_seqno_update_language', ui.item.scope().settingLanguage.id);
        }
    };

    $scope.seqnoUpdate = function (route, id) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        edit.update(null, $scope.data.settingsLanguage,
                function (data) {
                },
                function (error) {
                });
    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };

    $scope.init = function ()
    {
        var settingsLanguage = $resource(Routing.generate('api_hrm_settings_language_list'));
        $scope.data = settingsLanguage.get(
                function (data) {
                    console.log(true);
                    console.log(data);
                },
                function (error) {
                    OnError(error.statusText);
                });
                console.log($scope.data);
    };

    $scope.SelectInfos = function (id, type) {
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.data.settingsLanguage.length; k++) {
            if (typeof $scope.data.settingsLanguage[k].id !== "undefined") {
                if ($scope.data.settingsLanguage[k].id == id && 'settingLanguage' == type) {
                    $scope.edit = $scope.data.settingsLanguage[k];
                }
            }
        }
    };

    $scope.save = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate("api_hrm_settings_language_save"));
        url.save($scope.new,
                function (data) {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    if ($('#checkbox').is(":checked")) {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $scope.IsDisabled = false;
                         return;
                    } else {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $('.close').click();
                        $scope.IsDisabled = false;
                    }
                    $scope.init();
                },
                function (error) {

                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;

                });
    };

    $scope.update = function (id)
    {
        var settingsLanguage = $resource(Routing.generate('api_hrm_settings_language_update', {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        settingsLanguage.update(null, $scope.edit,
                function (data) {
                    if (data.http_code == '200')
                    {
                        $scope.init();
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $('.close').click();
                    for (var key in $scope.edit) {
                        $scope.edit[key] = null;
                    }
                    return;

                    $scope.IsDisabled = false;
                },
                function (error) {
                    console.log(error.CustomerTypeText);
                });
    };

    $scope.delete = function (id)
    {
        var settingLanguage = $resource(Routing.generate('api_hrm_settings_language_delete', {id: id}));
        settingLanguage.delete(
                function (data) {
                    if (data.http_code == '200')
                    {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $scope.init();
                },
                function (error) {
                    return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };
  });
  
    // settings language  Controller
app.controller("settingsLanguageLevelCtrl", function ($scope, $resource, filepickerService, $window,$filter) {

    $scope.sortableOptions = {
        update: function (e, ui) {
            $scope.seqnoUpdate('api_seqno_update_language_level', ui.item.scope().settingLanguageLevel.id);
        }
    };

    $scope.seqnoUpdate = function (route, id) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        edit.update(null, $scope.data.settingsLanguageLevel,
                function (data) {
                },
                function (error) {
                });
    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };

    $scope.init = function ()
    {
        var settingsLanguageLevel = $resource(Routing.generate('api_hrm_settings_language_level_list'));
        $scope.data = settingsLanguageLevel.get(
                function (data) {
                    console.log(true);
                    console.log(data);
                },
                function (error) {
                    OnError(error.statusText);
                });
                console.log($scope.data);
    };

    $scope.SelectInfos = function (id, type) {
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.data.settingsLanguageLevel.length; k++) {
            if (typeof $scope.data.settingsLanguageLevel[k].id !== "undefined") {
                if ($scope.data.settingsLanguageLevel[k].id == id && 'settingLanguageLevel' == type) {
                    $scope.edit = $scope.data.settingsLanguageLevel[k];
                }
            }
        }
    };

    $scope.save = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate("api_hrm_settings_language_level_save"));
        url.save($scope.new,
                function (data) {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    if ($('#checkbox').is(":checked")) {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $scope.IsDisabled = false;
                         return;
                    } else {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $('.close').click();
                        $scope.IsDisabled = false;
                    }
                    $scope.init();
                },
                function (error) {

                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;

                });
    };

    $scope.update = function (id)
    {
        var settingsLanguageLevel = $resource(Routing.generate('api_hrm_settings_language_level_update', {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        settingsLanguageLevel.update(null, $scope.edit,
                function (data) {
                    if (data.http_code == '200')
                    {
                        $scope.init();
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $('.close').click();
                    for (var key in $scope.edit) {
                        $scope.edit[key] = null;
                    }
                    return;

                    $scope.IsDisabled = false;
                },
                function (error) {
                    console.log(error.CustomerTypeText);
                });
    };

    $scope.delete = function (id)
    {
        var settingLanguageLevel = $resource(Routing.generate('api_hrm_settings_language_level_delete', {id: id}));
        settingLanguageLevel.delete(
                function (data) {
                    if (data.http_code == '200')
                    {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $scope.init();
                },
                function (error) {
                    return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };
  });
  
    var OnSuccess = function (msg, title = '', icon = 'glyphicon  glyphicon-ok-sign')
    {

        // notif
        $.notify({

            icon: icon,
            title: ((title == '') ? msg : title),
            message: ((title == '') ? title : msg),
        }, {
            // settings
            element: 'body',
            position: null,
            type: "success",
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: false,
            placement: {
                from: "bottom",
                align: "left"
            },
            offset: 20,
            spacing: 10,
            z_index: 1031,
            delay: 5000,
            timer: 1000,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            icon_type: 'class',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title"><b>{1}</b><br></span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
        });
        // end notif
        /*$.gritter.add({
         title: txt,  
         sticky: false,
         time: 2000,
         class_name: 'gritter-success'
         });*/
    };

    var OnError = function (msg, title = '', icon = 'glyphicon  glyphicon-warning-sign')
    {
        // notif
        $.notify({

            icon: icon,
            title: ((title == '') ? msg : title),
            message: ((title == '') ? title : msg),
        }, {
            // settings
            element: 'body',
            position: null,
            type: "danger",
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: false,
            placement: {
                from: "bottom",
                align: "left"
            },
            offset: 20,
            spacing: 10,
            z_index: 1031,
            delay: 5000,
            timer: 1000,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            icon_type: 'class',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title"><b>{1}</b><br></span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
        });
        /*$.gritter.add({
         title: txt,  
         sticky: false,
         time: 2000,
         class_name: 'gritter-error'
         });*/
    };

})();
