app.controller("expenseTypesCtrl", function ($scope, $resource, $filter) {
        
    $scope.sortableOptions = {
        update: function (e, ui) {
//            console.log(ui.item.scope());
//            return;
            if(ui.item.scope().task.type=="Transport"){
                $scope.seqnoUpdate('api_seqno_update_exp_transport', ui.item.scope().task.id);
            }  
            if(ui.item.scope().task.type=="Hebergement"){
                $scope.seqnoUpdate('api_seqno_update_exp_accommodation', ui.item.scope().task.id);                
            
            }  
            if(ui.item.scope().task.type=="Restauration"){
                $scope.seqnoUpdate('api_seqno_update_exp_dinning', ui.item.scope().task.id);
            } 
            if(ui.item.scope().task.type=="Autres"){
                 $scope.seqnoUpdate('api_seqno_update_exp_other', ui.item.scope().task.id);
            }
  
        }
        
        
    };

    //Global function Update
    $scope.seqnoUpdate = function (route, id) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        edit.update(null, $scope.dataResult.result,
                function (data) {
                },
                function (error) {
                    alert('NOT updated');
                });

    };
        
    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {

        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    }

    $scope.typeSearch = '';
    $scope.filterByType = function (type) {
        if (type === 'All') {
            $scope.typeSearch = '';
        } else {
            $scope.typeSearch = type;
        }
    }

    $scope.modelsList = function () {
        var models = $resource(Routing.generate('api_admin_setting_model_tax_list'));
        $scope.datamodels = models.get(
            function (data) {

            },
            function (error) {
            });
    };
    $scope.init = function () {
        $scope.loading = true;
        $scope.blockloader = true;
        var dataResult = $resource(Routing.generate('api_admin_expense_types_list'));

        $scope.dataResult = dataResult.get({display: 'full'},
            function (ResponseData) {

                var tasks = [];
                $scope.tasks = ResponseData.result;
                // $scope.tasks = $scope.data;
                for (var i = 0; i < ResponseData.result.length; i++) {
                    tasks.push(ResponseData.result[i]);
                }


                $scope.perPage = 10;
                $scope.maxSize = 5;
                $scope.setPage = function (pageNo) {
                    $scope.currentPage = pageNo;
                };
                $scope.$watch('searchText', function (term) {
                    var obj = term;
                    $scope.filterList = $filter('filter')(tasks, obj);
                    $scope.currentPage = 1;
                });

                $scope.loading = false;
                $scope.blockloader = false;
            },
            function (error) {
            });

    };

    $scope.getUrlByType = function (type) {
        var url;
        if (type == 'Ik') {
            url = $resource(Routing.generate('adminSettingsExpCoeff_save'));
        }
        if (type == 'Transport') {
            url = $resource(Routing.generate('admin_hrm_settings_exp_transport_save'));
        }
        if (type == 'Accomodation') {
            url = $resource(Routing.generate('admin_hrm_settings_exp_accommodation_save'));
        }
        if (type == 'dining') {
            url = $resource(Routing.generate('admin_hrm_settings_exp_dinning_save'));
        }
        if (type == 'Other') {
            url = $resource(Routing.generate('admin_hrm_settings_exp_other_save'));
        }
        return url;
    }

    $scope.SelectInfos = function (id, type) {
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.tasks.length; k++) {
            if (typeof $scope.tasks[k].id !== "undefined") {
                if ($scope.tasks[k].id == id && $scope.tasks[k].type == type) {
                    $scope.detail = $scope.tasks[k];
                    // if ($scope.tasks[k].type == 'Ik') {
                    //     $scope.detail.contributor.id = ($scope.tasks[k].contributor.id).toString();
                    //     $scope.detail.validator.id = ($scope.tasks[k].validator.id).toString();
                    // }
                    if ($scope.tasks[k].type == 'Transport') {
                        $scope.detail.tax_model.id = ($scope.tasks[k].tax_model.id).toString();
                    }

                    if ($scope.tasks[k].type == 'Hebergement') {
                        $scope.detail.tax_model.id = ($scope.tasks[k].tax_model.id).toString();
                    }

                    if ($scope.tasks[k].type == 'Restauration') {
                        $scope.detail.tax_model.id = ($scope.tasks[k].tax_model.id).toString();
                    }

                    if ($scope.tasks[k].type == 'Autres') {
                        $scope.detail.tax_model.id = ($scope.tasks[k].tax_model.id).toString();
                    }
                }
            }
        }

        $scope.loading = false;
        $scope.blockloader = false;
    }
    $scope.save = function (type) {
        $scope.IsDisabled = true;
        var url = $scope.getUrlByType(type);

        url.save($scope.data,
            function (data) {
                if ($('#checkbox').is(":checked")) {
                    $scope.data.display_name = null;
                    $scope.data.tax_model = 0;
                    $scope.data.description = null;
                    $scope.data.checkbox = null;
                    $scope.IsDisabled = false;
                    return;
                } else {
                    $scope.data.display_name = null;
                    $scope.data.tax_model = 0;
                    $scope.data.description = null;
                    $scope.data.checkbox = null;
                    $('.close').click();
                    $scope.IsDisabled = false;
                }
                $scope.init();
            },
            function (error) {
            });
    }


//update general method
    $scope.update = function (route, id) {
        $scope.IsDisabled = true;
        var edit = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        edit.update(null, $scope.detail,
            function (data) {
            $scope.init();
                $('.close').click();
                $scope.IsDisabled = false;
            },
            function (error) {
                $scope.IsDisabled = false;
            });
    };

    $scope.RoutingDelete = function (type) {
        var route;
        if (type == 'Transport') {
            route = "admin_hrm_settings_exp_transport_delete";
        }
        if (type == 'Autres') {
            route = "admin_hrm_settings_exp_other_delete";
        }
        if (type == 'Restauration') {
            route = "admin_hrm_settings_exp_dinning_delete";
        }
        if (type == 'Hebergement') {
            route = "api_hrm_settings_exp_accommodation_delete";
        }
        return route;
    };

    $scope.delete = function (id, type) {
        var routeParam = $scope.RoutingDelete(type);
        var deletescope = $resource(Routing.generate(routeParam, {id: id}));
        deletescope.delete(
            function (data) {
                if (data.http_code == '200') {
                    console.log('success');
                } else {
                    console.log('error');
                }
                $scope.init();
            },
            function (error) {
                console.log('error');
            });
    };

    $scope.data = {};

    $scope.data.type = 'Transport';
    $scope.transport = true;

// console.log($scope.data.type);
    $scope.appear = function () {
//Type appear/disapear
//         if ($scope.data.type == 'Ik') {
//             $scope.ik = true;
//             $scope.transport = false;
//             $scope.accomodation = false;
//             $scope.dining = false;
//             $scope.other = false;
//         }
        if ($scope.data.type == 'Transport') {
            $scope.ik = false;
            $scope.transport = true;
            $scope.accomodation = false;
            $scope.dining = false;
            $scope.other = false;
        }
        if ($scope.data.type == 'Hebergement') {
            $scope.ik = false;
            $scope.transport = false;
            $scope.accomodation = true;
            $scope.dining = false;
            $scope.other = false;
        }
        if ($scope.data.type == 'Restauration') {
            $scope.ik = false;
            $scope.transport = false;
            $scope.accomodation = false;
            $scope.dining = true;
            $scope.other = false;
        }
        if ($scope.data.type == 'Autres') {
            $scope.ik = false;
            $scope.transport = false;
            $scope.accomodation = false;
            $scope.dining = false;
            $scope.other = true;
        }
    }
//end type

    $('.coefficient-year').datetimepicker({
        format: 'YYYY',
        viewMode: "years",
    });
    $('.coefficient-year').on("dp.change", function () {
        $scope.data.year = $('.coefficient-year').val();
    });

});