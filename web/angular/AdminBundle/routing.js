//Routing
 (function() {
    'use strict'; 
app.config([ "$routeProvider", "$locationProvider", "$httpProvider",function( $routeProvider, $httpProvider){
   
   return $routeProvider
           
// type contracts
   .when('/typecontracts',
   {
       caseInsensitiveMatch:true,
       controller:'TypeContractsCtrl',
       templateUrl:Routing.generate('adminTypeContracts_index')
   })
   .when('/typecontracts/new',
   { 
       caseInsensitiveMatch:true,
       controller:'TypeContractsCtrl',
       templateUrl:Routing.generate('adminTypeContracts_new')
   })
   .when('/typecontracts/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'TypeContractsCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminTypeContracts_edit', {id: params.id});
       }
              
   })
   
   // Categories
    .when('/categories',
   {
       caseInsensitiveMatch:true,
       controller:'CategoriesCtrl',
       templateUrl:Routing.generate('adminCategories_index')
   })
   .when('/categories/new',
   { 
       caseInsensitiveMatch:true,
       controller:'CategoriesCtrl',
       templateUrl:Routing.generate('adminCategories_new')
   })
   .when('/categories/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'CategoriesCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminCategories_edit', {id: params.id});
       }
              
   })
   
   //Roles
   .when('/roles',
   {
       caseInsensitiveMatch:true,
       controller:'RolesCtrl',
       templateUrl:Routing.generate('adminRoles_index')
   })
   .when('/roles/new',
   { 
       caseInsensitiveMatch:true,
       controller:'RolesCtrl',
       templateUrl:Routing.generate('adminRoles_new')
   })
   .when('/roles/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'RolesCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminRoles_edit', {id: params.id});
       }
              
   })
  // Projets
    .when('/projects',
   {
       caseInsensitiveMatch:true,
       controller:'ProjectsCtrl',
       templateUrl:Routing.generate('adminProjects_index')
   })
   .when('/projects/new',
   { 
       caseInsensitiveMatch:true,
       controller:'ProjectsCtrl',
       templateUrl:Routing.generate('adminProjects_new')
   })
   .when('/projects/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'ProjectsCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminProjects_edit', {id: params.id});
       }
              
   })
   //typeconges
   .when('/typeconges/',
   {
       caseInsensitiveMatch:true,
       controller:'TypeCongesCtrl',
       templateUrl:Routing.generate('adminTypeConges_index')
   })
   .when('/typeconges/new',
   { 
       caseInsensitiveMatch:true,
       controller:'TypeCongesCtrl',
       templateUrl:Routing.generate('adminTypeConges_new')
   })
   .when('/typeconges/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'TypeCongesCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminTypeConges_edit', {id: params.id});
       }
              
   })
   
   //Conges
   .when('/conges/',
   {
       caseInsensitiveMatch:true,
       controller:'CongesCtrl',
       templateUrl:Routing.generate('adminConges_index')
   })
   .when('/conges/new',
   { 
       caseInsensitiveMatch:true,
       controller:'CongesCtrl',
       templateUrl:Routing.generate('adminConges_new')
   })
   .when('/conges/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'CongesCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminConges_edit', {id: params.id});
       }
              
   })
   // payment method
   .when('/paymentmethod',
   {
       caseInsensitiveMatch:true,
       controller:'PaymentMethodCtrl',
       templateUrl:Routing.generate('adminPaymentMethod_index')
   })
   .when('/paymentmethod/new',
   { 
       caseInsensitiveMatch:true,
       controller:'PaymentMethodCtrl',
       templateUrl:Routing.generate('adminPaymentMethod_new')
   })
   .when('/paymentmethod/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'PaymentMethodCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminPaymentMethod_edit', {id: params.id});
       }
              
   })
   // Training Type
   .when('/trainingtype',
   {
       caseInsensitiveMatch:true,
       controller:'TrainingTypeCtrl',
       templateUrl:Routing.generate('adminTrainingType_index')
   })
   .when('/trainingtype/new',
   { 
       controller:'TrainingTypeCtrl',
       templateUrl:Routing.generate('adminTrainingType_new')
   })
   .when('/trainingtype/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'TrainingTypeCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminTrainingType_edit', {id: params.id});
       }
              
   })
    .when('/status',
   {
       caseInsensitiveMatch:true,
       controller:'StatusCtrl',
       templateUrl:Routing.generate('adminStatus_index')
   })
   .when('/status/new',
   { 
       caseInsensitiveMatch:true,
       controller:'StatusCtrl',
       templateUrl:Routing.generate('adminStatus_new')
   })
   .when('/status/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'StatusCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminStatus_edit', {id: params.id});
       }
              
   })
   .when('/premiumtype',
   {
       caseInsensitiveMatch:true,
       controller:'PremiumTypeCtrl',
       templateUrl:Routing.generate('adminPremiumType_index')
   })
   .when('/premiumtype/new',
   { 
       caseInsensitiveMatch:true,
       controller:'PremiumTypeCtrl',
       templateUrl:Routing.generate('adminPremiumType_new')
   })
   .when('/premiumtype/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'PremiumTypeCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminPremiumType_edit', {id: params.id});
       }
              
   })
    .when('/subpremiumtype',
   {
       caseInsensitiveMatch:true,
       controller:'SubPremiumTypeCtrl',
       templateUrl:Routing.generate('adminSubPremiumType_index')
   })
   .when('/subpremiumtype/new',
   { 
       caseInsensitiveMatch:true,
       controller:'SubPremiumTypeCtrl',
       templateUrl:Routing.generate('adminSubPremiumType_new')
   })
   .when('/subpremiumtype/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SubPremiumTypeCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminSubPremiumType_edit', {id: params.id});
       }
              
   })
   // Type Transports
   .when('/typetransports',
   {
       caseInsensitiveMatch:true,
       controller:'TypeTransportsCtrl',
       templateUrl:Routing.generate('adminTypeTransports_index')
   })
   .when('/typetransports/new',
   { 
       caseInsensitiveMatch:true,
       controller:'TypeTransportsCtrl',
       templateUrl:Routing.generate('adminTypeTransports_new')
   })
   .when('/typetransports/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'TypeTransportsCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminTypeTransports_edit', {id: params.id});
       }
              
   })
   // Type Expenses
   .when('/typeexpenses',
   {
       caseInsensitiveMatch:true,
       controller:'TypeExpensesCtrl',
       templateUrl:Routing.generate('admin_hrm_settings_exp_other_index')
   })
   .when('/typeexpenses/new',
   { 
       caseInsensitiveMatch:true,
       controller:'TypeExpensesCtrl',
       templateUrl:Routing.generate('admin_hrm_settings_exp_other_new')
   })
   .when('/typeexpenses/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'TypeExpensesCtrl',
       templateUrl:function(params) {
           return  Routing.generate('admin_hrm_settings_exp_other_edit', {id: params.id});
       }
              
   })
   .when('/profil',
   {
       caseInsensitiveMatch:true,
       controller:'ProfilsCtrl',
       templateUrl:Routing.generate('_profils_edit')
   })
   // settings absences
   .when('/settingsabsences',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsAbsencesCtrl',
       templateUrl:Routing.generate('adminSettingsAbsences_index')
   })
   .when('/settingsabsences/new',
   { 
       caseInsensitiveMatch:true,
       controller:'SettingsAbsencesCtrl',
       templateUrl:Routing.generate('adminSettingsAbsences_new')
   })
   .when('/settingsabsences/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsAbsencesCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminSettingsAbsences_edit', {id: params.id});
       }
              
   })
   // Accommodation controller
   .when('/settingsexpaccommodation',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsExpAccommodationCtrl',
       templateUrl:Routing.generate('admin_hrm_settings_exp_accommodation_index')
   })
   .when('/settingsexpaccommodation/new',
   { 
       caseInsensitiveMatch:true,
       controller:'SettingsExpAccommodationCtrl',
       templateUrl:Routing.generate('admin_hrm_settings_exp_accommodation_new')
   })
   .when('/settingsexpaccommodation/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsExpAccommodationCtrl',
       templateUrl:function(params) {
           return  Routing.generate('admin_hrm_settings_exp_accommodation_edit', {id: params.id});
       }
              
   })
   
    // settingsExpCoeff controller
   .when('/settingsexpcoeff',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsExpCoeffCtrl',
       templateUrl:Routing.generate('adminSettingsExpCoeff_index')
   })
   .when('/settingsexpcoeff/new',
   { 
       caseInsensitiveMatch:true,
       controller:'SettingsExpCoeffCtrl',
       templateUrl:Routing.generate('adminSettingsExpCoeff_new')
   })
   .when('/settingsexpcoeff/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsExpCoeffCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminSettingsExpCoeff_edit', {id: params.id});
       }
              
   })
   // TAX controller
   .when('/settingstax',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsTaxCtrl',
       templateUrl:Routing.generate('admin_hrm_settings_tax_index')
   })
   .when('/settingstax/new',
   { 
       caseInsensitiveMatch:true,
       controller:'SettingsTaxCtrl',
       templateUrl:Routing.generate('admin_hrm_settings_tax_new')
   })
   .when('/settingstax/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsTaxCtrl',
       templateUrl:function(params) {
           return  Routing.generate('admin_hrm_settings_tax_edit', {id: params.id});
       }
              
   })
   // exp setting transport
   .when('/settingsexptransport',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsExpTransportCtrl',
       templateUrl:Routing.generate('admin_hrm_settings_exp_transport_index')
   })
   .when('/settingsexptransport/new',
   { 
       caseInsensitiveMatch:true,
       controller:'SettingsExpTransportCtrl',
       templateUrl:Routing.generate('admin_hrm_settings_exp_transport_new')
   })
   .when('/settingsexptransport/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsExpTransportCtrl',
       templateUrl:function(params) {
           return  Routing.generate('admin_hrm_settings_exp_transport_edit', {id: params.id});
       }
              
   })
  // exp setting dinning
  .when('/settingsexpdinning',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsExpDinningCtrl',
       templateUrl:Routing.generate('admin_hrm_settings_exp_dinning_index')
   })
   .when('/settingsexpdinning/new',
   { 
       caseInsensitiveMatch:true,
       controller:'SettingsExpDinningCtrl',
       templateUrl:Routing.generate('admin_hrm_settings_exp_dinning_new')
   })
   .when('/settingsexpdinning/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsExpDinningCtrl',
       templateUrl:function(params) {
           return  Routing.generate('admin_hrm_settings_exp_dinning_edit', {id: params.id});
       }
              
   })
   // setting exp gift context
   .when('/settingsgiftcontext',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsGiftContextCtrl',
       templateUrl:Routing.generate('adminSettingsGiftContext_index')
   })
   .when('/settingsgiftcontext/new',
   { 
       caseInsensitiveMatch:true,
       controller:'SettingsGiftContextCtrl',
       templateUrl:Routing.generate('adminSettingsGiftContext_new')
   })
   .when('/settingsgiftcontext/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsGiftContextCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminSettingsGiftContext_edit', {id: params.id});
       }
              
   })
  // Vehicle
  .when('/settingsadvvehicle',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsAdvVehicleCtrl',
       templateUrl:Routing.generate('adminSettingsAdvVehicle_index')
   })
   .when('/settingsadvvehicle/new',
   { 
       caseInsensitiveMatch:true,
       controller:'SettingsAdvVehicleCtrl',
       templateUrl:Routing.generate('adminSettingsAdvVehicle_new')
   })
   .when('/settingsadvvehicle/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsAdvVehicleCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminSettingsAdvVehicle_edit', {id: params.id});
       }
              
   })
   // study level
   .when('/settingsstudylevel',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsStudyLevelCtrl',
       templateUrl:Routing.generate('adminSettingsStudyLevel_index')
   })
   .when('/settingsstudylevel/new',
   { 
       caseInsensitiveMatch:true,
       controller:'SettingsStudyLevelCtrl',
       templateUrl:Routing.generate('adminSettingsStudyLevel_new')
   })
   .when('/settingsstudylevel/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsStudyLevelCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminSettingsStudyLevel_edit', {id: params.id});
       }
              
   })
   // study type
   .when('/settingsstudytype',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsStudyTypeCtrl',
       templateUrl:Routing.generate('adminSettingsStudyType_index')
   })
   .when('/settingsstudytype/new',
   { 
       caseInsensitiveMatch:true,
       controller:'SettingsStudyTypeCtrl',
       templateUrl:Routing.generate('adminSettingsStudyType_new')
   })
   .when('/settingsstudytype/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsStudyTypeCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminSettingsStudyType_edit', {id: params.id});
       }
              
   })
   //currency
   .when('/settingscurrencies',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsCurrencyCtrl',
       templateUrl:Routing.generate('adminSettingsCurrency_index')
   })
   .when('/settingscurrencies/new',
   { 
       caseInsensitiveMatch:true,
       controller:'SettingsCurrencyCtrl',
       templateUrl:Routing.generate('adminSettingsCurrency_new')
   })
   .when('/settingscurrencies/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsCurrencyCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminSettingsCurrency_edit', {id: params.id});
       }
              
   })
   .when('/contract',
   {
       caseInsensitiveMatch:true,
       controller:'EmployeeContractCtrl',
       templateUrl:Routing.generate('_EmployeeContract_edit')
   })
   //emp status
   .when('/settingsempstatus',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsEmpStatusCtrl',
       templateUrl:Routing.generate('adminSettingsEmpStatus_index')
   })
   .when('/settingsempstatus/new',
   { 
       caseInsensitiveMatch:true,
       controller:'SettingsEmpStatusCtrl',
       templateUrl:Routing.generate('adminSettingsEmpStatus_new')
   })
   .when('/settingsempstatus/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsEmpStatusCtrl',
       templateUrl:function(params) {
           return  Routing.generate('adminSettingsEmpStatus_edit', {id: params.id});
       }
              
   })
   // hrm_settings_exp_tax_model controller
   .when('/settings_exp_tax_model',
   {
       caseInsensitiveMatch:true,
       controller:'hrm_settings_exp_tax_modelCtrl',
       templateUrl:Routing.generate('admin_hrm_settings_exp_tax_model_index')
   })
   .when('/settings_exp_tax_model/new',
   { 
       caseInsensitiveMatch:true,
       controller:'hrm_settings_exp_tax_modelCtrl',
       templateUrl:Routing.generate('admin_hrm_settings_exp_tax_model_new')
   })
   .when('/settings_exp_tax_model/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'hrm_settings_exp_tax_modelCtrl',
       templateUrl:function(params) {
           return  Routing.generate('admin_hrm_settings_exp_tax_model_edit', {id: params.id});
       }
              
   })
   
   // Menu 
    .when('/menu',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsMenuCtrl',
       templateUrl:Routing.generate('admin_hrm_settings_menu_index')
   })
   .when('/menu/:id/edit',
   {
       caseInsensitiveMatch:true,
       controller:'SettingsMenuCtrl',
       templateUrl:function(params) {
           return  Routing.generate('admin_hrm_settings_menu_edit', {id: params.id});
       }
              
   })   
   .when('/',
   {
       caseInsensitiveMatch:true,
       controller:'RolesCtrl',
       templateUrl:Routing.generate('adminRoles_index')
   })
    .otherwise({
        redirectTo: '/'
      });
}]);

})();
