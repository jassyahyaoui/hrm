   
    app.controller("settingsBenefitPeriodicityCtrl", function ($scope, $resource, $filter) {
        $scope.sortableOptions = {
            update: function (e, ui) {
                $scope.seqnoUpdate('api_seqno_update_benefit_periodicity', ui.item.scope().settingsBenefitPeriodicity.id);

            }
        };

        //Global function Update
        $scope.seqnoUpdate = function (route, id) {
            var edit = $resource(Routing.generate(route, {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });
            edit.update(null, $scope.data.hrm_settings_benefit_periodicity,
                    function (data) {
                    },
                    function (error) {
                    });

        };
        $scope.flashText = {};
        $scope.flashTranslate = function (alias) {

            $scope.flashText = $filter('translate')(alias);
            var MSG = $scope.flashText;
            return MSG;
        }

        $scope.init = function ()
        {
            var SettingsBenefitPeriodicityList = $resource(Routing.generate('adminSettingsBenefitPeriodicity_list'));

            $scope.data = SettingsBenefitPeriodicityList.get({display: 'full'},
                    function (data) {
                        console.log(true);
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.path = function (route, id = null)
        {
            if (id == null)
                window.location = Routing.generate(route).replace('%23', '#');
            else
                window.location = Routing.generate(route, {id: id}).replace('%23', '#');
        };

        $scope.setCurrentId = function (id)
        {
            var SettingsBenefitPeriodicityId = $resource(Routing.generate('adminSettingsBenefitPeriodicity_get', {id: id}));

            $scope.detail = SettingsBenefitPeriodicityId.get({display: 'no'},
                    function (data) {
                        console.log(true);
                    },
                    function (error) {
                        OnError(error.statusText);
                    });

        };

        $scope.save = function ()
        {
            var SettingsBenefitPeriodicityList = $resource(Routing.generate('adminSettingsBenefitPeriodicity_save'));
            SettingsBenefitPeriodicityList.save($scope.new_settingsBenefitPeriodicity,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        if ($scope.createother == true)
                        {
                            for (var key in $scope.new_settingsBenefitPeriodicity) {
                                $scope.new_settingsBenefitPeriodicity[key] = null;
                            }
                            return;
                        } else
                        {
                            $('.close').click();
                            $scope.IsDisabled = false;
                        }
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };
        $scope.SelectInfos = function (id, type) {
            for (var k = 0; k < $scope.data.hrm_settings_benefit_periodicity.length; k++) {
                if (typeof $scope.data.hrm_settings_benefit_periodicity[k].id !== "undefined")
                {
                    if ($scope.data.hrm_settings_benefit_periodicity[k].id == id && 'BenefitPeriodicity' == type)
                    {
                        $scope.edit_settingsBenefitPeriodicity = angular.copy($scope.data.hrm_settings_benefit_periodicity[k]);
                    }
                }
            }
            
        }
        $scope.update = function (id)
        {

            var SettingsBenefitPeriodicityUp = $resource(Routing.generate('adminSettingsBenefitPeriodicity_update', {id: id}), null,
                    {
                        'update': {method: 'PUT'}
                    });

            SettingsBenefitPeriodicityUp.update(null, $scope.edit_settingsBenefitPeriodicity,
                    function (data) {
                        if (data.http_code == '200')
                        {
                            $scope.init();
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $('.close').click();
                        for (var key in $scope.edit_settingsBenefitPeriodicity) {
                            $scope.edit_settingsBenefitPeriodicity[key] = null;
                        }
                        return;

                        $scope.IsDisabled = false;
                    },
                    function (error) {
                        OnError(error.statusText);
                    });
        };

        $scope.delete = function (id)
        {
            var SettingsBenefitPeriodicityList = $resource(Routing.generate('adminSettingsBenefitPeriodicity_delete', {id: id}));
            SettingsBenefitPeriodicityList.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

    });