 (function() {
    'use strict'; 
app.directive('ngValidation', function () {    
    return {
        scope: { ngModel: '=' },
        link: function(scope, el) {          
           el.on('input', function(e) {              
              this.value = this.value.
                            replace (/(^\s*)|(\s*$)/, "").   // removes leading and trailing spaces
                            replace (/[ ]{2,}/gi," ").       // replaces multiple spaces with one space 
                            replace (/\n +/,"\n");           // Removes spaces after newlines
           
              scope.ngModel = this.value;
              
           });
        }
    };
});

app.directive('fpCustomDirective', function (filepickerService) {
    return {
        scope: {
            options: '=',
            onSuccess: '&',
            onError: '&',
        },
        template: '<button class="fp__btn" ng-click="openPicker()">Joindre des fichiers</button>',
        link: function (scope, elm, attrs) {
            scope.openPicker = openPicker;
            scope.options = scope.options || {};
            function openPicker() {
                filepickerService.pick(
                        scope.options,
                        function (Blob) {
                            scope.onSuccess({Blob: Blob});
                        },
                        function (Error) {
                            scope.onError({Error: Error});
                        }
                );
            }
        }
    };
});
app.directive('stringToNumber', function() {
  return {
    require: 'ngModel',
    link: function(scope, element, attrs, ngModel) {
      ngModel.$parsers.push(function(value) {
        return '' + value;
      });
      ngModel.$formatters.push(function(value) {
        return parseFloat(value);
      });
    }
  };
});

app.directive('ngDom', function () {    
    return {
        restrict: 'AE',
        link: function(scope, elem, attrs, ctrl) {
            
            elem[0].addEventListener("keyup", function(event) {   
                  if($('#'+attrs.id).val() !== '' )
                  {
                      var regexp1=new RegExp("^[0-9]{15}$");
                      var i = $('#'+attrs.id).val();
                        if( !regexp1.test(i))
                        {  
                            elem[0].setCustomValidity(attrs.title); 
                            return;
                        }                        
                  }  
                  elem[0].setCustomValidity('');
        });
       }
    };
});
})();