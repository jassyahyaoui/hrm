app.controller("expenseIkCtrl", function ($scope, $resource, $filter) {
    $scope.sortableOptions = {
        update: function (e, ui) {
//            console.log(ui.item.scope());
//            return;
            $scope.seqnoUpdate('api_seqno_update_ExpCoeff', ui.item.scope().task.id);
        }
        
        
    };

    //Global function Update
    $scope.seqnoUpdate = function (route, id) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        edit.update(null, $scope.tasks,
                function (data) {
                },
                function (error) {
                    alert('NOT updated');
                });

    };
    
    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    }

    $scope.init = function () {
        $scope.loading = true;
        $scope.blockloader = true;
        var dataResult = $resource(Routing.generate('adminSettingsExpCoeff_list'));

        $scope.dataResult = dataResult.get({display: 'full'},
            function (ResponseData) {

                var tasks = [];
                $scope.tasks = ResponseData.settingsExpCoeffs;
                // $scope.tasks = $scope.data;
                for (var i = 0; i < ResponseData.settingsExpCoeffs.length; i++) {
                    tasks.push(ResponseData.settingsExpCoeffs[i]);
                }


                $scope.perPage = 10;
                $scope.maxSize = 5;
                $scope.setPage = function (pageNo) {
                    $scope.currentPage = pageNo;
                };
                $scope.$watch('searchText', function (term) {
                    var obj = term;
                    $scope.filterList = $filter('filter')(tasks, obj);
                    $scope.currentPage = 1;
                });

                $scope.loading = false;
                $scope.blockloader = false;
            },
            function (error) {
            });

    };


    $scope.SelectInfos = function (id, type) {
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.tasks.length; k++) {
            if (typeof $scope.tasks[k].id !== "undefined") {
                if ($scope.tasks[k].id == id && 'Ik' == type) {
                    $scope.detail = $scope.tasks[k];
                }
            }
        }

        $scope.loading = false;
        $scope.blockloader = false;
    }
    $scope.save = function (route) {
        $scope.IsDisabled = true;

        var url = $resource(Routing.generate(route));

        url.save($scope.data,
            function (data) {
                if ($('#checkbox').is(":checked")) {
                    $scope.data.display_name = null;
                    $scope.data.description = null;
                    $scope.data.checkbox = null;
                    $scope.IsDisabled = false;
                    return;
                } else {
                    $scope.data.display_name = null;
                    $scope.data.description = null;
                    $scope.data.checkbox = null;
                    $('.close').click();
                    $scope.IsDisabled = false;
                }
                $scope.init();
            },
            function (error) {
            });
    }


//update general method
    $scope.update = function (route, id) {
        $scope.IsDisabled = true;
        var edit = $resource(Routing.generate(route, {id: id}), null,
            {
                'update': {method: 'PUT'}
            });
        edit.update(null, $scope.detail,
            function (data) {
            $scope.init();
                $('.close').click();
                $scope.IsDisabled = false;
            },
            function (error) {
                $scope.IsDisabled = false;
            });
    };


    $scope.delete = function (route,id) {
        var deletescope = $resource(Routing.generate(route, {id: id}));
        deletescope.delete(
            function (data) {
                if (data.http_code == '200') {
                    console.log('success');
                } else {
                    console.log('error');
                }
                $scope.init();
            },
            function (error) {
                console.log('error');
            });
    };

    $('.coefficient-year').datetimepicker({
        format: 'YYYY',
        viewMode: "years",
    });
    $('.coefficient-year').on("dp.change", function () {
        $scope.data.year = $('.coefficient-year').val();
    });
    $('.edit-coefficient-year').datetimepicker({
        format: 'YYYY',
        viewMode: "years",
    });
    $('.edit-coefficient-year').on("dp.change", function () {
        $scope.detail.year = $('.edit-coefficient-year').val();
    });

});