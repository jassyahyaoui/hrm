app.controller("settingsCustomertypeCtrl", function ($scope, $resource, $filter) {
    console.log("settingsCustomertypeCtrl");

    $scope.sortableOptions = {
        update: function (e, ui) {
            $scope.seqnoUpdate('api_seqno_update_customer_type', ui.item.scope().customerType.id);

        }
    };

    //Global function Update
    $scope.seqnoUpdate = function (route, id) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
//                console.log($scope.data);
        edit.update(null, $scope.data.settingsCustomerType,
                function (data) {
//                    alert('updated');
                },
                function (error) {
//                    alert('NOT updated');
                });
    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };

    $scope.init = function ()
    {
        var settingsCustomerTypeList = $resource(Routing.generate('api_hrm_settings_customer_type_list'));

        $scope.data = settingsCustomerTypeList.get(
                function (data) {
                    console.log(true);
                    console.log(data);
                },
                function (error) {
                    OnError(error.statusText);
                });
    };

//        
//    $scope.initttt = function () {
//        console.log("i am init");
//        $scope.loading = true;
//        $scope.blockloader = true;
//        var dataResult = $resource(Routing.generate('api_hrm_settings_customer_type_list'));
//
//        $scope.newResult = dataResult.get({display: 'full'},
//                function (ResponseData) {
//                    console.log(ResponseData);
//                    var tasks = [];
//                    $scope.tasks = ResponseData.settingsCustomerType;
//                    // $scope.tasks = $scope.new;
//                    for (var i = 0; i < ResponseData.settingsCustomerType.length; i++) {
//                        tasks.push(ResponseData.settingsCustomerType[i]);
//                    }
//
//
//                    $scope.perPage = 10;
//                    $scope.maxSize = 5;
//                    $scope.setPage = function (pageNo) {
//                        $scope.currentPage = pageNo;
//                    };
//                    $scope.$watch('searchText', function (term) {
//                        var obj = term;
//                        $scope.filterList = $filter('filter')(tasks, obj);
//                        $scope.currentPage = 1;
//                    });
//
//                    $scope.loading = false;
//                    $scope.blockloader = false;
//                },
//                function (error) {
//                });
//
//    };


    $scope.SelectInfos = function (id, type) {
        
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.data.settingsCustomerType.length; k++) {
            if (typeof $scope.data.settingsCustomerType[k].id !== "undefined") {
                if ($scope.data.settingsCustomerType[k].id == id && 'settingsCustomerType' == type) {
                    $scope.edit = $scope.data.settingsCustomerType[k];
                }
            }
        }
    };

    $scope.save = function (route) {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate("api_hrm_settings_customer_type_save"));
        url.save($scope.new,
                function (data) {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    if ($('#checkbox').is(":checked")) {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $scope.IsDisabled = false;
                        // return;
                    } else {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $('.close').click();
                        $scope.IsDisabled = false;
                    }
                    $scope.init();
                },
                function (error) {

                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;

                });
    };

    $scope.update = function (id)
    {
        var TypeTransportsUp = $resource(Routing.generate('api_hrm_settings_customer_type_update', {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        TypeTransportsUp.update(null, $scope.edit,
                function (data) {
                    if (data.http_code == '200')
                    {
                        $scope.init();
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $('.close').click();
                    for (var key in $scope.edit) {
                        $scope.edit[key] = null;
                    }
                    return;

                    $scope.IsDisabled = false;
                },
                function (error) {
                    console.log(error.CustomerTypeText);
                });
    };    
    
    $scope.delete = function (id)
        {
            console.log("api_hrm_settings_customer_type_delete");
             console.log(Routing.generate('api_hrm_settings_customer_type_delete', {id: id})  ) ;
            
            var TypeTransportsDel = $resource(Routing.generate('api_hrm_settings_customer_type_delete', {id: id}));
            TypeTransportsDel.delete(
                    function (data) {
                        if (data.http_code == '200')
                        {
                            flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        } else
                        {
                            return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        }
                        $scope.init();
                    },
                    function (error) {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    });
        };

});