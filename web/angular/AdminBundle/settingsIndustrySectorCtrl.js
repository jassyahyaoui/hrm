app.controller("settingsIndustrySectorCtrl", function ($scope, $resource, $filter) {

    $scope.sortableOptions = {
        update: function (e, ui) {
            $scope.seqnoUpdate('api_seqno_update_industry_sector', ui.item.scope().industrySector.id);
        }
    };

    $scope.seqnoUpdate = function (route, id) {
        var edit = $resource(Routing.generate(route, {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        edit.update(null, $scope.data.settingsIndustrySectorsList,
                function (data) {
                },
                function (error) {
                });
    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };

    $scope.init = function ()
    {
        var settingsIndustrySectorsList = $resource(Routing.generate('api_hrm_settings_industry_sector_list'));

        $scope.data = settingsIndustrySectorsList.get(
                function (data) {
                    console.log(true);
                    console.log(data);
                },
                function (error) {
                    OnError(error.statusText);
                });
    };

    $scope.SelectInfos = function (id, type) {
        $scope.IsDisabled = false;
        for (var k = 0; k < $scope.data.settingsIndustrySectorsList.length; k++) {
            if (typeof $scope.data.settingsIndustrySectorsList[k].id !== "undefined") {
                if ($scope.data.settingsIndustrySectorsList[k].id == id && 'settingsIndustrySectors' == type) {
                    $scope.edit = $scope.data.settingsIndustrySectorsList[k];
                }
            }
        }
    };

    $scope.save = function (route) {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate("api_hrm_settings_industry_sector_save"));
        url.save($scope.new,
                function (data) {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    if ($('#checkbox').is(":checked")) {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $scope.IsDisabled = false;
                         return;
                    } else {
                        $scope.new.display_name = null;
                        $scope.new.description = null;
                        $scope.new.checkbox = null;
                        $('.close').click();
                        $scope.IsDisabled = false;
                    }
                    $scope.init();
                },
                function (error) {

                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;

                });
    };

    $scope.update = function (id)
    {
        var TypeTransportsUp = $resource(Routing.generate('api_hrm_settings_industry_sector_update', {id: id}), null,
                {
                    'update': {method: 'PUT'}
                });
        TypeTransportsUp.update(null, $scope.edit,
                function (data) {
                    if (data.http_code == '200')
                    {
                        $scope.init();
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $('.close').click();
                    for (var key in $scope.edit) {
                        $scope.edit[key] = null;
                    }
                    return;

                    $scope.IsDisabled = false;
                },
                function (error) {
                    console.log(error.CustomerTypeText);
                });
    };

    $scope.delete = function (id)
    {
        var TypeTransportsDel = $resource(Routing.generate('api_hrm_settings_industry_sector_delete', {id: id}));
        TypeTransportsDel.delete(
                function (data) {
                    if (data.http_code == '200')
                    {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    }
                    $scope.init();
                },
                function (error) {
                    return flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };

});