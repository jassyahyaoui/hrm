app.controller('hiringCtrl', function ($scope, $resource, $filter, $http, Utils, Ie11, $compile, $timeout)
{
    $scope.new_company = {};
    $scope.new_job_description = {};
    $scope.new_job_applicant = {};
    $scope.new_job_others = {};
    $scope.edit_company = {};
    $scope.edit_job_description = {};
    $scope.edit_job_applicant = {};
    $scope.edit_job_others = {};

    $scope.studies = [{id: 'study 1'}];
    $scope.companies = [{id: 'company 1'}];
    $scope.languages = [{id: 'language 1'}];
    $scope.technlogies = [{id: 'technlogy 1'}];

    $scope.getChecked = function (param, type) {
        if (type === 'NEWS') {
            $scope.new_job_applicant.jobApplicationStudy.checked = param;
        } else if (type === 'NEWC') {
            $scope.new_job_applicant.jobApplicationCompany.checked = param;
        } else if (type === 'NEWL') {
            $scope.new_job_applicant.jobApplicationLanguage.checked = param;
        } else if (type === 'NEWT') {
            $scope.new_job_applicant.jobApplicationTechnology.checked = param;
        } else if (type === 'EDITS') {
            $scope.edit_job_applicant.jobApplicationStudy.checked = param;
        } else if (type === 'EDITC') {
            $scope.edit_job_applicant.jobApplicationCompany.checked = param;
        } else if (type === 'EDITL') {
            $scope.edit_job_applicant.jobApplicationLanguage.checked = param;
        } else if (type === 'EDITT') {
            $scope.edit_job_applicant.jobApplicationTechnology.checked = param;
        }
    };

    $scope.up = function (index, PassedScope) {
        if (index <= 0)
            return;
        var temp = PassedScope[index];
        PassedScope[index] = PassedScope[index - 1];
        PassedScope.checked--;
        PassedScope[index - 1] = temp;
    };

    $scope.down = function (index, PassedScope, ContainerLength) {
        if (index == ContainerLength - 1)
            return;
        var temp = PassedScope[index];
        PassedScope[index] = PassedScope[index + 1];
        PassedScope.checked++;
        PassedScope[index + 1] = temp;
    };

    $scope.initCompany = function ()
    {
        $scope.loading = true;
        var infos = $resource(Routing.generate('api_company_get'));
        infos.get(function (data) {
            $scope.new_company = angular.copy(data.company);
            $scope.edit_company = angular.copy(data.company);
            $scope.showCompany = angular.copy(data.company);
            if ($scope.new_company.industrySector)
                $scope.new_company.industrySector = ($scope.new_company.industrySector.id).toString();
            if ($scope.new_company.customerType)
                $scope.new_company.customerType = ($scope.new_company.customerType.id).toString();
            $scope.new_company.jobCountry = 'Fr';
            if ($scope.edit_company.industrySector)
                $scope.edit_company.industrySector = ($scope.edit_company.industrySector.id).toString();
            if ($scope.edit_company.customerType)
                $scope.edit_company.customerType = ($scope.edit_company.customerType.id).toString();
            $scope.new_company.jobCountry = "FR";
            $scope.company = angular.copy($scope.edit_company);
            $scope.loading = false;
        },
                function (error) {
                });
    };

    $scope.addNewChoice = function (param) {
        var newItemNo = param.length + 1;
        param.push({'id': 'choice' + newItemNo});
    };

    $scope.removeChoice = function (param, PasserScope) {
        var lastItem = param.length - 1;
        param.splice(lastItem);
        delete PasserScope[lastItem];
    };

    $scope.getSettings = function ()
    {   

        var infos = $resource(Routing.generate('api_settings_hiring'));
        infos.get(
                function (data) {
//                     $('.industry-sector').select2({
//                        placeholder: $scope.flashTranslate('INDUSTRYSECTOR'), 
//                        width:"100%",
//                        allowClear: true,
//                        data: $.map(data.settingsIndustrySector, function (v, k) {
//                            return {
//                                id: v['id'],
//                                text: v['display_name']
//                            }              
//                    })
//                }); 
                    $scope.settingsIndustrySector = data.settingsIndustrySector;
                    $scope.settingsSettingsStudyTypes = data.settingsSettingsStudyTypes;
                    $scope.settingsStudyLevels = data.settingsStudyLevels;
                    $scope.settingsPlannedJourney = data.settingsPlannedJourney;
                    $scope.settingsPeriodicity = data.settingsPeriodicity;
                    $scope.settingsTypecontracts = data.settingsTypecontracts;
                    $scope.settingsCustomerType = data.settingsCustomerType;
                    $scope.settingsLanguageLevel = data.settingsLanguageLevel;
                    $scope.settingsLanguage = data.settingsLanguage;
                    $scope.settingsTechnology = data.settingsTechnology;
                    $scope.settingsTechnologyLevel = data.settingsTechnologyLevel;
                    $scope.settingsJob = data.settingsJob;
                                        
                },
                function (error) {
                });
    };

    $scope.change = function (settingObject, scopeItem, item)
    { 
        if (~settingObject.indexOf("jobApplicationLanguage"))
        {
            $scope.getApplicationLanguage(eval('$scope.' + settingObject));

        } else if (~settingObject.indexOf("jobApplicationStudy"))
        {
            $scope.getApplicationStudy(eval('$scope.' + settingObject));
        } else if (~settingObject.indexOf("jobApplicationTechnology"))
        {
            $scope.getApplicationTechnology(eval('$scope.' + settingObject));
        } else if (~settingObject.indexOf("jobApplicationCompany"))
        {
            $scope.getApplicationCompany(eval('$scope.' + settingObject));
        } else if (~settingObject.indexOf("applicantContact") || ~settingObject.indexOf("settingsSuperior"))
        {
            $scope.getApplicantContact(settingObject, scopeItem);
        } else
        {
            if (undefined !== eval('$scope.' + settingObject))
                $.each(eval('$scope.' + settingObject), function (k, v) {
                    if (v['id'] == parseInt(scopeItem)) {
                        eval('$scope.' + item + '=v["display_name"]');
                    }                    
                });
        }
    };

    $scope.getApplicationStudy = function (settingObject) {
        $scope.jobApplicationStudyModel = [];
        $.each(settingObject, function (k, v) {
            studyLevel = $scope.getType(v['studyLevelId'], $scope.settingsStudyLevels);
            studyType = $scope.getType(v['studyTypeId'], $scope.settingsSettingsStudyTypes);
            if (undefined !== v['name'])
                name = v['name'];
            else
                name = "";
            $scope.jobApplicationStudyModel.push({"studyType": studyType, "studyLevel": studyLevel, "name": name});
        });
    };

    $scope.getApplicationCompany = function (settingObject) {
        $scope.jobApplicationCompanyModel = [];
        $.each(settingObject, function (k, v) {
            industrysector = $scope.getType(v['industrysectorId'], $scope.settingsIndustrySector);
            jobType = $scope.getType(v['jobId'], $scope.settingsJob);
            if (undefined !== v['name'])
                name = v['name'];
            else
                name = "";
            $scope.jobApplicationCompanyModel.push({"jobType": jobType, "industrysector": industrysector, "name": name});
        });
    };

    $scope.getApplicationLanguage = function (settingObject) {
        $scope.getApplicationLanguageModel = [];
        $.each(settingObject, function (k, v) {
            languageLevel = $scope.getType(v['languageLevelId'], $scope.settingsLanguageLevel);
            language = $scope.getType(v['languageId'], $scope.settingsLanguage);
            $scope.getApplicationLanguageModel.push({"language": language, "languageLevel": languageLevel});
        });
    };

    $scope.getApplicationTechnology = function (settingObject) {
        $scope.ApplicationTechnologyModel = [];
        $.each(settingObject, function (k, v) {
            technologyLevel = $scope.getType(v['technologyLevelId'], $scope.settingsTechnologyLevel);
            technology = $scope.getType(v['technologyId'], $scope.settingsTechnology);
            $scope.ApplicationTechnologyModel.push({"technology": technology, "technologyLevel": technologyLevel});
        });
    };

    $scope.getType = function (id, scope) {
        if (undefined === id)
            return "";
        var str = "";
        angular.forEach(scope, function (v, k) {
            if (parseInt(v['id']) == id)
            {
                str = v['display_name'];
            }
            ;
        });
        return str;
    };

    /**
     * retrive emp ApplicantContact
     * @returns {undefined}
     */
    $scope.getApplicantContact = function (to, id)
    {
        var infos = $resource(Routing.generate('api_settings_application_contact', {id: id}));
        infos.get(
                function (data) {
                    if (to == 'applicantContact')
                        $scope.applicantContact = data.applicantContact;
                    else
                        $scope.applicantSuperior = data.applicantContact;
                },
                function (error) {
                });
                console.log( $scope.applicantSuperior);
    };

    $scope.getTags = function ()
    {
        $scope.loading = true;
        var infos = $resource(Routing.generate('api_hrm_settings_tag_list'));
        infos.get(
                function (data) {
                    var tags = [];
                    angular.forEach(data.tags, function (u, key) {
                        tags.push({"text": u.display_name, "id": u.id});
                    });
                    $scope.tags = tags;
                },
                function (error) {
                });
    };

    $scope.loadTags = function (query) {
        var tags = $scope.tags;
        return tags.filter(function (tag) {
            return tag.text.toLowerCase().indexOf(query.toLowerCase()) != -1;
        });
        return tags;
    };

    $scope.saveNDValidate = function (e) {
        console.log("saveNDValidate");
        switch (e)
        {
            case 'COMPANY':
                if ($('#saveCompany').is(":focus"))
                    $scope.save_company();
                if ($('#validateCompany').is(":focus"))
                    $scope.save_continue_company();
                if ($('#EditSaveCompany').is(":focus"))
                    $scope.update_company();
                if ($('#EditValidateCompany').is(":focus"))
                    $scope.update_continue_company();
                break;
            case 'JOB':
                if ($('#saveJobDescription').is(":focus"))
                    $scope.save_job();
                if ($('#validateJobDescription').is(":focus"))
                    $scope.save_continue_job();
                if ($('#editSaveJob').is(":focus"))
                    $scope.update_job();
                if ($('#editValidateJob').is(":focus"))
                    $scope.update_continue_job();
                break;
            case 'APPLICANT':
                if ($('#saveApplicant').is(":focus"))
                    $scope.save_applicant();
                if ($('#validateApplicant').is(":focus"))
                    $scope.save_continue_save_applicant();
                if ($('#EditSaveApplicant').is(":focus"))
                    $scope.update_applicant();
                if ($('#EditValidateApplicant').is(":focus"))
                    $scope.update_continue_applicant();
                break;
            case 'OTHERS':
                if ($('#saveOthers').is(":focus"))
                    $scope.save_others();
                if ($('#validateOthers').is(":focus"))
                    $scope.save_continue_others();
                if ($('#EditSaveOthers').is(":focus"))
                    $scope.update_others();
                if ($('#EditValidateOthers').is(":focus"))
                    $scope.update_continue_others();
                break;
        }
    };

    $scope.save_continue_company = function () {
        $('#btn_new_hiring_job').click();
        $scope.save_company();
    };

    $scope.update_continue_company = function () {
        $('#btn_edit_hiring_job').click();
        $scope.update_company();
    };

    $scope.save_company = function () {
        console.log("save_company")
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate('api_hrm_document_jobdescription_company_save'));
        if ($scope.jobId)
            $scope.new_company.jobId = $scope.jobId;
        if ($scope.companyId)
            $scope.new_company.companyId = $scope.companyId;
        url.save($scope.new_company,
                function (successResult) {
                    $scope.jobId = successResult["jobId"];
                    $scope.companyId = successResult["companyId"];
                    $scope.IsDisabled = false;
                    $scope.datatable();
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                },
                function (errorResult) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;
                });
    };

    $scope.update_company = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate('api_hrm_document_jobdescription_company_save'));
        if ($scope.jobId)
            $scope.edit_company.jobId = $scope.jobId;
        url.save($scope.edit_company,
                function (successResult) {
                    $scope.jobId = successResult["jobId"];
                    $scope.IsDisabled = false;
                    $scope.datatable();
                    setTimeout(function () {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    }, 0);
                },
                function (errorResult) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;
                });
    };

    $scope.save_continue_job = function () {
        $scope.save_job();
        $('#btn_new_hiring_applicant').click();
    };

    $scope.update_continue_job = function () {
        $scope.update_job();
        $('#btn_edit_hiring_applicant').click();
    };

    $scope.new = function () {
        if ($scope.new_company)
        {
            delete  $scope.new_company.jobCity;
            delete  $scope.new_company.description;
            delete  $scope.new_company.applicantContact;
            delete  $scope.new_company.jobAdress;
            delete  $scope.new_company.jobPostalCode;
            delete  $scope.new_company.jobCountry;
        } else
        {
            $scope.new_company = {};
            $scope.new_company.jobCountry = "FR";
        }
        $scope.new_company = $scope.company;
        $scope.change('settingsIndustrySector', $scope.new_company.industrySector, 'industrySector');
        $scope.change('settingsCustomerType', $scope.new_company.customerType, 'customerType');
        for (var key in $scope.new_job_description) {
            $scope.new_job_description[key] = null;
        }
        if ($scope.new_job_description)
            delete $scope.new_job_description.plannedJourneyNumber;
        for (var key in $scope.new_job_applicant) {
          delete  $scope.new_job_applicant[key] ;
        }
         if ($scope.new_job_others)
            delete $scope.new_job_others.otherComment;
        delete $scope.jobId;
        if ($scope.new_company)
            delete $scope.new_company.jobId;
        if ($scope.new_job_description)
            delete $scope.new_job_description.jobId;
        if ($scope.new_job_applicant)
            delete $scope.new_job_applicant.jobId;

        $scope.new_job_applicant.ApplicantStudySup = false;
        $scope.studies = [{id: 'study 1'}];
        $scope.companies = [{id: 'company 1'}];
        $scope.languages = [{id: 'language 1'}];
        $scope.technlogies = [{id: 'technlogy 1'}];
        $scope.IsDisabled = false;

        $scope.new_job_applicant.jobApplicationStudy = [{id: 'study 1'}];
        $scope.new_job_applicant.jobApplicationStudy.checked = {};
        $scope.new_job_applicant.jobApplicationCompany = [{id: 'company 1'}];
        $scope.new_job_applicant.jobApplicationCompany.checked = {};
        $scope.new_job_applicant.jobApplicationLanguage = [{id: 'language 1'}];
        $scope.new_job_applicant.jobApplicationLanguage.checked = {};
        $scope.new_job_applicant.jobApplicationTechnology = [{id: 'technology 1'}];
        $scope.new_job_applicant.jobApplicationTechnology.checked = {};
    };

    $scope.save_job = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate('api_hrm_document_jobdescription_save'));
        if ($scope.new_job_description)
            if ($scope.new_job_description.contratType == 2) // type contract == CDI
            {
                $scope.new_job_description.startDate = null;
                $scope.new_job_description.endDate = null;
            }
        if ($scope.jobId)
        {
            if ($scope.new_job_description)
                $scope.new_job_description.jobId = $scope.jobId;
        }
        url.save($scope.new_job_description,
                function (successResult) {
                    $scope.jobId = successResult["jobId"];
                    $scope.IsDisabled = false;
                    $scope.datatable();
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                },
                function (errorResult) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;
                });
    };

    $scope.update_job = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate('api_hrm_document_jobdescription_save'));
        if ($scope.edit_job_description)
            if ($scope.edit_job_description.contratType == 2) // type contract == CDI
            {
                $scope.edit_job_description.startDate = null;
                $scope.edit_job_description.endDate = null;
            }
        if ($scope.jobId)
        {
            if ($scope.edit_job_description)
                $scope.edit_job_description.jobId = $scope.jobId;
        }
        url.save($scope.edit_job_description,
                function (successResult) {
                    $scope.jobId = successResult["jobId"];
                    $scope.IsDisabled = false;
                    $scope.datatable();
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                },
                function (errorResult) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;
                });
    };

    $scope.save_continue_save_applicant = function () {
        $('#btn_new_hiring_other').click();
        $scope.save_applicant();
    };

    $scope.update_continue_applicant = function () {
        $('#btn_edit_hiring_other').click();
        $scope.update_applicant();
    };

    $scope.save_applicant = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate('api_hrm_document_jobdescription_applicant_save'));
        if (undefined !== $scope.jobId)
            $scope.new_job_applicant.jobId = $scope.jobId;
        url.save($scope.new_job_applicant,
                function (successResult) {
                    $scope.jobId = successResult["jobId"];
                    $scope.companyId = successResult["companyId"];
                    $scope.IsDisabled = false;
                    $scope.datatable();
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                },
                function (errorResult) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;
                });
    };

    $scope.initHiringNull = function () {
        for (var key in $scope.new_job_applicant) {
            $scope.new[key] = null;
        }
        for (var key in $scope.new_job_applicant) {
            $scope.edit[key] = null;
        }
    };

    $scope.update_applicant = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate('api_hrm_document_jobdescription_applicant_save'));
        if ($scope.jobId)
            $scope.edit_job_applicant.jobId = $scope.jobId;
        url.save($scope.edit_job_applicant,
                function (successResult) {
                    $scope.jobId = successResult["jobId"];
                    $scope.IsDisabled = false;
                    $scope.datatable();
                    setTimeout(function () {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    }, 0);
                },
                function (errorResult) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;
                });
    };

    $scope.add = function (Route) {
        var url = $resource(Routing.generate(Route));
        url.save($scope.edit_job_applicant,
                function (successResult) {
                    $('.close').click();
                },
                function (errorResult) {
                });
    };

    $scope.delete = function (id) {
        var remove = $resource(Routing.generate("remove_job_description", {id: id}));
        remove.delete(
                function (data) {
                    $scope.datatable();
                    flash($scope.flashTranslate('FLASH_VALID_DELETE'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                },
                function (error) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };

    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {
        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    };

    $scope.userData = {
    };

    $scope.closewindow = function (id)
    {
        if (undefined !== id)
        {
            $("#" + id + " .close").click();
            return;
        }
        $('.close').click();
    };

    $scope.pushToArray = function (array, value) {
        if (array.indexOf(value) === -1) {
            array.push(value);
        }
        return array;
    };

    $scope.hideCKeditor = function (event, action)
    {
        $scope.getTemplate(action);
    };

    $scope.getTemplate = function (action) {

        $scope.loading = true;
        $scope.blockloader = true;
        $(function () {
            var infos = $resource(Routing.generate('api_settings_hiring_template', {'action': action}));
            infos.get(
                    function (data) {
                        var el;
                        
                        if (action == 'show')
                        {
                            el = $('#templateShow').html(data['html']);
                            $compile(el)($scope);

                        } else if (action == 'edit')
                        {
                            if ($.inArray($scope.edit_hrm_document.htmlTemplate, ["", null]) == -1)
                            {
                                el = $('#templateEdit').html(data['html']);
                                $('.content-ckeditor').html($scope.edit_hrm_document.htmlTemplate);
                            } else
                            {
                                el = $('#templateEdit').html(data['html']);
                            }
                            $compile(el)($scope);
                        } else
                        {
                            $compile($('#templateBody').html(data['html']))($scope);
                            $scope.html = $compile(data['html'])($scope);
                        }
                        $scope.loading = false;
                        $scope.blockloader = false;

                    },
                    function (error) {
                    });
        });
    };

    $scope.editCKeditor = function (content) {
        if (undefined === content)
        {
            var dataHtml = CKEDITOR.instances.form_content.getData();
            $('#form-ckeditor').hide();
            $('.content-ckeditor').html(dataHtml);
            $('.content-ckeditor').show();
            $('.form-actions-ckeditor').hide();
            $('.form-actions').show();
        } else
        {
            if (undefined !== $scope.edit_hrm_document)
            {
                var cntstr = $('.' + content).html();
                $('.' + content).hide();
                CKEDITOR.instances.form_content.setData(cntstr);
                $('.form-actions').hide();
                $('#form-ckeditor').show();
                $('.form-actions-ckeditor').show();

            } else if (confirm($scope.flashTranslate('EDIT_CKEDITOR'))) {
                var cntstr = $('.' + content).html();
                $('.' + content).hide();
                CKEDITOR.instances.form_content.setData(cntstr);
                $('.form-actions').hide();
                $('#form-ckeditor').show();
                $('.form-actions-ckeditor').show();
            }
        }
    };

    $scope.imprimer_page = function (printpage) {
        var headstr = "<html ><head><title></title></head><body>";
        var footstr = "</body>";
        var newstr = document.all.item(printpage).innerHTML;
        var oldstr = document.body.innerHTML;
        var x = window.open('', '', 'location=no,toolbar=0');
        x.document.body.innerHTML = headstr + newstr + footstr;
        x.print();
        x.close();
       // $scope.hideCKeditor();
        return false;
    };

    $scope.save_document = function (classContent)
    {
        var obj = new Object();
        obj.data = $('.' + classContent).html();
        if (undefined !== $scope.jobId)
            obj.jobId = $scope.jobId;
        var url = $resource(Routing.generate('api_hrm_document_jobdescription_applicant_save_document'));
        url.save(JSON.stringify(obj),
                function (successResult) {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    $scope.datatable();
                    $scope.closewindow();
                },
                function (errorResult) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };

    $scope.send = function (content)
    {
        $scope.mail.attachment_file = $('.' + content).html();

        var url = $resource(Routing.generate('api_hrm_document_jobdescription_applicant_send_document'));
        url.save($scope.mail,
                function (successResult) {
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    delete $scope.mail;
                    $scope.closewindow('SendTemp');
                },
                function (errorResult) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };

    $scope.download_page = function (classContent)
    {
        var obj = new Object();
        $scope.content = $('.' + classContent).html();
    };

    $scope.datatable = function () {
        $scope.loading = true;
        $scope.blockloader = true;
        var infos = $resource(Routing.generate('api_hrm_jobdescription_list'));
        infos.get(
                function (ResponseData) {
                    var tasks = [];
                    $scope.data = ResponseData.documentJobDescriptions;
                    $scope.FiltersData = ResponseData.result;
                    $scope.tasks = $scope.data;
                    var contractArray = [];
                    angular.forEach(ResponseData.typecontracts, function (c, key) {
                        contractArray.push({"display_name": c.display_name});
                    });

                    $scope.contractTypes = contractArray;
                    for (var k = 0; k < ResponseData.documentJobDescriptions.length; k++) {
                        ResponseData.documentJobDescriptions[k].create_date = moment($filter('timezone')(ResponseData.documentJobDescriptions[k].create_date.date)).format('DD/MM/YYYY');
                        
                        //////---------------
                        console.log( "datatable startdate");
                        console.log(ResponseData.documentJobDescriptions[k].startDate);
                        
//                        ResponseData.documentJobDescriptions[k].startDate = moment($filter('timezone')(ResponseData.documentJobDescriptions[k].create_date.startDate)).format('DD/MM/YYYY');
//                       if(ResponseData.documentJobDescriptions[k].startDate)
//                       ResponseData.documentJobDescriptions[k].startDate = moment($filter('timezone')(ResponseData.documentJobDescriptions[k].startDate)).format('DD/MM/YYYY');
//                   else
//                       ResponseData.documentJobDescriptions[k].startDate = '';
                        
                        
                        ResponseData.documentJobDescriptions[k].startDate = (ResponseData.documentJobDescriptions[k].startDate) ? moment($filter('timezone')(ResponseData.documentJobDescriptions[k].startDate.date)).format('DD/MM/YYYY') :'';
                        ResponseData.documentJobDescriptions[k].endDate = (ResponseData.documentJobDescriptions[k].endDate) ? moment($filter('timezone')(ResponseData.documentJobDescriptions[k].endDate.date)).format('DD/MM/YYYY') :'';
                        
                        tasks.push(ResponseData.documentJobDescriptions[k]);
                        if (ResponseData.documentJobDescriptions[k].contractType !== null)
                            ResponseData.documentJobDescriptions[k]._contractType = ResponseData.documentJobDescriptions[k].contractType.display_name;
                        else
                            ResponseData.documentJobDescriptions[k]._contractType = null;
                    }
                    $scope.perPage = 10;
                    $scope.maxSize = 5;
                    $scope.setPage = function (pageNo) {
                        $scope.currentPage = pageNo;
                    };
                    $scope.$watch('searchText', function (term) {
                        var obj = term;
                        $scope.filterList = $filter('filter')(tasks, obj);
                        $scope.currentPage = 1;
                    });
                    $scope.loading = false;
                    $scope.blockloader = false;
                },
                function (error) {
                });
    };

    $scope.SelectInfos = function (id, type) {
        for (var k = 0; k < $scope.tasks.length; k++) {
            if (typeof $scope.tasks[k].id !== "undefined") {
                if ($scope.tasks[k].id == id && 'jobDescription' == type) {
                    $scope.edit = angular.copy($scope.tasks[k]);
                    $scope.show = angular.copy($scope.tasks[k]);
                    $scope.edit_hrm_document = angular.copy($scope.tasks[k]);

                    if (!$scope.jobId)
                        $scope.jobId = angular.copy($scope.tasks[k].id);

//                    $scope.edit_company = angular.copy($scope.tasks[k]);
                    $scope.edit_company = angular.copy($scope.company);
                    $scope.edit_company.jobAdress = angular.copy($scope.tasks[k].jobAdress);
                    $scope.edit_company.jobPostalCode = angular.copy($scope.tasks[k].jobPostalCode);
                    $scope.edit_company.jobCity = angular.copy($scope.tasks[k].jobCity);
                    $scope.edit_company.applicantContact = ($scope.tasks[k].applicantContact !== null) ? ($scope.tasks[k].applicantContact.id).toString() : '';
                    $scope.showCompany.applicantContact = ($scope.tasks[k].applicantContact !== null) ? ($scope.tasks[k].applicantContact) : '';
                    $scope.edit_company.jobCountry = $scope.tasks[k].jobCountry;
                    $scope.edit_job_description = angular.copy($scope.tasks[k]);
                    $scope.edit_job_description.superior = ($scope.tasks[k].superior !== null) ? ($scope.tasks[k].superior.id).toString() : '';
                    $scope.edit_job_description.contractType = ($scope.tasks[k].contractType !== null) ? ($scope.tasks[k].contractType.id).toString() : '';
                    $scope.edit_job_description.plannedJourneyType = ($scope.tasks[k].plannedJourneyType !== null) ? ($scope.tasks[k].plannedJourneyType.id).toString() : '';
                    $scope.edit_job_description.partTimeDayPeriodicity = ($scope.tasks[k].partTimeDayPeriodicity !== null) ? ($scope.tasks[k].partTimeDayPeriodicity.id).toString() : '';
                   /////-------------------------------
                   
                    if ($scope.tasks[k].startDate)
                         if ($scope.tasks[k].startDate.date)
                             $scope.edit_job_description.startDate = moment($filter('timezone')($scope.tasks[k].startDate.date)).format('DD/MM/YYYY');
                    
//                       if ($scope.tasks[k].endDate)
//                         if ($scope.tasks[k].endDate.date)
//                             $scope.edit_job_description.endDate = moment($filter('timezone')($scope.tasks[k].endDate.date)).format('DD/MM/YYYY');
//                   
                    $scope.edit_job_description.endDate = ($scope.tasks[k].endDate) ? ( $filter('date')($scope.tasks[k].endDate, 'DD/MM/YYYY')) : '';

                    
//                    $scope.edit_job_description.startDate = ($scope.tasks[k].startDate !== null) ? (moment($filter('timezone')($scope.tasks[k].startDate.date)).format('DD/MM/YYYY')) : '';
                   

                    $scope.edit_job_applicant = angular.copy($scope.tasks[k]);
                    $scope.edit_job_applicant.ApplicantStudyComment = $scope.tasks[k].applicantStudyComment;
                    $scope.edit_job_applicant.ApplicantExperienceComment = $scope.tasks[k].applicantExperienceComment;
                    $scope.edit_job_applicant.applicantProfessionalExperience = $scope.tasks[k].applicantProfessionalExperience;
                    $scope.edit_job_applicant.ApplicantStudySup = $scope.tasks[k].applicantStudySup;

                    $scope.edit_job_others.otherComment = angular.copy($scope.tasks[k].otherComment);


                    if ($scope.tasks[k].jobApplicationStudy) {
                        if ($scope.tasks[k].jobApplicationStudy.length > 0)
                        {
                            $scope.edit_job_applicant.jobApplicationStudy = angular.copy($scope.tasks[k].jobApplicationStudy);
                            for (var s = 0; s < $scope.tasks[k].jobApplicationStudy.length; s++) {
                                if ($scope.tasks[k].jobApplicationStudy[s].studyTypeId)
                                    $scope.edit_job_applicant.jobApplicationStudy[s].studyTypeId = angular.copy($scope.edit_job_applicant.jobApplicationStudy[s].studyTypeId.id).toString();
                                if ($scope.tasks[k].jobApplicationStudy[s].studyLevelId)
                                    $scope.edit_job_applicant.jobApplicationStudy[s].studyLevelId = angular.copy($scope.edit_job_applicant.jobApplicationStudy[s].studyLevelId.id).toString();
                            }
                        } else
                            $scope.edit_job_applicant.jobApplicationStudy = [{id: 'jobApplicationStudy 1'}];
                    } else {
                        $scope.edit_job_applicant.jobApplicationStudy = [{id: 'jobApplicationStudy 1'}];
                    }
                    if ($scope.tasks[k].jobApplicationLanguage) {
                        if ($scope.tasks[k].jobApplicationLanguage.length > 0) {
                            $scope.edit_job_applicant.jobApplicationLanguage = angular.copy($scope.tasks[k].jobApplicationLanguage);
                            for (var s = 0; s < $scope.edit_job_applicant.jobApplicationLanguage.length; s++) {
                                if ($scope.tasks[k].jobApplicationLanguage[s].languageId)
                                    $scope.edit_job_applicant.jobApplicationLanguage[s].languageId = ($scope.edit_job_applicant.jobApplicationLanguage[s].languageId.id).toString();
                                if ($scope.edit_job_applicant.jobApplicationLanguage[s].languageLevelId)
                                    $scope.edit_job_applicant.jobApplicationLanguage[s].languageLevelId = ($scope.edit_job_applicant.jobApplicationLanguage[s].languageLevelId.id).toString();
                            }
                        } else
                            $scope.edit_job_applicant.jobApplicationLanguage = [{id: 'jobApplicationLanguage 1'}];
                    } else {
                        $scope.edit_job_applicant.jobApplicationLanguage = [{id: 'jobApplicationLanguage 1'}];
                    }
                    if ($scope.tasks[k].jobApplicationTechnology) {
                        if ($scope.tasks[k].jobApplicationTechnology.length > 0)
                        {
                            $scope.edit_job_applicant.jobApplicationTechnology = angular.copy($scope.tasks[k].jobApplicationTechnology);
                            for (var s = 0; s < $scope.edit_job_applicant.jobApplicationTechnology.length; s++) {
                                $scope.edit_job_applicant.jobApplicationTechnology[s].technologyId = ($scope.edit_job_applicant.jobApplicationTechnology[s].technologyId.id).toString();
                                $scope.edit_job_applicant.jobApplicationTechnology[s].technologyLevelId = ($scope.edit_job_applicant.jobApplicationTechnology[s].technologyLevelId.id).toString();
                            }
                        } else
                            $scope.edit_job_applicant.jobApplicationTechnology = [{id: 'jobApplicationTechnology 1'}];
                    } else {
                        $scope.edit_job_applicant.jobApplicationTechnology = [{id: 'jobApplicationTechnology 1'}];
                    }
                    if ($scope.tasks[k].jobApplicationCompany) {
                        if ($scope.tasks[k].jobApplicationCompany.length > 0)
                        {
                            $scope.edit_job_applicant.jobApplicationCompany = angular.copy($scope.tasks[k].jobApplicationCompany);
                            for (var s = 0; s < $scope.tasks[k].jobApplicationCompany.length; s++) {
                                $scope.edit_job_applicant.jobApplicationCompany[s].jobId = ($scope.edit_job_applicant.jobApplicationCompany[s].jobId.id).toString();
                                $scope.edit_job_applicant.jobApplicationCompany[s].industrysectorId = ($scope.edit_job_applicant.jobApplicationCompany[s].industrysectorId.id).toString();
                            }
                        } else
                            $scope.edit_job_applicant.jobApplicationTechnology = [{id: 'jobApplicationTechnology 1'}];
                    } else {
                        $scope.edit_job_applicant.jobApplicationTechnology = [{id: 'jobApplicationTechnology 1'}];
                    }
                    $scope.edittags = [];
                    $scope.edit_job_applicant.applicantOthersCompetenciesTags = angular.copy($scope.tasks[k].applicantOthersCompetenciesTags);
                    if ($scope.tasks[k].applicantOthersCompetenciesTags) {
                        if ($scope.tasks[k].applicantOthersCompetenciesTags.length > 0) {
                            for (var s = 0; s < $scope.tasks[k].applicantOthersCompetenciesTags.length; s++) {
                                $scope.edittags.push($scope.edit_job_applicant.applicantOthersCompetenciesTags[s].tag);
                            }
                            $scope.edittags = unique($scope.edittags);
                            $scope.edit_job_applicant.applicantOthersCompetenciesTags = $scope.edittags;
                        }
                    }
                }
            }
        }
        $scope.loading = false;
        $scope.blockloader = false;
    };

    var unique = function (origArr) {
        var newArr = [],
                origLen = origArr.length,
                found, x, y;
        for (x = 0; x < origLen; x++) {
            found = undefined;
            for (y = 0; y < newArr.length; y++) {
                if (origArr[x] === newArr[y]) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                newArr.push(origArr[x]);
            }
        }
        return newArr;
    };

    $scope.save_continue_others = function () {
        $('#btn_new_hiring_job_opp').click();
        $scope.save_others();
    };

    $scope.update_continue_others = function () {
        $('#btn_edit_hiring_job_opp').click();
        $scope.update_others();
    };

    $scope.save_others = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate('api_hrm_document_job_others_save'));
        if ($scope.jobId)
            $scope.new_job_others.jobId = $scope.jobId;
        url.save($scope.new_job_others,
                function (successResult) {
                    $scope.jobId = successResult["jobId"];
                    $scope.IsDisabled = false;
                    $scope.datatable();
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                },
                function (errorResult) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;
                });
    };

    $scope.update_others = function () {
        $scope.IsDisabled = true;
        var url = $resource(Routing.generate('api_hrm_document_job_others_save'));
        if ($scope.jobId)
            $scope.edit_job_others.jobId = $scope.jobId;
        url.save($scope.edit_job_others,
                function (successResult) {
                    $scope.jobId = successResult["jobId"];
                    $scope.IsDisabled = false;
                    $scope.datatable();
                    flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                },
                function (errorResult) {
                    flash($scope.flashTranslate('FLASH_INVALID'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    $scope.IsDisabled = false;
                });
    };

    $scope.contractSearch = '';
    $scope.filterByContract = function (contract) {
        if (contract === 'All') {
            $scope.contractSearch = '';
        } else {
            $scope.contractSearch = contract;
        }
    };

    $scope.toggleCustomSearch = function () {
        $scope.custom_search = $scope.custom_search === false ? true : false;
    };

    $scope.getCountry = function (id,value) {
        eval('var country = $("#'+id+'").find("option[value='+ value+']").text();');
        return country;
    };
  
});
