//Routing
(function () {
    'use strict';
    app.config(["$routeProvider", "$locationProvider", "$httpProvider", function ($routeProvider, $httpProvider) {

        return $routeProvider

            .when('/company',
                {
                    controller: 'companyCtrl',
                    templateUrl: Routing.generate('company_single_page')
                })
            .when('/hiring',
                {
                    controller: 'collaboraterCtrl',
                    templateUrl: Routing.generate('hiring_single_page')
                })

            .when('/collaborator',
                {
                    controller: 'collaboraterCtrl',
                    templateUrl: Routing.generate('collaborator_single_page')
                })

            .when('/expense',
                {
                    controller: 'expenseCtrl',
                    templateUrl: Routing.generate('expense_single_page')
                })

            .when('/absence',
                {
                    controller: 'requestAbsenceCtrl',
                    templateUrl: Routing.generate('absence_single_page')
                })

            .when('/remuneration',
                {
                    controller: 'collaboraterCtrl',
                    templateUrl: Routing.generate('remuneration_single_page')
                })

            .when('/export',
                {
                    controller: 'payPeriodExpenseCtrl',
                    templateUrl: Routing.generate('export_single_page')
                })

            .when('/preferences',
                {
                    controller: 'settingsCtrl',
                    templateUrl: Routing.generate('preference_single_page')
                })
            //
            // .when('/bonus',
            //     {
            //         controller: 'bonusCtrl',
            //         templateUrl: Routing.generate('bonus_single_page')
            //     })

            .when('/',
                {
                    controller: 'expenseCtrl',
                    templateUrl: Routing.generate('dashboard_single_page')
                })

            .otherwise({
                redirectTo: '/'
            });

    }]);

})();
