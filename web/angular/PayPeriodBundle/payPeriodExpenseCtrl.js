
/************ Pai paiPeriodExpenseCtrl ******************/
app.controller('payPeriodExpenseCtrl', function ($scope, $resource, $http, $filter, dataService,Ie11) {
    // retrive data from paiPeriodCtrl for search period
    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {

        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    }    
    $scope.payPeriodSearch = dataService.payPeriodSearch;
    $scope.collaborateurSearch = dataService.collaborateurSearch;
    $scope.searchText = dataService.searchText;
    /****************/
    $scope.date_next_pay_period = null;
    $scope.payperiodLength = null;
    $scope.pay_period = null;
    $scope.payperiod_add = function ()
    {
        var pay_period = ('01/' + $scope.pay_period).split('/');
        var addPaiPeriod = $resource(Routing.generate('apiSettingsPayPeriod_add'), null,
                {
                    'post': {method: 'POST'}
                });
        addPaiPeriod.post(null, {start_pay_period: pay_period[2] + '-' + pay_period[1] + '-' + pay_period[0]},
                function (data) {
                    $('#add_pp').modal('hide'); // close modal
                    if (data['status'] == 'ERR_CURRENT_PERIOD')
                    {
                        flash($scope.flashTranslate('FLASH_SELECT_MONTH'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    } else if (data['status'] === 'ERR_CURRENT_PERIOD_CLOSED')
                    {
                        flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED_ALREADY'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    } else
                    {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        $scope.get();
                        $scope.datatable();
                    }

                },
                function (error) {
                    flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };

    $scope.payperiod_open = function ()
    {
        var pay_period = ('01/' + $scope.open_pay_period).split('/');
        var update = $resource(Routing.generate('apiSettingsPayPeriod_open'), null,
                {
                    'update': {method: 'PUT'}
                });
        update.update(null, {next_pay_period: pay_period[2] + '-' + pay_period[1] + '-' + pay_period[0]},
                function (data) {
                    $('#open_pp').modal('hide'); // close modal
                    if (data['status'] == 'ERR_DIFF_PERIOD')
                    {
                        flash($scope.flashTranslate('FLASH_SELECT_MONTH_FOLLOWING'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                    } else
                    {
                        flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        $scope.get();
                    }



                },
                function (error) {
                    flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };

    $scope.get = function () {
        var getpayperiod = $resource(Routing.generate('apiSettingsPayPeriod_get'));
        getpayperiod.get({display: 'no'},
                function (data) {
                    if (data.payperiodlist !== undefined)
                    {
                        $scope.ArrayOfPayPeriod = [];
                        $scope.ArrayOfPayPeriod = data;
                        if (Object.keys(data.payperiodlist).length == 2)
                        {
                            $scope.ArrayOfPayPeriod.payperiodlist[0].start_date = moment($filter('timezone')($scope.ArrayOfPayPeriod.payperiodlist[0].start_date)).format('MM/YYYY');

                            $scope.ArrayOfPayPeriod.payperiodlist[1].start_date = moment($filter('timezone')($scope.ArrayOfPayPeriod.payperiodlist[1].start_date)).format('MM/YYYY');

                            $scope.payperiodLength = 2;
                        } else if (Object.keys(data.payperiodlist).length == 1)
                        {
                            $scope.ArrayOfPayPeriod.payperiodlist[0].start_date = moment($filter('timezone')($scope.ArrayOfPayPeriod.payperiodlist[0].start_date)).format('MM/YYYY');

                            $scope.first_pay_period = $scope.ArrayOfPayPeriod.payperiodlist[0].start_date;
                            $scope.payperiodLength = 1;
                        } else
                        {
                            $scope.payperiodLength = 0;
                        }
                    } else
                        $scope.payperiodLength = 0;
                },
                function (error) {
                    OnError(error.statusText);
                });
    };

    $scope.payperiod_close = function () {
        if ($scope.pp_status !== 'CLOTURE')
        {
            $('.c_pp_status').focus();
            return;
        }
        var update = $resource(Routing.generate('apiSettingsPayPeriod_close'), null,
                {
                    'update': {method: 'PUT'}
                });
        update.update(null, {status: $scope.pp_status, id: $scope.payToClose.id},
                function (data) {

                    if (data['status'] == 'ERR_NOT_FOUND')
                    {
                        flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    } else
                    {
                        flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        $scope.get();
                    }

                    $('#close_pp').modal('hide'); // close modal

                },
                function (error) {
                    flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    }

    $scope.datatable = function (period)
    {
        Ie11.Params(period);
        $scope.loading_expense = true;
        var infos = $resource(Routing.generate('apiSettingsPayPeriodExpenceBonus_list', {_period: period}));
        infos.get(
                function (data) {
                    if (data.users.length == 0)
                    {
                        $scope.loading_expense = false;
                        return;
                    }

                    var username = [];
                    $scope.data = data;
                    /*for (var i = 0; i < data.users.length; i++) {
                        username.push(data.users[i].user.username);
                        username.push(data.users[i].user.first_name+" "+data.users[i].user.last_name);
                    }*/

                    var somme_expense = 0, somme_advantage = 0, somme_transport = 0, somme_payday = 0, somme_bonus = 0;
                    $.each(data.users, function (j, item1) {
                        username.push($.trim(item1.user.first_name)+" "+$.trim(item1.user.last_name));
                        item1.FLname = $.trim(item1.user.first_name)+" "+$.trim(item1.user.last_name);       
                        if (data.bonus != null)
                            $.each(data.bonus, function (i, item2) {
                                if (item1.user.id === item2.emp)
                                {
                                    somme_bonus = somme_bonus + (parseFloat(item2.bonus_amount) || 0);
                                }
                            });
                        //get all bonus amount (bonus)
                        item1.bonus_amount = somme_bonus;
                        somme_bonus = 0;
                        if (data.public_transport != null)
                            $.each(data.public_transport, function (i, item2) {
                                if (item1.user.id === item2.emp)
                                {
                                    somme_transport = somme_transport + (parseFloat(item2.publictransport_amount) || 0);
                                }                                
                            });
                        if (data.personal_transport != null)
                            $.each(data.personal_transport, function (i, item2) {
                                {
                                    if (item1.user.id === item2.emp)
                                    {
                                        somme_transport = somme_transport + (parseFloat(item2.personaltransport_amount) || 0);
                                    }
                                }
                            });
                        // get all trans amount (personal, public)
                        item1.transport_amount = somme_transport;
                        somme_transport = 0;
                        if (data.advantage_housing != null)
                            $.each(data.advantage_housing, function (i, item2) {
                                if (item1.user.id === item2.emp)
                                {
                                    somme_advantage = somme_advantage + (parseFloat(item2.housing_amount) || 0);
                                }
                            });
                        if (data.advantage_dining != null)
                            $.each(data.advantage_dining, function (i, item2) {
                                if (item1.user.id === item2.emp)
                                {
                                    somme_advantage = somme_advantage + (parseFloat(item2.dinning_amount) || 0);
                                }
                            });
                        item1.advantage_amount = somme_advantage;
                        somme_advantage = 0;
                        if (data.expense_ik != null)
                            $.each(data.expense_ik, function (i, item2) {
                                if (item1.user.id === item2.emp)
                                {
                                    somme_expense = somme_expense + (parseFloat(item2.expense_ik_amount) || 0);
                                    //item1.period = moment($filter('timezone')(item2.period)).format('MM/YYYY');
                                }
                            });

                        if (data.expense_transport != null)
                            $.each(data.expense_transport, function (i, item2) {
                                if (item1.user.id === item2.emp) {
                                    somme_expense = somme_expense + (parseFloat(item2.expense_transport_amount) || 0);
                                    //item1.period = moment($filter('timezone')(item2.period)).format('MM/YYYY');
                                }
                            });
                        if (data.expense_accomodation != null)
                            $.each(data.expense_accomodation, function (i, item2) {
                                if (item1.user.id === item2.emp)
                                {
                                    somme_expense = somme_expense + (parseFloat(item2.expense_accomodation_amount) || 0);
                                    //item1.period = moment($filter('timezone')(item2.period)).format('MM/YYYY');
                                }
                            });
                        if (data.expense_dinning != null)
                            $.each(data.expense_dinning, function (i, item2) {
                                if (item1.user.id === item2.emp)
                                {
                                    somme_expense = somme_expense + (parseFloat(item2.expense_dining_amount) || 0);
                                    //item1.period = moment($filter('timezone')(item2.period)).format('MM/YYYY');
                                }
                            });
                        if (data.expense_other != null)
                            $.each(data.expense_other, function (i, item2) {
                                if (item1.user.id === item2.emp)
                                {
                                    somme_expense = somme_expense + (parseFloat(item2.expense_other_amount) || 0);
                                    //item1.period = moment($filter('timezone')(item2.period)).format('MM/YYYY');
                                }
                            });
                        // get all expense amount (ik, trans, accom, dinning, other)
                        item1.expense_amount = somme_expense;
                        somme_expense = 0;

                        if (data.payday != null)
                            $.each(data.payday, function (i, item2) {
                                if (item1.user.id === item2.emp)
                                {
                                    somme_payday = somme_payday + (parseFloat(item2.payday_amount) || 0);
                                }
                            });
                        // get amount of all payday
                        item1.payday_amount = somme_payday;
                        somme_payday = 0;

                        // remove user if empty data expense and other
                        if ((item1.bonus_amount == 0) &&
                                (item1.transport_amount == 0) &&
                                (item1.advantage_amount == 0) &&
                                (item1.expense_amount == 0) &&
                                (item1.payday_amount == 0))
                        {
                            delete data.users[j];
                        }
                    });


                    $scope.CollabArrayValues = username;
                    for (var i = 0; i < $scope.data.users.length; i++) {
                        $scope.tasks = $scope.data.users;
                        $scope.perPage = 10;
                        $scope.maxSize = 5;
                        $scope.setPage = function (pageNo) {
                            $scope.currentPage = pageNo;
                        };
                        $scope.$watch('searchText.item', function (term) {
                            var obj = term;
                            $scope.filterList = $filter('filter')($scope.tasks, obj);
                            $scope.currentPage = 1;
                        });
                    }
                    $scope.loading_expense = false;
                },
                function (error) {
                });
    };

    $scope.pushToArray = function (array, value) {
        if (array.indexOf(value) === -1) {
            array.push(value);
        }
        return array;
    };

}); /*********** end pai period ******************/

//var flash = function (msg = null, title = null, type = null, icon = null)
