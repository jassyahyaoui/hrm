
/************ Pai Period ******************/
app.controller('payPeriodCtrl', function ($scope, $resource, $http, $filter, dataService, $rootScope, $translate,Ie11) {
    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {

        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    }
    
    $scope.changeLanguage = function (key) {
        //console.log("please translate "+key);
        $rootScope.$broadcast('changeLanguage', key);

    };

    $scope.$on('changeLanguage', function (event, args) {
        //console.log("recived translate");
        $translate.use(args);

    });

    $scope.changeLanguage = function (key) {
        //console.log("i will translate");
        $translate.use('fr');
    };

    $scope.userData = {
    };
    // Toggle button to display dropdown details filters 	
    $scope.custom = true;
    $scope.toggleCustom = function () {
        $scope.custom = $scope.custom === false ? true : false;
    };
    $scope.custom_search = true;
    $scope.toggleCustomSearch = function () {
        $scope.custom_search = $scope.custom_search === false ? true : false;
    };




    // send data to  dataService and get it by paiPeriodExpenseCtrl
    $scope.payPeriodSearch = dataService.payPeriodSearch;
    $scope.collaborateurSearch = dataService.collaborateurSearch;
    $scope.searchText = dataService.searchText;
    /************/

    $scope.date_next_pay_period = null;
    $scope.payperiodLength = null;
    $scope.pay_period = null;
    $scope.payperiod_add = function ()
    {
        var pay_period = ('01/' + $scope.pay_period).split('/');
        var addPaiPeriod = $resource(Routing.generate('apiSettingsPayPeriod_add'), null,
                {
                    'post': {method: 'POST'}
                });
        addPaiPeriod.post(null, {start_pay_period: pay_period[2] + '-' + pay_period[1] + '-' + pay_period[0]},
                function (data) {
                    $('#add_pp').modal('hide'); // close modal
                    if (data['status'] == 'ERR_CURRENT_PERIOD')
                    {
                        flash($scope.flashTranslate('FLASH_SELECT_MONTH'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    } else if (data['status'] === 'ERR_CURRENT_PERIOD_CLOSED')
                    {
                        flash($scope.flashTranslate('FLASH_PAYPERIOD_CLOSED_ALREADY'), '', 'danger', 'glyphicon  glyphicon-warning-sign');

                    } else
                    {
                        flash($scope.flashTranslate('FLASH_VALID_ADD'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        $scope.get();
                        $scope.datatable();
                    }

                },
                function (error) {
                    flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };

    $scope.payperiod_open = function ()
    {
        //console.log($scope.open_pay_period);return;
        var pay_period = ('01/' + $scope.open_pay_period).split('/');
        var update = $resource(Routing.generate('apiSettingsPayPeriod_open'), null,
                {
                    'update': {method: 'PUT'}
                });
        update.update(null, {next_pay_period: pay_period[2] + '-' + pay_period[1] + '-' + pay_period[0]},
                function (data) {
                    $('#open_pp').modal('hide'); // close modal
                    if (data['status'] == 'ERR_DIFF_PERIOD')
                    {
                        flash($scope.flashTranslate('FLASH_SELECT_MONTH_FOLLOWING'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    } else if (data['status'] == 'ERR_CLOSED_PERIOD')
                    {
                        flash($scope.flashTranslate('PAYPERIOD_CLOSED_MONTH'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    } else
                    {
                        flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        $scope.get();
                    }



                },
                function (error) {
                    flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    };


    // retrive open pay period list
    $scope.get = function () {
        var getpayperiod = $resource(Routing.generate('apiSettingsPayPeriod_get'));
        getpayperiod.get({display: 'no'},
                function (data) {
                    console.log(data);
                    if (data.payperiodlist !== undefined)
                    {

                        $scope.ArrayOfPayPeriod = [];
                        $scope.ArrayOfPayPeriod = data;
                        if (Object.keys(data.payperiodlist).length == 2)
                        {
                            $scope.ArrayOfPayPeriod.payperiodlist[0].start_date = moment($filter('timezone')($scope.ArrayOfPayPeriod.payperiodlist[0].start_date)).format('MM/YYYY');

                            $scope.ArrayOfPayPeriod.payperiodlist[1].start_date = moment($filter('timezone')($scope.ArrayOfPayPeriod.payperiodlist[1].start_date)).format('MM/YYYY');

                            $scope.payperiodLength = 2;
                        } else if (Object.keys(data.payperiodlist).length == 1)
                        {
                            $scope.ArrayOfPayPeriod.payperiodlist[0].start_date = moment($filter('timezone')($scope.ArrayOfPayPeriod.payperiodlist[0].start_date)).format('MM/YYYY');

                            $scope.first_pay_period = $scope.ArrayOfPayPeriod.payperiodlist[0].start_date
                            $scope.payperiodLength = 1;
                        } else
                        {
                            $scope.payperiodLength = 0;
                        }

                        $('.paypriod-btn').datepicker('setDate', ( ($scope.ArrayOfPayPeriod.payperiodlist[0].status === 'open')?
                                                                    $scope.ArrayOfPayPeriod.payperiodlist[0].start_date : $scope.ArrayOfPayPeriod.payperiodlist[1].start_date));
                        $('.date-picker-expense').datepicker('setDate', ( ($scope.ArrayOfPayPeriod.payperiodlist[0].status === 'open')?
                                                                    $scope.ArrayOfPayPeriod.payperiodlist[0].start_date : $scope.ArrayOfPayPeriod.payperiodlist[1].start_date));
                        $('.date-picker-slct').datepicker('setDate', ( ($scope.ArrayOfPayPeriod.payperiodlist[0].status === 'open')?
                                                                    $scope.ArrayOfPayPeriod.payperiodlist[0].start_date : $scope.ArrayOfPayPeriod.payperiodlist[1].start_date));
                    } else
                        $scope.payperiodLength = 0;
                },
                function (error) {
                    OnError(error.statusText);
                });
    };

    $scope.payperiod_close = function () {
        
        if ( $.inArray($scope.pp_status,['CLOTURE', 'FENCED']) === -1)
        {
            $('.c_pp_status').focus();
            return;
        }
        var update = $resource(Routing.generate('apiSettingsPayPeriod_close'), null,
                {
                    'update': {method: 'PUT'}
                });
        update.update(null, {status: $scope.pp_status, id: $scope.payToClose.id},
                function (data) {

                    if (data['status'] == 'ERR_NOT_FOUND')
                    {
                        flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                    } else
                    {
                        flash($scope.flashTranslate('FLASH_VALID_MODIFY'), '', 'success', 'glyphicon  glyphicon-ok-sign');
                        $scope.get();
                    }

                    $('#close_pp').modal('hide'); // close modal

                },
                function (error) {
                    flash($scope.flashTranslate('UNABLE_CHANGE_REQUEST'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                });
    }

    $scope.datatable = function (period)
    {
        Ie11.Params(period);
        $scope.show_export = false;
        $scope.loading = true;

        var infos = $resource(Routing.generate('apiSettingsPayPeriod_list', {_period: period}));
        infos.get(
                function (data) {
                    // when reload datatable
                    delete $scope.data;
                    delete $scope.perPage;
                    delete $scope.maxSize;
                    delete $scope.currentPage;
                    delete $scope.tasks;
                    delete $scope.filterList;
                    delete $scope.CollabArrayValues;

                    if (data['status'])
                    {
                        // flash('Veuillez choisir une période valide', '', 'danger', 'glyphicon  glyphicon-warning-sign');
                        $scope.show_export = false;
                        $scope.loading = false;
                        return false;
                    }
                    if (data.users.length == 0)
                    {
                        $scope.loading = false;
                        return;
                    }
                    var username = [];
                    /******/
                    $scope.data = data;

                    $.each(data.users, function (i, item1) {
                        username.push($.trim(item1.user.first_name)+" "+$.trim(item1.user.last_name));
                        item1.FLname = $.trim(item1.user.first_name)+" "+$.trim(item1.user.last_name);
                        if (data.absence.length != 0)
                        {
                            $scope.show_export = true;
                            $.each(data.absence, function (j, item2) {
                                if (item1.user.id === item2.emp)
                                {
                                    item1.nbr_rtt = item2.nbr_rtt;
                                    item1.nbr_cp = item2.nbr_cp;
                                    item1.nbr_others = item2.nbr_others;
                                    item1.startdate = moment($filter('timezone')(item2.startdate)).format('MM/YYYY');
                                }
                            });
                            
                        }
                        if (data.training.length != 0)
                        {
                            $scope.show_export = true;
                            $.each(data.training, function (j, item2) {
                                if (item1.user.id === item2.emp)
                                {
                                    item1.nbr_training = item2.nbr_training;
                                    item1.startdate = moment($filter('timezone')(item2.startDate)).format('MM/YYYY');
                                }
                            });
                        }
                        if ((item1.nbr_rtt == undefined) && (item1.nbr_cp == undefined) && (item1.nbr_others == undefined) && (item1.nbr_training == undefined))
                        {
                            delete data.users[i];
                        } else if ((item1.nbr_training == undefined))
                        {
                            item1.nbr_training = 0;
                        } else if ((item1.nbr_rtt == undefined) && (item1.nbr_cp == undefined) && (item1.nbr_others == undefined))
                        {
                            $scope.show_export = true;
                            item1.nbr_rtt = 0;
                            item1.nbr_cp = 0;
                            item1.nbr_others = 0;
                        }
                         

                    });

                    $scope.CollabArrayValues = username;
                    for (var i = 0; i < $scope.data.users.length; i++) {
                        $scope.tasks = $scope.data.users;
                        $scope.perPage = 10;
                        $scope.maxSize = 5;
                        $scope.setPage = function (pageNo) {
                            $scope.currentPage = pageNo;
                        };
                        $scope.$watch('searchText.item', function (term) {
                            var obj = term;
                            $scope.filterList = $filter('filter')($scope.tasks, obj);
                            $scope.currentPage = 1;
                        });
                    }
                    // remplir datepicker
                    $scope.getlist();

                    $scope.loading = false;

                    /************ end script */
                },
                function (error) {
                });
    };
    $scope.pushToArray = function (array, value) {
        if (array.indexOf(value) === -1) {
            array.push(value);
        }
        return array;
    };

    $scope.getExportCsv = function () {
        var exportpayperiod = $resource(Routing.generate('apiSettingsPayPeriod_export_csv'));
        exportpayperiod.get({display: 'no'},
                function (data) {
                    flash($scope.flashTranslate('EXPORTING_FILE'), '', 'danger', 'glyphicon  glyphicon-warning-sign');
                },
                function (error) {
                    OnError(error.statusText);
                });
    };

    $scope.getlist = function () {
        $scope.obj_payperiod_list = null;
        $scope.d_current_pp = null;
        var a_arr = new Array();
        var getListpayperiod = $resource(Routing.generate('apiSettingsPayPeriod_get_list'));
        getListpayperiod.get({display: 'no'},
                function (data) {
                    if (data.payperiodlist !== undefined)
                    {
                        /****************/
                        var j = 0;
                        $.each(data.payperiodlist, function (i, item1) {
                            item1.startDate = moment($filter('timezone')(item1.startDate)).format('MM/YYYY');

                            if (item1.status == 'open')
                            {
                                a_arr[j] = item1.startDate;
                                j++;
                            }
                        });
                        /****************/
                        $scope.obj_payperiod_list = data.payperiodlist;

                        /* $('.date-picker-slct').datepicker('remove');
                         $('.date-picker-slct').datepicker({
                         format: 'mm/yyyy',
                         //autoclose: true,
                         todayHighlight: false, //If true, highlights the current date.
                         startView: "months",
                         minViewMode: "months",
                         beforeShowMonth:function(d)
                         {                               
                         date_f = moment($filter('timezone')(d)).format('MM/YYYY');
                         
                         if (jQuery.inArray(date_f.toString(), a_arr) !== -1 ) 
                         {
                         return 'highlight';
                         }
                         return "";
                         
                         }
                         });*/

                    }

                },
                function (error) {
                    OnError(error.statusText);
                });
    };


    /******************************************* FILTER Date for export ***********************************************/

   /* $scope.format_date = function (date)
    {
        if (date === undefined || date == "")
            return;

        date_split = date.split('/');
        return moment($filter('timezone')(date_split[1] + '-' + date_split[0] + '-' + '01')).format('YYYY-MM');
    }*/
});
/*********** end pai period ******************/


