var app = angular.module('exportApp', ['ngRoute', 'ngResource', 'datatables', 'ui.bootstrap']);
//angular.module('exportApp').controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, export) {
//
//    $scope.export = export;
//    $scope.export = {
//        export: export
//    };
//
//    $scope.ok = function () {
//        $uibModalInstance.close($scope.export);
//    };
//
//    $scope.cancel = function () {
//        $uibModalInstance.dismiss('cancel');
//    };
//});

app.controller('exportCtrl', function ($scope, $http, $filter, $resource) {

//
//$scope.absencetype = {
//    selecteddd:{}
//};
    $scope.flashText = {};
    $scope.flashTranslate = function (alias) {

        $scope.flashText = $filter('translate')(alias);
        var MSG = $scope.flashText;
        return MSG;
    }
    $scope.userData = {
    };
    
      $scope.init = function()   
      {
            $scope.AbsencetypesList();
          
      }
  
    // selected fruits
    $scope.selection = [];
    
    // toggle selection for a given fruit by name
    $scope.toggleSelection = function toggleSelection(fruitName) {
      var idx = $scope.selection.indexOf(fruitName);
      
      // is currently selected
      if (idx > -1) {
        $scope.selection.splice(idx, 1);
      }
      
      // is newly selected
      else {
        $scope.selection.push(fruitName.type);
      }
    };
    
    
// // Selected fruits
//  $scope.selection = [];
//
//  // Helper method to get selected fruits
//  $scope.selectedFruits = function selectedFruits() {
//    return filterFilter($scope.fruits, { selected: true });
//  };
//
//  // Watch fruits for changes
//  $scope.$watch('fruits|filter:{selected:true}', function (nv) {
//    $scope.selection = nv.map(function (fruit) {
//      return fruit.name;
//    });
//  }, true);
  
 // $scope.AbsencetypesList();

  $scope.AbsencetypesList = function()
    {
    var url = Routing.generate('api_getabsencetypes');
     console.log(url);
     
        $http({
            method: 'GET',
            url: url,
        }).then(function successCallback(response) {
           
            $scope.absencetypes = response.data;
        }, function errorCallback(response) {
        });
    };
  
     $scope.bonusList = function()
    {
    var url = Routing.generate('api_getbonus');
     console.log(url);
     
        $http({
            method: 'GET',
            url: url,
        }).then(function successCallback(response) {
           
            $scope.bonuss = response.data;
        }, function errorCallback(response) {
        });
    };
    
    
    
    $scope.submit = function ()
    {
        console.log("$scope");
        var url = Routing.generate('api_postexport');
        console.log($scope.export);
        $http({
            method: 'POST',
            url: url,
            data: $scope.export,
        }).then(function successCallback(response) {
            if(response);
            {var anchor = angular.element('<a/>');
            anchor.attr({
                href: 'data:attachment/csv;charset=utf-8,' + encodeURI(response.data),
                target: '_blank',
                download: 'filename.csv'
            })[0].click();
            }
        }, function errorCallback(response) {
        });
    }

    $scope.init = function () {
        var url = Routing.generate('api_gettransport');
        $http({
            method: 'GET',
            url: url,
        }).then(function successCallback(response) {
            $scope.transports = response.data.transports;
            $scope.users = response.data.users;
        }, function errorCallback(response) {
            OnError($scope.flashTranslate('MSG_UNABLE_LOAD_TRANSPORT'));
        });
    }

    var OnSuccess = function (msg, title = '', icon = 'glyphicon  glyphicon-ok-sign')
    {
        $.notify({
            icon: icon,
            title: ((title == '') ? msg : title),
            message: ((title == '') ? title : msg),
        }, {
            element: 'body',
            position: null,
            type: "success",
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: false,
            placement: {
                from: "bottom",
                align: "left"
            },
            offset: 20,
            spacing: 10,
            z_index: 1031,
            delay: 5000,
            timer: 1000,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            icon_type: 'class',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title"><b>{1}</b><br></span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
        });
    }

    var OnError = function (msg, title = '', icon = 'glyphicon  glyphicon-warning-sign')
    {

        $.notify({
            icon: icon,
            title: ((title == '') ? msg : title),
            message: ((title == '') ? title : msg),
        }, {
            element: 'body',
            position: null,
            type: "danger",
            allow_dismiss: true,
            newest_on_top: false,
            showProgressbar: false,
            placement: {
                from: "bottom",
                align: "left"
            },
            offset: 20,
            spacing: 10,
            z_index: 1031,
            delay: 5000,
            timer: 1000,
            animate: {
                enter: 'animated fadeInDown',
                exit: 'animated fadeOutUp'
            },
            icon_type: 'class',
            template: '<div data-notify="container" class="col-xs-11 col-sm-3 alert alert-{0}" role="alert">' +
                    '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                    '<span data-notify="icon"></span> ' +
                    '<span data-notify="title"><b>{1}</b><br></span> ' +
                    '<span data-notify="message">{2}</span>' +
                    '<div class="progress" data-notify="progressbar">' +
                    '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                    '</div>' +
                    '<a href="{3}" target="{4}" data-notify="url"></a>' +
                    '</div>'
        });
    }
});

app.config(function ($interpolateProvider) {
    $interpolateProvider.startSymbol('{[{').endSymbol('}]}');
});

app.config(function ($routeProvider) {
    $routeProvider
            .when('/',
                    {
                        controller: 'exportCtrl',
                        templateUrl: Routing.generate('export_index')
                    })
});

app.config(function ($locationProvider) {
    $locationProvider.hashPrefix('');
});